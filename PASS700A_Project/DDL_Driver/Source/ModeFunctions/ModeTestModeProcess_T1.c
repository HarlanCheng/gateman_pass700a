//------------------------------------------------------------------------------
/** 	@file		ModeTestModeProcess_T1.c
	@version 0.1.00
	@date	2016.04.27
	@brief	Test Mode Package
	@remark	테스트 모드 함수
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.27		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"

BYTE gbBatteryLifeCycleMode = 0;		
BYTE gbBatteryLifeCycleTimer1s = 0;
BYTE bBuffIndex = 0;
BYTE bPinCodeDataBuf[8] = {0xFF,};

BYTE gbMenuExchangeKey = 0x00;
BYTE gbMenuInnerExchangeKey = 0x00;

DWORD gTotalMotorRunCnt = 0;
WORD gLowBatMotorRunCnt = 0;

extern float AdcToPwrVoltFactor;

extern uint32_t LongSleepOn;

#define SEND_MSG printf("Bat : %.4fV ADC : %04ld Count : %04ld LowBatt : %04ld \r\n",((float)GetBatteryAdcValue()) * AdcToPwrVoltFactor,GetBatteryAdcValue(),gTotalMotorRunCnt,gLowBatMotorRunCnt)

void ModeGotoTestMenu(void)
{
	gbMainMode = MODE_TEST_MENU;
	gbModePrcsStep = 0;
}

void ModeTestMenu(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(VOICE_MIDI_INTO, VOL_CHECK);
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(2, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					gbModePrcsStep++;
					break;		

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;

				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}
			break;

		case 2:
			TestMenuSelectProcess();
			break;

		default:
			ModeClear();
			break;
	}
}



void ModeGotoTestAging5s(void);
void ModeGotoTestAging10s(void);
void ModeGotoMotiseCal(void);
void ModeGotoLongSleepSet(void);

#if defined (FACOTRY_PINCODE_TEST_MODE)
void ModeGotoPinCodeTestMode(void);
#endif 

#if defined (_USE_LOGGING_MODE_)
void ModeGotoLoggingMode(void);
void ModeGotoTimeSet(void);
#endif 

#ifdef	BLE_N_SUPPORT
void ModeGotoTestBLEN(void);
#endif 


void ModeBatteryLifeCycleEnable(void);

#ifdef 	DDL_CFG_RFID
void ModeGotoOpenUIDSet(void);
#endif 


void TestMenuSelectProcess(void)
{
	switch(gPinInputKeyFromBeginBuf[0])
	{
		case MENU_LONG_SLEEP:
			ModeGotoLongSleepSet();
			break;	
			
		case MENU_TESTMODE_AGING:
			ModeGotoTestAging5s();
			break;				

		case MENU_TESTMODE_AGING+3:
			ModeGotoTestAging10s();
			break;				

		case MENU_TESTMODE_BATTERY_LIFE_CYCLE_AGING:
			ModeBatteryLifeCycleEnable();
			break;				

#ifdef 	DDL_CFG_RFID
		case MENU_TESTMODE_OPENUID_SET:
			ModeGotoOpenUIDSet();
			break;				
#endif 

/*	
		case MENU_TESTMODE_VOICE:
			gbMainMode = MODE_TEST_VOICE;
			gbModePrcsStep = 0;
			break;				
*/			
#ifdef	BLE_N_SUPPORT
		case MENU_TESTMODE_BLE_APP_KEY:			
		case MENU_TESTMODE_BLE_APP:
		case MENU_TESTMODE_BLE_RF:			
			ModeGotoTestBLEN();
			break;	
#endif 			

#ifdef	_MASTERKEY_IBUTTON_SUPPORT
		case MENU_SPECIALMODE_MASTERKEY_REG:
			ModeGotoSpecialModeMasterKeyRegister();
			break;
#endif

#ifdef	_MASTER_CARD_SUPPORT
		case MENU_SPECIALMODE_MASTERKEY_REG:
			ModeGotoMasterCardRegister();
			break;
#endif


#if defined (DDL_CFG_MS)
		case MS_MENU_CREDENTIAL_INFO_SET:
			Ms_Credential_Info.bSelected_Item = CREDENTIAL_INFO_SET;
			MS_ModeGotoAuthorityVerify();
			break;
#endif 

#if defined (_USE_LOGGING_MODE_)
		case MENU_LOGGING_MODE:
			ModeGotoLoggingMode();
			break;

		case MENU_TIME_SET_MODE:
			ModeGotoTimeSet();
			break;			
#endif 

#if defined (FACOTRY_PINCODE_TEST_MODE)
		case MENU_PIOCODE_TEST_MODE:
			ModeGotoPinCodeTestMode();
			break;
#endif 

		default:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;
	}
}




void ModeGotoTestAging5s(void)
{
	SetupUartforPrintf(gh_mcu_uart_com);
	InitPrintfService(NULL,NULL,NULL);	
	gbMainMode = MODE_TEST_AGING;
	gbModePrcsStep = 0;
}


void ModeGotoTestAging10s(void)
{
	SetupUartforPrintf(gh_mcu_uart_com);
	InitPrintfService(NULL,NULL,NULL);	
	gbMainMode = MODE_TEST_AGING;
	gbModePrcsStep = 10;
}

void ModeGotoMotiseCal(void)
{
	gbMainMode = MODE_TEST_GET_CENTER;
	gbModePrcsStep = 0;
}

#ifdef	BLE_N_SUPPORT
void ModeGotoTestBLEN(void)
{
	gbMainMode = MODE_TEST_BLE_N;
	gbModePrcsStep = 0;
}
#endif 

#ifdef 	DDL_CFG_RFID
void ModeGotoOpenUIDSet(void)
{
	gbMainMode = MODE_TEST_OPEN_UID_SET;
	gbModePrcsStep = 0;
}
#endif 

void ModeGotoLongSleepSet(void)
{
	LongSleepOn = MAGIC_ENTER_LONG_SLEEP;
	FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
	ModeClear();
}


void ModeBatteryLifeCycleEnable(void)
{
	gbBatteryLifeCycleMode = 1;
	gTotalMotorRunCnt = 0;
	gLowBatMotorRunCnt = 0;
	ModeClear();
	printf("\r\n Battery life Cycle Test Enable and Start \r\n");
}

void ModeBatteryLifeCycleCounter(void)
{
	if(gbBatteryLifeCycleTimer1s)	--gbBatteryLifeCycleTimer1s;
}


BYTE GetTestModeTime(void)
{
	return gbBatteryLifeCycleTimer1s;
}

BYTE ModeBatteryLifeCycleKeyInput(BYTE Key , BYTE Ontime ,BYTE Step)
{
	gbInputKeyValue = Key;
	gbBatteryLifeCycleTimer1s = Ontime;	
	gbBatteryLifeCycleMode = Step;	
	return 0 ;
}

void ModeBatteryLifeCycleTest(void)
{
	BYTE bTmp;
	
	switch(gbBatteryLifeCycleMode)
	{
		case 0:

		break;

		case 1:
			SetupUartforPrintf(gh_mcu_uart_com);
			InitPrintfService(NULL,NULL,NULL);
			gbBatteryLifeCycleMode = 3;
		break;

		case 2:
			if(GetTestModeTime()) break; 	
			gbBatteryLifeCycleMode = 3;
		break;

		case 3:
			memset(bPinCodeDataBuf, 0xFF, 8);
			RomRead(bPinCodeDataBuf, (WORD)USER_CODE, 8);	
			
#if defined (_USE_IREVO_CRYPTO_)
			EncryptDecryptKey(bPinCodeDataBuf,8);
#endif 	
			gbBatteryLifeCycleMode++;
			bBuffIndex = 0;
		break;

		case 4:
			ModeBatteryLifeCycleKeyInput(TENKEY_MULTI,2,5);
		break;

		case 5:
			if(GetLedMode() || GetBuzPrcsStep() ||GetVoicePrcsStep() )	break;
			if(GetTestModeTime()) break; 			
			
			if((bPinCodeDataBuf[bBuffIndex] >> 4) != 0x0F)
				ModeBatteryLifeCycleKeyInput(bPinCodeDataBuf[bBuffIndex] >> 4,1,6);
			else 
				ModeBatteryLifeCycleKeyInput(TENKEY_STAR,10,9);
		break;

		case 6:
			if(GetTestModeTime()) break; 			

			if((bPinCodeDataBuf[bBuffIndex] & 0x0F) != 0x0F)
			{
				ModeBatteryLifeCycleKeyInput(bPinCodeDataBuf[bBuffIndex] & 0x0F,1,5);
				bBuffIndex++;
			}
			else 
				ModeBatteryLifeCycleKeyInput(TENKEY_STAR,10,9);
		break;

		case 9:
			if(GetTestModeTime() == 0)
			{
				gbBatteryLifeCycleMode = 3;
				break;
			}
			
			gbInputKeyValue = TENKEY_NONE;

			if(GetMotorPrcsStep()) break; 

			bTmp = GetFinalMotorStatus();
			
			if(bTmp == FINAL_MOTOR_STATE_OPEN)
			{
				if(GetLedMode() || GetBuzPrcsStep() ||GetVoicePrcsStep()) break;
				
				gbBatteryLifeCycleMode = 10;
				gbBatteryLifeCycleTimer1s = 2;

				gTotalMotorRunCnt++;
				if(gfLowBattery)
				{
					gLowBatMotorRunCnt++;
				}

				SEND_MSG;
			}
		break;

		case 10:

			if(GetTestModeTime()) break; 	
		
			ModeBatteryLifeCycleKeyInput(TENKEY_MULTI,10,12);
		break;

		case 12:
			if(GetTestModeTime() == 0)
			{
				gbBatteryLifeCycleMode = 10;
				break;
			}

			gbInputKeyValue = TENKEY_NONE;

			if(GetMotorPrcsStep()) break; 
			
			bTmp = GetFinalMotorStatus();
			
			if(bTmp == FINAL_MOTOR_STATE_CLOSE)
			{
				if(GetLedMode() || GetBuzPrcsStep() ||GetVoicePrcsStep()) break;

				gbBatteryLifeCycleMode = 2;
				gbBatteryLifeCycleTimer1s = 2;
			}
		break;

		default:
			ModeClear();
			break;
	}
}




void MotorOpenCloseInAgingCompleteCheck(void);

void StartMotorToggleForAging(void)
{
	BYTE bTmp;

	PCErrorClearSet();

	bTmp = MotorSensorCheck();
	
	if(bTmp == SENSOR_OPEN_STATE)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		BatteryCheck();
#else
		FeedbackMotorClose();
#endif
		StartMotorClose();
	}
	else if(bTmp == SENSOR_CLOSE_STATE)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		BatteryCheck();
#else
		FeedbackMotorOpen();
#endif
		StartMotorOpen();
	}
	else
	{
		//When Motor is not opened, not closed, then Motor run to open or close 
		//on reference to previous Motor run
		switch(GetFinalMotorStatus())
		{
			case FINAL_MOTOR_STATE_CLOSE:
			case FINAL_MOTOR_STATE_CLOSE_ERROR:
				//When previous Motor run is close, then Motor run to open
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorOpen();
#endif
				StartMotorOpen();
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
				FPMotorCoverAction(_OPEN); 
#endif


				break;
			
			default:
				//When previous Motor run is open, then Motor run to close
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorClose();
#endif
				StartMotorClose();
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
						FPMotorCoverAction(_CLOSE); 
#endif

				break;
		}
	}
}


void ModeTestAging(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
		case 10:
			FeedbackKeyPadLedOn(VOICE_MIDI_INTO, VOL_CHECK);

			SetModeTimeOut(50);
			gbModePrcsStep++;
			break;

		case 1:
		case 11:
			switch(gbInputKeyValue)
			{
				case FUNKEY_REG:
//					FeedbackModeCompleted(1, VOL_CHECK);
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					ModeClear();
					break;
				
				default :
					if(GetModeTimeOut())	break;

					if(gbModePrcsStep == 1)
					{
						SetModeTimeOut(50);
					}
					else
					{
						SetModeTimeOut(100);
                    }                    

					StartMotorToggleForAging();

					gTotalMotorRunCnt++;
					if(gfLowBattery)
					{
						gLowBatMotorRunCnt++;
					}

					SEND_MSG;
					
					gbModePrcsStep++;
					break;
			}
			break;
	
		case 2:
		case 12:
			MotorOpenCloseInAgingCompleteCheck();
			break;
		
		default:
			ModeClear();
			break;
	}
}


uint32_t starttime;
uint32_t endtime;
uint32_t openCentertime;
uint32_t CloseCentertime;

void	CalMotorContol(uint8_t MotorContol );
void CalSensorControl(uint8_t SensorContol );

void ModeTestGetCenter(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
		{
			starttime = 0;
			endtime= 0;
			openCentertime = 0;
			CloseCentertime = 0;

			CalSensorControl(0);

			if(GetFinalMotorStatus() == SENSOR_OPEN_STATE)
			{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorClose();
#endif
				CalMotorContol(1);// close
				gbModePrcsStep = 1;
			}
			else if(GetFinalMotorStatus() == SENSOR_CLOSE_STATE)
			{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorOpen();
#endif
				CalMotorContol(0);// open
				gbModePrcsStep = 10;
			}
		}
		break;

		case 1:
                {
			CalSensorControl(3);
			
			if(GetFinalMotorStatus() == FINAL_MOTOR_STATE_CLOSE)
			{
				CalMotorContol(3);
				CalMotorContol(2);				
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				FeedbackMotorClose();
#endif
				starttime = HAL_GetTick();
				CalMotorContol(0);// open
				gbModePrcsStep = 2;
			}
                }
		break;


		case 2:
		{
			CalSensorControl(2);
				
			if(GetFinalMotorStatus() == FINAL_MOTOR_STATE_OPEN)
			{
				endtime = HAL_GetTick();
				CalMotorContol(3);
				CalMotorContol(2);
				CloseCentertime = (endtime -starttime) / 2; 
				printf("\r\nstart : %ldms\r\n",starttime);
				printf("end : %ldms \r\n",endtime);
				printf("Close to to center time : %ldms \r\n",CloseCentertime);
				//StartMotorClose();
				ModeClear();
			}
		}
		break;

		case 10:
		{
			CalSensorControl(2);

			if(GetFinalMotorStatus() == FINAL_MOTOR_STATE_OPEN)
			{
				CalMotorContol(3);
				CalMotorContol(2);				
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				FeedbackMotorClose();
#endif
				starttime = HAL_GetTick();
				CalMotorContol(1);// close
				gbModePrcsStep = 20;
			}
		}
		break;


		case 20:
		{
			CalSensorControl(3);

			if(GetFinalMotorStatus() == FINAL_MOTOR_STATE_CLOSE)
			{
				endtime = HAL_GetTick();
				CalMotorContol(3);
				CalMotorContol(2);
				openCentertime = (endtime -starttime) / 2; 
				printf("\r\nstart : %ldms\r\n",starttime);
				printf("end : %ldms \r\n",endtime);
				printf("Open to center time : %ldms \r\n",openCentertime);
				//StartMotorOpen();
				ModeClear();
			}
		}
		break;

		default:
			ModeClear();
		break;
	}
}


#ifdef	BLE_N_SUPPORT
void ModeTestBLEN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			if(gPinInputKeyFromBeginBuf[0] == MENU_TESTMODE_BLE_APP_KEY)
			{
				BLEN_Module_App_Test_Start(gPinInputKeyFromBeginBuf[1]);

				ModeClear();	  
				FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			}
			else
			{
				BLEN_Module_Test_Start(gPinInputKeyFromBeginBuf[0]);

				if(gPinInputKeyFromBeginBuf[0] == MENU_TESTMODE_BLE_RF)
				{
					gbModePrcsStep++;
					SetModeProcessTime(10);
				}
				else 
				{
					ModeClear();      
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
				}
			}
			break;

		case 1:
			if(GetModeProcessTime())break;
			gbModuleMode = PACK_ID_CONFIRMED;
			gbInnerModuleMode = PACK_ID_CONFIRMED;
			BLENTxStartScanSend(0x00);
			gbModuleMode = PACK_ID_NOT_CONFIRMED;
			gbInnerModuleMode = PACK_ID_NOT_CONFIRMED;
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
			break;

		default:
			ModeClear();
			break;
	}
}
#endif 

#ifdef 	DDL_CFG_RFID
void MenuOpenUIDSetSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		case TENKEY_1:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_TURNON_CONT_SHARP, gbInputKeyValue);		
			break;

		case TENKEY_3:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_TURNOFF_CONT_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_SHARP:		 //# 버튼음 출력
#ifdef	P_VOICE_RST	
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
				FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;

		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;

		default:
			TimeExpiredCheck();
			break;
	}	
}


void OpenUidSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		case 1: 	
			CardOpenUidSupportedInfoSave(0x74);
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
			break;
	
		case 3: 		
			CardOpenUidSupportedInfoSave(0xFF);			
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
			break;
	
		default:
			TimeExpiredCheck();
			break;
	}			
}


void ModeTestOpenUidSet(void)
{
		switch(gbModePrcsStep)
		{
			case 0:
				AudioFeedback(AVML_TURNON1_TURNOFF3, gcbBuzSta, VOL_CHECK);
				LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
				LedGenerate(LED_KEY_1|LED_KEY_3|LED_KEY_SHARP, DIMM_ON_SLOW, 0);
#endif
								
#ifdef	DDL_CFG_NO_DIMMER
				LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
				LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
				TenKeyVariablesClear();
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	
				gMenuSelectKeyTempSave = TENKEY_NONE;
	
				gbModePrcsStep++;			
				break;
	
			case 1: 		
				MenuOpenUIDSetSelectProcess();
				break;	
		
			case 2:
				if(gMenuSelectKeyTempSave != TENKEY_NONE)
				{
					if(GetBuzPrcsStep() || GetVoicePrcsStep())	break;
				}
	
				OpenUidSetSelected();
				break;
	
			default:
				ModeClear();
				break;
		}
	
}
#endif 

void MotorOpenCloseInAgingCompleteCheck(void)
{
	BYTE bTmp;

	bTmp = GetFinalMotorStatus();
	
	if(bTmp == FINAL_MOTOR_STATE_OPEN_ING)		return;
#if 0
	if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ING) 	return;
	
	if(bTmp == FINAL_MOTOR_STATE_OPEN_ERROR)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
#if defined (__DDL_MODEL_B2C15MIN) || defined (__DDL_MODEL_YDM7111A) || defined (__DDL_MODEL_YMH71)|| defined (__DDL_MODEL_YMH70A) ||defined (__DDL_MODEL_YDM3211A) ||defined (__DDL_MODEL_YDM4111A)
		ModeClear();
#endif 
	}
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
#if defined (__DDL_MODEL_B2C15MIN) || defined (__DDL_MODEL_YDM7111A) || defined (__DDL_MODEL_YMH71)|| defined (__DDL_MODEL_YMH70A)  ||defined (__DDL_MODEL_YDM3211A) ||defined (__DDL_MODEL_YDM4111A)
		ModeClear();		
#endif 
	}
	else if(bTmp == FINAL_MOTOR_STATE_OPEN)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorOpen();
#endif
	}
	else if(bTmp == FINAL_MOTOR_STATE_CLOSE)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorClose();
#endif
	}

	gbModePrcsStep--;
}


#if defined (FACOTRY_PINCODE_TEST_MODE)
void ModeGotoPinCodeTestMode(void)
{
	gbMainMode = MODE_PINCODE_TEST_MODE;
	gbModePrcsStep = 0;
}

uint16_t KeyCount[20] = {0x0000,};

void ModePincoeTestMode(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			SetupUartforPrintf(gh_mcu_uart_com);
			InitPrintfService(NULL,NULL,NULL);
			printf("\r\nStart Pincode Test Mode\r\n");
			FeedbackKeyPadLedOnBuzOnly(VOICE_MIDI_INTO,VOL_CHECK|MANNER_CHK); 
			TenKeyVariablesClear();
			gbModePrcsStep++;
			for(uint8_t i = 0 ; i < 20 ; i++)
			{
				KeyCount[i] = 0x0000;
			}
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
				case TENKEY_STAR:	
				case TENKEY_SHARP:
				case TENKEY_MULTI:
				case TENKEY_ERROR:
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK|MANNER_CHK, gbInputKeyValue);

					if(gbInputKeyValue == TENKEY_MULTI)
						gbInputKeyValue = 13;
					else if (gbInputKeyValue == TENKEY_ERROR)
						gbInputKeyValue = 14;
					
					KeyCount[gbInputKeyValue]++;
					printf("Total Count [%05ld] \r\n",++KeyCount[15]);
					printf("Key 1 count [%05ld] Key 2 count [%05ld] Key 3 count [%05ld] \r\n", KeyCount[TENKEY_1],KeyCount[TENKEY_2],KeyCount[TENKEY_3]);
					printf("Key 4 count [%05ld] Key 5 count [%05ld] Key 6 count [%05ld] \r\n", KeyCount[TENKEY_4],KeyCount[TENKEY_5],KeyCount[TENKEY_6]);
					printf("Key 7 count [%05ld] Key 8 count [%05ld] Key 9 count [%05ld] \r\n", KeyCount[TENKEY_7],KeyCount[TENKEY_8],KeyCount[TENKEY_9]);
					printf("Key * count [%05ld] Key 0 count [%05ld] Key # count [%05ld] \r\n", KeyCount[TENKEY_STAR],KeyCount[TENKEY_0],KeyCount[TENKEY_SHARP]);
					printf("Key M count [%05ld] Key E count [%05ld] (M=Multi , E=Error) \r\n\r\n", KeyCount[13],KeyCount[14]);
				break;

				case FUNKEY_REG:
					printf("\r\n ********************************************* \r\n");
					printf("\r\nStop Test \r\n");
					printf("Total Count [%05ld] \r\n",KeyCount[15]);
					printf("Key 1 count [%05ld] Key 2 count [%05ld] Key 3 count [%05ld] \r\n", KeyCount[TENKEY_1],KeyCount[TENKEY_2],KeyCount[TENKEY_3]);
					printf("Key 4 count [%05ld] Key 5 count [%05ld] Key 6 count [%05ld] \r\n", KeyCount[TENKEY_4],KeyCount[TENKEY_5],KeyCount[TENKEY_6]);
					printf("Key 7 count [%05ld] Key 8 count [%05ld] Key 9 count [%05ld] \r\n", KeyCount[TENKEY_7],KeyCount[TENKEY_8],KeyCount[TENKEY_9]);
					printf("Key * count [%05ld] Key 0 count [%05ld] Key # count [%05ld] \r\n", KeyCount[TENKEY_STAR],KeyCount[TENKEY_0],KeyCount[TENKEY_SHARP]);
					printf("Key M count [%05ld] Key E count [%05ld] (M=Multi , E=Error) \r\n\r\n", KeyCount[13],KeyCount[14]);
					printf("\r\n ********************************************* \r\n");
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					ModeClear();
				break;

				case TENKEY_NONE:
				default:
				break;
			}
			break;

		default:
			break;
	}
}

#endif 

#if defined (_USE_LOGGING_MODE_)

#define MAX_LOG_INDEX 500u
#define LOG_DATA_LENGTH 10u

static struct {
	uint8_t logModeOn;
	uint8_t loggingRxDataBuf[100];
	uint8_t loggingRxDataCnt;
	uint8_t processon;
	UART_HandleTypeDef* loggingUart;
	uint16_t index;
}Logging;

const uint8_t GetLockStatus[] = {
	'G','0'
};

const uint8_t GetLog[] = {
	'1','0'
};

const uint8_t DelAll[] = {
	'2','0'
};

const uint8_t GetTime[] = {
	'3','0'
};

const uint8_t SetTime[] = {
	't','i','m','e','0'
};

const uint8_t Modeexit[] = {
	'x','0'
};

const uint8_t Version[] = {
	'V','0'
};

void ModeGotoLoggingMode(void)
{
	gbMainMode = MODE_LOGGING_PROCESS;
	gbModePrcsStep = 0;
#ifdef STD_GA_374_2019
	if(gfEepromErr ||gfRtcErr)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		ModeClear();
	}
#endif 		
}

void ModeGotoTimeSet(void)
{
	gbMainMode = MODE_MENU_TIME_SET;
	gbModePrcsStep = 0;
#ifdef STD_GA_374_2019
	if(gfEepromErr ||gfRtcErr)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		ModeClear();
	}
#endif 		
}

void LoggingRxDataProcess(void)
{
	extern uint8_t commpack_rx_byte;
	if(commpack_rx_byte =='\r')
	{
		Logging.processon	= 1;	
	}
	else
	{
		printf("%c",commpack_rx_byte);
		Logging.loggingRxDataBuf[Logging.loggingRxDataCnt++] = commpack_rx_byte;
	}
}

void initRxdata(void)
{
	memset(Logging.loggingRxDataBuf,0x00,sizeof(Logging.loggingRxDataBuf));
	Logging.loggingRxDataCnt = 0;
	Logging.processon = 0;
}	

uint8_t GetLoggingMode(void)
{
	return Logging.logModeOn;
}

void DeleAlarmReportLog(void)
{
	RomWriteWithSameData(0x00,LOG_DATA_START,(LOG_DATA_LENGTH*MAX_LOG_INDEX));
	RomWriteWithSameData(0x00,LOG_INDEX,2);
}

void GetRTCTime(RTC_TimeTypeDef* ptime , RTC_DateTypeDef* pdate)
{
	HAL_RTC_GetTime(gh_mcu_rtc,ptime,RTC_FORMAT_BIN);
	HAL_RTC_GetDate(gh_mcu_rtc,pdate,RTC_FORMAT_BIN);
}

void SetRTCTime(uint8_t* pData)
{
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;

	sTime.Hours = ((pData[6] & 0x0F) << 4 | (pData[7] & 0x0F));
	sTime.Minutes = ((pData[8] & 0x0F) << 4 | (pData[9] & 0x0F));
	sTime.Seconds = ((pData[10] & 0x0F) << 4 | (pData[11] & 0x0F));
	sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sTime.StoreOperation = RTC_STOREOPERATION_RESET;
	HAL_RTC_SetTime(gh_mcu_rtc, &sTime, RTC_FORMAT_BCD);

	sDate.WeekDay = 0x00;
	sDate.Month = ((pData[2] & 0x0F) << 4 | (pData[3] & 0x0F));
	sDate.Date = ((pData[4] & 0x0F) << 4 | (pData[5] & 0x0F));
	sDate.Year = ((pData[0] & 0x0F) << 4 | (pData[1] & 0x0F));
	HAL_RTC_SetDate(gh_mcu_rtc, &sDate, RTC_FORMAT_BCD);
	
}


void MenuLoggingPrintProcess(void)
{
	if(!Logging.processon)
	{
		return;
	}

	if(Logging.loggingRxDataCnt == 0)
	{
		printf("\r\nInvaild Command \r\n");
		initRxdata();	
		gbModePrcsStep = 0;
		return;
	}

	if(memcmp(Logging.loggingRxDataBuf,GetLockStatus,Logging.loggingRxDataCnt) == 0)
	{
		extern LOCKSTATUS AbnormalStatus;
		RomRead(AbnormalStatus.gbLockInfo,LOCK_MODE_STATUS,24);
		printf("\r\nGet  Final Lock Status \r\n");
		printf("AbnormalStatus gwLockModeStatus [%4x] \r\n",AbnormalStatus.gwLockModeStatus);
		printf("AbnormalStatus MainMode [%d] \r\n",AbnormalStatus.gbLockInfo[4]);
		printf("AbnormalStatus gbModePrcsStep [%d] \r\n",AbnormalStatus.gbLockInfo[5]);
		printf("AbnormalStatus GetCardMode() [%d] \r\n",AbnormalStatus.gbLockInfo[6]);
		printf("AbnormalStatus gCardProcessStep [%d] \r\n",AbnormalStatus.gbLockInfo[7]);
		printf("AbnormalStatus uart1 Pack Connection Mode [%d] \r\n",AbnormalStatus.gbLockInfo[8]);
		printf("AbnormalStatus uart1 Pack Tx mode [%d] \r\n",AbnormalStatus.gbLockInfo[9]);
		printf("AbnormalStatus uart1 Pack Rx mode [%d] \r\n",AbnormalStatus.gbLockInfo[10]);
		printf("AbnormalStatus uart2 Pack Connection Mode [%d] \r\n",AbnormalStatus.gbLockInfo[11]);
		printf("AbnormalStatus uart2 Pack Tx mode [%d] \r\n",AbnormalStatus.gbLockInfo[12]);
		printf("AbnormalStatus uart2 Pack Rx mode [%d] \r\n",AbnormalStatus.gbLockInfo[13]);
		printf("AbnormalStatus GPIO interrupt PIN number LSB [%x] \r\n",AbnormalStatus.gbLockInfo[14]);
		printf("AbnormalStatus GPIO interrupt PIN number MSB	 [%x] \r\n",AbnormalStatus.gbLockInfo[15]);
		printf("AbnormalStatus SYSTEM_ERROR_FLAG [%x] \r\n",AbnormalStatus.gbLockInfo[16]);
		printf("AbnormalStatus DDL_STATEFLAG [%x] \r\n",AbnormalStatus.gbLockInfo[17]);
		printf("AbnormalStatus last key [%x] \r\n",AbnormalStatus.gbLockInfo[18]);
		printf("AbnormalStatus last switch [%x] \r\n",AbnormalStatus.gbLockInfo[19]);
		printf("AbnormalStatus RESERVED [%x] \r\n",AbnormalStatus.gbLockInfo[20]);
		printf("AbnormalStatus RESERVED [%x] \r\n",AbnormalStatus.gbLockInfo[21]);
		printf("AbnormalStatus Reset Count [%x] \r\n",AbnormalStatus.gbLockInfo[22]);
		printf("AbnormalStatus Silent Reset Flag [%x] \r\n\r\n\r\n",AbnormalStatus.gbLockInfo[23]);
		initRxdata();
		gbModePrcsStep = 0;
	}
	else if(memcmp(Logging.loggingRxDataBuf,GetLog,Logging.loggingRxDataCnt) == 0)
	{
		uint16_t tempindex = 0x0000;
		uint8_t AlarmData[LOG_DATA_LENGTH] = {0x00,};

		/* https://en.wikipedia.org/wiki/ANSI_escape_code */
		/* \033[    color blink etc...     \033[ */ 
		
		printf("\r\nGet Log Report \r\n\r\n");
		printf("*** Information ***\r\n");
		printf("*** \033[1;33mOP\033[0m : Unlock \r\n");
		printf("*** CL : Lock \r\n");
		printf("*** RC : Register Card \r\n");
		printf("*** RF : Register Fingerprint \r\n");
		printf("*** RP : Register Pincode \r\n");
		printf("*** DC : Delete Card \r\n");
		printf("*** DF : Delete Fingerprint \r\n");
		printf("*** DP : Delete Pincode \r\n");
		printf("*** ST : Set Time \r\n\r\n");
		printf("Log Start \r\n\r\n");
		
		RomRead(AlarmData,LOG_INDEX,2);
		tempindex = AlarmData[0] << 8 | AlarmData[1];

		if(tempindex == 0)
		{
			printf("Log end \r\n");
			initRxdata();	
			gbModePrcsStep = 0;			
			return;
		}

		for(uint16_t i = 0 ; i < MAX_LOG_INDEX ; i++)
		{
			RomRead(AlarmData,LOG_DATA_START+(LOG_DATA_LENGTH*(tempindex-1)),LOG_DATA_LENGTH);

#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif

			if(AlarmData[6] == 0x00 || tempindex == 0x0000)
			{
				break;
			}

			printf("%03d ",(tempindex == 0x00 ? MAX_LOG_INDEX : tempindex));
			printf("20%02x.",AlarmData[0]);
			printf("%02x.",AlarmData[1]);
			printf("%02x ",AlarmData[2]);
			printf("%02x:",AlarmData[3]);
			printf("%02x:",AlarmData[4]);
			printf("%02x :: ",AlarmData[5]);

			if(AlarmData[6] == F0_ALARM_REPORT_EX ||AlarmData[6] == F0_ALARM_REPORT )
			{
				switch(AlarmData[7])
				{
					/* Alarm report */
#if 0
					case AL_CODE_CHANGED_ADDED: 
					{
						printf("Master code was changed at keypad \r\n");
					}
					break;
					
					case AL_CODE_DELETED:
					{
						printf("AL_CODE_DELETED \r\n");
					}
					break;

					case AL_TAMPER_ALARM:
					{
						uint16_t temp = AlarmData[8] << 8 |  AlarmData[9];
						
						if(temp == 0x0001)
						{
							printf("Keypad attempts exceed code entry limit \r\n");
						}
						else if(temp == 0x0002)
						{
							printf("Front escutcheon removed from main \r\n");
						}
						else if(temp == 0x0003)
						{
							printf("Intrusion alarm \r\n");
						}
						else if(temp == 0x0004)
						{
							printf("High temperature alarm \r\n");
						}
					}
					break;
#endif 					
					
					case AL_MANUAL_UNLOCKED:
					{
						uint16_t temp = AlarmData[8] << 8 |  AlarmData[9];
						if(temp == 0x0001)
						{
							printf("\033[1;33mOP\033[0m :: Unlock by key cylinder or inside thumb-turn  \r\n");
						}
						else if(temp == 0x0002)
						{
							printf("\033[1;33mOP\033[0m :: Unlock by Inside Button \r\n");
						}
					}
					break;
					
					case AL_RF_OP_UNLOCKED:
					{
						printf("\033[1;33mOP\033[0m :: Unlock by RF module \r\n");
					}					
					break;
					
					case AL_KEYPAD_UNLOCKED:
					{
						uint16_t temp = AlarmData[8] << 8 |  AlarmData[9];

						if(temp == RET_MASTER_CODE_MATCH)
							printf("\033[1;33mOP\033[0m :: Unlock by Master Code \r\n");
						else if(temp == RET_TEMP_CODE_MATCH)
							printf("\033[1;33mOP\033[0m :: Unlock by One Time Code \r\n");
						else
							printf("\033[1;33mOP\033[0m :: Unlock by Pin code user %03d \r\n",temp);
					}
					break;

#if 0
					case AL_MANUAL_LOCKED:
					{
						uint16_t temp = AlarmData[8] << 8 |  AlarmData[9];
						
						if(temp == 0x0001)
						{
							printf("CL :: Lock by key cylinder or inside thumb-turn \r\n");
						}
						else if(temp == 0x0002)
						{
							printf("CL :: Lock by one touch function (lock and leave) \r\n");
						}
						else if(temp == 0x0003)
						{
							printf("CL :: Lock By inside button \r\n");
						}
					}
					break;					
					
					case AL_KEYPAD_LOCKED:
					{
						uint16_t temp = AlarmData[8] << 4 |  AlarmData[9];
						
						printf("CL :: Lock by Pin user %03d \r\n",temp);
					}					
					break; 
					
					case AL_DEADBOLT_JAMMED:
					{
						uint16_t temp = AlarmData[8] << 4 |  AlarmData[9];
						
						if(temp == 0x0000)
						{
							printf("Deadbolt motor jammed Locking \r\n");
						}
						else if(temp == 0x0001)
						{
							printf("Deadbolt motor jammed Unlocking \r\n");
						}
					}					
					break; 					
					
					case AL_AUTO_LOCK_OP_LOCKED:
					{
						printf("CL :: Auto re-lock cycle complete, locked \r\n");
					}					
					break;
					
					case AL_OUT_OF_SCHEDULE:
					{
						printf("AL_OUT_OF_SCHEDULE \r\n");
					}					
					break;					
					
					case AL_FORCED_LOCKED:
					{
						printf("CL :: Inner Forced Lock Enabled \r\n");
					}					
					break;

					case AL_UNLOCK_FAIL_BY_FORCED_LOCKED:
					{
						uint16_t temp = AlarmData[8] << 4 |  AlarmData[9];
						
						if(temp == 0x0001)
						{
							printf("Locking fail because inner forced lock is enabled \r\n");
						}
						else if(temp == 0x0002)
						{
							printf("Unlocking fail because inner forced lock is enabled \r\n");
						}
					}					
					break;
					
					case AL_DOOR_OPEN_CLOSE_STATE:
					{
						uint16_t temp = AlarmData[8] << 4 |  AlarmData[9];
						
						if(temp == 0x0000)
						{
							printf("Door is opened \r\n");
						}
						else if(temp == 0x0001)
						{
							printf("Door is closed \r\n");
						}
					}	
					break;

					case AL_LOW_BATTERY:
					{
						printf("Battery Low \r\n");
					}	
					break;
#endif 
					default :
					break;
				}	
			}
			else if(AlarmData[6] == EV_USER_CREDENTIAL_UNLOCK)
			{
				switch(AlarmData[7])
				{
					/* Credentil report */
					case CREDENTIALTYPE_CARD:
					{
						printf("\033[1;33mOP\033[0m :: Unlock by Card user %03d \r\n",AlarmData[9]);
					}	
					break;

					case CREDENTIALTYPE_FINGERPRINT:
					{
						printf("\033[1;33mOP\033[0m :: Unlock by FingerPrint user %03d \r\n",AlarmData[9]);
					}	
					break;	

					default :
					break;
				}
			}
			else if(AlarmData[6] == EV_USER_CARD_ADDED)
			{
				switch(AlarmData[7])
				{
					/* Credentil report */
					case CREDENTIALTYPE_CARD:
					{
						if(AlarmData[9] == 0xFF)
							printf("RC :: Card added All \r\n");
						else
							printf("RC :: Card added %03d \r\n",AlarmData[9]);
					}	
					break;

					case CREDENTIALTYPE_FINGERPRINT:
					{
						if(AlarmData[9] == 0xFF)
							printf("RF :: Finger added All \r\n");
						else
							printf("RF :: Finger added %03d \r\n",AlarmData[9]);
					}	
					break;	 

					default :
					break;
				}
			}
			else if(AlarmData[6] == EV_USER_CARD_DELETED)
			{
					switch(AlarmData[7])
					{
						/* Credentil report */
						case CREDENTIALTYPE_CARD:
						{
							if(AlarmData[9] == 0xFF)
								printf("DC :: Card deleted All \r\n");
							else
								printf("DC :: Card deleted %03d \r\n",AlarmData[9]);
						}	
						break;

						case CREDENTIALTYPE_FINGERPRINT:
						{
							if(AlarmData[9] == 0xFF)
								printf("DF :: Finger deleted All \r\n");
							else
								printf("DF :: Finger deleted %03d \r\n",AlarmData[9]);
						}	
						break;	

						default :
						break;
					}
			}
			else if(AlarmData[6] == F0_USER_ADDED)
			{
				if(AlarmData[9] == RET_MASTER_CODE_MATCH )
					printf("RP :: MasterCode added  \r\n");
				else if(AlarmData[9] == RET_TEMP_CODE_MATCH )
					printf("RP :: OneTimeCode added  \r\n");
				else 
					printf("RP :: Pincode added %03d \r\n",AlarmData[9]);
			}
			else if(AlarmData[6] == F0_USER_DELETED)
			{
				if(AlarmData[9] == RET_MASTER_CODE_MATCH )
					printf("DP :: MasterCode deleted  \r\n");
				else if(AlarmData[9] == RET_TEMP_CODE_MATCH )
					printf("DP :: OneTimeCode deleted  \r\n");
				else 
					printf("DP :: Pincode deleted %03d \r\n",AlarmData[9]);
			}
			else if(AlarmData[6] == 0xFF)
			{
				switch(AlarmData[9])
				{
					case 0xA5:
					{
						printf("ST :: Old Time \r\n");	
					}
					break;

					case 0x00:
					{
						printf("ST :: New Time \r\n");
					}
					break;

					case 0xB6:
					{
						printf("ST :: RTC reset \r\n");
					}
					break;

					case 0xB5:
					{
						printf("ST :: Time lost (No battery) \r\n");
					}
					break;

					default :
					break;
				}
			}

			tempindex--;
			if(tempindex == 0x0000)
			{
				tempindex = MAX_LOG_INDEX;
			}
		}
//		printf("\r\n\r\n");
		printf("\r\nLog end \r\n");
		initRxdata();
		gbModePrcsStep = 0;			
	}
	else if(memcmp(Logging.loggingRxDataBuf,DelAll,Logging.loggingRxDataCnt) == 0)
	{
		printf("\r\n Delete ALL Log data \r\n");
		printf("\r\n");
	
		DeleAlarmReportLog();
		initRxdata();
		gbModePrcsStep = 0;			
	}
	else if(memcmp(Logging.loggingRxDataBuf,GetTime,Logging.loggingRxDataCnt) == 0)
	{
		printf("\r\nGet Time \r\n\r\n");
#ifdef RTC_PCF85063 
		EX_RTCTimeDef clock;
		GetEXRTCTime(&clock.Timebuffer[0]);
		printf("%02x.%02x.%02x \r\n",clock.time.Years+0x2000,clock.time.Mounth,clock.time.Days);
		printf("%02x:%02x:%02x \r\n",clock.time.Hours,clock.time.Minutes,clock.time.Seconds);
#else 
		RTC_TimeTypeDef time;
		RTC_DateTypeDef date;

		GetRTCTime(&time,&date);
		printf("%02d.%02d.%02d \r\n",date.Year+2000,date.Month,date.Date);
		printf("%02d:%02d:%02d \r\n",time.Hours,time.Minutes,time.Seconds);
#endif 		
		
		initRxdata();
		gbModePrcsStep = 0;
	}
	else if(memcmp(Logging.loggingRxDataBuf,SetTime,4) == 0)
	{
		if(Logging.loggingRxDataCnt != 19)
		{
			printf("\r\nInvaild Command \r\n");
			initRxdata();	
			gbModePrcsStep = 0;
			return;
		}

#ifdef RTC_PCF85063 
		EX_RTCTimeDef clock;

		clock.time.Seconds = ((Logging.loggingRxDataBuf[17] & 0x0F) << 4 | (Logging.loggingRxDataBuf[18] & 0x0F));
		clock.time.Minutes = ((Logging.loggingRxDataBuf[15] & 0x0F) << 4 | (Logging.loggingRxDataBuf[16] & 0x0F));
		clock.time.Hours = ((Logging.loggingRxDataBuf[13] & 0x0F) << 4 | (Logging.loggingRxDataBuf[14] & 0x0F));
		clock.time.Days = ((Logging.loggingRxDataBuf[11] & 0x0F) << 4 | (Logging.loggingRxDataBuf[12] & 0x0F));
		clock.time.WeekDays = 0x07; 	
		clock.time.Mounth = ((Logging.loggingRxDataBuf[9] & 0x0F) << 4 | (Logging.loggingRxDataBuf[10] & 0x0F));
		clock.time.Years = ((Logging.loggingRxDataBuf[7] & 0x0F) << 4 | (Logging.loggingRxDataBuf[8] & 0x0F));; 

		SetEXRTCTime(&clock.Timebuffer[0]);
#else 
		SetRTCTime(&Logging.loggingRxDataBuf[7]);
#endif 

		printf("\r\nSet Time complete \r\n\r\n");
		
		initRxdata();
		gbModePrcsStep = 0;
	}
	else if(memcmp(Logging.loggingRxDataBuf,Version,Logging.loggingRxDataCnt) == 0)
	{
		printf("\r\n");
		//printf("Build %s %s \r\n",__DATE__,__TIME__);
		printf("Product TYPE ID 1 : 0x%02X , ID 2 : 0x%02X \r\n",_PTODUCT_TYPE_ID1,_PTODUCT_TYPE_ID2); 		
		printf("Product ID Major Code : 0x%02X , Minor  Code : 0x%02X \r\n",DDL_SET_CODE_MAJOR,DDL_SET_CODE_MINOR); 
		printf("Version : v%d.%d %02d \r\n",PROGRAM_VERSION_MAJOR/10,PROGRAM_VERSION_MAJOR%10,PROGRAM_VERSION_MINOR);
		initRxdata();
		gbModePrcsStep = 0;
	}	
	else if(memcmp(Logging.loggingRxDataBuf,Modeexit,Logging.loggingRxDataCnt) == 0)
	{
		printf("\r\nPower On Reset start Bye! \r\n");
		initRxdata();		
		Logging.logModeOn = 0;
		HAL_NVIC_SystemReset();
	}
	else
	{
		if(Logging.loggingRxDataCnt)
		{
			printf("\r\nInvaild Command \r\n");
			initRxdata();	
			gbModePrcsStep = 0;
		}
	}
}

void Logging_uart_rxcplt_callback(UART_HandleTypeDef *huart)
{
	LoggingRxDataProcess();	
	CommPackRxInterruptRequest();
}

void FeedbackTestMode(void)
{
	AudioFeedback(VOICE_MIDI_INTO, gcbBuzSta, VOL_CHECK);
	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedGenerate((LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_5|LED_KEY_8), DIMM_ON_SLOW, 0);
#endif
	
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}

void ModeMenuTimeSet(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			gPinInputKeyCnt =0x00;
			FeedbackKeyPadLedOnBuzOnly(VOICE_MIDI_INTO, VOL_CHECK|MANNER_CHK); 
			SetModeTimeOut(_MODE_TIME_OUT_60S);
			gbModePrcsStep = 1;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);
					Logging.loggingRxDataBuf[5+gPinInputKeyCnt] = gbInputKeyValue;
					gPinInputKeyCnt++;
					SetModeTimeOut(_MODE_TIME_OUT_60S);
					break;
			
				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
			
				case TENKEY_SHARP:
					FeedbackKeyPadLedOnBuzOnly(VOICE_MIDI_INTO, VOL_CHECK|MANNER_CHK); 
					gPinInputKeyCnt = 0;
					gbModePrcsStep = 2;
				break;

				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}
			break;

		case 2:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	

					if(Logging.loggingRxDataBuf[5+gPinInputKeyCnt] != gbInputKeyValue)
					{
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						ModeClear();
						break;
					}
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);					
					gPinInputKeyCnt++;
					SetModeTimeOut(_MODE_TIME_OUT_60S);
					break;
			
				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
			
				case TENKEY_SHARP:
				{
					if(gPinInputKeyCnt != 14) //YYYYMMDDHHMMSS
					{
						gbModePrcsStep = 0;
						break;
					}

#ifdef RTC_PCF85063 
					EX_RTCTimeDef clock;
			
					clock.time.Seconds = ((Logging.loggingRxDataBuf[17] & 0x0F) << 4 | (Logging.loggingRxDataBuf[18] & 0x0F));
					clock.time.Minutes = ((Logging.loggingRxDataBuf[15] & 0x0F) << 4 | (Logging.loggingRxDataBuf[16] & 0x0F));
					clock.time.Hours = ((Logging.loggingRxDataBuf[13] & 0x0F) << 4 | (Logging.loggingRxDataBuf[14] & 0x0F));
					clock.time.Days = ((Logging.loggingRxDataBuf[11] & 0x0F) << 4 | (Logging.loggingRxDataBuf[12] & 0x0F));
					clock.time.WeekDays = 0xFF; 	
					clock.time.Mounth = ((Logging.loggingRxDataBuf[9] & 0x0F) << 4 | (Logging.loggingRxDataBuf[10] & 0x0F));
					clock.time.Years = ((Logging.loggingRxDataBuf[7] & 0x0F) << 4 | (Logging.loggingRxDataBuf[8] & 0x0F));; 
			
					SetEXRTCTime(&clock.Timebuffer[0]);
#else 
					SetRTCTime(&Logging.loggingRxDataBuf[7]);
#endif 
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					ModeClear();
				}
				break;

				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}
			break;
		break;			

		default:
			ModeClear();
			break;
	}
}

void ModeLoggingProcess(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			Logging.loggingUart = gh_mcu_uart_com;
			Logging.logModeOn = 1;
			
			FeedbackTestMode();

			SetupUartforPrintf(Logging.loggingUart);
			InitPrintfService(NULL,Logging_uart_rxcplt_callback,initRxdata);
			printf("\r\n");
			printf("* iRevo Smart Door Lock Terminal Menu * \r\n");
			printf("* Please Input below item * \r\n");
			printf("\r\n");
			printf("- 1 : Get Log \r\n");
			printf("- 2 : Delete All Log \r\n");
			printf("- 3 : Get Time \r\n");
			printf("- time : Set Time \r\n");
			printf("-   ex) YYYY.MM.DD HH:MM:SS 24 hour format \r\n");
			printf("-   ex) 2019.11.19 14:40:21 -> \"time 20191119144021\" \r\n");
			printf("- .... ....\r\n");
			printf("- .... ....\r\n");
			printf("- .... ....\r\n");			
			printf("- x : Exit Mode and Soft Reset\r\n");
			printf("\r\n");
			printf("\r\n");
			printf("- Enter : ");			
			gbModePrcsStep++;			
		break;

		case 1: 		
			MenuLoggingPrintProcess();
		break;	

		default:
			ModeClear();
			break;
	}

}

void SaveLog(uint8_t* recordData,uint8_t size)
{
	uint8_t AlarmData[LOG_DATA_LENGTH] = {0x00,};
	bool write = false;

#ifdef STD_GA_374_2019
	if(gfEepromErr ||gfRtcErr)
	{
		return;
	}
#endif 

	switch(recordData[0])
	{
		/* Alarm report */
		case F0_ALARM_REPORT: 
		case F0_ALARM_REPORT_EX:
		{
			switch(recordData[1])
			{
				case AL_CODE_CHANGED_ADDED: 
				case AL_MANUAL_UNLOCKED:
				case AL_RF_OP_UNLOCKED:
				case AL_KEYPAD_UNLOCKED:
//				case AL_CODE_DELETED:
//				case AL_TAMPER_ALARM:
//				case AL_MANUAL_LOCKED:
//				case AL_KEYPAD_LOCKED:
//				case AL_DEADBOLT_JAMMED:
//				case AL_AUTO_LOCK_OP_LOCKED:
//				case AL_OUT_OF_SCHEDULE:
//				case AL_FORCED_LOCKED:
//				case AL_UNLOCK_FAIL_BY_FORCED_LOCKED:
//				case AL_DOOR_OPEN_CLOSE_STATE:
//				case AL_LOW_BATTERY:
				{
					write = true;
				}
				break;

				default :
				break;
			}
		}
		break;

		case EV_USER_CREDENTIAL_UNLOCK:
		case EV_USER_CARD_ADDED:
		case EV_USER_CARD_DELETED:
		case F0_USER_ADDED:
		case F0_USER_DELETED:
		case 0xFF: // Set Time
//		case EV_USER_CREDENTIAL_LOCK:
//		case EV_ARM_REQEST:
		{
			write = true;
		}
		break;

		default :
		break;
	}

	if(write)
	{
		RomRead(AlarmData,LOG_INDEX,2);

		if(Logging.index >= MAX_LOG_INDEX)
		{
			Logging.index = 0;
		}
		else
		{
			Logging.index = AlarmData[0] << 8 | AlarmData[1];
		}

#ifdef RTC_PCF85063 
		EX_RTCTimeDef clock;
		GetEXRTCTime(&clock.Timebuffer[0]);

		AlarmData[0] = clock.time.Years;
		AlarmData[1] = clock.time.Mounth;
		AlarmData[2] = clock.time.Days;
		AlarmData[3] = clock.time.Hours;
		AlarmData[4] = clock.time.Minutes;
		AlarmData[5] = clock.time.Seconds;
#else 
		RTC_TimeTypeDef time;
		RTC_DateTypeDef Data;

		HAL_RTC_GetTime(gh_mcu_rtc,&time,RTC_FORMAT_BCD);
		HAL_RTC_GetDate(gh_mcu_rtc,&Data,RTC_FORMAT_BCD);

		AlarmData[0] = Data.Year;
		AlarmData[1] = Data.Month;
		AlarmData[2] = Data.Date;
		AlarmData[3] = time.Hours;
		AlarmData[4] = time.Minutes;
		AlarmData[5] = time.Seconds;
#endif 

		memcpy(&AlarmData[6],recordData,size);
		Logging.index++;
		RomWrite(AlarmData,LOG_DATA_START+(LOG_DATA_LENGTH*(Logging.index-1)),LOG_DATA_LENGTH);
		AlarmData[0] = (uint8_t)(Logging.index >> 8);
		AlarmData[1] = (uint8_t)(Logging.index & 0x00FF);
		RomWrite(AlarmData,LOG_INDEX,2);
	}
}
#endif 


void ModeTestVoice(void)
{
/*
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackModeEntering(VOICE_MIDI_INTO, VOL_CHECK);
			gbTenKeyCnt = 0;
			gbModeTimeOutTimer100ms = 50;
			gbModePrcsStep++;
			gfVoiceSet = 1;
			break;

		case 1:
			VoiceICTestProcess();
			break;
	
		default:
			ModeClear();
			break;
	}
*/
}
