#define		_DDL_MAIN_C_

#include "Main.h"





FLAG MainSysFlag;				///< Main System Flag
FLAG MainSysErrFlag;
FLAG MainSysTempFlag;
FLAG DDLStateFlag;

BYTE gtbNansu[8];
BYTE gtbNansuResetCnt = 0;

BYTE gbTestBuf[10];
BYTE gbTestTimer2ms = 0;

BYTE	gbtypevalue = 0;
BYTE	gbVoicevalue = 0;

LOCKSTATUS AbnormalStatus;
static WORD gwAbnormalSleepCheckTimer1s = 0x00000000;
BYTE	gbSilentBoot = 0;

void SetLockStatusInfo(BYTE index , BYTE Data)
{
	AbnormalStatus.gbLockInfo[index] = Data;
}

void SetAbnormalSleepCheckTimer(WORD Data)
{
	gwAbnormalSleepCheckTimer1s = Data;
}

void Silent_Reset_Check(void)
{
	BYTE Temp = 0x00;
	RomRead(&Temp,LOCK_MODE_STATUS+SILENT_RESET_FLAG,1);

	if(Temp == 0xAB || Temp == 0xCA)
	{
		if(Temp == 0xCA)
		{
			AlarmGotoAbnormalAlarm();
		}
		Temp = 0x00;
		RomWrite(&Temp ,LOCK_MODE_STATUS+SILENT_RESET_FLAG,1);
		gbSilentBoot = 0xAB;
	}
}

WORD GetAbnormalSleepCheckTimer(void)
{
	return gwAbnormalSleepCheckTimer1s;
}

void AbnormalSleepCheckCounter(void)
{
	if(gwAbnormalSleepCheckTimer1s)
	{
		gwAbnormalSleepCheckTimer1s--;
		if(gwAbnormalSleepCheckTimer1s == 0x00)
		{
			BYTE Temp = 0x00;

#if defined (FACOTRY_PINCODE_TEST_MODE)
			if(GetMainMode() == MODE_PINCODE_TEST_MODE) return;
#endif 
			if(GetMainMode() == MODE_TEST_AGING) return;
			if(GetMainMode() == MODE_PACK_SET_LED_PROCESS) return;
#if defined (_USE_LOGGING_MODE_)
			if(GetMainMode() == MODE_LOGGING_PROCESS) return;
#endif 			
			if(JigModeStatusCheck() == STATUS_SUCCESS) return;
			if(GetTamperProofPrcsStep()) return;
			if(AlarmStatusCheck() == STATUS_SUCCESS) return;
			

			SetLockStatusInfo(LOCK_MAIN_MODE,GetMainMode());
			SetLockStatusInfo(LOCK_MAIN_STEP_MODE,gbModePrcsStep);

#if defined (DDL_CFG_RFID)
			SetLockStatusInfo(LOCK_CARD_MODE,GetCardMode());
			SetLockStatusInfo(LOCK_CARD_STEP_MODE,gbModePrcsStep);
#endif 			

#if defined (_MASTERKEY_IBUTTON_SUPPORT) || defined (DDL_CFG_DS1972_IBUTTON)
			SetLockStatusInfo(LOCK_CARD_MODE,GetDS1972ModeProcess());
			SetLockStatusInfo(LOCK_CARD_STEP_MODE,gbiButtonPrcsStep);
#endif 			

#if defined (DDL_CFG_FP_HFPM)
			SetLockStatusInfo(LOCK_CARD_MODE,GetHFPMRxDataStep());
			SetLockStatusInfo(LOCK_CARD_STEP_MODE,0x00);
#endif 			

#if defined (P_COM_DDL_EN_T)	
			SetLockStatusInfo(LCOK_UART1_CONNECTION_MODE,gPackConnectionMode);
			SetLockStatusInfo(LCOK_UART1_TX_PROCESS_STOP,GetPackTxProcessStep());
			SetLockStatusInfo(LCOK_UART1_RX_PROCESS_STOP,GetPackRxProcessStep());
#endif 			

#if defined (P_BT_WAKEUP_T)
			SetLockStatusInfo(LCOK_UART2_CONNECTION_MODE,gInnerPackConnectionMode);
			SetLockStatusInfo(LCOK_UART2_TX_PROCESS_STOP,GetInnerPackTxProcessStep());
			SetLockStatusInfo(LCOK_UART2_RX_PROCESS_STOP,GetInnerPackRxProcessStep());
#endif 			


			SetLockStatusInfo(LOCK_MAIN_SYSTEM_ERROR_FLAG,gbMainSysErrData);
			SetLockStatusInfo(LOCK_MAIN_DDL_STATEFLAG_FLAG,gbDDLStateData);


			RomRead(&Temp,LOCK_MODE_STATUS+SILENT_RESET_COUNT,1);
			SetLockStatusInfo(SILENT_RESET_COUNT,++Temp);
			SetLockStatusInfo(SILENT_RESET_FLAG,0xAB);
			
			RomWrite(AbnormalStatus.gbLockInfo,LOCK_MODE_STATUS,24);
			HAL_NVIC_SystemReset();
		}
	}
}

void NansuGenerate(void)
{
	BYTE bSeed;
	
	gtbNansuResetCnt++;
	if((gtbNansuResetCnt&0x0F) == 0)
	{
#if 0
		bSeed = gtbNansu[1] & 0x07;
#else
		//TIM4 (100kHz)에서 seed 값을 읽어 옴.
		bSeed = gh_mcu_tim_usec->Instance->CNT & 0x07;
#endif
		gtbNansu[0] += gtbNansu[bSeed];
		bSeed = (bSeed + 1) & 0x07;
		gtbNansu[1] += gtbNansu[bSeed];
		bSeed = (bSeed + 1) & 0x07;
		gtbNansu[2] += gtbNansu[bSeed];
		bSeed = (bSeed + 1) & 0x07;
		gtbNansu[3] += gtbNansu[bSeed];
		bSeed = (bSeed + 1) & 0x07;
		gtbNansu[4] += gtbNansu[bSeed];
		bSeed = (bSeed + 1) & 0x07;
		gtbNansu[5] += gtbNansu[bSeed];
		bSeed = (bSeed + 1) & 0x07;
		gtbNansu[6] += gtbNansu[bSeed];
		bSeed = (bSeed + 1) & 0x07;
		gtbNansu[7] += gtbNansu[bSeed];
	}
	else
	{
		gtbNansu[0] += 181;
		gtbNansu[1] += 17;
		gtbNansu[2] += 233;
		gtbNansu[3] += 89;
		gtbNansu[4] += 31;
		gtbNansu[5] += 211;
		gtbNansu[6] += 53;
		gtbNansu[7] += 109;
	}
}


void DDLStatusFlagSet(BYTE bFlagData)
{
	BYTE bTmp;

	bTmp = gbDDLStateData & bFlagData;
	if(bTmp != bFlagData)
	{
		gbDDLStateData |= bFlagData;
		RomWrite(&gbDDLStateData, (WORD)DDL_STATE, 1);
	}
}


void DDLStatusFlagClear(BYTE bFlagData)
{
	BYTE bTmp;
	
	bTmp = gbDDLStateData & bFlagData;
	if(bTmp != 0)
	{
		gbDDLStateData &= ~bFlagData;
		RomWrite(&gbDDLStateData, (WORD)DDL_STATE, 1);
	}
}




#ifdef	DDL_CFG_DIMMER
void		Init_Dimming_Var( void )
{
	gl_dimming_i2c.type = DDL_I2C_GPIO;
	gl_dimming_i2c.slave_id = I2C_DIMMING_ADDRESS;
	gl_dimming_i2c.dev.gpio.SCL_GPIOx		= DIM_SCL_GPIO_Port;		//mxconstants.h에 선언되어 있음 
	gl_dimming_i2c.dev.gpio.SCL_GPIO_Pin	= DIM_SCL_Pin;
	gl_dimming_i2c.dev.gpio.SDA_GPIOx		= DIM_SDA_GPIO_Port;
	gl_dimming_i2c.dev.gpio.SDA_GPIO_Pin	= DIM_SDA_Pin;

#ifdef P_LED_RESET
	P_LED_RESET(1);
	HAL_Delay( 1 );
	P_LED_RESET(0);	
	HAL_Delay( 1 );
	P_LED_RESET(1);
#endif
}
#endif


#ifdef	DDL_CFG_TOUCHKEY
void		Init_TouchKey_Var( void )
{
	gl_tkey_i2c.type = DDL_I2C_GPIO;
	gl_tkey_i2c.slave_id = I2C_TOUCHKEY_ADDRESS;
	gl_tkey_i2c.dev.gpio.SCL_GPIOx		= TKEY_SCL_GPIO_Port;		//mxconstants.h에 선언되어 있음 
	gl_tkey_i2c.dev.gpio.SCL_GPIO_Pin	= TKEY_SCL_Pin;
	gl_tkey_i2c.dev.gpio.SDA_GPIOx		= TKEY_SDA_GPIO_Port;
	gl_tkey_i2c.dev.gpio.SDA_GPIO_Pin	= TKEY_SDA_Pin;
}
#endif

#ifdef	DDL_CFG_IOEXPANDER
void		Init_ExpanderOutput_Var( void )
{
	gl_ioexpanderoutput_i2c.type = DDL_I2C_GPIO;
	gl_ioexpanderoutput_i2c.slave_id = I2C_IOEXPANDEROUTPUT_ADDRESS;
	gl_ioexpanderoutput_i2c.dev.gpio.SCL_GPIOx		= I2C_SCL_IOEX_GPIO_Port;		//mxconstants.h에 선언되어 있음 
	gl_ioexpanderoutput_i2c.dev.gpio.SCL_GPIO_Pin	= I2C_SCL_IOEX_Pin;
	gl_ioexpanderoutput_i2c.dev.gpio.SDA_GPIOx		= I2C_SDA_IOEX_GPIO_Port;
	gl_ioexpanderoutput_i2c.dev.gpio.SDA_GPIO_Pin	= I2C_SDA_IOEX_Pin;
}
void		Init_ExpanderInput_Var( void )
{
	gl_ioexpanderinput_i2c.type = DDL_I2C_GPIO;
	gl_ioexpanderinput_i2c.slave_id = I2C_IOEXPANDERINPUT_ADDRESS;
	gl_ioexpanderinput_i2c.dev.gpio.SCL_GPIOx	= I2C_SCL_IOEX_GPIO_Port;		//mxconstants.h에 선언되어 있음 
	gl_ioexpanderinput_i2c.dev.gpio.SCL_GPIO_Pin	= I2C_SCL_IOEX_Pin;
	gl_ioexpanderinput_i2c.dev.gpio.SDA_GPIOx	= I2C_SDA_IOEX_GPIO_Port;
	gl_ioexpanderinput_i2c.dev.gpio.SDA_GPIO_Pin	= I2C_SDA_IOEX_Pin;
}
#endif


///////////////////////////////////////////////////////////////////////////////





void LockSetValueLoad(void)
{	
	BYTE bTmp;

	RomRead(&gbDDLStateData, DDL_STATE, 1);
	if(gfDDLStateRst)
	{
		gbDDLStateData = 0;
	}
	else
	{
#ifdef P_KEY_INTER_CL_T
		if(gfIntrusionAlarm || gfBrokenAlarm || gfKeyInterClAlarm)
#else 
		if(gfIntrusionAlarm || gfBrokenAlarm)
#endif 			
		{
			bTmp = KeyInputCheck();
			if(bTmp)
			{
				AlarmGotoAlarmClearNoFeedback();
			}
			else
			{
				if(gfIntrusionAlarm)
				{
					AlarmGotoIntrusionAlarm();
				}
#ifdef P_KEY_INTER_CL_T				
				else if(gfKeyInterClAlarm)
				{
					AlarmGotoKeyInterCylinderAlarm();
				}
#endif 				
				else
				{
					AlarmGotoBrokenAlarm();
				}
			}
		}
		else 	if(gfOutLock && (MotorSensorCheck() & SENSOR_OPEN_STATE == SENSOR_OPEN_STATE)) //외부 강제 잠금 상태에서 모터가 열린상태로 부팅시 경보 
		{
			AlarmGotoIntrusionAlarm();
		}
	}
}



//전역 변수들 초기화
void	InitializeDDL_Var( void )
{
#ifdef	DDL_CFG_TOUCHKEY
	Init_TouchKey_Var();
	RegisterTouchKey( &gl_tkey_i2c );
#endif

#ifdef	DDL_CFG_DIMMER
	Init_Dimming_Var();
#endif

#ifdef	DDL_CFG_IOEXPANDER
	Init_ExpanderOutput_Var();
	RegisterExpanderOutput( &gl_ioexpanderoutput_i2c );
	Init_ExpanderInput_Var();
	RegisterExpanderInput( &gl_ioexpanderinput_i2c );
#endif

}


void	InitializeDDL_HW( void )
{
	//TIM4 동작 시킴. 10usec 분해능의 타이머. 이 타이머를 사용하는 code이 많아서 제일 먼저 기동시킨다.
	HAL_TIM_Base_Start( gh_mcu_tim_usec );

	SystemInitial();			// Check HW option resisters & set global variables

	RomInitial();

#ifdef RTC_PCF85063
	RTCInit();
#endif

	VoiceICInitial();

#ifdef	DDL_CFG_RFID
#ifdef	DDL_CFG_RFID_TRF7970A	//2020년06월22일 SJC : MFRC523을 사용하면 RfidInitial()을 호출할 필요가 없기 때문에 호출 조건에 DDL_CFG_RFID_TRF7970A을 추가함
	RfidInitial();
#endif	
#endif

#ifdef	DDL_CFG_DS1972_IBUTTON //hyojoon_20160907 I-button
	iButtonDisable();
#endif 

#ifdef	_MASTERKEY_IBUTTON_SUPPORT
	MasterKeyDisable();
#endif 
	BatteryCheck();

#if 0
//양산용 지그로 EEPROM에 모델설정 코드를 넣기 전까지 시험용
{

#if		defined(	__DDL_MODEL_Z10_IH)
	__change_hwmodel( _DDL_HWMODEL_Z10_IH );		// for Test
#elif	defined(__DDL_MODEL_WF20_DMP)
	__change_hwmodel( _DDL_HWMODEL_WF20_DMP );	// for Test
#elif	defined(__DDL_MODEL_A20_IH)
	__change_hwmodel( _DDL_HWMODEL_A20_IH );	// for Test
#elif	defined(__DDL_MODEL_Rose2_IH)
	__change_hwmodel( _DDL_HWMODEL_Rose2_IH );	// for Test
#elif	defined(__DDL_MODEL_J20_IH)
	__change_hwmodel( _DDL_HWMODEL_J20_IH);	// for Test
#elif	defined(__DDL_MODEL_WV40_DMP)
	__change_hwmodel( _DDL_HWMODEL_WV40_DMP);	// for Test
#elif	defined(__DDL_MODEL_WE30_DMP)
	__change_hwmodel( _DDL_HWMODEL_WE30_DMP);	// for Test
#elif	defined(__DDL_MODEL_EDGE2_DMP)
	__change_hwmodel( _DDL_HWMODEL_EDGE2_DMP ); // for Test
#elif	defined(__DDL_MODEL_WV_200)
	__change_hwmodel( _DDL_HWMODEL_WV_200);		// for Test
#elif	defined(__DDL_MODEL_E300_FH)
	__change_hwmodel( _DDL_HWMODEL_E300_FH); 	// for Test
#elif	defined(__DDL_MODEL_YDG413) //hyojoon_20160802 add 
	__change_hwmodel( _DDL_HWMODEL_YDG413); 	// for Test
#endif


}


	InitialHWmodel();		//HW model 정보를 EEPROM에서 읽어 그에 따른 초기화를 한다.
#endif

#ifdef	DDL_CFG_TOUCHKEY
	TouchKeyInitProc( 3 );
#endif

	if(gbtypevalue == DDL_SET_BIO || gbtypevalue == DDL_SET_RF_BIO)
	{
//		BioCommCheck(0);
//		BioModuleOff();			
	}

#ifdef DDL_CFG_IOEXPANDER	
	IoExpanderOutPutInit();	
	IoExpanderInPutInit();
#endif	

}


void	InitializeDDL_SW( void )
{
#ifdef	CYLINDER_KEY_ALARM//eeprom에 저장된 실린더 알람  값을 넣기 위함.
	BYTE bTmp = 0xff;
#endif
#ifdef DDL_LED_DISPLAY_SELECT //led display 결정 
	BYTE bTmp = 0xff;
#endif


	SystickVariableInit();

#ifdef	DDL_CFG_DIMMER
	SetupDimmingLed( &gl_dimming_i2c );
#endif

#ifdef	DDL_CFG_NO_DIMMER
	SetupLed();
#endif

#ifdef	DDL_CFG_FP    		//hyojoon_20160831 PFM-3000 add 
	InitialSetupFingerModule();
#endif

	IntialKeySwtichSet();

#if defined (DDL_CFG_MS)
	MS_Get_Credential_Info();
#endif 

//	InitialValueSet();
	Init_Motor();
	#ifdef	USED_SECOND_MOTOR	
	Init_Motor_Second();
	gbAutoChangepassagemodevalue = AutoChangePassageModeLoad();
	if(gbAutoChangepassagemodevalue != AUTOCHANGEPASSMODESTEP_DONE)
	{
		gbAutoChangepassagemodevalue = 0;
	}
	#endif

	FactoryResetDoneCheck();
	FactoryResetCheck();

	LockWorkingModeLoad();

	LanguageLoad();

	TamperCountLoad();

	LockSetValueLoad();
	VolumeSettingLoad();

#ifdef	CYLINDER_KEY_ALARM	//저장된 실린더 알람 값 로드 
	RomRead(&bTmp, (WORD)CYLINDER_KEY_ALARM_SET, 1);
	if(bTmp != 0x00 &&  bTmp != 0x01)
	{	
		bTmp = 0x00;
		RomWrite(&bTmp, (WORD)CYLINDER_KEY_ALARM_SET, 1);
	}
#endif	

#ifdef	DDL_CFG_TOUCH_OC
	SafeOCButtonLoad();
#endif

	BOR_Config();

#ifdef	BLE_N_SUPPORT
	LoadAllLockOutStatus();
#endif

#ifdef	_MASTERKEY_IBUTTON_SUPPORT
	MasterKeyEnableCheck();
#endif

#ifdef	DDL_CFG_RFID
	CardOpenUidSupportedInfoLoad();
#endif

	AutoLockTimeAndAutoReLockTimeCalculationAndRead();

#ifdef	DDL_AAAU_AUTOLOCK_HOLD 	//호주 및 뉴질랜드 전용 autolock hold 기능

	AutolockHoldSettingLoad();
	if(gbAutolockHold != AUTOLOCKHOLD && gbAutolockHold != AUTOLOCKUNHOLD)	
	{
		AutolockHoldSetting(AUTOLOCKUNHOLD);
	}		
	
	RomRead(&gbAutolockTimeValue, (WORD)RELOCK_TIME, 1);
	if(gbAutolockTimeValue < 0x01 || gbAutolockTimeValue > 0xF0)
	{
		gbAutolockTimeValue = 3;
	}
#endif //DDL_AAAU_AUTORELOCK_UI //호주 및 뉴질랜드 AUTO LOCK UI	
/*	
	bTmp = SecureUsedSettingLoad();
	if(bTmp != 0x01 && bTmp != 0x02)
	{
		SecureUsedSetting(0x01);	//default Secure Mode Enable
	}
*/	
#ifdef DDL_LED_DISPLAY_SELECT //led display 결정 
	bTmp = LedDisplayEnableStateLoad();
	if(bTmp != 0xFF && bTmp != 0x00)
	{
		LedDisplayEnableStateSet(0xFF);	//default led display enable
	}
#endif //DDL_LED_DISPLAY_SELECT #endif

}

#ifdef DDL_CFG_FACTORY_DEFAULT_SETTING
void FactoryDefaultSetting(void)
{
	// normal mode USER_CODE 초기화 
	gPinInputKeyFromBeginBuf[0] = 0x12;
	gPinInputKeyFromBeginBuf[1] = 0x34;
	gPinInputKeyFromBeginBuf[2] = 0x56;	
	gPinInputKeyFromBeginBuf[3] = 0x78;
	gPinInputKeyFromBeginBuf[4] = 0x90;
	gPinInputKeyCnt = 10;
	
	UpdatePincodeToMemory(USER_CODE);
	SetUserStatus(1, USER_STATUS_OCC_ENABLED);
	SetScheduleStatus(1, SCHEDULE_STATUS_DISABLE);
	RomWriteWithSameData(0xFF, USER_SCHEDULE, 4);

	// AUTO RELOCK Setting 초기화 enable 
	DDLStatusFlagClear(DDL_STS_MANUALLOCK); 


	// 언어 초기화 중국어 
	// LanguageSetting(LANGUAGE_CHINESE);

	//OPEN UID 초기화 
#ifdef DDL_CFG_RFID	
#ifdef	IREVO_CARD_ONLY
	CardOpenUidSupportedInfoSave(0xFF);
#else
	CardOpenUidSupportedInfoSave(0x74);
#endif
#endif 
}
#endif 


BYTE SystemErrorCheck(void)
{
	// 에러 확인 우선 순위에 따라 순서 배열

	if(gfFrontBroken)
	{
		FeedbackSystemError(SYSTEM_ERR_FRONT_NO_CONNECT);
		return 1;
	}	

#if 0 // PC 생산지그 프로그램으로 대응 
	if(gfEepromErr)
	{
		FeedbackSystemError(SYSTEM_ERR_MEMORY_IC);
		return 1;
	}
#endif 	

	if(gfTouchErr)
	{
		FeedbackSystemError(SYSTEM_ERR_TOUCH_IC);
		return 1;
	}

	if(gfMotorSensorErr)
	{
		FeedbackSystemError(SYSTEM_ERR_MOTOR_SENSOR);
		return 1;
	}

#ifdef	DDL_CFG_FP
	if(gfBioErr)
	{
		FeedbackSystemError(SYSTEM_ERR_BIO);
		return 1;
	}

	if(gfBioRegnumErr) //hyojoon_20160902
	{
		FeedbackSystemError(SYSTEM_ERR_BIO_REGNUM);
		return 1;
	}
#endif

#ifdef	LOCKSET_HANDING_LOCK
	if(gfHandingLockErr)
	{
		FeedbackSystemError(SYSTEM_ERR_HANDING_LOCK);
		return 1;
	}	
#endif

	return 0;
}




void	StartDDL_SW( void )
{
#ifdef	DDL_CFG_WATCHDOG
	StartIwdg();
#endif

#ifdef	DDL_CFG_FP  //hyojoon_20160831 PFM-3000 add //hyojoon_20160902
	BioCommCheck(0);
	BioModuleOff();
#endif 

#ifdef	LOCKSET_HANDING_LOCK
	GetHandingLock();
#endif 

	Silent_Reset_Check();

	if(gbSilentBoot != 0xAB)
	{
		if(SystemErrorCheck())
		{
			;/* system error 이라고 하더라도 pack clear 및 handing lock 초기화 */
		}
		else 
		{
			FeedbackLedAllOnOff();
		}
	}
	
#if defined (P_COM_DDL_EN_T)	
	PackModuleModeClear();
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	PackSendRunApplicaionAck();
#endif 
#endif 

#if defined (P_BT_WAKEUP_T)
	InnerPackModuleModeClear();
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	InnerPackSendRunApplicaionAck();
#endif 
#endif 
}






uint32_t		dbg1, dbg2;

uint16_t	DDL_main( void )
{
	BasicTimer2ms();
	BuzzerProcess();
	VoiceDataProcess();
	LedProcess();

#ifdef DDL_CFG_AUTO_KEY_PROCESS
	AutoSwProcess();
#endif 

#ifdef 	DDL_CFG_RFID
	CardDetectionProcess();
	CardModeProcess();
#endif

#if defined (DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
	iButtonModeProcess();
#endif 

#ifdef	DDL_CFG_WATCHDOG
	RefreshIwdg();
#endif

#ifdef	DDL_CFG_COVER_SWITCH
	CoverSwProcess();
#endif
	ModeBatteryLifeCycleTest();

	MainModeProcess();
	
	MotorControlProcess();
	#ifdef	USED_SECOND_MOTOR	
	MotorControlProcess_Second();
	#endif
	LockStatusProcess();

#ifdef	DDL_CFG_HIGHTEMP_SENSOR
	TempSensorProcess();
#endif

	AlarmProcess();
	TamperProofProcess();

#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
	FPMotorCoverProcess();
#endif	

#if defined	(DDL_CFG_FP)
#if defined	(DDL_CFG_FP_HFPM)
	HFPMRxDataProcess();
#else 
	FPCoverSwProcess();
#endif
#endif
	
	JigModeEndCheck();
	
	MotorSensorClear();
	#ifdef	USED_SECOND_MOTOR	
	MotorSensorClear_Second();
	#endif

#ifdef	DEBUG_TOUCH_AUTO
	DebugTouchKeyAutoSend();
#endif

	FactoryResetRun();
	FactoryResetProcess();

#ifdef	DDL_TEST_SET_FOR_CARD
	Feedback_KC_Test_Noti();
#endif 

#if defined (P_BT_WAKEUP_T)
	InnerPackConnectionProcess();
	InnerPackRxDataProcess();
	InnerPackTxDataProcess();
#endif 
#if defined (P_COM_DDL_EN_T)	
	PackConnectionProcess();
	PackRxDataProcess();
	PackTxDataProcess();
#endif 
	PackTxAlarmReportLowBatteryProcess();
	StopMode();
		
	return	0;
}




// model별 stopmode 진입여부를 결정하는 함수
// return 0 : stopmode로 진입
// return 1 : stopmode로 들어가지 않는다.
uint32_t		StopModeBeforeCallback( void )
{
	uint32_t		ret=0;

	AbnormalStatus.gwLockModeStatus = 0x00;
	
	if(GetBuzPrcsStep()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01;
	}
		
	if(GetLedMode()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 1u;
	}
	
	if(GetMotorPrcsStep()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 2u;
	}

	if(gbWakeUpMinTime10ms){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 3u;
	}
	
#if defined	(DDL_CFG_FP_PFM_3000) ||defined (DDL_CFG_FP_TS1071M) || defined (DDL_CFG_FP_INTEGRATED_FPM)  //hyojoon_20160831 PFM-3000 add
	if(gbCoverAccessDelaytime100ms){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 4u;
	}
#endif 

	if(GetMainMode()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 5u;
	}

#ifdef 	DDL_CFG_RFID
	if(GetCardMode()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 6u;
	}
#endif

	//if(GetLockStatusPrcsStep()){
	if(GetLockStatusPrcsStep() != 0 && gLockStatusTimer100ms <= (AUTORELOCK_DEFAULT_TIME_LSB*10)){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 7u;
	}
	
	if(GetTamperProofPrcsStep()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 8u;
		SetAbnormalSleepCheckTimer(1800u);
	}
	
	if(AlarmStatusCheck() == STATUS_SUCCESS){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 9u;
		SetAbnormalSleepCheckTimer(1800u);		
	}

	if(JigModeStatusCheck() == STATUS_SUCCESS){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 10u;
		SetAbnormalSleepCheckTimer(1800u);		
	}

#ifdef 	DDL_CFG_IBUTTON
	if(GMTouchKeyReceiveStatusCheck()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 11u;
	}
#endif

#if defined (DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
	if(GetDS1972ModeProcess()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 12u;
	}
#endif 

#ifdef	__DDL_MODEL_FRONTDOORSUITE	//보이스 스텝 플레그로 인해 슬립으로 들어가지 않는 상황을 방지 하기 위함.
#else
		if(GetVoicePrcsStep()){
			ret = 1;
			AbnormalStatus.gwLockModeStatus |= 0x01 << 13u;
		}
#endif	//__DDL_MODEL_FRONTDOORSUITE #endif	


#if defined (P_COM_DDL_EN_T)	
	if(PackModeCheck() == STATUS_SUCCESS){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 14u;
	}
	if(GetPackTxProcessStep()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 15u;
	}
	if(GetPackRxProcessStep()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 16u;
	}
#endif 	
#if defined (P_BT_WAKEUP_T)
	if(InnerPackModeCheck() == STATUS_SUCCESS){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 17u;
	}
	if(GetInnerPackTxProcessStep()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 18u;
	}
	if(GetInnerPackRxProcessStep()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 19u;
	}
#endif 

		// KC 인증을 위한 카드 출력 ON 처리
#ifdef	DDL_TEST_SET_FOR_CARD
	if(IsCardPowerOnForTest() == true){
		ret = 1;
		SetAbnormalSleepCheckTimer(1800u);		
	}
#endif
	
#ifdef DDL_CFG_EMERGENCY_119
	if(gfEmergencyRun == 1){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 20u;
	}
#endif 

#if defined (DDL_CFG_FP_HFPM)
	if(GetHFPMRxDataStep()){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 21u;
	}
#else 
	if(gPackTxAckWaitTimer100ms || gInnerPackTxAckWaitTimer100ms){
		ret = 1;
		AbnormalStatus.gwLockModeStatus |= 0x01 << 21u;
	}
#endif 	

	if(GetTestModeTime()){
		ret = 1;
	}
	
#ifdef USED_SECOND_MOTOR
	if(gbAutoChangepassagemodevalue < AUTOCHANGEPASSMODESTEP_CHECK)
	{
		gbAutoChangepassagemodevalue = AUTOCHANGEPASSMODESTEP_REDY;
		PCErrorClearSet();
		StartMotorOpen();
		StartMotorOpen_Second();
		gbMainMode = MODE_PACK_LOCK_OPERATION;
		gbModePrcsStep = 110;
		ret = 1;
	}
#endif	

	return	ret;
}



void		StopModePrepareCallback( void )
{

#ifdef DDL_CFG_IOEXPANDER
	gbExpanderOutPortVal = 0x00;
	gbExpanderOutmsleep2 = 0;
	gbExpanderOutsnsen2 = 1;
	IoExpanderOutPutControl();
#endif

#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE//MCU sleep전에 지문 커버(모터타입)가 열려 있으면 닫아준다 3번
		FPMotorCoverCloseRetry();
#endif
#ifdef 	DDL_CFG_TOUCHKEY
	TouchKeyCheckBeforeSleep();
	Touch_I2C_Off();
#endif

#ifdef	DDL_CFG_LED_TYPE1
	P_LED_DECO(0);
	P_LED_TENKEY(0);
#endif

#ifdef	DDL_CFG_LED_TYPE4
	P_LED_DECO(0);
	P_LED_TENKEY(0);
#endif

#ifdef	DDL_CFG_LED_TYPE2
	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	P_LED_DEAD(0);
#endif

#ifdef	DDL_CFG_LED_TYPE3
	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	P_LED_DEAD(0);
        P_LED_IB(0); 
#endif

#ifdef	DDL_CFG_LED_TYPE5
	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	P_LED_DEAD(0);
	P_LED_OPEN(0);
	P_LED_CLOSE(0);
#endif
	
#ifdef	DDL_CFG_LED_TYPE6
	P_LED_TENKEY(0);
	P_LED_OPEN(0);
	P_LED_CLOSE(0);
#endif 

#ifdef	DDL_CFG_LED_TYPE8
	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	//P_LED_LOCK(0);
	P_LED_DEAD(0);
#endif 


#ifdef	DDL_CFG_INSIDE_LED_LOW
	P_LED_LOW(0);
#endif	

#ifdef	DDL_CFG_FP  //hyojoon_20160831 PFM-3000 add
	BioModuleOff();
#endif

	ChangeMotorSensorPinToAnalog();

#ifdef	P_MECHA_HALL1_T
	ChangeAdditionalPositionCheckPinToAnalog();
#endif

#ifdef	DDL_CFG_WATCHDOG
	// Release 시에는 적용하지만 Debug 시에는 해당 내용 적용하지 않아야 함.
	ChangeDebugPinToAnalog();
#endif

#ifdef 	DDL_CFG_DIMMER
	Dimming_I2C_Off();
#endif

#ifdef	P_TOUCH_O_C_T
	Touch_O_C_Off();
#endif

#if defined (P_EXTEND_EMERGENCY) && defined (P_EXTEND_BREAK_IN)
	P_EXTEND_EMERGENCY(0);
	P_EXTEND_BREAK_IN(0);
#endif 

#ifdef	DDL_CFG_DS1972_IBUTTON //hyojoon_20160907 I-button
	iButtonEnable();
#endif 

#ifdef	_MASTERKEY_IBUTTON_SUPPORT
	MasterKeyEnable();
#endif 


#ifndef	DEBUG_PRINTF_ENABLE
#if defined (P_COM_DDL_EN_T)
	ChangePackPinToAnalog();
#endif 
#if defined (P_BT_WAKEUP_T)
	ChangeInnerPackPinToAnalog();
#endif 
#endif 
}


// model별 stopmode  빠져 나 온뒤 처리할 일 등록
void		StopModeAfterCallback( void )
{
	//STOP mode를 정상적으로 수행했음을 표시
//	gwStopModeCheck = 0x84BB;


#ifdef 	DDL_CFG_RFID
	CardProcessTimeClear();
#endif
	
	ChangeMotorSensorPinToInput();

#ifdef	P_MECHA_HALL1_T
	ChangeAdditionalPositionCheckPinToInput();
#endif

#ifdef 	DDL_CFG_TOUCHKEY
	Touch_I2C_On();
#endif

#ifdef 	DDL_CFG_DIMMER
	Dimming_I2C_On();
#endif

#ifdef	P_TOUCH_O_C_T
	Touch_O_C_On();
#endif

#ifdef	DDL_CFG_DS1972_IBUTTON //hyojoon_20160907 I-button
	iButtonDisable();
#endif

#ifdef	_MASTERKEY_IBUTTON_SUPPORT
	MasterKeyDisable();
#endif 


#ifdef	DDL_CFG_HIGHTEMP_SENSOR
	TempSensorCheckCounter();
#endif
#ifdef	P_SNS_EDGE_T			// Edge sensor
	DoorClosedCheckCounter();
#endif 
	TamperProofLockoutCounter();
	PackAlarmReportLowBatteryCounter();

	ModeBatteryLifeCycleCounter();

	LockStatusCheckTimeClear();
	ZWaveZigbeeLockStatusAlarmRepot();
#ifdef RTC_PCF85063
	TimeCorrection();
#endif 

#ifdef	DDL_AAAU_AUTOLOCK_HOLD //호주 및 뉴질랜드 전용 autolock hold 기능
	AutolockHoldClearTimerCounter();
#endif	//DDL_AAAU_AUTOLOCK_HOLD #endif
	
}




