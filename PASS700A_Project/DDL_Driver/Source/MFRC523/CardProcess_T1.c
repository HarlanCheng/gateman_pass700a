//------------------------------------------------------------------------------
/** 	@file		CardProcess_T1.c
	@version 0.1.01
	@date	2016.04.22
	@brief	Card UID Read Functions 
	@remark	 Mifare Classic Card의 4~7 byte UID를 읽어 처리
	@see	ISO14443AFunctions_T1.c
	@see	TRF7970Functions_T1.c
	@see	ParallelFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.08		by Jay
			- 4byte UID 카드 및 7byte UID 카드를 모두 읽어서 해당 Data를 출력
			- 외부에서 Card Readig Start and Stop 제어가 가능
			- Card Reading Start가 설정되면, Stop 설정 시 까지 반복하여 카드 읽기 수행
			- 최대 5장의 카드가 동시에 입력되어도 모두 읽기 가능 (Anticollision),
			       현재는 하드웨어 영향인지 아래의 경우 제대로 안됨

			       -> 3장 이상의 카드가 동식에 입력되 경우 제대로 된 Data 수신 불가능
			       -> 4byte UID 카드와 7byte UID 카드가 동시에 입력될 경우에는 제대로 된 Data 수신 불가능
			- 입력된 모든 카드의 UID를 읽었을 경우 카드가 떨어질 때까지 추가 Reading Process는 중지
		
		V0.1.01 2016.04.22		by Jay
			- CardDetectionProcess 함수에서 카드가 감지될 경우 ModeGotoCardVerify 함수 호출하도록 추가
			=> 이렇게 하지 않을 경우 CardProcess 루틴의 단계가 삭제되지 않고 계속 구동되는 문제 있음
		
*/
//------------------------------------------------------------------------------

#include "Main.h"

#ifdef DDL_CFG_RFID_RC523
#include "ParallelFunctions_T2.h"
#include "MX_spi_lapper.h"
#include "RC523Functions_T1.h"
#include "ISO14443AFunctions_T2.h"
#endif

#define ISO14443A_4_BIT     0x20 // Support ISO14443A 4 layer

BYTE gCardMode = 0;													/**< 카드 읽기 모드  */
BYTE gCardProcessStep = 0;											/**< 카드 읽기 모드 내에서 처리를 위한 단계  */

BYTE gCardReadStatus = 0;											/**< 카드 읽기 처리 결과  */

BYTE gCardOneUid[MAX_CARD_UID_SIZE];								/**< 읽어들인 1장의 카드 UID 저장  */
BYTE gCardMifareData[MAX_CARD_DATA_SIZE];							/**< card 내 data 저장 */
DesfireCardType gDesfireCardType = DESFIRE_CARD_NORMAL;
BYTE gCardAllUidBuf[MAX_CARD_ANTI_SIZE][MAX_CARD_UID_SIZE];			/**< 읽어들인 카드 UID를 모두 저장  */
BYTE gCardUidLength = 0;											/**< 읽어들인 카드의 UID 길이 */
BYTE gCardInputNumber = 0;											/**< 입력된 카드 중 읽어들인 카드의 개수를 저장하는 버퍼 */

WORD gCardPrcsTimer2ms = 0;											/**< 카드 읽기 처리를 위한 Timer, 2ms Tick, 0~500ms 설정 동작  */
BYTE gCardAwayTimer2ms = 0;											/**< 입력된 카드 떨어짐 감지를 위한 Timer, 2ms Tick, 50ms 설정 동작  */

BYTE gCardCheckLoopCnt = 0;											/**< 입력된 카드의 UID를 모두 읽었는지 확인하기 위해 반복 시도하는 회수 
																			또는, 카드가 떨어지는 걸 check 하기 위해 반복 시도하는 회수 */

BYTE gCardCollRetryCnt = 0;							

BYTE gReadBlock = 0;

BYTE glCardRegNum[2];			//카드 등록시의 처리 상태를 저장
BYTE gbCardData[16];
//BYTE glCardBlock = 0;			//Card의 읽기 쓰기시에 사용하는 Block 값


BYTE glCardBlock = 0;

void SetupCard(void)																			
{
//	SetupMFRC523();
}
//------------------------------------------------------------------------------
/** 	@brief	Card Process Time Counter
	@param	None
	@return 	None
	@remark 카드 읽기 처리 중에 사용하는 Timer, Timer Tick에서 호출하여 사용
	@remark 2ms Tick, 0~500ms 설정 동작
*/
//------------------------------------------------------------------------------
void CardProcessTimeCounter(void)
{
	if(gCardPrcsTimer2ms)		--gCardPrcsTimer2ms;
}


//------------------------------------------------------------------------------
/** 	@brief	Card Process Time Clear
	@param	None
	@return 	None
	@remark 카드 폴링을 위해 Sleep Mode에서 Wake Up 될 경우 바로 카드를 읽기 위해 처리 
*/
//------------------------------------------------------------------------------
void CardProcessTimeClear(void)
{
	gCardPrcsTimer2ms = 0;;
}



//------------------------------------------------------------------------------
/** 	@brief	Card Off Check Time Counter
	@param	None
	@return 	None
	@remark 읽어 들인 카드의 떨어짐 감지에 사용하는 Timer, Timer Tick에서 호출하여 사용
	@remark 2ms Tick, 50ms 설정 동작
*/
//------------------------------------------------------------------------------
void CardAwayCheckTimeCounter(void)
{
	if(gCardAwayTimer2ms)		--gCardAwayTimer2ms;
}


//------------------------------------------------------------------------------
/** 	@brief	Current Card Process State Check
	@param 	None
	@return 	[gCardProcessStep] : 0이 아닐 경우 카드 읽기 처리 완료
							    0일 경우 카드 읽기 처리 중
	@remark 현재 카드 읽기 동작이 완료되었는지 확인하는 함수
*/
//------------------------------------------------------------------------------
BYTE GetCardMode(void)
{
	return (gCardMode);	
}



//------------------------------------------------------------------------------
/** 	@brief	Card Mode Clear
	@param 	None
	@return 	None
	@remark 카드 읽기 동작 종료
*/
//------------------------------------------------------------------------------
void CardModeClear(void)
{
	RfidPowerOff();

	gCardMode = 0;
	gCardProcessStep = 0;
}



//------------------------------------------------------------------------------
/** 	@brief	Card UID buffer all clear
	@param 	None
	@return 	None
	@remark 읽어 들인 카드의 UID가 저장되는 buffer를 모두 Clear
	@remark 카드 읽기 시작할 때 수행 
*/
//------------------------------------------------------------------------------
void CardAllUidBufferClear(void)
{

	gCardInputNumber = 0;

	memset(gCardOneUid, 0xFF, MAX_CARD_UID_SIZE);
	memset(gCardAllUidBuf, 0xFF, sizeof(gCardAllUidBuf));

}



//------------------------------------------------------------------------------
/** 	@brief	Temporary save Card UID 
	@param 	None
	@return 	[UID_NONE] : UID가 모두 00h이거나 FFh일 경우 
	@return 	[UID_AVAILABLE] : UID가 사용 가능할 경우
	@return 	[UID_DUPICATED] : UID가 이미 입력된 카드의 것과 동일할 경우 
	@remark 입력된 카드의 UID의 임시 저장을 위해 UID가 저장 가능한 것인지 비교
*/
//------------------------------------------------------------------------------
BYTE TempSaveCompletedUID(void)
{
	
	BYTE bCnt;
	
	if( DataCompare(gCardOneUid, 0xFF, gCardUidLength) == _DATA_IDENTIFIED	)		return (UID_NONE);		// UID가 모두 FF인지 검사
	if( DataCompare(gCardOneUid, 0x00, gCardUidLength) == _DATA_IDENTIFIED	)		return (UID_NONE);		// UID가 모두 00인지 검사

	// 1개 이상 UID를 읽어왔다면  같은 UID를 저장했었는지 검사
	for(bCnt = 0; bCnt < gCardInputNumber; bCnt++)
	{
		if( (BYTE)memcmp(gCardOneUid, gCardAllUidBuf[bCnt], MAX_CARD_UID_SIZE) == 0 )	return (UID_DUPICATED); 		// 이미 같은 UID가 저장되어 있다면 리턴
	}

	memcpy(gCardAllUidBuf[gCardInputNumber], gCardOneUid, MAX_CARD_UID_SIZE);
	gCardInputNumber++;

	return (UID_AVAILABLE);
}



//------------------------------------------------------------------------------
/** 	@brief	Card Read Start
	@param 	None
	@return 	None
	@remark 카드 읽기 시작
	@remark CardModePowerOnOff 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoReadStart(void)
{
	
	gCardProcessStep = 0;	

	gCardMode = CARDMODE_POWER_ONOFF;
	gCardProcessStep = CARDPRCS_RFID_POWERON;
	gCardReadStatus = 0;

	CardAllUidBufferClear();

}


//------------------------------------------------------------------------------
/** 	@brief	Card Read Stop
	@param 	None
	@return 	None
	@remark 카드 읽기 종료
	@remark CardModePowerOnOff 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoReadStop(void)
{
	gCardMode = CARDMODE_POWER_ONOFF;
	gCardProcessStep = CARDPRCS_READ_STOP;
	gCardReadStatus = 0;
}



//------------------------------------------------------------------------------
/** 	@brief	Read IC Off
	@param 	None
	@return 	None
	@remark 카드 출력 Off
	@remark CardModePowerOnOff 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoRfTxOff(void)
{
	ClearMifairMode();
	gCardMode = CARDMODE_POWER_ONOFF;
	gCardProcessStep = CARDPRCS_RFID_TXOFF;
	//SIOModeClear();//sjc_RC523 add 주석
}



//------------------------------------------------------------------------------
/** 	@brief	Card WUPA Start
	@param 	None
	@return 	None
	@remark 카드 UID 읽기 시작
	@remark CardModeSelect 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoReqaStart(void)
{
	gCardMode = CARDMODE_SELECT;
	gCardProcessStep = CARDPRCS_WUPA_SEND;
}



//------------------------------------------------------------------------------
/** 	@brief	Card Select Start
	@param 	None
	@return 	None
	@remark 카드 UID 읽기 시작
	@remark CardModeSelect 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoAnticollisionStart(void)
{

	gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;
	gCardCollRetryCnt = 32;
	AntiColVariInit();
	
	CascadeLevelSet(1);
	
	gCardMode = CARDMODE_SELECT;
	gCardProcessStep = CARDPRCS_ANTICOLLISION_SEND;

}



//------------------------------------------------------------------------------
/** 	@brief	Card Reading Result Process
	@param 	[ProcessStep] : 카드 결과 처리 단계
	@return 	None
	@remark 카드 UID 읽기 결과 처리
	@remark 카드 종류에 따라 ProcessStep으로 다르게 처리 가능
*/
//------------------------------------------------------------------------------
void CardGotoReadResult(BYTE ProcessStep)
{
	gCardMode = CARDMODE_READ_RESULT;
	gCardProcessStep = ProcessStep;
}



//------------------------------------------------------------------------------
/** 	@brief	Card Away from Reader Check
	@param 	None
	@return 	None
	@remark 입력된 카드 떨어짐 확인 시작
	@remark CardModeAwayFromReaderCheck	 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoAwayFromReaderCheck(void)
{
	RfidTxOff();

	// 카드가 떨어지는 걸 check 하기 위해 반복 시도하는 회수 설정 
	gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;

	gCardMode = CARDMODE_CARD_AWAY_CHK;
	gCardProcessStep = CARDPRCS_AWAY_CHECK;
}

//------------------------------------------------------------------------------
/** 	@brief	Send RATS
	@param 	[ProcessStep] : Issuing a Request for Answer to Select(RATS) command
	@return 	None
	@remark If a reply(ATS) is received then the tag is now fully selected and ready to exchange data with the Reader module.
	@remark After the tag is in the selected state, a PPS command can be issued to change the data rate of the communication.
			between the reader and the tag
*/
//------------------------------------------------------------------------------
/*//sjc_RC523 add 주석 시작
void CardGotoRATS(BYTE ProcessStep)
{
	gCardMode = CARDMODE_RATS;
	gCardProcessStep = ProcessStep;
}

void CardGotoDesfireSelectAppProcessBySIO(BYTE ProcessStep)
{
	gCardMode = CARDMODE_DESFIRE_SELECT_APP;
	gCardProcessStep = ProcessStep;
}

void CardGotoDesfireAuthProcessBySIO(BYTE ProcessStep)
{
	gCardMode = CARDMODE_DESFIRE_AUTHENTICATE;
	gCardProcessStep = ProcessStep;
}

void CardGotoDesfireReadFile00ProcessBySIO(BYTE ProcessStep)
{
	gCardMode = CARDMODE_DESFIRE_READ_FILE_00;
	gCardProcessStep = ProcessStep;
}

void CardGotoDesfireReadFile01ProcessBySIO(BYTE ProcessStep)
{
	gCardMode = CARDMODE_DESFIRE_READ_FILE_01;
	gCardProcessStep = ProcessStep;
}

void CardGotoDesfireWriteFile01Step1ProcessBySIO(BYTE ProcessStep)
{
	gCardMode = CARDMODE_DESFIRE_WRITE_FILE_01_STEP1;
	gCardProcessStep = ProcessStep;
}

void CardGotoDesfireWriteFile01Step2ProcessBySIO(BYTE ProcessStep)
{
	gCardMode = CARDMODE_DESFIRE_WRITE_FILE_01_STEP2;
	gCardProcessStep = ProcessStep;
}
*///sjc_RC523 add 주석 끝
//------------------------------------------------------------------------------
/** 	@brief	Card Mode - Idle 
	@param 	None
	@return 	None
	@remark 카드 읽기 동작 수행 중 아님
*/
//------------------------------------------------------------------------------
void CardModeIdleState(void)
{
	switch(gCardProcessStep)
	{
		case CARDPRCS_IDLE_STATE:
			break;

		default:
			break;
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Card Mode - Reader IC Power On/Off 
	@param 	None
	@return 	None
	@remark 카드 읽기를 위한 Reader IC의 EN ON/OFF
	@remark 카드 읽기를 위한 Reader IC의 RF TX ON/OFF
	@remark 카드 읽기 종료를 하지 않을 경우 500ms 간격으로 카드 읽기 반복 수행
	@remark 카드 출력만 ON/OFF 반복하면서 카드 읽기 수행
	@remark 카드 읽기 시작 및 종료는 외부에서 수행, 이 때는 EN ON/OFF
*/
//------------------------------------------------------------------------------
void CardModePowerOnOff(void)
{
	
	switch(gCardProcessStep)
	{
		case CARDPRCS_RFID_POWERON:
			if(gCardPrcsTimer2ms)		break;

			RfidPowerOn();
			gCardPrcsTimer2ms = 0;
			gCardProcessStep = CARDPRCS_RFID_TXON;

		case CARDPRCS_RFID_TXON:
			if(gCardPrcsTimer2ms)		break;

			RfidTxOn();

			// When a PICC is exposed to an unmodulated operating field
			// it shall be able to accept a quest within 5 ms.
			// PCDs should periodically present an unmodulated field of at least
			// 5,1 ms duration. (ISO14443-3)
			gCardPrcsTimer2ms = 4;		// 6~8ms. 

			CardAllUidBufferClear();

			CardGotoReqaStart();
			break;

		case CARDPRCS_RFID_TXOFF:
			RfidTxOff();
						
			gCardPrcsTimer2ms = 250;
			gCardProcessStep = CARDPRCS_RFID_TXON;
			break;
			
		case CARDPRCS_RFID_POWEROFF:
			if(gCardPrcsTimer2ms)		break;
		
		case CARDPRCS_READ_STOP:
		default:
			CardModeClear();
			break;
	}

}




//------------------------------------------------------------------------------
/** 	@brief	Card Mode - Card Select 
	@param 	None
	@return 	None
	@remark 카드 읽기 동작 수행
	@remark RF On -> REQA 전송 -> ATQA 확인 -> Anticollision 알고리즘 수행 -> Select 수행
	@remark 카드 UID 읽기가 완료되지 않을 경우 gCardCheckLoopCnt 회수만큼 반복 수행
	@remark 입력된 카드의 UID를 모두 읽었는지 확인하기 위해 gCardCheckLoopCnt 회수만큼 반복 수행
	@remark ISO14443A 규격 참조
*/
//------------------------------------------------------------------------------
void CardModeSelect(void)
{
	BYTE bTmp;
	BYTE sak;

	switch(gCardProcessStep)
	{
		case CARDPRCS_REQA_SEND:
			if(gCardPrcsTimer2ms)		break;

			IrqClear();
			Iso14443aReqaSend(ISO14443A_REQA);

			gCardPrcsTimer2ms = 2; 
			gCardProcessStep = CARDPRCS_REQA_RESPOND;
			break;
			
		case CARDPRCS_WUPA_SEND:
			if(gCardPrcsTimer2ms)		break;

			IrqClear();
			Iso14443aReqaSend(ISO14443A_WUPA);
		
			gCardPrcsTimer2ms = 2; 
			gCardProcessStep = CARDPRCS_REQA_RESPOND;
			break;

		case CARDPRCS_REQA_RESPOND:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gCardPrcsTimer2ms)	break;

				// 여러 개의 카드가 동시에 접촉될 경우에는 
				// 접촉된 카드를 모두 읽어들였는지를 
				//일정 회수동안 Loop를 수행하면서 확인할 수 밖에 없음.
				if(gCardCheckLoopCnt == 0)
				{
					if(gCardInputNumber == 0)
					{
						gCardReadStatus = CARDREAD_NO_CARD;
						CardGotoRfTxOff();
					}
					else
					{
						CardGotoReadResult(CARDPRCS_MIFARE_PROCESS);
					}
					break;
				}
				else
				{
					gCardCheckLoopCnt--;
					gCardProcessStep = CARDPRCS_REQA_SEND;
				}	
				break;	
			}

			
			if(Iso14443aReqaRespond(&gCardUidLength) == STATUS_SUCCESS)
			{
				CardGotoAnticollisionStart();
			}
			else
			{
				gCardProcessStep = CARDPRCS_REQA_SEND;				
			}
			break;

		case CARDPRCS_ANTICOLLISION_SEND:
			if(gCardCollRetryCnt==0){
				CardGotoRfTxOff();
				break;
			}
			gCardCollRetryCnt --;
			IrqClear();
			if(Iso14443aAnticollisionSend() == STATUS_FAIL)
			{
				gCardProcessStep = CARDPRCS_SELECT_SEND;
				break;
			}
			gCardPrcsTimer2ms = 5; 
			gCardProcessStep = CARDPRCS_ANTICOLLISION_RESPOND;
			break;

		case CARDPRCS_ANTICOLLISION_RESPOND:
			bTmp = IrqCheck();
			if((bTmp != IRQ_RX_COMPLETED) && (bTmp != IRQ_COLLISION_ERROR)) 
			{
				if(gCardPrcsTimer2ms)	break;
				
				gCardProcessStep = CARDPRCS_REQA_SEND;
				break;
			}

			bTmp = Iso14443aAnticollisionRespond(bTmp);
			if(bTmp == STATUS_SUCCESS)
			{
				gCardProcessStep = CARDPRCS_SELECT_SEND;
			}
			else if(bTmp == STATUS_FAIL)
			{
				gCardProcessStep = CARDPRCS_REQA_SEND;				
			}
			else
			{
				gCardProcessStep = CARDPRCS_ANTICOLLISION_SEND;
			}
			break;

		case CARDPRCS_SELECT_SEND:
			IrqClear();
			Iso14443aSelectSend();
			gCardPrcsTimer2ms = 5; 
			gCardProcessStep = CARDPRCS_SELECT_RESPOND;
			break;

		case CARDPRCS_SELECT_RESPOND:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gCardPrcsTimer2ms)	break;

				gCardProcessStep = CARDPRCS_REQA_SEND;
				break;				
			}

			bTmp = Iso14443aSelectRespond(&sak);
			if(bTmp == SAK_UID_COMPLETE)
			{
				//알레그로 원본이 원래 주석
				//Iso14443aHalt();

				//CopyCardUid(gCardOneUid, gCardUidLength);

				//if(TempSaveCompletedUID() == UID_AVAILABLE)
				//{
				//	gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;
				//	break;
				//}

				//gCardPrcsTimer2ms = 0;
				//gCardProcessStep = CARDPRCS_REQA_SEND;
				//// UID가 모두 00h 또는 FFh이거나 이미 등록한 UID와 중복될 경우 처리
				//break;
				//알레그로 원본이 원래 주석

				Iso14443aHalt();//sjc_RC523 add
				
				CopyCardUid(gCardOneUid, gCardUidLength);
				//sjc_RC523 add 주석 시작	
				/*
				TempSaveCompletedUID();		
				if(sak == ISO14443A_4_BIT)
				{
					//CardGotoRATS(CARDPRCS_RATS_SEND);//sjc_RC523 add 주석
				}
				else
				{
					CardGotoReadResult(CARDPRCS_MIFARE_PROCESS);
				}
				//sjc_RC523 add 주석 끝
				*/
				//sjc_RC523 add start
				bTmp = TempSaveCompletedUID();
				if(bTmp == UID_AVAILABLE)
				{
					gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;
					break;
				}
				else if(bTmp == UID_DUPICATED)
				{
					if(gCardCheckLoopCnt)    --gCardCheckLoopCnt;

					if(gCardCheckLoopCnt == 0)
					{
						if(gCardInputNumber == 0)
						{
							gCardReadStatus = CARDREAD_NO_CARD;
							CardGotoRfTxOff();
						}
						else
						{
#ifdef	FELICA_CARD_SUPPORTED
							SetInputCardType(CARD_TYPE_MIFARE);
#endif
							CardGotoReadResult(CARDPRCS_MIFARE_PROCESS);
						}
						break;
					}
				}

				gCardPrcsTimer2ms = 0;
				gCardProcessStep = CARDPRCS_REQA_SEND;
				// UID가 모두 00h 또는 FFh이거나 이미 등록한 UID와 중복될 경우 처리
				//sjc_RC523 add end
				break;
						
			}
			// 만약 UID 읽기가 complete되지 않았다면
			else if(bTmp == SAK_UID_NOT_COMPLETE)		
			{
				CopyCardUid(gCardOneUid, gCardUidLength);

				if(gCardUidLength == 7)
				{
					AntiColVariInit();

					CascadeLevelSet(2);

					gCardProcessStep = CARDPRCS_ANTICOLLISION_SEND;
					break;
				}
			}

			gCardProcessStep = CARDPRCS_REQA_SEND;
			break;

		default:
			CardModeClear();
			break;
	}		

}


BYTE CardReceiveCheck()
{
	BYTE bTmp;
	
	bTmp = GetMifairMode();
	if(bTmp != MIFAIR_NONE) return 0;		// processing

	bTmp = GetMifairPrsResult();
	return bTmp;
}

BYTE glFidBuf[MAX_CARD_DATA_SIZE];

/*//sjc_RC523 add 주석 시작
//BYTE FidCheck(BYTE* readFid)
BYTE FidCheck(BYTE* readFid, BYTE slotNo)
{
	BYTE bTmp, bCnt;
	BYTE bTmpBuf[MAX_CARD_DATA_SIZE];

	bCnt = 0;
	RomRead(bTmpBuf, (WORD)(MEM_CARD_DATA+((slotNo-1)*MAX_CARD_DATA_SIZE)), MAX_CARD_DATA_SIZE);
	bTmp = memcmp(bTmpBuf, &readFid[0], 4);
	if(bTmp==0){
		return	bCnt;
	}
	bTmp = memcmp(bTmpBuf, &readFid[4], 4);
	if(bTmp==0){
		return 0x80|bCnt;
	}
*///sjc_RC523 add 주석 끝

/*	
	for(bCnt=0;bCnt<MAX_REG_CARD_NUM;bCnt++)
	{
		RomRead(bTmpBuf, (WORD)(CARD_FID+(bCnt*MAX_CARD_DATA_SIZE)), MAX_CARD_DATA_SIZE);
		bTmp = memcmp(bTmpBuf, &readFid[0], 4);
		if(bTmp==0){
			return  bCnt;
		}
		bTmp = memcmp(bTmpBuf, &readFid[4], 4);
		if(bTmp==0){
			return 0x80|bCnt;
		}
	}
*/	
//	return 0xFF;	//sjc_RC523 add 주석
//}				//sjc_RC523 add 주석

//------------------------------------------------------------------------------
/** 	@brief	CardCheckCountReset
	@param 	None
	@return 	None
	@remark 
	@remark CardCheckCountReset
*/
//------------------------------------------------------------------------------

void CardCheckCountReset(void)  
{
	gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;
}
/*//sjc_RC523 add 주석 시작
void CardGotoReadFidCountStart(void)
{
	CardGotoReadResult(CARDPRCS_MIFARE_READ_COUNT_SEND);
	gCardPrcsTimer2ms = 0;
}
void CardGotoWriteFidCountStart(void)
{
	CardGotoReadResult(CARDPRCS_MIFARE_WRITE_REG_COUNT_SEND);
	gCardPrcsTimer2ms = 0;
}
*///sjc_RC523 add 주석 끝
void CardGotoWriteFidDataStart(BYTE* bFid)
{
//	glCardBlock = bBlock;
//	if(bLocation==2){
//		glCardBlock |= 0x80;
//	}
	memcpy(glFidBuf, bFid, MAX_CARD_DATA_SIZE);
//	CardGotoReadResult(CARDPRCS_MIFARE_WRITE_DATA_SEND);
	CardGotoReadResult(CARDPRCS_MIFARE_WRITE_AUTH_DATA_SEND);
	
	gCardPrcsTimer2ms = 0;
}

void CardGotoReadFidDataStart(BYTE bBlock)
{
	glCardBlock = bBlock;
	CardGotoReadResult(CARDPRCS_MIFARE_READ_AUTH_DATA_START);
}

BYTE GetCardFidBlock(void)
{
	return glCardBlock;
}


void CardModeReadResult(void)
{
	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_PROCESS:
			gCardReadStatus = CARDREAD_SUCCESS;
			CardGotoAwayFromReaderCheck();
			break;

		default:
			CardModeClear();
			break;
	}
	/*BYTE bTmp, bCnt;
	
	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_WAIT:
			
			if(gCardPrcsTimer2ms) break;
			CardGotoRfTxOff();
			break;
		case CARDPRCS_MIFARE_PROCESS:
			gCardProcessStep = CARDPRCS_MIFARE_KEYA_SEND;
			break;

		case CARDPRCS_MIFARE_KEYA_SEND:
			CardKeyAMake(1, gCardOneUid);
			GotoMifairAuthenticaion(1, gCardOneUid);
			gCardPrcsTimer2ms = 50;
			gCardProcessStep = CARDPRCS_MIFARE_KEYA_RESPOND;
			break;

		case CARDPRCS_MIFARE_KEYA_RESPOND:
			if(!gCardPrcsTimer2ms){
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){
				gCardProcessStep = CARDPRCS_MIFARE_GET_BLOCK_KEY_SEND;
			}
			else{
				CardGotoRfTxOff();
			}
			break;			

		case CARDPRCS_MIFARE_GET_BLOCK_KEY_SEND:
			GotoMifairReadBlock(1);
			gCardPrcsTimer2ms = 50;
			gCardProcessStep = CARDPRCS_MIFARE_GET_BLOCK_KEY_RESPONSE;
			break;

		case CARDPRCS_MIFARE_GET_BLOCK_KEY_RESPONSE:
			if(!gCardPrcsTimer2ms){
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){
				CardSaveKeyARdmData(gRfidFifoDataBuf);
				gCardReadStatus = CARDREAD_SUPPORT_CLASSIC_SUCCESS;
				
				gCardPrcsTimer2ms = 100;
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;
			}
			else{
				CardGotoRfTxOff();
			}

			break;


		case CARDPRCS_MIFARE_READ_COUNT_SEND:
//			if(gCardPrcsTimer2ms) break;				// 혹시 몰라 delay주기 위해 넣음. 추후 보고 삭제 필요. 
			GotoMifairReadBlock(2);
			gCardPrcsTimer2ms = 50;
			gCardProcessStep = CARDPRCS_MIFARE_READ_COUNT_RESPONSE;
			break;

		case CARDPRCS_MIFARE_READ_COUNT_RESPONSE:
			if(!gCardPrcsTimer2ms){
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){
				gCardReadStatus = CARDREAD_READ_COUNT_SUCCESS;
				
				glCardRegNum[0] = gRfidFifoDataBuf[0];
				glCardRegNum[1] = gRfidFifoDataBuf[1];

				if((glCardRegNum[0] < 1) || (glCardRegNum[0] > (MAX_REG_CARD_SLOT*2))){	//v12 추가
					glCardRegNum[0] = 1;
				}	
				gCardPrcsTimer2ms = 100;
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;
			}
			else{
				CardGotoRfTxOff();
			}
			break;
		case CARDPRCS_MIFARE_WRITE_REG_COUNT_SEND:
			glCardRegNum[0]++;
			if(glCardRegNum[0] > (MAX_REG_CARD_SLOT*2))
			{
				glCardRegNum[0] = 1;
			}
//			glCardRegNum[1] = 0;
			glCardRegNum[1] = glCardRegNum[0];			// 등록 후 바로 쓸 수 있도록 강제적으로 만듬. 


			for(bCnt = 0; bCnt < 16; bCnt++)
			{
				gbCardData[bCnt] = 0x00;
			}
			gbCardData[0] = glCardRegNum[0];
			gbCardData[1] = glCardRegNum[1];

			gbCardData[2] = DataChecksumCal(gbCardData, 2);
			GotoMifairWriteBlock(2, gbCardData);
			gCardPrcsTimer2ms = 100;
			gCardProcessStep = CARDPRCS_MIFARE_WRITE_REG_COUNT_RESPONSE;
			break;
			
		case CARDPRCS_MIFARE_WRITE_REG_COUNT_RESPONSE:
			if(!gCardPrcsTimer2ms){
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){
				GotoMifairReadBlock(2);		// 다시 한번 읽어서 잘 쓰여졌나 확인 하기 위함. 
				gCardPrcsTimer2ms = 50;
				gCardProcessStep = CARDPRCS_MIFARE_WRITE_REG_COUNT_VERIFY;	
			}
			else{
				CardGotoRfTxOff();
			}
			break;
		case CARDPRCS_MIFARE_WRITE_REG_COUNT_VERIFY:
			if(!gCardPrcsTimer2ms){
//				CardGotoRfTxOff();
				gCardProcessStep = CARDPRCS_MIFARE_WRITE_REG_COUNT_SEND;
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){
				bTmp = DataChecksumCal(gRfidFifoDataBuf, 2);
				if(bTmp != gRfidFifoDataBuf[2])
				{
//					CardGotoRfTxOff();					
					gCardProcessStep = CARDPRCS_MIFARE_WRITE_REG_COUNT_SEND;
					break;
				}
				
				if((gRfidFifoDataBuf[0] != gbCardData[0]) || (gRfidFifoDataBuf[1] != gbCardData[1]))
				{
					CardGotoRfTxOff();
					break;
				}
				
				glCardBlock = glCardRegNum[0];
				if(glCardBlock > MAX_REG_CARD_SLOT)
				{
					glCardBlock -= MAX_REG_CARD_SLOT;
				}
				glCardBlock = glCardBlock*4;

				gCardReadStatus = CARDREAD_WRITE_COUNT_SUCCESS;

				gCardPrcsTimer2ms = 100;
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;



			}
			else{
//				CardGotoRfTxOff();
				gCardProcessStep = CARDPRCS_MIFARE_WRITE_REG_COUNT_SEND;
			}
			break;

		case CARDPRCS_MIFARE_READ_AUTH_DATA_START:
			if(glCardBlock > MAX_REG_CARD_SLOT)
			{
				glCardBlock -= MAX_REG_CARD_SLOT;
			}	
			glCardBlock = glCardBlock*4;

			CardKeyAMake(glCardBlock, gCardOneUid);
			GotoMifairAuthenticaion(glCardBlock, gCardOneUid);

			gCardPrcsTimer2ms = 50;
			gCardProcessStep = CARDPRCS_MIFARE_READ_AUTH_DATA_SEND;
			break;
			
		case CARDPRCS_MIFARE_READ_AUTH_DATA_SEND:
			if(!gCardPrcsTimer2ms){
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){
				GotoMifairReadBlock(glCardBlock);
				gCardPrcsTimer2ms = 50;
				gCardProcessStep = CARDPRCS_MIFARE_READ_AUTH_DATA_RESPONSE;
			}
			else{
				CardGotoRfTxOff();
			}
			break;
			
		case CARDPRCS_MIFARE_READ_AUTH_DATA_RESPONSE:
			if(!gCardPrcsTimer2ms){
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){
				bTmp = DataChecksumCal(gRfidFifoDataBuf, 4);
				if(bTmp != gRfidFifoDataBuf[4])
				{
					if((gRfidFifoDataBuf[4] != 0x00) && (gRfidFifoDataBuf[4] != 0xFF))
					{
						CardGotoRfTxOff();
						break;
					}	
				}
				
				for(bCnt = 0; bCnt < 4; bCnt++)
				{
					gbCardData[bCnt] = gRfidFifoDataBuf[bCnt];
				}
				GotoMifairReadBlock(1+glCardBlock);
				gCardPrcsTimer2ms = 50;
				gCardProcessStep = CARDPRCS_MIFARE_READ_AUTH_DATA_NEXT_CHECH;
			}
			else{
				CardGotoRfTxOff();
			}			
			break;


		case CARDPRCS_MIFARE_READ_AUTH_DATA_NEXT_CHECH:		// secot에 2번쨰 FID 확인. 
			if(!gCardPrcsTimer2ms){
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){
				bTmp = DataChecksumCal(gRfidFifoDataBuf, 4);
				if(bTmp != gRfidFifoDataBuf[4])
				{
					if((gRfidFifoDataBuf[4] != 0x00) && (gRfidFifoDataBuf[4] != 0xFF))
					{
						CardGotoRfTxOff();
						break;
					}	
				}
				
				for(bCnt = 0; bCnt < 4; bCnt++)
				{
					gbCardData[4+bCnt] = gRfidFifoDataBuf[bCnt];
				}
				//sjc_RC523 add 주석 시작
				//bTmp = FidCheck(gbCardData, GetSlotNumberToFindCardData());

				
				//if(bTmp==0xFF){		// fail
				//	gCardReadStatus = CARDREAD_VERIFY_FID_FAIL;
				//	CardGotoAwayFromReaderCheck();
				//	break;
				//}
				//else if(bTmp&0x80){		//  second block에 쓰여진 경우. 
				//	glCardBlock |= 0x80;
				//	gCardReadStatus = CARDREAD_VERIFY_FID_SUCCESS;
				//	gCardPrcsTimer2ms = 100;
				//	gCardProcessStep = CARDPRCS_MIFARE_WAIT;					
				//}
				//else{			// first block 에 쓰여진 경우. 
				//	gCardReadStatus = CARDREAD_VERIFY_FID_SUCCESS;
				//	gCardPrcsTimer2ms = 100;
				//	gCardProcessStep = CARDPRCS_MIFARE_WAIT;					
				//}
				//sjc_RC523 add 주석 끝
			}
			else{
				CardGotoRfTxOff();
			}			
			break;

		case CARDPRCS_MIFARE_WRITE_AUTH_DATA_SEND:
			if(glCardBlock&0x80){				// second block에 쓰여진 경우. First Block에 데이터 저장.
				glCardBlock &= 0x7F;
			}
			else{							// First block에 쓰여진 경우, Second Block에 데이터 저장.
				glCardBlock &= 0x7F;
				glCardBlock++;
			}
			
			CardKeyAMake(glCardBlock, gCardOneUid);
			GotoMifairAuthenticaion(glCardBlock, gCardOneUid);

			gCardPrcsTimer2ms = 50;
			gCardProcessStep = CARDPRCS_MIFARE_WRITE_AUTH_DATA_RESPONSE;
			break;
		
		case CARDPRCS_MIFARE_WRITE_AUTH_DATA_RESPONSE:
			if(!gCardPrcsTimer2ms){
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){

				gCardPrcsTimer2ms = 50;
				gCardProcessStep = CARDPRCS_MIFARE_WRITE_DATA_SEND;
			}
			else{
				CardGotoRfTxOff();
			}		
			break;



		case CARDPRCS_MIFARE_WRITE_DATA_SEND:				// FID 저장부. 

			for(bCnt = 0; bCnt < 16; bCnt++)
			{
				gbCardData[bCnt] = 0x00;
			}
			gbCardData[0] = glFidBuf[0];
			gbCardData[1] = glFidBuf[1];
			gbCardData[2] = glFidBuf[2];
			gbCardData[3] = glFidBuf[3];

			gbCardData[4] = DataChecksumCal(gbCardData, 4);	


			//if(glCardBlock & 0x80){
			//	glCardBlock = glCardBlock&0x3F;
			//	gbCardData[7] = glCardBlock / 4;
			//	glCardBlock++;
			//}

			GotoMifairWriteBlock(glCardBlock, gbCardData);
			gCardPrcsTimer2ms = 50;
			gCardProcessStep = CARDPRCS_MIFARE_WRITE_DATA_RESPONSE;
			break;
			
		case CARDPRCS_MIFARE_WRITE_DATA_RESPONSE:
			if(!gCardPrcsTimer2ms){
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){
				GotoMifairReadBlock(glCardBlock);
				gCardPrcsTimer2ms = 50;
				gCardProcessStep = CARDPRCS_MIFARE_WRITE_DATA_VERIFY;
			}
			else{
				CardGotoRfTxOff();
			}		
			break;
			
		case CARDPRCS_MIFARE_WRITE_DATA_VERIFY:
			if(!gCardPrcsTimer2ms){
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS){
				gCardReadStatus = CARDREAD_WRITE_FID_SUCCESS;
				CardGotoAwayFromReaderCheck();
			}
			else{
				CardGotoRfTxOff();
			}					
			break;
			
	}*/

}

void CardModeRATS(void)
{
	BYTE bTmp;

	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_WAIT:
			if(gCardPrcsTimer2ms) break;
			CardGotoRfTxOff();
			break;

		case CARDPRCS_RATS_SEND:
			GotoMifairRATS();
			gCardPrcsTimer2ms = 50; // todo: Check timing
			gCardProcessStep = CARDPRCS_RATS_RESPONSE;
			break;

		case CARDPRCS_RATS_RESPONSE:
			if(!gCardPrcsTimer2ms)
			{
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS)
			{
				gCardReadStatus = CARDREAD_SUPPORT_DESFIRE_SUCCESS;
				//SIOGotoModeInitialize();//sjc_RC523 add 주석
//				gCardPrcsTimer2ms = 100; // todo: Check timing
				gCardPrcsTimer2ms = 250; // todo: Check timing
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;
			}
			else
			{
				CardGotoRfTxOff();
			}
			break;

		default:
			break;
	}
}

void CardModeDesfireSelectApp(void)
{
	BYTE bTmp;

	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_WAIT:
			if(gCardPrcsTimer2ms) break;
			CardGotoRfTxOff();
			break;

		case CARDPRCS_DESFIRE_SELECT_APP_SEND:
			GotoMifairDesfireSelectApp();
			gCardPrcsTimer2ms = 100; 
			gCardProcessStep = CARDPRCS_DESFIRE_SELECT_APP_RESPOND;
			break;

		case CARDPRCS_DESFIRE_SELECT_APP_RESPOND:
			if(!gCardPrcsTimer2ms)
			{
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS)
			{
				//SIOGotoCardApiDesfireSelectAppStep2();//sjc_RC523 add 주석
				gCardPrcsTimer2ms = 100; // todo: Check timing
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;
			}
			else
			{
				CardGotoRfTxOff();
			}
			break;

		default:
			break;
	}
}

void CardModeDesfireAuth(void)
{
	BYTE bTmp;

	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_WAIT:
			if(gCardPrcsTimer2ms) break;
			CardGotoRfTxOff();
			break;

		case CARDPRCS_DESFIRE_AUTH_SEND_STEP1:
			GotoMifairDesfireAuthStep1();
			gCardPrcsTimer2ms = 100; 
			gCardProcessStep = CARDPRCS_DESFIRE_AUTH_RSP_STEP1;
			break;

		case CARDPRCS_DESFIRE_AUTH_RSP_STEP1:
			if(!gCardPrcsTimer2ms)
			{
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS)
			{
				//SIOGotoCardApiDesfireAuthStep2();//sjc_RC523 add 주석
				gCardPrcsTimer2ms = 100; // todo: Check timing
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;
			}
			else
			{
				CardGotoRfTxOff();
			}
			break;

		case CARDPRCS_DESFIRE_AUTH_SEND_STEP2:
			GotoMifairDesfireAuthStep2();
			gCardPrcsTimer2ms = 100; 
			gCardProcessStep = CARDPRCS_DESFIRE_AUTH_RSP_STEP2;
			break;

		case CARDPRCS_DESFIRE_AUTH_RSP_STEP2:
			if(!gCardPrcsTimer2ms)
			{
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS)
			{
				//SIOGotoCardApiDesfireAuthStep3();//sjc_RC523 add 주석
				gCardPrcsTimer2ms = 100; // todo: Check timing
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;
			}
			else
			{
				CardGotoRfTxOff();
			}
			break;

		default:
			break;
	}
}

void CardModeDesfireReadFile00(void)
{
	BYTE bTmp;

	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_WAIT:
			if(gCardPrcsTimer2ms) break;
			CardGotoRfTxOff();
			break;

		case CARDPRCS_DESFIRE_READ_FILE00_SEND:
			GotoMifairDesfireReadFile00();
			gCardPrcsTimer2ms = 100; 
			gCardProcessStep = CARDPRCS_DESFIRE_READ_FILE00_RSP;
			break;

		case CARDPRCS_DESFIRE_READ_FILE00_RSP:
			if(!gCardPrcsTimer2ms)
			{
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS)
			{
				//SIOGotoCardApiDesfireReadFile00Step2();//sjc_RC523 add 주석
				gCardPrcsTimer2ms = 100; // todo: Check timing
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;
			}
			else
			{
				CardGotoRfTxOff();
			}
			break;

		default:
			break;
	}
}

void CardModeDesfireReadFile01(void)
{
	BYTE bTmp;

	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_WAIT:
			if(gCardPrcsTimer2ms) break;
			CardGotoRfTxOff();
			break;

		case CARDPRCS_DESFIRE_READ_FILE01_SEND:
			GotoMifairDesfireReadFile01();
			gCardPrcsTimer2ms = 100; 
			gCardProcessStep = CARDPRCS_DESFIRE_READ_FILE01_RSP;
			break;

		case CARDPRCS_DESFIRE_READ_FILE01_RSP:
			if(!gCardPrcsTimer2ms)
			{
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS)
			{
				//SIOGotoCardApiDesfireReadFile01Step2();//sjc_RC523 add 주석
				gCardPrcsTimer2ms = 100; // todo: Check timing
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;
			}
			else
			{
				CardGotoRfTxOff();
			}
			break;

		default:
			break;
	}
}

void CardModeDesfireWriteFile01Step1(void)
{
	BYTE bTmp;

	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_WAIT:
			if(gCardPrcsTimer2ms) break;
			CardGotoRfTxOff();
			break;

		case CARDPRCS_DESFIRE_WRITE_FILE01_STEP1_SEND:
			GotoMifairDesfireWriteFile01Step1();
			gCardPrcsTimer2ms = 100; 
			gCardProcessStep = CARDPRCS_DESFIRE_WRITE_FILE01_STEP1_RSP;
			break;

		case CARDPRCS_DESFIRE_WRITE_FILE01_STEP1_RSP:
			if(!gCardPrcsTimer2ms)
			{
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS)
			{
				//SIOGotoCardApiDesfireWriteFile01Step2();//sjc_RC523 add 주석
				gCardPrcsTimer2ms = 100; // todo: Check timing
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;
			}
			else
			{
				CardGotoRfTxOff();
			}
			break;

		default:
			break;
	}
}

void CardModeDesfireWriteFile01Step2(void)
{
	BYTE bTmp;

	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_WAIT:
			if(gCardPrcsTimer2ms) break;
			CardGotoRfTxOff();
			break;

		case CARDPRCS_DESFIRE_WRITE_FILE01_STEP2_SEND:
			GotoMifairDesfireWriteFile01Step2();
			gCardPrcsTimer2ms = 100; 
			gCardProcessStep = CARDPRCS_DESFIRE_WRITE_FILE01_STEP2_RSP;
			break;

		case CARDPRCS_DESFIRE_WRITE_FILE01_STEP2_RSP:
			if(!gCardPrcsTimer2ms)
			{
				CardGotoRfTxOff();
				break;
			}
			bTmp = CardReceiveCheck();
			if(bTmp==0) break;
			else if(bTmp == MIFAIR_SUCESS)
			{
				//SIOGotoCardApiDesfireWriteFile01Step3();//sjc_RC523 add 주석
				gCardProcessStep = CARDPRCS_MIFARE_WAIT;
			}
			else
			{
				CardGotoRfTxOff();
			}
			break;

		default:
			break;
	}
}

#if 0
//------------------------------------------------------------------------------
/** 	@brief	Card Mode - Card Read Result Process 
	@param 	None
	@return 	None
	@remark 카드 읽기 결과 처리
	@remark Mifare Classic 카드만 처리
	@remark 카드 종류 및 필요한 처리 내용에 따라 추가 가능
*/
//------------------------------------------------------------------------------
void CardModeReadResult(void)
{
	BYTE bTmp, bCnt;
	
	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_PROCESS:
//			gCardReadStatus = CARDREAD_SUCCESS;
//			CardGotoAwayFromReaderCheck();
			gCardProcessStep = CARDPRCS_MIFARE_KEYA_SEND;
			break;
			
		case CARDPRCS_MIFARE_KEYA_SEND:
			CardKeyAMake(1, gCardOneUid);
			Authentication_Send(1, gCardOneUid);
			gCardPrcsTimer2ms = 5;  		// max 10ms
			gCardProcessStep = CARDPRCS_MIFARE_KEYA_RESPOND;
			break;

		case CARDPRCS_MIFARE_KEYA_RESPOND:
			bTmp = Authentication_Respond();
			if(bTmp != 1){
				if(gCardPrcsTimer2ms)	break;
				CardGotoRfTxOff();
				break;
			}
			gReadBlock = 1;
			gCardProcessStep = CARDPRCS_MIFARE_READ_BLOCK_SEND;			
			break;

		case CARDPRCS_MIFARE_READ_BLOCK_SEND:
			ReadBlockData_Send(gReadBlock);
			gCardPrcsTimer2ms = 5;  		// max 10ms			
			gCardProcessStep = CARDPRCS_MIFARE_READ_BLOCK_RESPONSE;	
			break;

		case CARDPRCS_MIFARE_READ_BLOCK_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gCardPrcsTimer2ms)	break;
				CardGotoRfTxOff();
				break;				
			}		
			for(bCnt = 0; bCnt < 16; bCnt++){
				gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
			}

			if(gReadBlock == 1){				// Key Random Data Get
				CardSaveKeyARdmData(gRfidFifoDataBuf);
				gReadBlock = 2;
				gCardProcessStep = CARDPRCS_MIFARE_READ_BLOCK_SEND;
			}
			else if(gReadBlock == 2){

				glCardRegNum[0] = gRfidFifoDataBuf[0];
		        	glCardRegNum[1] = gRfidFifoDataBuf[1];

				if((glCardRegNum[0] < 1) || (glCardRegNum[0] > (MAX_REG_CARD_NUM*2))){	//v12 추가
					glCardRegNum[0] = 1;
				}	
				if((gbMainMode == MODE_CARD_REGISTER)||(gbMainMode == MODE_CARD_REGISTER_BY_BLE)){
					gReadBlock = 2;
					gCardProcessStep = CARDPRCS_MIFARE_WRITE_BLOCK_START_SEND;
				}
				else{
					CardGotoRfTxOff();
//					gCardProcessStep = CARDPRCS_MIFARE_READ_BLOCK_DATA_SEND;
				}
			}
			break;

		case CARDPRCS_MIFARE_WRITE_BLOCK_START_SEND:
			if((gbMainMode == MODE_CARD_REGISTER)||(gbMainMode == MODE_CARD_REGISTER_BY_BLE)){
				if(glCardRegNum[1] == 0){
//					gbCardMode =_CARD_MODE_READREGDATA;
					break;
				}		
				glCardRegNum[0]++;
				if(glCardRegNum[0] > (MAX_REG_CARD_NUM*2))
				{
					glCardRegNum[0] = 1;
				}
				glCardRegNum[1] = 0;
				WriteBlock_Start_Send(gReadBlock);
				gCardPrcsTimer2ms = 5;  		// max 10ms	
				gCardProcessStep = CARDPRCS_MIFARE_WRITE_BLOCK_DATA_SEND;
			}
			break;

		case CARDPRCS_MIFARE_WRITE_BLOCK_DATA_SEND:
//			if(gCardPrcsTimer2ms)	break;

//			if(IrqCheck() != IRQ_TX_COMPLETED)
			bTmp = IrqCheck();
			if(bTmp== IRQ_NONE)
			{
				if(gCardPrcsTimer2ms)	break;
				CardGotoRfTxOff();
				break;				
			}		
			
			for(bCnt = 0; bCnt < 16; bCnt++)
			{
				gbCardData[bCnt] = 0x00;
			}
			gbCardData[0] = glCardRegNum[0];
			gbCardData[1] = glCardRegNum[1];
			
			gbCardData[2] = DataChecksumCal(gbCardData, 2);	

			WriteBlock_Data_Send(gbCardData);
			gCardPrcsTimer2ms = 10;  		// max 20ms	
			gCardProcessStep = CARDPRCS_MIFARE_WRITE_BLOCK_DATA_RESPONSE;
			break;
			
		case CARDPRCS_MIFARE_WRITE_BLOCK_DATA_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gCardPrcsTimer2ms)	break;
				CardGotoRfTxOff();
				break;				
			}		
			if(WriteBlock_Data_Response() != 1){
				CardGotoRfTxOff();
				break;
			}
			gReadBlock = 2;
			gCardProcessStep = CARDPRCS_MIFARE_READ_BLOCK_DATA_SEND;
			break;

		case CARDPRCS_MIFARE_READ_BLOCK_DATA_SEND:
			ReadBlockData_Send(gReadBlock);
			gCardPrcsTimer2ms = 5;  		// max 10ms	
			gCardProcessStep = CARDPRCS_MIFARE_READ_BLOCK_DATA_RESPONSE;
			break;

		case CARDPRCS_MIFARE_READ_BLOCK_DATA_RESPONSE:
			if(ReadBlockData_Response() != 1){
				gReadBlock = 2;
				gCardProcessStep = CARDPRCS_MIFARE_WRITE_BLOCK_START_SEND;
				break;
			}
			if(gbCardMode == CARDPRCS_MIFARE_AUTHENTICATION_RESPONSE)
			{
				gbCardData[0] = glCardRegNum[0];
				for(bCnt = 0; bCnt < 4; bCnt++)
				{
					gbCardData[1+bCnt] = gRfidFifoDataBuf[bCnt];
				}
				gbCardMode =0;	
				gCardReadStatus = CARDREAD_SUCCESS;
				CardGotoAwayFromReaderCheck();
				break;
			}
			bTmp = DataChecksumCal(gRfidFifoDataBuf, 2);
			if(bTmp != gRfidFifoDataBuf[2])
			{
				gReadBlock = 2;
				gCardProcessStep = CARDPRCS_MIFARE_WRITE_BLOCK_START_SEND;
				break;
			}
			
			if((gRfidFifoDataBuf[0] != gbCardData[0]) || (gRfidFifoDataBuf[1] != gbCardData[1]))
			{
				CardGotoRfTxOff();
				break;
			}
			
//			gbCardMode =_CARD_MODE_READREGDATA;
			gCardProcessStep = CARDPRCS_MIFARE_AUTHENTICATION_SEND;
			break;

		case CARDPRCS_MIFARE_AUTHENTICATION_SEND:
			if((glCardRegNum[0] < 1) || (glCardRegNum[0] > (MAX_REG_CARD_NUM*2))){	//v12 추가
				CardGotoRfTxOff();
				break;
			}	

			glCardBlock = glCardRegNum[0];
			if(glCardBlock > MAX_REG_CARD_NUM)
			{
				glCardBlock -= MAX_REG_CARD_NUM;
			}
			glCardBlock = glCardBlock*4;

			CardKeyAMake(glCardBlock, gCardOneUid);

			Authentication_Send(glCardBlock, gCardOneUid);
			gCardPrcsTimer2ms = 5;  		// max 10ms
			gCardProcessStep = CARDPRCS_MIFARE_AUTHENTICATION_RESPONSE;
			break;

		case CARDPRCS_MIFARE_AUTHENTICATION_RESPONSE:
			bTmp = Authentication_Respond();
			if(bTmp != 1){
				if(gCardPrcsTimer2ms)	break;
				CardGotoRfTxOff();
				break;
			}
			
			gReadBlock = glCardBlock;
			gCardProcessStep = CARDPRCS_MIFARE_READ_BLOCK_DATA_SEND;
			gbCardMode = CARDPRCS_MIFARE_AUTHENTICATION_RESPONSE;
			break;
/*			
			gbCardData[0] = glCardRegNum[0];
			for(bCnt = 0; bCnt < 4; bCnt++)
			{
				gbCardData[1+bCnt] = glSerBuffer[bCnt];
			}
			gbCardMode =_CARD_MODE_STANBY;			
*/			
			break;


		default:
			CardModeClear();
			break;
	}

}
#endif



//------------------------------------------------------------------------------
/** 	@brief	Card Mode - Card Off Check 
	@param 	None
	@return 	None
	@remark 입력된 카드의 떨어짐 감지를 위한 처리
	@remark 50ms 간격으로 REQA 전송
	@remark 5회 연속으로 응답이 없을 경우 떨어짐 처리 (50ms*5회 = 250ms)
*/
//------------------------------------------------------------------------------
void CardModeAwayFromReaderCheck(void)
{

	BYTE Dummy;

	switch(gCardProcessStep)
	{
		case CARDPRCS_AWAY_CHECK:
			if(gCardAwayTimer2ms)		break;
			gCardAwayTimer2ms = 25;

			RfidTxOn();
			gCardPrcsTimer2ms = 2;
			
			gCardProcessStep = CARDPRCS_AWAY_REQA_SEND;
			break;				

		case CARDPRCS_AWAY_REQA_SEND:
			if(gCardPrcsTimer2ms)		break;

			IrqClear();
			Iso14443aReqaSend(ISO14443A_WUPA);
			gCardPrcsTimer2ms = 5; 
			gCardProcessStep = CARDPRCS_AWAY_REQA_RESPOND;
			break;
			
		case CARDPRCS_AWAY_REQA_RESPOND:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gCardPrcsTimer2ms)	break;

				if(gCardCheckLoopCnt)	gCardCheckLoopCnt--;
			}
			else
			{
				if(Iso14443aReqaRespond(&Dummy) == STATUS_SUCCESS)
				{
					gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;
				}
				else
				{
					if(gCardCheckLoopCnt)	gCardCheckLoopCnt--;
				}
			}
			RfidTxOff();

			if(gCardCheckLoopCnt)
			{
				gCardProcessStep = CARDPRCS_AWAY_CHECK;
				break;
			}

			gCardReadStatus= CARDREAD_NO_CARD;
			CardGotoRfTxOff();
			break;			

		default:
			CardModeClear();
			break;
	}

}



void CardReadStatusClear(void)
{
	gCardReadStatus = 0;
}


BYTE GetCardReadStatus(void)
{
	return (gCardReadStatus);
}


BYTE GetCardInputNumber(void)
{
	return (gCardInputNumber);
}



void(*const CardModeProcess_Tbl[])(void) = {
	CardModeIdleState,
	CardModePowerOnOff,
	CardModeSelect,
	CardModeReadResult,
	CardModeAwayFromReaderCheck,

	/*CardModeRATS,
	CardModeDesfireSelectApp,
	CardModeDesfireAuth,
	CardModeDesfireReadFile00,
	CardModeDesfireReadFile01,
	CardModeDesfireWriteFile01Step1,
	CardModeDesfireWriteFile01Step2,*/
};


void CardModeProcess(void)
{
//	MifairProcess();
	CardModeProcess_Tbl[gCardMode]();
}

/*
void CardTxOn(void)
{
	RfidPowerOn();
//	Delay(SYS_TIMER_1MS);				// wait until system clock started
	RfidTxOn();
}
*/

///////////////////card detection function ///////////////////////////


static CardDetectionPrs cdMainPrs = CARD_DETECT_IDLE;
static BYTE cdSubPrs = 0;

CardDetectionPrs GetCardDetectionMode(void)
{
	return (cdMainPrs);	
}
static void CdModeChange( CardDetectionPrs mode )
{
	cdMainPrs = mode;
	cdSubPrs = 0;
}


#ifdef _USE_RFID_DETECTION_IREVO

DBYTE cdAdcValue[ CARD_DETECT_ADC_MAX ];
BYTE cdAdcIndex = 0;

WORD cdThValue = 0;
BYTE cdReCalCnt = 0;


static WORD CdAdcMaxMinSum( DBYTE* data, BYTE dataCnt )
{
	BYTE bCnt;
	DBYTE adcMax = 0;
	DBYTE adcMin = 0xFFFF;

	for(bCnt=0;bCnt<dataCnt;bCnt++)	
	{
		if( adcMax < data[bCnt] )
		{
			adcMax = data[bCnt];
	}
		if( adcMin > data[bCnt] )
		{
			adcMin = data[bCnt];
	}
	}
	return (adcMax + adcMin);

}

static float CdAdcConvertToVoltage( float adc, BYTE resoulution )
{
	float voltage;
	DBYTE maxAdc = 0;
	BYTE bCnt;

	for(bCnt=0;bCnt<resoulution;bCnt++)
	{
		maxAdc = maxAdc<<1;	
		maxAdc |= 0x0001;
	}

	voltage = (ADC_REFERENCE_VOLTAGE * adc)/(maxAdc+1);

	return voltage;
}

static float CdMakeAvgVoltage( DBYTE* data, BYTE dataCnt )
{
	float avrAdc = 0;
	BYTE bCnt;

	for(bCnt=0;bCnt<dataCnt;bCnt++)
	{
		avrAdc += data[bCnt];
	}
	if(dataCnt>2)		// 3개 이상인 경우 제일 작은 수, 제일 큰수 제외하고 평균값 구함. 
		{
		avrAdc = avrAdc - (float)CdAdcMaxMinSum(data, dataCnt);
		avrAdc = (avrAdc / (dataCnt - 2));			// the avrage value without max and min value.
		}
	else
	{
		avrAdc = avrAdc / dataCnt;
	}

	avrAdc = CdAdcConvertToVoltage(avrAdc, ADC_RESOULUTION);
	
	return avrAdc;
}

static bool IsAllowedModeForCardDetection(void)
{
	bool allowed = false;

	switch(gbMainMode)
	{
		case 0:
		//case MODE_ANTISHOPLIFTING:				//sjc_RC523 add 주석
		//case MODE_SEMI_SECURE_VERIFICATION:		//sjc_RC523 add 주석
			allowed = true;
			break;

		/*case MODE_FIRST_TIME_CONFIG:			//sjc_RC523 add 주석
			if(IsFirstTimeConfigCompleted() == false)	//sjc_RC523 add 주석
			{									//sjc_RC523 add 주석
				allowed = true;					//sjc_RC523 add 주석
			}*/									//sjc_RC523 add 주석
			break;

		default:
			allowed = false;
			break;
	}
	return allowed;
}

static void CardDetectionModeIdle( void )
{
	//  retun mode check
	if(gCardPrcsTimer2ms)	return;

	if(IsAllowedModeForCardDetection() == false)		return;
	if(gCardMode)			return;


	if( cdThValue == 0 )
	{
		cdReCalCnt = 0;
		gCardPrcsTimer2ms = 50;
		CdModeChange( CARD_DETECT_CALIBRATION );
	}
	else
	{
		CdModeChange( CARD_DETECT_TX_ON );			
		gCardPrcsTimer2ms = 250;
	}				
}


static void CardDetectionModeTxOn( void )
{
	DBYTE dByte;
	float voltage;

	switch( cdSubPrs )
	{
		case 0:
	RfidPowerOn();
	RfidTxOn();
//			Delay(SYS_TICK_100US);
			cdAdcIndex = 0;
			cdSubPrs++;
			break;

		case 1:
			
			while( cdAdcIndex < CARD_DETECT_ADC_MAX )
			{
				dByte = ADCCheck( ADC_RFID_CHECK );
				cdAdcValue[cdAdcIndex++] = dByte;
	}
	RfidPowerOff();
			cdSubPrs++;
			break;
		
		case 2:
			voltage = CdMakeAvgVoltage( cdAdcValue, CARD_DETECT_ADC_MAX );
			if( (voltage > (cdThValue + CARD_DETECT_TH))||(voltage < (cdThValue - CARD_DETECT_TH)) )		// 정상인식됨.
			{
				cdReCalCnt++;
				CdModeChange( CARD_DETECT_IDLE );
//				LedSetting(gcbLedOutLock1, 0);			// for testing
//				printf("detected!!\n");
//				CardAllUidBufferClear();
				CardCheckCountReset();	 //2017.05.23 moonsungwoo 
//				CardGotoAnticollisionStart();
				CardGotoReadStart();

				//sjc_RC523 add 주석 시작
				/*if(GetMainMode() == MODE_ANTISHOPLIFTING)
				{
					ModeGotoAntishopliftingPrepareCardDetection();
				}
				else if(GetMainMode() == MODE_FIRST_TIME_CONFIG)
				{
					ModeGotoFirstTimeConfigCardVerification();
				}
				else
				{*/
				//sjc_RC523 add 주석 끝
					ModeGotoCardVerify();
				//}	//sjc_RC523 add 주석
			}
			else
			{
				CdModeChange( CARD_DETECT_IDLE );
				cdReCalCnt = 0;
		}

			if( cdReCalCnt >= CD_CALIBRATION_MAX)	// re calibration.
			{
				cdThValue = 0;
				cdReCalCnt = 0;
				CdModeChange( CARD_DETECT_CALIBRATION );
		}
			break;
	}
}
static void CardDetectionCalibration( void )
{
	DBYTE dByte;
	float voltage;

	switch( cdSubPrs )
	{
		case 0:
			if(gCardPrcsTimer2ms) break;
			RfidPowerOn();
			RfidTxOn();
//			Delay(SYS_TICK_100US);
			cdAdcIndex = 0;
			cdSubPrs++;
			break;

		case 1:
			while( cdAdcIndex < CARD_DETECT_ADC_MAX )
			{
				dByte = ADCCheck( ADC_RFID_CHECK );
				cdAdcValue[cdAdcIndex++] = dByte;
	}
			cdSubPrs++;
			break;
		
		case 2:
			voltage = CdMakeAvgVoltage( cdAdcValue, CARD_DETECT_ADC_MAX );
			

			if( CD_CALIBRATION_MAX > cdReCalCnt )
			{
				cdThValue += voltage;
				cdReCalCnt++;
				gCardPrcsTimer2ms = 50;
				cdSubPrs = 0;
	}
			else
			{
				cdThValue = cdThValue/CD_CALIBRATION_MAX;
				CdModeChange( CARD_DETECT_IDLE );
	}
			break;
		}
}




#else
static void CardDetectionModeIdle( void )
{
	//  retun mode check
	if(gCardPrcsTimer2ms)	return;

	if(GetMainMode())		return;
	if(gCardMode)			return;

	CdModeChange( CARD_DETECT_TX_ON );			
	gCardPrcsTimer2ms = 250;

}

static void CardDetectionModeTxOn( void )
{
	// KC 인증을 위한 카드 출력 ON 처리
#ifdef	DDL_TEST_SET_FOR_CARD
	if(IsCardPowerOnForTest() == true)		return;
#endif

	if(Iso14443aFindTag(&gCardUidLength))
	{
		CardAllUidBufferClear();
		CardCheckCountReset();	 //2017.05.23 moonsungwoo 
		CardGotoAnticollisionStart();
		ModeGotoCardVerify();
		LedSetting(gcbLedOutLock1, 0);			// for testing
	}
	CdModeChange( CARD_DETECT_IDLE );	

}

static void CardDetectionCalibration( void )
{
	CdModeChange( CARD_DETECT_IDLE );	
}
#endif

void(*const CardDetectModeProcess_Tbl[])( void  ) = {
	CardDetectionModeIdle, 
	CardDetectionModeTxOn, 
	CardDetectionCalibration
};

void CardDetectionProcess( void )
{
	//CardDetectModeProcess_Tbl[cdMainPrs]();
#ifndef	_KEYPAD_WAKEUP_FOR_CARD_USE
	// 3분락 수행 중일 경우 MainMode 처리하지 않음
	if(GetTamperProofPrcsStep())	return;

	if(GetMainMode())		return;
	if(gCardMode)			return;
	if(PowerDownModeForJig() == STATUS_SUCCESS) return;
	

	if(gCardPrcsTimer2ms)	return;
	gCardPrcsTimer2ms = 250;


	// KC 인증을 위한 카드 출력 ON 처리
#ifdef	DDL_TEST_SET_FOR_CARD
	if(IsCardPowerOnForTest() == true)		return;
#endif

#ifdef	FELICA_CARD_SUPPORTED
	// Falica 카드를 지원할 경우 Felica 카드 먼저 확인
	CardAllUidBufferClear();

	if(FelicaFindTag())
	{
		CopyFelicaCardUid(gCardOneUid);
		gCardUidLength = 8;

		if(TempSaveCompletedUID() == UID_AVAILABLE)
		{
			SetInputCardType(CARD_TYPE_FELICA);

			CardGotoReadResult(CARDPRCS_MIFARE_PROCESS);
			ModeGotoCardVerify();
		}
		return;
	}

#endif

	if(Iso14443aFindTag(&gCardUidLength))
	{
#ifdef	FELICA_CARD_SUPPORTED
		SetInputCardType(CARD_TYPE_MIFARE);
#endif

		CardAllUidBufferClear();
		CardCheckCountReset();   //2017.05.23 moonsungwoo 
		CardGotoAnticollisionStart();
		ModeGotoCardVerify();
	}
#endif			// _KEYPAD_WAKEUP_FOR_CARD_USE
}

//sjc_RC523 add start

void UpdateCardUidToMemoryByModule(BYTE* pCardUid, BYTE SlotNumber)
{
	if(gbManageMode == _AD_MODE_SET)
	{
		// Slot Numebr에 대해 한 번 더 확실하게 하기 위해
		if( (SlotNumber < 1) || (SlotNumber > SUPPORTED_USERCARD_NUMBER) )
		{
			return;
		}
#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(pCardUid,8);
#endif 	
		RomWrite(pCardUid, CARD_UID+(((WORD)SlotNumber-1)*MAX_CARD_UID_SIZE), MAX_CARD_UID_SIZE);
	}
}

BYTE DataChecksumCal(BYTE *bpData, BYTE bNum)
{
	BYTE bSum;
	BYTE bCnt;
	
	bSum = *bpData;
	
	for(bCnt = 1; bCnt < bNum; bCnt++)
	{
		bpData++;
		bSum ^= *bpData;	
	}
	bSum = bSum + 7;
	
	return bSum;
}



