//------------------------------------------------------------------------------
/** 	\file		VolumeControl_T1.h
	@date	2016.03.31
	@brief	Programmable Doorlock Volume Control Header
*/
//------------------------------------------------------------------------------


#ifndef __I2CIOEXPANDERCONTROL_T1_INCLUDED
#define __I2CIOEXPANDERCONTROL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


#define	I2C_IOEXPANDEROUTPUT_ADDRESS		0x40
#define	I2C_IOEXPANDERINPUT_ADDRESS		0x42

void RegisterExpanderOutput( ddl_i2c_t *h_i2c_dev );
void RegisterExpanderInput( ddl_i2c_t *h_i2c_dev );
void	IoExpanderOutPutInit( void );
void	IoExpanderInPutInit( void );



extern BYTE Test_value;
void IoExpanderOutPutControl(void);
void IoExpanderInPutRead(void);

extern FLAG ExpanderOutPortVal;
#define	gbExpanderOutPortVal		ExpanderOutPortVal.STATEFLAGDATA
#define	gbExpanderOutmpwren2	ExpanderOutPortVal.STATEFLAG.Bit0
#define	gbExpanderOutmclose2		ExpanderOutPortVal.STATEFLAG.Bit1
#define	gbExpanderOutmopen2		ExpanderOutPortVal.STATEFLAG.Bit2
#define	gbExpanderOutmsleep2		ExpanderOutPortVal.STATEFLAG.Bit3
#define	gbExpanderOutmainledr	ExpanderOutPortVal.STATEFLAG.Bit4
#define	gbExpanderOutmainledg	ExpanderOutPortVal.STATEFLAG.Bit5
#define	gbExpanderOutmainledb	ExpanderOutPortVal.STATEFLAG.Bit6
#define	gbExpanderOutsnsen2		ExpanderOutPortVal.STATEFLAG.Bit7

extern BYTE gbKeyInitialCnt;

extern FLAG ExpanderPortVal;
#define	gbExpanderPortVal		ExpanderPortVal.STATEFLAGDATA
#define	gfExpandercluch2		ExpanderPortVal.STATEFLAG.Bit0
#define	gfExpanderopensw		ExpanderPortVal.STATEFLAG.Bit1
#define	gfExpanderregsw		ExpanderPortVal.STATEFLAG.Bit2
#define	gfExpanderedge		ExpanderPortVal.STATEFLAG.Bit3




#endif
