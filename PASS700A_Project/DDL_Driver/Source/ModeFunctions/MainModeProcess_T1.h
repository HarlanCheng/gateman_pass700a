//------------------------------------------------------------------------------
/** 	@file		MainModeProcess-T1.h
	@brief	Main UI Process 
*/
//------------------------------------------------------------------------------

#ifndef __MAINMODEPROCESS_T1_INCLUDED
#define __MAINMODEPROCESS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"
 
#include "ModeKeyInputCheck_T1.h"
#include "ModeOpenCloseProcess_T1.h"
#include "ModePincodeVerify_T1.h"
#include "ModePincodeRegister_T1.h"
#ifdef	DDL_CFG_RFID
#include "ModeCardRegister_T1.h"
#include "ModeCardVerify_T1.h"
#endif

#ifdef	DDL_CFG_FP_TCS4K
#include "ModeFingerRegister_T1.h"
#include "ModeFingerPTOpen_T1.h"
#include "ModeFingerVerify_T1.h"
#include "ModeFingerDelete_T1.h"
#include "ModeMenuOneFingerRegDel_T1.h"
#elif defined	(DDL_CFG_FP_PFM_3000) || defined (DDL_CFG_FP_TS1071M)   //hyojoon_20160831 PFM-3000 add
#include "ModeFingerRegister_T2.h"
#include "ModeFingerVerify_T2.h"
#include "ModeFingerDelete_T2.h"
#include "ModeMenuOneFingerRegDel_T2.h"
#elif defined	 (DDL_CFG_FP_HFPM)
#include "ModeFingerRegister_T3.h"
#include "ModeFingerVerify_T3.h"
#include "ModeFingerDelete_T3.h"
#include "ModeMenuOneFingerRegDel_T3.h"
#elif defined	 (DDL_CFG_FP_INTEGRATED_FPM)
#include "ModeFingerRegister_T4.h"
#include "ModeFingerVerify_T4.h"
#include "ModeFingerDelete_T4.h"
#include "ModeMenuOneFingerRegDel_T4.h"
#endif


#ifdef	DDL_CFG_IBUTTON
#include "ModeGMTouchKeyRegister_T1.h"
#include "ModeGMTouchKeyVerify_T1.h"
#endif

#if	defined (DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
#include "ModeDS1972TouchKeyRegister_T1.h"
#include "ModeDS1972TouchKeyVerify_T1.h"
#endif


#include "ModeMenuMainSelect_T1.h"
#include "ModeMenuCredentialSelect_T1.h"


#include "ModeAlarmClearByKey_T1.h"


#include "ModePackLockOperation_T1.h"


#ifdef	_MASTERKEY_IBUTTON_SUPPORT
#include "ModeSpecialModeMasterKey_T1.h"
#endif


#ifdef	_MASTER_CARD_SUPPORT
#include "ModeSpecialModeMasterCard_T1.h"
#endif

#include "DebugFunctions_T1.h"


#define	 _MODE_TIME_OUT_7S			70
#define	 _MODE_TIME_OUT_20S			200
#define	 _MODE_TIME_OUT_60S			600

#define	 _MODE_TIME_OUT_DEFAULT		(_MODE_TIME_OUT_20S)



extern BYTE gbMainMode;															 
extern BYTE gbModePrcsStep;	

void ModeTimeOutCounter(void);
void ModeProcessTimeCounter(void);

void SetModeTimeOut(WORD ModeTimeout);
WORD GetModeTimeOut(void);
void SetModeProcessTime(WORD PrcsTime);
WORD GetModeProcessTime(void);

void TimeExpiredCheck(void);

BYTE GetMainMode(void);

void ModeClear(void);
void MainModeProcess(void);


enum{
	MODE_OPEN_CLOSE = 1,								//	ModeOpenClose,											
	MODE_TENEKY_WAKEUP_CHECK,						//	KeyTenKeyWakeUpProcess,
	MODE_REGISTER_CHECK,								//	ModeRegisterCheck,														   
	MODE_MENU_PIN_REGISTER, 							//	ModePINRegister,								  
	MODE_PIN_VERIFY,									//	ModePINVerify,						

#ifdef 	DDL_CFG_RFID
	MODE_CARD_REGISTER, 								//	ModeCardRegister,								   
	MODE_CARD_VERIFY,									//	ModeCardVerify, 														   
#endif

#ifdef 	DDL_CFG_FP_TCS4K
	MODE_FINGER_VERIFY,								//	ModeFingerVerify, 														   
	MODE_MENU_FINGER_REGISTER, 						//	ModeFingerRegister,								   
	MODE_MENU_FINGER_DELETE,							//	ModeAllFingerDelete, 								   
	MODE_FINGER_PT_OPEN,								//	ModeFingerPTOpen

	MODE_MENU_ONEFINGER_REGDEL,						//	ModeOneFingerRegDelCheck
	MODE_MENU_ONEFINGER_REGISTER, 						//	ModeOneFingerRegister,							   
	MODE_MENU_ONEFINGER_DELETE,							//	ModeOneFingerDelete,								   
#elif defined	(DDL_CFG_FP_PFM_3000)  || defined	(DDL_CFG_FP_TS1071M) ||  defined	 (DDL_CFG_FP_HFPM) || defined (DDL_CFG_FP_INTEGRATED_FPM)  //hyojoon_20160831 PFM-3000 add
	MODE_FINGER_VERIFY, 							//	ModeFingerVerify,														   
	MODE_MENU_FINGER_REGISTER,						//	ModeFingerRegister, 							   
	MODE_MENU_FINGER_DELETE,							//	ModeAllFingerDelete,								   

	MODE_MENU_ONEFINGER_REGDEL, 					//	ModeOneFingerRegDelCheck
	MODE_MENU_ONEFINGER_REGISTER,						//	ModeOneFingerRegister,							   
	MODE_MENU_ONEFINGER_DELETE, 						//	ModeOneFingerDelete,								   
#endif

#ifdef 	DDL_CFG_IBUTTON
	MODE_GM_TOUCHKEY_REGISTER,							// 	ModeGMTouchKeyRegister,
	MODE_GM_TOUCHKEY_VERIFY,							//	ModeGMTouchKeyVerify,
#endif

#if defined	(DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
	MODE_DS1972_TOUCHKEY_REGISTER,						// ModeDS1972TouchKeyRegister,
	MODE_DS1972_TOUCHKEY_VERIFY,						// ModeDS1972TouchKeyVerify,
#endif

	MODE_MENU_MAIN_SELECT, 								//	ModeMenuMainSelect,																																																																													 

	MODE_MENU_CREDENTIAL_SELECT, 								//	ModeMenuCredentialSelect,																																																																													 

	MODE_MENU_LOCK_SETTING,								//	ModeMenuLockSetting,									   

#ifdef	LOCKSET_HANDING_LOCK
	MODE_MENU_HANDING_LOCK_SET,							//	ModeHandingLockSetting
#endif

#ifdef	LOCKSET_AUTO_RELOCK
	MODE_MENU_AUTOLOCK_SET,								//	ModeAutoLockSetting
#endif

#ifdef	LOCKSET_VOLUME_SET
	MODE_MENU_VOLUME_SET,								//	ModeVolumeSetting
#endif

#ifdef	DDL_CFG_TOUCH_OC
	MODE_MENU_SAFE_OC_SET,								//	ModeSafeOCButtonSetting
#endif

	MODE_MENU_VISITORCODE_REGDEL,						//	ModeVisitorCodeRegDelCheck,
	MODE_MENU_VISITORCODE_REGISTER, 					//	ModeVisitorCodeRegister,							 
	MODE_MENU_VISITORCODE_DELETE,						//	ModeVisitorCodeDelete,				   

	MODE_MENU_ONETIMECODE_REGDEL,						//	ModeOnetimeCodeRegDelCheck
	MODE_MENU_ONETIMECODE_REGISTER, 					//	ModeOnetimeCodeRegister,							 
	MODE_MENU_ONETIMECODE_DELETE,						//	ModeOnetimeCodeDelete,				   

#ifdef 	DDL_CFG_RFID
	MODE_MENU_ALLCARD_DELETE,							//	ModeAllCardDelete

	MODE_MENU_ONECARD_REGDEL,							//	ModeCardRegDelCheck,
	MODE_MENU_ONECARD_REGISTER, 						//	ModeOneCardRegister,							   
	MODE_MENU_ONECARD_DELETE,							//	ModeOneCardDelete,								   
#endif


#ifdef 	DDL_CFG_IBUTTON
	MODE_MENU_ALLGMTOUCHKEY_DELETE,						//	ModeAllGMTouchKeyDelete,

	MODE_MENU_ONEGMTOUCHKEY_REGDEL,						//	ModeGMTouchKeyRegDelCheck,
	MODE_MENU_ONEGMTOUCHKEY_REGISTER, 					//	ModeOneGMTouchKeyRegister,							   
	MODE_MENU_ONEGMTOUCHKEY_DELETE,						//	ModeOneGMTouchKeyDelete,								   
#endif

#ifdef 	DDL_CFG_DS1972_IBUTTON
	MODE_MENU_ALLDS1972TOUCHKEY_DELETE, 					//	ModeAllDS1972TouchKeyDelete,

	MODE_MENU_ONEDS1972TOUCHKEY_REGDEL, 					//	ModeDS1972TouchKeyRegDelCheck,
	MODE_MENU_ONEDS1972TOUCHKEY_REGISTER,					//	ModeOneDS1972TouchKeyRegister,							   
	MODE_MENU_ONEDS1972TOUCHKEY_DELETE, 					//	ModeOneDS1972TouchKeyDelete,								   
#endif 

	MODE_MENU_REMOCON_REGDEL,							//	ModeRemoconRegDelCheck,
	MODE_MENU_REMOCON_REGISTER, 						//	ModeRemoconRegister,							 
	MODE_MENU_REMOCON_DELETE,							//	ModeRemoconDelete,				   

#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
	MODE_MENU_SUBREMOCON_REGDEL,							//	ModeSubRemoconRegDelCheck,
	MODE_MENU_SUBREMOCON_REGISTER, 						//	ModeSubRemoconRegister,							 
	MODE_MENU_SUBREMOCON_DELETE,							//	ModeSubRemoconDelete,	
#endif 	

#ifdef DDL_CFG_ADD_REMOCON_LINK_FUNCTION
	MODE_MENU_LOCKOPEN_CONNECTION_REGISTER, 			//	ModeLockOpenConnectionRegister, 			   
#endif
	
	MODE_MENU_ONEREMOCON_REGDEL,						//	ModeOneRemoconRegDelCheck,
	MODE_MENU_ONEREMOCON_REGISTER, 						//	ModeOneRemoconRegister,							 
	MODE_MENU_ONEREMOCON_DELETE,						//	ModeOneRemoconDelete,				   

#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
	MODE_MENU_ONESUBREMOCON_REGDEL,						//	ModeOneSubRemoconRegDelCheck,
	MODE_MENU_ONESUBREMOCON_REGISTER, 						//	ModeOneSubRemoconRegister,							 
	MODE_MENU_ONESUBREMOCON_DELETE,							// ModeOneSubRemoconDelete
#endif 	

	MODE_MENU_RFLINKMODULE_REGDEL,						//	ModeRfLinkModuleRegDelCheck,
	MODE_MENU_RFLINKMODULE_REGISTER, 					//	ModeRfLinkModuleRegister,							 
	MODE_MENU_RFLINKMODULE_DELETE,						//	ModeRfLinkModuleDelete,				   


	MODE_MENU_ADVANCED_MODE_SET,						//	ModeAdvancedModeSet,									   
	MODE_MENU_NORMAL_MODE_SET,							//	ModeNormalModeSet,		


	MODE_MENU_MASTERCODE_REGISTER,						//	ModeMasterCodeRegister, 						   

	MODE_MENU_ONEUSERCODE_REGDEL,						//	ModeOneUserCodeRegDelCheck
	MODE_MENU_ONEUSERCODE_REGISTER, 					//	ModeOneUserCodeRegister,								 
	MODE_MENU_ONEUSERCODE_DELETE,						//	ModeOneUserCodeDelete,							   

	MODE_MENU_ALLCREDENTIALS_DELETE, 					//	ModeAllCredentialsDelete,			   


	MODE_ALARM_CLR_BY_KEY,								// 	ModeAlarmClearByKey


	MODE_PACK_LOCK_OPERATION,						//	ModePackLockOperation


	MODE_TEST_MENU, 									//	ModeTestMenu,				   
	MODE_TEST_AGING,									//	ModeTestAging,									   
#ifdef 	DDL_CFG_RFID	
	MODE_TEST_OPEN_UID_SET,							//	ModeTestOpenUidSet,,
#endif 	

#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
	MODE_EXCHANGE_KEY_SET,							//	ModeExchangeKeySet,,
#endif 

#ifdef	LOCKSET_LANGUAGE_SET
	MODE_MENU_LANGUAGE_SET, 						//	ModeLanguageSetting,
#endif

#ifdef	LOCK_TYPE_DEADBOLT
	MODE_MENU_HANDING_LCOK,
#endif


#ifdef	BLE_N_SUPPORT
#ifdef 	DDL_CFG_RFID
	MODE_CARD_REGISTER_BYBLEN,						// ModeCardRegisterByBleN
	MODE_MENU_ONECARD_REGISTER_BYBLEN,				// ModeOneCardRegisterByBleN	
#endif

#if defined	(DDL_CFG_FP_TCS4K) || defined	(DDL_CFG_FP_PFM_3000)  || defined	(DDL_CFG_FP_TS1071M) //hyojoon_20160831 PFM-3000 add
	MODE_MENU_FINGER_REGISTER_BYBLEN,				// 	ModeFingerRegisterByBleN
	MODE_MENU_FINGER_DELETE_BYBLEN,					//	ModeAllFingerDeleteByBleN, 								   
	MODE_MENU_ONEFINGER_REGISTER_BYBLEN,			// 	ModeOneFingerRegisterByBleN
	MODE_MENU_ONEFINGER_DELETE_BYBLEN,				//	ModeOneFingerDeleteByBleN,								   
#endif
	
#if defined	(DDL_CFG_DS1972_IBUTTON)
	MODE_DS1972_TOUCHKEY_REGISTER_BYBLEN,				// ModeDS1972TouchKeyRegisterByBleN
	MODE_ONEDS1972_TOUCHKEY_REGISTER_BYBLEN,			// ModeOneDS1972TouchKeyRegisterByBleN	
#endif 	

	MODE_MENU_REMOCON_REGISTER_BYBLEN,				//	ModeRemoconRegisterByBleN,
	MODE_MENU_REMOCON_DELETE_BYBLEN,				//	ModeRemoconDeleteByBleN,				   

	MODE_MENU_ONEREMOCON_REGISTER_BYBLEN,			//	ModeOneRemoconRegisterByBleN, 						 
	MODE_MENU_ONEREMOCON_DELETE_BYBLEN,				//	ModeOneRemoconDeleteByBleN,				   


#ifdef 	DDL_CFG_IBUTTON
	MODE_GMTOUCHKEY_REGISTER_BYBLEN,
	MODE_MENU_ONEGMTOUCHKEY_REGISTER_BYBLEN,
#endif
	MODE_TEST_BLE_N,									//	ModeTestBLEN,,
#endif	//BLE_N_SUPPORT

#ifdef	DDL_AAAU_AUTOLOCK_HOLD //호주 및 뉴질랜드 전용 autolock hold 기능
	MODE_OPERATION_SELECT,						//ModeOperationSelect
#endif	//DDL_AAAU_AUTOLOCK_HOLD #endif


#if defined (DDL_CFG_MS)
	MS_MODE_MENU_CARD_CODE_REGDEL,						//	MS_ModeCardCodeRegDelCheck
	MS_MODE_MENU_AUTHORITY_VERIFY,						//	MS_ModeAuthorityVerify
	MS_MODE_MENU_CREDENTIAL_INFO_SET,					//	MS_ModeMenuCredentialInfoSet
#endif 

#if defined (_MASTER_CARD_SUPPORT)
	MODE_MASTER_CARD_REGISTER, 							//	ModeMasterCardRegister,								   
#endif

	MODE_ABNORMAL_PROCESS,									// ModeAbnormalProcess,

	MODE_PACK_SET_LED_PROCESS,									// ModePackSetLedProcess,

	MODE_ALL_DATA_RESET, 									//	ModeAllDataReset			   

#ifdef P_SW_FACTORY_BROKEN_T
	MODEWAITFACTORYRESETCOMPLETE, 						//ModeWaitFactoryResetComplete
#endif 	

#ifdef	CYLINDER_KEY_ALARM//실린더 알람 설정 메뉴 단계 추가.
	MODE_MENU_CYLINDERKEY_ALARM_SET,		//ModeCylinderKeyAlarmSetting
#endif

#if defined (_USE_LOGGING_MODE_)
	MODE_LOGGING_PROCESS,									//	ModeLoggingProcess,
	MODE_MENU_TIME_SET,									//	ModeMenuTimeSet,	
#endif 
	MODE_TEST_GET_CENTER,									//	ModeTestGetCenter,
#if defined (FACOTRY_PINCODE_TEST_MODE)
	MODE_PINCODE_TEST_MODE,									//	ModePincoeTestMode,
#endif 
/*
	MODE_MENU_LANGUAGE_SET, 						//	ModeLanguageSetting,
	MODE_MENU_RFLINK_REG_DEL,						//	ModeRfLinkModuleRegDel, 						   
	MODE_TAMPER_PROOF_LOCKOUT,						//	ModeTamperProofLockout, 							 
	MODE_TEST_VOICE, 
	MODE_TEST_PACK_SOFTRESET,						//	ModeTestPackSoftReset
*/
};	



extern BYTE gbManageMode;

#define	_AD_MODE_SET		0xAA


#if	defined (DDL_CFG_ADVANCED_DEFAULT) || defined (DDL_CFG_ADVANCED_ONLY)
#define	LOCK_WORKINGMODE_DEFAULT	_AD_MODE_SET	// Advanced Mode
#else
#define	LOCK_WORKINGMODE_DEFAULT	0				// Normal Mode
#endif


#endif


