//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneGMTouchKeyRegDel_T1.c
	@version 0.1.00
	@date	2016.05.24
	@brief	GateMan Touch Key Individual Register/Delete Mode
	@remark	터치키 개별 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@see	ModeMenuOneUserCodeRegDel_T1.c
	@see	ModeGMTouchKeyVerify_T1.h"
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.24		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gOneGMTouchKeyUidBuf[MAX_KEY_UID_SIZE];


void ModeGotoOneGMTouchKeyRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONEGMTOUCHKEY_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoOneGMTouchKeyRegister(void)
{
	gbMainMode = MODE_MENU_ONEGMTOUCHKEY_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoOneGMTouchKeyDelete(void)
{
	gbMainMode = MODE_MENU_ONEGMTOUCHKEY_DELETE;
	gbModePrcsStep = 0;
}


void OneGMTouchKeyModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 카드 개별 등록
		case 1: 	
			ModeGotoOneGMTouchKeyRegister();
			break;
	
		// 카드 개별 삭제
		case 3: 		
			ModeGotoOneGMTouchKeyDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuOneGMTouchKeyModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 카드 개별 등록
		case TENKEY_1:			
			//적당한 음원 없음
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_CARD_SHARP, gbInputKeyValue);	
			break;

		// 카드 개별 삭제
		case TENKEY_3:		
			//적당한 음원 없음
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_CARD_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_SHARP:
			OneGMTouchKeyModeSetSelected();
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




void ModeOneGMTouchKeyRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			//적당한 음원 없음
			Feedback13MenuOn(AVML_REGCARD1_DELCARD3, VOL_CHECK);
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOneGMTouchKeyModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}




void GMTouchKeySlotNumberToRegister(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= SUPPORTED_USERKEY_NUMBER))
	{
		//적당한 음원 없음
		FeedbackKeyPadLedOn(AVML_ENTER_CARD_SHARP, VOL_CHECK); 			

		GMTouchKeyInputClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

		gbModePrcsStep++;
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}



void OneGMTouchKeyInputCheckForRegister(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		case TENKEY_STAR:
			gbModePrcsStep = 0;
			break;

		default:
			if(GMTouchKeyInputCheck() == STATUS_SUCCESS)
			{
				gbModePrcsStep++;
				break;
			}

			TimeExpiredCheck();
			break;
	}
}



void InputOneGMTouchKeyProcessLoop(void)
{
	BYTE bTmp;

	// 카드는 처음 입력된 카드 1장만 처리
	bTmp = GMTouchKeyVerify();
	if((bTmp == _DATA_NOT_IDENTIFIED) || (bTmp == gSelectedSlotNumber))
	{
		memcpy(gOneGMTouchKeyUidBuf, gbiButtonBuf, MAX_KEY_UID_SIZE);
	
		FeedbackCredentialInput(VOICE_MIDI_BUTTON2, gSelectedSlotNumber);

		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

		gbModePrcsStep++;
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_ALREADY_USE_CARD, VOL_CHECK);		// The card key is already used.

		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

		// 입력한 터치키가 이미 등록한 터치티와 중복될 경우 다시 터치키 입력 대기 
		gbModePrcsStep = 10;
	}	
}



void ProcessRegisterOneGMTouchKey(void)
{
	switch(gbInputKeyValue)
	{
		case TENKEY_SHARP:
			//# 버튼음 출력
#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;			

		case TENKEY_STAR:
			ReInputOrModeMove(ModeGotoOneGMTouchKeyRegister);
			break;
		
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				
	
		default:
			TimeExpiredCheck();
			break;
	}
}



void BackwardFunctionForOneGMTouchKeyRegDel(void)
{
	ReInputOrModeMove(ModeGotoOneGMTouchKeyRegDelCheck);
}


void RegisterOneGMToucKey(void)
{
	// Slot Number는 1부터 시작, 저장 번지 Index는 0부터 시작
	RomWrite(gOneGMTouchKeyUidBuf, TOUCHKEY_UID+(((WORD)gSelectedSlotNumber-1)*MAX_KEY_UID_SIZE), MAX_KEY_UID_SIZE);
	
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
	
	TenKeyVariablesClear();
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	
	gbModePrcsStep++;
	
	PackTxEventCredentialAdded(CREDENTIALTYPE_GMTOUCHKEY, (WORD)gSelectedSlotNumber);
}


void ModeOneGMTouchKeyRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//enter the number of the user to register the user number not the code, then press the hash to continue.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(GMTouchKeySlotNumberToRegister, BackwardFunctionForOneGMTouchKeyRegDel,1);
			break;

		case 2:
			OneGMTouchKeyInputCheckForRegister();
			break;
		
		case 3:
			InputOneGMTouchKeyProcessLoop();
			break;

		case 4:
			ProcessRegisterOneGMTouchKey();
			break;

		case 5:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			RegisterOneGMToucKey();
			break;
			
		case 6:
			GotoPreviousOrComplete(ModeGotoOneGMTouchKeyRegister);
			break;
		
		case 10:
			// Touch Key 떨어짐 확인
			if(P_IBUTTON_T)
			{
				SetGMTouchKeyTime(100);
			}	
			
			if(GMTouchKeyTimeCheck() == STATUS_SUCCESS)
			{
				GMTouchKeyInputClear();
				// 입력한 터치키가 이미 등록한 터치키와 중복될 경우 다시 터치키 입력 대기 
				gbModePrcsStep = 2;
			}	
			break;

		default:
			ModeClear();
			break;
	}
}




void ProcessDeleteOneGMTouchKey(void)
{
	memset(gOneGMTouchKeyUidBuf, 0xFF, MAX_KEY_UID_SIZE);
	// Slot Number는 1부터 시작, 저장 번지 Index는 0부터 시작
	RomWrite(gOneGMTouchKeyUidBuf, TOUCHKEY_UID+(((WORD)gSelectedSlotNumber-1)*MAX_KEY_UID_SIZE), MAX_KEY_UID_SIZE);

	FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R2, VOL_CHECK);		//completed Press the star for an additional deletion or press the "R" button to finish

	TenKeyVariablesClear();
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	
	gbModePrcsStep++;

	PackTxEventCredentialDeleted(CREDENTIALTYPE_GMTOUCHKEY, (WORD)gSelectedSlotNumber);
}



void GMTouchKeySlotNumberToDelete(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= SUPPORTED_USERKEY_NUMBER))
	{						
		//# 버튼음 출력
#ifdef	P_VOICE_RST	
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
		gbModePrcsStep++;
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}



void ModeOneGMTouchKeyDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_FOR_DEL_SHARP, VOL_CHECK);			//enter the number of the user to delete the user number not the code, then press the hash to continue.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;

		case 1:
			SelectSlotNumber(GMTouchKeySlotNumberToDelete, BackwardFunctionForOneGMTouchKeyRegDel,0);
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessDeleteOneGMTouchKey();
			break;
				
		case 3:
			GotoPreviousOrComplete(ModeGotoOneGMTouchKeyDelete);
			break;

		default:
			ModeClear();
			break;
	}
}



#ifdef	BLE_N_SUPPORT
void ModeGotoOneGMTouchKeyRegisterByBleN(void)
{
	gbMainMode = MODE_MENU_ONEGMTOUCHKEY_REGISTER_BYBLEN;
	gbModePrcsStep = 0;
}



void OneGMTouchKeyInputCheckForRegisterByBleN(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		default:
			if(GMTouchKeyInputCheck() == STATUS_SUCCESS)
			{
				gbModePrcsStep++;
				break;
			}

			TimeExpiredCheck();
			break;
	}
}


void ModeOneGMTouchKeyRegisterByBleN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			gPinInputKeyCnt = 2;
			GMTouchKeySlotNumberToRegister();
			break;

		case 1:
			OneGMTouchKeyInputCheckForRegisterByBleN();
			break;
		
		case 2:
			InputOneGMTouchKeyProcessLoop();
			break;

		case 3:
			RegisterOneGMToucKey();

			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);		//completed

			gbModePrcsStep = 20;
			break;

		case 10:
			// Touch Key 떨어짐 확인
			if(P_IBUTTON_T)
			{
				SetGMTouchKeyTime(100);
			}	
			
			if(GMTouchKeyTimeCheck() == STATUS_SUCCESS)
			{
				GMTouchKeyInputClear();
				// 입력한 터치키가 이미 등록한 터치키와 중복될 경우 다시 터치키 입력 대기 
				gbModePrcsStep = 1;
			}	
			break;

		case 20:
			// Touch Key 떨어짐 확인
			if(P_IBUTTON_T)
			{
				SetGMTouchKeyTime(100);
			}	
			
			if(GMTouchKeyTimeCheck() == STATUS_SUCCESS)
			{
				GMTouchKeyInputClear();
				ModeClear();
			}	
			break;

		default:
			ModeClear();
			break;
	}
}

void ProcessDeleteOneGMTouchKeyByBleN(BYTE SlotNumber)
{
	memset(gOneGMTouchKeyUidBuf, 0xFF, MAX_KEY_UID_SIZE);
	// Slot Number는 1부터 시작, 저장 번지 Index는 0부터 시작
	RomWrite(gOneGMTouchKeyUidBuf, TOUCHKEY_UID+(((WORD)SlotNumber-1)*MAX_KEY_UID_SIZE), MAX_KEY_UID_SIZE);
}

#endif






