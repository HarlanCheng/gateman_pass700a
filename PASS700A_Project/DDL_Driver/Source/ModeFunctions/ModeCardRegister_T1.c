//------------------------------------------------------------------------------
/** 	@file		ModeCardRegister_T1.c
	@version 0.1.00
	@date	2016.04.20
	@brief	Card Register Mode 
	@remark	 카드 등록 모드
	@see	MainModeProcess_T1.c
	@see	CardProcess_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.20		by Jay
			- 신규 UI에 따른 처리
			[카드 등록 과정]
			1.카드 입력 확인
			2.한번에 입력된 카드들 처리
			3.입력된 카드 UID 모두 저장
			4.카드 떨어짐 대기
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gbInputCardNumberForRegister = 0;				/**< 전체 카드 등록 모드에서 입력된 전체 카드 수  */
BYTE gCardProcessLoopCnt = 0;						/**< 전체 카드 등록 모드에서 한번에 동시 입력된 카드 수  */

// 전체 카드 등록 모드에서 입력된 카드의 전체 UID 저장
// 카드 입력 시 처리는 UID Size 별로 하고,  메모리에 저장 시에는 한번에 하기 위해 union 구조 사용
union{
	BYTE gbInPutCardUidBuf[SUPPORTED_USERCARD_NUMBER][MAX_CARD_UID_SIZE];
	BYTE gAllCardRegisterBuf[(SUPPORTED_USERCARD_NUMBER*MAX_CARD_UID_SIZE)];
}UCardDataBuf;




//------------------------------------------------------------------------------
/** 	@brief	Card Variables Clear to register all card 
	@param	None
	@return 	None
	@remark 전체 카드 등록을 위해 관련 변수 초기화
*/
//------------------------------------------------------------------------------
void AllCardDataBufClear(void)
{
	gbInputCardNumberForRegister = 0;
	memset(UCardDataBuf.gAllCardRegisterBuf, 0xFF, (SUPPORTED_USERCARD_NUMBER*MAX_CARD_UID_SIZE));
}



//------------------------------------------------------------------------------
/** 	@brief	Start Card Register Mode
	@param	None
	@return 	None
	@remark 전체 카드 등록 모드 시작
*/
//------------------------------------------------------------------------------
void ModeGotoCardRegister(void)
{
	AllCardDataBufClear();
	
	gbMainMode = MODE_CARD_REGISTER;
	gbModePrcsStep = 0;
}


//------------------------------------------------------------------------------
/** 	@brief	Temporary save of input card UID
	@param	[bCardData] 입력된 카드 UID
	@return 	[_DATA_IDENTIFIED] : 입력된 카드 UID가 저장된 UID와 동일할 때
								(동일 등록 과정에서 이미 등록한 카드)
	@return 	[_DATA_NOT_IDENTIFIED] : 입력된 카드 UID가 저장된 UID와 동일하지 않을 때
								(동일 등록 과정에서 새로운 카드)
	@remark 전체 카드 등록 모드에서 입력된 카드 임시 저장 및 기존 저장된 카드와 비교
*/
//------------------------------------------------------------------------------
BYTE CardVerifyWithTempSave(BYTE *bCardData)
{
	BYTE bCnt;

	for(bCnt = 0; bCnt < gbInputCardNumberForRegister; bCnt++)
	{
		if(memcmp(UCardDataBuf.gbInPutCardUidBuf[bCnt], bCardData, MAX_CARD_UID_SIZE) == 0)
		{
			return (_DATA_IDENTIFIED);
		}
	}
	return (_DATA_NOT_IDENTIFIED);
}



//------------------------------------------------------------------------------
/** 	@brief	Wait Key & Card in Register Mode
	@param	None
	@return 	None
	@remark 전체 카드 등록 모드에서 키 입력이나 카드 입력 대기
*/
//------------------------------------------------------------------------------
void CardInputCheckForRegister(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, TENKEY_NONE);
#endif
			gbModePrcsStep+=3;
			break;
			
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		default:
			if(GetCardReadStatus() == CARDREAD_SUCCESS)
			{
				gCardProcessLoopCnt = GetCardInputNumber(); 	
				gbModePrcsStep++;
				SetModeProcessTime(0);
				break;
			}

			CardReadStatusClear();
			TimeExpiredCheck();
			break;
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Input Cards Temporary save Process
	@param	None
	@return 	None
	@remark 전체 카드 등록 모드에서 한번에 입력된 카드를 비교, 임시 저장 처리
	@remark 한번에 여러 장의 유효한 카드가 입력되었을 경우 차례로 부저 처리하기 위해 아래와 같이 구현
*/
//------------------------------------------------------------------------------
#if defined (DDL_CFG_MS)
void InputCardProcessLoop(void)
{
	BYTE bTemp = 0x00;

	if(GetModeProcessTime())	return;
	
	if(gCardProcessLoopCnt == 0)
	{
		gbModePrcsStep++;
		return;
	}

	gCardProcessLoopCnt--;

	if(CardVerifyWithTempSave(gCardAllUidBuf[gCardProcessLoopCnt]) == _DATA_NOT_IDENTIFIED && CardVerify(1) == _DATA_NOT_IDENTIFIED)
	{
		memcpy(UCardDataBuf.gbInPutCardUidBuf[gbInputCardNumberForRegister], gCardAllUidBuf[gCardProcessLoopCnt], MAX_CARD_UID_SIZE);
		gbInputCardNumberForRegister++; 

		/* 등록 시에 기존에 저장 되어 있는 card 를 지우지 않고 쌓는다 */
		/* 그래서 등록 가능 총 갯수 와 현재 등록된 갯수 를 더해서 비교 */
		MS_Get_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,&bTemp);
		if(gbInputCardNumberForRegister+bTemp >= (MS_GetSupported_Credential_Number(Ms_Credential_Info.bSelected_Item)))
		{
			gbModePrcsStep+=2;
			return;
		}

		MS_Get_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,&bTemp);
		bTemp += gbInputCardNumberForRegister;

		if(gbInputCardNumberForRegister == 1)
		{
			FeedbackCredentialInput(AVML_REG_CARD_R, bTemp);
		}
		else
		{
			FeedbackCredentialInput(VOICE_MIDI_BUTTON, bTemp);
		}	
		// 한번에 여러 장이 입력되었을 경우 차례로 부저 처리하기 위해 300ms 대기 후 다음 처리
		SetModeProcessTime(30);
	}
	else
	{
		MS_Get_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,&bTemp);
		bTemp += gbInputCardNumberForRegister;
		FeedbackErrorNumberOn(AVML_ALREADY_USE_CARD, VOL_CHECK, bTemp);
	}	

	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
}
#else 
void InputCardProcessLoop(void)
{
	if(GetModeProcessTime())	return;
	
	if(gCardProcessLoopCnt == 0)
	{
		gbModePrcsStep++;
		return;
	}

	gCardProcessLoopCnt--;

	if(CardVerifyWithTempSave(gCardAllUidBuf[gCardProcessLoopCnt]) == _DATA_NOT_IDENTIFIED)
	{
		memcpy(UCardDataBuf.gbInPutCardUidBuf[gbInputCardNumberForRegister], gCardAllUidBuf[gCardProcessLoopCnt], MAX_CARD_UID_SIZE);
		gbInputCardNumberForRegister++; 

		if(gbInputCardNumberForRegister >= SUPPORTED_USERCARD_NUMBER)
		{
#if 	(SUPPORTED_USERCARD_NUMBER >= 100)
			AudioFeedback(VOICE_MIDI_BUTTON, gcbBuzNum, VOL_CHECK);
			LedModeRefresh();
#ifdef	DDL_CFG_NO_DIMMER
			LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#else 
			LedGenerate(LED_KEY_0, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_0, DIMM_OFF_FAST, 0);
			LedGenerate(LED_KEY_0, DIMM_ON_FAST, 0);
#endif			
#endif 
			gbModePrcsStep+=2;
			return;
		}

		if(gbInputCardNumberForRegister == 1)
		{
			FeedbackCredentialInput(AVML_REG_CARD_R, gbInputCardNumberForRegister);
		}
		else
		{
			FeedbackCredentialInput(VOICE_MIDI_BUTTON, gbInputCardNumberForRegister);
		}	
		// 한번에 여러 장이 입력되었을 경우 차례로 부저 처리하기 위해 300ms 대기 후 다음 처리
		SetModeProcessTime(30);
	}
	else
	{
		FeedbackErrorNumberOn(AVML_ALREADY_USE_CARD, VOL_CHECK, gbInputCardNumberForRegister);
	}	

	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
}
#endif 

//------------------------------------------------------------------------------
/** 	@brief	Input all cards UID save in memory
	@param	None
	@return 	None
	@remark 전체 카드 등록 모드에서 입력된 모든 카드를 메모리에 저장
*/
//------------------------------------------------------------------------------
void AllCardRegister(void)
{
#if defined (_USE_IREVO_CRYPTO_)
	BYTE i = 0;
	
	for(i = 0 ; i < SUPPORTED_USERCARD_NUMBER ;  i++)
	{
		EncryptDecryptKey((UCardDataBuf.gAllCardRegisterBuf+((WORD)i*8)),8);
	}
#endif 
	
#if defined (DDL_CFG_MS)
	BYTE bTemp = 0x00; 
	MS_Get_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,&bTemp);
	MS_Add_Credential_Info(UCardDataBuf.gAllCardRegisterBuf,gbInputCardNumberForRegister);
	RomWrite(UCardDataBuf.gAllCardRegisterBuf,MS_GetSupported_Credential_Start_Address(Ms_Credential_Info.bSelected_Item)+((WORD)bTemp*MAX_CARD_UID_SIZE),((WORD)gbInputCardNumberForRegister*MAX_CARD_UID_SIZE));
	MS_Set_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,gbInputCardNumberForRegister+bTemp);
#else 
	RomWrite(UCardDataBuf.gAllCardRegisterBuf, CARD_UID, (SUPPORTED_USERCARD_NUMBER*MAX_CARD_UID_SIZE));
#endif 

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	ScheduleDeleteOfAllUserCard();
#endif
}


//------------------------------------------------------------------------------
/** 	@brief	Delete all saved cards in memory
	@param	None
	@return 	None
	@remark 전체 카드 삭제 모드에서 모든 카드를 메모리에서 삭제
*/
//------------------------------------------------------------------------------
void AllCardDelete(void)
{
#if defined (DDL_CFG_MS)
	RomWriteWithSameData(0xFF, MS_GetSupported_Credential_Start_Address(Ms_Credential_Info.bSelected_Item), (MS_GetSupported_Credential_Number(Ms_Credential_Info.bSelected_Item)*MAX_CARD_UID_SIZE));
	MS_Set_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,0x00);
	FactoryResetDoneCheck();
#else 	
#if defined (_USE_IREVO_CRYPTO_)
	BYTE bTemp[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	BYTE i = 0x00;
	
	EncryptDecryptKey(bTemp, 8);
	
	for( i = 0 ; i < SUPPORTED_USERCARD_NUMBER ; i++)
	{
#ifdef	DDL_CFG_WATCHDOG
		RefreshIwdg();
#endif
		RomWrite(bTemp,CARD_UID+(i*MAX_CARD_UID_SIZE),MAX_CARD_UID_SIZE);
	}
#else 	
	RomWriteWithSameData(0xFF, CARD_UID, (SUPPORTED_USERCARD_NUMBER*MAX_CARD_UID_SIZE));
#endif 
#endif 

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	ScheduleDeleteOfAllUserCard();
#endif
}


//------------------------------------------------------------------------------
/** 	@brief	Pincode Register Mode Start
	@param	None
	@return 	None
	@remark 비밀번호 등록 모드 시작
*/
//------------------------------------------------------------------------------
void ModeCardRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			CardInputCheckForRegister();
			break;

		case 1:
			InputCardProcessLoop();
			break;			

		case 2:
			if(GetCardReadStatus()== CARDREAD_NO_CARD)
			{
				CardReadStatusClear();
				gbModePrcsStep = 0;
				break;
			}	
			TimeExpiredCheck();
			break;

		case 3:
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
#endif

#if 	(SUPPORTED_USERCARD_NUMBER >= 100)
			if(GetLedMode())	break;
#endif 			

#ifdef	DDL_TEST_SET_FOR_CARD
			if(IsCardPowerOnForTest() == true)
			{
				gfCardPowerOnForTest = 0;
			}
#endif 				
			
			if(gbInputCardNumberForRegister == 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;
			}

			AllCardRegister();
			
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			gbModePrcsStep++;

			PackTxEventCredentialAdded(CREDENTIALTYPE_CARD, 0xFF);
			break;

#if defined (DDL_CFG_MS)
		case 4:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			
			CardGotoReadStop();
			ModeGotoMenuMainSelect();
			break;
#else 
		case 4:
			if(GetCardReadStatus()== CARDREAD_NO_CARD)
			{
				ModeClear();
			}	
			break;
#endif 

		default:
			ModeClear();
			break;
	}
}



#ifdef	BLE_N_SUPPORT
void ModeGotoCardRegisterByBleN(void)
{
	AllCardDataBufClear();
	
	gbMainMode = MODE_CARD_REGISTER_BYBLEN;
	gbModePrcsStep = 0;

	FeedbackKeyPadLedOn(VOICE_MIDI_INTO, VOL_CHECK);

	CardGotoReadStart();

	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
}


void CardInputCheckForRegisterByBleN(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, TENKEY_NONE);
#endif
			gbModePrcsStep+=3;
			break;
			
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		default:
			if(GetCardReadStatus() == CARDREAD_SUCCESS)
			{
				gCardProcessLoopCnt = GetCardInputNumber(); 	
				gbModePrcsStep++;
				SetModeProcessTime(0);
				break;
			}

			CardReadStatusClear();

			if(GetModeTimeOut() == 0)
			{
				gbModePrcsStep+=3;
			}
			break;
	}
}


void InputCardProcessLoopByBleN(void)
{
	if(GetModeProcessTime())	return;
	
	if(gCardProcessLoopCnt == 0)
	{
		gbModePrcsStep++;
		return;
	}

	gCardProcessLoopCnt--;

	if(CardVerifyWithTempSave(gCardAllUidBuf[gCardProcessLoopCnt]) == _DATA_NOT_IDENTIFIED)
	{
		memcpy(UCardDataBuf.gbInPutCardUidBuf[gbInputCardNumberForRegister], gCardAllUidBuf[gCardProcessLoopCnt], MAX_CARD_UID_SIZE);
		gbInputCardNumberForRegister++; 
	
		if(gbInputCardNumberForRegister >= SUPPORTED_USERCARD_NUMBER)
		{
			gbModePrcsStep+=2;
			return;
		}
	
		// BLE-N에 의한 카드 등록 시에는 음성 안내 하지 않도록 수정, 보이스 안내가 길어, 보이스 안내 음성이 모두 재생되지 않아서,
		FeedbackCredentialInput(VOICE_MIDI_BUTTON, gbInputCardNumberForRegister);

		// 한번에 여러 장이 입력되었을 경우 차례로 부저 처리하기 위해 300ms 대기 후 다음 처리
		SetModeProcessTime(30);
	}
	else
	{
		FeedbackErrorNumberOn(AVML_ALREADY_USE_CARD, VOL_CHECK, gbInputCardNumberForRegister);
	}	

	SetModeTimeOut(50);
}


void ModeCardRegisterByBleN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			CardInputCheckForRegisterByBleN();
			break;

		case 1:
			InputCardProcessLoopByBleN();
			break;			

		case 2:
			if(GetCardReadStatus()== CARDREAD_NO_CARD)
			{
				CardReadStatusClear();
				gbModePrcsStep = 0;
				break;
			}	
			// 카드 등록 중 Time Out이 발생하면 자동 종료되도록 수정
//			TimeExpiredCheck();
			if(GetModeTimeOut() == 0)
			{
				gbModePrcsStep++;
			}
			break;

		case 3:
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
#endif
			if(gbInputCardNumberForRegister == 0)
			{
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);
				ModeClear();
				break;
			}

			AllCardRegister();
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			gbModePrcsStep++;

			PackTxEventCredentialAdded(CREDENTIALTYPE_CARD, 0xFF);
			break;

		case 4:
			if(GetCardReadStatus()== CARDREAD_NO_CARD)
			{
				ModeClear();
			}	
			break;

		default:
			ModeClear();
			break;
	}
}
#endif

