//------------------------------------------------------------------------------
/** 	@file		LedFunctions_T1.c
	@version 0.1.00
	@date	2016.04.25
	@brief	Led Control with or without Dimming IC Philips PCA9532 Interface Functions 
	@remark	Dimming IC로 LED 제어하는 경우와 그렇지 않을 경우 모두 포함  
	@see	FeedbackFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.25		by Jay
			- 전체 LED ON/OFF의 경우나 정해진 LED를 같이 ON/OFF할 경우는 
				Constant Data 사용하지 않고, Dimming Data를 생성하여 처리할 수 있도록 구현
			- 기존 Constant Data로도 구현 가능하도록 처리
			- LED Queue를 이용하여 최대 3개까지 한꺼번에 설정 가능하도록 처리
			    (Dimming On-> Delay -> Dimming Off 의 3단계를 한꺼번에 설정 가능)
		V0.1.00 2016.04.26		by Youn
			 - 지문 모듈 처리와 관련된 LED 출력 값 추가
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gbLedMode = 0;							/**< LED 출력 처리를 위한 Process 변수  */
BYTE gbLedCnt = 0;							/**< LED 출력 On/Off 시간 확인을 위한 변수  */

BYTE gbLedTimer2ms = 0;						/**< LED 출력 처리를 위한 Timer 변수, 2ms Tick  */

BYTE gbLedDisplayMode = 0;					/**< 출력할 LED가 처리하는 방식을 위한 변수  */

#ifdef DDL_LED_DISPLAY_SELECT //led display 결정 
BYTE gLedDisplayEnableState;
#endif //DDL_LED_DISPLAY_SELECT #endif
//------------------------------------------------------------------------------
/*! 	\def	MAX_SEQ_LED_QUEUE		
	전송할 LED 출력 종류를 저장하는 Queue, 
	한꺼번에 정해진 개수까지 연속으로 출력 설정 가능
*/
//------------------------------------------------------------------------------
#define	MAX_SEQ_LED_QUEUE		10


LedType_Diplay	CurrentLedData;				/**< 현재 출력하는 LED 구조체  */
LedType_Diplay	SequentialLedDataQueue[MAX_SEQ_LED_QUEUE];		/**< 출력할 LED 구조체를 저장하는 Queue  */

BYTE gSeqLedQueueHead = 0;				/**< 출력할 LED 구조체에 새로운 Data 입력을 위한 Queue Head */
BYTE gSeqLedQueueTail = 0;				/**< 출력할 LED 구조체로부터 새로운 Data 출력을 위한 Queue Tail */	


const BYTE gcbWaitLedNextData[] = {LED_ENDD}; 		/**< Sequential Queue에 저장된 LED Data 사이에 Delay를 주고할 경우 사용하는 LED Constand Data */

#ifdef DDL_CFG_FP
BYTE gbLedDisplayFPMOperation = 0;	
#endif 

//------------------------------------------------------------------------------
/** 	@brief	외부에서 LED 동작을 진행 중인지 확인할 때 사용
	@param	None
	@return	[gbLedMode] : 현재 LED 단계 회신
	@remark 0으로 회신할 경우 LED 구동하고 있지 않음, 그 외에는 LED 구동 중
*/	
//------------------------------------------------------------------------------
BYTE GetLedMode(void)
{
	return (gbLedMode);
}


//------------------------------------------------------------------------------
/** 	@brief	LED Process Time Count
	@param	None
	@return 	None
	@remark LED 구동에 사용하는 Time 변수, 반드시 BasicTimer_2ms_local 함수에 추가 
*/
//------------------------------------------------------------------------------
void LedProcessTimeCount(void)
{
	if(gbLedTimer2ms)		--gbLedTimer2ms;
}




//------------------------------------------------------------------------------
/** 	@brief	출력할 LED Data를 Sequential Queue에 저장
	@param	[LedData] 저장할 Led Data 종류 구조체
	@return 	None
	@remark 출력할 Led Data 종류 구조체를 Queue에 저장, Constant Data도 모두 저장 가능
*/
//------------------------------------------------------------------------------
void PutDataToSeqLedQueue(LedType_Diplay LedData)
{
	BYTE bTmp;

	bTmp = (gSeqLedQueueHead+1);
	// & 처리로 순환을 구현할 경우 Masking할 수 있는 값으로만 MAX_SEQ_LED_QUEUE 값을 설정해야 하는 한계가 있고,
	// % 처리로 구현할 경우에는 에러가 발생하여, 아래와 같이 처리
	if(bTmp >= MAX_SEQ_LED_QUEUE)
	{
		bTmp = 0;
	}
	
	// QHead를 저장할 경우 QTail Data를 덮어쓰지만 않으면 저장하도록 처리
	if(bTmp != gSeqLedQueueTail)
	{
		SequentialLedDataQueue[gSeqLedQueueHead] = LedData;
		gSeqLedQueueHead++;
		if(gSeqLedQueueHead >= MAX_SEQ_LED_QUEUE)
		{
			gSeqLedQueueHead = 0;
		}
	}
}



//------------------------------------------------------------------------------
/** 	@brief	출력할 LED Data를 Sequential Queue로부터 Load
	@param	None
	@return 	[0] 더이상 Load할 LED Data 없음
	@return 	[1] LED Data Load 성공 
	@remark 출력할 LED Data 종류 구조체를 Queue로부터 CurrentLedData에 Load
*/
//------------------------------------------------------------------------------
BYTE GetDataFromSeqLedQueue(void)
{
	// QTail의 경우 QHead와 값이 다르면 저장된 값이 있다고 판단하고 바로 처리
	if(gSeqLedQueueTail != gSeqLedQueueHead)
	{
		CurrentLedData = SequentialLedDataQueue[gSeqLedQueueTail];
	
		gSeqLedQueueTail++;
		if(gSeqLedQueueTail >= MAX_SEQ_LED_QUEUE)
		{
			gSeqLedQueueTail = 0;
		}
		return 1;
	}
	return 0;
}



//------------------------------------------------------------------------------
/** 	@brief	Constant LED Data를 Queue에 저장하여 수행하고자 할 경우 사용
	@param	[bpLedData] : 실행하고자 하는 Constant LED Data
	@param	[bMode] : gcbWaitLedNextData 일 경우에는 Delay 시간
					 그 외의 경우에는 Display Mode 적용
	@return	None
	@remark 기존 내용과 대부분 동일하지만, Queue를 읽어들이는 쪽으로 이동하는 부분이 달라짐
*/	
//------------------------------------------------------------------------------
void LedSetting(BYTE const* bpLedData, BYTE bMode)
{
	LedType_Diplay DimmData;

#ifdef	DDL_CFG_FP
	if(GetMainMode() == MODE_FINGER_VERIFY){
		gbLedDisplayFPMOperation = 0x01;
	}
	else 
	{
		gbLedDisplayFPMOperation = 0x00;
	}
#endif 
	
	if(bpLedData == gcbWaitLedNextData)
	{
		// Queue에 저장된 Data 사이의 단순 Delay일 경우에는 해당 값을 별도 저장
		DimmData.WaitTimeForNextDisplay = bMode;

		bMode = LED_KEEP_DISPLAY;
	}

	DimmData.pLedBuf = (BYTE*)bpLedData;

	DimmData.DisplayMode = bMode;

	PutDataToSeqLedQueue(DimmData);

	gbLedMode = 10;
	gbLedCnt = 0;
	gbLedTimer2ms = 0;
}

//LED Dimmer가 존재하면...
#ifdef	DDL_CFG_DIMMER


BYTE	gbFLedData_i2cbuff[9];					/**< I2C를 이용해 Dimming IC로 전달할 LED Data의 총 저장 변수  */
BYTE	*gbFLedData = gbFLedData_i2cbuff + 1;	/**< I2C를 이용해 Dimming IC로 전달할 LED Data의 시작 위치 포인터 */


ddl_i2c_t		*h_dimming_i2c_dev;			/**< Dimming IC 제어를 위한 Handle  */



BYTE gLSxOnBuf[4];							/**< Dimming IC로 제어할 LED ON 처리 참조 Data  */
BYTE gLSxOffBuf[4];							/**< Dimming IC로 제어할 LED OFF 처리 참조 Data  */
BYTE gNumberOfDimmStepMax = 0;				/**< Dimming IC로 전송할 Dimming Data의 총 단계의 수  */
BYTE gNumberOfDimmStepCnt = 0;				/**< Dimming IC로 전송한 Dimming Data의 단계의 수  */
BYTE gBrightness = 0;						/**< Dimming IC로 전송할 PWM0의 밝기 정도 변수  */

BYTE gMakeDimmPrcsStep = 0;					/**< Dimming IC로 전송할 Dimming Data를 PWM0의 밝기 정도 변수  */

WORD gDimmLedLastSwitch = 0;				/**< Dimming IC로 제어한 마지막 Dimming Data  */



//------------------------------------------------------------------------------
/*! 	\def	MAX_DIMM_QUEUE		
	Dimming IC로 전송할 LED 출력 Data를 Packet별로 생성하여 저장하는 Queue, 
	전체 Dimming On/Off의 경우 해당 Dimming Data를 매번 연산하여 Queue에 차례로 저장
*/
//------------------------------------------------------------------------------
#define	MAX_DIMM_QUEUE		5					


BYTE gArrayDimmingDataBuf[MAX_DIMM_QUEUE][7];	/**< 현재 출력하는 Dimming Data를 생성하여 차례로 저장하는 Queue, 1 Packet이 7bytes  */

BYTE gArrayDimmingQueueHead = 0;			/**< 현재 출력하는 Dimming Data의 생성된 Packet를 Queue에 입력하기 위한 Queue Head */
BYTE gArrayDimmingQueueTail = 0;			/**< 현재 출력하는 Dimming Data의 생성된 Packet를 Queue로부터 출력하기 위한 Queue Tail */

BYTE gTempDimmingDataBuf[7];				/**< 현재 출력하는 Dimming Data Packet을 저장한 버퍼 */


//------------------------------------------------------------------------------
/*! 	\def	ONEKEY_DIM_BLINK_DATA_NO		
	BLINK_DIMM_ON 수행할 때의 Dimm Off->On 단계 표시
*/
//------------------------------------------------------------------------------
#define	ONEKEY_DIM_BLINK_DATA_NO	12

BYTE DIMM[ONEKEY_DIM_BLINK_DATA_NO] = {125, 60, 0, 0, 0, 25, 62, 100, 137, 175, 212, 250};		/**< 번호 입력 시 Blink 표시하기 위한 Dimming PWM0 Data */




BYTE DimmTypeBuf_Off[] = {0xFC, 0xF3, 0xCF, 0x3F};	/**< Dimming IC의 LSx 항목을 각각 Off 설정하는데 사용하는 배열 */
BYTE DimmTypeBuf_On[] = {0x01, 0x04, 0x10, 0x40};	/**< Dimming IC의 LSx 항목을 각각 On 설정하는데 사용하는 배열 */
BYTE DimmTypeBuf_Dimm[] = {0x02, 0x08, 0x20, 0x80};	/**< Dimming IC의 LSx 항목을 각각 PWM0 On 설정하는데 사용하는 배열 */

BYTE *DimmTypeBuf[3] = {DimmTypeBuf_Off, DimmTypeBuf_On, DimmTypeBuf_Dimm};	/**< Dimming IC의 LSx 항목을 각각 설정하는데 사용하는 배열 포인터 */

//------------------------------------------------------------------------------
/*! 	\def	DIMM_COVERT_MODE_OFF		
	Dimming IC의 LSx 항목을 배열에서 Off 설정 

	\def	DIMM_COVERT_MODE_ON		
	Dimming IC의 LSx 항목을 배열에서 On 설정 

	\def	DIMM_COVERT_MODE_DIMM 	
	Dimming IC의 LSx 항목을 배열에서 PWM0 On 설정 
*/
//------------------------------------------------------------------------------
#define	DIMM_COVERT_MODE_OFF	0
#define	DIMM_COVERT_MODE_ON		1
#define	DIMM_COVERT_MODE_DIMM	2


const BYTE gcbLedKeyOff[]={
	LED_ENDD
};														/**< 출력 중인 LED 전체 Off */

const BYTE gcbLedErr[]={
	 00,  00, 0x44, 0x11, 0x00, 0x40, 6,	//13579
	 00,  00, 0x00, 0x00, 0x00, 0x00, 6,	//ALL Off
	 00,  00, 0x44, 0x11, 0x00, 0x40, 6,	//13579
	 00,  00, 0x00, 0x00, 0x00, 0x00, 6,	//ALL Off
	 00,  00, 0x44, 0x11, 0x00, 0x40, 6,	//13579
	 00,  00, 0x00, 0x00, 0x00, 0x00, 6,	//ALL Off
	 00,  00, 0x44, 0x11, 0x00, 0x40, 6,	//13579
	 00,  00, 0x00, 0x00, 0x00, 0x00, 6,	//ALL Off
	 00,  00, 0x44, 0x11, 0x00, 0x40, 6,	//13579
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	//ALL Off
	LED_ENDD
};														/**< 에러 상황에서 사용하는 Dimming LED Constand Data */
const BYTE gcbLedErr3[]={
	// 센서 에러
	 00,  00, 0x05, 0x55, 0x00, 0x50, 4,	//147* 369#
	 00,  00, 0x50, 0x00, 0x14, 0x00, 4,	//2580
	 00,  00, 0x05, 0x55, 0x00, 0x50, 4,	//147* 369#
	 00,  00, 0x50, 0x00, 0x14, 0x00, 4,	//2580
	 00,  00, 0x05, 0x55, 0x00, 0x50, 4,	//147* 369#
	 00,  00, 0x50, 0x00, 0x14, 0x00, 4,	//2580
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	//ALL Off
	LED_ENDD
};
const BYTE gcbLedLock[]={
	 00,  00, 0x00, 0x55, 0x00, 0x00, 2,	//147*
	212,  00, 0x50, 0xAA, 0x14, 0x00, 0,	//2580 / 147* (T1) 85%
	175,  00, 0x50, 0xAA, 0x14, 0x00, 0,	//2580 / 147* (T1) 70%
	137,  00, 0x50, 0xAA, 0x14, 0x00, 0,	//2580 / 147* (T1) 55%
	100, 212, 0xF5, 0xAA, 0x3C, 0x50, 0,	//369# / 2580(T2) 85% / 147* (T1) 40%
	 62, 175, 0xF5, 0xAA, 0x3C, 0x50, 0,	//369# / 2580(T2) 70% / 147* (T1) 25%
	 25, 137, 0xF5, 0xAA, 0x3C, 0x50, 0,	//369# / 2580(T2) 55% / 147* (T1) 10%
	212, 100, 0xFA, 0x00, 0x3C, 0xA0, 0,	//369#(T1) 85% / 2580(T2) 40%
	175,  62, 0xFA, 0x00, 0x3C, 0xA0, 0,	//369#(T1) 70% / 2580(T2) 25%
	137,  25, 0xFA, 0x00, 0x3C, 0xA0, 0,	//369#(T1) 55% / 2580(T2) 10%
	100,  00, 0x0A, 0x00, 0x00, 0xA0, 0,	//369#(T1) 40%
	 62,  00, 0x0A, 0x00, 0x00, 0xA0, 0,	//369#(T1) 25%
	 25,  00, 0x0A, 0x00, 0x00, 0xA0, 0,	//369#(T1) 10%
	 00,  00, 0x05, 0x00, 0x00, 0x50, 2,	//369#	
	212,  00, 0x5A, 0x00, 0x14, 0xA0, 0,	//2580 / 369#(T1)
	175,  00, 0x5A, 0x00, 0x14, 0xA0, 0,	//2580 / 369#(T1)
	137,  00, 0x5A, 0x00, 0x14, 0xA0, 0,	//2580 / 369#(T1)
	100, 212, 0xFA, 0x55, 0x3C, 0xA0, 0,	//147* / 2580(T2) / 369#(T1)
	 62, 175, 0xFA, 0x55, 0x3C, 0xA0, 0,	//147* / 2580(T2) / 369#(T1)
	 25, 137, 0xFA, 0x55, 0x3C, 0xA0, 0,	//147* / 2580(T2) / 369#(T1)
	212, 100, 0xF0, 0xAA, 0x3C, 0x00, 0,	//147*(T1) / 2580(T2)
	175,  62, 0xF0, 0xAA, 0x3C, 0x00, 0,	//147*(T1) / 2580(T2)
	137,  25, 0xF0, 0xAA, 0x3C, 0x00, 0,	//147*(T1) / 2580(T2)
	100,  00, 0x00, 0xAA, 0x00, 0x00, 0,	//147*(T1)
	 62,  00, 0x00, 0xAA, 0x00, 0x00, 0,	//147*(T1)
	 25,  00, 0x00, 0xAA, 0x00, 0x00, 0,	//147*(T1)
	 00,  00, 0x00, 0x55, 0x00, 0x00, 2,	//147*
	212,  00, 0x50, 0xAA, 0x14, 0x00, 0,	//2580 / 147* (T1) 85%
	175,  00, 0x50, 0xAA, 0x14, 0x00, 0,	//2580 / 147* (T1) 70%
	137,  00, 0x50, 0xAA, 0x14, 0x00, 0,	//2580 / 147* (T1) 55%
	100, 212, 0xF5, 0xAA, 0x3C, 0x50, 0,	//369# / 2580(T2) 85% / 147* (T1) 40%
	 62, 175, 0xF5, 0xAA, 0x3C, 0x50, 0,	//369# / 2580(T2) 70% / 147* (T1) 25%
	 25, 137, 0xF5, 0xAA, 0x3C, 0x50, 0,	//369# / 2580(T2) 55% / 147* (T1) 10%
	212, 100, 0xFA, 0x00, 0x3C, 0xA0, 0,	//369#(T1) 85% / 2580(T2) 40%
	175,  62, 0xFA, 0x00, 0x3C, 0xA0, 0,	//369#(T1) 70% / 2580(T2) 25%
	137,  25, 0xFA, 0x00, 0x3C, 0xA0, 0,	//369#(T1) 55% / 2580(T2) 10%
	100,  00, 0x0A, 0x00, 0x00, 0xA0, 0,	//369#(T1) 40%
	 62,  00, 0x0A, 0x00, 0x00, 0xA0, 0,	//369#(T1) 25%
	 25,  00, 0x0A, 0x00, 0x00, 0xA0, 0,	//369#(T1) 10%
	 00,  00, 0x05, 0x00, 0x00, 0x50, 2,	//369#	
	212,  00, 0x5A, 0x00, 0x14, 0xA0, 0,	//2580 / 369#(T1)
	175,  00, 0x5A, 0x00, 0x14, 0xA0, 0,	//2580 / 369#(T1)
	137,  00, 0x5A, 0x00, 0x14, 0xA0, 0,	//2580 / 369#(T1)
	100, 212, 0xFA, 0x55, 0x3C, 0xA0, 0,	//147* / 2580(T2) / 369#(T1)
	 62, 175, 0xFA, 0x55, 0x3C, 0xA0, 0,	//147* / 2580(T2) / 369#(T1)
	 25, 137, 0xFA, 0x55, 0x3C, 0xA0, 0,	//147* / 2580(T2) / 369#(T1)
	212, 100, 0xF0, 0xAA, 0x3C, 0x00, 0,	//147*(T1) / 2580(T2)
	175,  62, 0xF0, 0xAA, 0x3C, 0x00, 0,	//147*(T1) / 2580(T2)
	137,  25, 0xF0, 0xAA, 0x3C, 0x00, 0,	//147*(T1) / 2580(T2)
	100,  00, 0x00, 0xAA, 0x00, 0x00, 0,	//147*(T1)
	 62,  00, 0x00, 0xAA, 0x00, 0x00, 0,	//147*(T1)
	 25,  00, 0x00, 0xAA, 0x00, 0x00, 0,	//147*(T1)
	LED_ENDD
};														/**< 외부/내부강제잠금 상태 알림에 사용하는  Dimming LED Constand Data */


const BYTE gcbLedOk[]={
	 90,  00, 0x10, 0x00, 0x00, 0x00, 2,	//2
	 90,  00, 0x10, 0x01, 0x00, 0x00, 2,	//12
	 90,  00, 0x10, 0x05, 0x00, 0x00, 2,	//124
	 90,  00, 0x10, 0x15, 0x00, 0x00, 2,	//1247
	 90,  00, 0x10, 0x15, 0x04, 0x00, 2,	//12478
	 90,  00, 0x10, 0x15, 0x04, 0x40, 2,	//124789
	 90,  00, 0x11, 0x15, 0x04, 0x40, 2,	//1246789
	 90,  00, 0x15, 0x15, 0x04, 0x40, 20,	//12346789
	240,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	220,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	200,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	180,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	160,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	140,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	120,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	100,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	 80,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	 60,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	 40,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	 20,  00, 0x2A, 0x2A, 0x08, 0x80, 0,	//12346789 (T1)
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	//ALL Off
	LED_ENDD
};														/**< 등록/설정 시에 사용하는 Dimming LED Constand Data */



const BYTE gcbLedAlert[]={
	 90, 170, 0x10, 0x00, 0x00, 0x00, 2,	//2
	 90, 170, 0x14, 0x00, 0x00, 0x00, 2,	//23
	 90, 170, 0x15, 0x00, 0x00, 0x00, 2,	//236
	 90, 170, 0x15, 0x00, 0x00, 0x40, 2,	//2369
	 90, 170, 0x15, 0x00, 0x00, 0x50, 2,	//2369#
	 90, 170, 0x15, 0x00, 0x10, 0x50, 2,	//23690#
	 90, 170, 0x15, 0x40, 0x10, 0x50, 2,	//2369*0#
	 90, 170, 0x15, 0x50, 0x10, 0x50, 2,	//23679*0#
	 90, 170, 0x15, 0x54, 0x10, 0x50, 2,	//234679*0#
	 90, 170, 0x15, 0x55, 0x10, 0x50, 2,	//1234679*0#
	 90, 170, 0x05, 0x55, 0x10, 0x50, 2,	//134679*0#
	 90, 170, 0x01, 0x55, 0x10, 0x50, 2,	//14679*0#
	 90, 170, 0x00, 0x55, 0x10, 0x50, 2,	//1479*0#
	 90, 170, 0x00, 0x55, 0x10, 0x10, 2,	//147*0#
	 90, 170, 0x00, 0x55, 0x10, 0x00, 2,	//147*0
	 90, 170, 0x00, 0x55, 0x00, 0x00, 2,	//147*
	 90, 170, 0x00, 0x15, 0x00, 0x00, 2,	//147
	 90, 170, 0x00, 0x05, 0x00, 0x00, 2,	//14
	 90, 170, 0x00, 0x01, 0x00, 0x00, 2,	//1
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	//ALL Off
	LED_ENDD
};														/**< 경보 발생 시에 사용하는 Dimming LED Constand Data */


const BYTE gcbLedOpen[]={
	 00,  00, 0x00, 0x40, 0x10, 0x10, 3,	//*0#
	 00,  00, 0x00, 0x50, 0x14, 0x50, 3,	//789*0#
	 00,  00, 0x41, 0x54, 0x14, 0x50, 3,	//456789*0#
	200,  00, 0x55, 0x95, 0x24, 0x60, 0,	//123456789 / *0# 80% (T1)
	187,  00, 0x55, 0x95, 0x24, 0x60, 0,	//123456789 / *0# 75% (T1)
	175,  00, 0x55, 0x95, 0x24, 0x60, 0,	//123456789 / *0# 70% (T1)
	162,  00, 0x55, 0x95, 0x24, 0x60, 0,	//123456789 / *0# 65% (T1)
	150,  00, 0x55, 0x95, 0x24, 0x60, 0,	//123456789 / *0# 60% (T1)
	137,  00, 0x55, 0x95, 0x24, 0x60, 0,	//123456789 / *0# 55% (T1)
	125,  00, 0x55, 0x95, 0x24, 0x60, 0,	//123456789 / *0# 50% (T1)
	112,  00, 0x55, 0x95, 0x24, 0x60, 0,	//123456789 / *0# 45% (T1)
	100, 200, 0x55, 0xB5, 0x2C, 0xE0, 0,	//123456 / 789 80% (T2) / *0# 40% (T1)
	 87, 187, 0x55, 0xB5, 0x2C, 0xE0, 0,	//123456 / 789 75% (T2) / *0# 35% (T1)
	 75, 175, 0x55, 0xB5, 0x2C, 0xE0, 0,	//123456 / 789 70% (T2) / *0# 30% (T1)
	 62, 162, 0x55, 0xB5, 0x2C, 0xE0, 0,	//123456 / 789 65% (T2) / *0# 25% (T1)
	 50, 150, 0x55, 0xB5, 0x2C, 0xE0, 0,	//123456 / 789 60% (T2) / *0# 20% (T1)
	 37, 137, 0x55, 0xB5, 0x2C, 0xE0, 0,	//123456 / 789 55% (T2) / *0# 15% (T1)
	 25, 125, 0x55, 0xB5, 0x2C, 0xE0, 0,	//123456 / 789 50% (T2) / *0# 10% (T1)
	 12, 112, 0x55, 0xB5, 0x2C, 0xE0, 0,	//123456 / 789 45% (T2) / *0#  5% (T1)
	200, 100, 0x96, 0x39, 0x0C, 0xC0, 0,	//123 / 456 80% (T1) / 789 40% (T2)
	187,  87, 0x96, 0x39, 0x0C, 0xC0, 0,	//123 / 456 75% (T1) / 789 35% (T2)
	175,  75, 0x96, 0x39, 0x0C, 0xC0, 0,	//123 / 456 70% (T1) / 789 30% (T2)
	162,  62, 0x96, 0x39, 0x0C, 0xC0, 0,	//123 / 456 65% (T1) / 789 25% (T2)
	145,  50, 0x96, 0x39, 0x0C, 0xC0, 0,	//123 / 456 60% (T1) / 789 20% (T2)
	130,  37, 0x96, 0x39, 0x0C, 0xC0, 0,	//123 / 456 55% (T1) / 789 15% (T2)
	120,  25, 0x96, 0x39, 0x0C, 0xC0, 0,	//123 / 456 50% (T1) / 789 10% (T2)
	112,  12, 0x96, 0x39, 0x0C, 0xC0, 0,	//123 / 456 45% (T1) / 789  5% (T2)
	100, 245, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 80% (T2) / 456 40% (T1)
	 94, 245, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 75% (T2) / 456 35% (T1)
	 87, 220, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 70% (T2) / 456 30% (T1)
	 75, 207, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 65% (T2) / 456 25% (T1)
	 62, 195, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 60% (T2) / 456 20% (T1)
	 50, 195, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 55% (T2) / 456 15% (T1)
	 37, 182, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 50% (T2) / 456 10% (T1)
	 25, 182, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 45% (T2) / 456  5% (T1)
	 12, 170, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 40% (T2)
	 00, 170, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 35% (T2)
	 00, 157, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 30% (T2)
	 00, 157, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 25% (T2)
	 00, 145, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 20% (T2)
	 00, 132, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 15% (T2)
	 00, 120, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 10% (T2)
	 00, 107, 0xBE, 0x0B, 0x00, 0x00, 0,	//123  5% (T2)
	 00, 101, 0xBE, 0x0B, 0x00, 0x00, 0,	//123  5% (T2)
	 00,  95, 0xBE, 0x0B, 0x00, 0x00, 0,	//123  5% (T2)
	 00,  89, 0xBE, 0x0B, 0x00, 0x00, 0,	//123  5% (T2)
	 00,  82, 0xBE, 0x0B, 0x00, 0x00, 0,	//123  5% (T2)
	 00,  76, 0xBE, 0x0B, 0x00, 0x00, 0,	//123  5% (T2)
	 00,  70, 0xBE, 0x0B, 0x00, 0x00, 0,	//123  5% (T2)
	 00,  64, 0xBE, 0x0B, 0x00, 0x00, 0,	//123  5% (T2)
	 00,  57, 0xBE, 0x0B, 0x00, 0x00, 0,	//123  5% (T2)
	 00,  51, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 15% (T2)
	 00,  45, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 15% (T2)
	 00,  32, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 15% (T2)
	 00,  25, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 15% (T2)
	 00,  12, 0xBE, 0x0B, 0x00, 0x00, 0,	//123 15% (T2)
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	//ALL Off
	LED_ENDD
};														/**< 모터 열림 시에 사용하는 Dimming LED Constand Data */


const BYTE gcbLedClose[]={
	 00,  00, 0x14, 0x01, 0x00, 0x00, 3,	//123
	 00,  00, 0x55, 0x05, 0x00, 0x00, 3,	//123456
	 00,  00, 0x55, 0x15, 0x04, 0x40, 3,	//123456789
	200,  00, 0x69, 0x56, 0x14, 0x50, 0,	//456789*0# / 123 80% (T1)
	187,  00, 0x69, 0x56, 0x14, 0x50, 0,	//456789*0# / 123 75% (T1)
	175,  00, 0x69, 0x56, 0x14, 0x50, 0,	//456789*0# / 123 70% (T1)
	162,  00, 0x69, 0x56, 0x14, 0x50, 0,	//456789*0# / 123 65% (T1)
	150,  00, 0x69, 0x56, 0x14, 0x50, 0,	//456789*0# / 123 60% (T1)
	137,  00, 0x69, 0x56, 0x14, 0x50, 0,	//456789*0# / 123 55% (T1)
	125,  00, 0x69, 0x56, 0x14, 0x50, 0,	//456789*0# / 123 50% (T1)
	112,  00, 0x69, 0x56, 0x14, 0x50, 0,	//456789*0# / 123 45% (T1)
	100, 200, 0xEB, 0x5E, 0x14, 0x50, 0,	//789*0# / 456 80% (T2) / 123 40% (T1)
	 87, 187, 0xEB, 0x5E, 0x14, 0x50, 0,	//789*0# / 456 75% (T2) / 123 35% (T1)
	 75, 175, 0xEB, 0x5E, 0x14, 0x50, 0,	//789*0# / 456 70% (T2) / 123 30% (T1)
	 62, 162, 0xEB, 0x5E, 0x14, 0x50, 0,	//789*0# / 456 65% (T2) / 123 25% (T1)
	 50, 150, 0xEB, 0x5E, 0x14, 0x50, 0,	//789*0# / 456 60% (T2) / 123 20% (T1)
	 37, 137, 0xEB, 0x5E, 0x14, 0x50, 0,	//789*0# / 456 55% (T2) / 123 15% (T1)
	 25, 125, 0xEB, 0x5E, 0x14, 0x50, 0,	//789*0# / 456 50% (T2) / 123 10% (T1)
	 12, 112, 0xEB, 0x5E, 0x14, 0x50, 0,	//789*0# / 456 45% (T2) / 123  5% (T1)
	200, 100, 0xC3, 0x6C, 0x18, 0x90, 0,	//*0# / 789 80% (T1) / 456 40% (T2)
	187,  87, 0xC3, 0x6C, 0x18, 0x90, 0,	//*0# / 789 75% (T1) / 456 35% (T2)
	175,  75, 0xC3, 0x6C, 0x18, 0x90, 0,	//*0# / 789 70% (T1) / 456 30% (T2)
	162,  62, 0xC3, 0x6C, 0x18, 0x90, 0,	//*0# / 789 65% (T1) / 456 25% (T2)
	150,  50, 0xC3, 0x6C, 0x18, 0x90, 0,	//*0# / 789 60% (T1) / 456 20% (T2)
	137,  37, 0xC3, 0x6C, 0x18, 0x90, 0,	//*0# / 789 55% (T1) / 456 15% (T2)
	125,  25, 0xC3, 0x6C, 0x18, 0x90, 0,	//*0# / 789 50% (T1) / 456 10% (T2)
	112,  12, 0xC3, 0x6C, 0x18, 0x90, 0,	//*0# / 789 45% (T1) / 456  5% (T2)
	100, 200, 0x00, 0xE0, 0x38, 0xB0, 0,	//*0# 80% (T2) / 789 40% (T1)
	 87, 187, 0x00, 0xE0, 0x38, 0xB0, 0,	//*0# 75% (T2) / 789 35% (T1)
	 75, 175, 0x00, 0xE0, 0x38, 0xB0, 0,	//*0# 70% (T2) / 789 30% (T1)
	 62, 175, 0x00, 0xE0, 0x38, 0xB0, 0,	//*0# 65% (T2) / 789 25% (T1)
	 50, 162, 0x00, 0xE0, 0x38, 0xB0, 0,	//*0# 60% (T2) / 789 20% (T1)
	 37, 162, 0x00, 0xE0, 0x38, 0xB0, 0,	//*0# 55% (T2) / 789 15% (T1)
	 25, 150, 0x00, 0xE0, 0x38, 0xB0, 0,	//*0# 50% (T2) / 789 10% (T1)
	 12, 150, 0x00, 0xE0, 0x38, 0xB0, 0,	//*0# 45% (T2) / 789  5% (T1)
	 00, 137, 0x00, 0xC0, 0x30, 0x30, 0,	//*0# 40% (T2)
	 00, 137, 0x00, 0xC0, 0x30, 0x30, 0,	//*0# 35% (T2)
	 00, 125, 0x00, 0xC0, 0x30, 0x30, 0,	//*0# 30% (T2)
	 00, 112, 0x00, 0xC0, 0x30, 0x30, 0,	//*0# 25% (T2)
	 00, 100, 0x00, 0xC0, 0x30, 0x30, 0,	//*0# 20% (T2)
	 00,  87, 0x00, 0xC0, 0x30, 0x30, 0,	//*0# 15% (T2)
	 00,  81, 0x00, 0xC0, 0x30, 0x30, 0,	//*0# 10% (T2)
	 00,  75, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,  69, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,  62, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,  56, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,  50, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,  44, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,  37, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,  31, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,  25, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,  19, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,  12, 0x00, 0xC0, 0x30, 0x30, 0,	//*0#  5% (T2)
	 00,   0, 0x00, 0x00, 0x00, 0x00, 0,	//ALL Off
	LED_ENDD
};														/**< 모터 닫힘 시에 사용하는 Dimming LED Constand Data */


const BYTE gcbLedFPMOperation[]={
	00,   0, 0x00, 0x00, 0x00, 0x04, 0,
	LED_ENDD
};	


const BYTE gcbMakingDimmingData[] = {NULL};				/**< Dimming Data를 Constant Data를 바로 쓰는 게 아니라 생성하여 사용할 때 사용하는 Dimming LED Constand Data */



//------------------------------------------------------------------------------
/** 	@brief	I2C로 Dimming IC를 제어할 때 초기 설정하는 내용
	@param	[h_i2c_dev] : Sofrware I2C 제어 Handle
	@return 	None
	@remark Dimming IC Front에 장착되어 있어 안정성을 위해 Software I2C로 제어
*/
//------------------------------------------------------------------------------
void SetupDimmingLed( ddl_i2c_t *h_i2c_dev )	
{
	P_DIM_SCL(1);
	P_DIM_SDA(1);

	h_dimming_i2c_dev = h_i2c_dev;
}



//------------------------------------------------------------------------------
/** 	@brief	Dimming Data Packet을 I2C로 출력하는 내용
	@param	None
	@return 	None
	@remark LED Dimming 제어 Data 8byte 출력 (Control Regiser 0x12 + 8bytes Register Setting Data)
*/
//------------------------------------------------------------------------------
 void FLedWrite(void)
{
	gbFLedData_i2cbuff[ 0 ] = 0x12;
	ddl_i2c_write( h_dimming_i2c_dev, 0x12, 1, gbFLedData_i2cbuff, 9 );
}


void LedModeRefresh(void)
{
	if(GetLedMode())
	{
		gArrayDimmingQueueHead = 0;
		gArrayDimmingQueueTail = 0;

		gMakeDimmPrcsStep = 0;		

		gSeqLedQueueHead = 0;
		gSeqLedQueueTail = 0;
	}
}


//------------------------------------------------------------------------------
/** 	@brief	Dimming Data의 LSx SELECTOR register 설정을 위한 Data 가공
	@param	[pLSxBuf] 가공된 LSx 값을 저장하기 위한 포인터
	@param	[LedSwitchBit] 가공할 LED 지정 (LED0~LED15)
	@param	[ConvertMode] 가공할 LED의 값 지정 On, Off, Dimming)
	@return 	None
	@remark 특정 LED의 제어 내용(On, Off, Dimming)을 설정
*/
//------------------------------------------------------------------------------
void ConvertLedSwitchBitToLSxData(BYTE *pLSxBuf, BYTE LedSwitchBit, BYTE ConvertMode)
{
	switch(LedSwitchBit)
	{
		case LS0_LED0:	pLSxBuf[0] &= DimmTypeBuf[0][0]; if(ConvertMode)	pLSxBuf[0] |= DimmTypeBuf[ConvertMode][0];	break;			// DimmLED0 of LS0
		case LS0_LED1:	pLSxBuf[0] &= DimmTypeBuf[0][1]; if(ConvertMode)	pLSxBuf[0] |= DimmTypeBuf[ConvertMode][1];	break;			// DimmLED1 of LS0
		case LS0_LED2:	pLSxBuf[0] &= DimmTypeBuf[0][2]; if(ConvertMode)	pLSxBuf[0] |= DimmTypeBuf[ConvertMode][2];	break;			// DimmLED2 of LS0
		case LS0_LED3:	pLSxBuf[0] &= DimmTypeBuf[0][3]; if(ConvertMode)	pLSxBuf[0] |= DimmTypeBuf[ConvertMode][3];	break;			// DimmLED3 of LS0
		case LS1_LED4:	pLSxBuf[1] &= DimmTypeBuf[0][0]; if(ConvertMode)	pLSxBuf[1] |= DimmTypeBuf[ConvertMode][0];	break;			// DimmLED4 of LS1
		case LS1_LED5:	pLSxBuf[1] &= DimmTypeBuf[0][1]; if(ConvertMode)	pLSxBuf[1] |= DimmTypeBuf[ConvertMode][1];	break;			// DimmLED5 of LS1
		case LS1_LED6:	pLSxBuf[1] &= DimmTypeBuf[0][2]; if(ConvertMode)	pLSxBuf[1] |= DimmTypeBuf[ConvertMode][2];	break;			// DimmLED6 of LS1
		case LS1_LED7: 	pLSxBuf[1] &= DimmTypeBuf[0][3]; if(ConvertMode)	pLSxBuf[1] |= DimmTypeBuf[ConvertMode][3];	break;			// DimmLED7 of LS1
		case LS2_LED8:	pLSxBuf[2] &= DimmTypeBuf[0][0]; if(ConvertMode)	pLSxBuf[2] |= DimmTypeBuf[ConvertMode][0];	break;			// DimmLED8 of LS2
		case LS2_LED9:	pLSxBuf[2] &= DimmTypeBuf[0][1]; if(ConvertMode)	pLSxBuf[2] |= DimmTypeBuf[ConvertMode][1];	break;			// DimmLED9 of LS2
		case LS2_LED10:	pLSxBuf[2] &= DimmTypeBuf[0][2]; if(ConvertMode)	pLSxBuf[2] |= DimmTypeBuf[ConvertMode][2];	break;			// DimmLED10 of LS2
		case LS2_LED11:	pLSxBuf[2] &= DimmTypeBuf[0][3]; if(ConvertMode)	pLSxBuf[2] |= DimmTypeBuf[ConvertMode][3];	break;			// DimmLED11 of LS2
		case LS3_LED12:	pLSxBuf[3] &= DimmTypeBuf[0][0]; if(ConvertMode)	pLSxBuf[3] |= DimmTypeBuf[ConvertMode][0];	break;			// DimmLED12 of LS3
		case LS3_LED13: pLSxBuf[3] &= DimmTypeBuf[0][1]; if(ConvertMode)	pLSxBuf[3] |= DimmTypeBuf[ConvertMode][1];	break;			// DimmLED13 of LS3
		case LS3_LED14:	pLSxBuf[3] &= DimmTypeBuf[0][2]; if(ConvertMode)	pLSxBuf[3] |= DimmTypeBuf[ConvertMode][2];	break;			// DimmLED14 of LS3
		case LS3_LED15:	pLSxBuf[3] &= DimmTypeBuf[0][3]; if(ConvertMode)	pLSxBuf[3] |= DimmTypeBuf[ConvertMode][3];	break;			// DimmLED15 of LS3
		default:		break;
	}
}



void DimmOneKeyBlink(void);
void DimmLedDimming(void);

//------------------------------------------------------------------------------
/** 	@brief	Dimming Data 생성을 위한 기본 Process
	@param	None
	@return 	[0] : Dimming Data 설정 실패 (해당 경우는 없어야 함)
	@return 	[1] : Dimming Data 설정 성공
	@remark Dimming Data 종류 구조체에 따라 Dimming Data 생성을 위한 기본 Data 설정
*/
//------------------------------------------------------------------------------
BYTE MakeArrayDimmingDataBuf(void)
{
	switch(CurrentLedData.DimmDisplayType)
	{
		case LED_ALL_OFF:
		case LED_SELECT_ON:
		case BLINK_DIMM_ON:
		case BLINK_ON:
		case BLINK_OFF:
			DimmOneKeyBlink();
			return 1;

		case DIMM_ON_SLOW:
		case DIMM_ON_NORMAL:
		case DIMM_ON_FAST:
		case DIMM_OFF_SLOW:
		case DIMM_OFF_NORMAL:
		case DIMM_OFF_FAST:
			DimmLedDimming();
			return 1;
	}	

	return 0;
}



//------------------------------------------------------------------------------
/** 	@brief	생성할 Dimming Data 종류에 따라 기본 Data 처리
	@param	None
	@return 	None
	@remark LED_ALL_OFF, LED_SELECT_ON, BLINK_DIMM_ON, BLINK_ON, BLINK_OFF의 경우에 사용
*/
//------------------------------------------------------------------------------
void DimmOneKeyBlink(void)
{
	BYTE bCnt;

	memset(gLSxOnBuf, 0x00, 4);
	memset(gLSxOffBuf, 0x00, 4);
	for(bCnt = 0; bCnt < 16; bCnt++)
	{
		if(!Bit_Test(CurrentLedData.DimmLedNewSwitch, bCnt)) continue;

		ConvertLedSwitchBitToLSxData(gLSxOnBuf, bCnt, DIMM_COVERT_MODE_ON);

		if(CurrentLedData.DimmDisplayType)
		{
			ConvertLedSwitchBitToLSxData(gLSxOffBuf, bCnt, DIMM_COVERT_MODE_ON);
			// Blink를 하고 현재 숫자가 Blink할 숫자와 같다면
			if(CurrentLedData.DimmBlinkKey == bCnt)
			{
				if(CurrentLedData.DimmDisplayType == BLINK_DIMM_ON)
				{
					ConvertLedSwitchBitToLSxData(gLSxOnBuf, bCnt, DIMM_COVERT_MODE_DIMM);
				}
				else
				{
					ConvertLedSwitchBitToLSxData(gLSxOffBuf, bCnt, DIMM_COVERT_MODE_OFF);
				}
			}	
		}
	}

	if(CurrentLedData.DimmDisplayType == BLINK_DIMM_ON)
	{
		gNumberOfDimmStepMax = ONEKEY_DIM_BLINK_DATA_NO;
	}
	else if(CurrentLedData.DimmDisplayType == BLINK_ON)
	{
		gNumberOfDimmStepMax = 2;
	}
	else
	{
		gNumberOfDimmStepMax = 1;
	}
	gNumberOfDimmStepCnt = 0;
}



void MakeDimmLedSpeed(void);

//------------------------------------------------------------------------------
/** 	@brief	Dimming On이나 Off 처리 시 Dimming 초기 Data 설정
	@param	None
	@return 	None
	@remark 요청한 모드에 맞게 초기 PWM0(밝기) 및 Dimming 처리 단계(Dimming Resolution)을 설정
*/
//------------------------------------------------------------------------------
void DimmLedDimming(void)
{
	BYTE bCnt;

#ifdef	LED_LOW_BATTERY
	if(gfLowBattery)
	{
		CurrentLedData.DimmLedNewSwitch |= LED_LOW_BATTERY;
	}
#endif

#ifdef DDL_CFG_FP
	if(gbLedDisplayFPMOperation)
	{
		CurrentLedData.DimmLedNewSwitch |= LED_FPM_OPERATION;
	}
#endif

	memset(gLSxOnBuf, 0x00, 4);
	for(bCnt = 0; bCnt < 16; bCnt++)
	{
		if(!Bit_Test(CurrentLedData.DimmLedNewSwitch, bCnt)) continue;

		ConvertLedSwitchBitToLSxData(gLSxOnBuf, bCnt, DIMM_COVERT_MODE_DIMM);
	}

	gDimmLedLastSwitch = CurrentLedData.DimmLedNewSwitch;

	MakeDimmLedSpeed();
}




//------------------------------------------------------------------------------
/** 	@brief	Dimming On이나 Off 처리 시 Dimming 초기 Data 설정
	@param	None
	@return 	None
	@remark 요청한 모드에 맞게 초기 PWM0(밝기) 및 Dimming 처리 단계(Dimming Resolution)을 설정
	@remark gBrightness가 0일 경우 꺼진 상태, 250일 경우 켜진 상태
*/
//------------------------------------------------------------------------------
void MakeDimmLedSpeed(void)
{
	switch(CurrentLedData.DimmDisplayType)
	{
		case DIMM_ON_SLOW :		gBrightness = 0;	gNumberOfDimmStepMax = 25;	break;
		case DIMM_ON_NORMAL:	gBrightness = 0;	gNumberOfDimmStepMax = 17;	break;
		case DIMM_ON_FAST:		gBrightness = 0;	gNumberOfDimmStepMax = 12;	break;

		case DIMM_OFF_SLOW :	gBrightness = 250;	gNumberOfDimmStepMax = 25;	break;
		case DIMM_OFF_NORMAL:	gBrightness = 250;	gNumberOfDimmStepMax = 17;	break;
		case DIMM_OFF_FAST:		gBrightness = 250;	gNumberOfDimmStepMax = 12;	break;

		default:			gNumberOfDimmStepMax = 0;	break;
	}
	
	gNumberOfDimmStepCnt = 0;
}



void SelectKeyOnOff(BYTE OnOffMode);
void DimmTypeSelectKeyOffBlinkOnOff(void); 
void DimmTypeBlinkDimmOn(void); 
void DimmTypeDimmOn(void);
void DimmTypeDimmOff(void);


//------------------------------------------------------------------------------
/** 	@brief	설정된 Dimming Data의 기본 정보를 이용해 각각의 전송할 Dimming Data Packet 생성
	@param	None
	@return 	None
	@remark 한번에 전송하는 Dimming Data Packet 생성
*/
//------------------------------------------------------------------------------
void SetDimmVariablesToMakeArray(void)
{
	switch(CurrentLedData.DimmDisplayType)
	{
		case LED_SELECT_ON:
			SelectKeyOnOff(1);
			gNumberOfDimmStepCnt++;
			break;
					
		case LED_ALL_OFF:
		case BLINK_ON:
		case BLINK_OFF:
			DimmTypeSelectKeyOffBlinkOnOff();
			gNumberOfDimmStepCnt++;
			break;
			
		case BLINK_DIMM_ON:
			DimmTypeBlinkDimmOn();
			gNumberOfDimmStepCnt++;
			break;

		case DIMM_ON_SLOW:
		case DIMM_ON_NORMAL:
		case DIMM_ON_FAST:
			DimmTypeDimmOn();
			gNumberOfDimmStepCnt++;
			break;
			
		case DIMM_OFF_SLOW:
		case DIMM_OFF_NORMAL:
		case DIMM_OFF_FAST:
			DimmTypeDimmOff();
			gNumberOfDimmStepCnt++;
			break;
			
	}	
}



//------------------------------------------------------------------------------
/** 	@brief	설정된 LED Data를 켜거나 입력한 Key 값을 끌 때 사용
	@param	None
	@return 	None
	@remark 한번에 전송하는 Dimming Data Packet 생성
*/
//------------------------------------------------------------------------------
void SelectKeyOnOff(BYTE OnMode)
{
	if(OnMode == 0)
	{
		// 설정한 Key값의 LED를 끄거나 모든 LED를 끌 때 사용
		gTempDimmingDataBuf[0] = 0x00;
		gTempDimmingDataBuf[1] = 0x00;
		gTempDimmingDataBuf[2] = (gLSxOnBuf[0] & gLSxOffBuf[0]);
		gTempDimmingDataBuf[3] = (gLSxOnBuf[1] & gLSxOffBuf[1]);
		gTempDimmingDataBuf[4] = (gLSxOnBuf[2] & gLSxOffBuf[2]);
		gTempDimmingDataBuf[5] = (gLSxOnBuf[3] & gLSxOffBuf[3]);
		gTempDimmingDataBuf[6] = 0x08;			// 설정한 Key 값을 끄고 다시 켤 때 Delay를 주기 위해 (8*20ms = 160ms)
	}
	else
	{
		// 설정한 Key값의 LED를 다시 켤 때 사용
		gTempDimmingDataBuf[0] = 0x00;
		gTempDimmingDataBuf[1] = 0x00;
		gTempDimmingDataBuf[2] = gLSxOnBuf[0];
		gTempDimmingDataBuf[3] = gLSxOnBuf[1];
		gTempDimmingDataBuf[4] = gLSxOnBuf[2];
		gTempDimmingDataBuf[5] = gLSxOnBuf[3];
		gTempDimmingDataBuf[6] = 0x00;
	}


}


//------------------------------------------------------------------------------
/** 	@brief	설정된 LED Data를 끄거나 다시 껼 때, 또는 모든 LED를 끌 때 사용
	@param	None
	@return 	None
	@remark 선택된 Key LED를 끄고 다시 켤 때는 SelectKeyOnOff(0)+SelectKeyOnOff(1), 2단계 수행
	@remark 선택된 Key LED를 끌 때나 모든 LED를 끌 때는 SelectKeyOnOff(0), 1단계 수행
*/
//------------------------------------------------------------------------------
void DimmTypeSelectKeyOffBlinkOnOff(void) 
{
	if(gNumberOfDimmStepCnt == 0)
	{
		SelectKeyOnOff(0);
	}
	else
	{
		SelectKeyOnOff(1);
	}
}



//------------------------------------------------------------------------------
/** 	@brief	선택된 Key LED를 Dimming 방식으로 끄고 다시 껼 때 사용
	@param	None
	@return 	None
	@remark 비밀번호 입력 시 사용 가능
*/
//------------------------------------------------------------------------------
void DimmTypeBlinkDimmOn(void) 
{
	if(gNumberOfDimmStepCnt < ONEKEY_DIM_BLINK_DATA_NO)
	{
		gTempDimmingDataBuf[0] = DIMM[gNumberOfDimmStepCnt];
		gTempDimmingDataBuf[1] = 0x00;
		gTempDimmingDataBuf[2] = gLSxOnBuf[0];
		gTempDimmingDataBuf[3] = gLSxOnBuf[1];
		gTempDimmingDataBuf[4] = gLSxOnBuf[2];
		gTempDimmingDataBuf[5] = gLSxOnBuf[3];
		gTempDimmingDataBuf[6] = 0x00;
	}
}


//------------------------------------------------------------------------------
/** 	@brief	선택된 LED 전체를 Dimming 방식으로 껼 때 사용
	@param	None
	@return 	None
	@remark 전체 Keypad Dimming On 시 사용 가능
*/
//------------------------------------------------------------------------------
void DimmTypeDimmOn(void)
{
	if((gNumberOfDimmStepCnt+1) >= gNumberOfDimmStepMax)
	{
		//마지막에 켤 때는 무조건 250이 되도록 조치
		gBrightness = 250;
	}
	else
	{
		// Dimming 구동 단계에 따른 증가 Resolution 계산 & 수행
		gBrightness += 250/gNumberOfDimmStepMax;
	}

	gTempDimmingDataBuf[0] = gBrightness;
	gTempDimmingDataBuf[1] = 0x00;
	gTempDimmingDataBuf[2] = gLSxOnBuf[0];
	gTempDimmingDataBuf[3] = gLSxOnBuf[1];
	gTempDimmingDataBuf[4] = gLSxOnBuf[2];
	gTempDimmingDataBuf[5] = gLSxOnBuf[3];
	gTempDimmingDataBuf[6] = 0x00;
}


//------------------------------------------------------------------------------
/** 	@brief	선택된 LED 전체를 Dimming 방식으로 끌 때 사용
	@param	None
	@return 	None
	@remark 전체 Keypad Dimming Off 시 사용 가능
*/
//------------------------------------------------------------------------------
void DimmTypeDimmOff(void)
{
	if((gNumberOfDimmStepCnt+1) >= gNumberOfDimmStepMax)
	{
		//마지막에 끌 때는 무조건 0이 되도록 조치
		gBrightness = 0;
		// off 시에 마지막 data 가 update 가 되지 않으므로 여기서 LED 를 전부 죽여야 한다 
		memset(gLSxOnBuf, 0x00,4);
	}
	else
	{
		// Dimming 구동 단계에 따른 감소 Resolution 계산 & 수행
		gBrightness -= 250/gNumberOfDimmStepMax;
	}
	
	gTempDimmingDataBuf[0] = gBrightness;
	gTempDimmingDataBuf[1] = 0x00;
	gTempDimmingDataBuf[2] = gLSxOnBuf[0];
	gTempDimmingDataBuf[3] = gLSxOnBuf[1];
	gTempDimmingDataBuf[4] = gLSxOnBuf[2];
	gTempDimmingDataBuf[5] = gLSxOnBuf[3];
	gTempDimmingDataBuf[6] = 0x00;
}




WORD ExtraLedAddedForMakingData(void)
{
	BYTE bTmp;
	WORD wLedSwitch = 0;

	bTmp = GetFinalMotorStatus();
	if((bTmp == FINAL_MOTOR_STATE_OPEN_ERROR) || (bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR))
	{
		wLedSwitch |= LED_DEADBOLT_JAM;
	}

	if((bTmp == FINAL_MOTOR_STATE_OPEN) || (bTmp == FINAL_MOTOR_STATE_OPEN_ING))
	{
		wLedSwitch |= LED_OPERATION;
	}

#ifdef DDL_CFG_FP
	if(gbLedDisplayFPMOperation)
	{
		wLedSwitch |= LED_FPM_OPERATION; 
	}
#endif 

	if(gfLowBattery)
	{
		wLedSwitch |= LED_LOW_BATTERY;
	}

	return (wLedSwitch);
}



//------------------------------------------------------------------------------
/** 	@brief	Dimming Data 생성하여 구동하고자 할 경우 호출하는 함수
	@param	[LedSwitch] : 제어하고자 하는 LED 종류
	@param 	[BlinkOn] : 제어하고자 하는 LED 방식
	@param	[BlinkNum] : Blink하고자 하는 특정 Key 값
	@return	None
	@remark 생성하고자 하는 Dimming Data를 구조체로 만들어 Queue에 저장 
*/
//------------------------------------------------------------------------------
void LedGenerate(WORD LedSwitch, BYTE BlinkOn, BYTE BlinkNum)
{
	LedType_Diplay DimmData;

#ifdef	DDL_CFG_FP
	if(GetMainMode() == MODE_FINGER_VERIFY){
		gbLedDisplayFPMOperation = 0x01;
	}
	else 
	{
		gbLedDisplayFPMOperation = 0x00;
	}
#endif 
	
	DimmData.DimmLedNewSwitch = LedSwitch | ExtraLedAddedForMakingData();
	DimmData.DimmDisplayType = BlinkOn;
	DimmData.DimmBlinkKey = BlinkNum;

	DimmData.pLedBuf = (BYTE*)gcbMakingDimmingData;

	PutDataToSeqLedQueue(DimmData);

	gbLedMode = 10;
	gbLedCnt = 0;
}




//------------------------------------------------------------------------------
/** 	@brief	최종적으로 Display한 Dimming Data를 제어하고자 할 경우 사용
	@param	[DimmType] : 제어하고자 하는 LED 방식
	@return	None
	@remark  LED_ALL_OFF, DIMM_OFF_X 에만 사용, 즉 기존에 켜져 있던 것을 모두 끄고자 할 경우에 사용
*/
//------------------------------------------------------------------------------
void LastDimmingDisplayClear(BYTE DimmType)
{
	LedGenerate(gDimmLedLastSwitch, DimmType, 0);
}




//------------------------------------------------------------------------------
/** 	@brief	현재의 Dimming Data 구조체로 생성한 각각의 Packet을 Queue에 저장
	@param	None
	@return	[0] 더 이상 생성할 Packet이 없을 경우
	@return	[1] 생성할 Packet이 계속 있는 경우
	@remark  생성할 Packet이 더 이상 없을 경우에는 생성 Process를 종료할 수 있도록 처리
*/
//------------------------------------------------------------------------------
BYTE PutDataToArryDimmingQueue(void)
{
	BYTE bTmp;
	BYTE RetValue = 0;

	bTmp = (gArrayDimmingQueueHead+1);
	// & 처리로 순환을 구현할 경우 Masking할 수 있는 값으로만 MAX_DIMM_QUEUE 값을 설정해야 하는 한계가 있고,
	// % 처리로 구현할 경우에는 에러가 발생하여, 아래와 같이 처리
	if(bTmp >= MAX_DIMM_QUEUE)
	{
		bTmp = 0;
	}
	
	if(bTmp != gArrayDimmingQueueTail)
	{
		if(gNumberOfDimmStepCnt >= gNumberOfDimmStepMax)
		{
			memset(gTempDimmingDataBuf, 0x00, 7);
			gTempDimmingDataBuf[0] = LED_ENDD;
			RetValue = 1;
		}
		else
		{
			SetDimmVariablesToMakeArray();
		}
		
		memcpy(gArrayDimmingDataBuf[gArrayDimmingQueueHead], gTempDimmingDataBuf, 7);
		gArrayDimmingQueueHead++;
		if(gArrayDimmingQueueHead >= MAX_DIMM_QUEUE)
		{
			gArrayDimmingQueueHead = 0;
		}
	}

	return (RetValue);
}



//------------------------------------------------------------------------------
/** 	@brief	현재의 Dimming Data 구조체로 생성한 각각의 Packet을 Queue에서 Load
	@param	[TempFLedData] : Load한 Packet을 임시 저장할 버퍼
	@param	[TempLedCnt] : Load한 Packet이 실행되는 시간을 저장할 변수
	@return	[0] Load 성공
	@return	[1] Queue에 Load할 Data가 없어서 실패
	@remark Dimming Data를 IC로 출력하기 위해 Data Load
*/
//------------------------------------------------------------------------------
BYTE GetDataFromArryDimmingQueue(BYTE *TempFLedData, BYTE *TempLedCnt)
{
	if(gArrayDimmingQueueTail != gArrayDimmingQueueHead)
	{
		TempFLedData[0] = 0x00;
		TempFLedData[1] = gArrayDimmingDataBuf[gArrayDimmingQueueTail][0];
		TempFLedData[2] = 0x00;
		TempFLedData[3] = gArrayDimmingDataBuf[gArrayDimmingQueueTail][1];
		TempFLedData[4] = gArrayDimmingDataBuf[gArrayDimmingQueueTail][2];
		TempFLedData[5] = gArrayDimmingDataBuf[gArrayDimmingQueueTail][3];
		TempFLedData[6] = gArrayDimmingDataBuf[gArrayDimmingQueueTail][4];
		TempFLedData[7] = gArrayDimmingDataBuf[gArrayDimmingQueueTail][5];
		*TempLedCnt = gArrayDimmingDataBuf[gArrayDimmingQueueTail][6];	
	
		gArrayDimmingQueueTail++;
		if(gArrayDimmingQueueTail >= MAX_DIMM_QUEUE)
		{
			gArrayDimmingQueueTail = 0;
		}
		return 1;
	}
	return 0;
}




//------------------------------------------------------------------------------
/** 	@brief	현재의 Dimming Data 구조체로 Packet 생성하는 과정
	@param	None
	@return	None
	@remark 1. Dimming Packet 생성
			2. 생성된 Packet을 Queue에 저장
*/	
//------------------------------------------------------------------------------
void MakeDimmDataProcess(void)
{
	switch(gMakeDimmPrcsStep)
	{
		case 0:
			break;

		case 1:
			if(MakeArrayDimmingDataBuf())
			{
				gMakeDimmPrcsStep++;
				break;
			}
			gMakeDimmPrcsStep = 0;
			break;
		
		case 2:
			if(PutDataToArryDimmingQueue())
			{
				gMakeDimmPrcsStep = 0;
			}
			break;

		default:	
			gMakeDimmPrcsStep = 0;
			break;
	}
}



void ExtraLedAddedForConstData(BYTE *pLSxBuf, BYTE LedSwitchBit)
{
	BYTE bTmp;

	if(LedSwitchBit == FUNC_DEADBOLT_JAM)
	{
		bTmp = GetFinalMotorStatus();
		if((bTmp == FINAL_MOTOR_STATE_OPEN_ERROR) || (bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR))
		{
			ConvertLedSwitchBitToLSxData(pLSxBuf, FUNC_DEADBOLT_JAM, DIMM_COVERT_MODE_ON);
		}
	}
	else if(LedSwitchBit == FUNC_LOW_BATTERY)
	{
		if(gfLowBattery)
		{
			ConvertLedSwitchBitToLSxData(pLSxBuf, FUNC_LOW_BATTERY, DIMM_COVERT_MODE_ON);
		}
	}else if(LedSwitchBit == FUNC_OPERATION)
	{
		bTmp = GetFinalMotorStatus();
		if((bTmp == FINAL_MOTOR_STATE_OPEN) || (bTmp == FINAL_MOTOR_STATE_OPEN_ING))
		{
			ConvertLedSwitchBitToLSxData(pLSxBuf, FUNC_OPERATION, DIMM_COVERT_MODE_ON);
		}
	}
#ifdef DDL_CFG_FP
	else if(LedSwitchBit == FUNC_FPM_OPERATION)
	{
		if(gbLedDisplayFPMOperation)
		{
			ConvertLedSwitchBitToLSxData(pLSxBuf, FUNC_FPM_OPERATION, DIMM_COVERT_MODE_ON);
		}
	}
#endif 
}


//------------------------------------------------------------------------------
/** 	@brief	현재의 Dimming Data 구조체의 Packet을 생성하고, Queue에 저장하며, 출력하는 과정
	@param	None
	@return	None
	@remark 1. Dimming Packet 생성
			2. 생성된 Packet을 Queue에 저장
			3. 생성된 Packet을 Queue에서 읽어와 출력
*/	
//------------------------------------------------------------------------------
// 20ms마다 Dimming IC와 I2C 통신 수행
//ESD 시험에서 임펄스를 맞어 Dimming IC가 reset되더라도 LED 출력을 직전 상태를 유지하기 위해
//LED를 켜야할 상황이 아니어도 계속 I2C로 Dimming IC에 출력을 보냄


void LedProcess(void)
{
	BYTE TempFLedData[8];
	BYTE TempLedCnt = 0;

	// 생성된 Dimming Data 처리일 경우 Packet 생성 
	MakeDimmDataProcess();

	if(gbLedTimer2ms)		return;
	gbLedTimer2ms = 10;		//20ms

	switch(gbLedMode)
	{
		case 0:
			break;

		//LED 동작 유지 시간 계산
		case 1:
			if(gbLedCnt)
			{
				gbLedCnt--;
				break;
			}
			gbLedMode++;

		case 2:
			if(CurrentLedData.pLedBuf == gcbMakingDimmingData)
			{
				// 생성된 Dimming Data로 출력하고자 할 경우 처리
				if(GetDataFromArryDimmingQueue(TempFLedData, &TempLedCnt) == 0)	break;
				
				if(TempFLedData[1] == LED_ENDD)
				{
#ifdef	DDL_CFG_INSIDE_LED_LOW
					P_LED_LOW(0);
#endif	
					gbLedMode = 10;
#ifdef DDL_CFG_FP
					gbLedDisplayFPMOperation = 0x00;	
#endif 

				}
				else
				{
					memcpy(gbFLedData, TempFLedData, 8);
					gbLedCnt = TempLedCnt;

					gbLedMode = 1;	
				}
			}
			else
			{
				// Constant Dimming Data로 출력하고자 할 경우 처리 (기존 처리 방식과 동일)
				if(*CurrentLedData.pLedBuf == LED_ENDD)
				{
					if(!(gbLedDisplayMode & LED_KEEP_DISPLAY))
					{
						memset(gbFLedData, 0x00, 8);

#ifdef	DDL_CFG_INSIDE_LED_LOW
						P_LED_LOW(0);
#endif	
					}

#ifdef DDL_CFG_FP
					gbLedDisplayFPMOperation = 0x00;	
#endif 
					gbLedMode = 10;
				}
				else
				{
					BYTE LedOffData[8] = {0x00,};
					
					//LED 동작 종류 설정
					gbFLedData[0] = 0;
					gbFLedData[1] = *CurrentLedData.pLedBuf++;
					gbFLedData[2] = 0;
					gbFLedData[3] = *CurrentLedData.pLedBuf++;
					gbFLedData[4] = *CurrentLedData.pLedBuf++;		// LED Buffer Start Point
					gbFLedData[5] = *CurrentLedData.pLedBuf++;
					gbFLedData[6] = *CurrentLedData.pLedBuf++;
					gbFLedData[7] = *CurrentLedData.pLedBuf++;
					//LED 동작 유지 시간 설정
					gbLedCnt = *CurrentLedData.pLedBuf++;

					if(memcmp(gbFLedData,LedOffData,8) != 0) //off 시킬때는 더해 주지 않는다 
					{
						ExtraLedAddedForConstData(&gbFLedData[4], FUNC_DEADBOLT_JAM);
						ExtraLedAddedForConstData(&gbFLedData[4], FUNC_LOW_BATTERY);
						ExtraLedAddedForConstData(&gbFLedData[4], FUNC_OPERATION);
#ifdef DDL_CFG_FP
						ExtraLedAddedForConstData(&gbFLedData[4], FUNC_FPM_OPERATION);
#endif 
					
					}

#ifdef	DDL_CFG_INSIDE_LED_LOW
					if(gfLowBattery)
					{
						P_LED_LOW(1);
					}
#endif	
					gbLedMode = 1;	
				}	
			}
			break;

		case 10:
			// 생성된 Dimming Data이든, Constant Dimming Data이든 모두 Queue에 저장되므로 해당 Queue에 Data가 있는지 확인
			if(GetDataFromSeqLedQueue() == 0)
			{
				// 해당 Queue에 Data가 없을 경우 종료
				gMakeDimmPrcsStep = 0;		
				gbLedMode = 0;
				#ifdef	DDL_MAIN_OC_LED
					P_LED_OPEN(0);
					P_LED_CLOSE(0);

				#endif
				break;
			}

			if(CurrentLedData.pLedBuf == gcbMakingDimmingData)
			{
				// 해당 Queue에 생성된 Dimming Data를 이용하여 출력하는 Data가 있을 경우 해당 Process 수행
				gMakeDimmPrcsStep = 1;		
				gbLedMode = 2;
			}
			else if(CurrentLedData.pLedBuf == gcbWaitLedNextData)
			{
				// 해당 Queue에 Delay를 위한 Constant Data가 있을 경우 해당 값만큼 단순 Delay 수행
				gbLedCnt = CurrentLedData.WaitTimeForNextDisplay;	
				gbLedDisplayMode = CurrentLedData.DisplayMode;

				gMakeDimmPrcsStep = 0;		

				gbLedMode = 1;
			}
			else
			{
				// 해당 Queue에 Constant Dimming Data가 있을 경우 해당 Process 수행 (기존 처리 방식과 동일)
				gbLedDisplayMode = CurrentLedData.DisplayMode;

				gMakeDimmPrcsStep = 0;		
				gbLedMode = 2;
			}	
			break;

		default:
			memset(gbFLedData, 0x00, 8);
			gbLedMode = 0;
			break;
	}			


	// Dimming Data Packet을 I2C로 출력
	FLedWrite();
}


void LedForcedAllOff(void)
{
	memset(gbFLedData, 0x00, 8);

#ifdef	DDL_CFG_INSIDE_LED_LOW
	P_LED_LOW(0);
#endif	

	// Dimming Data Packet을 I2C로 출력
	FLedWrite();

	gArrayDimmingQueueHead = 0;
	gArrayDimmingQueueTail = 0;

	gMakeDimmPrcsStep = 0;		

	gSeqLedQueueHead = 0;
	gSeqLedQueueTail = 0;

	gbLedMode = 0;
}



//------------------------------------------------------------------------------
/** 	@brief	입력된 키에 대한 빠른 처리
	@param	None
	@return	None
	@remark 정상적으로 LED Data 생성하여 Queue 구조 활용할 경우 키 반응이 느려서 개선
	@remark 기존에 저장된 LED 출력 내용 무시하고 입력된 키 출력이 바로 되도록 처리
*/	
//------------------------------------------------------------------------------
/*
void DimmOneKeyFastBlink(BYTE KeyVal)
{
	BYTE bCnt;

	memset(gLSxOnBuf, 0x00, 4);
	memset(gLSxOffBuf, 0x00, 4);
	for(bCnt = 0; bCnt < 16; bCnt++)
	{
		if(!Bit_Test(gDimmLedLastSwitch, bCnt)) continue;

		ConvertLedSwitchBitToLSxData(gLSxOnBuf, bCnt, DIMM_COVERT_MODE_ON);

		ConvertLedSwitchBitToLSxData(gLSxOffBuf, bCnt, DIMM_COVERT_MODE_ON);

		if(KeyVal == bCnt)
		{
			ConvertLedSwitchBitToLSxData(gLSxOffBuf, bCnt, DIMM_COVERT_MODE_OFF);
		}	
	}

	gArrayDimmingDataBuf[0][0] = 0x00;
	gArrayDimmingDataBuf[0][1] = 0x00;
	gArrayDimmingDataBuf[0][2] = (gLSxOnBuf[0] & gLSxOffBuf[0]);
	gArrayDimmingDataBuf[0][3] = (gLSxOnBuf[1] & gLSxOffBuf[1]);
	gArrayDimmingDataBuf[0][4] = (gLSxOnBuf[2] & gLSxOffBuf[2]);
	gArrayDimmingDataBuf[0][5] = (gLSxOnBuf[3] & gLSxOffBuf[3]);
	gArrayDimmingDataBuf[0][6] = 0x08;			// 설정한 Key 값을 끄고 다시 켤 때 Delay를 주기 위해 (8*20ms = 160ms)

	// 설정한 Key값의 LED를 다시 켤 때 사용
	gArrayDimmingDataBuf[1][0] = 0x00;
	gArrayDimmingDataBuf[1][1] = 0x00;
	gArrayDimmingDataBuf[1][2] = gLSxOnBuf[0];
	gArrayDimmingDataBuf[1][3] = gLSxOnBuf[1];
	gArrayDimmingDataBuf[1][4] = gLSxOnBuf[2];
	gArrayDimmingDataBuf[1][5] = gLSxOnBuf[3];
	gArrayDimmingDataBuf[1][6] = 0x00;

	gArrayDimmingDataBuf[2][0] = LED_ENDD;

	gArrayDimmingQueueHead = 3;
	gArrayDimmingQueueTail = 0;

	gMakeDimmPrcsStep = 0;		

	gSeqLedQueueHead = 0;
	gSeqLedQueueTail = 0;

	gbLedMode = 2;
	gbLedCnt = 0;
}
*/





//==============================================================
//	TEST FUNCTIONS
//==============================================================
// 아래 함수는 LED Dimming 제어 함수가 수정되었을 때 제대로 동작하는지 여부를 확인하기 위한 테스트 함수임

#if 0
BYTE gDimmLedTestPrcsStep = 0;
BYTE gDimmLedTestTimer100ms = 0;


void DimmLedTestTimeCount(void)
{
	if(gDimmLedTestTimer100ms)	--gDimmLedTestTimer100ms;
}


void DimmLedTestProcess(void)
{
	if(GetLedMode())
	{
		gDimmLedTestTimer100ms = 5;
		return;
	}

	if(gDimmLedTestTimer100ms)	return;

	switch(gDimmLedTestPrcsStep)
	{
		case 0:
			LedGenerate(LED_ALL, DIMM_ON_SLOW, 0);
			LedSetting(gcbWaitLedNextData, 25);
			gDimmLedTestPrcsStep++;
			break;

		case 1:
			LastDimmingDisplayClear(DIMM_OFF_SLOW);
			LedSetting(gcbWaitLedNextData, 25);
			LedGenerate(LED_KEYPAD_ALL, DIMM_ON_SLOW, 0);
			gDimmLedTestPrcsStep++;
			break;
			
		case 2:
			LastDimmingDisplayClear(DIMM_OFF_SLOW);
			LedSetting(gcbWaitLedNextData, 25);
			LedGenerate(LED_TENKEY_ALL, DIMM_ON_SLOW, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 3:
			LastDimmingDisplayClear(DIMM_OFF_SLOW);
			LedSetting(gcbWaitLedNextData, 25);
			LedGenerate(LED_TENKEY_ALL, DIMM_ON_SLOW, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 4:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_1);
			gDimmLedTestPrcsStep++;
			break;

		case 5:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_2);
			gDimmLedTestPrcsStep++;
			break;

		case 6:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_3);
			gDimmLedTestPrcsStep++;
			break;

		case 7:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_4);
			gDimmLedTestPrcsStep++;
			break;

		case 8:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_5);
			gDimmLedTestPrcsStep++;
			break;

		case 9:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_6);
			gDimmLedTestPrcsStep++;
			break;

		case 10:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_7);
			gDimmLedTestPrcsStep++;
			break;

		case 11:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_8);
			gDimmLedTestPrcsStep++;
			break;

		case 12:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_9);
			gDimmLedTestPrcsStep++;
			break;

		case 13:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_0);
			gDimmLedTestPrcsStep++;
			break;

		case 14:
			LastDimmingDisplayClear(DIMM_OFF_SLOW);
			gDimmLedTestPrcsStep++;
			break;

		case 15:
			LedGenerate(LED_KEY_1, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_1, DIMM_OFF_FAST, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 16:
			LedGenerate(LED_KEY_2, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_2, DIMM_OFF_FAST, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 17:
			LedGenerate(LED_KEY_3, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_3, DIMM_OFF_FAST, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 18:
			LedGenerate(LED_KEY_4, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_4, DIMM_OFF_FAST, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 19:
			LedGenerate(LED_KEY_5, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_5, DIMM_OFF_FAST, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 20:
			LedGenerate(LED_KEY_6, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_6, DIMM_OFF_FAST, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 21:
			LedGenerate(LED_KEY_7, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_7, DIMM_OFF_FAST, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 22:
			LedGenerate(LED_KEY_8, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_8, DIMM_OFF_FAST, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 23:
			LedGenerate(LED_KEY_9, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_9, DIMM_OFF_FAST, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 24:
			LedGenerate(LED_KEY_0, DIMM_ON_FAST, 0);
			LedGenerate(LED_KEY_0, DIMM_OFF_FAST, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 25:
			LedSetting(gcbLedOk, 0);
			LedSetting(gcbWaitLedNextData, 25);
			gDimmLedTestPrcsStep++;
			break;

		case 26:
			LedGenerate(LED_KEYPAD_ALL, DIMM_ON_SLOW, 0);
			LedSetting(gcbWaitLedNextData, 25);
			gDimmLedTestPrcsStep++;
			break;

		case 27:
			LastDimmingDisplayClear(DIMM_OFF_FAST);
			LedGenerate(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5, DIMM_ON_SLOW, 0);
			LedSetting(gcbWaitLedNextData, 25);
			gDimmLedTestPrcsStep++;
			break;

		case 28:
			LastDimmingDisplayClear(DIMM_OFF_FAST);
			LedGenerate(LED_KEY_1|LED_KEY_3|LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_SLOW, 0);
			LedSetting(gcbWaitLedNextData, 25);
			gDimmLedTestPrcsStep++;
			break;

		case 29:
			LedGenerate(gDimmLedLastSwitch, BLINK_DIMM_ON, TENKEY_1);
			gDimmLedTestPrcsStep++;
			break;

		case 30:
			LastDimmingDisplayClear(DIMM_OFF_FAST);
			LedGenerate(LED_KEYPAD_ALL, DIMM_ON_SLOW, 0);
			LedSetting(gcbWaitLedNextData, 25);
			gDimmLedTestPrcsStep++;
			break;

		case 31:
			LedSetting(gcbLedErr, 0);
			LedSetting(gcbWaitLedNextData, 25);
			gDimmLedTestPrcsStep++;
			break;


		case 32:
			LedSetting(gcbWaitLedNextData, 25);
			LedGenerate(LED_TENKEY_ALL, DIMM_ON_SLOW, 0);
			gDimmLedTestPrcsStep++;
			break;

		case 33:
			LedGenerate(gDimmLedLastSwitch, BLINK_ON, TENKEY_1);
			gDimmLedTestPrcsStep++;
			break;

		case 34:
			LedGenerate(gDimmLedLastSwitch, BLINK_ON, TENKEY_2);
			gDimmLedTestPrcsStep++;
			break;

		case 35:
			LedGenerate(gDimmLedLastSwitch, BLINK_ON, TENKEY_3);
			gDimmLedTestPrcsStep++;
			break;

		case 36:
			LedGenerate(gDimmLedLastSwitch, BLINK_ON, TENKEY_4);
			gDimmLedTestPrcsStep++;
			break;

		case 37:
			LedGenerate(gDimmLedLastSwitch, BLINK_OFF, TENKEY_5);
			gDimmLedTestPrcsStep++;
			break;

		case 38:
			LedGenerate(gDimmLedLastSwitch, BLINK_OFF, TENKEY_6);
			gDimmLedTestPrcsStep++;
			break;

		case 39:
			LedGenerate(gDimmLedLastSwitch, BLINK_OFF, TENKEY_7);
			gDimmLedTestPrcsStep++;
			break;

		case 40:
			LedGenerate(gDimmLedLastSwitch, BLINK_OFF, TENKEY_8);
			gDimmLedTestPrcsStep++;
			break;

		case 41:
			LastDimmingDisplayClear(LED_ALL_OFF);
			gDimmLedTestPrcsStep++;
			break;

		case 42:
			LedGenerate(LED_KEY_7|LED_KEY_8|LED_KEY_9|LED_KEY_STAR|LED_KEY_SHARP, LED_SELECT_ON, 0);
			LedSetting(gcbWaitLedNextData, 25);
			gDimmLedTestPrcsStep++;
			break;

		case 43:
			LastDimmingDisplayClear(LED_ALL_OFF);
			gDimmLedTestPrcsStep = 0;
			break;

	}
}
#endif














FLAG LEDFlag;


BYTE gbLedPrcsTimer10ms = 0;

BYTE gcbLedMultilevelUI[20] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};




//==============================================================================//
//	LED CONSTANT								//
//==============================================================================//
//	PSC0 PWM0  PSC1 PWM1  LS0   LS1   LS2   LS3   TIME(30ms)
const BYTE gcbLedPwr[]={
	 10,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	 20,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	 30,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	 40,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	 50,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	 60,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	 70,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	 80,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	 90,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	100,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	115,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	130,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	145,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	160,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	175,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	190,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	210,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	230,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning
	245,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Oning

	254,  00, 0x55, 0x55, 0x55, 0x51, 15,	//ALL On

	240,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	220,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	200,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	180,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	160,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	140,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	120,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	100,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	 80,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	 60,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	 40,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	 20,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	  0,  00, 0xAA, 0xAA, 0xAA, 0xA2, 0,	//ALL Offing
	LED_ENDD
};


/////////////////////////////////////
const BYTE gcbLedNumOn1[]={
	 90, 170, 0x00, 0x01, 0x00, 0x00, 8,	// On
	 90, 170, 0x00, 0x03, 0x00, 0x00, 2,	// 70%
	 90, 170, 0x00, 0x02, 0x00, 0x00, 2,	// 30%
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};
const BYTE gcbLedNumOn2[]={
	 90, 170, 0x10, 0x00, 0x00, 0x00, 8,	// On
	 90, 170, 0x30, 0x00, 0x00, 0x00, 2,	// 70%
	 90, 170, 0x20, 0x00, 0x00, 0x00, 2,	// 30%
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};
const BYTE gcbLedNumOn3[]={
	 90, 170, 0x04, 0x00, 0x00, 0x00, 8,	// On
	 90, 170, 0x0C, 0x00, 0x00, 0x00, 2,	// 70%
	 90, 170, 0x08, 0x00, 0x00, 0x00, 2,	// 30%
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};
const BYTE gcbLedNumOn4[]={
	 90, 170, 0x00, 0x04, 0x00, 0x00, 8,	// On
	 90, 170, 0x00, 0x0C, 0x00, 0x00, 2,	// 70%
	 90, 170, 0x00, 0x08, 0x00, 0x00, 2,	// 30%
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};
const BYTE gcbLedNumOn5[]={
	 90, 170, 0x40, 0x00, 0x00, 0x00, 8,	// On
	 90, 170, 0xC0, 0x00, 0x00, 0x00, 2,	// 70%
	 90, 170, 0x80, 0x00, 0x00, 0x00, 2,	// 30%
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};
const BYTE gcbLedNumOn6[]={
	 90, 170, 0x01, 0x00, 0x00, 0x00, 8,	// On
	 90, 170, 0x03, 0x00, 0x00, 0x00, 2,	// 70%
	 90, 170, 0x02, 0x00, 0x00, 0x00, 2,	// 30%
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};
const BYTE gcbLedNumOn7[]={
	 90, 170, 0x00, 0x10, 0x00, 0x00, 8,	// On
	 90, 170, 0x00, 0x30, 0x00, 0x00, 2,	// 70%
	 90, 170, 0x00, 0x20, 0x00, 0x00, 2,	// 30%
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};
const BYTE gcbLedNumOn8[]={
	 90, 170, 0x00, 0x00, 0x04, 0x00, 8,	// On
	 90, 170, 0x00, 0x00, 0x0C, 0x00, 2,	// 70%
	 90, 170, 0x00, 0x00, 0x08, 0x00, 2,	// 30%
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};
const BYTE gcbLedNumOn9[]={
	 90, 170, 0x00, 0x00, 0x00, 0x40, 8,	// On
	 90, 170, 0x00, 0x00, 0x00, 0xC0, 2,	// 70%
	 90, 170, 0x00, 0x00, 0x00, 0x80, 2,	// 30%
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};
const BYTE gcbLedNumOn0[]={
	 90, 170, 0x00, 0x00, 0x10, 0x00, 8,	// On
	 90, 170, 0x00, 0x00, 0x30, 0x00, 2,	// 70%
	 90, 170, 0x00, 0x00, 0x20, 0x00, 2,	// 30%
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};
const BYTE gcbLedNumOnSharp[]={
 	90, 170, 0x00, 0x00, 0x00, 0x10, 8,	// On
	90, 170, 0x00, 0x00, 0x00, 0x30, 2,	// 70%
	90, 170, 0x00, 0x00, 0x00, 0x20, 2,	// 30%
	00,  00, 0x00, 0x00, 0x00, 0x00, 0,	// ALL Off
	LED_ENDD
};

const BYTE gcbLedOff[]={
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	//ALL Off
	LED_ENDD
};
const BYTE gcbLedDimmOn[]={
	 10,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	 20,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	 30,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	 40,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	 50,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	 60,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	 70,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	 80,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	 90,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	100,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	115,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	130,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	145,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	160,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	175,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	190,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	210,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	230,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	245,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Oning
	LED_ENDD
};

const BYTE gcbLedDimmOff[]={
	240,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	220,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	200,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	180,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	160,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	140,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	120,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	100,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	 80,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	 60,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	 40,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	 20,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	  0,  00, 0xAA, 0xAA, 0x28, 0xA0, 0,	//ALL Offing
	LED_ENDD
};

const BYTE gcbLedOutLock1[]={
	 00,  00, 0x14, 0x01, 0x00, 0x00, 4,	//123
	LED_ENDD
};
const BYTE gcbLedOutLock2[]={
	 00,  00, 0x55, 0x05, 0x00, 0x00, 4,	//123456
	LED_ENDD
};
const BYTE gcbLedOutLock3[]={
	 00,  00, 0x55, 0x15, 0x04, 0x40, 4,	//123456789
	LED_ENDD
};
const BYTE gcbLedOutLock4[]={
	 00,  00, 0x55, 0x55, 0x14, 0x50, 4,	//123456789*0#
	LED_ENDD
};
const BYTE gcbLedLock3Min1[]={
	 90, 170, 0x00, 0x01, 0x00, 0x00, 33,	//1
	 00,  00, 0x00, 0x00, 0x00, 0x00, 33,	//ALL Off
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	//ALL Off
	LED_ENDD
};
const BYTE gcbLedLock3Min2[]={
	 00,  00, 0x10, 0x00, 0x00, 0x00, 33,	//2
	 00,  00, 0x00, 0x00, 0x00, 0x00, 33,	//ALL Off
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	//ALL Off
	LED_ENDD
};
const BYTE gcbLedLock3Min3[]={
	 90, 170, 0x04, 0x00, 0x00, 0x00, 33,	//3
	 00,  00, 0x00, 0x00, 0x00, 0x00, 33,	//ALL Off
	 00,  00, 0x00, 0x00, 0x00, 0x00, 0,	//ALL Off
	LED_ENDD
};

//지문 입력을 지시하는 하향 화살표 에니메이션용 LED 설정 값들
const BYTE gcbLedFM[]={
	 00,  00, 0x50, 0x10, 0x14, 0x45, 0,	//BB on
	LED_ENDD
};
const BYTE gcbLedFM2[]={
	 00,  00, 0x51, 0x04, 0x04, 0x05, 0,	//BB on
	LED_ENDD
};
const BYTE gcbLedFM3[]={
	 00,  00, 0x54, 0x01, 0x00, 0x05, 0,	//BB on
	LED_ENDD
};
//지문 모듈 이상시 텐키 입력을 받게 하기 위해 숫자키 켜기용
const BYTE gcbLedFMERROR[]={
	00,  00, 0x55, 0x15, 0x14, 0x40, 0,	//BB on	
	LED_ENDD
};

//지문 입력을 할 때, 매회 진행을 표시하는데 사용되는 에니용 LED값
const BYTE gcbLedFMREG[]={
	 00,  00, 0x14, 0x01, 0x00, 0x05, 0,	//BB on
	LED_ENDD
};
const BYTE gcbLedFMREG1[]={
	 00,  00, 0x14, 0x00, 0x00, 0x05, 0,	//BB on
	LED_ENDD
};
const BYTE gcbLedFMREG2[]={
	 00,  00, 0x04, 0x01, 0x00, 0x05, 0,	//BB on
	LED_ENDD
};
const BYTE gcbLedFMREG3[]={
	 00,  00, 0x10, 0x01, 0x00, 0x05, 0,	//BB on
	LED_ENDD
};

#if defined (DDL_CFG_FP_PFM_3000) || defined (DDL_CFG_FP_TS1071M)
// 지문 등록을 할때 8회 등록에 대한 표시 값 
const BYTE gcbLedAREAFMREG[]={
	 00,  00, 0x55, 0x15, 0x04, 0x05, 0,	//BB on
	LED_ENDD
};

const BYTE gcbLedAREAFMREGSTEP[8][8]={
	{00,  00, 0x55, 0x14, 0x04, 0x05, 0, LED_ENDD},	//BB on
	{00,  00, 0x45, 0x15, 0x04, 0x05, 0, LED_ENDD},	//BB on
	{00,  00, 0x51, 0x15, 0x04, 0x05, 0, LED_ENDD},	//BB on
	{00,  00, 0x55, 0x11, 0x04, 0x05, 0, LED_ENDD},	//BB on
	{00,  00, 0x15, 0x15, 0x04, 0x05, 0, LED_ENDD},	//BB on
	{00,  00, 0x54, 0x15, 0x04, 0x05, 0, LED_ENDD},	//BB on
	{00,  00, 0x55, 0x05, 0x04, 0x05, 0, LED_ENDD},	//BB on
	{00,  00, 0x55, 0x15, 0x00, 0x05, 0, LED_ENDD},	//BB on	
};
#endif 

#if defined (DDL_CFG_FP_HFPM)
const BYTE gcbLedAREAFMREGSTEP[3][8]={
	{ 00,  00, 0x14, 0x00, 0x00, 0x05, 0, LED_ENDD},
	{ 00,  00, 0x04, 0x01, 0x00, 0x05, 0, LED_ENDD},
	{ 00,  00, 0x10, 0x01, 0x00, 0x05, 0, LED_ENDD}
};
#endif 


void MultilevelUILedGenerate(WORD UISwitch, BYTE BlinkOn, BYTE BlinkNum)
{
	BYTE LSon[4] = {0x00, 0x00, 0x00, 0x00};
	BYTE LSoff[4] = {0xff, 0xff, 0xff, 0xff};
	BYTE bCnt, LedOff;

	for(bCnt = 0; bCnt < 16; bCnt++)
	{
		if(!Bit_Test(UISwitch, bCnt)) continue;

		LedOff = ( (BlinkNum == bCnt) && BlinkOn ) ? 1 : 0;			// Blink를 하고 현재 숫자가 Blink할 숫자와 같다면

		switch(bCnt)
		{
			case TENKEY_6:		LSon[0] |= 0x01;	if(LedOff) LSoff[0] &= (BYTE)(~0x01);	break;			// DimmLED0 of LS0
			case TENKEY_3:		LSon[0] |= 0x04;	if(LedOff) LSoff[0] &= (BYTE)(~0x04);	break;			// DimmLED1 of LS0
			case TENKEY_2:		LSon[0] |= 0x10;	if(LedOff) LSoff[0] &= (BYTE)(~0x10);	break;			// DimmLED2 of LS0
			case TENKEY_5:		LSon[0] |= 0x40;	if(LedOff) LSoff[0] &= (BYTE)(~0x40);	break;			// DimmLED3 of LS0

			case TENKEY_1:		LSon[1] |= 0x01;	if(LedOff) LSoff[1] &= (BYTE)(~0x01);	break;			// DimmLED4 of LS1
			case TENKEY_4:		LSon[1] |= 0x04;	if(LedOff) LSoff[1] &= (BYTE)(~0x04);	break;			// DimmLED5 of LS1
			case TENKEY_7:		LSon[1] |= 0x10;	if(LedOff) LSoff[1] &= (BYTE)(~0x10);	break;			// DimmLED6 of LS1
/*  *  */		case TENKEY_STAR:	LSon[1] |= 0x40;	if(LedOff) LSoff[1] &= (BYTE)(~0x40);	break;			// DimmLED7 of LS1

///* cancle */	case 13 :	LSon[2] |= 0x01;	if(LedOff) LSoff[2] &= (BYTE)(~0x01);	break;			// DimmLED8 of LS2
			case TENKEY_8:		LSon[2] |= 0x04;	if(LedOff) LSoff[2] &= (BYTE)(~0x04);	break;			// DimmLED9 of LS2
			case TENKEY_0:		LSon[2] |= 0x10;	if(LedOff) LSoff[2] &= (BYTE)(~0x10);	break;			// DimmLED10 of LS2

/*#*/		case TENKEY_SHARP:	LSon[3] |= 0x10;	if(LedOff) LSoff[3] &= (BYTE)(~0x10);	break;			// DimmLED14 of LS3
			case TENKEY_9:		LSon[3] |= 0x40;	if(LedOff) LSoff[3] &= (BYTE)(~0x40);	break;			// DimmLED15 of LS3

			default:			break;
		}
	}

	bCnt = 0;
	
	gcbLedMultilevelUI[bCnt++] = 0x00;						// PWM0
	gcbLedMultilevelUI[bCnt++] = 0x00;						// PWM1
	gcbLedMultilevelUI[bCnt++] = ( LSon[0] & LSoff[0] );	// LED0 ~ LED3
	gcbLedMultilevelUI[bCnt++] = ( LSon[1] & LSoff[1] );	// LED4 ~ LED7
	gcbLedMultilevelUI[bCnt++] = ( LSon[2] & LSoff[2] );	// LED8 ~ LED11
	gcbLedMultilevelUI[bCnt++] = ( LSon[3] & LSoff[3] );	// LED12 ~ LED15 

	if(BlinkOn&&(BlinkNum!=0xFF))
	{
		gcbLedMultilevelUI[bCnt++] = 0x08;					// Keep Time
		gcbLedMultilevelUI[bCnt++] = 0x00;					// PWM0 
		gcbLedMultilevelUI[bCnt++] = 0x00;					// PWM1
		gcbLedMultilevelUI[bCnt++] = LSon[0];				// LED0 ~ LED3
		gcbLedMultilevelUI[bCnt++] = LSon[1];				// LED4 ~ LED7
		gcbLedMultilevelUI[bCnt++] = LSon[2];				// LED8 ~ LED11
		gcbLedMultilevelUI[bCnt++] = LSon[3];				// LED12 ~ LED15
	}

	gcbLedMultilevelUI[bCnt++] = 0x00;
	gcbLedMultilevelUI[bCnt++] = LED_ENDD;

}


/*
void LedKeyNumSetting(BYTE bKeyValue)
{
	MultilevelUILedGenerate((WORD)(KEYNUMALL|KEYSTARSHARP), BLINK_ON, bKeyValue);
	LedSetting((BYTE*)gcbLedMultilevelUI, LED_KEEP_DISPLAY);
}
*/
void LedRecCountSetting(BYTE bcount, BYTE bMode)
{
	switch(bcount)
	{
		case 0:
			break;
		case 1:case 2:case 3:case 4:case 5:case 6:case 7:case 8:case 9:
			MultilevelUILedGenerate((WORD)(KEY1<<(bcount-1)), BLINK_OFF, 0);
			break;
		case 10:
			MultilevelUILedGenerate((WORD)(KEYSHARP|KEY0), BLINK_OFF, 0);
			break;
		case 11:case 12:case 13:case 14:case 15:case 16:case 17:case 18:case 19:
			MultilevelUILedGenerate((WORD)(KEYSHARP|(KEY1<<(bcount-11))), BLINK_OFF, 0);
			break;
		case 20:
			MultilevelUILedGenerate((WORD)(KEYSTAR|KEY0), BLINK_OFF, 0);
			break;
		case 21:	case 22:case 23:case 24:case 25:case 26:case 27:case 28:case 29:
			MultilevelUILedGenerate((WORD)(KEYSTAR|(KEY1<<(bcount-21))), BLINK_OFF, 0);
			break;
		case 30:
			MultilevelUILedGenerate((WORD)(KEYSHARP|KEYSTAR|KEY0), BLINK_OFF, 0);
			break;
		case 31:case 32:case 33:case 34:case 35:case 36:case 37:case 38:case 39:
			MultilevelUILedGenerate((WORD)(KEYSHARP|KEYSTAR|(KEY1<<(bcount-31))), BLINK_OFF, 0);
			break;
		case 40:
			MultilevelUILedGenerate((WORD)(KEYSHARP|KEYSTAR|KEY0|KEY4), BLINK_OFF, 0);
			break;


	}
	LedSetting((BYTE*)gcbLedMultilevelUI, bMode);
}




void LedIndivLedSetting(BYTE bNum)
{
	if(bNum == 0)		gbFLedData[6] |= 0x10;
	else if(bNum == 1)	gbFLedData[5] |= 0x01;	
	else if(bNum == 2)	gbFLedData[4] |= 0x10;	
	else if(bNum == 3)	gbFLedData[4] |= 0x04;	
	else if(bNum == 4)	gbFLedData[5] |= 0x04;	
	else if(bNum == 5)	gbFLedData[4] |= 0x40;	
	else if(bNum == 6)	gbFLedData[4] |= 0x01;	
	else if(bNum == 7)	gbFLedData[5] |= 0x10;	
	else if(bNum == 8)	gbFLedData[6] |= 0x04;	
	else if(bNum == 9)	gbFLedData[7] |= 0x40;	
	else if(bNum == 10)	gbFLedData[5] |= 0x40;	//*
	else if(bNum == 11)	gbFLedData[7] |= 0x10;	//#
	else if(bNum == 12)	gbFLedData[6] |= 0x40;	//LOW
	else if(bNum == 13)	gbFLedData[6] |= 0x01;	
	else if(bNum == 14)	gbFLedData[7] |= 0x04;	
	else if(bNum == 15)	gbFLedData[7] |= 0x01;	
}


void LedPWLedSetting(BYTE bcount)
{
	if(bcount == 0)		LedSetting(gcbLedNumOn0, 0);
	else if(bcount == 1)	LedSetting(gcbLedNumOn1, 0);
	else if(bcount == 2)	LedSetting(gcbLedNumOn2, 0);
	else if(bcount == 3)	LedSetting(gcbLedNumOn3, 0);
	else if(bcount == 4)	LedSetting(gcbLedNumOn4, 0);
	else if(bcount == 5)	LedSetting(gcbLedNumOn5, 0);
	else if(bcount == 6)	LedSetting(gcbLedNumOn6, 0);
	else if(bcount == 7)	LedSetting(gcbLedNumOn7, 0);
	else if(bcount == 8)	LedSetting(gcbLedNumOn8, 0);
	else if(bcount == 9)	LedSetting(gcbLedNumOn9, 0);
	else if(bcount == 12)	LedSetting(gcbLedNumOnSharp, 0);
}



#endif		// End of "false of ifdef DDL_CFG_DIMMER"

void LedOTAReady(void)
{
#ifdef	DDL_CFG_DIMMER
	uint8_t Ledbuffer[9];
	Ledbuffer[ 0 ] = 0x12;
	Ledbuffer[ 1 ] = 100;
	Ledbuffer[ 2 ] = 200;
	Ledbuffer[ 3 ] = 100;
	Ledbuffer[ 4 ] = 200;
	Ledbuffer[ 5 ] = 0x2A;
	Ledbuffer[ 6 ] = 0x2A;
	Ledbuffer[ 7 ] = 0x08;
	Ledbuffer[ 8 ] = 0x80;

	ddl_i2c_write( h_dimming_i2c_dev, 0x12, 1, Ledbuffer, 9);
#else
	/*EEPROM 에 기록을 해서 bootloader 가 port 와 pin 번호를 가지고 LED 켜준다. */
#if 0
	if(LED_TENKEY_GPIO_Port == GPIOA)
	{
		// port 위치 저장 
		// GPIOA  == 0x01 
	}
	else if(LED_TENKEY_GPIO_Port == GPIOB)
	{
		// port 기록
		// GPIOA  == 0x02 
	}
	else if(LED_TENKEY_GPIO_Port == GPIOC)
	{
		// port 기록
		// GPIOC  == 0x03 
	}
	else if(LED_TENKEY_GPIO_Port == GPIOD)
	{
		// port 기록
		// GPIOD  == 0x04 
	}
	else if(LED_TENKEY_GPIO_Port == GPIOE)
	{
		// port 기록
		// GPIOE  == 0x05 
	}
	else
	{
		// port 기록
		// GPIOH  == 0x06 
	}

	for(uint8_t i = 0 ; i < 15 ; i++)
	{
		if(LED_TENKEY_Pin & (1 << i))
		{
			/*pin 번호 저장 */

			break;
		}
	}
#endif 	
#endif 
}


#ifdef	DDL_CFG_LED_TYPE1
// LED_DECO, LED_BACK이 있는 경우


BYTE	*gbFLedData;			/**< 출력할 LED Data의 포인터 */


const BYTE gcbNoDimLed100msOn[]={
	NODIM_LED_DECO_ON, 	5,		
	LED_ENDD
};

const BYTE gcbNoDimLed2sOn[]={
//	NODIM_LED_ALL_ON, 100,	
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_ALL_ON, 50,		
	LED_ENDD
};

const BYTE gcbNoDimLedBlink[]={
	NODIM_LED_DECO_OFF, 3,		
	NODIM_LED_DECO_ON, 	0,		
	LED_ENDD
};

const BYTE gcbNoDimLedTamperLockout[]={
	NODIM_LED_DECO_ON, 	50,		
	NODIM_LED_DECO_OFF, 100,	
	LED_ENDD
};

const BYTE gcbNoDimLedOff[]={
	NODIM_LED_ALL_OFF, 	3,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr3[]={
	NODIM_LED_ALL_ON, 7,		
	NODIM_LED_ALL_OFF, 7,		
	NODIM_LED_ALL_ON, 7,		
	NODIM_LED_ALL_OFF, 7,		
	NODIM_LED_ALL_ON, 7,		
	NODIM_LED_ALL_OFF, 0,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr4[]={
	NODIM_LED_ALL_ON, 7,		
	NODIM_LED_ALL_OFF, 7,		
	NODIM_LED_ALL_ON, 7,		
	NODIM_LED_ALL_OFF, 7,		
	NODIM_LED_ALL_ON, 7,		
	NODIM_LED_ALL_OFF, 7,		
	NODIM_LED_ALL_ON, 7,		
	NODIM_LED_ALL_OFF, 0,		
	LED_ENDD
};




void SetupLed(void)
{
	P_LED_DECO(0);
	P_LED_TENKEY(0);
}


void LedModeRefresh(void)
{
	if(GetLedMode())
	{
		gSeqLedQueueHead = 0;
		gSeqLedQueueTail = 0;
	}
}


void LedOutputProcess(BYTE LedOutMode)
{
	switch(LedOutMode)
	{
		case NODIM_LED_ALL_ON:
			P_LED_DECO(1);
			P_LED_TENKEY(1);
			break;		

		case NODIM_LED_ALL_OFF:
			P_LED_DECO(0);
			P_LED_TENKEY(0);
			break;		

		case NODIM_LED_DECO_ON:
			P_LED_DECO(1);
			break;		
	
		case NODIM_LED_DECO_OFF:
			P_LED_DECO(0);
			break;		
	}
}



//------------------------------------------------------------------------------
/** 	@brief	출력 요청된 LED에 대한 출력 처리
	@param	None
	@return	None
	@remark LED 출력 처리
*/	
//------------------------------------------------------------------------------
// 20ms마다 LED 출력 처리 - Dimming IC와 제어 형태와 동일하게 처리하기 위해
void LedProcess(void)
{
	if(gbLedTimer2ms)		return;
	gbLedTimer2ms = 10;		//20ms

	switch(gbLedMode)
	{
		case 0:
			break;

		//LED 동작 유지 시간 계산
		case 1:
			if(gbLedCnt)
			{
				gbLedCnt--;
				break;
			}
			gbLedMode++;

		case 2:
			// Constant LED Data로 출력하고자 할 경우 처리 (기존 처리 방식과 동일)
			if(*CurrentLedData.pLedBuf == LED_ENDD)
			{
				if(!(gbLedDisplayMode & LED_KEEP_DISPLAY))
				{
					LedOutputProcess(NODIM_LED_ALL_OFF);
				}
				
#ifdef	DDL_CFG_INSIDE_LED_LOW
				P_LED_LOW(0);
#endif	
				gbLedMode = 10;
			}
			else
			{
				//LED 동작 종류 설정
				LedOutputProcess(*CurrentLedData.pLedBuf++);
				//LED 동작 유지 시간 설정
				gbLedCnt = *CurrentLedData.pLedBuf++;
#ifdef	DDL_CFG_INSIDE_LED_LOW
				if(gfLowBattery)
				{
					P_LED_LOW(1);
				}
#endif	
				gbLedMode = 1;	
			}	
			break;

		case 10:
			// Constant LED Data가 Queue에 저장되므로 해당 Queue에 Data가 있는지 확인
			if(GetDataFromSeqLedQueue() == 0)
			{
				// 해당 Queue에 Data가 없을 경우 종료
				gbLedMode = 0;
				break;
			}

			if(CurrentLedData.pLedBuf == gcbWaitLedNextData)
			{
				// 해당 Queue에 Delay를 위한 Constant Data가 있을 경우 해당 값만큼 단순 Delay 수행
				gbLedCnt = CurrentLedData.WaitTimeForNextDisplay;	
				gbLedDisplayMode = CurrentLedData.DisplayMode;

				gbLedMode = 1;
			}
			else
			{
				gbLedDisplayMode = CurrentLedData.DisplayMode;
				// 해당 Queue에 Constant LED Data가 있을 경우 해당 Process 수행 (기존 처리 방식과 동일)
				gbLedMode = 2;
			}	
			break;


		default:
			LedOutputProcess(NODIM_LED_ALL_OFF);
			gbLedMode = 0;
			break;
	}			
}


void LedForcedAllOff(void)
{
#ifdef	DDL_CFG_INSIDE_LED_LOW
	P_LED_LOW(0);
#endif	

	P_LED_DECO(0);
	P_LED_TENKEY(0);

	gSeqLedQueueHead = 0;
	gSeqLedQueueTail = 0;
	gbLedMode = 0;
}





#endif		// End of "false of ifdef DDL_CFG_LED_TYPE1"




#ifdef	DDL_CFG_LED_TYPE2
// LED_TENKEY, LED_LOWBAT, LED_DEAD가 있는 경우


BYTE	*gbFLedData;			/**< 출력할 LED Data의 포인터 */


const BYTE gcbNoDimLed100msOn[]={
	NODIM_LED_BACK_ON, 	5,		
	LED_ENDD
};

const BYTE gcbNoDimAllLed2sOn[]={
	NODIM_LED_ALL_ON, 100,		
	LED_ENDD
};

const BYTE gcbNoDimLed2sOn[]={
//	NODIM_LED_BACK_ON, 100,		
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_BACK_ON, 50,		
	LED_ENDD
};

const BYTE gcbNoDimLedBlink[]={
	NODIM_LED_BACK_OFF, 3,		
	NODIM_LED_BACK_ON, 	0,		
	LED_ENDD
};

const BYTE gcbNoDimLedTamperLockout[]={
	NODIM_LED_BACK_ON, 	50,		
	NODIM_LED_ALL_OFF, 100,	
	LED_ENDD
};

const BYTE gcbNoDimLedOff[]={
	NODIM_LED_ALL_OFF, 	3,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr3[]={
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 0,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr4[]={
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 0,		
	LED_ENDD
};




void SetupLed(void)
{
	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	P_LED_DEAD(0);
}


void LedModeRefresh(void)
{
	if(GetLedMode())
	{
		gSeqLedQueueHead = 0;
		gSeqLedQueueTail = 0;
	}
}


void LedOutputProcess(BYTE LedOutMode)
{
	BYTE bTmp;

	bTmp = GetFinalMotorStatus();
	if((bTmp == FINAL_MOTOR_STATE_OPEN_ERROR) || (bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR))
	{
		P_LED_DEAD(1);
	}
	
	if(gfLowBattery)
	{
		P_LED_LOWBAT(1);
	}

	switch(LedOutMode)
	{
		case NODIM_LED_ALL_ON:
			P_LED_TENKEY(1);
			P_LED_LOWBAT(1);
			P_LED_DEAD(1);
			break;		

		case NODIM_LED_ALL_OFF:
			P_LED_TENKEY(0);
			P_LED_LOWBAT(0);
			P_LED_DEAD(0);
			break;		

		case NODIM_LED_BACK_ON:
			P_LED_TENKEY(1);
			break;		

		case NODIM_LED_BACK_OFF:
			P_LED_TENKEY(0);
			break;		
	}
}



//------------------------------------------------------------------------------
/** 	@brief	출력 요청된 LED에 대한 출력 처리
	@param	None
	@return	None
	@remark LED 출력 처리
*/	
//------------------------------------------------------------------------------
// 20ms마다 LED 출력 처리 - Dimming IC와 제어 형태와 동일하게 처리하기 위해
void LedProcess(void)
{
	if(gbLedTimer2ms)		return;
	gbLedTimer2ms = 10;		//20ms

	switch(gbLedMode)
	{
		case 0:
			break;

		//LED 동작 유지 시간 계산
		case 1:
			if(gbLedCnt)
			{
				gbLedCnt--;
				break;
			}
			gbLedMode++;

		case 2:
			// Constant LED Data로 출력하고자 할 경우 처리 (기존 처리 방식과 동일)
			if(*CurrentLedData.pLedBuf == LED_ENDD)
			{
				if(!(gbLedDisplayMode & LED_KEEP_DISPLAY))
				{
					LedOutputProcess(NODIM_LED_ALL_OFF);
				}
				
#ifdef	DDL_CFG_INSIDE_LED_LOW
				P_LED_LOW(0);
#endif	
				gbLedMode = 10;
			}
			else
			{
				//LED 동작 종류 설정
				LedOutputProcess(*CurrentLedData.pLedBuf++);
				//LED 동작 유지 시간 설정
				gbLedCnt = *CurrentLedData.pLedBuf++;

#ifdef	DDL_CFG_INSIDE_LED_LOW
				if(gfLowBattery)
				{
					P_LED_LOW(1);
				}
#endif	
				gbLedMode = 1;	
			}	
			break;

		case 10:
			// Constant LED Data가 Queue에 저장되므로 해당 Queue에 Data가 있는지 확인
			if(GetDataFromSeqLedQueue() == 0)
			{
				// 해당 Queue에 Data가 없을 경우 종료
				gbLedMode = 0;
				break;
			}

			if(CurrentLedData.pLedBuf == gcbWaitLedNextData)
			{
				// 해당 Queue에 Delay를 위한 Constant Data가 있을 경우 해당 값만큼 단순 Delay 수행
				gbLedCnt = CurrentLedData.WaitTimeForNextDisplay;	
				gbLedDisplayMode = CurrentLedData.DisplayMode;

				gbLedMode = 1;
			}
			else
			{
				gbLedDisplayMode = CurrentLedData.DisplayMode;
				// 해당 Queue에 Constant LED Data가 있을 경우 해당 Process 수행 (기존 처리 방식과 동일)
				gbLedMode = 2;
			}	
			break;


		default:
			LedOutputProcess(NODIM_LED_ALL_OFF);
			gbLedMode = 0;
			break;
	}			
}



void LedForcedAllOff(void)
{
#ifdef	DDL_CFG_INSIDE_LED_LOW
	P_LED_LOW(0);
#endif	

	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	P_LED_DEAD(0);

	gSeqLedQueueHead = 0;
	gSeqLedQueueTail = 0;
	gbLedMode = 0;
}




#endif		// End of "false of ifdef DDL_CFG_LED_TYPE2"





#ifdef	DDL_CFG_LED_TYPE3
// LED_TENKEY, LED_LOWBAT, LED_DEAD, LED_IB가 있는 경우


BYTE	*gbFLedData;			/**< 출력할 LED Data의 포인터 */


const BYTE gcbNoDimLed100msOn[]={
	NODIM_LED_DECO_ON, 	5,		
	LED_ENDD
};

const BYTE gcbNoDimAllLed2sOn[]={
	NODIM_LED_ALL_ON, 100,		
	LED_ENDD
};

const BYTE gcbNoDimLed2sOn[]={
//	NODIM_LED_BACKDECO_ON, 100,		
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_BACKDECO_ON, 50,		
	LED_ENDD
};

const BYTE gcbNoDimLedBlink[]={
	NODIM_LED_DECO_OFF, 3,		
	NODIM_LED_DECO_ON, 	0,		
	LED_ENDD
};

const BYTE gcbNoDimLedBlinkWithBack[]={
	NODIM_LED_BACKDECO_OFF, 3,		
	NODIM_LED_BACKDECO_ON, 	0,		
	LED_ENDD
};

const BYTE gcbNoDimLedTamperLockout[]={
	NODIM_LED_BACKDECO_ON, 	50,		
	NODIM_LED_ALL_OFF, 100,	
	LED_ENDD
};

const BYTE gcbNoDimLedOff[]={
	NODIM_LED_ALL_OFF, 	3,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr3[]={
	NODIM_LED_BACKDECO_ON, 7,		
	NODIM_LED_BACKDECO_OFF, 7,		
	NODIM_LED_BACKDECO_ON, 7,		
	NODIM_LED_BACKDECO_OFF, 7,		
	NODIM_LED_BACKDECO_ON, 7,		
	NODIM_LED_BACKDECO_OFF, 0,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr4[]={
	NODIM_LED_BACKDECO_ON, 7,		
	NODIM_LED_BACKDECO_OFF, 7,		
	NODIM_LED_BACKDECO_ON, 7,		
	NODIM_LED_BACKDECO_OFF, 7,		
	NODIM_LED_BACKDECO_ON, 7,		
	NODIM_LED_BACKDECO_OFF, 7,		
	NODIM_LED_BACKDECO_ON, 7,		
	NODIM_LED_BACKDECO_OFF, 0,		
	LED_ENDD
};




void SetupLed(void)
{
	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	P_LED_DEAD(0);
	P_LED_IB(0);
}


void LedModeRefresh(void)
{
	if(GetLedMode())
	{
		gSeqLedQueueHead = 0;
		gSeqLedQueueTail = 0;
	}
}


void LedOutputProcess(BYTE LedOutMode)
{
	BYTE bTmp;

	bTmp = GetFinalMotorStatus();
	if((bTmp == FINAL_MOTOR_STATE_OPEN_ERROR) || (bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR))
	{
		P_LED_DEAD(1);
	}
	
	if(gfLowBattery)
	{
		P_LED_LOWBAT(1);
	}

	switch(LedOutMode)
	{
		case NODIM_LED_ALL_ON:
			P_LED_TENKEY(1);
			P_LED_LOWBAT(1);
			P_LED_DEAD(1);
			P_LED_IB(1);
			break;		

		case NODIM_LED_ALL_OFF:
			P_LED_TENKEY(0);
			P_LED_LOWBAT(0);
			P_LED_DEAD(0);
			P_LED_IB(0);
			break;		

		case NODIM_LED_BACK_ON:
			P_LED_TENKEY(1);
			break;		

		case NODIM_LED_BACK_OFF:
			P_LED_TENKEY(0);
			break;		

		case NODIM_LED_DECO_ON:
			P_LED_IB(1);
			break;		
	
		case NODIM_LED_DECO_OFF:
			P_LED_IB(0);
			break;		

		case NODIM_LED_BACKDECO_ON:
			P_LED_TENKEY(1);
			P_LED_IB(1);
			break;		
		
		case NODIM_LED_BACKDECO_OFF:
			P_LED_TENKEY(0);
			P_LED_IB(0);
			break;		
	}
}



//------------------------------------------------------------------------------
/** 	@brief	출력 요청된 LED에 대한 출력 처리
	@param	None
	@return	None
	@remark LED 출력 처리
*/	
//------------------------------------------------------------------------------
// 20ms마다 LED 출력 처리 - Dimming IC와 제어 형태와 동일하게 처리하기 위해
void LedProcess(void)
{
	if(gbLedTimer2ms)		return;
	gbLedTimer2ms = 10;		//20ms

	switch(gbLedMode)
	{
		case 0:
			break;

		//LED 동작 유지 시간 계산
		case 1:
			if(gbLedCnt)
			{
				gbLedCnt--;
				break;
			}
			gbLedMode++;

		case 2:
			// Constant LED Data로 출력하고자 할 경우 처리 (기존 처리 방식과 동일)
			if(*CurrentLedData.pLedBuf == LED_ENDD)
			{
				if(!(gbLedDisplayMode & LED_KEEP_DISPLAY))
				{
					LedOutputProcess(NODIM_LED_ALL_OFF);
				}
				
#ifdef	DDL_CFG_INSIDE_LED_LOW
				P_LED_LOW(0);
#endif	
				gbLedMode = 10;
			}
			else
			{
				//LED 동작 종류 설정
				LedOutputProcess(*CurrentLedData.pLedBuf++);
				//LED 동작 유지 시간 설정
				gbLedCnt = *CurrentLedData.pLedBuf++;

#ifdef	DDL_CFG_INSIDE_LED_LOW
				if(gfLowBattery)
				{
					P_LED_LOW(1);
				}
#endif	
				gbLedMode = 1;	
			}	
			break;

		case 10:
			// Constant LED Data가 Queue에 저장되므로 해당 Queue에 Data가 있는지 확인
			if(GetDataFromSeqLedQueue() == 0)
			{
				// 해당 Queue에 Data가 없을 경우 종료
				gbLedMode = 0;
				break;
			}

			if(CurrentLedData.pLedBuf == gcbWaitLedNextData)
			{
				// 해당 Queue에 Delay를 위한 Constant Data가 있을 경우 해당 값만큼 단순 Delay 수행
				gbLedCnt = CurrentLedData.WaitTimeForNextDisplay;	
				gbLedDisplayMode = CurrentLedData.DisplayMode;

				gbLedMode = 1;
			}
			else
			{
				gbLedDisplayMode = CurrentLedData.DisplayMode;
				// 해당 Queue에 Constant LED Data가 있을 경우 해당 Process 수행 (기존 처리 방식과 동일)
				gbLedMode = 2;
			}	
			break;


		default:
			LedOutputProcess(NODIM_LED_ALL_OFF);
			gbLedMode = 0;
			break;
	}			
}


void LedForcedAllOff(void)
{
#ifdef	DDL_CFG_INSIDE_LED_LOW
	P_LED_LOW(0);
#endif	

	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	P_LED_DEAD(0);
	P_LED_IB(0);

	LedOutputProcess(NODIM_LED_ALL_OFF);
	gSeqLedQueueHead = 0;
	gSeqLedQueueTail = 0;
	gbLedMode = 0;
}



#endif		// End of "false of ifdef DDL_CFG_LED_TYPE3"







#ifdef	DDL_CFG_LED_TYPE4
// LED_DECO, LED_BACK이 있는 경우


BYTE	*gbFLedData;			/**< 출력할 LED Data의 포인터 */


const BYTE gcbNoDimLed100msOn[]={
	NODIM_LED_DECO_ON, 	5,		
	LED_ENDD
};

const BYTE gcbNoDimAllLed2sOn[]={
	NODIM_LED_ALL_ON, 100,		
	LED_ENDD
};

const BYTE gcbNoDimLed2sOn[]={
//	NODIM_LED_BACK_ON, 100,	
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_BACK_ON, 50,	
	LED_ENDD
};

const BYTE gcbNoDimLedBlink[]={
	NODIM_LED_DECO_OFF, 3,		
	NODIM_LED_DECO_ON, 	0,		
	LED_ENDD
};

const BYTE gcbNoDimLedTamperLockout[]={
	NODIM_LED_DECO_ON, 	50,		
	NODIM_LED_ALL_OFF, 100,	
	LED_ENDD
};

const BYTE gcbNoDimLedOff[]={
	NODIM_LED_ALL_OFF, 	3,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr3[]={
	NODIM_LED_DECO_ON, 7,		
	NODIM_LED_ALL_OFF, 7,		
	NODIM_LED_DECO_ON, 7,		
	NODIM_LED_ALL_OFF, 7,		
	NODIM_LED_DECO_ON, 7,		
	NODIM_LED_ALL_OFF, 0,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr4[]={
	NODIM_LED_DECO_ON, 7,		
	NODIM_LED_ALL_OFF, 7,		
	NODIM_LED_DECO_ON, 7,		
	NODIM_LED_ALL_OFF, 7,		
	NODIM_LED_DECO_ON, 7,		
	NODIM_LED_ALL_OFF, 7,		
	NODIM_LED_DECO_ON, 7,		
	NODIM_LED_ALL_OFF, 0,		
	LED_ENDD
};




void SetupLed(void)
{
	P_LED_DECO(0);
	P_LED_TENKEY(0);
}


void LedModeRefresh(void)
{
	if(GetLedMode())
	{
		gSeqLedQueueHead = 0;
		gSeqLedQueueTail = 0;
	}
}


void LedOutputProcess(BYTE LedOutMode)
{
	switch(LedOutMode)
	{
		case NODIM_LED_ALL_ON:
			P_LED_DECO(1);
			P_LED_TENKEY(1);
			break;		

		case NODIM_LED_ALL_OFF:
			P_LED_DECO(0);
			P_LED_TENKEY(0);
			break;		

		case NODIM_LED_DECO_ON:
			if(gfCoverSw)
			{
				//Cover Up
				P_LED_TENKEY(1);
			}
			else
			{
				//Cover Down
				P_LED_DECO(1);
			}				
			break;		

		case NODIM_LED_DECO_OFF:
			if(gfCoverSw)
			{
				//Cover Up
				P_LED_TENKEY(0);
			}
			else
			{
				//Cover Down
				P_LED_DECO(0);
			}				
			break;		

		case NODIM_LED_BACK_ON:
			if(gfCoverSw)
			{
				//Cover Up
				P_LED_TENKEY(1);
			}
			else
			{
				//Cover Down
				P_LED_TENKEY(1);
				P_LED_DECO(1);
			}				
			break;		
	
	}
}



//------------------------------------------------------------------------------
/** 	@brief	출력 요청된 LED에 대한 출력 처리
	@param	None
	@return	None
	@remark LED 출력 처리
*/	
//------------------------------------------------------------------------------
// 20ms마다 LED 출력 처리 - Dimming IC와 제어 형태와 동일하게 처리하기 위해
void LedProcess(void)
{
	if(gbLedTimer2ms)		return;
	gbLedTimer2ms = 10;		//20ms

	switch(gbLedMode)
	{
		case 0:
			break;

		//LED 동작 유지 시간 계산
		case 1:
			if(gbLedCnt)
			{
				gbLedCnt--;
				break;
			}
			gbLedMode++;

		case 2:
			// Constant LED Data로 출력하고자 할 경우 처리 (기존 처리 방식과 동일)
			if(*CurrentLedData.pLedBuf == LED_ENDD)
			{
				if(!(gbLedDisplayMode & LED_KEEP_DISPLAY))
				{
					LedOutputProcess(NODIM_LED_ALL_OFF);
				}
				
#ifdef	DDL_CFG_INSIDE_LED_LOW
				P_LED_LOW(0);
#endif	
				gbLedMode = 10;
			}
			else
			{
				//LED 동작 종류 설정
				LedOutputProcess(*CurrentLedData.pLedBuf++);
				//LED 동작 유지 시간 설정
				gbLedCnt = *CurrentLedData.pLedBuf++;

#ifdef	DDL_CFG_INSIDE_LED_LOW
				if(gfLowBattery)
				{
					P_LED_LOW(1);
				}
#endif	
				gbLedMode = 1;	
			}	
			break;

		case 10:
			// Constant LED Data가 Queue에 저장되므로 해당 Queue에 Data가 있는지 확인
			if(GetDataFromSeqLedQueue() == 0)
			{
				// 해당 Queue에 Data가 없을 경우 종료
				gbLedMode = 0;
				break;
			}

			if(CurrentLedData.pLedBuf == gcbWaitLedNextData)
			{
				// 해당 Queue에 Delay를 위한 Constant Data가 있을 경우 해당 값만큼 단순 Delay 수행
				gbLedCnt = CurrentLedData.WaitTimeForNextDisplay;	
				gbLedDisplayMode = CurrentLedData.DisplayMode;

				gbLedMode = 1;
			}
			else
			{
				gbLedDisplayMode = CurrentLedData.DisplayMode;
				// 해당 Queue에 Constant LED Data가 있을 경우 해당 Process 수행 (기존 처리 방식과 동일)
				gbLedMode = 2;
			}	
			break;


		default:
			LedOutputProcess(NODIM_LED_ALL_OFF);
			gbLedMode = 0;
			break;
	}			
}


void LedForcedAllOff(void)
{
#ifdef	DDL_CFG_INSIDE_LED_LOW
	P_LED_LOW(0);
#endif	
	P_LED_DECO(0);
	P_LED_TENKEY(0);

	LedOutputProcess(NODIM_LED_ALL_OFF);
	gSeqLedQueueHead = 0;
	gSeqLedQueueTail = 0;
	gbLedMode = 0;
}




#endif		// End of "false of ifdef DDL_CFG_LED_TYPE4"

#ifdef	DDL_CFG_LED_TYPE5
// LED_TENKEY, LED_LOWBAT, LED_DEAD, LED_OPEN, LED_CLOSE가 있는 경우


BYTE	*gbFLedData;			/**< 출력할 LED Data의 포인터 */


const BYTE gcbNoDimLed100msOn[]={
	NODIM_LED_BACK_ON, 	5,		
	LED_ENDD
};

const BYTE gcbNoDimAllLed2sOn[]={
	NODIM_LED_ALL_ON, 100,		
	LED_ENDD
};

const BYTE gcbNoDimLed2sOn[]={
//	NODIM_LED_BACK_ON, 100,		
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_BACK_ON, 50,		
	LED_ENDD
};

const BYTE gcbNoDimLedBlink[]={
	NODIM_LED_BACK_OFF, 3,		
	NODIM_LED_BACK_ON, 	0,		
	LED_ENDD
};

const BYTE gcbNoDimLedTamperLockout[]={
	NODIM_LED_BACK_ON, 	50,		
	NODIM_LED_ALL_OFF, 100,	
	LED_ENDD
};

const BYTE gcbNoDimLedOff[]={
	NODIM_LED_ALL_OFF, 	3,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr3[]={
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 0,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr4[]={
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 0,		
	LED_ENDD
};

const BYTE gcbNoDimBackOpenLed2sOn[]={		
	NODIM_LED_OPEN_ON, 1,	
	NODIM_LED_BACK_ON, 49,		
	LED_ENDD
};

const BYTE gcbNoDimBackCloseLed2sOn[]={	
	NODIM_LED_CLOSE_ON, 1,	
	NODIM_LED_BACK_ON, 49,		
	LED_ENDD
};



const BYTE gcbNoDimLedOpen[]={		
	NODIM_LED_OPEN_ON, 50,		
	NODIM_LED_OPEN_OFF, 0,		
	LED_ENDD
};


const BYTE gcbNoDimLedClose[]={		
	NODIM_LED_CLOSE_ON, 50,		
	NODIM_LED_CLOSE_OFF, 0,		
	LED_ENDD
};






void SetupLed(void)
{
	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	P_LED_DEAD(0);	
	P_LED_OPEN(0);
	P_LED_CLOSE(0);
}


void LedModeRefresh(void)
{
	if(GetLedMode())
	{
		gSeqLedQueueHead = 0;
		gSeqLedQueueTail = 0;
	}
}


void LedOutputProcess(BYTE LedOutMode)
{
	BYTE bTmp;

	bTmp = GetFinalMotorStatus();
	if((bTmp == FINAL_MOTOR_STATE_OPEN_ERROR) || (bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR))
	{
		P_LED_DEAD(1);
	}
	
	if(gfLowBattery)
	{
		P_LED_LOWBAT(1);
	}

	switch(LedOutMode)
	{
		case NODIM_LED_ALL_ON:
			P_LED_TENKEY(1);
			P_LED_LOWBAT(1);
			P_LED_DEAD(1);
			P_LED_OPEN(1);
			P_LED_CLOSE(1);
			break;		

		case NODIM_LED_ALL_OFF:
			P_LED_TENKEY(0);
			P_LED_LOWBAT(0);
			P_LED_DEAD(0);
			P_LED_OPEN(0);
			P_LED_CLOSE(0);
			break;		

		case NODIM_LED_BACK_ON:
			P_LED_TENKEY(1);
			break;		

		case NODIM_LED_BACK_OFF:
			P_LED_TENKEY(0);
			break;		

		case NODIM_LED_OPEN_ON:
			P_LED_OPEN(1);
			break;		
		case NODIM_LED_OPEN_OFF:
			P_LED_OPEN(0);
			break;		
		case NODIM_LED_CLOSE_ON:
			P_LED_CLOSE(1);
			break;		
		case NODIM_LED_CLOSE_OFF:
			P_LED_CLOSE(0);
			break;		
	}
}



//------------------------------------------------------------------------------
/** 	@brief	출력 요청된 LED에 대한 출력 처리
	@param	None
	@return	None
	@remark LED 출력 처리
*/	
//------------------------------------------------------------------------------
// 20ms마다 LED 출력 처리 - Dimming IC와 제어 형태와 동일하게 처리하기 위해
void LedProcess(void)
{
	if(gbLedTimer2ms)		return;
	gbLedTimer2ms = 10;		//20ms

	switch(gbLedMode)
	{
		case 0:
			break;

		//LED 동작 유지 시간 계산
		case 1:
			if(gbLedCnt)
			{
				gbLedCnt--;
				break;
			}
			gbLedMode++;

		case 2:
			// Constant LED Data로 출력하고자 할 경우 처리 (기존 처리 방식과 동일)
			if(*CurrentLedData.pLedBuf == LED_ENDD)
			{
				if(!(gbLedDisplayMode & LED_KEEP_DISPLAY))
				{
					LedOutputProcess(NODIM_LED_ALL_OFF);
				}
				
#ifdef	DDL_CFG_INSIDE_LED_LOW
				P_LED_LOW(0);
#endif	
				gbLedMode = 10;
			}
			else
			{
				//LED 동작 종류 설정
				LedOutputProcess(*CurrentLedData.pLedBuf++);
				//LED 동작 유지 시간 설정
				gbLedCnt = *CurrentLedData.pLedBuf++;

#ifdef	DDL_CFG_INSIDE_LED_LOW
				if(gfLowBattery)
				{
					P_LED_LOW(1);
				}
#endif	
				gbLedMode = 1;	
			}	
			break;

		case 10:
			// Constant LED Data가 Queue에 저장되므로 해당 Queue에 Data가 있는지 확인
			if(GetDataFromSeqLedQueue() == 0)
			{
				// 해당 Queue에 Data가 없을 경우 종료
				gbLedMode = 0;
				break;
			}

			if(CurrentLedData.pLedBuf == gcbWaitLedNextData)
			{
				// 해당 Queue에 Delay를 위한 Constant Data가 있을 경우 해당 값만큼 단순 Delay 수행
				gbLedCnt = CurrentLedData.WaitTimeForNextDisplay;	
				gbLedDisplayMode = CurrentLedData.DisplayMode;

				gbLedMode = 1;
			}
			else
			{
				gbLedDisplayMode = CurrentLedData.DisplayMode;
				// 해당 Queue에 Constant LED Data가 있을 경우 해당 Process 수행 (기존 처리 방식과 동일)
				gbLedMode = 2;
			}	
			break;


		default:
			LedOutputProcess(NODIM_LED_ALL_OFF);
			gbLedMode = 0;
			break;
	}			
}



void LedForcedAllOff(void)
{
#ifdef	DDL_CFG_INSIDE_LED_LOW
	P_LED_LOW(0);
#endif	

	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	P_LED_DEAD(0);
	P_LED_OPEN(0);
	P_LED_CLOSE(0);

	gSeqLedQueueHead = 0;
	gSeqLedQueueTail = 0;
	gbLedMode = 0;
}




#endif		// End of "false of ifdef DDL_CFG_LED_TYPE2"


#ifdef	DDL_CFG_LED_TYPE6
// LED_TENKEY, LED_LOW, LED_OPEN, LED_CLOSE가 있는 경우


BYTE	*gbFLedData;			/**< 출력할 LED Data의 포인터 */


const BYTE gcbNoDimLed100msOn[]={
	NODIM_LED_BACK_ON, 	5,		
	LED_ENDD
};

const BYTE gcbNoDimAllLed2sOn[]={
	NODIM_LED_ALL_ON, 100,		
	LED_ENDD
};

const BYTE gcbNoDimLed2sOn[]={
//	NODIM_LED_BACK_ON, 100,		
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_BACK_ON, 50,		
	LED_ENDD
};

const BYTE gcbNoDimLedBlink[]={
	NODIM_LED_BACK_OFF, 3,		
	NODIM_LED_BACK_ON, 	0,		
	LED_ENDD
};

const BYTE gcbNoDimLedTamperLockout[]={
	NODIM_LED_BACK_ON, 	50,		
	NODIM_LED_ALL_OFF, 100,	
	LED_ENDD
};

const BYTE gcbNoDimLedOff[]={
	NODIM_LED_ALL_OFF, 	3,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr3[]={
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 0,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr4[]={
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 0,		
	LED_ENDD
};

const BYTE gcbNoDimBackOpenLed2sOn[]={		
	NODIM_LED_OPEN_ON, 1,	
	NODIM_LED_BACK_ON, 49,		
	LED_ENDD
};

const BYTE gcbNoDimBackCloseLed2sOn[]={	
	NODIM_LED_CLOSE_ON, 1,	
	NODIM_LED_BACK_ON, 49,		
	LED_ENDD
};



const BYTE gcbNoDimLedOpen[]={		
	NODIM_LED_OPEN_ON, 50,		
	NODIM_LED_OPEN_OFF, 0,		
	LED_ENDD
};


const BYTE gcbNoDimLedClose[]={		
	NODIM_LED_CLOSE_ON, 50,		
	NODIM_LED_CLOSE_OFF, 0,		
	LED_ENDD
};






void SetupLed(void)
{
	P_LED_TENKEY(0);
#ifdef	DDL_CFG_INSIDE_LED_LOW	
	P_LED_LOW(0);
#endif 
	P_LED_OPEN(0);
	P_LED_CLOSE(0);
}


void LedModeRefresh(void)
{
	if(GetLedMode())
	{
		gSeqLedQueueHead = 0;
		gSeqLedQueueTail = 0;
	}
}


void LedOutputProcess(BYTE LedOutMode)
{
//	BYTE bTmp;

//	bTmp = GetFinalMotorStatus();
	
	if(gfLowBattery)
	{
		P_LED_LOW(1);
	}

	switch(LedOutMode)
	{
		case NODIM_LED_ALL_ON:
			P_LED_TENKEY(1);
#ifdef	DDL_CFG_INSIDE_LED_LOW			
			P_LED_LOW(1);
#endif 
			P_LED_OPEN(1);
			P_LED_CLOSE(1);
			break;		

		case NODIM_LED_ALL_OFF:
			P_LED_TENKEY(0);
#ifdef	DDL_CFG_INSIDE_LED_LOW			
			P_LED_LOW(0);
#endif 
			P_LED_OPEN(0);
			P_LED_CLOSE(0);
			break;		

		case NODIM_LED_BACK_ON:
			P_LED_TENKEY(1);
			break;		

		case NODIM_LED_BACK_OFF:
			P_LED_TENKEY(0);
			break;		

		case NODIM_LED_OPEN_ON:
			P_LED_OPEN(1);
			break;		
		case NODIM_LED_OPEN_OFF:
			P_LED_OPEN(0);
			break;		
		case NODIM_LED_CLOSE_ON:
			P_LED_CLOSE(1);
			break;		
		case NODIM_LED_CLOSE_OFF:
			P_LED_CLOSE(0);
			break;		
	}
}



//------------------------------------------------------------------------------
/** 	@brief	출력 요청된 LED에 대한 출력 처리
	@param	None
	@return	None
	@remark LED 출력 처리
*/	
//------------------------------------------------------------------------------
// 20ms마다 LED 출력 처리 - Dimming IC와 제어 형태와 동일하게 처리하기 위해
void LedProcess(void)
{
	if(gbLedTimer2ms)		return;
	gbLedTimer2ms = 10;		//20ms

	switch(gbLedMode)
	{
		case 0:
			break;

		//LED 동작 유지 시간 계산
		case 1:
			if(gbLedCnt)
			{
				gbLedCnt--;
				break;
			}
			gbLedMode++;

		case 2:
			// Constant LED Data로 출력하고자 할 경우 처리 (기존 처리 방식과 동일)
			if(*CurrentLedData.pLedBuf == LED_ENDD)
			{
				if(!(gbLedDisplayMode & LED_KEEP_DISPLAY))
				{
					LedOutputProcess(NODIM_LED_ALL_OFF);
				}
				
#ifdef	DDL_CFG_INSIDE_LED_LOW
				P_LED_LOW(0);
#endif	
				gbLedMode = 10;
			}
			else
			{
				//LED 동작 종류 설정
				LedOutputProcess(*CurrentLedData.pLedBuf++);
				//LED 동작 유지 시간 설정
				gbLedCnt = *CurrentLedData.pLedBuf++;

#ifdef	DDL_CFG_INSIDE_LED_LOW
				if(gfLowBattery)
				{
					P_LED_LOW(1);
				}
#endif	
				gbLedMode = 1;	
			}	
			break;

		case 10:
			// Constant LED Data가 Queue에 저장되므로 해당 Queue에 Data가 있는지 확인
			if(GetDataFromSeqLedQueue() == 0)
			{
				// 해당 Queue에 Data가 없을 경우 종료
				gbLedMode = 0;
				break;
			}

			if(CurrentLedData.pLedBuf == gcbWaitLedNextData)
			{
				// 해당 Queue에 Delay를 위한 Constant Data가 있을 경우 해당 값만큼 단순 Delay 수행
				gbLedCnt = CurrentLedData.WaitTimeForNextDisplay;	
				gbLedDisplayMode = CurrentLedData.DisplayMode;

				gbLedMode = 1;
			}
			else
			{
				gbLedDisplayMode = CurrentLedData.DisplayMode;
				// 해당 Queue에 Constant LED Data가 있을 경우 해당 Process 수행 (기존 처리 방식과 동일)
				gbLedMode = 2;
			}	
			break;


		default:
			LedOutputProcess(NODIM_LED_ALL_OFF);
			gbLedMode = 0;
			break;
	}			
}



void LedForcedAllOff(void)
{
#ifdef	DDL_CFG_INSIDE_LED_LOW
	P_LED_LOW(0);
#endif	
	P_LED_TENKEY(0);
	P_LED_OPEN(0);
	P_LED_CLOSE(0);

	gSeqLedQueueHead = 0;
	gSeqLedQueueTail = 0;
	gbLedMode = 0;
}




#endif		// End of "false of ifdef DDL_CFG_LED_TYPE2"

#ifdef	DDL_CFG_LED_TYPE7	//FDS
// LED_TENKEY, LED_LOWBAT, LED_DEAD가 있는 경우


BYTE	*gbFLedData;			/**< 출력할 LED Data의 포인터 */


const BYTE gcbNoDimLed100msOn[]={
	NODIM_LED_ALL_ON, 	5,		
	LED_ENDD
};

const BYTE gcbNoDimAutoLockHold[]={
	NODIM_LED_BLUE_ON, 	5,		
	LED_ENDD
};

const BYTE gcbNoDimAutoLockUnHold[]={
	NODIM_LED_RED_ON, 	5,		
	LED_ENDD
};


const BYTE gcbNoDimAllLed2sOn[]={
	NODIM_LED_ALL_ON, 100,		
	LED_ENDD
};

const BYTE gcbNoDimLed2sOn[]={
//	NODIM_LED_BACK_ON, 100,		
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_ALL_ON, 50,		
	LED_ENDD
};

const BYTE gcbNoDimPassage[]={
//	NODIM_LED_BACK_ON, 100,		
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_GREEN_ON, 15,	
	NODIM_LED_GREEN_OFF, 10,	
	NODIM_LED_GREEN_ON, 15,		
	NODIM_LED_GREEN_OFF, 10,		
	LED_ENDD
};


const BYTE gcbNoDimSafety[]={
//	NODIM_LED_BACK_ON, 100,		
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_YELLOW_ON, 15,		
	NODIM_LED_YELLOW_OFF, 10,		
	NODIM_LED_YELLOW_ON, 15,		
	NODIM_LED_YELLOW_OFF, 10,		
	LED_ENDD
};


const BYTE gcbNoDimSecure[]={
//	NODIM_LED_BACK_ON, 100,		
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_RED_ON, 15,		
	NODIM_LED_RED_OFF, 10,		
	NODIM_LED_RED_ON, 15,		
	NODIM_LED_RED_OFF, 10,		
	LED_ENDD
};




const BYTE gcbNoDimLedBlink[]={
	NODIM_LED_ALL_OFF, 3,		
	NODIM_LED_ALL_ON, 	0,		
	LED_ENDD
};

const BYTE gcbNoDimLedTamperLockout[]={
	NODIM_LED_ALL_ON, 	50,		
	NODIM_LED_ALL_OFF, 100,	
	LED_ENDD
};

const BYTE gcbNoDimLedOff[]={
	NODIM_LED_ALL_OFF, 	3,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr3[]={
	NODIM_LED_RED_ON, 7,		
	NODIM_LED_RED_OFF, 7,		
	NODIM_LED_RED_ON, 7,		
	NODIM_LED_RED_OFF, 7,		
	NODIM_LED_RED_ON, 7,		
	NODIM_LED_RED_OFF, 0,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr4[]={
	NODIM_LED_RED_ON, 7,		
	NODIM_LED_RED_OFF, 7,		
	NODIM_LED_RED_ON, 7,		
	NODIM_LED_RED_OFF, 7,		
	NODIM_LED_RED_ON, 7,		
	NODIM_LED_RED_OFF, 7,		
	NODIM_LED_RED_ON, 7,		
	NODIM_LED_RED_OFF, 0,		
	LED_ENDD
};




void SetupLed(void)
{
	/*P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	P_LED_DEAD(0);*/
	P_FRONT_LED_R(0);
	P_FRONT_LED_G(0);
	P_FRONT_LED_B(0);
#ifdef DDL_CFG_IOEXPANDER_LED	
	gbExpanderOutmainledr = 0;
	gbExpanderOutmainledg = 0;
	gbExpanderOutmainledb = 0;
	IoExpanderOutPutControl();
#else
	P_MAIN_LED_R(0);
	P_MAIN_LED_G(0);
	P_MAIN_LED_B(0);
#endif	
}


void LedModeRefresh(void)
{
	if(GetLedMode())
	{
		gSeqLedQueueHead = 0;
		gSeqLedQueueTail = 0;
	}
}


void LedOutputProcess(BYTE LedOutMode)
{
	BYTE bTmp;
#ifdef DDL_LED_DISPLAY_SELECT //led display 결정 	
	if(LedOutMode != NODIM_LED_ALL_OFF && gLedDisplayEnableState == 0x00) return;
#endif //DDL_LED_DISPLAY_SELECT #endif	
	bTmp = GetFinalMotorStatus();
	#ifdef	_DDL_CONFIG_FRONTDOORSUITE_H_
	#else
	if((bTmp == FINAL_MOTOR_STATE_OPEN_ERROR) || (bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR))
	{
		P_FRONT_LED_R(1);
#ifdef DDL_CFG_IOEXPANDER_LED	
		gbExpanderOutmainledr = 1;
#else
		P_MAIN_LED_R(1);
#endif
	}		
	if(gfLowBattery)
	{		
		P_FRONT_LED_R(1);
		P_FRONT_LED_G(1);
		P_FRONT_LED_B(1);
#ifdef DDL_CFG_IOEXPANDER_LED	
		gbExpanderOutmainledr = 1;
		gbExpanderOutmainledg = 1;
		gbExpanderOutmainledb = 1;
#else
		P_MAIN_LED_R(1);
		P_MAIN_LED_G(1);
		P_MAIN_LED_B(1);
#endif		
	}
	#endif

	switch(LedOutMode)
	{
		case NODIM_LED_ALL_ON:			
			P_FRONT_LED_R(1);
			P_FRONT_LED_G(1);
			P_FRONT_LED_B(1);
#ifdef DDL_CFG_IOEXPANDER_LED	
			gbExpanderOutmainledr = 1;
			gbExpanderOutmainledg = 1;
			gbExpanderOutmainledb = 1;
#else			
			P_MAIN_LED_R(1);
			P_MAIN_LED_G(1);
			P_MAIN_LED_B(1);
#endif
			break;		

		case NODIM_LED_ALL_OFF:
			P_FRONT_LED_R(0);
			P_FRONT_LED_G(0);
			P_FRONT_LED_B(0);
#ifdef DDL_CFG_IOEXPANDER_LED	
			gbExpanderOutmainledr = 0;
			gbExpanderOutmainledg = 0;
			gbExpanderOutmainledb = 0;
#else
			P_MAIN_LED_R(0);
			P_MAIN_LED_G(0);
			P_MAIN_LED_B(0);
#endif			
			break;		

		case NODIM_LED_BACK_ON:
			P_FRONT_LED_B(1);
#ifdef DDL_CFG_IOEXPANDER_LED	
			gbExpanderOutmainledb = 1;
#else
			P_MAIN_LED_B(1);
#endif
			break;		

		case NODIM_LED_BACK_OFF:
			P_FRONT_LED_B(0);
#ifdef DDL_CFG_IOEXPANDER_LED	
			gbExpanderOutmainledb = 0;
#else
			P_MAIN_LED_B(0);
#endif
			break;	
			
		case NODIM_LED_RED_ON:
			P_FRONT_LED_R(1);
#ifdef DDL_CFG_IOEXPANDER_LED			
			gbExpanderOutmainledr = 1;
#else
			P_MAIN_LED_R(1);
#endif
			break;
			
		case NODIM_LED_RED_OFF:
			P_FRONT_LED_R(0);
#ifdef DDL_CFG_IOEXPANDER_LED			
			gbExpanderOutmainledr = 0;
#else
			P_MAIN_LED_R(0);
#endif
			break;
			
		case NODIM_LED_GREEN_ON:
			P_FRONT_LED_G(1);
#ifdef DDL_CFG_IOEXPANDER_LED	
			gbExpanderOutmainledg = 1;
#else
			P_MAIN_LED_G(1);
#endif
			break;
			
		case NODIM_LED_GREEN_OFF:
			P_FRONT_LED_G(0);
#ifdef DDL_CFG_IOEXPANDER_LED	
			gbExpanderOutmainledg = 0;
#else
			P_MAIN_LED_G(0);
#endif
			break;

		case NODIM_LED_BLUE_ON:
			P_FRONT_LED_B(1);
#ifdef DDL_CFG_IOEXPANDER_LED	
			gbExpanderOutmainledb = 1;
#else
			P_MAIN_LED_B(1);
#endif
			break;
			
		case NODIM_LED_BLUE_OFF:
			P_FRONT_LED_B(0);
#ifdef DDL_CFG_IOEXPANDER_LED	
			gbExpanderOutmainledb = 0;
#else
			P_MAIN_LED_B(0);
#endif
			break;

		case NODIM_LED_YELLOW_ON:			
			P_FRONT_LED_G(1);
			P_FRONT_LED_R(1);
#ifdef DDL_CFG_IOEXPANDER_LED	
			gbExpanderOutmainledg = 1;
			gbExpanderOutmainledr = 1;
#else
			P_MAIN_LED_G(1);
			P_MAIN_LED_R(1);
#endif
			break;	
			
		case NODIM_LED_YELLOW_OFF:		
			P_FRONT_LED_G(0);
			P_FRONT_LED_R(0);
#ifdef DDL_CFG_IOEXPANDER_LED	
			gbExpanderOutmainledg = 0;
			gbExpanderOutmainledr = 0;
#else
			P_MAIN_LED_G(0);
			P_MAIN_LED_R(0);
#endif
			break;	
	}
#ifdef DDL_CFG_IOEXPANDER_LED		
	IoExpanderOutPutControl();
#endif
}



//------------------------------------------------------------------------------
/** 	@brief	출력 요청된 LED에 대한 출력 처리
	@param	None
	@return	None
	@remark LED 출력 처리
*/	
//------------------------------------------------------------------------------
// 20ms마다 LED 출력 처리 - Dimming IC와 제어 형태와 동일하게 처리하기 위해
void LedProcess(void)
{
	if(gbLedTimer2ms)		return;
	gbLedTimer2ms = 10;		//20ms

	switch(gbLedMode)
	{
		case 0:
			break;

		//LED 동작 유지 시간 계산
		case 1:
			if(gbLedCnt)
			{
				gbLedCnt--;
				break;
			}
			gbLedMode++;

		case 2:
			// Constant LED Data로 출력하고자 할 경우 처리 (기존 처리 방식과 동일)
			if(*CurrentLedData.pLedBuf == LED_ENDD)
			{
				if(!(gbLedDisplayMode & LED_KEEP_DISPLAY))
				{
					LedOutputProcess(NODIM_LED_ALL_OFF);
				}
				
#ifdef	DDL_CFG_INSIDE_LED_LOW
				P_LED_LOW(0);
#endif	
				gbLedMode = 10;
			}
			else
			{
				//LED 동작 종류 설정
				LedOutputProcess(*CurrentLedData.pLedBuf++);
				//LED 동작 유지 시간 설정
				gbLedCnt = *CurrentLedData.pLedBuf++;

#ifdef	DDL_CFG_INSIDE_LED_LOW
				if(gfLowBattery)
				{
					P_LED_LOW(1);
				}
#endif	
				gbLedMode = 1;	
			}	
			break;

		case 10:
			// Constant LED Data가 Queue에 저장되므로 해당 Queue에 Data가 있는지 확인
			if(GetDataFromSeqLedQueue() == 0)
			{
				// 해당 Queue에 Data가 없을 경우 종료
				gbLedMode = 0;
				break;
			}

			if(CurrentLedData.pLedBuf == gcbWaitLedNextData)
			{
				// 해당 Queue에 Delay를 위한 Constant Data가 있을 경우 해당 값만큼 단순 Delay 수행
				gbLedCnt = CurrentLedData.WaitTimeForNextDisplay;	
				gbLedDisplayMode = CurrentLedData.DisplayMode;

				gbLedMode = 1;
			}
			else
			{
				gbLedDisplayMode = CurrentLedData.DisplayMode;
				// 해당 Queue에 Constant LED Data가 있을 경우 해당 Process 수행 (기존 처리 방식과 동일)
				gbLedMode = 2;
			}	
			break;


		default:
			LedOutputProcess(NODIM_LED_ALL_OFF);
			gbLedMode = 0;
			break;
	}			
}



void LedForcedAllOff(void)
{
	P_FRONT_LED_R(0);
	P_FRONT_LED_G(0);
	P_FRONT_LED_B(0);
#ifdef DDL_CFG_IOEXPANDER_LED	
	gbExpanderOutmainledr = 0;
	gbExpanderOutmainledg = 0;
	gbExpanderOutmainledb = 0;
#else	
	P_MAIN_LED_R(0);
	P_MAIN_LED_G(0);
	P_MAIN_LED_B(0);
#endif	

	gSeqLedQueueHead = 0;
	gSeqLedQueueTail = 0;
	gbLedMode = 0;
}
#ifdef DDL_LED_DISPLAY_SELECT //led display 결정 	
BYTE LedDisplayEnableStateLoad(void)
{
	BYTE bTmp;
	RomRead(&bTmp, (WORD)LOCK_STATUS_LED, 1);
	gLedDisplayEnableState = bTmp;
	return gLedDisplayEnableState;
}
void  LedDisplayEnableStateSet(BYTE bVal)
{
	gLedDisplayEnableState = bVal;
	RomWrite(&gLedDisplayEnableState, (WORD)LOCK_STATUS_LED, 1);
}
#endif //DDL_LED_DISPLAY_SELECT #endif




#endif		// End of "false of ifdef DDL_CFG_LED_TYPE2"

#ifdef	DDL_CFG_LED_TYPE8	//PANPAN FC2A-P-A/F5P-A
// LED_TENKEY, LED_LOWBAT, LED_DEAD가 있는 경우


BYTE	*gbFLedData;			/**< 출력할 LED Data의 포인터 */


const BYTE gcbNoDimLed100msOn[]={
	NODIM_LED_BACK_ON, 	5,		
	LED_ENDD
};

const BYTE gcbNoDimAllLed2sOn[]={
	NODIM_LED_ALL_ON, 100,		
	LED_ENDD
};

const BYTE gcbNoDimLed2sOn[]={
//	NODIM_LED_BACK_ON, 100,		
	// Motor Open/Close 수행 과정에서 LED가 너무 오래 켜져 있어 시간을 절반으로 줄임
	NODIM_LED_BACK_ON, 50,		
	LED_ENDD
};

const BYTE gcbNoDimLedBlink[]={
	NODIM_LED_BACK_OFF, 3,		
	NODIM_LED_BACK_ON, 	0,		
	LED_ENDD
};

extern const BYTE gcbLedOutLock1[]={
	NODIM_LED_BACK_OFF, 3,		
	NODIM_LED_BACK_ON, 	0,		
	LED_ENDD
};

extern const BYTE gcbLedOutLock2[]={
	NODIM_LED_BACK_OFF, 3,		
	NODIM_LED_BACK_ON, 	0,		
	LED_ENDD
};

extern const BYTE gcbLedOutLock3[]={
	NODIM_LED_BACK_OFF, 3,		
	NODIM_LED_BACK_ON, 	0,		
	LED_ENDD
};

extern const BYTE gcbLedOutLock4[]={
	NODIM_LED_BACK_OFF, 3,		
	NODIM_LED_BACK_ON, 	0,		
	LED_ENDD
};

const BYTE gcbLedOk[]={
	NODIM_LED_BACK_OFF, 3,		
	NODIM_LED_BACK_ON, 	0,		
	LED_ENDD
};	

const BYTE gcbNoDimLedTamperLockout[]={
	NODIM_LED_BACK_ON, 	50,		
	NODIM_LED_ALL_OFF, 100,	
	LED_ENDD
};

const BYTE gcbNoDimLedOff[]={
	NODIM_LED_ALL_OFF, 	3,		
	LED_ENDD
};

const BYTE gcbLedOff[]={
	NODIM_LED_ALL_OFF, 	3,		
	LED_ENDD
};


const BYTE gcbNoDimLedErr3[]={
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 0,		
	LED_ENDD
};

const BYTE gcbNoDimLedErr4[]={
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 7,		
	NODIM_LED_BACK_ON, 7,		
	NODIM_LED_BACK_OFF, 0,		
	LED_ENDD
};
 



void SetupLed(void)
{
	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	//P_LED_LOCK(0);
	P_LED_DEAD(0);
}


void LedModeRefresh(void)
{
	if(GetLedMode())
	{
		gSeqLedQueueHead = 0;
		gSeqLedQueueTail = 0;
	}
}


void LedOutputProcess(BYTE LedOutMode)
{
	BYTE bTmp;

	bTmp = GetFinalMotorStatus();
	/*if(bTmp == FINAL_MOTOR_STATE_CLOSE)
	{
		P_LED_LOCK(1);
	}*/
	if((bTmp == FINAL_MOTOR_STATE_OPEN_ERROR) || (bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR))
	{
		P_LED_DEAD(1);
	}
	
	if(gfLowBattery)
	{
		P_LED_LOWBAT(1);
	}

	switch(LedOutMode)
	{
		case NODIM_LED_ALL_ON:
			P_LED_TENKEY(1);
			P_LED_LOWBAT(1);
			//P_LED_LOCK(1);
			P_LED_DEAD(1);
			break;		

		case NODIM_LED_ALL_OFF:
			P_LED_TENKEY(0);
			P_LED_LOWBAT(0);
			//P_LED_LOCK(0);
			P_LED_DEAD(0);
			break;		

		case NODIM_LED_BACK_ON:
			P_LED_TENKEY(1);
			break;		

		case NODIM_LED_BACK_OFF:
			P_LED_TENKEY(0);
			break;		
	}
}



//------------------------------------------------------------------------------
/** 	@brief	출력 요청된 LED에 대한 출력 처리
	@param	None
	@return	None
	@remark LED 출력 처리
*/	
//------------------------------------------------------------------------------
// 20ms마다 LED 출력 처리 - Dimming IC와 제어 형태와 동일하게 처리하기 위해
void LedProcess(void)
{
	if(gbLedTimer2ms)		return;
	gbLedTimer2ms = 10;		//20ms

	switch(gbLedMode)
	{
		case 0:
			break;

		//LED 동작 유지 시간 계산
		case 1:
			if(gbLedCnt)
			{
				gbLedCnt--;
				break;
			}
			gbLedMode++;

		case 2:
			// Constant LED Data로 출력하고자 할 경우 처리 (기존 처리 방식과 동일)
			if(*CurrentLedData.pLedBuf == LED_ENDD)
			{
				if(!(gbLedDisplayMode & LED_KEEP_DISPLAY))
				{
					LedOutputProcess(NODIM_LED_ALL_OFF);
				}
				
#ifdef	DDL_CFG_INSIDE_LED_LOW
				P_LED_LOW(0);
#endif	
				gbLedMode = 10;
			}
			else
			{
				//LED 동작 종류 설정
				LedOutputProcess(*CurrentLedData.pLedBuf++);
				//LED 동작 유지 시간 설정
				gbLedCnt = *CurrentLedData.pLedBuf++;

#ifdef	DDL_CFG_INSIDE_LED_LOW
				if(gfLowBattery)
				{
					P_LED_LOW(1);
				}
#endif	
				gbLedMode = 1;	
			}	
			break;

		case 10:
			// Constant LED Data가 Queue에 저장되므로 해당 Queue에 Data가 있는지 확인
			if(GetDataFromSeqLedQueue() == 0)
			{
				// 해당 Queue에 Data가 없을 경우 종료
				gbLedMode = 0;
				break;
			}

			if(CurrentLedData.pLedBuf == gcbWaitLedNextData)
			{
				// 해당 Queue에 Delay를 위한 Constant Data가 있을 경우 해당 값만큼 단순 Delay 수행
				gbLedCnt = CurrentLedData.WaitTimeForNextDisplay;	
				gbLedDisplayMode = CurrentLedData.DisplayMode;

				gbLedMode = 1;
			}
			else
			{
				gbLedDisplayMode = CurrentLedData.DisplayMode;
				// 해당 Queue에 Constant LED Data가 있을 경우 해당 Process 수행 (기존 처리 방식과 동일)
				gbLedMode = 2;
			}	
			break;


		default:
			LedOutputProcess(NODIM_LED_ALL_OFF);
			gbLedMode = 0;
			break;
	}			
}



void LedForcedAllOff(void)
{
#ifdef	DDL_CFG_INSIDE_LED_LOW
	P_LED_LOW(0);
#endif	

	P_LED_TENKEY(0);
	P_LED_LOWBAT(0);
	//P_LED_LOCK(0);
	P_LED_DEAD(0);

	gSeqLedQueueHead = 0;
	gSeqLedQueueTail = 0;
	gbLedMode = 0;
}




#endif		// End of "false of ifdef DDL_CFG_LED_TYPE2"


