#include "Main.h"



//Voice 및 Buzzer 등 청각적 피드백에 대한 통합 설정 함수
void AudioFeedback(WORD wVoiceData, WORD const* bpBuzzerData, BYTE bVoiceMode)
{
	if(gfVoiceSet && (wVoiceData != 0xFFFF))
	{
		BuzzerOff();
		VoiceSetting(wVoiceData, (BYTE)bVoiceMode);
	}
	else if(gfVoiceSet && (wVoiceData == 0xFFFF))
	{
		BuzzerOff();		
		//VoiceSetting(0, (BYTE)bVoiceMode); //0xFFFF 로 들어온 경우 아예 voice setting 하지 않도록 

		BuzzerSetting(bpBuzzerData, bVoiceMode);
	}
	else
	{
		BuzzerSetting(bpBuzzerData, bVoiceMode);
	}
}




#ifdef	DDL_CFG_DIMMER
WORD ChangeNumberToLedVal[10] = {LED_KEY_0, LED_KEY_1, LED_KEY_2, LED_KEY_3, LED_KEY_4 
								,LED_KEY_5, LED_KEY_6, LED_KEY_7, LED_KEY_8, LED_KEY_9}; 
#endif


void LedNumberDisplay(BYTE Number, BYTE LastOff)
{
#ifdef	DDL_CFG_DIMMER
	BYTE Number1st = 0;
	BYTE Number2nd = 0;

	if(Number >= 10)
	{
		Number1st = Number/10;
		Number -= (Number1st*10);

		LedGenerate(ChangeNumberToLedVal[Number1st], DIMM_ON_FAST, 0);
		LedGenerate(ChangeNumberToLedVal[Number1st], DIMM_OFF_FAST, 0);
	}	

	Number2nd = Number;
	LedGenerate(ChangeNumberToLedVal[Number2nd], DIMM_ON_FAST, 0);

	if(LastOff)
	{
		LedGenerate(ChangeNumberToLedVal[Number2nd], DIMM_OFF_FAST, 0);
	}	
#endif
										
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif
}



void LedNumberDisplayForMenu(BYTE Number, BYTE LastOff)
{
#ifdef	DDL_CFG_DIMMER
	BYTE Number1st = 0;
	BYTE Number2nd = 0;

	if(Number >= 100)
	{
		Number1st = Number/100;
		Number -= (Number1st*100);

		LedGenerate(ChangeNumberToLedVal[Number1st], DIMM_ON_FAST, 0);
		LedGenerate(ChangeNumberToLedVal[Number1st], DIMM_OFF_FAST, 0);

		Number2nd = Number/10;

		if(Number2nd != 0x00)
			Number -= (Number2nd*10);
		
		LedGenerate(ChangeNumberToLedVal[Number2nd], DIMM_ON_FAST, 0);
		LedGenerate(ChangeNumberToLedVal[Number2nd], DIMM_OFF_FAST, 0);
	}
	else
	{
		if(Number >= 10)
		{
			Number1st = Number/10;
			Number -= (Number1st*10);

			LedGenerate(ChangeNumberToLedVal[Number1st], DIMM_ON_FAST, 0);
			LedGenerate(ChangeNumberToLedVal[Number1st], DIMM_OFF_FAST, 0);
		}	
	}

	Number2nd = Number;
	
	if(gbManageMode == _AD_MODE_SET)
	{
		// Advanced Mode에서는 개별 등록에 사용되어, 다음 모드 처리를 위해 *, # 버튼이 활성화 되어야 함.
		LedGenerate(ChangeNumberToLedVal[Number2nd]|LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_FAST, 0);
	}
	else
	{
		LedGenerate(ChangeNumberToLedVal[Number2nd], DIMM_ON_FAST, 0);
	}

	if(LastOff)
	{
		if(gbManageMode == _AD_MODE_SET)
		{
			// Advanced Mode에서는 개별 등록에 사용되어, 다음 모드 처리를 위해 *, # 버튼이 활성화 되어야 함.
			LedGenerate(ChangeNumberToLedVal[Number2nd]|LED_KEY_STAR|LED_KEY_SHARP, DIMM_OFF_FAST, 0);
		}
		else
		{
			LedGenerate(ChangeNumberToLedVal[Number2nd], DIMM_OFF_FAST, 0);
		}
	}	
#endif
										
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif
}


void LedNumberDisplayForMenu_1(BYTE Number, BYTE LastOff)
{
#ifdef	DDL_CFG_DIMMER
	BYTE Number1st = 0;
	BYTE Number2nd = 0;

	if(Number >= 10)
	{
		Number1st = Number/10;
		Number -= (Number1st*10);

		LedGenerate(ChangeNumberToLedVal[Number1st], DIMM_ON_FAST, 0);
		LedGenerate(ChangeNumberToLedVal[Number1st], DIMM_OFF_FAST, 0);
	}	

	Number2nd = Number;
	
	// 다음 모드 처리를 위해 *, # 버튼이 활성화 되어야 함.
	LedGenerate(ChangeNumberToLedVal[Number2nd]|LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_FAST, 0);

	if(LastOff)
	{
		if(gbManageMode == _AD_MODE_SET)
		{
			// Advanced Mode에서는 개별 등록에 사용되어, 다음 모드 처리를 위해 *, # 버튼이 활성화 되어야 함.
			LedGenerate(ChangeNumberToLedVal[Number2nd]|LED_KEY_STAR|LED_KEY_SHARP, DIMM_OFF_FAST, 0);
		}
		else
		{
			LedGenerate(ChangeNumberToLedVal[Number2nd], DIMM_OFF_FAST, 0);
		}
	}	
#endif
										
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif
}


void FeedbackLedAllOnOff(void)
{
#ifdef	POWER_ON_MSG	
	if(ChineseGatemanCheck() != 0)
	{
		AudioFeedback(AVML_HELLO_MSG1, gcbBuzPwr, VOL_CHECK);
	}
	else
	{
		AudioFeedback(POWER_ON_MSG, gcbBuzPwr, VOL_CHECK);
	}
#else 
	#ifdef	DDL_CFG_NO_BUZZER
	VoiceSetting(VOICE_MIDI_OPEN, VOL_CHECK);				
	#else
	BuzzerSetting(gcbBuzPwr, VOL_CHECK);
	#endif
#endif 

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER

#ifdef	DDL_MAIN_OC_LED
				P_LED_OPEN(1);
				P_LED_CLOSE(1);
#endif

	LedGenerate(LED_ALL, DIMM_ON_SLOW, 0);
	LedSetting(gcbWaitLedNextData, 25);
	LedGenerate(LED_ALL, DIMM_OFF_SLOW, 0);
	LedSetting(gcbLedOff, LED_DISPLAY_OFF);
#endif

#ifdef	DDL_CFG_LED_TYPE1
	LedSetting(gcbNoDimLed2sOn, LED_DISPLAY_OFF);
#endif

#ifdef	DDL_CFG_LED_TYPE2
	LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif

#ifdef	DDL_CFG_LED_TYPE3
	LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif

#ifdef	DDL_CFG_LED_TYPE4
	LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif

#ifdef	DDL_CFG_LED_TYPE5
	LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif

#ifdef	DDL_CFG_LED_TYPE6
	LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif

#ifdef	DDL_CFG_LED_TYPE8
		LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif

#ifdef	DDL_CFG_LED_TYPE7
		LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif

	//내부 Low Battery LED가 있는 제품은 전원 인가할 때 해당 LED도 켜지도록(LED 동작 확인용) 처리
#ifdef	DDL_CFG_INSIDE_LED_LOW
	P_LED_LOW(1);
#endif	
}


#if 0
void FeedbackJigModeEntering(void)
{
	AudioFeedback(0xFFFF, gcbBuzPwrErr, VOL_CHECK);
	LedSetting(gcbLedPwr, LED_DISPLAY_OFF);
}
#endif


void FeedbackKeyPadLedOnBuzOnly(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);
}


void FeedbackKeyPadLedOnLedOnly(void)
{
	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedGenerate(LED_KEYPAD_ALL, DIMM_ON_SLOW, 0);
#endif
	
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}


void FeedbackKeyPadLedOn(WORD wVoiceData, BYTE bVoiceMode)
{
	FeedbackKeyPadLedOnBuzOnly(wVoiceData, bVoiceMode);
	FeedbackKeyPadLedOnLedOnly();
}


void FeedbackKeyStarOn(WORD wVoiceData, BYTE bVoiceMode)
{
	if (wVoiceData)
		AudioFeedback(wVoiceData, gcbBuzReg, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
//	LastDimmingDisplayClear(DIMM_ON_FAST);
	LedGenerate(LED_KEY_STAR, DIMM_ON_SLOW, 0);
#endif
		
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}


void FeedbackKeySharpOn(WORD wVoiceData, BYTE bVoiceMode)
{
	if (wVoiceData)
		AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
//	LastDimmingDisplayClear(DIMM_ON_FAST);
	LedGenerate(LED_KEY_SHARP, DIMM_ON_SLOW, 0);
#endif
			
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}


void FeedbackAdvancedMenuOn(WORD wVoiceData, BYTE bVoiceMode)
{
#ifdef	DDL_CFG_DIMMER
	WORD LedSwitch;
#endif 
	
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	if(PackConnectedCheck() || InnerPackConnectedCheck())
	{
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
		if(GetPackCBARegisterSet())
		{
			LedSwitch = LED_KEY_1|LED_KEY_STAR|LED_KEY_SHARP;
		}
		else
#endif
		{
			LedSwitch = LED_ADVANCED_MENU_PACK;
		}
		
		if(gbModuleProtocolVersion >= 0x30 || gbInnerModuleProtocolVersion >= 0x30)
		{
			LedSwitch |= LED_KEY_7;
		}
		LedGenerate(LedSwitch, DIMM_ON_SLOW, 0);
	}
	else
	{
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
		if(GetPackCBARegisterSet())
		{
			// Advanced Mode에서 등록된 CBA 모듈이 장착될 경우 CBA 등록/삭제를 제외한 나머지 메뉴 설정 모드는 모두 disable
			LedSwitch = LED_KEY_1|LED_KEY_STAR|LED_KEY_SHARP;
			LedGenerate(LedSwitch, DIMM_ON_SLOW, 0);
		}
		else
#endif
		{
			LedGenerate(LED_ADVANCED_MENU, DIMM_ON_SLOW, 0);
		}
	}
#endif


#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}


void FeedbackNormalMenuOn(WORD wVoiceData, BYTE bVoiceMode)
{
#ifdef	DDL_CFG_DIMMER
	WORD LedSwitch;
#endif 	
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
#if defined (DDL_CFG_MS)
	/* MS 사양 setting 에 의해 지원 하는 인증 수단 관련 menu LED 를 control 한다 */
	LedGenerate(MS_Make_MainMenu_LED(), DIMM_ON_SLOW, 0);
#else 
	if(PackConnectedCheck() || InnerPackConnectedCheck())
	{
		LedSwitch = LED_NORMAL_MENU_PACK;

#ifdef	_NEW_YALE_ACCESS_UI
		if(gbModuleProtocolVersion >= 0x30 || gbInnerModuleProtocolVersion >= 0x30)
		{
			LedSwitch |= LED_KEY_7;
		}
#endif	// _NEW_YALE_ACCESS_UI

		LedGenerate(LedSwitch, DIMM_ON_SLOW, 0);
	}
	else
	{
		LedGenerate(LED_NORMAL_MENU, DIMM_ON_SLOW, 0);
	}
#endif 
#endif
				
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}


void FeedbackLockSettinglMenuOn(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
#if defined (DDL_CFG_MS)
	{
		/* MS 사양은 언어 선택 메뉴 생성 안하게 */
		WORD wTemp = LED_KEY_5; // MSB 영역이 혹시나 토글 안될 까봐 미리 word type 에 넣고 변환 
		wTemp = (LED_LOCK_SETTING_MENU & ~(wTemp));
		LedGenerate(wTemp, DIMM_ON_SLOW, 0);
	}
#else 
	LedGenerate(LED_LOCK_SETTING_MENU, DIMM_ON_SLOW, 0);
#endif 
#endif
				
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}



void Feedback13MenuOn(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedGenerate(LED_KEY_1|LED_KEY_3|LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_SLOW, 0);
#endif
					
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}

void Feedback139MenuOn(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedGenerate(LED_KEY_1|LED_KEY_3|LED_KEY_9|LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_SLOW, 0);
#endif
					
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}

void Feedback138MenuOn(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
#ifdef DDL_CFG_ADD_REMOCON_LINK_FUNCTION
	LedGenerate(LED_KEY_1|LED_KEY_3|LED_KEY_8|LED_KEY_9|LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_SLOW, 0);
#else	//DDL_CFG_ADD_REMOCON_LINK_FUNCTION
	LedGenerate(LED_KEY_1|LED_KEY_3|LED_KEY_8|LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_SLOW, 0);
#endif	//DDL_CFG_ADD_REMOCON_LINK_FUNCTION
#endif	//DDL_CFG_DIMMER
					
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}

void Feedback12MenuOn(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedGenerate(LED_KEY_1|LED_KEY_2|LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_SLOW, 0);
#endif
					
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}

void Feedback123MenuOn(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedGenerate(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_SLOW, 0);
#endif
					
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}

void Feedback1234MenuOn(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedGenerate(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4 |LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_SLOW, 0);
#endif
					
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}

void Feedback123456MenuOn(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedGenerate(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4 |LED_KEY_5 |LED_KEY_6 |LED_KEY_STAR|LED_KEY_SHARP, DIMM_ON_SLOW, 0);
#endif
					
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}


void FeedbackSelectedMenuOn(WORD wVoiceData, BYTE bVoiceMode, WORD LedSwitch)
{
	AudioFeedback(wVoiceData, gcbBuzSta, bVoiceMode);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedGenerate(LedSwitch, DIMM_ON_SLOW, 0);
#endif
					
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}


void FeedbackError(WORD wVoiceData, BYTE bVoiceMode)
{
	gfMute = 0;

	AudioFeedback(wVoiceData, gcbBuzErr, bVoiceMode); 		

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedErr, LED_DISPLAY_OFF);	
#endif
						
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLedErr4, LED_DISPLAY_OFF);
#endif
}

void FeedbackModeCompletedLedOff(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzReg, bVoiceMode);	

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LastDimmingDisplayClear(DIMM_OFF_FAST);
#endif
							
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
#endif
}


void FeedbackModeCompleted(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzReg, bVoiceMode);	

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedOk, LED_DISPLAY_OFF);
#endif
							
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_DISPLAY_OFF);
#endif
}


void FeedbackModeCompletedKeepMode(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzReg, bVoiceMode);	

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedOk, LED_DISPLAY_OFF);
	LedGenerate(LED_KEY_STAR, DIMM_ON_SLOW, 0);
#endif
							
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}


void FeedbackModeClear(void)
{
	AudioFeedback(VOICE_MIDI_CANCLE, gcbBuzPwr, VOL_CHECK);	

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedOk, LED_DISPLAY_OFF);
#endif
							
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_DISPLAY_OFF);
#endif
}



void FeedbackMotorOpen(void)
{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
//BatteryCheck();// 2018-6-18 moonsungwoo, 모터 동작 이전에 배터리 체크를 하고 모터 동작 완료 후 피드백
#else
	BatteryCheck();
#endif

	if(gfLowBattery)
	{
//		AudioFeedback(VOICE_MIDI_LOWBATT, gcbBuzLow, VOL_HIGH);	
//		AudioFeedback(102, gcbBuzLow, VOL_HIGH);		//건전지를 교체해 주세요
		AudioFeedback(AVML_CHG_BATTERY, gcbBuzLow, VOL_HIGH);		//건전지를 교체해 주세요
		PackAlarmReportLowBatterySet();
	}
	else
	{
		AudioFeedback(AVML_OPEN_MSG, gcbBuzOpn, VOL_CHECK|MANNER_CHK);	
	}

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedOpen, LED_DISPLAY_OFF);
#ifdef	DDL_MAIN_OC_LED
	P_LED_CLOSE(0);
	P_LED_OPEN(1);
#endif

#endif
								
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	//LedSetting(gcbNoDimLed2sOn, LED_DISPLAY_OFF);

#if defined	(DDL_CFG_LED_TYPE5) ||defined	(DDL_CFG_LED_TYPE6) 
		LedSetting(gcbNoDimBackOpenLed2sOn, LED_DISPLAY_OFF);
#elif defined (DDL_CFG_LED_TYPE7)
			LedSetting(gcbNoDimPassage, LED_DISPLAY_OFF);
	#else
		LedSetting(gcbNoDimLed2sOn, LED_DISPLAY_OFF);
	#endif

#endif
}



void FeedbackMotorClose(void)
{
#ifdef DDL_CFG_LED_TYPE7
	BYTE bTmp;
#endif
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
//BatteryCheck();// 2018-6-18 moonsungwoo, 모터 동작 이전에 배터리 체크를 하고 모터 동작 완료 후 피드백
#else
	BatteryCheck();
#endif

	if(gfLowBattery)
	{
//		AudioFeedback(VOICE_MIDI_LOWBATT, gcbBuzLow, VOL_HIGH);	
//		AudioFeedback(102, gcbBuzLow, VOL_HIGH);		//건전지를 교체해 주세요
		AudioFeedback(AVML_CHG_BATTERY, gcbBuzLow, VOL_HIGH);		//건전지를 교체해 주세요
		PackAlarmReportLowBatterySet();
	}
	else
	{
#if defined 	(DDL_CFG_CLUTCH) || defined (DDL_CFG_CMPL_CLUTCH) || defined (DDL_CFG_ONEWAY_ONESENSOR_CLUTCH)
		AudioFeedback(VOICE_MIDI_CLOSE, gcbBuzCls, VOL_CHECK|MANNER_CHK);	
#else 
		AudioFeedback(AVML_CLOSE_MSG, gcbBuzCls, VOL_CHECK|MANNER_CHK);	
#endif 
	}
	
	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedClose, LED_DISPLAY_OFF);
#ifdef	DDL_MAIN_OC_LED
	P_LED_OPEN(0);
	P_LED_CLOSE(1);
#endif

#endif
									
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	//LedSetting(gcbNoDimLed2sOn, LED_DISPLAY_OFF);
	
#if defined	(DDL_CFG_LED_TYPE5) ||defined	(DDL_CFG_LED_TYPE6) 
		LedSetting(gcbNoDimBackCloseLed2sOn, LED_DISPLAY_OFF);
#elif defined (DDL_CFG_LED_TYPE7)
		bTmp = MotorSensorCheck_Second();	
		if(bTmp == SENSOR_CLOSE_STATE)
		{
			LedSetting(gcbNoDimSecure, LED_DISPLAY_OFF);
		}
		else
		{
			LedSetting(gcbNoDimSafety, LED_DISPLAY_OFF);
		}

	#else		
		LedSetting(gcbNoDimLed2sOn, LED_DISPLAY_OFF);
	#endif
#endif

	gfMute = 0;

#ifdef DDL_AAAU_AUTOLOCK_HOLD //호주 및 뉴질랜드 전용 autolock hold 기능(Autohold 상태에서 OC 버튼을 제외한 수단으로  safty mode로 변경 되었을 경우에도 Autolockhold상태를 해제 하기 위홤)
	if(AutolockHoldSettingLoad() == AUTOLOCKHOLD)
	{
		AutolockHoldSetting(AUTOLOCKUNHOLD);	
	}
#endif //DDL_AAAU_AUTOLOCK_HOLD #endif
}



void FeedbackLockIn(void)
{
	gfMute = 0;

//	AudioFeedback(111, gcbBuzErr3, VOL_HIGH);		// The Internal forced lock
	AudioFeedback(AVML_IN_FORCE_LOCK, gcbBuzErr3, VOL_HIGH);		// The Internal forced lock

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedLock, LED_DISPLAY_OFF);
#endif
									
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLedErr3, LED_DISPLAY_OFF);
#endif
}

#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN) \
  || defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER) 
void FeedbackLockInOpenAllowState(void)
{
	gfMute = 0;
	AudioFeedback(AVML_IN_FORCE_LOCK, gcbBuzErr3, VOL_HIGH);		// The Internal forced lock
}
#endif 

void FeedbackLockOut(void)
{
	gfMute = 0;

//	AudioFeedback(110, gcbBuzErr3, VOL_HIGH);		// The Outter forced lock
	AudioFeedback(AVML_OUT_FORCE_LOCK, gcbBuzErr3, VOL_HIGH);		// The Outter forced lock

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedLock, LED_DISPLAY_OFF);
#endif
									
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLedErr3, LED_DISPLAY_OFF);
#endif
}


void FeedbackBuzStopLedDimOff(void)
{
	// 1회용 무음 모드 진입 후 비밀번호 입력 없이 *버튼이나 #버튼 입력되어 종료될 경우 1회용 무음 모드 바로 해제하기 위해 추가
	gfMute = 0;

//	AudioFeedback(0, gcbBuzOff, VOL_CHECK);	
	
	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LastDimmingDisplayClear(DIMM_OFF_SLOW);
	LedSetting(gcbLedOff, LED_DISPLAY_OFF);
#endif
										
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
#endif
}



void FeedbackBuzStopLedOff(void)
{
// 저전압 경고 알림 중 카드나 버튼이 떨어질 경우 바로 안내가 종료되는 부분 때문에 해당 부분은 삭제	
//	AudioFeedback(0, gcbBuzOff, VOL_CHECK); 
		
	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedOff, LED_DISPLAY_OFF);
#endif
											
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
#endif
}



void FeedbackDoorSensorDetect(void)
{
	AudioFeedback(VOICE_MIDI_AUTOLOCK, gcbBuzMag, VOL_CHECK|MANNER_CHK);
}


void FeedbackAlarm(BYTE bMode)
{
	if(bMode == FEEDBACK_ALARM_HIGHTEMP)
	{
//		AudioFeedback(115, gcbBuzFire, VOL_HIGH);		// Fire alarm
		AudioFeedback(AVML_FIRE_ALARM, gcbBuzFire, VOL_HIGH);		// Fire alarm
	}	
	else if(bMode == FEEDBACK_ALARM_BROKEN)
	{
//		AudioFeedback(113, gcbBuzBroken, VOL_HIGH);	// Damage alarm.
		AudioFeedback(AVML_TAMPER_ALARM, gcbBuzBroken, VOL_HIGH);	// Damage alarm.
	}
	else
	{
//		AudioFeedback(114, gcbBuzWar, VOL_HIGH);		// 	Intruder alarm.
		AudioFeedback(AVML_INVADE_ALARM, gcbBuzWar, VOL_HIGH);		// 	Intruder alarm.
	}
		
	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedAlert, LED_DISPLAY_OFF);
#endif
									
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedErr3, LED_DISPLAY_OFF);
#endif

}



void FeedbackLockOutSetting(BYTE bMode)
{
	if(bMode != 4)
		AudioFeedback(VOICE_MIDI_BUTTON, gcbBuzNum, VOL_CHECK|MANNER_CHK);

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	if(bMode == 1)
	{
		LedGenerate(LED_KEY_1|LED_KEY_2|LED_KEY_3, BLINK_ON, 0);
	}
	else if(bMode == 2)
	{
		LedGenerate(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_6, BLINK_ON, 0);
	}
	else if(bMode == 3)
	{
		LedGenerate(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_6|LED_KEY_7|LED_KEY_8|LED_KEY_9, BLINK_ON, 0);
	}
	else if(bMode == 4)
	{
		LedGenerate(LED_KEYPAD_ALL, DIMM_ON_FAST, 0);
	}		
#endif
									
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLed100msOn, LED_DISPLAY_OFF);
#endif
}



void FeedbackTamperProofLockout(BYTE bCnt)
{
	if(GetLedMode())		return;
	
	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	if(bCnt > 120)
	{
		LedGenerate(LED_KEY_3, DIMM_ON_FAST, 0);
		LedSetting(gcbWaitLedNextData, 33);
		LedGenerate(LED_KEY_3, DIMM_OFF_FAST, 0);
		LedSetting(gcbWaitLedNextData, 33);
	}
	else if(bCnt > 60)
	{
		LedGenerate(LED_KEY_2, DIMM_ON_FAST, 0);
		LedSetting(gcbWaitLedNextData, 33);
		LedGenerate(LED_KEY_2, DIMM_OFF_FAST, 0);
		LedSetting(gcbWaitLedNextData, 33);
	}
	else
	{
		LedGenerate(LED_KEY_1, DIMM_ON_FAST, 0);
		LedSetting(gcbWaitLedNextData, 33);
		LedGenerate(LED_KEY_1, DIMM_OFF_FAST, 0);
		LedSetting(gcbWaitLedNextData, 33);
	}
#endif
										
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedTamperLockout, LED_DISPLAY_OFF);
#endif
}





/*
void FeedbackTenKeyBlink(WORD wVoiceData, BYTE bVoiceMode, BYTE bKeyVal)
{
	AudioFeedback(wVoiceData, gcbBuzNum, bVoiceMode);

#ifdef	DDL_CFG_DIMMER
	DimmOneKeyFastBlink(bKeyVal);	
#endif

#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif
}
*/

void FeedbackTenKeyBlink(WORD wVoiceData, BYTE bVoiceMode, BYTE bKeyVal)
{
	AudioFeedback(wVoiceData, gcbBuzNum, bVoiceMode);

#ifdef	DDL_CFG_DIMMER
	LedModeRefresh();
	LedGenerate(gDimmLedLastSwitch, BLINK_ON, bKeyVal);
#endif

#ifdef	DDL_CFG_LED_TYPE1
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif

#ifdef	DDL_CFG_LED_TYPE4
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif

#ifdef	DDL_CFG_LED_TYPE5
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif

#ifdef	DDL_CFG_LED_TYPE6
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif

#ifdef	DDL_CFG_LED_TYPE2
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif

#ifdef	DDL_CFG_LED_TYPE3
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlinkWithBack, LED_KEEP_DISPLAY);
#endif

}


void FeedbackTenKeyErrorBlink(WORD wVoiceData, BYTE bVoiceMode, BYTE bKeyVal)
{
	AudioFeedback(wVoiceData, gcbBuzErr, bVoiceMode);

#ifdef	DDL_CFG_DIMMER
	LedModeRefresh();
	LedGenerate(gDimmLedLastSwitch, BLINK_ON, bKeyVal);
#endif

#ifdef	DDL_CFG_LED_TYPE1
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif

#ifdef	DDL_CFG_LED_TYPE4
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif

#ifdef	DDL_CFG_LED_TYPE5
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif

#ifdef	DDL_CFG_LED_TYPE6
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif

#ifdef	DDL_CFG_LED_TYPE2
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlink, LED_KEEP_DISPLAY);
#endif

#ifdef	DDL_CFG_LED_TYPE3
	LedModeRefresh();
	LedSetting(gcbNoDimLedBlinkWithBack, LED_KEEP_DISPLAY);
#endif

}
void FeedbackCredentialInput(WORD wVoiceData, BYTE Number)
{
	AudioFeedback(wVoiceData, gcbBuzNum, VOL_CHECK);
	LedModeRefresh();
	LedNumberDisplayForMenu(Number, 0);	
}


void FeedbackCredentialInput_1(WORD wVoiceData, BYTE Number)
{
	AudioFeedback(wVoiceData, gcbBuzNum, VOL_CHECK);
	LedModeRefresh();
	LedNumberDisplayForMenu_1(Number, 0);	
}


void FeedbackOngoing(void)
{
	AudioFeedback(VOICE_MIDI_BUTTON2, gcbBuzNum, VOL_CHECK);
}


void FeedbackPincodeDisplay(BYTE KeyVal)
{
	FeedbackOngoing();
	LedModeRefresh();
	LedNumberDisplay(KeyVal, 1);
}



void FeedbackErrorNumberOn(WORD wVoiceData, BYTE bVoiceMode, BYTE Number)
{
	AudioFeedback(wVoiceData, gcbBuzErr, bVoiceMode); 		

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedErr, LED_DISPLAY_OFF);	
	LedNumberDisplayForMenu(Number, 0);	
#endif
						
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLedErr4, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}


void FeedbackErrorModeKeepOn(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzErr, bVoiceMode); 		

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedErr, LED_DISPLAY_OFF);	
	LedGenerate(LED_KEYPAD_ALL, DIMM_ON_SLOW, 0);
#endif
						
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLedErr4, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
}


void FeedbackErrorFactoryReset(WORD wVoiceData, BYTE bVoiceMode)
{
	AudioFeedback(wVoiceData, gcbBuzErr, bVoiceMode); 		

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedErr, LED_DISPLAY_OFF);	
#endif
						
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLedErr4, LED_DISPLAY_OFF);
#endif
}



//------------------------------------------------------------------------------
/** 	@brief	Alarm Feedback Resetting Process
	@param	[BuzData] : 침입/고온/파손에 따른 경보음 종류
	@return 	None
	@remark 경보음 발생을 지속적으로 설정하기 위해 확인
*/
//------------------------------------------------------------------------------
void AlarmFeedbackProcess(WORD const* BuzData)
{
	if(GetBuzPrcsStep() == 0)
	{
		BuzzerSetting(BuzData, VOL_HIGH);
	}
	if((GetLedMode()== 0) && (GetMainMode() == 0))
	{

		LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
		LedSetting(gcbLedAlert, LED_DISPLAY_OFF);
#endif
									
#ifdef	DDL_CFG_NO_DIMMER
		LedSetting(gcbNoDimLedErr3, LED_DISPLAY_OFF);
#endif
	}
}



void FeedbackSystemError(BYTE SystemErrorMode)
{
	LedModeRefresh();
					
	switch(SystemErrorMode)
	{
		case SYSTEM_ERR_FRONT_NO_CONNECT:
#ifdef DDL_CFG_NO_BUZZER			
			AudioFeedback(VOICE_MIDI_ERROR,gcbBuzSysFrontNoConnect, VOL_HIGH);
#else 
			BuzzerSetting(gcbBuzSysFrontNoConnect, VOL_HIGH);
#endif 
			break;

		case SYSTEM_ERR_MEMORY_IC:
#ifdef DDL_CFG_NO_BUZZER			
			AudioFeedback(VOICE_MIDI_ERROR,gcbBuzSysErr4, VOL_HIGH);
#else 
			BuzzerSetting(gcbBuzSysErr4, VOL_HIGH);
#endif 
#ifdef	DDL_CFG_DIMMER
			LedNumberDisplay(4, 1);
#endif
			break;
			
		case SYSTEM_ERR_TOUCH_IC:
#ifdef DDL_CFG_NO_BUZZER			
			AudioFeedback(VOICE_MIDI_ERROR,gcbBuzSysErr5, VOL_HIGH);
#else 
			BuzzerSetting(gcbBuzSysErr5, VOL_HIGH);
#endif 
#ifdef	DDL_CFG_DIMMER
			LedNumberDisplay(5, 1);
#endif
			break;

		case SYSTEM_ERR_MOTOR_SENSOR:
#ifdef DDL_CFG_NO_BUZZER			
			AudioFeedback(VOICE_MIDI_ERROR,gcbBuzSysErr6, VOL_HIGH);
#else 			
			BuzzerSetting(gcbBuzSysErr6, VOL_HIGH);
#endif 
#ifdef	DDL_CFG_DIMMER
			LedNumberDisplay(6, 1);
#endif
			break;

		case SYSTEM_ERR_CARD_DETECT:
#ifdef DDL_CFG_NO_BUZZER			
			AudioFeedback(VOICE_MIDI_ERROR,gcbBuzSysErr7, VOL_HIGH);
#else 			
			BuzzerSetting(gcbBuzSysErr7, VOL_HIGH);
#endif 
#ifdef	DDL_CFG_DIMMER
			LedNumberDisplay(7, 1);
#endif
			break;

		case SYSTEM_ERR_RTC_IC:
#ifdef DDL_CFG_NO_BUZZER			
			AudioFeedback(VOICE_MIDI_ERROR,gcbBuzSysErr8, VOL_HIGH);
#else 			
			BuzzerSetting(gcbBuzSysErr8, VOL_HIGH);
#endif 
#ifdef	DDL_CFG_DIMMER
			LedNumberDisplay(8, 1);
#endif
			break;

		case SYSTEM_ERR_HANDING_LOCK:
#ifdef DDL_CFG_NO_BUZZER			
			AudioFeedback(VOICE_MIDI_ERROR,gcbBuzSysErr9, VOL_HIGH);
#else 			
			BuzzerSetting(gcbBuzSysErr9, VOL_HIGH);
#endif 
#ifdef	DDL_CFG_DIMMER
			LedNumberDisplay(9, 1);
#endif
			break;

		case SYSTEM_ERR_PACK:
#ifdef DDL_CFG_NO_BUZZER			
			AudioFeedback(VOICE_MIDI_ERROR,gcbBuzSysErr10, VOL_HIGH);
#else 			
			BuzzerSetting(gcbBuzSysErr10, VOL_HIGH);
#endif 
#ifdef	DDL_CFG_DIMMER
			LedNumberDisplay(10, 1);
#endif
			break;

		case SYSTEM_ERR_BIO: 
//			AudioFeedback(118, gcbBuzSysErr11, VOL_HIGH); // 지문 장치가 정상 동작 하지 않습니다.
			AudioFeedback(AVML_MALFUNCTION_FPM_MSG, gcbBuzSysErr11, VOL_HIGH); // 지문 장치가 정상 동작 하지 않습니다.
#ifdef	DDL_CFG_DIMMER
			LedNumberDisplay(11, 1);
#endif
			break;

		case SYSTEM_ERR_BIO_REGNUM: 
//			AudioFeedback(119, gcbBuzSysErr12, VOL_HIGH); //지문인증 모듈을 점검하세요
			AudioFeedback(AVML_CHECK_FPM_MSG, gcbBuzSysErr12, VOL_HIGH); //지문인증 모듈을 점검하세요
#ifdef	DDL_CFG_DIMMER
			LedNumberDisplay(12, 1);
#endif
			break;
	}

#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLedErr4, LED_DISPLAY_OFF);
#endif
}



void FeedbackAllCodeLockOut(void)
{
	gfMute = 0;

	AudioFeedback(VOICE_MIDI_ERROR, gcbBuzErr, VOL_HIGH);	

	LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
	LedSetting(gcbLedLock, LED_DISPLAY_OFF);
#endif
									
#ifdef	DDL_CFG_NO_DIMMER
	LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
	LedSetting(gcbNoDimLedErr3, LED_DISPLAY_OFF);
#endif
}

#ifdef	DDL_TEST_SET_FOR_CARD
void Feedback_KC_Test_Noti(void)
{
	if(IsCardPowerOnForTest() == false) return ;
	
	if(gKCFeedbackTimer1s == 5)
	{
		// KC test 중 
		LedModeRefresh();

#ifdef	DDL_CFG_DIMMER
		LedGenerate(LED_KEY_SHARP, DIMM_ON_SLOW, 0);
		LedSetting(gcbWaitLedNextData, 25);
		LedGenerate(LED_KEY_SHARP, DIMM_OFF_SLOW, 0);
		LedSetting(gcbLedOff, LED_DISPLAY_OFF);
#endif
						
#ifdef	DDL_CFG_NO_DIMMER
#ifdef	DDL_CFG_LED_TYPE1
		LedSetting(gcbNoDimLed2sOn, LED_DISPLAY_OFF);
#endif
	
#ifdef	DDL_CFG_LED_TYPE2
		LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif
	
#ifdef	DDL_CFG_LED_TYPE3
		LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif
	
#ifdef	DDL_CFG_LED_TYPE4
		LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif
	
#ifdef	DDL_CFG_LED_TYPE5
		LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif
	
#ifdef	DDL_CFG_LED_TYPE6
		LedSetting(gcbNoDimAllLed2sOn, LED_DISPLAY_OFF);
#endif
#endif 		
	}
	else if(gKCFeedbackTimer1s == 0)
	{
		gKCFeedbackTimer1s = 5;
	}
}
#endif 




