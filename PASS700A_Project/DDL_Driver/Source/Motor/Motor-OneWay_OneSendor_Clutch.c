#include "Main.h"



uint16_t		sns_o, sns_c;
void	test_Motor( void )
{
#if 0
	uint32_t		dir = 0;

	P_SNS_EN( 0 );
	Delay(SYS_TIMER_1MS);

	if ( sns_o = P_SNS_OPEN_T )
		dir = 1;
	else if ( sns_c = P_SNS_CLOSE_T )
		dir = 2;


	if ( dir == 1 ) {
		_MOTOR_CLOSE;
		do {
			sns_c = P_SNS_CLOSE_T;
		}
		while( !sns_c );

		_MOTOR_STOP;
	}
	else if ( dir == 2 ) {
		_MOTOR_OPEN;
		do {
			sns_o = P_SNS_OPEN_T;
		}
		while( !sns_o );

		_MOTOR_STOP;
	}
	
	P_SNS_EN( 1 );
#endif 
}


/******************************************************************/
#if 0
void OpenPinToInputPin(BYTE Data)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	if(Data)
	{
		GPIO_InitStruct.Pin = M_OPEN_1_Pin|M_CLOSE_1_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
		GPIO_InitStruct.Pull = GPIO_PULLDOWN;
		HAL_GPIO_Init(M_OPEN_1_GPIO_Port, &GPIO_InitStruct);
	}
	else
	{
		GPIO_InitStruct.Pin = M_OPEN_1_Pin|M_CLOSE_1_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(M_OPEN_1_GPIO_Port, &GPIO_InitStruct);
	}
}
#endif 

void		rim_lock_sensor_on( void )
{
	P_SNS_EN( 0 );								//Sensor Power On (Acitive Low)
}

void		rim_lock_sensor_off( void )
{
	P_SNS_EN( 1 );								//Sensor Power Off (Acitive Low)
}

void		rim_lock_motor_open( void )
{
	_MOTOR_PWR_EN;
	_MOTOR_SLEEP_EXIT;
	_MOTOR_OPEN;
}

void		rim_lock_motor_close( void )
{
	_MOTOR_PWR_EN;
	_MOTOR_SLEEP_EXIT;
	_MOTOR_CLOSE;
}

void		rim_lock_motor_stop( void )
{
	_MOTOR_STOP;
	_MOTOR_PWR_DISABLE;
	_MOTOR_SLEEP_EN;
}

void		rim_lock_motor_hold( void )
{
	_MOTOR_PWR_EN;
	_MOTOR_SLEEP_EXIT;
	_MOTOR_BREAK;
}

uint32_t		rim_lock_is_motor_open( void )
{
	return	!P_SNS_CLUTCH_T;
}

uint32_t		rim_lock_is_motor_close( void )
{
	return	P_SNS_CLUTCH_T;
}

#ifdef	P_SNS_CENTER_T
uint32_t		rim_lock_is_motor_center( void )
{
	return	P_SNS_CENTER_T;
}
#endif

#ifdef	P_SNS_LOCK_T
uint32_t		rim_lock_is_motor_lock( void )
{
#ifdef	DDL_CFG_SNS_LOCK_ACTIVE_HIGH
	return	(P_SNS_LOCK_T);
#else 
	return	(!P_SNS_LOCK_T);
#endif 
}
#endif


ddl_motor_ctrl_t		Clutch_Mecha_motor  = {
	.TO_open= { 500, 0, 0, 75, 0, 0, 0, 0, 0},			// unit 2msec
	.TO_close= { 500, 0, 0, 75, 0, 0, 0, 0, 0},			// unit 2msec
	
	/*.TO_open= { 500, 0, 0, 75, 0, 0, 0, 0, 0},			// unit 2msec
	.TO_close= { 650, 55, 0, 0, 0, 0, _USE_MOTOR_TIME_SET, 0, 100},			// unit 2msec*/

	/*.TO_open= { 1000, 25, 0, 0, 0, 0, _USE_MOTOR_TIME_SET, 0, 0},			// unit 2msec
	.TO_close= { 650, 25, 0, 72, 0, 0, 0, 0, 100},			// unit 2msec*/

	/*.TO_open= { 1000, 25, 0, 75, 0, 0, 0, 0, 0},			// unit 2msec
	.TO_close= { 650, 25, 0, 75, 0, 0, 0, 0, 0},			// unit 2msec*/

	.init_sensor_hw	= NULL,
	.sensor_on		= rim_lock_sensor_on,
	.sensor_off		= rim_lock_sensor_off,

	.motor_open		= rim_lock_motor_open,
	//.motor_close		= rim_lock_motor_close,
	.motor_close		= rim_lock_motor_open,
	.motor_stop		= rim_lock_motor_stop,
	.motor_hold		= rim_lock_motor_hold,

	.is_motor_open	= rim_lock_is_motor_open,
	.is_motor_close	= rim_lock_is_motor_close,
#ifdef	P_SNS_CENTER_T
	.is_motor_center = rim_lock_is_motor_center,
#else 
	.is_motor_center 	= NULL, 
#endif 
#ifdef		P_SNS_LOCK_T	
	.is_motor_lock		= rim_lock_is_motor_lock
#else
	.is_motor_lock		= InnerForcedLockCheck
#endif
};

void SetupBatteryPwrFactor(void);	

void	Init_Motor( void )
{
#ifdef	P_SNS_LEFT_RIGHT_T
	HandingLockAutoRun();
#endif
	register_motor_ctrl( &Clutch_Mecha_motor );
	MotorStatusInitialCheck();
	SetupBatteryPwrFactor();
}

//=============================================================
// Inner Forced Lock set by O/C button
//=============================================================

#ifdef		P_SNS_LOCK_T	

uint32_t InnerForcedLockCheck(void) // 이 함수는 안쓰는 것으로 생각 한다. 
{
#ifdef	DDL_CFG_SNS_LOCK_ACTIVE_HIGH
	if(P_SNS_LOCK_T)	return 1;
#else 
	if(!P_SNS_LOCK_T)	return 1;
#endif 	
	return 0;
}

#else

BYTE gbInLockEn = 0; 

void InnerForcedLockSet(void)
{
	gbInLockEn = 0xAA;
	RomWrite(&gbInLockEn, (WORD)INLOCK_STATE, 1);	
}

void InnerForcedLockClear(void)
{
	gbInLockEn = 0xBB;
	RomWrite(&gbInLockEn, (WORD)INLOCK_STATE, 1);	
}


void InnerForcedLockSettingLoad(void)
{
	RomRead(&gbInLockEn, (WORD)INLOCK_STATE, 1);		
	if(gbInLockEn != 0xAA)
	{
		gbInLockEn = 0xBB;
	}
}

uint32_t InnerForcedLockCheck(void)
{
	if(gbInLockEn == 0xAA)	return 1;

	return 0;
}

#endif



float AdcToPwrVoltFactor;
float PwrVolt;


void SetupBatteryPwrFactor(void)			// 파워 전압 계산을 하기 위한 인자들 설정
{
///////////// 사용자 정의 변수 //////////////////////////////////////////////////////////////////////////////////////////////////////////
	float R1 = 5.6;						// 항상 소수점으로 표시 2 -> 2.0 			// 위쪽 분배 저항 R1,  (실제저항값) 나누기 (1000)해서 적기.. 5.6K옴  -> 5600/1000 = 5.6
	float R2 = 2.0;						// 항상 소수점으로 표시 2 -> 2.0			// 아래 분배 저항 R2,  (실제저항값) 나누기 (1000)해서 적기.. 2.0K옴  -> 2000/1000 = 2.0
	float AdcRefVolt = 3.3;				// ADC 레퍼런스 전압. Vref가 3.3V면 3.3, 3.0V면 3.0
//	BYTE AdcResolutionBit = 10;			// 12비트 레졸루션이면 12 씀,.. 10비트 레졸류션이면 10
	BYTE AdcResolutionBit = 12;			// 12비트 레졸루션이면 12 씀,.. 10비트 레졸류션이면 10
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	float AdcToVoltFactor;				// ADC값을 전압으로 변환해주는 팩터
	float VoltToPwrFactor;				// 분배된 전압을 pwr 전압으로 변환해 주는 팩터
	WORD AdcResolution;					// 레졸루션 10진수 값
	BYTE bCnt;

	AdcResolution = 1;
	for(bCnt = 0; bCnt < AdcResolutionBit; bCnt++)
	{	
		AdcResolution = AdcResolution * 2;
	}

	AdcToVoltFactor = AdcRefVolt / ((float)AdcResolution);				
	VoltToPwrFactor = R2 / (R1 + R2);							// 전압 분배 값

	AdcToPwrVoltFactor = AdcToVoltFactor / VoltToPwrFactor;

}

void PwrVoltCalculation(void)						// 파워 전압 계산
{
	PwrVolt = ((float)GetBatteryAdcValue()) * AdcToPwrVoltFactor;		// 파워 전압 계산	
}

BYTE MotorTimeIndexCalculation(void)				// 모터 구동 타임의 배열 인덱스 설정
{
	BYTE bMotorTimeIndex;
	
	if(PwrVolt > 6.99)	PwrVolt = 6.9;						// 전압이 6.9v이상이면 무조건 6.9볼트로 고정, 6.9볼트 이상은 계산하지 않는다.
	if(PwrVolt < 3.0)	PwrVolt = 3.0;						// 전압이 3v이하면 무조건 3볼트로 고정, 3볼트 이하는 계산하지 않는다.

	bMotorTimeIndex = (BYTE)((PwrVolt * 10.0) - 30.0);		// volt에서 30을 뺌, Index의 범위를 0~39(총 40종류, 3.0V, 3.1V,3.2V ~ 6.9V)까지 하기 위해서 30을 뺌

	if(bMotorTimeIndex > 39) bMotorTimeIndex = 39;		// 배열의 인덱스가 됨

	return bMotorTimeIndex;
}

BYTE MotorTimeGenerate(BYTE ArryIndex, BYTE MinTime10ms, BYTE MaxTime10ms)
{
	BYTE bCnt;
	BYTE bFinalMotorTime;
	float bTimeInterval;
	float bTime;

	bFinalMotorTime = 0;

	if(MinTime10ms > MaxTime10ms)
	{
		return 0;
	}

	if(MinTime10ms == MaxTime10ms)
	{
		return MinTime10ms;
	}

	bCnt = MaxTime10ms - MinTime10ms;
	bTimeInterval = ((float)bCnt) / (40.0 - 1.0);		// -1을 하는 이유는 40단계이지만 개수는 39개로 함.

	bTime = (float)MinTime10ms;	

	for(bCnt = 40; bCnt > 0; bCnt--)				// 직선보간법	
	{
		if( ArryIndex == (bCnt - 1) )
		{
			bFinalMotorTime = (BYTE)bTime;
			break;
		}
		
		bTime = bTime + bTimeInterval;
	}

	return bFinalMotorTime;

}

// 현재 파워 전압에 따른 모터 구동 시간 리턴 해줌
BYTE MotorTimeSetting(BYTE MinTime10ms, BYTE MaxTime10ms)		// 이 함수 구동 시간 STM32L151VBXXA 32Mhz 기준 130us
{
	BYTE bTmp;

	PwrVoltCalculation();
	bTmp = MotorTimeIndexCalculation(); 					// 파워 전압에 따른 모터 시간 배열 인덱스 추출
	bTmp = MotorTimeGenerate(bTmp, MinTime10ms, MaxTime10ms);		// 파워 전압 대 모터 구동 시간 테이블 생성

	return bTmp;
}



#define		TIME_PWR_FULL 		38
#define		TIME_PWR_LOW		72	



uint32_t MotorBackTurnTimeSetting_open(void)
{
	uint32_t RetTimeValue;
	
//	RetTimeValue = (uint32_t)MotorTimeSetting(TIME_PWR_FULL, TIME_PWR_LOW); 
	RetTimeValue = (uint32_t)MotorTimeSetting(TIME_PWR_FULL-1, TIME_PWR_LOW-1); 

	// 10ms Timer 기준으로 나온 값을 2ms Timer에 사용할 수 있도록 처리
	RetTimeValue = RetTimeValue * 5;
	
	return (RetTimeValue);
}
	

uint32_t MotorBackTurnTimeSetting_close(void)
{
	uint32_t RetTimeValue;
	
//	RetTimeValue = (uint32_t)MotorTimeSetting(TIME_PWR_FULL-5, TIME_PWR_LOW-5); 
	RetTimeValue = (uint32_t)MotorTimeSetting(TIME_PWR_FULL-7, TIME_PWR_LOW-7); 

	// 10ms Timer 기준으로 나온 값을 2ms Timer에 사용할 수 있도록 처리
	RetTimeValue = RetTimeValue * 5;
	
	return (RetTimeValue);
}


