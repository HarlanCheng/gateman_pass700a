//------------------------------------------------------------------------------
/** 	@file		ModeMenuCredentialSelect_T1.c
	@version 0.1.00
	@date	2016.11.29
	@brief	normal 및 advanced menu 상에서 3번 menu 대응  
	@remark	메뉴 모드
	@see	MainModeProcess_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.11.29		by hyojoon
*/
//------------------------------------------------------------------------------

#include "Main.h"

void ModeGotoMenuAdvancedCredentialSelect(void)
{
	gbMainMode = MODE_MENU_CREDENTIAL_SELECT;
	gbModePrcsStep = 0;
}

void ModeGotoMenuNormalCredentialSelect(void)
{
	gbMainMode = MODE_MENU_CREDENTIAL_SELECT;
	gbModePrcsStep = 1;
}

void ModeGotoMenuCredentialSelect(void)
{
	if(gbManageMode == _AD_MODE_SET)
	{
		ModeGotoMenuAdvancedCredentialSelect();
	}
	else
	{
		ModeGotoMenuNormalCredentialSelect();
	}
}

void AssignAdvancedCredentialSelectedMenu(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		case TENKEY_1:
#ifdef	DDL_CFG_FP
			ModeGotoOneFingerRegDelCheck();
#endif			
		break;
		
		case TENKEY_2:	
#ifdef	DDL_CFG_RFID
			ModeGotoOneCardRegDelCheck();
#endif
		break;
	
		default:
			TimeExpiredCheck();
			break;
	}
}


void MenuAdvancedCredentialSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		case TENKEY_1:	
#ifdef	DDL_CFG_FP
			MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_FINGER_SHARP, gbInputKeyValue); //지문 등록/삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
#endif			
			break;

		case TENKEY_2:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_CARD_SHARP, gbInputKeyValue);
                        break;

		case TENKEY_SHARP:
			AssignAdvancedCredentialSelectedMenu();
			break;
									
		case TENKEY_STAR:
			ReInputOrModeMove(ModeGotoMenuMainSelect);
			break;
			
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;			

		default:
			TimeExpiredCheck();
			break;
	}
}



void AssignNormalCredentialSelectedMenu(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		case TENKEY_1:
#ifdef	DDL_CFG_FP
			ModeGotoNormalAllFingerDelete();
#endif			
		break;
		
		case TENKEY_2:	
#ifdef	DDL_CFG_RFID
			ModeGotoAllCardDelete();
#endif
		break;
	
		default:
			TimeExpiredCheck();
			break;
	}
}


void MenuNormalCredentialSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		case TENKEY_1:	
#ifdef	DDL_CFG_FP
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_ALLFINGER_SHARP, gbInputKeyValue); 	//지문 (전체) 삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
#endif			
			break;

		case TENKEY_2:			// 카드/지문 전체 삭제
#ifdef	DDL_CFG_RFID
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_ALLCARD_SHARP, gbInputKeyValue);		//카드키 전체삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
#endif
                        break;

		case TENKEY_SHARP:
			AssignNormalCredentialSelectedMenu();
			break;
									
		case TENKEY_STAR:
			ReInputOrModeMove(ModeGotoMenuMainSelect);
			break;
			
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;			

		default:
			TimeExpiredCheck();
			break;
	}
}


//------------------------------------------------------------------------------
/** 	@brief	ModeMenuCredentialSelect
	@param	None
	@return 	None
	@remark  credential select menu
*/
//------------------------------------------------------------------------------
void ModeMenuCredentialSelect(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			if(GetSupportCredentialCount() == 2)
				Feedback12MenuOn(AVML_SELECT_MENU, VOL_CHECK);
			else if(GetSupportCredentialCount() == 3)
				Feedback123MenuOn(AVML_SELECT_MENU, VOL_CHECK);				
			else 
				Feedback1234MenuOn(AVML_SELECT_MENU, VOL_CHECK);

			gMenuSelectKeyTempSave = TENKEY_NONE;
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep = 11;
			break;
			
		case 1:
			if(GetSupportCredentialCount() == 2)
				Feedback12MenuOn(AVML_SELECT_MENU, VOL_CHECK);
			else if(GetSupportCredentialCount() == 3)
				Feedback123MenuOn(AVML_SELECT_MENU, VOL_CHECK);				
			else 
				Feedback1234MenuOn(AVML_SELECT_MENU, VOL_CHECK);

			gMenuSelectKeyTempSave = TENKEY_NONE;
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep = 15;
			break;
		
		case 11:
			MenuAdvancedCredentialSelectProcess();
			break;

		//Menu Mode in Normal mode	
		case 15:							
			MenuNormalCredentialSelectProcess();	
			break;

		default:
			ModeClear();
			break;
	}
}



