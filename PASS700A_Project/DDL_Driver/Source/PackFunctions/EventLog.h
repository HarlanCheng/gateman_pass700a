#ifndef __EVENTLOG_INCLUDED        
#define __EVENTLOG_INCLUDED   

#include "DefineMacro.h"
//#include "DefinePin.h"


//==============================================================================//
//	EVENTs For iRevo Module
//==============================================================================//

#define EV_ALL_REGISTRATION					0x08
#define EV_ALL_REMOVE                   				0x09

#define EV_OUTER_FORCED_LOCK_SET			0x0A
#define EV_OUTER_FORCED_LOCK_REL			0x0B


#define EV_REGDEVICE_IN						0x41		//등록할 장치 입력
#define EV_ALLREG_DEV_END					0x42		//전체 등록 완료
#define EV_ALLREG_DEV_CANCLE				0x43		//전체 등록 취소

#define EV_RESET_DATA						0x4B		// FOR CBA RESET DATA M -> L


#define EV_ADD_1_USER_CARD					0x50
#define EV_DELETE_1_USER_CARD				0x51
#define EV_USER_CARD_ADDED					0x52
#define EV_USER_CARD_DELETED				0x53
#define EV_GET_1_USER_CREDENTIAL			0x54
#define EV_GET_NUMBER_OF_CARDS_SPT		0x55


#define EV_ARM_REQEST						0x56
#define EV_USER_CREDENTIAL_UNLOCK			0x57
#define EV_USER_CREDENTIAL_LOCK				0x59



#define EV_CHANGE_TO_ADD_USER_CREDENTIAL_MODE		0x5A


#define EV_SOFTWARE_RESET					0x39

#define EV_LOCK_LOOP_TEST					0x1D

//==============================================================================//
//	EVENTs For Initialization
//==============================================================================//

#define	EV_DEVICE_CHK					0x44		//장치 확인

//==============================================================================//
//	EVENTs For Protocol V1
//==============================================================================//

#define EV_SET_USER_SCHEDULE		0x1F
#define EV_GET_USER_SCHEDULE		0x20
#define EV_ENABLE_USER_SCHEDULE	0x23

#define EV_GET_TIME					0x34

//==============================================================================//
//	SOURCEs For N-Protocol
//==============================================================================//

#define ES_MODULE_EVENT 		0xA0
#define ES_MODULE_ACK			0xA1
#define ES_MODULE_NACK			0xA2
#define ES_MODULE_EVENT_NOACK	0xA3


#define ES_LOCK_EVENT			0xB0
#define ES_LOCK_ACK 				0xB1
#define ES_LOCK_NACK			0xB2

//==============================================================================//
//	Credential Type
//==============================================================================//

#define	CREDENTIALTYPE_PINCODE			0x00
#define	CREDENTIALTYPE_CARD				0x01
#define	CREDENTIALTYPE_FINGERPRINT			0x02
#define	CREDENTIALTYPE_GMTOUCHKEY		0x03
#define	CREDENTIALTYPE_DS1972TOUCHKEY	CREDENTIALTYPE_GMTOUCHKEY
#define	CREDENTIALTYPE_FACE				0x04
#define	CREDENTIALTYPE_IRIS					0x05
#define	CREDENTIALTYPE_ALL_CREDENTIALS	0x0E
#define	CREDENTIALTYPE_BLE					0x10
#define	CREDENTIALTYPE_REMOCON			0x11
#define	CREDENTIALTYPE_PANIC_FINGER		0x12
#define	CREDENTIALTYPE_PANIC_CODE			0x13

/********************* RF module to door lock messages *********************/

#define F0_LOCK_OPERATION						0x10
#define F0_GET_LOCK_STATUS						0x11
#define F0_UNLOCK_BY_ACCESSORY				0x14
#define F0_GET_DOOR_STATUS						0x2B

#define F0_ADD_1_USER_CODE						0x12
#define F0_DELETE_USER_CODE 					0x13
#define F0_GET_1_USER_CODE						0x15
#define F0_SET_1_USER_CODE_STATUS				0x16
#define F0_GET_1_USER_CODE_STATUS				0x17

#define F0_ADD_1_USER_CODE_EX					0x82
#define F0_DELETE_USER_CODE_EX 					0x83
#define F0_GET_1_USER_CODE_EX					0x85
#define F0_SET_1_USER_CODE_STATUS_EX			0x86
#define F0_GET_1_USER_CODE_STATUS_EX			0x87

#define F0_SET_CONFIGURATION_PARAMETER			0x18
#define F0_GET_CONFIGURATION_PARAMETER		0x19
#define F0_GET_BATTERY_LEVEL					0x1A
#define F0_GET_BATTERY_LEVEL_REPORT			0x1A
#define F0_GET_PRODUCT_DATA					0x1B


#define F0_GET_NUMBER_OF_USERS_SUPPORTED		0x1C
#define F0_GET_LOCKFUNCTIONS					0x1E
#define F0_GET_NUMBER_OF_USERS_SUPPORTED_EX	0x8C



 
#define F0_NETWORK_REGISTER_EVENT								0x21
#define F0_GET_SERIAL_NUMBER									0x22
#define F0_SET_CONFIGURATION_PARAMETER_TO_DEFAULT_VALUE		0x24
#define F0_GET_HARDWARE_VERSION								0x25
#define F0_SET_LED_MODE											0x26
#define F0_GET_LED_MODE											0x27
#define F0_SET_DAILY_REPEATING_SCHEDULE_WITH_SLOT_NUMBER	0x28
#define F0_GET_DAILY_REPEATING_SCHEDULE_WITH_SLOT_NUMBER	0x29
#define F0_GET_MODEL_IDENTIFIER_STRING							0x2A
#define F0_ENABLE_DISABLE_ALL_SCHEDULE_TYPE					0x5E
#define F0_GET_SCHEDULE_STATUS									0x5F
#define F0_ENABLE_DISABLE_DAILY_REPEATING_SCHEDULE_WITH_SLOT_NUMBER	0x60
#define F0_SET_USER_YEAR_DAY_SCHEDULE							0x61
#define F0_GET_USER_YEAR_DAY_SCHEDULE							0x62
#define F0_ENABLE_DISABLE_YEAR_DAY_SCHEDULE					0x63
#define F0_GET_USER_SCHEDULES_SUPPORTED						0x64
#define F0_GET_ALL_FIRMWARE_VERSIONS							0x65
#define F0_GET_Z_WAVE_DEVICE_ID									0x66
#define F0_SEND_FW_IMAGE										0x67
#define F0_FW_IMAGE_COMPLETE									0x68
#define F0_REBOOT												0xA3

#define F0_GET_PROTOCOL_VERSION								0xA4
#define F0_ENCRYPTION_MESSAGE									0xE1
#define F0_ENCRYPTION_KEY_CHANGE								0xE2
#define F0_ENCRYPTION_GET_CHECKSUM							0xE3

#define F0_RESET4A												0x4A

#define F0_SET_CONFIGURATION_PARAMETER_TLV					0x49		//0x00000000 : Disable										//ADDED SINCE 3.2
																		//0xFFFFFFFF : Enable , auto-lock : 3S , auto-re-lock : 7S			//ADDED SINCE 3.2
																		//0x0000YYYY : Enable , auto-lock : 3S , auto-re-lock : YYYYS		//ADDED SINCE 3.2
																		//0xXXXXYYYY : Enable , auto-lock : XXXXS , auto-re-lock : YYYYS	//ADDED SINCE 3.2
#define F0_GET_CONFIGURATION_PARAMETER_TLV					0x4C		//ADDED SINCE 3.2


/********************* RF module to lock bootloader messages *********************/

#define F0_RUN_APPLICATION							0xA5
#define F0_BEGIN_FLASH								0xA6
#define F0_LOCK_FIRMWARE_WRITE						0xA7
#define F0_FIRMWARE_UPGRADE_COMPLETE				0xA8

/********************* Door lock to RF module messages *********************/

#define F0_ALARM_REPORT									0x30
#define F0_USER_ADDED									0x31
#define F0_USER_DELETED									0x32

#define F0_ALARM_REPORT_EX								0x80
#define F0_USER_ADDED_EX								0x81
#define F0_USER_DELETED_EX								0x84


#define F0_CONFIGRATION_REPORT							0x33
#define F0_RF_MODULE_REGISTER							0x36
#define PA_RF_MODULE_REGISTER_TIMEOUT					180u // sec 

#define F0_RFM_CHECK									0x37
#define F0_GET_TIME_AND_DATE							0x38
#define F0_ENCRYPTION_KEY_EXCHANGE						0xE2
#define F0_GET_CHECKSUM_OF_EXISTING_ENCRYPTION_KEY	0xE3
#define F0_RF_MODULE_DEVICE_INFORMATION				0x44


#define EV_REMOCON_CMD_SEND							0x9F		

#define F0_CONFIGURATION_REPORT_TLV					0x4D	//ADDED SINCE 3.2

/********************* Alram Defines *********************/

#define AL_CODE_CHANGED_ADDED				0x70
#define AL_CODE_DELETED						0x21
#define AL_TAMPER_ALARM						0xA1
#define AL_MANUAL_UNLOCKED					0x16
#define AL_RF_OP_UNLOCKED					0x19
#define AL_KEYPAD_UNLOCKED					0x13
#define AL_MANUAL_LOCKED					0x15
#define AL_RF_OP_LOCKED						0x18
#define AL_KEYPAD_LOCKED					0x12
#define AL_DEADBOLT_JAMMED					0x09
#define AL_LOCK_RESET_TO_FACT				0x30
#define AL_NON_ACCESS						0x26
#define AL_TOO_LOW_TO_OPERATE				0xA9		// Too Low To operate level(0~6%)
#define AL_CRITICAL_BATT_LEVEL				0xA8		// Critial Battery level(7~13%)
#define AL_LOW_BATTERY						0xA7		// Low Battery level(14~20%)
#define AL_HANDING_CYCLE_COMPLETED		0x81
#define AL_AUTO_LOCK_OP_LOCKED			0x1B
#define AL_DUPLICATE_PIN_ERROR				0x71
#define AL_DUPLICATE_RFID_ERROR				0x72
#define REGISTERING_CREDENTIAL_MODE_ERROR	0x73
#define AL_RF_MODULE_POWER_CYCLED			0x82
#define AL_DISABLED_USER_AT_KEYPAD			0x83
#define AL_OUT_OF_SCHEDULE 					0x84
#define AL_CARD_UNLOCKED					0x90
#define AL_FINGERPRINT_UNLOCKED			0x91
#define AL_LOCK_STATUS_REPORT				0x9A
#define AL_FORCED_LOCKED					0x9B
#define AL_UNLOCK_FAIL_BY_FORCED_LOCKED	0x9C
#define AL_DOOR_POSITION_SENSOR			0x2B
#define AL_DAILY_SCH_SET					0x60
#define AL_DAILY_SCH_ENABLE_DISABLE		0x61
#define AL_YEARDAY_SCH_SET					0x62
#define AL_YEARDAY_SCH_ENABLE_DISABLE		0x63
#define AL_ALL_SCH_TYPE_SET					0x64
#define AL_ALL_SCH_TYPE_ENABLE_DISABLE		0x65
#define AL_MOBILE_ACCESS					0xB0
#define AL_MOBILE_USER_DB_UPDATE			0xB1
#define AL_MOBILE_CONFIG_DB_UPDATE			0xB2

#define AL_DOOR_OPEN_CLOSE_STATE			0x23

/********************* Parameter Defines *********************/

#define PA_SILENT_MODE_ONOFF			1
#define PA_AUTO_RELOCK_ONOFF			2
#define PA_RELOCK_TIME					3
#define PA_WRONG_CODE_ENTRY_LIMIT		4
#define PA_LANGUAGE						5
#define PA_SHUT_DOWN_TIME				7
#define PA_OPERATING_MODE				8
#define PA_ONE_TOUCH_LOCKING			11
#define PA_PRIVACY_BUTTON				12
#define PA_LOCK_STATUS_LED				13
#define PA_RESET_TO_FACTORY_DEFAULT	15
#define PA_ESCAPE_RETURN_MODE			16
#define PA_PASSGE_MODE					17
#define PA_DOOR_PROPPED_TIMER			18
#define PA_DPS_ALARMS					19
#define PA_DOUBLE_CHECK_UNLOCK_LOCK	25
#define PA_EMERGENCY_UNLOCK			26

#define PA_AUTOLOCK_AUTORELOCK_TIME	101



/* PA_LANGUAGE 의 PA 값 향후 N-Protocol 에서 아래와 같이 사용 하기로 함 */ 
typedef enum
{
	LANGUAGE_KOREAN = 0x01,
	LANGUAGE_ENGLISH,
	LANGUAGE_CHINESE,
	LANGUAGE_PORTUGUESE,
	LANGUAGE_SPANISH,
	LANGUAGE_TAIWANESE,
	LANGUAGE_TURKISH,
	LANGUAGE_RUSSIAN,
	LANGUAGE_FRANCE,
	LANGUAGE_JAPANESE,
	LANGUAGE_HINDI,	
	LANGUAGE_MAX = 0xFF
} Language_Set;

/********************* NACK *********************/
#define NACK_UNKNOWN_MESSAGE			0x00
#define NACK_NOT_SUPPORTED_MESSAGE	0x01
#define NACK_INVAILD_PARAMETER			0x02



// Global N-protocol 문서 참조
#define USER_STATUS_AVAILABLE			0x00
#define USER_STATUS_OCC_ENABLED		0x01
#define USER_STATUS_OCC_DISABLED		0x03
#define USER_STATUS_NON_ACCESS_USER	0x04


#define SCHEDULE_STATUS_ENABLE			0xFF
#define SCHEDULE_STATUS_DISABLE			0x00

#if 0
//==============================================================================//
//	EVENT
//==============================================================================//
#define EV_DOOR_OPEN                    0x01
#define EV_DOOR_CLOSE                   0x02
#define EV_DOOR_CLOSE_MAG_ERR           0x03
#define EV_DOOR_OPEN_ERR                0x04
#define EV_DOOR_CLOSE_ERR               0x05
#define EV_ALARM_OCC                    0x06
#define EV_ALARM_REL                    0x07
#define EV_OUTER_FORCED_LOCK_SET        0x0A
#define EV_OUTER_FORCED_LOCK_REL        0x0B
#define EV_AUTO_LOCK_SET                0x0C
#define EV_AUTO_LOCK_REL                0x0D
#define EV_DOOR_STS_CHK                 0x0E
#define EV_DOOR_STS_REP                 0x0F
#define EV_EXT_FORCED_LOCK_STS_CHK      0x10
#define EV_EXT_FORCED_LOCK_STS_REP      0x11
#define EV_ALARM_STS_CHK                0x12
#define EV_ALARM_STS_REP                0x13
#define EV_TIME_SET                     0x14
#define EV_3_MIN_LOCK_SET               0x15
#define EV_3_MIN_LOCK_REL               0x16
#define EV_RESET                        0x17
#define EV_LOOP_TEST                    0x18
#define EV_LOOP_TEST_FAIL               0x19
#define EV_NO_KEY_CHK                   0x1A
#define EV_NO_KEY_REP                   0x1B
#define EV_COM_ACK_RX_FAIL              0x1C
#define EV_AUTO_LOCK_STS_CHK            0x1D
#define EV_AUTO_LOCK_STS_REP            0x1E
#define EV_LOW_BAT                      0x1F
#define EV_PART_REGISTRATION            0x20
#define EV_PART_REMOVE                  0x21


#define EV_ARM_GO_OUT_SET               0x24	//외출경비설정
#define EV_ARM_GO_OUT_REL               0x25	//경비해제

#define EV_ALL_CHK                      0x34	//전체 조회 (HA)
#define EV_ALL_REP                      0x35	//전체 보고 (HA)
#define EV_PASSWORD_REG                 0x36	//비밀번호 등록 (HA)
#define EV_PASSWORD_SEND_REQ            0x37	//비밀번호 전송요청 (HA)
#define EV_ID_DOWNLOAD                  0x38	//Log-On-Data Download 실행
#define EV_VERIFY_MODE_SET              0x39
#define EV_IDENTIFY_MODE_SET            0x3A
#define	EV_TRACE_SEND_END		0x3B	//TRACE DATA 전송 종료
#define	EV_DDLALLSTS_CHK		0x3C	//도어록 전체 상태 조회
#define EV_DDLALLSTS_SET		0x3D	//도어록 전체 상태 설정
#define	EV_SENDSTEP_SET			0x3E	//전송 단계 설정
#define	EV_VERSION_CHK			0x3F	//도어록 프로그램 버전 조회
#define	EV_VISITOR_ENTER		0x40	//방문자 출입 발생
#define	EV_DEVICE_CHK			0x44	//장치 확인
#define	EV_DEV_PART_REG			0x45	//통신 모듈에 의한 개별 등록
#define	EV_DEV_PART_DEL			0x46	//통신 모듈에 의한 개별 삭제
#define	EV_DEV_ALL_DEL			0x47	//통신 모듈에 의한 전체 삭제
#define EV_ID_PASSWORD_REQ		0x48	//통신 모듈에 의한 조회
#define EV_TIME_REQ			0x49	//시간 조회
#define EV_REGDEV_IN_FULL		0x4A	//등록할 장치 입력이 다 되었을 때 
#define	EV_DDLINFO_REQ			0x4B	//도어록 생산 정보 조회
#define	EV_DDLINFO_SET			0x4C	//도어록 생산 정보 설정
#define EV_DDLTEST_START		0x4D	//도어록 세트 검사 시작
#define	EV_DDLTEST_END			0x4E	//도어록 세트 검사 완료
//==============================================================================//


//==============================================================================//
//	EVENT SOURCE (Old iRevo Protocol)
//==============================================================================//
#define ES_IBUTTON                      0x01    
#define ES_SMART_CARD                   0x02
#define ES_FINGER_PRINT                 0x03
#define ES_PASSWORD                     0x04
#define ES_REMOCON                      0x05
#define ES_EMER_KEY                     0x06
#define ES_OPEN_CLOSE_BUTTON            0x07
#define ES_LINKING_DEVICE               0x08
#define ES_PERIPHERAL_DEVICE            0x09
#define ES_COVER_SWITCH                 0x0A
#define ES_AUTO_LOCK                    0x0B
#define ES_INNER_FORCED_LOCK            0x0C
#define ES_OUTER_FORCED_LOCK            0x0D
#define ES_DEAD_BOLT_ZAMMED             0x0E
#define ES_INTRUSION                    0x0F
#define ES_BROKEN                       0x10
#define ES_FIRE                         0x11
#define ES_USER                         0x12
#define ES_DDL                          0x13
#define ES_MANAGE_NUMBER                0x14    //추가 (관리 번호)
#define ES_PIR_SENSOR                   0x15    // 인체감지센서
#define ES_WINDOW_SENSOR                0x16    // 도어창문센서
#define ES_SAFE_SENSOR                  0x17    // 금고센서
#define ES_FIRE_SENSOR                  0x18    // 화재센서
#define ES_GAS_SENSOR                   0x19    // 가스센서
#define ES_GAS_CONTROL                  0x1A    // 가스차단기
#define ES_EMER_REMOCON                 0x1B    // 비상리모콘


#define ES_JIG				0x1E	//생산 지그
//==============================================================================//추가
#define PACK_ID_ERROR			0xFB	//TRX PACK 의 ID가 맞지 않을 경우
#define	MECHANICAL_OPEN			0xFC	//기구적으로 강제로 문을 열었을 경우(수동 레버 포함)
#define	FLOATING_ID_LOST		0xFD	//iButton UID는 일치하나 Floating ID 일치하지 않음
#define	NONE_SUB_DATA			0xFE	//Event Sub Data 없음
#define	TX_ALLID_SEND			0xFF	//전송할 연동 장치 모두
#define	NONE_ADD_DATA			0x00	//전송할 추가 Data 없음	

#define	TEST_FRAME_STX		0x02
#define	TEST_FRAME_ETX		0x03


#define	TEST_CLASS_MAFA_JIG		0x00	// 생산 지그 동작
#define	TEST_JIG_GET_VERSION		0x00	// 프로그램 버전 확인 2byte
#define	TEST_JIG_SET_SERIAL		0x01	// 시리얼번호 설정 16byte
#define	TEST_JIG_GET_SERIAL		0x02	// 시리얼번호 가져오기  16byte
#define	TEST_JIG_SET_INITIAL		0x03	// 초기화 셋팅
#define	TEST_JIG_GET_INITIAL		0x04	// 초기화 확인 
#define	TEST_JIG_SET_OUTPUT		0x05	// 출력 요청 
#define 	TEST_JIG_SET_MASTER		0x06	// 마스터 설정  
#define 	TEST_JIG_GET_MASTER		0x07	// 마스터 확인  
#define 	TEST_JIG_SET_SLEEP		0x0C	// Sleep 진입
#define 	TEST_JIG_REQ_LOOPTEST		0x0D	// looptest 요청  

#define	TEST_JIG_SEND_VERSION		0x80	// 프로그램 버전 확인 
#define	TEST_JIG_SEND_SERIAL		0x81	// 시리얼 넘버 확인 
#define	TEST_JIG_SEND_INITAIL		0x82	// 초기값 확인 
#define	TEST_JIG_SEND_INPUT		0x83	// 입력 확인
#define	TEST_JIG_REQ_TEST_PRCS	0x84	// 검사 공정 테스트 요
#define 	TEST_JIG_ACK_LOOPTEST		0x88	// LoopTest ACK 

#define	SETTEST_EEPROM		0x01
#define	SETTEST_MOTOR_SENSOR	0x02
#define	SETTEST_POLLING		0x03
#define	SETTEST_KEY_1		0x04
#define	SETTEST_KEY_2		0x05
#define	SETTEST_KEY_3		0x06
#define	SETTEST_KEY_4		0x07
#define	SETTEST_KEY_5		0x08
#define	SETTEST_KEY_6		0x09
#define	SETTEST_KEY_7		0x0A
#define	SETTEST_KEY_8		0x0B
#define	SETTEST_KEY_9		0x0C
#define	SETTEST_KEY_0		0x0D
#define	SETTEST_KEY_SHARP	0x0E
#define	SETTEST_KEY_STAR	0x0F
#define	SETTEST_KEY_REG	0x10
#define	SETTEST_KEY_MENU	0x11	
#define	SETTEST_KEY_CLS	0x12
#define	SETTEST_KEY_FIRE	0x13
#define	SETTEST_DOOR_SW	0x14
#define	SETTEST_MOTOR		0x15
#define	SETTEST_LOW_BAT	0x16
#define	SETTEST_COVER_SW	0x17
#define	SETTEST_CARD_IN	0x18
#define	SETTEST_LED		0x19
#define	SETTEST_BUZZER	0x1A
#define	SETTEST_VOICE		0x1B
#define	SETTEST_STOP		0x1C

#endif


#endif
