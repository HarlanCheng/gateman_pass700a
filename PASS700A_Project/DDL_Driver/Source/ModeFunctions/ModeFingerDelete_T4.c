#define		_MODE_FINGER_DELETE_C_

#include "main.h"

extern	BYTE		no_fingers;
extern	BYTE 	FingerReadBuff[4];
extern	BYTE 	FingerWriteBuff[4];

#define	 _MODE_TIME_OUT_5S			50

void	ModeGotoNormalAllFingerDelete( void )//노멀 All
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 0;
#ifdef	BLE_N_SUPPORT
	gbEnterMode = 0x00;
#endif 
}


void	ModeGotoAdvancedAllFingerDelete1( void ) //노멀 -> 어드벤스 All  , 어드벤스 -> 노멀 all
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
#ifdef	DDL_CFG_DIMMER	
	gbModePrcsStep = 1;			// 2; //이 함수를 부르는 함수에서 gbModeProcsStep을 하나 증가 시키므로 여기서는 원래 가고자 하는 값보다 1 작게 해야 한다.
#else 
	gbModePrcsStep = 2; 		
#endif 
}


#ifdef	BLE_N_SUPPORT
void	ModeGotoAllFingerDeleteByBleN( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 2;
	gbEnterMode = 0x01;	
}
#endif 

void	ModeGotoAdvancedAllFingerDelete2( void ) //어드벤스 all
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 2;
}


void	ModeAllFingerDelete( void )
{
	BYTE	bTmp;

	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_PIN_SHARP, VOL_CHECK);		//비밀번호를 입력하세요. 계속하시려면 샵 버턴을 누르세요.
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_20S);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
					switch(PincodeVerify(0))
					{
						case 1:
							gbModePrcsStep++;
							break;

						case RET_NO_INPUT:
						case RET_WRONG_DIGIT_INPUT:
							FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
							ModeClear();
							break;

						default:
							gbModePrcsStep=20;
					}
					break;		

				case TENKEY_STAR:
					if(GetSupportCredentialCount() > 1)
					{
						ReInputOrModeMove(ModeGotoMenuCredentialSelect);
					}
					else 
					{
						ReInputOrModeMove(ModeGotoMenuMainSelect);
					}
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					BioOffModeClear();
					break;
			}	
			break;

		case 2:
			BioModuleON();
			SetModeTimeOut(10);
			gbModePrcsStep = 3;
			break;
			
		case 3:
			if(GetModeTimeOut() == 0)
			{
				/* ATR 이 1s 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep	= 100;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
		
				// Device status check 
				if(gbFingerPrint_rxd_buf[12] != 0x00)
				{
					FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					gbModePrcsStep	= 100;
					break;
				}
				//Get ALT 
				gbfingerALT = gbFingerPrint_rxd_buf[11];
				gbModePrcsStep = 4;
				SetModeTimeOut(gbfingerALT);
				NFPMTxEventDelete(0x00);
			}
			break;

		case 4: 		
			if(GetModeTimeOut() == 0)
			{
				/* ALT 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep = 100;
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[3] != 0x00)
				{
					FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					gbModePrcsStep = 100;
					break;
				}

				no_fingers = 0;
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				

				FingerWriteBuff[0] = FingerWriteBuff[1] = 0xFF;

				for(bTmp = 0; bTmp< _MAX_REG_BIO; bTmp++){		
#ifdef	DDL_CFG_WATCHDOG
					RefreshIwdg();
#endif
					RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*bTmp), 2);
				}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				ScheduleDeleteOfAllUserFingerprint();
#endif

				DDLStatusFlagClear(DDL_STS_FAIL_BIO);

				NFPMTxEventStop(0x00);
				SetModeTimeOut(gbfingerALT);
				gbModePrcsStep = 5;
			}
			break;

		case 5:
			if(GetModeTimeOut() == 0)
			{
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep	= 100;
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				BioModuleOff();

#ifdef	BLE_N_SUPPORT
				if(gbEnterMode)
				{
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					ModeClear();
					PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, 0xFF);
					gbEnterMode = 0x00;
					break;
				}
#endif 				
				if( PowerDownModeForJig() == STATUS_SUCCESS ) 
				{ 
					FeedbackModeCompleted(AVML_DEL_ALL_FINGER_DONE_MSG, VOL_CHECK);
					gbModePrcsStep = 100;
				}
				else 
				{
					if ( gbModeChangeFlag4Finger == 1 ) {		//모드 전환을 위해 전체 지문을 지운 것이라면 pincode 디스플레이를 한다.
						gbModePrcsStep = 6;
						gbModeChangeFlag4Finger= 0;
						break;
					}
					else if ( gbModeChangeFlag4Finger == 2 ) {
						gbModePrcsStep = 7;
						gbModeChangeFlag4Finger= 0;
#ifdef	DDL_CFG_DIMMER
						SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
						FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
#else					
#ifdef	DDL_CFG_LED_TYPE8
//디밍이 없는 지문 제품의 경우 다음 스텝에서 LED 프로세스의 구동 시간이 짧아 타임아웃 에러가 발생 이를 막기 위함 , 
//현재 디밍 없는 지문 제품은 PANPAN만 해당 되기 때문에 __DDL_MODEL_PANPAN_FC2A을 선언 하여 처리.
						SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
						FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
#else	//__DDL_MODEL_PANPAN_FC2A #else
						SetModeTimeOut(1);
#endif	//__DDL_MODEL_PANPAN_FC2A #endif

#endif
						
						break;
					}
					else if(FactoryResetRunCheck() == STATUS_SUCCESS) 
					{
						gbModePrcsStep = 100;
					}
					else if ( gbModeChangeFlag4Finger == 3 ) {
						gbModeChangeFlag4Finger= 0;
						ModeGotoAllDataReset(0xFF);
						gbModePrcsStep = 4;
						break;
					}
					else 
					{
						FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK); 		//	completed, Press the star for setting other options or press the "R" button to finish
						gbModePrcsStep = 8;
						SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					}
					PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, 0xFF);
				}
			}
		break;

		case 6:
			DisplayRegisteredPincode(PincodeRegisterCompleteProcess);
		break;

		case 7:
			GotoPreviousOrComplete(ModeGotoMenuMainSelect);
		break;

		case 8:
			if(GetSupportCredentialCount() > 1)
			{
				GotoPreviousOrComplete(ModeGotoMenuCredentialSelect);
			}
			else 
			{
				GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			}			
			break;

		case 20:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			BioOffModeClear();
			break;

		case 100:
			NFPMTxEventStop(0x00);
			SetModeTimeOut(gbfingerALT);
			gbModePrcsStep = 101;
			break;

		case 101:
			if(GetModeTimeOut() == 0)
			{
				BioOffModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				// Rx 만 받으면 그냥 off 
				gbFingerPrint_rxd_end = 0;
				BioOffModeClear();	
				break;
			}
		break;

		default:
			gbModePrcsStep = 100;
			break;
	}
}


