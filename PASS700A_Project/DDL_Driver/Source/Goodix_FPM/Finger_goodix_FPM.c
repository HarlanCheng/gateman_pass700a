//------------------------------------------------------------------------------
/** 	@file		Finger_iRevo_NFPM.c
	@brief	uart 통신 protocol 관련 파일 
*/
//------------------------------------------------------------------------------


#define		_FINGER_IREVO_NFPM_C_

#include	"main.h"

FLAG BioFlag;

/* gbBioTimer10ms 다른 지문 모듈과 같은 방식으로 gbBioTimer10ms 제어 하지 말것 */
WORD gbBioTimer10ms=0;

static BYTE gbFingerPrint_txd_end;
static BYTE FingerPrint_txding_buf[40];

BYTE gbFingerPrint_rxd_end;
static BYTE gbFingerPrint_rxding_buf[40] ;
BYTE gbFingerPrint_rxd_buf[40];

BYTE gbfingerALT;
BYTE gbfingerMaxTemplateNumber;


BYTE gbCoverAccessDelaytime100ms = 0x00;
BYTE gbFingerLongTimeWait100ms = 0;
BYTE gbFID=0;
BYTE gbFingerRegisterDisplayStep;
BYTE dbDisplayOn = 0x00;

#ifdef	BLE_N_SUPPORT
BYTE gbEnterMode = 0x00;
#endif 


static BYTE gbNFPMRxAddDataLen = 0;
static BYTE gbNFPMRxChecksum = 0;
static WORD gbNFPMRxByteCnt = 0;
static uint8_t		gbnfpm_rx_byte;

// UART 
void		Register_NFPM_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) )
{
	if ( uart_rxcplt_callback != NULL )
		register_uart_rxcplt_callback( gh_mcu_uart_finger, uart_rxcplt_callback );
	if ( uart_txcplt_callback != NULL )
		register_uart_txcplt_callback( gh_mcu_uart_finger, uart_txcplt_callback );
}

void NFPMRepairUart(void)
{
	HAL_UART_DeInit(gh_mcu_uart_finger);
	Delay(SYS_TIMER_10MS);
	gh_mcu_uart_finger->Instance = USART3;		
	gh_mcu_uart_finger->Init.BaudRate = 19200;
	gh_mcu_uart_finger->Init.WordLength = UART_WORDLENGTH_8B;
	gh_mcu_uart_finger->Init.StopBits = UART_STOPBITS_1;
	gh_mcu_uart_finger->Init.Parity = UART_PARITY_NONE;
	gh_mcu_uart_finger->Init.Mode = UART_MODE_TX_RX;
	gh_mcu_uart_finger->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	gh_mcu_uart_finger->Init.OverSampling = UART_OVERSAMPLING_16;	
	HAL_UART_Init( gh_mcu_uart_finger );
	InitialSetupFingerModule();
}

//RX
void NFPM_uart_rxcplt_callback(UART_HandleTypeDef *huart)
{
	gbFingerPrint_rxding_buf[gbNFPMRxByteCnt] = gbnfpm_rx_byte;
	if(gbNFPMRxByteCnt >= (gbNFPMRxAddDataLen + 2)) // STX + LENGTH 2byte 
	{
		gbNFPMRxByteCnt++;

		if(gbFingerPrint_rxding_buf[gbNFPMRxAddDataLen + 2] == gbNFPMRxChecksum)	// 체크섬이 맞으면 
		{
			memcpy(gbFingerPrint_rxd_buf,gbFingerPrint_rxding_buf,gbNFPMRxByteCnt);
		}
		NFPMRxDataClear();
		gbFingerPrint_rxd_end = 0x01;
	}
	else
	{	
		if(gbNFPMRxByteCnt == 0x00 && gbnfpm_rx_byte != NFPM_STX)
		{
			/* 첫번째 data 가 STX 아니면 skip */
			NFPMRxDataClear();
		}
		else
		{
			gbFingerPrint_rxd_end = 0x00;
			gbNFPMRxChecksum ^= gbFingerPrint_rxding_buf[gbNFPMRxByteCnt];
			gbNFPMRxByteCnt++;
			if(gbNFPMRxByteCnt == NFPM_EVENT_INDEX)
			{
				gbNFPMRxAddDataLen = gbFingerPrint_rxding_buf[NFPM_LENGTH_INDEX];
			}
		}
	}
	HAL_UART_Receive_IT( gh_mcu_uart_finger, &gbnfpm_rx_byte, sizeof(gbnfpm_rx_byte));
}

// TX
void NFPM_uart_txcplt_callback(UART_HandleTypeDef *huart)
{
	gbBioTimer10ms = 0;
	gbFingerPrint_txd_end = 1;
}

void	InitialSetupFingerModule( void )
{
	Register_NFPM_INT_Callback( NFPM_uart_txcplt_callback, NFPM_uart_rxcplt_callback);
	HAL_UART_Receive_IT( gh_mcu_uart_finger, &gbnfpm_rx_byte, sizeof(gbnfpm_rx_byte) );
}


BYTE GetNoFingerFromEeprom( void )
{
	BYTE	no_eeprom_key;

	RomRead(&no_eeprom_key, (WORD)KEY_NUM, 1);					// 현재 등록된 지문수 불러오기..
	return	no_eeprom_key;
}

void NFPMRxDataClear(void)
{
	gbNFPMRxByteCnt = 0;
	gbNFPMRxAddDataLen = 0;
	gbNFPMRxChecksum = 0;
}

void FingerPrintModuleCheck(void)
{
	BYTE bFPMTempArray[8];
	BYTE bTemp1[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	BYTE bTemp2[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	
	// 전원control 
	BioModuleOff();
	Delay(SYS_TIMER_15MS);
	BioModuleON();

	// wait ATR 1s 
	gbBioTimer10ms = 100;
	gbFingerPrint_rxd_end = 0;
        
	while (!gbFingerPrint_rxd_end)
	{
		Delay(SYS_TIMER_10MS);
		gbBioTimer10ms--;
		if(gbBioTimer10ms == 0x00)
		{
			gfBioErr = 0x01;
			return;
		}
	}
	gbBioTimer10ms = 0;	
	gbFingerPrint_rxd_end = 0;

	// Device status check 
	if(gbFingerPrint_rxd_buf[12] != 0x00)
	{
		gfBioErr = 0x01;
		return;
	}

	// Device UID check 
	RomRead(bFPMTempArray,HFPM_MODULE_ID,8);

	// 최소 ROM 에 UID 가 없으면 저장 UID는 암호화 하지 않는다 
	if(memcmp(bFPMTempArray,bTemp1,8) == 0x00 || memcmp(bFPMTempArray,bTemp2,8) == 0x00)
	{
		RomWrite(&gbFingerPrint_rxd_buf[3],HFPM_MODULE_ID,8);
	}

	gbfingerALT = gbFingerPrint_rxd_buf[11];
	
	gbBioTimer10ms = 0x00;
	gfBioErr = 0x00;
}

void FingerPrintTxDataSend(BYTE Length)
{
	NFPMRxDataClear();	
	gbFingerPrint_txd_end = 0;
	gbBioTimer10ms = 5;
	HAL_UART_Transmit_IT(gh_mcu_uart_finger,FingerPrint_txding_buf,Length);
}

void NFPMTxEventEnroll(BYTE SlotNumber)
{
	BYTE Index = 0x00;
	
	FingerPrint_txding_buf[Index++] = NFPM_STX;
	FingerPrint_txding_buf[Index++] = 0x02; 
	FingerPrint_txding_buf[Index++] = NFPM_EVNET_ENROLL;
	FingerPrint_txding_buf[Index++] = SlotNumber;	// 0x00 순차 , 0x01 ~ Max number 
	FingerPrint_txding_buf[Index] = Get_Checksum_BCC(FingerPrint_txding_buf,Index);
	Index++;
	FingerPrintTxDataSend(Index);
}

void NFPMTxEventEnrollAlarm(BYTE TemplateNumber)
{
	BYTE Index = 0x00;
	
	FingerPrint_txding_buf[Index++] = NFPM_STX;
	FingerPrint_txding_buf[Index++] = 0x02; 
	FingerPrint_txding_buf[Index++] = NFPM_EVNET_ENROLL_ALARM;
	FingerPrint_txding_buf[Index++] = TemplateNumber;	
	FingerPrint_txding_buf[Index] = Get_Checksum_BCC(FingerPrint_txding_buf,Index);
	Index++;
	FingerPrintTxDataSend(Index);
}

void NFPMTxEventVerify(BYTE Parameter)
{
	BYTE Index = 0x00;

	FingerPrint_txding_buf[Index++] = NFPM_STX;
	FingerPrint_txding_buf[Index++] = 0x02; 
	FingerPrint_txding_buf[Index++] = NFPM_EVNET_VERIFY;
	FingerPrint_txding_buf[Index++] = Parameter;	//TBD 0x00 넣는다 
	FingerPrint_txding_buf[Index] = Get_Checksum_BCC(FingerPrint_txding_buf,Index);
	Index++;
	FingerPrintTxDataSend(Index);
}

void NFPMTxEventDelete(BYTE SlotNumber)
{
	BYTE Index = 0x00;

	FingerPrint_txding_buf[Index++] = NFPM_STX;
	FingerPrint_txding_buf[Index++] = 0x02; 
	FingerPrint_txding_buf[Index++] = NFPM_EVNET_DELETE;
	FingerPrint_txding_buf[Index++] = SlotNumber;	//0x00 delete all , 0x01 ~ slotnumber
	FingerPrint_txding_buf[Index] = Get_Checksum_BCC(FingerPrint_txding_buf,Index);
	Index++;
	FingerPrintTxDataSend(Index);
}

void NFPMTxEventSlotInfo(BYTE Parameter)
{
	BYTE Index = 0x00;
	Parameter = 0x00; // TBD
      
	FingerPrint_txding_buf[Index++] = NFPM_STX;
	FingerPrint_txding_buf[Index++] = 0x02; 
	FingerPrint_txding_buf[Index++] = NFPM_EVNET_GET_LIST;
	FingerPrint_txding_buf[Index++] = Parameter;
	FingerPrint_txding_buf[Index] = Get_Checksum_BCC(FingerPrint_txding_buf,Index);
	Index++;
	FingerPrintTxDataSend(Index);
}

void NFPMTxEventStop(BYTE Parameter)
{
	BYTE Index = 0x00;
        Parameter = 0x00; // TBD

	FingerPrint_txding_buf[Index++] = NFPM_STX;
	FingerPrint_txding_buf[Index++] = 0x02; 
	FingerPrint_txding_buf[Index++] = NFPM_EVNET_STOP_MODULE;
	FingerPrint_txding_buf[Index++] = Parameter;
	FingerPrint_txding_buf[Index] = Get_Checksum_BCC(FingerPrint_txding_buf,Index);
	Index++;
	FingerPrintTxDataSend(Index);
}

void NFPMBioTimerCallback(void)
{
	/* Tx 발생 시키기 전에 gbBioTimer10ms = 5 , gbFingerPrint_txd_end = 0 */
	/* Tx complete callback 에서 gbBioTimer10ms = 0 , gbFingerPrint_txd_end = 1 */
	/* Tx 가 complete 되지 않으면 uart 재 설정 */
	/* gbBioTimer10ms 다른 지문 모듈과 같은 방식으로 gbBioTimer10ms 제어 하지 말것 */
	if(gbBioTimer10ms)
	{
		--gbBioTimer10ms;
		if(gbBioTimer10ms == 0 && !gbFingerPrint_txd_end)
		{
			NFPMRepairUart();
			FeedbackError(AVML_MALFUNCTION_FPM_MSG, VOL_CHECK);
			BioOffModeClear();
		}
	}
}

void BioCommCheck(BYTE bsave)
{
	FingerPrintModuleCheck();
	BioModuleOff();
}

void FPCoverSwProcess(void)
{
	if(!gfXORCoverSw)	return; 					//Cover의 신호가 바뀌어야만 루틴 수행

	gfXORCoverSw = 0;

	if(gbCoverAccessDelaytime100ms) return;
	if(GetVoicePrcsStep() && !gfIntrusionAlarm) return; //경보 중일 경우는 열수 있게 
	if(GetTamperProofPrcsStep()) return;
	if ( gfBrokenAlarm )	return;

	if(GetMainMode() == 0)
	{		
		if(gfCoverSw)
		{
			if(GetMotorPrcsStep())		return;
			ModeGotoFingerVerify();
		}
	}
	else
	{
		if(GetMainMode() == MODE_PIN_VERIFY && gfMute == 1 && gPinInputKeyCnt == 0)
		{
			ModeGotoFingerVerify();
		}
	}
}

void BioModuleON( void )
{
	NFPMRxDataClear();
	gbFingerPrint_rxd_end = 0x00;
	P_FP_EN(1);
}

void BioModuleOff( void )
{
	P_FP_EN(0);
	NFPMRxDataClear();
	gbFingerPrint_rxd_end = 0x00;
	return ;
}

void BioOffModeClear( void )
{
	BioModuleOff();
	ModeClear();
	gbModeChangeFlag4Finger = 0;
}

void FingerPrintRegisterDisplay(void)
{
	static WORD wLedswitch = 0x0000;
	
	if(gbFingerLongTimeWait100ms >= 5)	
	{
		if(dbDisplayOn == 0x00)
		{
			// Maybe .... LED_1 ~ LED_8 
			for(BYTE i = 0 ; i < gbfingerMaxTemplateNumber ; i++)
			{
				wLedswitch |= (1<<(i+1)); // LED_KEY_1
			}
			
			//LedGenerate(wLedswitch, BLINK_ON, 0);
#if !defined (DDL_CFG_NO_DIMMER)	//디밍을 사용하지 않는 지문 모델을 위해 sjc
			LedGenerate(wLedswitch, BLINK_ON, 0);
#endif
			dbDisplayOn = 0x01;
		}
	}
	else if(gbFingerLongTimeWait100ms < 5)
	{ 
		if(dbDisplayOn == 0x01)
		{
			// off 될 LED 제거 
			wLedswitch &= ~(1<<(1+gbFingerRegisterDisplayStep)); 

			//LedGenerate(wLedswitch, BLINK_OFF, 0);
#if !defined (DDL_CFG_NO_DIMMER)	//디밍을 사용하지 않는 지문 모델을 위해 sjc
			LedGenerate(wLedswitch, BLINK_OFF, 0);
#endif

			dbDisplayOn = 0x00;
		}

		if(gbFingerLongTimeWait100ms == 0) 
			gbFingerLongTimeWait100ms = 10;
	}
}

