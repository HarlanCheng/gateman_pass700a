//------------------------------------------------------------------------------
/** 	@file	PFM_3000_UART.c
	@brief	UART functions for PFM-3000-UART communication
	@brief	20161020 시점에서 필리아 PFM-3000 지문 모듈과 / 중국 Biosec TS1072M 모듈의 
	@brief	공용 들라이버 로 두 모듈이 공동으로 사용 한다. 
*/
//------------------------------------------------------------------------------

#define		_PFM_3000_UART_C_

#include	"main.h"


//UART 이상시 재설도 하기 때문에 초기화 코드를 별도로 마련해 둔다.
// 대기 전류를 줄이기 위해 STOP mode가 될때 Tx/Rx pin을 analog로 만들기에 이를 다시 UART용으로 설정하는 코드이다.
void		SetupPFM_3000_UART( void )
{
	GPIO_InitTypeDef GPIO_InitStruct;

	//지문모듈과의 통신핀 Tx pin을 gpio로 설정 사용한 뒤 다시 uart로 사용하려면 해당 pin의 설정에서 Alternate function 기능을 활성화 해야한다.
	GPIO_InitStruct.Pin = FP_DDL_TX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;				// Select Alternate function
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART3;			// Select Alternate function
	HAL_GPIO_Init(FP_DDL_TX_GPIO_Port, &GPIO_InitStruct);

	//지문모듈과의 통신핀 Rx pin을 gpio로 설정 사용한 뒤 다시 uart로 사용하려면 해당 pin의 설정에서 Alternate function 기능을 활성화 해야한다.
	GPIO_InitStruct.Pin = FP_DDL_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;				// Select Alternate function
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART3;			// Select Alternate function
	HAL_GPIO_Init(FP_DDL_RX_GPIO_Port, &GPIO_InitStruct);

	gh_mcu_uart_finger->Instance = MCU_PFM_3000_UART;		
	gh_mcu_uart_finger->Init.BaudRate = 9600;
	gh_mcu_uart_finger->Init.WordLength = UART_WORDLENGTH_8B;
	gh_mcu_uart_finger->Init.StopBits = UART_STOPBITS_1;
	gh_mcu_uart_finger->Init.Parity = UART_PARITY_NONE;
	gh_mcu_uart_finger->Init.Mode = UART_MODE_TX_RX;
	gh_mcu_uart_finger->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	gh_mcu_uart_finger->Init.OverSampling = UART_OVERSAMPLING_16;
	HAL_UART_Init( gh_mcu_uart_finger );
}

void FingerModuleUartPinSetOutPutLow(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = FP_DDL_TX_Pin | FP_DDL_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(FP_DDL_TX_GPIO_Port, &GPIO_InitStruct);

	P_FP_DDL_TX(0);
	//P_FP_DDL_RX(0);	
}

void FingerModuleUartPinSetInput(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = FP_DDL_TX_Pin | FP_DDL_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(FP_DDL_TX_GPIO_Port, &GPIO_InitStruct);
}


// 대기 전류를 줄이기 위해 PFM-3000와의 통신 pin을 analog로 바꾼다.
void OffGpioPFM_3000(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = FP_DDL_TX_Pin | FP_DDL_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(FP_DDL_TX_GPIO_Port, &GPIO_InitStruct);
}



// UART callback 함수를 MX_uart_lapper에 등록한다.
void		RegisterPFM_3000_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) )
{
	if ( uart_rxcplt_callback != NULL )
		register_uart_rxcplt_callback( gh_mcu_uart_finger, uart_rxcplt_callback );
	if ( uart_txcplt_callback != NULL )
		register_uart_txcplt_callback( gh_mcu_uart_finger, uart_txcplt_callback );
}


void		UartSendPFM_3000( BYTE data )
{
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &data, sizeof(data) );
}


