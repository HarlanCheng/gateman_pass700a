#define		_MODE_FINGER_DELETE_C_

#include "main.h"


/***
void	ModeGotoFingerDelete( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 0;
}
***/


extern	BYTE	no_fingers;
extern	BYTE 	FingerReadBuff[4];
extern	BYTE 	FingerWriteBuff[4];


void	ModeGotoNormalAllFingerDelete( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 0;
}

void	ModeGotoAdvancedAllFingerDelete1( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 1;			// 2; //이 함수를 부르는 함수에서 gbModeProcsStep을 하나 증가 시키므로 여기서는 원래 가고자 하는 값보다 1 작게 해야 한다.
}

void	ModeGotoAdvancedAllFingerDelete2( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 2;
}


void	ModeAllFingerDelete( void )
{
	BYTE	bTmp;
	const BYTE bResult[4] = {0x00,0x00,0x07,0x12};

	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(271, VOL_CHECK);		//비밀번호를 입력하세요. 계속하시려면 샵 버턴을 누르세요.
			FeedbackKeyPadLedOn(AVML_IN_PIN_SHARP, VOL_CHECK);		//비밀번호를 입력하세요. 계속하시려면 샵 버턴을 누르세요.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_20S);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
					switch(PincodeVerify(0))
					{
						case 1:
							gbModePrcsStep++;
							break;

						case RET_NO_INPUT:
						case RET_WRONG_DIGIT_INPUT:
//							FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
							FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
							ModeClear();
							break;

						default:
							gbModePrcsStep=20;
					}
					break;		

				case TENKEY_STAR:
					if(GetSupportCredentialCount() > 1)
					{
						ReInputOrModeMove(ModeGotoMenuCredentialSelect);
					}
					else 
					{
						ReInputOrModeMove(ModeGotoMenuMainSelect);
					}
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					BioOffModeClear();
					break;
			}	
			break;

		case 2:
			BioModuleON();
			gbBioTimer10ms = BIO_WAKEUP_TM; 	// 100ms
			SetModeProcessTime( 100 );			// 1sec
			gbModePrcsStep++;
			break;
		case 3:
			if(GetModeProcessTime()) break;

			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR
			gbFingerPTMode = FINGER_MODE_ALL_DELETE;
			SetModeProcessTime( 300 );			// 3sec
			ModeGotoFingerPTOpen();
			break;


		case 4: 		// ptopen 완료.. 후.. 
			if(GetBuzPrcsStep())	break;
			if(gbBioTimer10ms == 0){
				BioModuleOff();
//				AudioFeedback(118, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.
				AudioFeedback(AVML_MALFUNCTION_FPM_MSG, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.
				LedSetting(gcbLedErr, LED_DISPLAY_OFF);
				ModeClear();
				break;
			}

			pFingerRx = &FingerRx.PACKET.Start_st;
			PTDELALL_SEND();							// 모두 삭제 전송 

			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;
		
		case 5:
			/*if(gbBioTimer10ms == 0){
				BioModuleOff();
//				AudioFeedback(118, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.				
				AudioFeedback(AVML_MALFUNCTION_FPM_MSG, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.
				LedSetting(gcbLedErr, LED_DISPLAY_OFF);
				ModeClear();

				break;
			}*/

			if(gfUART0TxEnd){					// 송신 완료//
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;

		case 6:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			if(gfUART0RxEnd == 1)	{		// 수신 완료..
				gfUART0RxEnd=0;
				pFingerRx = &FingerRx.PACKET.Start_st+1;	//리셋 에크 수신 CMD 부
				if((*pFingerRx == 0x05) && (gbErrorCnt<2)){ 	// PTOPEN ACK CMD 재 수신 받을 시 
					gbModePrcsStep = 4;
					gbErrorCnt++;
					break;
				}
#if 1 // 예의 처리 변경 	
				// command tag + result code 를 전부 본다 
				// 0x1207+0000 

				pFingerRx = &FingerRx.PACKET.Start_st+7;

				if(memcmp(pFingerRx,bResult,4) != 0)
				{
					gbModePrcsStep=200;
					break;
				}
#else 
				pFingerRx = &FingerRx.PACKET.Start_st+9;
	
				if(*pFingerRx != 0x07){
					gbModePrcsStep=200;
					break;
				}
				pFingerRx = &FingerRx.PACKET.Start_st+10;

				if(*pFingerRx != 0x12){
					gbModePrcsStep=200;
					break;
				}
#endif 				
				no_fingers = 0;
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//

				FingerReadBuff[0] = FingerReadBuff[1] = 0xFF;
				for(bTmp = 0; bTmp< _MAX_REG_BIO; bTmp++){		// 모두 초기화..
					RomWrite(FingerReadBuff, (WORD)BIO_ID+(2*bTmp), 2);
				}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				ScheduleDeleteOfAllUserFingerprint();
#endif

				DDLStatusFlagClear(DDL_STS_FAIL_BIO);

				if( PowerDownModeForJig() == STATUS_SUCCESS ) {	// jigmode의 동작이면
//					FeedbackModeCompleted(134, VOL_CHECK);	//	등록된 모든 지문이 모두 삭제 되었습니다.
					FeedbackModeCompleted(AVML_DEL_ALL_FINGER_DONE_MSG, VOL_CHECK);	//	등록된 모든 지문이 모두 삭제 되었습니다.
					BioOffModeClear();
				}
				else {
					BioModuleOff();
					if ( gbModeChangeFlag4Finger == 1 ) {		//모드 전환을 위해 전체 지문을 지운 것이라면 pincode 디스플레이를 한다.
						gbModePrcsStep = 7;
						SetModeProcessTime(0);
						gbModeChangeFlag4Finger= 0;
					}else if ( gbModeChangeFlag4Finger == 2 ) {
						gbModePrcsStep = 9;
						gbModeChangeFlag4Finger= 0;
						SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
						FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
						break;
					}else if ( gbModeChangeFlag4Finger == 3 ) {
						gbModeChangeFlag4Finger= 0;
						ModeGotoAllDataReset(0xFF);
						gbModePrcsStep = 4;
						break;
					}
					else if(FactoryResetRunCheck() == STATUS_SUCCESS) // factory 중이라면  //hyojoon_20160922
					{
						ModeClear();
					}
					else 
					{
//						FeedbackModeCompletedKeepMode(39, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
						FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
						gbModePrcsStep = 8;
						break;
					}

					// 지문 전체 삭제 정보 전송
					PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, 0xFF);
					// 지문 전체 삭제 정보 전송
				}

#ifdef	_MASTERKEY_IBUTTON_SUPPORT
				MasterKeyEnabledClear();
#endif
				break;
			}
			break;

		case 7:		// display pincode after changing mode (normal <-> advanced)
			DisplayRegisteredPincode(PincodeRegisterCompleteProcess);
			//위의 함수 호출 후 mode clear가 일어난다.
			break;

		case 8:
			if(GetSupportCredentialCount() > 1)
			{
				GotoPreviousOrComplete(ModeGotoMenuCredentialSelect);
			}
			else 
			{
				GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			}				
			//위 함수호출로 mode clear가 일어날 수 있다.
			break;

		case 9:
			// 안심모드에서 5번으로 전체 삭제일 경우 
			GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			//위 함수호출로 mode clear가 일어날 수 있다.
			break;


		case 20:		//비밀번호 인증이 틀린 경우 에러처리
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			BioOffModeClear();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
			BioOffModeClear();
//			AudioFeedback(118, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.				
			AudioFeedback(AVML_MALFUNCTION_FPM_MSG, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.				
			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;
		default:
			BioOffModeClear();
			break;
	}
}





#ifdef	BLE_N_SUPPORT
void	ModeGotoAllFingerDeleteByBleN( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE_BYBLEN;
	gbModePrcsStep = 0;
}


void	ModeAllFingerDeleteByBleN( void )
{
	BYTE	bTmp;
	const BYTE bResult[4] = {0x00,0x00,0x07,0x12};

	switch(gbModePrcsStep)
	{
		case 0:
			SetModeTimeOut(_MODE_TIME_OUT_20S);

			gbModePrcsStep = 2;
			break;

		case 2:
			BioModuleON();
			gbBioTimer10ms = BIO_WAKEUP_TM; 	// 100ms
			SetModeProcessTime( 100 );			// 1sec
			gbModePrcsStep++;
			break;
		case 3:
			if(GetModeProcessTime()) break;

			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR
			gbFingerPTMode = FINGER_MODE_ALL_DELETE_BYBLEN;
			SetModeProcessTime( 300 );			// 3sec
			ModeGotoFingerPTOpen();
			break;


		case 4: 		// ptopen 완료.. 후.. 
			if(GetBuzPrcsStep())	break;
			if(gbBioTimer10ms == 0){
				BioModuleOff();
//				AudioFeedback(118, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.
				AudioFeedback(AVML_MALFUNCTION_FPM_MSG, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.
				LedSetting(gcbLedErr, LED_DISPLAY_OFF);
				ModeClear();
				break;
			}

			pFingerRx = &FingerRx.PACKET.Start_st;
			PTDELALL_SEND();							// 모두 삭제 전송 

			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;
		
		case 5:
			/*if(gbBioTimer10ms == 0){
				BioModuleOff();
//				AudioFeedback(118, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.				
				AudioFeedback(AVML_MALFUNCTION_FPM_MSG, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.
				LedSetting(gcbLedErr, LED_DISPLAY_OFF);
				ModeClear();

				break;
			}*/

			if(gfUART0TxEnd){					// 송신 완료//
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;

		case 6:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			if(gfUART0RxEnd == 1)	{		// 수신 완료..
				gfUART0RxEnd=0;
				pFingerRx = &FingerRx.PACKET.Start_st+1;	//리셋 에크 수신 CMD 부
				if((*pFingerRx == 0x05) && (gbErrorCnt<2)){ 	// PTOPEN ACK CMD 재 수신 받을 시 
					gbModePrcsStep = 4;
					gbErrorCnt++;
					break;
				}

#if 1 // 예의 처리 변경 	
				// command tag + result code 를 전부 본다 
				// 0x1207+0000 

				pFingerRx = &FingerRx.PACKET.Start_st+7;

				if(memcmp(pFingerRx,bResult,4) != 0)
				{
					gbModePrcsStep=200;
					break;
				}
#else 
				pFingerRx = &FingerRx.PACKET.Start_st+9;
	
				if(*pFingerRx != 0x07){
					gbModePrcsStep=200;
					break;
				}
				pFingerRx = &FingerRx.PACKET.Start_st+10;

				if(*pFingerRx != 0x12){
					gbModePrcsStep=200;
					break;
				}
#endif 				

				no_fingers = 0;
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//

				FingerReadBuff[0] = FingerReadBuff[1] = 0xFF;
				for(bTmp = 0; bTmp< _MAX_REG_BIO; bTmp++){		// 모두 초기화..
					RomWrite(FingerReadBuff, (WORD)BIO_ID+(2*bTmp), 2);
				}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				ScheduleDeleteOfAllUserFingerprint();
#endif

				DDLStatusFlagClear(DDL_STS_FAIL_BIO);

				if( PowerDownModeForJig() == STATUS_SUCCESS ) {	// jigmode의 동작이면
//					FeedbackModeCompleted(134, VOL_CHECK);	//	등록된 모든 지문이 모두 삭제 되었습니다.
					FeedbackModeCompleted(AVML_DEL_ALL_FINGER_DONE_MSG, VOL_CHECK);	//	등록된 모든 지문이 모두 삭제 되었습니다.
					BioOffModeClear();
				}
				else {
					BioModuleOff();

//					FeedbackModeCompleted(1, VOL_CHECK);
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					ModeClear();

					// 지문 전체 삭제 정보 전송
					PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, 0xFF);
					// 지문 전체 삭제 정보 전송
				}
				break;
			}
			break;


		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
			BioOffModeClear();
//			AudioFeedback(118, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.				
			AudioFeedback(AVML_MALFUNCTION_FPM_MSG, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.
			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;
		default:
			BioOffModeClear();
			break;
	}
}




#endif



