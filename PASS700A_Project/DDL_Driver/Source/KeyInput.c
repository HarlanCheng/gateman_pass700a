#include	"Main.h"

FLAG NewPort;
FLAG PortVal;

BYTE gbOldPortChatering = 0;

BYTE gbOldKeyBuffer = 0;
BYTE gbNewKeyBuffer = TENKEY_NONE;	//키패드 없는 모델의 생산지그 프로그램을 사용하기 위함.2020년02월21일 sjc

BYTE gbOldFunctionKey =	0;
BYTE gbOldOCFunctionKey =	0;

BYTE gbNewFunctionKey = 0;
BYTE gbNewOCFunctionKey = 0;
BYTE gbPreOCFunctionKey = FUNKEY_OPCLOSE_PRESS;



BYTE gbInputKeyValue = 0;
BYTE gbKeyCheckCnt = 0;
BYTE gbKeyOCCheckCnt = 0;

BYTE gbSwCheckCnt = 0;

BYTE gbKeyInitialCnt = 100;//50;


// touch OC button이 있는 모델인 경우 별도 함수로 아래를 구현한다.
// touch OC가 없으면 다른 source에서 구현하지 않는다.
// if OC button pressed, return 1
#ifdef	P_TOUCH_O_C_T
int	CheckTouchOCbutton( void )
{
/*
	if ( (gbDDL_hwmodel == _DDL_HWMODEL_Z10_IH) || (gbDDL_hwmodel == _DDL_HWMODEL_J20_IH) ) {
		if ( !P_TOUCH_O_C_T )
			return	dbg = 1;
		else
			return	dbg = 0;
	}
	return	1;
*/

	if(IsSafeOCButtonClear() == STATUS_SUCCESS)
	{
		return	1;
	}
	else
	{
		if ( !P_TOUCH_O_C_T )
			return	1;
		else
			return	0;
	}
}
#else 
int	CheckTouchOCbutton( void )
{
	return	1;
}
#endif 

#ifdef	P_SW_O_C_T		// Open/Close button OChyojoon.kim
void KeyOCCheckRtn(void)
{
	BYTE bKeyTmp;
	BYTE bNewKeyTmp;	
 	
	bKeyTmp = 0;
	bNewKeyTmp = 0;

	if(P_SW_O_C_T)		bKeyTmp = FUNKEY_OPCLOSE_PRESS;
	else 					bKeyTmp = FUNKEY_OPCLOSE_RELEASE;

	if(gbOldOCFunctionKey != bKeyTmp)
	{
		gbKeyOCCheckCnt = 0;
		gbOldOCFunctionKey = bKeyTmp;
	}

	gbKeyOCCheckCnt++;

	if(gbKeyOCCheckCnt >= 20)
	{
		if ( bKeyTmp == FUNKEY_OPCLOSE_PRESS ) {
			//touch OC button에 대한 chattering 처리는 하지 않는다.
			if ( CheckTouchOCbutton() )
				bNewKeyTmp = gbOldOCFunctionKey;
		}
		else {
			bNewKeyTmp = gbOldOCFunctionKey;
		}

		if(bNewKeyTmp == FUNKEY_OPCLOSE_PRESS && (gbPreOCFunctionKey == FUNKEY_OPCLOSE_RELEASE) )
		{
			gbNewOCFunctionKey = FUNKEY_OPCLOSE;
			gbPreOCFunctionKey = FUNKEY_OPCLOSE_PRESS;
		}
		else if(bNewKeyTmp == FUNKEY_OPCLOSE_RELEASE && (gbPreOCFunctionKey == FUNKEY_OPCLOSE_PRESS))
		{
			gbNewOCFunctionKey = TENKEY_NONE;
			gbPreOCFunctionKey = FUNKEY_OPCLOSE_RELEASE;
		}
	
		gbKeyOCCheckCnt = 0;
	}
}
#endif 

void KeyCheckRtn(void)
{
	BYTE bKeyTmp;
 	
	bKeyTmp = 0;

	// Function Key 처리 우선 순위
	//	열림/닫힘 버튼 > * 버튼 > 등록 버튼 > MUTE 버튼
#ifdef	P_SW_REG_T		// 등록 버튼
#ifdef	DDL_CFG_SW_REG_ACTIVE_LOW
	if(!P_SW_REG_T) 		bKeyTmp = FUNKEY_REG;
#else
	if(P_SW_REG_T)		bKeyTmp = FUNKEY_REG;
#endif
#endif

#if 0
#ifdef	P_SW_O_C_T		// Open/Close button
	if(P_SW_O_C_T)		bKeyTmp = FUNKEY_OPCLOSE;
#endif
#endif

#ifdef	P_MUTE_T
	if(!P_MUTE_T)		bKeyTmp = FUNKEY_MUTE;
#endif
		
#ifdef	P_KEY_STAR_T	// Front * button
	if(P_KEY_STAR_T)	bKeyTmp = TENKEY_STAR;
#endif

	if(gbOldFunctionKey != bKeyTmp)
	{
		gbKeyCheckCnt = 0;
		gbOldFunctionKey = bKeyTmp;
	}

	gbKeyCheckCnt++;

	if(gbKeyCheckCnt >= 20)
	{
		if ( bKeyTmp == FUNKEY_OPCLOSE ) {
			//touch OC button에 대한 chattering 처리는 하지 않는다.
			if ( CheckTouchOCbutton() )
				gbNewFunctionKey = gbOldFunctionKey;
		}
		else {
			gbNewFunctionKey = gbOldFunctionKey;
		}
		
		gbKeyCheckCnt = 0;
	}
}


void SwitchCheckRtn(void)
{
	BYTE bTmp = 0;

#ifdef	DDL_DUAL_POINT_DPS	//FDS에서 사용하는 투 포인트 DPS 사용 정의
	if(!P_SNS_NORMAL_OPEN && P_SNS_NORMAL_CLOSE)	
	{
		bTmp |=	0x01;
	}
#else	//DDL_DUAL_POINT_DPS #else
#ifdef	P_SNS_EDGE_T			// Edge sensor
	#ifdef DDL_CFG_SNS_EDGE_ACTIVE_HIGH 
		if(P_SNS_EDGE_T)	bTmp |=	0x01;
	#else
		if(!P_SNS_EDGE_T)	bTmp |=	0x01;
	#endif 	
#endif
#endif	//DDL_DUAL_POINT_DPS #endif

#ifdef	P_SW_AUTO_T				// Auto close switch
	if(!P_SW_AUTO_T)
		bTmp |= 0x02;
#endif

#ifdef	P_SNS_F_BROKEN_T		// Front break signal
	if(P_SNS_F_BROKEN_T)
		bTmp |=	0x04;
#endif

#ifdef	P_IBUTTON_COVER_T		// FingerPrint module cover switch
#ifdef DDL_CFG_FP_COVER_ACTIVE_HIGH	
	if(P_IBUTTON_COVER_T)
#else 
	if( !P_IBUTTON_COVER_T ) 
#endif 		
	{
		bTmp |=	0x08;
	}
#endif

#ifdef	P_SW_COVER_T			// Front Ten Key cover switch
	#ifdef	DDL_CFG_COVER_ACTIVE_HIGH
		if(P_SW_COVER_T)		bTmp |=	0x08;
	#else
	        if(!P_SW_COVER_T)		bTmp |=	0x08;
        #endif
#endif

#ifdef P_KEY_INTER_CL_T
	if(!P_KEY_INTER_CL_T)
		bTmp |=	0x10;
#endif 

	if(gbOldPortChatering != bTmp)
	{
		gbSwCheckCnt = 0;
		gbOldPortChatering = bTmp;
		return;
	}
	gbSwCheckCnt++;

	if(gbSwCheckCnt >= 20)
	{
		gbSwCheckCnt = 0;

		if(gbKeyInitialCnt)
		{
			gbPortVal = 0;
		}
		else
		{
			gbPortVal = gbNewPortChatering ^ gbOldPortChatering;
		}
		gbNewPortChatering = gbOldPortChatering;
	}
}



void KeyChattering(void)
{
	if(gbKeyInitialCnt)	--gbKeyInitialCnt;
	
#ifdef DDL_CFG_IOEXPANDER
	IoExpanderInPutRead();
#endif

#ifdef	P_SW_O_C_T		// Open/Close button
	KeyOCCheckRtn();
#endif 
	KeyCheckRtn();
#ifdef	DDL_CFG_TOUCHKEY
	TKeyCheckRtn();
#endif

#ifdef	DDL_CFG_PUSHKEY
	ADKeyCheckRtn();
#endif
	SwitchCheckRtn();
}


BYTE KeyInputCheck(void)
{
	BYTE bTmp = 0;

//내부 키만 처리할 것
	
#ifdef	P_SW_REG_T		// 등록 버튼
#ifdef	DDL_CFG_SW_REG_ACTIVE_LOW
	if(!P_SW_REG_T)		bTmp++;
#else 
	if(P_SW_REG_T)		bTmp++;
#endif
#endif

#ifdef	P_SW_O_C_T		// Open/Close button
	if(P_SW_O_C_T)		bTmp++;
#endif

	return bTmp;
}
	
void KeyInterruptInitial(void)
{
	// Fucntion Key 로 처리되는 경우에 대해서만 해당 변수 Clear
	gbNewFunctionKey = 0;
}


void IntialKeySwtichSet(void)
{
#ifdef	DDL_DUAL_POINT_DPS	//FDS에서 사용하는 투 포인트 DPS 사용 정의
		if(!P_SNS_NORMAL_OPEN && P_SNS_NORMAL_CLOSE)	
		{
			gfDoorSwOpenState = 1;
		}	
#else	//DDL_DUAL_POINT_DPS #else	

#ifdef	P_SNS_EDGE_T			// Edge sensor

#ifdef DDL_CFG_SNS_EDGE_ACTIVE_HIGH  
			if(P_SNS_EDGE_T)
			{
				gfDoorSwOpenState = 1;
			}
#else
			if(!P_SNS_EDGE_T)
			{
				gfDoorSwOpenState = 1;
			}
#endif	//DDL_CFG_SNS_EDGE_ACTIVE_HIGH #endif

#endif	//P_SNS_EDGE_T #endif

#endif	//DDL_DUAL_POINT_DPS #endif
	
#ifdef	P_SW_AUTO_T				// Auto close switch
		if(!P_SW_AUTO_T)
			gfAutoLockState = 1;
#endif
	
#ifdef	P_SNS_F_BROKEN_T		// Front break signal
		if(P_SNS_F_BROKEN_T)
			gfFrontBroken = 1;
#endif
	
#ifdef	P_IBUTTON_COVER_T		// FingerPrint module cover switch
#ifdef DDL_CFG_FP_COVER_ACTIVE_HIGH	
		if(P_IBUTTON_COVER_T)
#else 
		if( !P_IBUTTON_COVER_T ) 
#endif 			
		{
			gfCoverSw = 1;
		}
#endif
	
#ifdef	P_SW_COVER_T			// Front Ten Key cover switch
	#ifdef	DDL_CFG_COVER_ACTIVE_HIGH
			if(P_SW_COVER_T)
			{
				gfCoverSw = 1;
			}
	#else
			if(!P_SW_COVER_T)
			{
				gfCoverSw = 1;
			}
    #endif
#endif

#ifdef P_KEY_INTER_CL_T
	if(!P_KEY_INTER_CL_T)
		gfkeyInterCl = 1;
#endif 

}



