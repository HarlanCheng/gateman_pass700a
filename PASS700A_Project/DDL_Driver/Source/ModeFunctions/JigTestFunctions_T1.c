#include "Main.h"

#include "stm32l1xx_hal_usart.h"



BYTE gbJigDataBuf[16];
BYTE gbJigTimer1s = 0;
BYTE gbJigPrcsMode = 0;
BYTE gbJigInputDataBuf[9];

BYTE gbJigPowerDownModeEnable = 0;

BYTE gbJigPackMotorControl = 0;

#define JIG_TEST_FRONT	0x01
#define JIG_TEST_MAIN	0x02
#define JIG_TEST_FULL	0x03

BYTE gbJigTestMode = 0;

BYTE gBypassDoneCheck = 0;	//PC 생산 지그에서 송신한 Module sleep data를 받았는지 판단 하는 변수, 프로비저닝 이후 PC생산 지그를 생산 하기 위함

// 최대 값은 2까지만 허용, 0, 1, 2 모두 구현 가능, 
//	3 이상 처리할 경우에는 MemAccess 함수 수정 필요
#define	ACCESS_PERMIT_MEMAREA_NO	2	

#if (DDL_CFG_BLE_30_ENABLE >= 0x31)

#define	ACCESS_PERMIT_MEMAREA_1_START	0x0000
#define	ACCESS_PERMIT_MEMAREA_1_END		0x0037

#define	ACCESS_PERMIT_MEMAREA_2_START	0x1FA0
#define	ACCESS_PERMIT_MEMAREA_2_END		0x1FFF

#else	// DDL_CFG_BLE_30_ENABLE

#define	ACCESS_PERMIT_MEMAREA_1_START	0x0000
#define	ACCESS_PERMIT_MEMAREA_1_END		0x002F

#if defined (DDL_CFG_MS)
#define	ACCESS_PERMIT_MEMAREA_2_START	0x0360
#else
#define	ACCESS_PERMIT_MEMAREA_2_START	0x03C0
#endif 
#define	ACCESS_PERMIT_MEMAREA_2_END		0x03FF

#endif	// DDL_CFG_BLE_30_ENABLE

void JigSetHistroy(BYTE bData)
{
	BYTE bTemp = 0x00;
	BYTE bTemp1 = 0x00;
	BYTE bTemp2 = 0x00;
	BYTE bTemp3 = 0x00;

	RomRead(&bTemp, (WORD)MP_HISTORY_COUNTER, 1);

	if(bTemp == 0x00 || bTemp == 0xFF)
	{
		// factory , N1 , N2 , PC 생산 프롣그램중 처음 시도할 경우 
		bTemp = 0x00;
		RomWriteWithSameData(bTemp,MP_HISTORY_COUNTER,5);// 상태 초기화 
	}

	// 8 개 저장 
	bTemp1 = bTemp % 8;

	if(bTemp1 <= 1)
		bTemp3 = 0;
	else if (bTemp1 <= 3)
		bTemp3 = 1;		
	else if (bTemp1 <= 5)	
		bTemp3 = 2;
	else if (bTemp1 <= 7)	
		bTemp3 = 3;
	else 
		bTemp3 = 0;

	RomRead(&bTemp2, (WORD)MP_HISTORY_COUNTER+1+bTemp3,1);			
	
	if(bTemp1 % 2)
	{
		//홀수 하위 니블 
		bTemp2 = (bTemp2 & 0xF0) |bData ;
	}
	else 
	{
		//짝수 상위 니블 	
		bTemp2 = (bTemp2 & 0x0F) |(bData << 0x04) ;
	}

	RomWrite(&bTemp2,MP_HISTORY_COUNTER+1+bTemp3,1);

	bTemp++;
	RomWrite(&bTemp,MP_HISTORY_COUNTER,1);

}

void JigHistroyload(void)
{
	BYTE bHistory[5] = {0x00,};
	BYTE bBuffer[8] = {0x00,};
	BYTE bTemp = 0x00;
	BYTE bTemp1 = 0x00;

	RomRead(bHistory, (WORD)MP_HISTORY_COUNTER, 5);

	// data 를 일단 채우고 
	for(bTemp = 0x00; bTemp < 4;bTemp++)
	{
		bBuffer[bTemp1] = ((bHistory[bTemp+1] & 0xF0) >> 4);
		bBuffer[bTemp1+1] = ((bHistory[bTemp+1] & 0x0F));
		bTemp1 += 2 ;
	}

	// data 를 sort 하는데 가장 최근의 것을 가장 첫째 data 로 
	bTemp = 0x00;
	bTemp1 = 0x00;	

	bTemp = bHistory[0] % 8;

	for(bTemp1 = 0x00 ; bTemp1 < 8;bTemp1++)
	{
		if(bTemp == 0)
			bTemp = 7;
		else 
			bTemp--;
		gbJigDataBuf[bTemp1] = bBuffer[bTemp];
	}
}

void SetJigModeID(BYTE *pDeviceIdBuf)
{
	if(*pDeviceIdBuf == 0xAB)
	{
		gbJigTestMode = JIG_TEST_FRONT;
	}
	else
	{	
		gbJigTestMode = 0x00;
	}	
}

BYTE GetJigModeID(void)
{
	return	gbJigTestMode;
}


void JigProcessTimeCount(void)
{
	if(gbJigTimer1s)	--gbJigTimer1s; 
}



void JigModeCheck(void)
{
#ifdef P_KEY_INTER_CL_T
	if(gfIntrusionAlarm || gfBrokenAlarm || gfKeyInterClAlarm)
#else 
	if(gfIntrusionAlarm || gfBrokenAlarm)	
#endif 		
	{
		return;
	}
	
	if(KeyInputCheck())
	{
		gbJigTimer1s = 60;
	}
}


BYTE JigModeStatusCheck(void)
{
	if(gbJigTimer1s)
	{
		return (STATUS_SUCCESS);
	}
	else
	{
		return (STATUS_FAIL);
	}
}




void JigDataReceive(void)
{
	BYTE bTmp;

	if(!gbJigTimer1s)	return;

	if ( CommPack_DequeueRx( &bTmp, 1 ) != 1 )	// receive queue is empty
		return;

	gbPackRxDataBuf[gbPackRxByteCnt] = bTmp;
	gbPackRxByteCnt++;
		
	switch(gbPackRxByteCnt)
	{
		case 1:
			if(gbPackRxDataBuf[0] != TEST_FRAME_STX)
			{
				gbPackRxByteCnt = 0;
			}
			break;
	
		case 2: 		
			if(gbPackRxDataBuf[1] != TEST_CLASS_MAFA_JIG)
			{
				gbPackRxByteCnt = 0;
			}
			break;
	
		case 4: 		
			gbPackRxAddDataLen = gbPackRxDataBuf[3];
			break;
	
		case 3:
			break;	

		default:
			if(gbPackRxByteCnt == (gbPackRxAddDataLen+5))
			{
				if(gbPackRxDataBuf[gbPackRxAddDataLen+4] != TEST_FRAME_ETX)
				{
					gbPackRxByteCnt = 0;
				}
				else
				{
					gbJigPrcsMode = 2;
					gbPackRxByteCnt = 0;
				}	
			}
			if(gbPackRxByteCnt >= MAX_PACK_BUF_SIZE)	
                          gbPackRxByteCnt = 0;					
			break;
	}

}



void JigTxAckSetting(BYTE bCmd, BYTE* bpData, BYTE bNum)
{
	BYTE bCnt;
	
	if(!gbJigTimer1s)	return;
	
	gbPackTxDataBuf[0] = TEST_FRAME_STX;
	gbPackTxDataBuf[1] = TEST_CLASS_MAFA_JIG;
	gbPackTxDataBuf[2] = bCmd;

	gbPackTxDataBuf[3] = bNum;

	for(bCnt = 0; bCnt < bNum; bCnt++)
	{ 		
		gbPackTxDataBuf[4+bCnt] = *bpData;	
		bpData++;
	}

	gbPackTxDataBuf[bNum+4] = TEST_FRAME_ETX;

	gbPackTxByteNum = bNum+5;					

	UartSendCommPack(gbPackTxDataBuf, gbPackTxByteNum);
}



// 지그 프로그램에서 처리할 수 있는 값으로 변환
enum{
	JIG_SENSOR_OPEN_STATE = 0,
	JIG_SENSOR_OPENCEN_STATE,
	JIG_SENSOR_OPENLOCK_STATE,
	JIG_SENSOR_CLOSE_STATE,
	JIG_SENSOR_CLOSECEN_STATE,
	JIG_SENSOR_CLOSELOCK_STATE,
	JIG_SENSOR_CENTER_STATE,
	JIG_SENSOR_CENTERLOCK_STATE,
	JIG_SENSOR_NOT_STATE,
	JIG_SENSOR_LOCK_STATE,
};	


BYTE CovertMotorStatus(BYTE bStatus)
{
	BYTE RetVal = 0;

	switch(bStatus)
	{
		case SENSOR_OPEN_STATE:
			RetVal = JIG_SENSOR_OPEN_STATE;
			break;
			
		case SENSOR_CLOSE_STATE:
			RetVal = JIG_SENSOR_CLOSE_STATE;
			break;
			
		case SENSOR_OPENLOCK_STATE:
			RetVal = JIG_SENSOR_OPENLOCK_STATE;
			break;

		case SENSOR_CLOSELOCK_STATE:
			RetVal = JIG_SENSOR_CLOSELOCK_STATE;
			break;

		case SENSOR_LOCK_STATE:
			RetVal = JIG_SENSOR_LOCK_STATE;
			break;

#ifdef P_SNS_CENTER_T
		case SENSOR_OPENCEN_STATE:
			RetVal = JIG_SENSOR_OPENCEN_STATE;
			break;

		case SENSOR_CLOSECEN_STATE:
			RetVal = JIG_SENSOR_CLOSECEN_STATE;
			break;

		case SENSOR_CENTER_STATE:
			RetVal = JIG_SENSOR_CENTER_STATE;
			break;

		case SENSOR_CENTERLOCK_STATE:
			RetVal = JIG_SENSOR_CENTERLOCK_STATE;
			break;
#endif
		default:
			RetVal = JIG_SENSOR_NOT_STATE;
			break;
	}

#ifndef	P_SNS_LOCK_T
	//내부강제잠금 센서 미지원
	RetVal |= 0x80;	
#endif	

	return (RetVal);
}


void JigDDLStatusDataSet(void)
{
	BYTE Temp = 0x00;
	
	if(gbJigInputDataBuf[0] == 0xFF)
	{
		gbJigDataBuf[0] = 0x31;
	//--------------------------------
		if(gfLowBattery)
		{
			gbJigDataBuf[1] = 0x31;
		}
		else
		{
			gbJigDataBuf[1] = 0x30;
		}

	//--------------------------------
		gbJigDataBuf[2] = CovertMotorStatus(MotorSensorCheck());

	//--------------------------------
		if(gbNewFunctionKey != 0)
		{
			gbJigDataBuf[3] = gbNewFunctionKey;
		}
		else if(gbNewKeyBuffer != TENKEY_NONE)
		{
			gbJigDataBuf[3] = gbNewKeyBuffer;
		}
#ifdef	P_SW_O_C_T		// Open/Close button OChyojoon.kim
		else if(gbNewOCFunctionKey != TENKEY_NONE)
		{
			gbJigDataBuf[3] = gbNewOCFunctionKey;
		}
#endif 
		else
		{
			gbJigDataBuf[3] = TENKEY_NONE;
		}

	//--------------------------------
#ifdef	P_SNS_EDGE_T			// Edge sensor
		//도어위치센서가 있는 경우
		if(gfDoorSwOpenState)
		{
			gbJigDataBuf[4] = 0x31;
		}
		else
		{
			gbJigDataBuf[4] = 0x30;
		}
#else
		//도어위치센서가 없을 경우
		gbJigDataBuf[4] = 0xFF;
#endif
	//--------------------------------
		// RTC 없음	
#ifdef RTC_PCF85063
		if(gfRtcErr)
		{
			gbJigDataBuf[5] = 0x31;
		}
		else
		{
				gbJigDataBuf[5] = 0x30;
			}
#else 
		gbJigDataBuf[5] = 0xFF;
#endif 

	//--------------------------------
#ifdef	LOCKSET_HANDING_LOCK
		gbJigDataBuf[6] = GetHandingLock();
#else
		// 좌수/우수 없음 
		gbJigDataBuf[6] = 0xFF;
#endif

	//--------------------------------
#ifdef	P_SW_AUTO_T				// Auto close switch
		//자동/수동 설정 스위치가 있는 경우
		if(AutoLockSetCheck())
		{
			gbJigDataBuf[7] = 0x30;
		}
		else
		{
			gbJigDataBuf[7] = 0x31;
		}
#else			
		//자동/수동 설정 스위치가 없을 경우
		gbJigDataBuf[7] = 0xFF;
#endif
	//--------------------------------

#ifdef	P_SNS_F_BROKEN_T		// Front Broken
		if(gfFrontBroken)
		{
			gbJigDataBuf[8] = 0x31;
		}
		else
		{
			gbJigDataBuf[8] = 0x30;
		}
#else			
		//파손 감지 기능이 없을 경우
		gbJigDataBuf[8] = 0xFF;
#endif
	//--------------------------------

#ifdef	P_SW_COVER_T		// 
		// 커버 슬라이드가 있는 경우 
		if(gfCoverSw)
		{
			gbJigDataBuf[9] = 0x31;
		}
		else
		{
			gbJigDataBuf[9] = 0x30;
		}
#else			
		//커버 슬라이드가 없는 경우 
		gbJigDataBuf[9] = 0xFF;
#endif

#ifdef	DDL_CFG_FP		// 
		// 지문 uart 검증 
		if(gfBioErr)
		{
			gbJigDataBuf[10] = 0x31;
		}
		else
		{
			gbJigDataBuf[10] = 0x30;
		}

		//지문 커버 or 에어리어 타잎 touch 
#ifdef	P_IBUTTON_COVER_T
#ifdef	DDL_CFG_FP_MOTOR_COVER_TYPE
		// 자동 커버 의 경우 
		if(gfCoverSw)
		{
			// 커버 열림 상태 			
			gbJigDataBuf[11] = 0x33;
		}
		else 
		{
			// 커버 닫힘 상태 
			gbJigDataBuf[11] = 0x32;
		}
#else 
		// 수동 커버의 경우 
		if(gfCoverSw)
		{
			// 커버 열림 상태 			
			gbJigDataBuf[11] = 0x31;
		}
		else 
		{
			// 커버 닫힘 상태 
			gbJigDataBuf[11] = 0x30;
		}
#endif 		
#else 
		// 지문 모듈 이지만 지문 모듈 커버가 없는 경우 
		gbJigDataBuf[11] = 0xFF;
#endif 
#else			
		//지문이 없는 경우 
		gbJigDataBuf[10] = 0xFF;
		gbJigDataBuf[11] = 0xFF;
#endif

#ifdef	DDL_CFG_TOUCHKEY
		// touch IC 검증 
		if(gfTouchErr)
		{
			gbJigDataBuf[12] = 0x31;
		}
		else
		{
			gbJigDataBuf[12] = 0x30;
		}
#else 
		// touch IC 없는 경우 
		gbJigDataBuf[12] = 0xFF;
#endif 

		// volume 검증 
		Temp = VolumeSettingCheck();

#ifdef	DDL_CFG_BUZ_VOL_TYPE1
		//Programmable volume
		if(Temp == VOL_SILENT)
		{
			gbJigDataBuf[13] = 0x33;
		}
		else if(Temp == VOL_LOW)
		{
			gbJigDataBuf[13] = 0x34;
		}
		else if(Temp == VOL_HIGH)
		{
			gbJigDataBuf[13] = 0x35;
		}
		else 
#else 
		// switch type volume 
		if(Temp == VOL_SILENT)
		{
			gbJigDataBuf[13] = 0x30;
		}
		else if(Temp == VOL_LOW)
		{
			gbJigDataBuf[13] = 0x31;
		}
		else if(Temp == VOL_HIGH)
		{
			gbJigDataBuf[13] = 0x32;
		}
		else 		
#endif 
		{
			// 위 3 가지가 아닌 경우 VolumeSettingCheck() 구조상 여기 올리는 없지만... 
			gbJigDataBuf[13] = 0xFF;
		}

			//--------------------------------
		memset(&gbJigDataBuf[14], 0xFF,2);


#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
		// 추가 상태 확인 정보
		gbJigDataBuf[15] = ADD_DDL_STATUS_DATA_NUM;
#endif	


	}
	else
	{
		memset(gbJigDataBuf, 0xFF, 8);
		gbJigDataBuf[0] = gbJigInputDataBuf[0];
		
		memcpy(&gbJigDataBuf[8], &gbJigInputDataBuf[1], 8);
		memset(gbJigInputDataBuf, 0xFF, 9);
	}
}


#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
void JigDDLStatusDataSet_1(void)
{
	if(gbJigInputDataBuf[0] == 0xFF)
	{
		gbJigDataBuf[0] = 0x81;

		gbJigDataBuf[1] = HW_CHECK_DATA;

	//--------------------------------
#ifdef	USED_SECOND_MOTOR
		gbJigDataBuf[2] = CovertMotorStatus(MotorSensorCheck_Second());
#else
		gbJigDataBuf[2] = 0xFF;
#endif
	//--------------------------------

#ifdef EX_EEPROM
		if(gfEepromErr)
		{
			gbJigDataBuf[3] = 0x31;
		}
		else
		{
			gbJigDataBuf[3] = 0x30;
		}
#else
		gbJigDataBuf[3] = 0xFF;
#endif 


		memset(&gbJigDataBuf[4], 0xFF, 12);
	}
	else
	{
		memset(gbJigDataBuf, 0xFF, 8);
		gbJigDataBuf[0] = gbJigInputDataBuf[0];
		
		memcpy(&gbJigDataBuf[8], &gbJigInputDataBuf[1], 8);
		memset(gbJigInputDataBuf, 0xFF, 9);
	}
}
#endif	


void JigInputDataSave(BYTE bMode, BYTE *pData, BYTE bSize)
{
	if(!gbJigTimer1s)		return;

	memset(gbJigInputDataBuf, 0xFF, 9);
	gbJigInputDataBuf[0] = bMode;
	memcpy(&gbJigInputDataBuf[1], pData, bSize); 
}



WORD CovertAdc12bitTo10bit(WORD wValue)
{
	WORD RetVal;

	RetVal = (wValue*0x3FF)/0xFFF;

	return (RetVal);
}




void JigInitialWithKeepPincode(void);
void JigMemAccessAreaCheck(void);
void JigMemAccessAreaReadWrite(BYTE *pDataBuf);


void JigRxCmdProcess(void)
{
	WORD wAdTmp;
	BYTE bAckDataBuf[16];
	BYTE bTemp = 0x00;
#ifdef DDL_AAAU_FACTORY_RESET_UI//호주 및 뉴질랜드 전용 Factory Reset UI
	BYTE bTmpArray[4];
#endif	

	if(!gbJigTimer1s)	return;

	switch(gbPackRxDataBuf[2])
	{
		case TEST_JIG_REQ_LOOPTEST:
			gbJigDataBuf[0] = 0x00;
			JigTxAckSetting(TEST_JIG_ACK_LOOPTEST, gbJigDataBuf, 0);
			gbJigTimer1s = 60;
			break;

		case TEST_JIG_GET_VERSION:
			gbJigDataBuf[0] = DDL_SET_CODE_MAJOR;
			gbJigDataBuf[1] = DDL_SET_CODE_MINOR;
			gbJigDataBuf[2] = PROGRAM_VERSION_MAJOR;
			gbJigDataBuf[3] = PROGRAM_VERSION_MINOR;
			JigTxAckSetting(TEST_JIG_SEND_VERSION, gbJigDataBuf, 8);
			gbJigTimer1s = 60;
			break;

		case TEST_JIG_SET_SERIAL:
//			RomWrite(gbPackRxDataBuf+4, (WORD)PRODUCT_CODE, 16);
			RomWrite(gbPackRxDataBuf+4, (WORD)DOORLOCK_ID, 16);
			gbJigTimer1s = 60;
			break;

		case TEST_JIG_GET_SERIAL:
//			RomRead(gtbRomReadBuf, (WORD)PRODUCT_CODE, 16);  
			RomRead(bAckDataBuf, (WORD)DOORLOCK_ID, 16);  
			
			JigTxAckSetting(TEST_JIG_SEND_SERIAL, bAckDataBuf, 16); 
			gbJigTimer1s = 60;
			break;

		case TEST_JIG_SET_INITIAL:
			if(gbPackRxDataBuf[3] == 16)
			{
#if defined (DDL_CFG_MS)
				//RomWriteWithSameData(0x00,MS_MANAGER_CARD_NUM,MS_CREDENTIAL_NUM_SIZE);	
				//FactoryResetSet(FACTORYRESET_DONE_BY_MANUAL);
				MS_All_Credential_Init();
				MS_Credential_Info_Init();

					/* MS 사양 정보가 입력 되었을 경우만 PIN code 저장 */
				/* MS_TEMP_PINCODE 위치에 받은 값을 setting */					
#ifdef DDL_CFG_EMERGENCY_119
					memset(gPinInputKeyFromBeginBuf, 0xFF,((MAX_PIN_LENGTH>>1)+2));	
#else 
					memset(gPinInputKeyFromBeginBuf, 0xFF,(MAX_PIN_LENGTH>>1));	
#endif					
					memcpy(gPinInputKeyFromBeginBuf,&gbPackRxDataBuf[4],5);
					gPinInputKeyCnt = gbPackRxDataBuf[11];
				UpdatePincodeToMemory(MS_TEMP_PINCODE);
				LockWorkingModeDefaultSet();

#ifdef	LOCKSET_HANDING_LOCK
				HandingLockDefaultSet(gbPackRxDataBuf[12]);

#endif
#else 
				MasterCodeDelete();
				OnetimeCodeDelete();
				AllUserCodeDelete();
#if defined (_USE_LOGGING_MODE_)
				DeleAlarmReportLog();
#endif 

#ifdef 	DDL_CFG_RFID
				AllCardDelete();
#endif	

#ifdef 	_MASTER_CARD_SUPPORT
				AllMasterCardDelete();
#endif	
			//	AllScheduleStatusClear(CLEAR_ALL_CODE);
		
#if defined	(DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
				AllDS1972TouchKeyDelete();
#endif 

#ifdef 	DDL_CFG_IBUTTON
				AllGMTouchKeyDelete();
#endif 
				
				LockWorkingModeDefaultSet();
				
#ifdef	LOCKSET_HANDING_LOCK
				HandingLockDefaultSet(gbPackRxDataBuf[12]);
#endif

				if(gbManageMode == _AD_MODE_SET)
				{
					UpdatePincodeToMemoryByModule(&gbPackRxDataBuf[4], 8, RET_MASTER_CODE_MATCH, gbPackRxDataBuf[11]);
				}
				else
				{
					UpdatePincodeToMemoryByModule(&gbPackRxDataBuf[4], 8, 0x01, gbPackRxDataBuf[11]);
				
					SetUserStatus(1, USER_STATUS_OCC_ENABLED);
				}
#endif

				if(gbPackRxDataBuf[4] == 0xFF && gbPackRxDataBuf[5] == 0xFF)
				{
					//N2 init pack 
					JigSetHistroy(MP_N2_HISTORY);
				}
				else 
				{
					// PC 생산지그 
					JigSetHistroy(MP_PC_HISTORY);
				}
			}

			gbJigTimer1s = 60;

			memset(bAckDataBuf, 0xFF, 16);
#if defined (DDL_CFG_MS)
			RomRead(bAckDataBuf, (WORD)MS_TEMP_PINCODE, 6);
#else 
			if(gbManageMode == _AD_MODE_SET)
			{
				RomRead(bAckDataBuf, (WORD)MASTER_CODE, 6);	
			}
			else
			{
				RomRead(bAckDataBuf, (WORD)USER_CODE, 6);	
			}
#endif 

#if defined (_USE_IREVO_CRYPTO_)
			EncryptDecryptKey(bAckDataBuf,6);
#endif 	

			wAdTmp = TempSensorCheck();

			// 지그 프로그램이 10bit ADC에 맞추어져 있어 변환
			wAdTmp = CovertAdc12bitTo10bit(wAdTmp);		
					
			bAckDataBuf[8] = (BYTE)(wAdTmp>>8);
			bAckDataBuf[9] = (BYTE)(wAdTmp);

			JigTxAckSetting(TEST_JIG_SEND_INITAIL, bAckDataBuf, 16); 

			gbDDLStateData = 0x00;							
			RomWrite(&gbDDLStateData, (WORD)DDL_STATE, 1);			

			TamperCountClear();

#ifndef		P_SNS_LOCK_T	
			InnerForcedLockClear();
#endif
			
#ifdef DDL_AAAU_FACTORY_RESET_UI//호주 및 뉴질랜드 전용 Factory Reset UI
			AutoRelockDisable();
			//AutoLockTimeAndAutoReLockTimeDefaultSetAndWrite();
			bTmpArray[0] = 0;
			bTmpArray[1] = 0;
			bTmpArray[2] = 0;
			bTmpArray[3] = 0;
			AutoLockTimeAndAutoReLockTimeCalculationAndWrite(bTmpArray);
#ifdef DDL_AAAU_AUTOLOCK_HOLD //호주 및 뉴질랜드 전용 autolock hold 기능			
			AutolockHoldSetting(AUTOLOCKUNHOLD);	//AutoHold 기능은 Unhold 상태로 설정
#endif			
#ifdef DDL_LED_DISPLAY_SELECT //led display 결정 
			LedDisplayEnableStateSet(0xFF);//LED Display Enable 0x00 = OFF, 0xFF = ON default is 0xFF or ON.
#endif			
#else //DDL_AAAU_FACTORY_RESET_UI #else
			AutoRelockEnable();
			AutoLockTimeAndAutoReLockTimeDefaultSetAndWrite();
#endif //DDL_AAAU_FACTORY_RESET_UI #endif

			VolumeSetting(VOL_HIGH); //볼륨 LOW

			//FactoryResetSet(FACTORYRESET_DONE_BY_MANUAL);
			FactoryResetDoneCheck();

#ifdef	DDL_CFG_RFID
#ifdef	IREVO_CARD_ONLY
			CardOpenUidSupportedInfoSave(0xFF);
#else
			CardOpenUidSupportedInfoSave(0x74);
#endif
#endif 

#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			SetPackCBARegisterSet(0);
#endif
			break;

		case TEST_JIG_GET_INITIAL:
			gbJigTimer1s = 60;

			memset(bAckDataBuf, 0xFF, 16);
					
			wAdTmp = TempSensorCheck();
			
			bAckDataBuf[8] = (BYTE)(wAdTmp>>8);
			bAckDataBuf[9] = (BYTE)(wAdTmp);

			JigTxAckSetting(TEST_JIG_SEND_INITAIL, bAckDataBuf, 16); 
			break;
				
		case TEST_JIG_SET_SLEEP:
			
			gbJigTimer1s = 0;
			gbJigPrcsMode = 0;

			gbJigPowerDownModeEnable = 1;

#if defined	(P_SNS_LEFT_RIGHT_T) || defined (LOCK_TYPE_DEADBOLT)
			if(GetJigModeID() != JIG_TEST_FRONT)
			HandingLockAutoRun();
#endif

#ifdef	DDL_CFG_FP
			 //아래 모드를 변경하는 코드로 인해 위의 JigPowerDownModeEnable의 값을 1로 바꾼 효과는 지문이 모두 지워진 뒤에 발생한다.
			//TCS4K 지문 모듈의 지문을 지우는 것은 MainMode를 변화시켜 처리한다.
			if(GetJigModeID() != JIG_TEST_FRONT)
			ModeGotoAdvancedAllFingerDelete2();
#endif		
			//Provisioning 이후 PC생산 지그를 생산 하기 위함
			//YALE Access Module 은 Provisioning 하기전 PC에서 송신 하는 Module sleep data 와 도어락 에서 송신 하는 Module sleep data를 함께 수신 하면 
			//Sleep 진입 시간이 늘어남
			//gBypassDoneCheck = 0 이면 PC 생산지그에서 송신 한 Module sleep data 를 수신 하지 않았으니 Provisioning을 했다는 의미로 판단 하여 도어락에서 만든 Module sleep data를 송신 
			//gBypassDoneCheck = 1 이면 PC 생산지그에서 송신 한 Module sleep data 를 수신 했으니 도어락에서 만든 Module sleep data를 송신 하지 않음 
			if(gBypassDoneCheck == 0)
			{
				if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeCBABle == 1)	//프로비저닝 이후 강제로 모듈을 Sleep 상태로 전환 하기 위함
				{
					bAckDataBuf[0] = 0x02;
					bAckDataBuf[1] = 0x01;
					bAckDataBuf[2] = 0x00;
					PackTx_MakePacketSinglePort(0x4E, ES_LOCK_EVENT, &gbComCnt, bAckDataBuf,3);	
				}
				if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeCBABle == 1)
				{
					bAckDataBuf[0] = 0x02;
					bAckDataBuf[1] = 0x01;
					bAckDataBuf[2] = 0x00;
					InnerPackTx_MakePacketSinglePort(0x4E, ES_LOCK_EVENT, &gbComCnt, bAckDataBuf,3);	
				}
			}
			break;

		case TEST_JIG_SET_OUTPUT:
			gbJigTimer1s = 60;
			bAckDataBuf[0] = 0x00;
			if(gbPackRxDataBuf[4] == 0x01) 
			{
				// 출력 구분 요청 Feedback (Buzzer , Voice , LED 포함)

				//MSB 3bit volume 
				bTemp = ((gbPackRxDataBuf[5] >> 5) & 0x07);
				
				// program volume 설정 
				if(bTemp == 0)					
				{
					// 무음 설정 VOL_SILENT
					gbVolumeMode = VOL_SILENT;
				}
				else if (bTemp == 0x01)
				{
					// 저음 설정 VOL_LOW
					gbVolumeMode = VOL_LOW;
				}
				else if (bTemp == 0x02)
				{
					// 고음 설정 VOL_HIGH					
					gbVolumeMode = VOL_HIGH;
				}
				else 
				{
					// 0x03 HW volume 설정 부분 아무 것도 안한다 
				}

				//LSB 5bit
				switch ((gbPackRxDataBuf[5] & 0x1F))
				{
					case 0x00: //Reserved 

					break;

					case 0x01: //Open 
						FeedbackMotorOpen();
					break;

					case 0x02:  // Close 
						FeedbackMotorClose();
					break;

					case 0x03: // 저전압 
						AudioFeedback(AVML_CHG_BATTERY, gcbBuzLow, VOL_CHECK|MANNER_CHK);
					break;
					
					case 0x04: // 데드볼트걸림 그냥 error 임 
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK|MANNER_CHK);	
					break;

					case 0x05: // KEYPADLED 
						FeedbackKeyPadLedOnLedOnly();
					break;

					case 0x1E : // 중지 
						VolumeSettingLoad(); // 중지가 오면 ROM 값을 읽어서 초기화 
						AudioFeedback(VOICE_STOP,	gcbBuzOff, VOL_CHECK|MANNER_CHK);
						LedForcedAllOff();
					break;
					
					default :
						break;
				}
			}
			else if(gbPackRxDataBuf[4] == 0x02) // 경보 시작 / 종료 
			{
				//MSB 3bit 명령  
				bTemp = ((gbPackRxDataBuf[5] >> 5) & 0x07);

				// 경보 종류 및 시작 
				if (bTemp == 0x01)
				{
					switch ((gbPackRxDataBuf[5] & 0x1F))
					{
						case 0x00: //Reserved 

						break;

						case 0x01: // 침입 경보 시작 
							AlarmGotoIntrusionAlarm(); 

#ifdef P_SW_FACTORY_BROKEN_T
							ChangeFactoryPintoBrokenPinControl(1);
#endif							
						break;

						case 0x1E : //Reserved 
						
						break;
						
						default :
							break;
					}
				}
				else if (bTemp == 0x02)
				{
					AlarmGotoAlarmClear(); // 경보 해제 
				}
				else 
				{
					// Reseved 
				}
			}
			else if((gbPackRxDataBuf[4] == 0x03) && (gbPackRxDataBuf[3] == 0x02))// 테스트 모드 시작 / 종료 
			{
				gPinInputKeyFromBeginBuf[0] = gbPackRxDataBuf[5];
				TestMenuSelectProcess();
			}
			else if((gbPackRxDataBuf[4] == 0x03) && (gbPackRxDataBuf[3] == 0x03))// 테스트 모드 시작 / 종료 with Key Value
			{
				gPinInputKeyFromBeginBuf[0] = gbPackRxDataBuf[5];
				gPinInputKeyFromBeginBuf[1] = gbPackRxDataBuf[6];
				TestMenuSelectProcess();
			}
			JigTxAckSetting(TEST_JIG_ACK_SET_OUTPUT,bAckDataBuf,0);
			break;

		case TEST_JIG_REQ_STATUS:
			if(gbPackRxDataBuf[3] == 0)
			{
				JigDDLStatusDataSet();
			}
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			else if((gbPackRxDataBuf[3] == 1) || (gbPackRxDataBuf[4] == 1))
			{
				JigDDLStatusDataSet_1();
			}
#endif
			JigTxAckSetting(TEST_JIG_ACK_STATUS, gbJigDataBuf, 16);
			gbJigTimer1s = 60;
			break;

		case TEST_JIG_REQ_MOTOR:
			if(gbPackRxDataBuf[3] == 0x00)
			{
				// 제 1 모터 구동 (일반적 모터)
				if(GetMotorPrcsStep())		break;
			
				//ModeGotoOpenClose();	//2017년08월29일 지그모드에서 모터 구동 명령어를 받았을때 바로 모터를 돌리게 하기 위해 이전 함수 호출은 주석 처리  by심재철

				//2017년08월29일 지그모드에서 모터 구동 명령어를 받았을때 바로 모터를 돌리게 하기 위해 추가 시작  by심재철
				gbJigPackMotorControl = 1;
#ifdef USED_SECOND_MOTOR				
				StartMotorToggle_Second();
#endif
				StartMotorToggle();

				gbJigPackMotorControl = 0;
				gbMainMode = MODE_OPEN_CLOSE;
				gbModePrcsStep = MODEPRCS_OPEN_BYKEY_COMPLETE_CHECK;
				//2017년08월29일 지그모드에서 모터 구동 명령어를 받았을때 바로 모터를 돌리게 하기 위해 추가 끝  by심재철
			}
			else if(gbPackRxDataBuf[3] == 0x01)
			{
				if(gbPackRxDataBuf[4] == 0x01)
				{
					ModeClear();
					// 제 2 모터 구동 (지문 커버 같은 것들) 
#ifdef	DDL_CFG_FP_MOTOR_COVER_TYPE
					if(gbFingerCoverState == FINGER_COVER_OPENING || gbFingerCoverState == FINGER_COVER_CLOSING)
						break;
				
					if(gbFingerCoverState == FINGER_COVER_OPENED)
						FPMotorCoverAction(_CLOSE);
					else 
						FPMotorCoverAction(_OPEN);
#endif 
				}
			}
			else if(gbPackRxDataBuf[3] == 0x02)
			{
				//좌수/우수 설정 내용에 따른 모터 구동 요청 처리
				if(gbPackRxDataBuf[4] == 0x00)
				{
					// 제 1 모터 구동 (일반적 모터)
					if(GetMotorPrcsStep())		break;
				
#ifdef	LOCKSET_HANDING_LOCK
					HandingLockDefaultSet(gbPackRxDataBuf[5]);
#endif

					gbJigPackMotorControl = 1;
					StartMotorToggle();
					gbJigPackMotorControl = 0;
					gbMainMode = MODE_OPEN_CLOSE;
					gbModePrcsStep = MODEPRCS_OPEN_BYKEY_COMPLETE_CHECK;
				}
			}
			gbJigTimer1s = 60;			
			JigTxAckSetting(TEST_JIG_ACK_MOTOR, gbJigDataBuf, 0); 
			break;	


		case TEST_JIG_REQ_RIGHTLEFT:
			gbJigTimer1s = 60;
			JigInitialWithKeepPincode();
			JigSetHistroy(MP_N1_HISTORY);
			break;

		case TEST_JIG_REQ_MEMACCESS:
			gbJigTimer1s = 60;
			JigMemAccessAreaCheck();
			break;

		case TEST_JIG_REQ_MEMREADWRITE:
			gbJigTimer1s = 60;
			JigMemAccessAreaReadWrite(gbPackRxDataBuf);
#ifdef	DDL_CFG_RFID
			CardOpenUidSupportedInfoLoad();
#endif 

#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
			LanguageLoad();
#else
#ifdef	P_VOICE_RST
			LanguageLoad();
#endif 			
#endif
			break;

		case TEST_JIG_REQ_JIGHISTORY:
			gbJigTimer1s = 60;
			JigHistroyload();
			JigTxAckSetting(TEST_JIG_ACK_JIGHISTORY, gbJigDataBuf, 8); 
			break;

		case TEST_JIG_REQ_LOCKFUNCTIONS:
			gbJigTimer1s = 60;
			LockFunctionsLoad(&gbJigDataBuf[0]);
			JigTxAckSetting(TEST_JIG_ACK_LOCKFUNCTIONS, gbJigDataBuf, 16); 
			break;

#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
		case TEST_JIG_REQ_CMD_BYPASS:
			gbJigTimer1s = 60;
			JigPacket_Bypass_Process(JIG_BYPASS_TO_INNER_MODULE, gbPackRxDataBuf, (gbPackRxDataBuf[3]+5));
			gBypassDoneCheck = 1;	//PC생산 지그에서 송신 한 Module sleep data 를 수신 했음
			break;	

		case TEST_JIG_ACK_CMD_BYPASS:
			gbJigTimer1s = 60;
			JigPacket_Bypass_Process(JIG_BYPASS_TO_COM_MODULE, gbPackRxDataBuf, (gbPackRxDataBuf[3]+5));				
			break;
#endif

		default:
			break;
		}
	
		memset(gbJigDataBuf, 0xFF, 16);
}


void JigModeEndCheck(void)
{
	switch(gbJigPrcsMode)
	{
		case 0:
			if(gbJigTimer1s)
			{
				gbJigPrcsMode = 1;
			}
			break;

		case 1:
			if(gbJigTimer1s)		break;

			while(1);

		case 2:
			JigRxCmdProcess();
			PackRxDataClear();

			gbJigPrcsMode--;
			break;

		default:
			gbJigPrcsMode = 0;
			break;			
	}
}


void JigModeStart(void)
{
	gbJigTimer1s = 60;

	memset(gbJigInputDataBuf, 0xFF, 9);

	TamperCountClear();
	
//	FeedbackJigModeEntering();
}


BYTE PowerDownModeForJig(void)
{
	if(gbJigPowerDownModeEnable)	return (STATUS_SUCCESS);

	return (STATUS_FAIL);
}

void PowerDownModeForJigClear(void)
{
	gbJigPowerDownModeEnable = 0;
}


// 해당 함수는 첫번째 User Code를 유지한 상태에서 나머지 User Code는 모두 삭제하도록 하기 위한 함수
// 초기화 생산지그팩을 사용할 경우 PCB 업체에서 등록한 User Code가 모두 지워지므로
//	조립 과정에서 이를 확인하기 위해서 PCB 업체에서 등록한 User Code가 지워지지 않도록 
//	좌수/우수 확인 지그를 이용해 해당 기능 테스트 진행
void AllUserCodeDeleteExceptTheFirstOne(void)
{
#if defined (_USE_IREVO_CRYPTO_)
	BYTE bTemp[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	BYTE i = 0x00;

	EncryptDecryptKey(bTemp, 8);

	for( i = 0 ; i < (SUPPORTED_USERCODE_NUMBER-1) ; i++)
	{
		RomWrite(bTemp,USER_CODE+8,8);
	}
#else 
	RomWriteWithSameData(0xFF, USER_CODE+8, ((SUPPORTED_USERCODE_NUMBER-1)*8));
#endif 

	// Status 삭제 필요
	RomWriteWithSameData(USER_STATUS_AVAILABLE, USER_STATUS+1, (SUPPORTED_USERCODE_NUMBER-1));

	// Schedule status 영역 삭제 
	RomWriteWithSameData(SCHEDULE_STATUS_DISABLE, USER_SCH_STA+1, (30U-1)); 
	
	// Schedule 영역 삭제 
	RomWriteWithSameData(0xFF, USER_SCHEDULE+4, ((30U-1)*4)); 
}


// 해당 함수에서는 등록된 기본 Master Code 또는 첫번째 User Code는 삭제하지 않고, 
//	온도 센서 확인도 하지 않음.
void JigInitialWithKeepPincode(void)
{
	gbJigTimer1s = 60;

	if(gbManageMode == _AD_MODE_SET)
	{
		// Advanced Mode가 기본이면, Master Code를 삭제하지 않음.
		OnetimeCodeDelete();
		AllUserCodeDelete();
	}
	else
	{
		// Normal Mode가 기본이면, 첫번째 User Code를 삭제하지 않음.
		MasterCodeDelete();
		OnetimeCodeDelete();
		AllUserCodeDeleteExceptTheFirstOne();
	}

#ifdef 	DDL_CFG_RFID
	AllCardDelete();
#endif	

#ifdef 	_MASTER_CARD_SUPPORT
	AllMasterCardDelete();
#endif	

//	AllScheduleStatusClear(CLEAR_ALL_CODE);

#if defined (DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
	AllDS1972TouchKeyDelete();
#endif 

#ifdef 	DDL_CFG_IBUTTON
	AllGMTouchKeyDelete();
#endif 

	LockWorkingModeDefaultSet();
	
#ifdef	LOCKSET_HANDING_LOCK
//	HandingLockDefaultSet();
	HandingLockDefaultToggle();
#endif

	gbDDLStateData = 0x00;							
	RomWrite(&gbDDLStateData, (WORD)DDL_STATE, 1);			

	TamperCountClear();

#ifndef		P_SNS_LOCK_T	
	InnerForcedLockClear();
#endif
	VolumeSetting(VOL_HIGH);

#ifdef	DDL_CFG_RFID
#ifdef	IREVO_CARD_ONLY
	CardOpenUidSupportedInfoSave(0xFF);
#else
	CardOpenUidSupportedInfoSave(0x74);
#endif
#endif 

	JigTxAckSetting(TEST_JIG_ACK_RIGHTLEFT, gbJigDataBuf, 0); 
}


void JigMemAccessAreaCheck(void)
{
	BYTE bDataNum = 1;
	
	gbJigDataBuf[0] = ACCESS_PERMIT_MEMAREA_NO;
	
	if(ACCESS_PERMIT_MEMAREA_NO > 0)
	{
		gbJigDataBuf[1] = (BYTE)(ACCESS_PERMIT_MEMAREA_1_START >> 8);
		gbJigDataBuf[2] = (BYTE)(ACCESS_PERMIT_MEMAREA_1_START & 0xFF);
		gbJigDataBuf[3] = (BYTE)(ACCESS_PERMIT_MEMAREA_1_END >> 8);
		gbJigDataBuf[4] = (BYTE)(ACCESS_PERMIT_MEMAREA_1_END & 0xFF);

		bDataNum = 5;
	}

	if(ACCESS_PERMIT_MEMAREA_NO > 1)
	{
		gbJigDataBuf[5] = (BYTE)(ACCESS_PERMIT_MEMAREA_2_START >> 8);
		gbJigDataBuf[6] = (BYTE)(ACCESS_PERMIT_MEMAREA_2_START & 0xFF);
		gbJigDataBuf[7] = (BYTE)(ACCESS_PERMIT_MEMAREA_2_END >> 8);
		gbJigDataBuf[8] = (BYTE)(ACCESS_PERMIT_MEMAREA_2_END & 0xFF);

		bDataNum = 9;
	}
	
	JigTxAckSetting(TEST_JIG_ACK_MEMACCESS, gbJigDataBuf, bDataNum); 
}


void JigMemAccessAreaReadWrite(BYTE *pDataBuf)
{
	WORD AreaAddrs;

	if(pDataBuf[5] > (MAX_PACK_BUF_SIZE-6))
	{
		// Length 오류
		gbJigDataBuf[0] = 0x03;
		JigTxAckSetting(TEST_JIG_ACK_MEMREADWRITE, gbJigDataBuf, 1); 
		return;
	}

	AreaAddrs = (WORD)pDataBuf[6] << 8;
	AreaAddrs |= (WORD)pDataBuf[7];

	if(ACCESS_PERMIT_MEMAREA_NO == 0)
	{
		// 접근 범위 오류
		gbJigDataBuf[0] = 0x02;
		JigTxAckSetting(TEST_JIG_ACK_MEMREADWRITE, gbJigDataBuf, 1); 
		return;
	}
	else if(ACCESS_PERMIT_MEMAREA_NO == 1)
	{
		if(!((AreaAddrs >= ACCESS_PERMIT_MEMAREA_1_START) && (AreaAddrs <= ACCESS_PERMIT_MEMAREA_1_END))) 
			
		{
			// 접근 범위 오류
			gbJigDataBuf[0] = 0x02;
			JigTxAckSetting(TEST_JIG_ACK_MEMREADWRITE, gbJigDataBuf, 1); 
			return;
		}
	}
	else if(ACCESS_PERMIT_MEMAREA_NO == 2)
	{
		if(!(((AreaAddrs >= ACCESS_PERMIT_MEMAREA_1_START) && (AreaAddrs <= ACCESS_PERMIT_MEMAREA_1_END))
			|| ((AreaAddrs >= ACCESS_PERMIT_MEMAREA_2_START) && (AreaAddrs <= ACCESS_PERMIT_MEMAREA_2_END)))) 
			
		{
			// 접근 범위 오류
			gbJigDataBuf[0] = 0x02;
			JigTxAckSetting(TEST_JIG_ACK_MEMREADWRITE, gbJigDataBuf, 1); 
			return;
		}
	}
	else
	{
		// 기타 오류
		gbJigDataBuf[0] = 0x03;
		JigTxAckSetting(TEST_JIG_ACK_MEMREADWRITE, gbJigDataBuf, 1); 
	}
		

	if(pDataBuf[4] == 0x00)
	{
		// Memory Read
		RomRead(&gbJigDataBuf[4], AreaAddrs, pDataBuf[5]);

		gbJigDataBuf[0] = 0x00;
		gbJigDataBuf[1] = pDataBuf[5];
		gbJigDataBuf[2] = (BYTE)(AreaAddrs >> 8);
		gbJigDataBuf[3] = (BYTE)(AreaAddrs & 0xFF);
		JigTxAckSetting(TEST_JIG_ACK_MEMREADWRITE, gbJigDataBuf, (pDataBuf[5]+4)); 
	}
	else if(pDataBuf[4] == 0x01)
	{
		if(AreaAddrs == LANG_MODE)
		{	
			pDataBuf[8] |= (pDataBuf[8] << 4);
		}
		// Memory Write
		RomWrite(&pDataBuf[8], AreaAddrs, pDataBuf[5]);

		gbJigDataBuf[0] = 0x01;
		gbJigDataBuf[1] = pDataBuf[5];
		gbJigDataBuf[2] = (BYTE)(AreaAddrs >> 8);
		gbJigDataBuf[3] = (BYTE)(AreaAddrs & 0xFF);
		JigTxAckSetting(TEST_JIG_ACK_MEMREADWRITE, gbJigDataBuf, 4); 
		
#if defined (DDL_CFG_MS)
		if(AreaAddrs == MS_MANAGER_CARD_INFO)
		{
			MS_Get_Credential_Info();
			if(Ms_Credential_Info.bCredential_Info_Set_Complete != 0x00)
			{
				MS_All_Credential_Init();
				FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			}
			else
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			}
		}
#endif 
		
	}
	else
	{
		// 기타 오류
		gbJigDataBuf[0] = 0x03;
		JigTxAckSetting(TEST_JIG_ACK_MEMREADWRITE, gbJigDataBuf, 1); 
	}
}


// TYPE
#define	LF_TYPE_DIV_APAC		0x00	

//BUTTON
#define	LF_BTN1_KEYPAD_TOUCH	0x80	
#define	LF_BTN1_KEYPAD_PUSH		0x40
#define	LF_BTN2_MUTE			0x08
#define	LF_BTN2_TOUCH_OC		0x04
#define	LF_BTN2_OC				0x02
#define	LF_BTN2_REG				0x01

// SENSOR
#define	LF_SNS1_CLUTCH_OFF_1	0x80
#define	LF_SNS1_CLUTCH_ON_1		0x40
#define	LF_SNS1_CLUTCH_OFF_2	0x20
#define	LF_SNS1_CLUTCH_ON_2		0x10
#define	LF_SNS2_UNLOCK_2		0x08
#define	LF_SNS2_LOCK_2			0x04
#define	LF_SNS2_UNLOCK_1		0x02
#define	LF_SNS2_LOCK_1			0x01
#define	LF_SNS3_MOTOR_1			0x80
#define	LF_SNS3_MOTOR_2			0x40
#define	LF_SNS3_HANDLE_2		0x04
#define	LF_SNS3_HANDLE_1		0x02
#define	LF_SNS3_DOOR			0x01

// MODE
#define	LF_MOD_ADVANCED			0x02
#define	LF_MOD_NORMAL			0x01

// CREDENTIAL
#define	LF_CRED2_IBUTTON		0x08
#define	LF_CRED2_FINGER			0x04
#define	LF_CRED2_CARD			0x02
#define	LF_CRED2_PINCODE		0x01

// CONFIGURATION
#define	LF_CONFIG4_TOUCH_OC_EN	0x80
#define	LF_CONFIG4_HW_HANDING	0x40
#define	LF_CONFIG4_SW_HANDING	0x20
#define	LF_CONFIG4_VOICE		0x10
#define	LF_CONFIG4_HW_VOLUME	0x08
#define	LF_CONFIG4_SW_VOLUME	0x04
#define	LF_CONFIG4_HW_AUTOLOCK	0x02
#define	LF_CONFIG4_SW_AUTOLOCK	0x01



void JigLockFunctions_Button(BYTE *bpData)
{
#ifdef	DDL_CFG_TOUCHKEY
	*bpData |= LF_BTN1_KEYPAD_TOUCH;
#endif

#ifdef	DDL_CFG_PUSHKEY
	*bpData |= LF_BTN1_KEYPAD_PUSH;
#endif

	bpData++;
#ifdef	P_MUTE_T
	*bpData |= LF_BTN2_MUTE;
#endif
			
#ifdef	P_TOUCH_O_C_T
	*bpData |= LF_BTN2_TOUCH_OC;
#endif
		
#if defined	(P_SW_O_C_T) && !defined	(DDL_CFG_DISABLE_OC)
	*bpData |= LF_BTN2_OC;
#endif

#ifdef	P_SW_REG_T
	*bpData |= LF_BTN2_REG;
#endif
}

void JigLockFunctions_Sensor(BYTE *bpData)
{
#if defined	(DDL_CFG_RIM_MOTOR) || defined (DDL_CFG_L_MECHA) || defined (DDL_CFG_ROOMLOCKCLUTCH)
	*bpData = 0;

	bpData++;
#ifdef	P_SNS_OPEN_T
	*bpData |= LF_SNS2_UNLOCK_1;
#endif

#ifdef	P_SNS_CLOSE_T
	*bpData |= LF_SNS2_LOCK_1;
#endif

	bpData++;
	*bpData |= LF_SNS3_MOTOR_1;

#ifdef	P_SNS_EDGE_T
	*bpData |= LF_SNS3_DOOR;
#endif

#endif	// DDL_CFG_RIM_MOTOR


#ifdef	DDL_CFG_DEADBOLT
	*bpData = 0;

	bpData++;
#ifdef	P_SNS_OPEN_T
	*bpData |= LF_SNS2_UNLOCK_1;
#endif

#ifdef	P_SNS_CLOSE1_T
	*bpData |= LF_SNS2_LOCK_1;
#endif

#ifdef	P_SNS_CLOSE2_T
	*bpData |= LF_SNS2_LOCK_2;
#endif

	bpData++;
	*bpData |= LF_SNS3_MOTOR_1;

#ifdef	P_SNS_EDGE_T
	*bpData |= LF_SNS3_DOOR;
#endif

#endif	// DDL_CFG_DEADBOLT
}


void JigLockFunctions_Mode(BYTE *bpData)
{
#if	defined (DDL_CFG_NORMAL_DEFAULT) || defined (DDL_CFG_ADVANCED_DEFAULT)
	*bpData |= LF_MOD_ADVANCED | LF_MOD_NORMAL;
#endif

#ifdef	DDL_CFG_NORMAL_ONLY
	*bpData |= LF_MOD_NORMAL;
#endif

#ifdef	DDL_CFG_ADVANCED_ONLY
	*bpData |= LF_MOD_ADVANCED;
#endif
}


void JigLockFunctions_Credential(BYTE *bpData)
{
	*bpData = 0;

	bpData++;
#if defined (DDL_CFG_IBUTTON) || defined (DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
	*bpData |= LF_CRED2_IBUTTON;
#endif

#ifdef	DDL_CFG_FP
	*bpData |= LF_CRED2_FINGER;
#endif

#ifdef	DDL_CFG_RFID
	*bpData |= LF_CRED2_CARD;
#endif

#if defined (DDL_CFG_TOUCHKEY) || defined (DDL_CFG_PUSHKEY)
	*bpData |= LF_CRED2_PINCODE;
#endif
}


void JigLockFunctions_Configuration(BYTE *bpData)
{
	*bpData = 0;

	bpData++;
	*bpData = 0;

	bpData++;
	*bpData = 0;

	bpData++;
#ifdef 	DDL_CFG_TOUCH_OC
	*bpData |= LF_CONFIG4_TOUCH_OC_EN;
#endif

#ifdef	P_SNS_LEFT_RIGHT_T
	*bpData |= LF_CONFIG4_HW_HANDING;
#endif

#ifdef	 LOCKSET_HANDING_LOCK
	*bpData |= LF_CONFIG4_SW_HANDING;
#endif

#ifdef	 LOCKSET_LANGUAGE_SET
	*bpData |= LF_CONFIG4_VOICE;
#endif

#ifdef	 LOCKSET_VOLUME_SET
	*bpData |= LF_CONFIG4_SW_VOLUME;
#else
	*bpData |= LF_CONFIG4_HW_VOLUME;
#endif


#ifdef	 LOCKSET_AUTO_RELOCK
	*bpData |= LF_CONFIG4_SW_AUTOLOCK;
#else
	*bpData |= LF_CONFIG4_HW_AUTOLOCK;
#endif
}



void LockFunctionsLoad(BYTE *bpData)
{
	memset(bpData, 0, 16);

	// TYPE
	bpData[0] = LF_TYPE_DIV_APAC;

	//BUTTON
	JigLockFunctions_Button(&bpData[1]);
	// SENSOR
	JigLockFunctions_Sensor(&bpData[3]);
	// MODE
	JigLockFunctions_Mode(&bpData[6]);
	// CREDENTIAL
	JigLockFunctions_Credential(&bpData[7]);
	// CONFIGURATION
	JigLockFunctions_Configuration(&bpData[9]);
}



#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
BYTE JigPacket_Bypass_Check(BYTE* bpDesBuf, BYTE* bSrcBuf, BYTE bRxCount, BYTE bInputData)
{
	static BYTE bJigBypassPrcsStep = 0; 
	static BYTE bRxAddDataLen = 0;

	if((!gbJigTimer1s) || (bJigBypassPrcsStep != bRxCount)) 
	{
		bRxAddDataLen = 0;
		bJigBypassPrcsStep = 0;
		return 0;
	}

	switch(bJigBypassPrcsStep)
	{
		case 0:
			if(bInputData != TEST_FRAME_STX)
			{
				bRxAddDataLen = 0;
				bJigBypassPrcsStep = 0;
				return 0;
			}
			break;

		case 1: 		
			if(bInputData != TEST_CLASS_MAFA_JIG)
			{
				bRxAddDataLen = 0;
				bJigBypassPrcsStep = 0;
				return 0;
			}
			break;
	
		case 2:
			break;	

		case 3: 		
			bRxAddDataLen = bInputData;
			break;
	
		default:
			if(bRxCount == (bRxAddDataLen+4))
			{
				bRxAddDataLen = 0;
				bJigBypassPrcsStep = 0;

				if(bInputData != TEST_FRAME_ETX)
				{
					return 0;
				}

				memcpy(bpDesBuf, bSrcBuf, (bRxCount+1));

				gbJigPrcsMode = 2;

				return 1;
			}
			break;
	}
	
	bJigBypassPrcsStep++;
	return 1;
}



void JigPacket_Bypass_Process(BYTE bDirection, BYTE* bpData, BYTE bLength)
{
	if(bDirection == JIG_BYPASS_TO_INNER_MODULE)
	{
		memcpy(gbInnerPackTxDataBuf, bpData, bLength);
		gbInnerPackTxByteNum = bLength;

		if((!gfDirectSending) &&	!InnerPackTx_QueueIsFull())
		{
			InnerPackTx_EnQueue();
		}
	}
	else if(bDirection == JIG_BYPASS_TO_COM_MODULE)
	{
		memcpy(gbPackTxDataBuf, bpData, bLength);
		gbPackTxByteNum = bLength;
		
		UartSendCommPack(gbPackTxDataBuf, gbPackTxByteNum);
	}
	else{}
}
#endif




