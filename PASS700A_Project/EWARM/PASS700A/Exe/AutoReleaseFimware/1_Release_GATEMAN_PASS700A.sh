#!/bin/bash


#1 开始自动编译
StartTime=$(date +%Y-%m-%d\ %H:%M:%S)
echo "------------------ Start auto build $StartTime ------------------"


#2 自动拉取最新代码。注：如支持此功能，请将SSH key的id_rsa文件放到 home\.ssh目录下。
#git.exe fetch --progress --prune --recurse-submodules=no origin 



#3 为了防止ddl_config.h文件被修改，导致以下宏定义运行出错，必须先丢弃已经修改的部分，恢复成默认状态。
#git.exe checkout -- "../../../../DDL_Source/ddl_config.h"
#选用#define __DDL__GATEMAN_PASS700A
sed -i '/'#define\ \_\_DDL\_\_GATEMAN\_PASS700A'/c'#define\ \_\_DDL\_\_GATEMAN\_PASS700A'' ../../../../DDL_Driver/Source/config_pin/ddl_config.h


#4 获取版本号
MajorVer=`sed -n '/define\ PROGRAM\_VERSION\_MAJOR/p' ../../../../DDL_Driver/Source/config_pin/ddl_config-pass700a.h  | awk -F" " '{print $3}'`
SubVer=`sed -n '/define\ PROGRAM\_VERSION\_MINOR/p' ../../../../DDL_Driver/Source/config_pin/ddl_config-pass700a.h  | awk -F" " '{print $3}'`
#${变量名:起始:长度}得到子字符串
MajorVer1=${MajorVer:0:1}
MajorVer2=${MajorVer:1:1}
Vesrion=$MajorVer1"."$MajorVer2"."$SubVer



echo "------------------PASS700A build start------------------"
#5 自动编译第一个产品
./IAR_build.bat



#6 处理第一个产品的固件
DateTime=$(date +%Y%m%d%H%M%S)
Path="Release_PASS700A_"$Vesrion"_"$DateTime
FirmwarePath=$Vesrion"_"$DateTime

mkdir $Path
cd $Path 
mkdir $FirmwarePath
cd  $FirmwarePath
cp ../../../Output/HB_GATEMAN_PASS700A_P.bin ./"HB_GATEMAN_PASS700A_P_"$Vesrion"_"$DateTime.bin
cp ../../../Output/OTA_GATEMAN_PASS700A_P.bin ./"OTA_GATEMAN_PASS700A_P_"$Vesrion"_"$DateTime.bin
cd ../../
echo "------------------PASS700A build end------------------"



#7 结束自动编译
EndTime=$(date +%Y-%m-%d\ %H:%M:%S)
echo "------------------ End auto build $EndTime ------------------"



exit





