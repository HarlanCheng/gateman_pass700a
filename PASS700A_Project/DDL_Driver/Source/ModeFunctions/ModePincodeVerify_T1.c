//------------------------------------------------------------------------------
/** 	@file		ModePincodeVerify_T1.c
	@version 0.1.00
	@date	2016.04.19
	@brief	Pincode Verify Mode 
	@remark	 입력한 Pincode를 저장된 Pincode와 비교하여 처리하는 모드
	@see	MainModeProcess_T1.c
	@see	PincodeFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.19		by Jay
			- 신규 UI에 따른 처리
			[입력한 Pincode 처리 단계] 
			1.1회용 무음 모드 진입 여부 확인
			2.입력한 Pincode를 저장된 Pincode와 비교
			3.일치하는 Pincode의 Schedule 확인
			4.일치하는 Pincode에 의한 모터 열림
			5.외부강제잠금 설정

		V0.1.01 2016.04.20		by Jay
			- 외부강제잠금 부분 정리 (카드 처리 함수와 공용 사용을 위해)
			- Onetime Pincode 인증 부분 추가
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gPincodeVerifyMode = 0;			/**< Pincode Verify 실행 모드  */
BYTE gOutForcedLockSettingCnt = 0;		/**< 외부강제잠금 설정 진입 Count 변수  */
BYTE gbLockOperationbyAccessoryAckAccess = 0; //keypad Accessory에 의한 모드 진입을 판단 하기 위한 변수, ACK를 송신 하기 위함


//------------------------------------------------------------------------------
/** 	@brief	Pincode Verify Mode Start
	@param	None
	@return 	None
	@remark 비밀번호 인증 모드 시작
*/
//------------------------------------------------------------------------------
void ModeGotoPINVerify(void)
{
	gbMainMode = MODE_PIN_VERIFY;
	gbModePrcsStep = 0;

	gPincodeVerifyMode = PINCODEMODE_ONETIMESILENT_CHECK;
}	


//------------------------------------------------------------------------------
/** 	@brief	Pincode Verify Step Start in Mode
	@param	None
	@return 	None
	@remark 입력된 비밀번호 비교 시작
*/
//------------------------------------------------------------------------------
void PincodeModeGotoVerifyTenKeySetp(void)
{
	gbMainMode = MODE_PIN_VERIFY;
	gbModePrcsStep = MODEPRCS_VERIFY_TENKEY_START;		

	gPincodeVerifyMode = PINCODEMODE_VERIFYTENKEY_STEP;

#ifdef	BLE_N_SUPPORT
	if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)
	{
		BLENTxStartScanSend(0x00);
	}
#endif
}


//------------------------------------------------------------------------------
/** 	@brief	Schedule Check Start by verified Pincode
	@param	None
	@return 	None
	@remark 일치된 비밀번호의 Schedule 확인 시작
*/
//------------------------------------------------------------------------------
void PincodeModeGotoVerifyScheduleSetp(void)
{
	gbMainMode = MODE_PIN_VERIFY;
	gbModePrcsStep = MODEPRCS_SCHEDULE_CHECK;

	gPincodeVerifyMode = PINCODEMODE_VERIFYSCHEDULE_STEP;
}




//------------------------------------------------------------------------------
/** 	@brief	Motor Open Start by verified Pincode
	@param	None
	@return 	None
	@remark 인증이 완료된 비밀번호에 의한 모터 열림 시작
*/
//------------------------------------------------------------------------------
void PincodeModeGotoOpenProcessByPincode(void)
{
	gbMainMode = MODE_PIN_VERIFY;
	gbModePrcsStep = MODEPRCS_OPEN_START_BYPINCODE;

	gPincodeVerifyMode = PINCODEMODE_OPENPROCESS_STEP;
}


//------------------------------------------------------------------------------
/** 	@brief	Out Forced Lock Setting by verified Pincode
	@param	None
	@return 	None
	@remark 인증이 완료된 비밀번호에 의한 외부강제잠금 설정 시작
*/
//------------------------------------------------------------------------------
void PincodeModeGotoOutForcedLockSettingByKey(void)
{
	gbMainMode = MODE_PIN_VERIFY;
	gbModePrcsStep = MODEPRCS_LOCKOUTINIT_BY_PIN;

	gPincodeVerifyMode = PINCODEMODE_OUTFORCEDLOCKSET_STEP;
}



//------------------------------------------------------------------------------
/** 	@brief	Pincode Verify Mode Clear
	@param	None
	@return 	None
	@remark 비밀번호 인증 모드 종료
*/
//------------------------------------------------------------------------------
void PincodeVerifyModeClear(void)
{
	gPincodeVerifyMode = 0;
	ModeClear();
}



//============================================================
// 1회용 무음 모드 진입 여부 확인 단계
//============================================================


BYTE gfMute = 0;

#define	 _MODE_MULTI_TIME_OUT		25

//------------------------------------------------------------------------------
/** 	@brief	Onetime Silent Mode Check
	@param	None
	@return 	None
	@remark 멀티 터치에 의한 1회용 무음 모드 진입 여부 확인
*/
//------------------------------------------------------------------------------
void OnetimeSilentModeCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOnLedOnly();

			if(AlarmStatusCheck() == STATUS_SUCCESS)
			{
#ifdef	DDL_CFG_RFID
				CardGotoReadStart();
#endif			
				PincodeModeGotoVerifyTenKeySetp();
				SetModeTimeOut(_MODE_TIME_OUT_7S);
				break;
			}

#ifdef	DDL_CFG_ONETIME_MUTE
			gbModePrcsStep = MODEPRCS_MULTI_CHECK;
			SetModeTimeOut(_MODE_MULTI_TIME_OUT);
#else

#ifdef	DDL_CFG_RFID
			CardGotoReadStart();
#endif			
			PincodeModeGotoVerifyTenKeySetp();
			SetModeTimeOut(_MODE_TIME_OUT_7S);
#endif
			break;					
			
		case MODEPRCS_MULTI_CHECK:
			if((gbNewKeyBuffer == TENKEY_MULTI))
			{
				if(GetModeTimeOut())	break;

				gfMute = 1;

				FeedbackKeyPadLedOnLedOnly();
			}

#ifdef	DDL_CFG_RFID
			CardGotoReadStart();
#endif			
			PincodeModeGotoVerifyTenKeySetp();
			SetModeTimeOut(_MODE_TIME_OUT_7S);
			break;

		default:
			PincodeVerifyModeClear();
			break;
	}

}





//============================================================
// 입력한 Pincode를 저장된 Pincode와 비교하는 단계
//	- 모터 열림을 위한 인증 처리
//	- 외부강제잠금 설정을 위한 인증 처리
//	- 메뉴 모드 진입을 위한 인증 처리
//============================================================


void TempSaveInputTenKey(void);
#ifdef	DDL_CFG_RFID
void StartCardVerifyModeCheck(void);
#endif
void VerifiedProcessByPincode(void);
void StartOutForcedLockSettingModeCheck(void);
void StartMenuModeCheck(void);

//------------------------------------------------------------------------------
/** 	@brief	Verify Input Pincode
	@param	None
	@return 	None
	@remark 입력된 비밀번호가 등록된 비밀번호와 같은지 비교
*/
//------------------------------------------------------------------------------
void VerifyInputPincodeProcessSetp(void)
{
	switch(gbModePrcsStep)
	{
		case MODEPRCS_VERIFY_TENKEY_START:
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
			if((MotorSensorCheck() & SENSOR_LOCK_STATE) == 0x00) //내부 강제 잠금 상태가 아니면 normal operation 
			{
				FeedbackKeyPadLedOnBuzOnly(VOICE_MIDI_INTO, VOL_CHECK|MANNER_CHK); 				
			}
#else 
			FeedbackKeyPadLedOnBuzOnly(VOICE_MIDI_INTO, VOL_CHECK|MANNER_CHK); 
#endif 


			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_7S);

			gbModePrcsStep = MODEPRCS_VERIFY_TENKEY_TEMPSAVE;
			break;

		case MODEPRCS_VERIFY_TENKEY_TEMPSAVE:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
#ifdef	BLE_N_SUPPORT
					if((GetInputKeyCount() == 0) && (gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN))
					{
						BLENTxStopScanSend();
					}
#endif
					TempSaveInputTenKey();
					break;
			
				case TENKEY_NONE:
#ifdef	DDL_CFG_RFID
					StartCardVerifyModeCheck();
#endif

#ifdef 	DDL_CFG_IBUTTON
					GMTouchKeyDetectionProcess();
#endif

#ifdef 	DDL_CFG_DS1972_IBUTTON
					DS1972TouchKeyDetectionProcess();
#endif 		
					TimeExpiredCheck();
					break;
			
				case TENKEY_STAR:
				case TENKEY_MULTI:
#ifdef	DDL_CFG_RFID
					CardGotoReadStop();
#endif			

					JigInputDataSave(0x01, gPinInputKeyFromBeginBuf, (MAX_PIN_LENGTH>>1));

					VerifiedProcessByPincode();
					break;
			
				case TENKEY_SHARP:
#ifdef	DDL_CFG_RFID
					CardGotoReadStop();
#endif			
					StartOutForcedLockSettingModeCheck();
					break;
			
				case FUNKEY_REG:
#ifdef	DDL_CFG_RFID
					CardGotoReadStop();
#endif			

#if defined (DDL_CFG_MS)
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					PincodeVerifyModeClear();
#else 
					StartMenuModeCheck();
#endif 
					break;
			
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					PincodeVerifyModeClear();
					break;
			}
			break;

		default:
			PincodeVerifyModeClear();
			break;
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Temporary TenKey Save
	@param	None
	@return 	None
	@remark 입력된 비밀번호 임시 저장
*/
//------------------------------------------------------------------------------
void TempSaveInputTenKey(void)
{
	BYTE bFakeEnable;
	
	FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK|MANNER_CHK, gbInputKeyValue);

#if defined (DDL_CFG_MS)
	bFakeEnable = 0;
#else 
	bFakeEnable = (gbManageMode == _AD_MODE_SET)? 0:1;									
#endif 

	if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, bFakeEnable) == TENKEY_INPUT_OVER)
	{
		TamperCountIncrease();
//		FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		PincodeVerifyModeClear();
		return;
	}

	SetModeTimeOut(_MODE_TIME_OUT_7S);
}


//------------------------------------------------------------------------------
/** 	@brief	Card Input Check
	@param	None
	@return 	None
	@remark 카드 입력이 있는지 확인
*/
//------------------------------------------------------------------------------
#ifdef	DDL_CFG_RFID
void StartCardVerifyModeCheck(void)
{
	if(GetCardReadStatus() == CARDREAD_SUCCESS)
	{
		JigInputDataSave(0x00, gCardAllUidBuf[0], MAX_CARD_UID_SIZE);

		ModeGotoInputCardVerify();
	}
	
	CardReadStatusClear();
}
#endif

void ProcessOpenByOnetimeCode(void);

//------------------------------------------------------------------------------
/** 	@brief	Pincode Verify Result
	@param	None
	@return 	None
	@remark 입력된 Pincode와 등록된 Pincode를 비교하여 결과 처리
*/
//------------------------------------------------------------------------------
void VerifiedProcessByPincode(void)
{
	BYTE bTmp;

	PCErrorClearSet();

#if defined (DDL_CFG_MS)
	bTmp = PincodeVerify(0);

	if(!MS_GetSupported_Authority(Ms_Credential_Info.bVerifiedType,MOTOR_OPEN_ALLOW)) 
	{
		/* 문여는 권한 없으면 못열게 */
		if(bTmp != RET_NO_INPUT)
			bTmp = RET_NO_MATCH;
	}

#else 
	bTmp = PincodeVerify(1);
#endif 	

	switch(bTmp)
	{
		case RET_NO_INPUT:
			FeedbackBuzStopLedDimOff();
			PincodeVerifyModeClear();
			if(gbLockOperationbyAccessoryAckAccess)	//keypad Accessory에 의한 모드 진입인 경우 ACK를 송신 한다.
			{
				gbLockOperationbyAccessoryAckAccess = 0;
				PackTxLockOperationbyAccessoryAckSend(0x00, 0x02);
			}
			break;
			
		case RET_WRONG_DIGIT_INPUT:
			TamperCountIncrease();
//			FeedbackError(105, VOL_CHECK);
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);
			PincodeVerifyModeClear();
			if(gbLockOperationbyAccessoryAckAccess)	//keypad Accessory에 의한 모드 진입인 경우 ACK를 송신 한다.
			{
				gbLockOperationbyAccessoryAckAccess = 0;
				PackTxLockOperationbyAccessoryAckSend(0x00, 0x02);
			}
			break;

		case RET_NO_MATCH:
			TamperCountIncrease();
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			PincodeVerifyModeClear();
			if(gbLockOperationbyAccessoryAckAccess)	//keypad Accessory에 의한 모드 진입인 경우 ACK를 송신 한다.
			{
				gbLockOperationbyAccessoryAckAccess = 0;
				PackTxLockOperationbyAccessoryAckSend(0x00, 0x02);
			}
			break;
	
		case RET_TEMP_CODE_MATCH:
			if(gbLockOperationbyAccessoryAckAccess)	//keypad Accessory에 의한 모드 진입인 경우 ACK를 송신 한다.
			{
				gbLockOperationbyAccessoryAckAccess = 0;
				PackTxLockOperationbyAccessoryAckSend(0x00, 0x01);
			}
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER)
			// 마스터 카드가 아닌 비밀번호는 내부강제잠금 상태에서 문 열림 안됨
			bTmp = MotorSensorCheck();
			switch(bTmp)
			{
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CENTERLOCK_STATE:
				case SENSOR_LOCK_STATE:
					PackAlarmReportInnerForcedLockOpenCloseFailSend();
					FeedbackLockIn();
					PincodeVerifyModeClear();
					return;
			
				default:
					break;
			}
#endif	// DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER

#ifdef	BLE_N_SUPPORT
			/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */ 
			if(AllLockOutStatusCheck(bTmp))
			{
				FeedbackAllCodeLockOut();

				ModeClear();
				break;
			}
#endif
			ProcessOpenByOnetimeCode();
			break;
	
		case RET_MASTER_CODE_MATCH:
			if(gbLockOperationbyAccessoryAckAccess)	//keypad Accessory에 의한 모드 진입인 경우 ACK를 송신 한다.
			{
				gbLockOperationbyAccessoryAckAccess = 0;
				PackTxLockOperationbyAccessoryAckSend(0x00, 0x01);
			}
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER)
			// 마스터 카드가 아닌 비밀번호는 내부강제잠금 상태에서 문 열림 안됨
			bTmp = MotorSensorCheck();
			switch(bTmp)
			{
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CENTERLOCK_STATE:
				case SENSOR_LOCK_STATE:
					PackAlarmReportInnerForcedLockOpenCloseFailSend();
					FeedbackLockIn();
					PincodeVerifyModeClear();
					return;
			
				default:
					break;
			}
#endif	// DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER

#ifdef	BLE_N_SUPPORT			
			/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */ 
			if(AllLockOutStatusCheck(bTmp))
			{
				FeedbackAllCodeLockOut();

				ModeClear();
				break;
			}
#endif 
			DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
			TamperCountClear();
			
			PincodeModeGotoOpenProcessByPincode();
			break;

		default:
			if(gbLockOperationbyAccessoryAckAccess)	//keypad Accessory에 의한 모드 진입인 경우 ACK를 송신 한다.
			{
				gbLockOperationbyAccessoryAckAccess = 0;
				PackTxLockOperationbyAccessoryAckSend(0x00, 0x01);
			}
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER)
			// 마스터 카드가 아닌 비밀번호는 내부강제잠금 상태에서 문 열림 안됨
			bTmp = MotorSensorCheck();
			switch(bTmp)
			{
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CENTERLOCK_STATE:
				case SENSOR_LOCK_STATE:
					PackAlarmReportInnerForcedLockOpenCloseFailSend();
					FeedbackLockIn();
					PincodeVerifyModeClear();
					return;
			
				default:
					break;
			}
#endif	// DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER

#ifdef	BLE_N_SUPPORT
			/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */ 
			if(AllLockOutStatusCheck(bTmp))
			{
				FeedbackAllCodeLockOut();

				ModeClear();
				break;
			}
#endif
#if defined (DDL_CFG_MS)
			DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
			TamperCountClear();
			PincodeModeGotoOpenProcessByPincode();
#else 
			PincodeModeGotoVerifyScheduleSetp();
#endif 
			break;
	}		
}



//------------------------------------------------------------------------------
/** 	@brief	Process by One Time Code 
	@param	None
	@return 	None
	@remark 입력된 One Time Code 처리
*/
//------------------------------------------------------------------------------
void ProcessOpenByOnetimeCode(void)
{
//	PackSlotNumberSet(bTmp);

	if(AlarmStatusCheck() == STATUS_SUCCESS)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		PincodeVerifyModeClear();
		return;
	}

	OnetimeCodeDelete();

	// Onetime Code에 의한 문 열림 Event 전송
	
	DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
	TamperCountClear();

	PincodeModeGotoOpenProcessByPincode();
}





//------------------------------------------------------------------------------
/** 	@brief	Verify Input Pincode for Out Forced Lock Setting Process
	@param	None
	@return 	None
	@remark 입력된 Pincode로 외부강제잠금 설정을 위한 인증 과정 
*/
//------------------------------------------------------------------------------
void StartOutForcedLockSettingModeCheck(void)
{
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER)
	BYTE bTmp;
#endif

	if(AlarmStatusCheck() == STATUS_SUCCESS)
	{
#ifdef	DDL_CFG_FP
//지문 모듈 커버거 고장나서 지문 모듈로 인증을 못할 때, 멀티터치로 lock 깨우고 # 터치로 지문 인증 모드로 진입 기능 추가
		if (PincodeVerify(1) == RET_NO_INPUT)
		{
#if  !defined	(DDL_CFG_FP_INTEGRATED_FPM)  && !defined	(DDL_CFG_FP_TS1071M)// Goodix && Biosec 제외 한다. 
			ModeGotoFingerVerify();
			return;
#endif 			
		}
#endif
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		PincodeVerifyModeClear();
		return;
	}
	
	switch(PincodeVerify(1))
	{
		case RET_NO_INPUT:
#if defined	DDL_CFG_FP && !defined (DDL_CFG_FP_INTEGRATED_FPM)  && !defined	(DDL_CFG_FP_TS1071M)  // Goodix 는 제외 한다. 
			//지문 모듈 커버거 고장나서 지문 모듈로 인증을 못할 때, 멀티터치로 lock 깨우고 # 터치로 지문 인증 모드로 진입 기능 추가
			// Jig test 중이면 일반 처리 
			if(JigModeStatusCheck() == STATUS_SUCCESS)
			{
				FeedbackBuzStopLedDimOff();
				PincodeVerifyModeClear();
			}
			else
			{
				ModeGotoFingerVerify();
			}
#else
			FeedbackBuzStopLedDimOff();
			PincodeVerifyModeClear();
#endif
			break;
			
		case RET_WRONG_DIGIT_INPUT:
		case RET_NO_MATCH:
		case RET_TEMP_CODE_MATCH:
			TamperCountIncrease();
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			PincodeVerifyModeClear();
			break;
	
		default:
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER)
			// 마스터 카드가 아닌 비밀번호는 내부강제잠금 상태에서 외부강제잠금 설정 안됨
			bTmp = MotorSensorCheck();
			switch(bTmp)
			{
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CENTERLOCK_STATE:
				case SENSOR_LOCK_STATE:
					PackAlarmReportInnerForcedLockOpenCloseFailSend();
					FeedbackLockIn();
					PincodeVerifyModeClear();
					return;
			
				default:
					break;
			}
#endif	// DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER

//			PackArmRequestSet(bTmp);
	
//20160923 외부강제잠금 시도도 인증된 것이므로 인증오류카운트도 초기화 시킨다.
//20161011 인증이 되었으니 외부강제 잠금 / 허수 사용 여부 전부 초기화 by hyojoon_20161011 
			TamperCountClear();
			DDLStatusFlagClear(DDL_STS_OUTLOCK|DDL_STS_TAMPER_PROOF); 
			PincodeModeGotoOutForcedLockSettingByKey();
			break;
	}		
}




//------------------------------------------------------------------------------
/** 	@brief	Verify Input Pincode for Menu Mode
	@param	None
	@return 	None
	@remark 입력된 Pincode로 메뉴 모드 진입을 위한 인증 과정 
*/
//------------------------------------------------------------------------------
void StartMenuModeCheck(void)
{
	if(AlarmStatusCheck() == STATUS_SUCCESS)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		PincodeVerifyModeClear();
		return;
	}
	
	switch(PincodeVerify(1))
	{
		case RET_MASTER_CODE_MATCH:
			ModeGotoMenuAdvancedMain();
			TamperCountClear();			
			DDLStatusFlagClear(DDL_STS_OUTLOCK |DDL_STS_TAMPER_PROOF);
			break;

		case 1:
			if(gbManageMode == _AD_MODE_SET)	
			{
				TamperCountIncrease();
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				PincodeVerifyModeClear();
				break;
			}

			ModeGotoMenuNormalMain();
			TamperCountClear();
			DDLStatusFlagClear(DDL_STS_OUTLOCK |DDL_STS_TAMPER_PROOF);
			break;
	
		default:
			TamperCountIncrease();
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			PincodeVerifyModeClear();
			break;
	}
}





//============================================================
// 인증된 Pincode의 Schedule이 유효한지 확인하는 단계
//============================================================

void ScheduleEnableCheck(void);
void WaitGetTimeData(void);
void OpenProcessByScheduleVerified(void);

//------------------------------------------------------------------------------
/** 	@brief	Verify Input Pincode for Menu Mode
	@param	None
	@return 	None
	@remark 입력된 Pincode로 메뉴 모드 진입을 위한 인증 과정 
*/
//------------------------------------------------------------------------------
void VerifyScheduleStepForPincodeVerify(void)
{
	switch(gbModePrcsStep)
	{
		case MODEPRCS_SCHEDULE_CHECK:
			ScheduleEnableCheck();
			break;

		//Get time Check
		case MODEPRCS_GET_TIME_CHECK:
			WaitGetTimeData();
			break;
		
		//Schedule Verify
		case MODEPRCS_SCHEDULE_VERIFY:		
			OpenProcessByScheduleVerified();
			break;			

		default:
			PincodeVerifyModeClear();
			break;
	}
}


//------------------------------------------------------------------------------
/** 	@brief 	Schedule Data Check
	@param	None
	@return 	None
	@remark 인증된 Pincode가 Schedule 확인 대상인지 검토
*/
//------------------------------------------------------------------------------
void ScheduleEnableCheck(void)
{
	BYTE bTmp;
	WORD SlotNumber;
	BYTE bData;

	SlotNumber = GetSlotNumberForPack();
	if((SlotNumber == 0) || (SlotNumber > SUPPORTED_USERCODE_NUMBER))
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		PincodeVerifyModeClear();
		return;
	}

	//해당 비밀번호가 LOCKOUT인지 체크
	bData = GetUserStatus(SlotNumber); 
	if(bData != USER_STATUS_OCC_ENABLED)
	{
		TamperCountIncrease();

		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		PincodeVerifyModeClear();
		return;
	}
	
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
	{
		if(ScheduleEnableCheck_Ble30(CREDENTIALTYPE_PINCODE, (SlotNumber-1)) != 0)
		{
			TimeDataClear();

			PackTx_MakePacket(F0_GET_TIME_AND_DATE, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);
			InnerPackTxWaitTime_EnQueue(5); //keypad Accessory에 송신할 ACK와 EVENT 송신 사이의 간격이 짧아 모듈이 수신 못하기 때문에 EVENT송신에 지연을 둠

			// Time Data가 1초 내에 회신되지 않으면 에러 처리
			gModeTimeOutTimer100ms = 10;

			gbModePrcsStep = MODEPRCS_GET_TIME_CHECK;
			return;
		}
	}
	else
#endif	// DDL_CFG_BLE_30_ENABLE
	{
		//해당 비밀번호의 Schedule 적용 설정 여부를 확인
		bData = GetScheduleStatus(SlotNumber); 
		if(bData == SCHEDULE_STATUS_ENABLE)
		{
			TimeDataClear();

			PackTx_MakePacket(EV_GET_TIME, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);

			// Time Data가 1초 내에 회신되지 않으면 에러 처리
			gModeTimeOutTimer100ms = 10;

			gbModePrcsStep = MODEPRCS_GET_TIME_CHECK;
			return;
		}
	}		
	
	DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
	TamperCountClear();

	PincodeModeGotoOpenProcessByPincode();
}


//------------------------------------------------------------------------------
/** 	@brief 	Wait Get Time Data
	@param	None
	@return 	None
	@remark Schedule 확인을 위해 요청한 Time Data 수신 대기
*/
//------------------------------------------------------------------------------
void WaitGetTimeData(void)
{
	if(IsGetTimeDataCompleted() == true)
	{
		gbModePrcsStep = MODEPRCS_SCHEDULE_VERIFY;
	}
	else if(gModeTimeOutTimer100ms == 0)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		PincodeVerifyModeClear();
	}
}


//------------------------------------------------------------------------------
/** 	@brief 	Schedule Check Result Process
	@param	None
	@return 	None
	@remark Schedule 확인한 결과에 따른 처리
*/
//------------------------------------------------------------------------------
void OpenProcessByScheduleVerified(void)
{
	WORD SlotNumber;
			
	SlotNumber = GetSlotNumberForPack();
	if((SlotNumber == 0) || (SlotNumber > SUPPORTED_USERCODE_NUMBER))
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		PincodeVerifyModeClear();
		return;
			}
			
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
	{
		if(ScheduleVerify_Ble30(CREDENTIALTYPE_PINCODE, gbTimeBuff_Ble30, (BYTE)SlotNumber) == 1)
		{
			DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
			TamperCountClear();
		
			PincodeModeGotoOpenProcessByPincode();
		}
		else
		{
			TamperCountIncrease();

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			PincodeVerifyModeClear();
		}
	}
	else
#endif
	{
		if(ScheduleVerify(gbTimeBuff, (BYTE)SlotNumber) == 1)
		{
			DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
			TamperCountClear();
		
			PincodeModeGotoOpenProcessByPincode();
		}
		else
		{
			TamperCountIncrease();

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			PincodeVerifyModeClear();
		}
	}
}




//============================================================
// 인증된 Pincode로 모터 열림 수행하는 단계
//============================================================

void MotorOpenCompleteByPincode(void);
#ifdef	CLOSE_BYPINCODE//비밀번호를  이용한 모터 잠김 정보 송신
void MotorCloseCompleteByPincode(void);
#endif


//------------------------------------------------------------------------------
/** 	@brief 	Motor Open Process by Verified Pincode
	@param	None
	@return 	None
	@remark 인증된 Pincode에 의한 모터 열림 처리
*/
//------------------------------------------------------------------------------
void MotorOpenProcessByPincode(void)
{
	switch(gbModePrcsStep)
	{
		case MODEPRCS_OPEN_START_BYPINCODE:
			if(AlarmStatusCheck() == STATUS_SUCCESS)
			{
				PincodeVerifyModeClear();

				AlarmGotoAlarmClear();
				break;
			}	

#ifdef	CLOSE_BYPINCODE	
//비밀번호 인증이 정상이면 무조건 모터 open이 아니고 센서 값에 따라 모터 동작을 결정 한다.
			BYTE bTemp;
			bTemp = MotorSensorCheck();
			switch(bTemp)
			{					
				case SENSOR_CLOSE_STATE:
				case SENSOR_CLOSECEN_STATE:
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CLOSECENLOCK_STATE:
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorOpen();
#endif		
					StartMotorOpen();
	
					gbModePrcsStep = MODEPRCS_OPEN_BYPINCODE_COMPLETE_CHECK;				
					break;

				case SENSOR_OPEN_STATE:
				case SENSOR_OPENCEN_STATE:
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_OPENCENLOCK_STATE:
				default : //case SENSOR_NOT_STATE, case SENSOR_LOCK_STATE, case SENSOR_CENTER_STATE,case SENSOR_CENTERLOCK_STATE
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorClose();
#endif		
					StartMotorClose();
	
					gbModePrcsStep = MODEPRCS_CLOSE_BYPINCODE_COMPLETE_CHECK;				
					break;	
				
			}			

#else	//CLOSE_BYPINCODE #else
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
			BatteryCheck();
#else
			FeedbackMotorOpen();
#endif		
			StartMotorOpen();

#ifdef USED_SECOND_MOTOR	//FDS , SCREENDOOR의 경우 제어해야 할 모터가 두개 이 기 때문 
			StartMotorOpen_Second();
#endif	//USED_SECOND_MOTOR #endif

	
			gbModePrcsStep = MODEPRCS_OPEN_BYPINCODE_COMPLETE_CHECK;
#endif	//__DDL_MODEL_PANPAN_FC2A #endif		

			break;

		case MODEPRCS_OPEN_BYPINCODE_COMPLETE_CHECK:
			MotorOpenCompleteCheck(MotorOpenCompleteByPincode, MODEPRCS_LOCKOUTSET_ERROR);
			break;

#ifdef	CLOSE_BYPINCODE		//Pincode를 이용한 모터 닫힘 동작 수행후 이후 센서 확인 및 다음 스텝 결정
		case MODEPRCS_CLOSE_BYPINCODE_COMPLETE_CHECK:
			MotorCloseCompleteCheck(MotorCloseCompleteByPincode, MODEPRCS_LOCKOUTSET_ERROR);
			break;
#endif


		case MODEPRCS_LOCKOUTSET_ERROR:
			if(GetLedMode())	break;
			
			if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}

		case MODEPRCS_LOCKOUTSET_END:
		default:
			PincodeVerifyModeClear();
			break;
	}
}


//------------------------------------------------------------------------------
/** 	@brief 	Motor Open Result Check
	@param	[CompleteFunction] : 모터 열림 정상 종료 시 실행 함수
	@param	[ErrorStep] : 모터 열림 실패 시 처리 단계
	@return 	None
	@remark 인증된 Pincode나 Credential에 의한 모터 열림 결과 처리
*/
//------------------------------------------------------------------------------
void MotorOpenCompleteCheck(void (*CompleteFunction)(void), BYTE ErrorStep)
{
	BYTE bTmp;

#ifdef USED_SECOND_MOTOR
	BYTE bTemp;

	bTmp = GetFinalMotorStatus();
	bTemp = GetFinalMotorStatus_Second();
	
	if(bTmp == FINAL_MOTOR_STATE_OPEN_ING || bTemp == FINAL_MOTOR_STATE_OPEN_ING)	return;
#if 0
	if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
	
	if(bTmp == FINAL_MOTOR_STATE_OPEN_ERROR ||bTemp == FINAL_MOTOR_STATE_OPEN_ERROR )
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
	
	// Pack Module로 Jammed Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x01);
	}
	else if(bTmp == FINAL_MOTOR_STATE_OPEN && bTemp == FINAL_MOTOR_STATE_OPEN)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorOpen();// 2018-6-18 moonsungwoo, 배터리체크-모터동작완료-피드백, 순서 변경  
#endif
		CompleteFunction();
		return;
	}

	gbModePrcsStep = ErrorStep;


#else	//USED_SECOND_MOTOR #else
	bTmp = GetFinalMotorStatus();
	
	if(bTmp == FINAL_MOTOR_STATE_OPEN_ING)	return;
#if 0
	if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
	
	if(bTmp == FINAL_MOTOR_STATE_OPEN_ERROR)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
	
	// Pack Module로 Jammed Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x01);
	}
	else if(bTmp == FINAL_MOTOR_STATE_OPEN)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorOpen();// 2018-6-18 moonsungwoo, 배터리체크-모터동작완료-피드백, 순서 변경  
#endif
		CompleteFunction();
		return;
	}

	gbModePrcsStep = ErrorStep;
#endif	//USED_SECOND_MOTOR #endif
}


//------------------------------------------------------------------------------
/** 	@brief 	Motor Open Complete Process By Pincode
	@param	None
	@return 	None
	@remark 인증된 비밀번호에 의한 모터 열림 완료된 후 동작 처리 
*/
//------------------------------------------------------------------------------
void MotorOpenCompleteByPincode(void)
{
	PincodeVerifyModeClear();

#ifdef	DDL_CFG_ADD_REMOCON_LINK_FUNCTION
	PackTxRemoconLinkSend(0x02);
#endif

#ifdef 	DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN	
#ifndef	P_SNS_LOCK_T
	InnerForcedLockClear(); 
#endif 
#endif 

	// Pack Module로 Event 내용 전송 
	PackTx_MakeAlarmPacket(AL_KEYPAD_UNLOCKED, 0x00, GetSlotNumberForPack());
}

#ifdef	CLOSE_BYPINCODE		//지문을 이용한 모터 잠김 정보 송신
//------------------------------------------------------------------------------
/** 	@brief 	Motor Close Complete Process By Pincode
	@param	None
	@return 	None
	@remark 인증된 비밀번호에 의한 모터 닫힘 완료된 후 동작 처리 
*/
//------------------------------------------------------------------------------
void MotorCloseCompleteByPincode(void)
{
	PincodeVerifyModeClear();

#ifdef	DDL_CFG_ADD_REMOCON_LINK_FUNCTION
	PackTxRemoconLinkSend(0x03);
#endif

#ifdef 	DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN	
#ifndef	P_SNS_LOCK_T
	InnerForcedLockClear(); 
#endif 
#endif 

	// Pack Module로 Event 내용 전송 
	PackTx_MakeAlarmPacket(AL_KEYPAD_LOCKED, 0x00, GetSlotNumberForPack());
}

#endif


//============================================================
// 인증된 Pincode로 외부강제잠금 설정하는 단계
//============================================================

void CancelConditionOfOutForcedLockByKey(void);
void OutForcedLockSetByPincode(void);
void WaitResponseFromModule(void);

//------------------------------------------------------------------------------
/** 	@brief 	Out Forced Lock Setting Process by Pincode
	@param	None
	@return 	None
	@remark 인증된 Pincode로 외부강제잠금 설정하는 과정
*/
//------------------------------------------------------------------------------
void OutForcedLockSettingByPincode(void)
{
	switch(gbModePrcsStep)
	{
		case MODEPRCS_LOCKOUTINIT_BY_PIN:
			WaitStartOutForcedLockSetting(MODEPRCS_LOCKOUTSET_BY_PIN);
			break;
		
		case MODEPRCS_LOCKOUTSET_BY_PIN:
			WaitSomeTimeByLongContact(CancelConditionOfOutForcedLockByKey, MODEPRCS_LOCKOUTSET_BY_PIN_CHK);
			break;

		case MODEPRCS_LOCKOUTSET_BY_PIN_CHK:
			MotorCloseCompleteCheck(OutForcedLockSetByPincode, MODEPRCS_LOCKOUTSET_ERROR);
			break;

		case MODEPRCS_LOCKOUTSET_SEND_CHK:
			WaitResponseFromModule();
			break;

		case MODEPRCS_LOCKOUTSET_ERROR:
			if(GetLedMode())	break;
			
			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}

		case MODEPRCS_LOCKOUTSET_END:
		default:
			PincodeVerifyModeClear();
			break;
	}
}



//------------------------------------------------------------------------------
/** 	@brief 	Out Forced Lock Setting Start
	@param	[NextStep] : 설정이 시작되면 이동할 다음 단계
	@return 	None
	@remark 인증된 Pincode 또는 Credential로 외부강제잠금 설정 시작
*/
//------------------------------------------------------------------------------
void WaitStartOutForcedLockSetting(BYTE NextStep)
{
	if(GetFinalMotorStatus() == FINAL_MOTOR_STATE_OPEN_ING)
	{
		return;
	}

	if(GetBuzPrcsStep())	return;
	if(GetLedMode())		return;

	SetModeProcessTime(50);

	gOutForcedLockSettingCnt = 0;
	gbModePrcsStep = NextStep;
}


//------------------------------------------------------------------------------
/** 	@brief 	Wait Long Key or Credential contact for Out Forced Lock Setting
	@param	[CancelCondition] : 외부강제잠금 설정 모드에서 빠져나가는 조건 함수
	@param	[NextStep] : 외부강제잠금 설정 시 처리 단계
	@return 	None
	@remark 외부강제잠금 설정을 위한 Long Key나 Credential 접촉 대기
*/
//------------------------------------------------------------------------------
void WaitSomeTimeByLongContact(void (*CancelCondition)(void), BYTE NextStep)
{
	if(GetModeProcessTime())
	{
		CancelCondition();
		return;
	}
#ifdef DDL_TEST_SET_FOR_CARD
	SetModeProcessTime(300);  // 3sec 에 한번 씩 
#else 
	SetModeProcessTime(50);
#endif 	

	gOutForcedLockSettingCnt++;
	FeedbackLockOutSetting(gOutForcedLockSettingCnt);

	if(gOutForcedLockSettingCnt > 3)
	{
#ifdef DDL_DPS_CHECK_BEFORE_WORK	//문이 열려 있으면 Secure mode 전환 금지
		if(gfDoorSwOpenState)	//문이 열려 있으면 에러 처리	
		{
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
		}
		else
		{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
			BatteryCheck();
#else
			FeedbackMotorClose();
#endif

#ifdef USED_SECOND_MOTOR	//모터 두개 대응(Secure mode 포함)
			StartMotorClose();
			StartMotorClose_Second();
			gbModePrcsStep = NextStep;
#else //USED_SECOND_MOTOR #else	
			StartMotorClose();
			gbModePrcsStep = NextStep;
#endif //USED_SECOND_MOTOR #endif
		}
#else //DDL_DPS_CHECK_BEFORE_WORK #else

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		BatteryCheck();
#else
		FeedbackMotorClose();
#endif

#ifdef USED_SECOND_MOTOR	//모터 두개 대응(Secure mode 포함)
		StartMotorClose();
		StartMotorClose_Second();
		gbModePrcsStep = NextStep;
#else //USED_SECOND_MOTOR #else	
		StartMotorClose();
		gbModePrcsStep = NextStep;
#endif //USED_SECOND_MOTOR #endif

#endif //DDL_DPS_CHECK_BEFORE_WORK #endif
	}
}


//------------------------------------------------------------------------------
/** 	@brief 	Out Forced Lock Cancel Condition By Key
	@param	None
	@return 	None
	@remark 외부강제잠금 설정 모드에서 빠져나가기 위한 조건
	@remark 접촉된 # 키가 떨어질 경우 외부강제잠금 설정 종료
*/
//------------------------------------------------------------------------------
void CancelConditionOfOutForcedLockByKey(void)
{
	if(gbNewKeyBuffer != TENKEY_SHARP)
	{
		FeedbackBuzStopLedOff();
		PincodeVerifyModeClear();
	}
}



//------------------------------------------------------------------------------
/** 	@brief 	Out Forced Lock Setting Complete
	@param	[CompleteFunction] : 모터 닫힘 정상 종료 시 실행 함수
	@param	[ErrorStep] : 모터 닫힘 실패 시 처리 단계
	@return 	None
	@remark 외부강제잠금 설정 완료에 따른 모터 닫힘 확인 및 완료 처리
*/
//------------------------------------------------------------------------------
void MotorCloseCompleteCheck(void (*CompleteFunction)(void), BYTE ErrorStep)
{
	BYTE bTmp;
#ifdef USED_SECOND_MOTOR
	BYTE bTemp;

	bTmp = GetFinalMotorStatus();
	bTemp = GetFinalMotorStatus_Second();

	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ING || bTemp == FINAL_MOTOR_STATE_CLOSE_ING) return;
#if 0
	if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
	
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR || bTemp == FINAL_MOTOR_STATE_CLOSE_ERROR)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
	
		// Pack Module로 Jammed Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x00);
	}
	else if(bTmp == FINAL_MOTOR_STATE_CLOSE || bTemp == FINAL_MOTOR_STATE_CLOSE)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorClose();// 2018-6-18 moonsungwoo 
#endif
		//2019년 11월 11일 심재철 추가 시작 
		//Credential로 Lock이 된 이후에 모듈에서 Lock Status를 조회할 때 Locked가 아닌 다른 값으로 응답하는 상황을 개선 하기 위함
		//이유는 Lock Status를 수신 했을때 gLockStatusTimer1s가 초기화 되지 않기 때문임
#ifndef P_SNS_EDGE_T
 		AutoRelockClear();
#endif

		CompleteFunction();
		return;
	}

	gbModePrcsStep = ErrorStep;
	
#else	//USED_SECOND_MOTOR #else
	bTmp = GetFinalMotorStatus();
	
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ING) return;
#if 0
	if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
	
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
	
		// Pack Module로 Jammed Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x00);
	}
	else if(bTmp == FINAL_MOTOR_STATE_CLOSE)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorClose();// 2018-6-18 moonsungwoo 
#endif
		//2019년 11월 11일 심재철 추가 시작 
		//Credential로 Lock이 된 이후에 모듈에서 Lock Status를 조회할 때 Locked가 아닌 다른 값으로 응답하는 상황을 개선 하기 위함
		//이유는 Lock Status를 수신 했을때 gLockStatusTimer1s가 초기화 되지 않기 때문임
#ifndef P_SNS_EDGE_T
 		AutoRelockClear();
#endif

		CompleteFunction();
		return;
	}

	gbModePrcsStep = ErrorStep;
#endif	//USED_SECOND_MOTOR #endif	
}


//------------------------------------------------------------------------------
/** 	@brief 	Wait Responde to Sending Out Forced Lock Setting
	@param	None
	@return 	None
	@remark 외부강제잠금 설정 완료 Event 전송에 따른 ACK 대기
*/
//------------------------------------------------------------------------------
void WaitResponseFromModule(void)
{
	// 외부강제잠금에 의한 잠김 신호가 없어 아래 신호 추가 전송
	if(GetModeProcessTime()== 0)
	{
		PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
	
		gbModePrcsStep = MODEPRCS_LOCKOUTSET_END;
	}


/*
	if(gbPackCommResult == PACK_RESULT_ACK_OK)
	{
//		FeedbackModeCompleted(1, VOL_CHECK);
		FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
	}
	else if(gModePrcsTimer10ms == 0)
	{
		ErrorModeClear();
	}
	
	if(gbPackCommResult != 0)
	{
		gbPackCommResult = 0;
	}
*/
}


//------------------------------------------------------------------------------
/** 	@brief 	Out Forced Lock Complete Process By Pincode
	@param	None
	@return 	None
	@remark 인증된 비밀번호에 의한 외부강제잠금 완료된 후 동작 처리 
*/
//------------------------------------------------------------------------------
void OutForcedLockSetByPincode(void)
{
	BYTE bTmpArray[11];

	DDLStatusFlagSet(DDL_STS_OUTLOCK);

//	if(gbModuleMode == RF_TRX_MODULE_2WAY)
	{
		// Pack Module로 Event 내용 전송 
		bTmpArray[0] = 0xFF;		// No Code Data
		bTmpArray[1] = CREDENTIALTYPE_PINCODE;
		bTmpArray[2] = GetSlotNumberForPack();
		memset(&bTmpArray[3], 0xFF, 8);
		PackTx_MakePacket(EV_ARM_REQEST, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 11);
		
		SetModeProcessTime(100);
		gbModePrcsStep = MODEPRCS_LOCKOUTSET_SEND_CHK;
		
	}
}


																			
void(*const PincodeVerifyModeProcess_Tbl[])(void) = {
	OnetimeSilentModeCheck,
	VerifyInputPincodeProcessSetp,
	VerifyScheduleStepForPincodeVerify,
	MotorOpenProcessByPincode,
	OutForcedLockSettingByPincode,
};


void ModePINVerify(void)
{
	PincodeVerifyModeProcess_Tbl[gPincodeVerifyMode]();
}



