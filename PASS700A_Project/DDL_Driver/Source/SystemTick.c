#define	_SYSTEM_TICK_C_

#include	"Main.h"


WORD gbBasicTimer2 = 0;
WORD gbBasicTimer10 = 0;
WORD gbBasicTimer100 = 0;
WORD gbBasicTimer1s = 0;


WORD gbWakeUpMinTime10ms = 0;




uint32_t gwBasicTimerInitCnt = 0;


void HAL_SYSTICK_Callback(void)
{
	if ( HAL_GetTick() & 1 )
		systick_2ms_flag = 1;			 //2msec 단위로 처리
}



uint32_t SystickLoad(void)
{
	return (uint32_t) gh_mcu_tim_usec->Instance->CNT;
}


void SysTimerStart(uint32_t *wData)
{
	*wData = SystickLoad();
}


BYTE SysTimerEndCheck(uint32_t wData, uint32_t wLimit)
{
	uint32_t wTmp;

	wTmp = SystickLoad();

	wTmp = (wTmp - wData) & 0xFFFF;

	if(wTmp >= wLimit)
		return 1;
	else
		return 0;
}


// 아래 4개의 함수는 모델별로 추가할 timer counter들의 처리를 한다.
// SystemTickLocal.c에 모델별로 동일한 이름의 함수에 필요한 타이머 기능을 추가한다.
void	BasicTimer_2ms_local( void )
{
#ifdef	DDL_CFG_TOUCHKEY
	if(gbTKeyTimer2ms)		--gbTKeyTimer2ms;
#endif

	if(gwVoiceTimer2ms) 		--gwVoiceTimer2ms;

	if (gwMotorCtrlTimer2ms)		--gwMotorCtrlTimer2ms;
#ifdef	USED_SECOND_MOTOR	//모터 두개 대응(Secure mode 포함)
		if (gwMotorCtrlTimer2ms_Second) 	--gwMotorCtrlTimer2ms_Second;
#endif	//USED_SECOND_MOTOR #endif

#ifdef	DDL_CFG_RFID
	CardProcessTimeCounter();
	CardAwayCheckTimeCounter();
#endif

	LedProcessTimeCount();

#ifdef	DDL_CFG_IBUTTON
	GMTouchKeyTimeCount();
#endif

#if defined	(DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
	if(gwDetectionTimer2ms) --gwDetectionTimer2ms;
	if(gwIbuttonVerifyDetectionTimer2ms)	--gwIbuttonVerifyDetectionTimer2ms; 
#endif 

#if defined (P_COM_DDL_EN_T)
	PackTxProcessTimeCounter();
#endif 
#if defined (P_BT_WAKEUP_T)
	InnerPackTxProcessTimeCounter();
#endif 
}


void	BasicTimer_10ms_local( void )
{
#if defined	(DDL_CFG_FP_TCS4K) || defined	(DDL_CFG_FP_PFM_3000)  || defined (DDL_CFG_FP_TS1071M) //hyojoon_20160831 PFM-3000 add
	if (gbBioTimer10ms) 	--gbBioTimer10ms;
#elif defined 	(DDL_CFG_FP_HFPM)
	HFPMBioTimerCallback();
#elif defined 	(DDL_CFG_FP_INTEGRATED_FPM) 
	NFPMBioTimerCallback();
#endif

#ifdef	DEBUG_TOUCH
	DebugTouchKeyTimeCount();
#endif

	ModeProcessTimeCounter();
#if defined (P_COM_DDL_EN_T)
	PackModeTimeCounter();
	PackTxAckWaitTimeCounter();
	PackTxWaitTimeCounter();
#endif 	
#if defined (P_BT_WAKEUP_T)
	InnerPackModeTimeCounter();
	InnerPackTxAckWaitTimeCounter();
	InnerPackTxWaitTimeCounter();
#endif 

	LockStatusCheckTimeCount();
}


void	BasicTimer_100ms_local( void )
{
	AutoRelockTimeCounter();
#if defined	(DDL_CFG_FP_TCS4K)
	if(gbFingerLongTimeWait100ms)	--gbFingerLongTimeWait100ms;

//	TCS4K_ProcessTimerCounter();
#elif defined	(DDL_CFG_FP_PFM_3000) || defined (DDL_CFG_FP_TS1071M)  || defined (DDL_CFG_FP_HFPM) || defined (DDL_CFG_FP_INTEGRATED_FPM)  //hyojoon_20160831 PFM-3000 add
	if(gbFingerLongTimeWait100ms)	--gbFingerLongTimeWait100ms;
	if(gbCoverAccessDelaytime100ms) -- gbCoverAccessDelaytime100ms;
#endif
	ModeTimeOutCounter();

#if defined (P_COM_DDL_EN_T)
	PackCheckTimeCounter();
	PackTxAckWaitCallback();
#endif 
#if defined (P_BT_WAKEUP_T)
	InnerPackCheckTimeCounter();
	InnerPackTxAckWaitCallback();
#endif 


#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
	if(gbFeedbackCloseOnceTimercnt100ms)	--gbFeedbackCloseOnceTimercnt100ms;
#endif	
/*
#ifdef	__DDL_MODEL_FRONTDOORSUITE	
	LockStatusMainMotorAutoOpenCheckTimer();
#endif
*/
}


void	BasicTimer_1000ms_local( void )
{
	//AutoRelockTimeCounter();
	AlarmOnTimeCounter();
	TamperProofLockoutCounter();
	JigProcessTimeCount();

#ifdef	P_SNS_EDGE_T			// Edge sensor
	DoorClosedCheckCounter();
#endif

#ifdef	DDL_CFG_HIGHTEMP_SENSOR
	TempSensorCheckCounter();
#endif
	PackAlarmReportLowBatteryCounter();
	ModeBatteryLifeCycleCounter();

#ifdef	DDL_TEST_SET_FOR_CARD
	if(gKCFeedbackTimer1s)	--gKCFeedbackTimer1s;
#endif 	

	AbnormalSleepCheckCounter();
	PackLedControlCounter();
	
#ifdef DDL_AAAU_AUTOLOCK_HOLD	//호주 및 뉴질랜드 전용 autolock hold 기능
	AutolockHoldClearTimerCounter();
#endif //DDL_AAAU_AUTOLOCK_HOLD #endif
}

void TimerBufferInc(void)
{
	gbBasicTimer2++;				//2ms Tick

	BasicTimer_2ms_local();			// 2msec timers

	if ( gbBasicTimer2 >= 5 ) {
		gbBasicTimer2 = 0;
		//10ms Tick
		gbBasicTimer10++;

		BasicTimer_10ms_local();			// 10msec timers

		if(gbWakeUpMinTime10ms) 	--gbWakeUpMinTime10ms;
	}

	if ( gbBasicTimer10 >= 10 ) {
		gbBasicTimer10 = 0;
		//100ms Tick
		gbBasicTimer100++;

		BasicTimer_100ms_local();		// 100msec timers
	}

	if ( gbBasicTimer100 >= 10 ) {
		gbBasicTimer100 = 0;
		//1s Tick
		gbBasicTimer1s++;

		BasicTimer_1000ms_local();		// 1000msec timers
	}

}



void BasicTimer2ms(void)
{
	BYTE bTmp0 = 0;
	BYTE bTmp1 = 0;

	if(gbNewKeyBuffer != TENKEY_NONE)	bTmp0 |= 0x04;
	if(gbNewFunctionKey != 0)			bTmp0 |= 0x02;
	if(gbNewOCFunctionKey != TENKEY_NONE)		bTmp0 |= 0x08;	//  Open/Close button OChyojoon.kim 


	if( systick_2ms_flag ) {
		// 2ms 마다 처리하는 함수들
		KeyChattering();
		TimerBufferInc();
		systick_2ms_flag = 0;
	}

	if(gbNewKeyBuffer != TENKEY_NONE)	bTmp1 |= 0x04;
	if(gbNewFunctionKey != 0)			bTmp1 |= 0x02;
	if(gbNewOCFunctionKey != TENKEY_NONE)		bTmp1 |= 0x08;		 //  Open/Close button OChyojoon.kim 

	bTmp1 = bTmp0 ^ bTmp1;
	bTmp1 = bTmp1 & (~bTmp0);

	gbInputKeyValue = TENKEY_NONE;

	if(gbKeyInitialCnt) return;

	if(bTmp1 == 0x02)		
		gbInputKeyValue = gbNewFunctionKey;
	else if(bTmp1 == 0x04)	
		gbInputKeyValue = gbNewKeyBuffer;
	else if(bTmp1 == 0x08)	
		gbInputKeyValue = gbNewOCFunctionKey; //  Open/Close button OChyojoon.kim 

	if(gbInputKeyValue != TENKEY_NONE)
	{
		SetLockStatusInfo(LOCK_LAST_KEY,gbInputKeyValue);
	}
}

void SystickVariableInit(void)
{
	SysTimerStart(&gwBasicTimerInitCnt);
//	SysTimerStart(&gwRomWriteTimerInitCnt);
	SysTimerStart(&gdwBuzTimerInitCnt);
}





/**************************************************************************//**
 * @brief Delays number of msTick Systicks (typically 2 ms)
 * @param dlyTicks Number of ticks to delay
 *****************************************************************************/
void Delay(uint32_t dlyTicks)
{
  	uint32_t curTicks;
  	uint32_t wTmp;

  	curTicks = SystickLoad();
  	while (1) {
	  	wTmp = SystickLoad();

		wTmp = (wTmp - curTicks) & 0xFFFF;

		if(wTmp >= dlyTicks)
			break;
  	}
}




