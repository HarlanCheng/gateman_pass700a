

#ifndef	__MOTOR_INCLUDED_SECOND
#define	__MOTOR_INCLUDED_SECOND

#include "DefineMacro.h"
//#include "DefinePin.h"

#ifdef M_CLOSE_PWMCH1_Pin//second pwm_주석 by sjc

void SetMotorPWMFreq_Second(void);
#define PWM_START_DUTY 50 //50%
#define PWM_DUTY_HOLD_COUNTER 150  // 100 count 당 6ms 이므로 9ms 정도 
#define PWM_BACKTURN_SKIP_COUNT 5000 
#define PWM_HOLD_OPEN_TIME 100 //200ms

// 기존 define 사용 하기 위해 
#define M_OPEN_1_GPIO_Port		M_OPEN_PWMCH2_GPIO_Port
#define M_OPEN_1_Pin				M_OPEN_PWMCH2_Pin
#define M_CLOSE_2_GPIO_Port		M_CLOSE_PWMCH1_GPIO_Port
#define M_CLOSE_2_Pin			M_CLOSE_PWMCH1_Pin//second pwm_주석 by sjc

// 아래 매크로들은 
// HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin | M_OPEN_2_Pin, GPIO_PIN_RESET); 이런 식으로 쓸수도 있지만,
// 향후 서로 다른 GPIO port로 매핑 될 수 도 있을 듯 싶어 분리해서 만듬

#define 	_MOTOR_CLOSE_Second	{ \
			HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_OPEN_2_GPIO_Port, M_OPEN_2_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, GPIO_PIN_SET); \
}

#define 	_MOTOR_OPEN_Second	{ \
			HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_CLOSE_2_GPIO_Port, M_CLOSE_2_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_OPEN_2_GPIO_Port, M_OPEN_2_Pin, GPIO_PIN_SET); \
}
#else 
// 아래 매크로들은 
// HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin | M_OPEN_2_Pin, GPIO_PIN_RESET); 이런 식으로 쓸수도 있지만,
// 향후 서로 다른 GPIO port로 매핑 될 수 도 있을 듯 싶어 분리해서 만듬

#define 	_MOTOR_CLOSE_Second	{ \
			HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, GPIO_PIN_SET); \
}

#define 	_MOTOR_OPEN_Second	{ \
			HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin, GPIO_PIN_SET); \
}
#endif 
//#endif
#define	_MOTOR_BREAK_Second	{ \
			HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin, GPIO_PIN_SET); \
			HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, GPIO_PIN_SET); \
}


#define 	_MOTOR_STOP_Second	{ \
			HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, GPIO_PIN_RESET); \
}

#define 	_MOTOR_PWR_EN_Second {\
			HAL_GPIO_WritePin(M_PWR_EN1_GPIO_Port, M_PWR_EN1_Pin, GPIO_PIN_SET); \
}

#define 	_MOTOR_PWR_DISABLE_Second {\
			HAL_GPIO_WritePin(M_PWR_EN1_GPIO_Port, M_PWR_EN1_Pin, GPIO_PIN_RESET); \
}

#define 	_MOTOR_SLEEP_EN_Second {\
			HAL_GPIO_WritePin(M_SLEEP_1_GPIO_Port, M_SLEEP_1_Pin, GPIO_PIN_RESET); \
}

#define 	_MOTOR_SLEEP_EXIT_Second {\
			HAL_GPIO_WritePin(M_SLEEP_1_GPIO_Port, M_SLEEP_1_Pin, GPIO_PIN_SET); \
}

void	Init_Motor_Second( void );

//extern BYTE gbTestCnt3;
//extern BYTE gbTestCnt2;
//extern BYTE gbTestCnt1;

//extern BYTE gbTestErrCnt3;
//extern BYTE gbTestErrCnt2;
//extern BYTE gbTestErrCnt1;


void InnerForcedLockSet_Second(void);//InnerForcedLockSet_Second
void InnerForcedLockClear_Second(void);//InnerForcedLockClear_Second
void InnerForcedLockSettingLoad_Second(void);	//InnerForcedLockSettingLoad_Second
uint32_t InnerForcedLockCheck_Second(void);//InnerForcedLockCheck_Second()




#endif


