//------------------------------------------------------------------------------
/** 	@file		ISO14443AFunctions_T1.c
	@version 0.1.01
	@date	2016.05.30
	@brief	4~7 bytes ISO14443A Card UID Read Functions using TRF7970
	@remark	 TRF7970을 이용해 ISO14443A 카드의 UID를 읽어들이기 위한 함수
	@remark	 4~7 byte의 UID를 가진 Mifare Classic 카드 Read 대응 함수
	@remark	 Anticollision 알고리즘 수행
	@remark	 ========== ISO 14443 규정 =========================================================
	@remark	  1.Cascade Level 1 = 0x93 	
	@remark	 	UID Size => Single		
	@remark	 	Number of UID bytes => 4
	@remark	     Cascade Level 1 = 0x95 	
	@remark	 	UID Size => Double		
	@remark	 	Number of UID bytes => 7
	@remark
	@remark   2.NVB = 0x20 
	@remark		This value defines that the PCD will transmit no part of UID CLn. 
	@remark		Consequently this command forces all PICCs in the field to respond with	their complete UID CLn. 		
	@remark
	@remark		Collision 발생 이전 전송 Data => 0x00+0x20+0x93+0x20
	@remark		Collision 발생 이후 전송 Data => 0x00+TxLengthByte2+0x93+NVB+깨진 Byte전까지의 Data+깨진 Byte의 Bit까지의 Data
	@remark		ANTICOLLISION Command => SEL+NVB 
	@remark				  				    SEL+NVB+UID CLn 
	@remark	  3.NVB										
	@remark	 	Upper 4 bit(Byte Count) : PCD에서 보내어지는 모든 valid data bit를 8로 나눈 정수값을 지정 (SEL, NVB 포함) (2~7) 		
	@remark	 	Lower 4 bit(Bit Count) : PCD에서 보내어지는 모든 valid data bit를 8로 나눈 값의 나머지를 지정 (SEL, NVB 포함) (0~7)
	@remark
	@remark	  4. SELECT
	@remark		If no further collision occurs, the PCD shall assign NVB with the value of '70'.
	@remark       	This value defines that the PCD will transmit the complete UID CLn.
	@remark	 	SELECT Command => SEL+NVB+UID CLn+CRC_A
	@remark
	@remark	  5. SAK(Select Acknowledge) 
	@remark		NVB가 40 valid data bit로 지정되었거나 모든 data bit가 UID CLn과 일치하였을 때 PICC가 보냄	
	@remark		bit3=1(& 0x04) => Cascade bit set;UID not complete	
	@remark		bit6=1(& 0x20) => UID complete, PICC compliant with ISO/IEC 14443-4 
	@remark		bit3=0 && bit6=0 => UID complete, PICC not compliant with ISO/IEC 14443-4
	@remark	 ==============================================================================

	@see	TRF7970Functions_T1.c
	@see	CardProcess_T1.c
	@see	ParallelFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.08		by Jay
			- TRF7970을 SPI로 Serial 제어
			- 해당 함수는 ISO14443A 규격 대응 내용을 포함하며, 카드 구동을 위해서는 
				TRF7970Functions_T1 과 CardProcess_T1 이 모두 연결되어 있어 같이 결합하여 사용해야 함.
			- 설정에 의해 아이레보 카드만 대응도 가능하고, OPEN UID 대응도 가능하게 설계
			- 별도 설정 없이 4byte UID 카드 및 7byte UID 카드 모두 대응 가능

		V0.1.01 2017.05.30		by MoonSungWoo
			- 중국(APAC) 모델 중에서 RF폴링 시간을 조절 해야 하는 경우가 잇어 ddl_config-모델명.h 에서 조절 가능하게 ifdef문 추가.
			- 'ddl_config-모델명.h'에 아래와 같이 딜레이 조절 define 선언  
			ex>  #define		OPEN_UID_DELAY	 SYS_TIMER_?MS
			- #ifdef OPEN_UID_DELAY

		V0.1.02 2017.08.30		by MoonSungWoo
			- 카드 모델의 경우 반드시 ddl_main.c의 "void InitializeDDL_SW( void ){}" 아래 코드 추가 
			- 테스트 모드에서 OPEN UID 설정 모드, OPEN UID 설정 유무 Load  함수 임,  
========================================
#ifdef	DDL_CFG_RFID
	CardOpenUidSupportedInfoLoad();
#endif
========================================
			

*/
//------------------------------------------------------------------------------


#include "Main.h"


BYTE gIso14443aCascadeLevel = 0;				/**< Anticollision 과정 중에서 카드의 UID 읽기 위한 Cascase level  */

BYTE gIsoCollisionSet = 0;						/**< Collision 에러 발생 여부 확인 Flag 변수, 1일 경우 Collision 발생  */
BYTE gIso14443aTempUidBuf[5];					/**< Anticollision 알고리즘 수행 중에 읽어 온 UID를 임시 저장하는 버퍼, 최대 5byte만 저장  */
												/**< NVB값을 참조한 값 (Collision Bit Count가 없을 경우 NVB와 동일) */
									
BYTE gIso14443aNVB = 0;							/**< 깨진 Bit 다음부터 전송 요구를 하기 위한 NVB값 */




BYTE gCardOpenUidSupported = 0;


																			
void CardOpenUidSupportedInfoLoad(void)
{
	RomRead(&gCardOpenUidSupported, OPEN_UID_EN, 1);
}

void CardOpenUidSupportedInfoSave(BYTE SetValue)
{
	gCardOpenUidSupported = SetValue;
	RomWrite(&gCardOpenUidSupported, OPEN_UID_EN, 1);
}


bool IsCardOpenUidSupported(void)
{
	if(gCardOpenUidSupported == 0x74)	return (true);
	else								return (false);
}
											



//------------------------------------------------------------------------------
/** 	@brief	UID size check 
	@param 	[Atqa1stData] : ATQA의 첫번째 Byte 
	@return 	[7] : double UID (7byte UID 카드)
	@return 	[4] : single UID (4byte UID 카드)
	@remark 입력된 카드의 UID byte 확인, double UID(7byte)까지만 구현
	@remark ISO14443A 규격 참조
*/
//------------------------------------------------------------------------------
BYTE UidSizeCheck(BYTE Atqa1stData)
{
	switch(Atqa1stData & 0xC0)
	{
		case 0x00:	
			// single UID (4byte)
			return 4; 

		case 0x40:	
			// double UID (7byte)
			return 7; 
	}

	// UID size length가 정해진 값이 아닐 경우 에러 처리
	return 0; 
}


//------------------------------------------------------------------------------
/** 	@brief	Input UID BCC Check
	@param 	[Uid] UID 버퍼의 포인터 	
	@return 	[STATUS_SUCCESS] BCC matched
	@return 	[STATUS_FAIL] BCC Not matched
	@remark 입력된 UID 4byte의 BCC Data 검증
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
BYTE isUidBccOk(BYTE *Uid)
{
	BYTE Bcc = 0;
	BYTE bCnt;	

	// 5byte의 Data 중 마지막 byte가 BCC
	for(bCnt = 0; bCnt < 4; bCnt++)
	{
		Bcc ^= *Uid;
		Uid++;
	}

	if(Bcc == *Uid)
	{
		return STATUS_SUCCESS;
	}

	return STATUS_FAIL;
}


//------------------------------------------------------------------------------
/** 	@brief	Make a mask data to delete broken bits in broken byte
	@param 	[CollisionBit] Collision이 발생한 바로 다음 Bit, 해당 Bit는 1 또는 0으로 설정하여 전송 	
	@return 	[bMask] Collision이 발생한 Bit 바로 다음까지의 Bit를 제외하고는 모두 0으로 처리하기 위한 변수
	@remark Anticollision 수행 시 Collision 발생 이후 수신한 Byte와 Bit까지의 정보 전송에 사용
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
BYTE MakeMaskBrokenBit(BYTE CollisionBit)
{
	BYTE bCnt;
	BYTE bMask = 0;

	for(bCnt = 0; bCnt < CollisionBit; bCnt++)
	{
		// left shift to make a mask for last broken byte (create all bit 1 belong to how many bit in broken byte)
		bMask = (bMask << 1) + 1;			
	}

	return (bMask);
}


//------------------------------------------------------------------------------
/** 	@brief	Make a mask data to delete unbroken bits in broken byte
	@param 	[CollisionBit] Collision이 발생한 바로 다음 Bit, 이미 수신한 Bit와 새로 수신한 Bit를 겹합하여 Byte 완성 	
	@return 	[bMask] Collision이 발생한 Bit까지의 Bit는 그대로 두고 새로 수신한 Bit를 결합하기 위한 변수
	@remark Anticollision 수행에 따라 수신한 수신한 Bit와 이미 수신된 Bit의 결합을 위해 사용
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
BYTE MakeMaskUnbrokenBit(BYTE CollisionBit)
{
	BYTE bCnt;
	BYTE bMask = 0xFF;
	BYTE bBit = 0x01;
	
	for(bCnt = 0; bCnt < CollisionBit; bCnt++)
	{
		bMask &= ~bBit;
		bBit <<= 1;
	}

	return (bMask);
}


//------------------------------------------------------------------------------
/** 	@brief	Set Cascade Level 
	@param 	[Level] : Cascade Level 
	@return 	None
	@remark Anticollision 및 Select 과정에서의 Cascade Level 설정, 2단계까지만 구현
	@remark ISO14443A 규격 참조
*/
//------------------------------------------------------------------------------
void CascadeLevelSet(BYTE Level)
{
	if(Level == 2)
	{
		gIso14443aCascadeLevel = ISO14443A_SEL_CASCADE2;
	}
	else
	{
		gIso14443aCascadeLevel = ISO14443A_SEL_CASCADE1;
	}		
}



//------------------------------------------------------------------------------
/** 	@brief	Clear anticollision variables
	@param 	None 	
	@return 	None
	@remark Anticollision에 사용된 모든 변수 초기화
*/
//------------------------------------------------------------------------------
void AntiColVariInit(void)
{
	gIsoCollisionSet = 0;
	gIso14443aNVB = 0x20;

	memset(gIso14443aTempUidBuf, 0xFF, 5);		
}



//------------------------------------------------------------------------------
/** 	@brief	Copy read UID to target buffer
	@param 	[TargetBuf] Target Buffer to copy
	@param 	[UidSize] UID Size (4byte or 7 byte)
	@return 	None
	@remark TRF7970에서 읽어 들인 UID를 요청한 Buffer에 복사
*/
//------------------------------------------------------------------------------
void CopyCardUid(BYTE *TargetBuf, BYTE UidSize)
{
	if(UidSize == 7)
	{
		if(gIso14443aCascadeLevel == ISO14443A_SEL_CASCADE1)
		{
			memcpy(TargetBuf, gIso14443aTempUidBuf+1, 3);
		}
		else
		{
			memcpy(TargetBuf+3, gIso14443aTempUidBuf, 4);
		}
	}
	else
	{
		// 4바이트 UID 저장
		memcpy(TargetBuf, gIso14443aTempUidBuf, 4); 	
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Send WUPA for detecting card
	@param 	None 	
	@return 	[0] : 감지된 카드 없음
	@return 	[1] : 감지된 카드 있음
	@remark 폴링하면서 카드 입력 감지하기 위한 함수
	@remark 카드 접촉이 없을 경우 EN ON부터 EN OFF까지 약 2~3ms 소요 		
	@remark [1] Power On (TRF7970 EN->1ms Delay->Register Intialization)
	@remark [2] RF TX On	
	@remark [3] Wait
	@remark		-> Open UID : 원래 4ms 대기였는데 ISO 규정 만족을 위해 6ms로 변경 
	@remark		-> iRevo Card : 500us 대기 
	@remark [4] Send REQA
	@remark [5] ATQA Check
	@remark 	-> 1 : Received			
*/
//------------------------------------------------------------------------------
BYTE Iso14443aFindTag(BYTE *UidSize)
{
#ifdef	DDL_CFG_RFID_PARALLEL 

	BYTE bNum = 80;	// 약 300us 정도 대기

	RfidPowerOn();

	RfidTxOn();

//#ifdef 	IREVO_CARD_ONLY
	if(IsCardOpenUidSupported() == false)
	{
	//아이레보 카드만을 위한 폴링 동작은 카드가 없을 때 RF 출력 시간이 1ms에 맞추어지도록 값 조정해야 함. 	

//	Delay(SYS_TIMER_200US);
//	Delay(SYS_TIMER_50US);				// wait until system clock started
	}
//#else
	else
	{

	// When a PICC is exposed to an unmodulated operating field
	// it shall be able to accept a quest within 5 ms.
	// PCDs should periodically present an unmodulated field of at least
	// 5,1 ms duration. (ISO14443-3)
#ifdef OPEN_UID_DELAY
	Delay(OPEN_UID_DELAY);
#else
	Delay(SYS_TIMER_2MS);
#endif
	}
//#endif

	IrqClear();

	Iso14443aReqaSend(ISO14443A_WUPA);
	// Reference Source에서는 TX Wait Time 5ms, RX Wait Time 50ms 설정하여 사용하였지만,
	// 배터리 문제로 RX Wait Time을 약 350us로 조정하여 사용 

	while(--bNum)
	{
		if(IrqCheck() == IRQ_RX_COMPLETED)
		{
			if(Iso14443aReqaRespond(UidSize) == STATUS_SUCCESS)
			{
				return 1;
			}
			break;
		}
	}

	RfidPowerOff();

   	return 0;

#else

	BYTE bNum = 250;	// 약 350us 정도 대기

	RfidPowerOn();

	RfidTxOn();

//#ifdef 	IREVO_CARD_ONLY
	if(IsCardOpenUidSupported() == false)
	{
	//아이레보 카드만을 위한 폴링 동작은 카드가 없을 때 RF 출력 시간이 1ms에 맞추어지도록 값 조정해야 함. 	

	Delay(SYS_TIMER_200US);
	Delay(SYS_TIMER_50US);				// wait until system clock started
	}
//#else
	else
	{
	// When a PICC is exposed to an unmodulated operating field
	// it shall be able to accept a quest within 5 ms.
	// PCDs should periodically present an unmodulated field of at least
	// 5,1 ms duration. (ISO14443-3)
		Delay(SYS_TIMER_2MS);
	}	
//#endif

	IrqClear();

	Iso14443aReqaSend(ISO14443A_WUPA);
	// Reference Source에서는 TX Wait Time 5ms, RX Wait Time 50ms 설정하여 사용하였지만,
	// 배터리 문제로 RX Wait Time을 약 350us로 조정하여 사용 
	while(--bNum)
	{
		if(IrqCheck() == IRQ_RX_COMPLETED)
		{
			if(Iso14443aReqaRespond(UidSize) == STATUS_SUCCESS)
			{
				return 1;
			}
			break;
		}
	}

	RfidPowerOff();

   	return 0;

#endif
}



//=============================================================
//	ISO14443A INTERFACE FUNCTION						
//=============================================================

//------------------------------------------------------------------------------
/** 	@brief	ISO14443A Standard, Send REQA or WUPA
	@param 	Command : REQA or WUPA 	
	@return 	None 
	@remark ISO14443A 규격의 REQA 또는 WUPA 신호 전송
	@remark ISO_CONTROL
	@remark 	- No Rx CRC
	@remark		- ISO14443A, bit rate 106kbs
	@remark TRF7960 Datasheet 참조
*/
//------------------------------------------------------------------------------
void Iso14443aReqaSend(BYTE Command)
{
	TrfSetRegister(ISO_CONTROL, 0x88);		//ISO Control (Address 01) = 0x88
											//	No Rx CRC,
											//	ISO14443A, bit rate 106kbit/s

	// 이 부분은 Reference Source에는 있지만, 실제 구현에서는 삭제한 부분
//	TrfGetRegisterData(ISO_CONTROL,&bTmp, 1);	
//	Delay(SYS_TIMER_6MS);

	gTrfFifoDataBuf[0] = 0x8F;		// Reset Command
	gTrfFifoDataBuf[1] = 0x90;		// Transmit No CRC Command
	gTrfFifoDataBuf[2] = 0x3D;		// Address of Register TX_LENGTH_BYTE_1
	gTrfFifoDataBuf[3] = 0x00;		// Data of TX_LENGTH_BYTE_1
	gTrfFifoDataBuf[4] = 0x0F;		// Data of TX_LENGTH_BYTE_2
	gTrfFifoDataBuf[5] = Command;	// Data to be sent

	TrfSendInventoryCommand(gTrfFifoDataBuf, 6);	
}


//------------------------------------------------------------------------------
/** 	@brief	ISO14443A Standard, Response Check on REQA
	@param 	[UidSize] : ATQA를 통해 확인한 UID Size 정보 저장 	
	@param 	[gTrfFifoDataBuf] : 수신한 2 byte Data 저장 	
	@return 	[STATUS_FAIL] : FIFO에 수신된 Data가 2byte가 아님
	@return 		-> TRF7960의 경우에는 FIFO Status+1이 실제 수신한 byte 수
	@return 		-> TRF7970의 경우에는 FIFO Status가 실제 수신한 byte 수
	@return 	[STATUS_SUCCESS] : 2 byte 수신 완료
	@remark ISO14443A 규격의 REQA or WUPA 신호에 대한 응답 확인
	@remark 2 byte 응답이 수신되어야 정상 처리
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
BYTE Iso14443aReqaRespond(BYTE *UidSize)
{
	BYTE FifoLength;
	WORD wTmp;
	BYTE bTmp;
	
	//How many bytes loaded in FIFO were not read out yet
	TrfGetRegisterData(FIFO_CONTROL, &FifoLength, 1);		

	//	(TRF7960에서는 수신한 byte-1이 Load되어 -1 처리한 값으로 비교해야 함.)
	if((FifoLength & 0x0F) != 0x02)				//For TRF7970
//	if((FifoLength & 0x0F) != 0x01)					//For TRF7960
	{
		// 수신한 byte가 2byte가 아닐 경우 에러 처리 
		return STATUS_FAIL;
	}

	TrfGetRegisterData(FIFO, gTrfFifoDataBuf, FifoLength);

	wTmp = (WORD)gTrfFifoDataBuf[0];
	wTmp = (wTmp << 8) | (WORD)gTrfFifoDataBuf[1];

	// Noise에 의해 잘못 입력된 값 처리 방지를 위해 ATRA의 RFU bit가 Set되어 있을 경우에는 에러 처리
//#ifdef	IREVO_CARD_ONLY
	if(IsCardOpenUidSupported() == false)
	{
		if((wTmp != 0x0400) && (wTmp != 0x4400))
		{
			return STATUS_FAIL;
		}
		}
//#else
		else
		{
		if(wTmp & 0x20F0)
		{
			return STATUS_FAIL;
		}
	}
//#endif

	bTmp = UidSizeCheck(gTrfFifoDataBuf[0]);
	// UID size length가 정해진 값이 아닐 경우 에러 처리
	if(bTmp == 0)
	{
		return STATUS_FAIL;
	}

	*UidSize = bTmp;

	return STATUS_SUCCESS;
}



//------------------------------------------------------------------------------
/** 	@brief	ISO14443A Standard, Halt
	@param 	None 	
	@return 	None 
	@remark 선택된 ISO14443A PICC를 Halt 처리
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void Iso14443aHalt(void)
{
	TrfSetRegister(ISO_CONTROL, 0x08);					//ISO Control (Address 01) = 0x08
														//	Rx CRC,
														//	ISO14443A, bit rate 106kbit/s

	gTrfFifoDataBuf[0] = 0x8F; 	// Reset Command
	gTrfFifoDataBuf[1] = 0x91; 	// Transmit CRC Command
	gTrfFifoDataBuf[2] = 0x3D; 	// Address of Register TX_LENGTH_BYTE_1
	gTrfFifoDataBuf[3] = 0x00; 	// Data of TX_LENGTH_BYTE_1
	gTrfFifoDataBuf[4] = 0x20; 	// Data of TX_LENGTH_BYTE_2
	gTrfFifoDataBuf[5] = ISO14443A_HLTA; // Data to be sent
	gTrfFifoDataBuf[6] = 0x00;
	
	TrfSendInventoryCommand(gTrfFifoDataBuf, 7);	

	//If the PICC responds with any modulation during a period of 1 ms after the end of the frame containing the
	//HLTA command, this response shall be interpreted as 'not acknowledge'.
	//NOTE> The PCD should apply an additional waiting time margin of 0,1 ms.

	// Wait about 2ms, It is not respond with data.
	Delay(SYS_TIMER_2MS);
	IrqClear();
}


//------------------------------------------------------------------------------
/** 	@brief	Send Anticollision
	@param 	[gIso14443aCascadeLevel] : 전송할 Cascade Level 결정 	
	@return 	[STATUS_SUCCESS] 전송 성공
	@return 	[STATUS_SUCCESS] 전송 실패 (NVB byte가 처리 범위를 넘어 바로 Select 전송 처리 필요)
	@remark Anticollision 처리를 위한 명령 전송
	@remark 4~7 byte의 UID Read
	@remark ISO14443A 규격 참조
	@remark
	@remark ========== ISO 14443 규정 ============================================
	@remark   1.Cascade Level 1 = 0x93 
	@remark			UID Size => Single			
	@remark			Number of UID bytes => 4
	@remark      Cascade Level 1 = 0x95 
	@remark 		UID Size => Double			
	@remark 		Number of UID bytes => 7
	@remark								
	@remark	  2.NVB = 0x20 							
	@remark               This value defines that the PCD will transmit no part of UID CLn.
	@remark			Consequently this command forces all PICCs in the field to respond with	their complete UID CLn. 						
	@remark										
	@remark			Collision 발생 이전 전송 Data => 0x00 + 0x20 + 0x93 + 0x20
	@remark			Collision 발생 이후 전송 Data => 0x00 + TxLengthByte2 + 0x93 + NVB
	@remark										+ 깨진 Byte전까지의 Data
	@remark										+ 깨진 Byte의 Bit까지의 Data
	@remark			ANTICOLLISION Command 	=> 	SEL+NVB
	@remark										SEL+NVB+UID CLn 				
	@remark =================================================================
*/
//------------------------------------------------------------------------------
BYTE Iso14443aAnticollisionSend(void)
{
	BYTE bCnt;
	BYTE Length;
	BYTE CollisionByteCnt;

	TrfSetRegister(ISO_CONTROL, 0x88);		//ISO Control (Address 01) = 0x88
											//	No Rx CRC,
											//	ISO14443A, bit rate 106kbit/s

	gTrfFifoDataBuf[0] = 0x8F;				// Reset Command

	// select command, otherwise anticollision command
	if(gIso14443aNVB == 0x70)		
	{
		gTrfFifoDataBuf[1] = 0x91;			// Transmit with CRC Command
	}
	else
	{
		gTrfFifoDataBuf[1] = 0x90;			// Transmit No CRC Command
	}

	gTrfFifoDataBuf[2] = 0x3D;				// Address of Register TX_LENGTH_BYTE_1
	gTrfFifoDataBuf[3] = 0x00;				// Data of TX_LENGTH_BYTE_1

	// Data of TX_LENGTH_BYTE_2, number of broken bits, last bit is 1 means broken byte
	gTrfFifoDataBuf[4] = gIso14443aNVB & 0xF0;		// number of complete bytes
	if((gIso14443aNVB & 0x07) != 0x00)
	{
		gTrfFifoDataBuf[4] |= ((gIso14443aNVB & 0x07) << 1) + 1; 	// number of broken bits, last bit is 1 means broken byte

	}
	
	gTrfFifoDataBuf[5] = gIso14443aCascadeLevel;		// can be 0x93, 0x95 or 0x97
	gTrfFifoDataBuf[6] = gIso14443aNVB;					// number of valid bits

	Length = 7;

	// 깨진 Bit가 발생한 Byte Count Load
	CollisionByteCnt = (gIso14443aNVB >> 4)-2;

	//깨진 Bit가 발생한 다음 Bit가 있는 Byte의 Data까지 Load
	if(gIso14443aNVB & 0x07)
	{
		CollisionByteCnt++;
	}

	if(CollisionByteCnt > 4)
	{
		// 처리 범위를 넘어서기 때문에 바로 Select 처리
		return (STATUS_FAIL);
	}

	//깨진 byte, 깨진 bit 다음까지의 Data 저장
	for(bCnt = 0; bCnt < CollisionByteCnt; bCnt++)
	{
		gTrfFifoDataBuf[7+bCnt] = gIso14443aTempUidBuf[bCnt]; 
		Length++;
	}

	TrfSendInventoryCommand(gTrfFifoDataBuf, Length); 

	return (STATUS_SUCCESS);
}



//------------------------------------------------------------------------------
/** 	@brief	Anticollision Respond Process
	@param 	[bStatus] IRQ Status
	@param 		- IRQ_RX_COMPLETED : IRQ Rx Complete
	@param 		- IRQ_COLLISION_ERROR : IRQ Collision Occurrence
	@return 	[STATUS_FAIL] : 규정에 맞지 않아 처리 실패
	@return 	[STATUS_SUCCESS] : UID 완성
	@return 	[STATUS_PROCESSING] : Collision 발생 등으로 추가 처리 필요
	@remark Anticollision에 응답한 결과 처리
	@remark 4~7 byte의 UID Read
	@remark ISO14443A 규격 참조
	@remark
	@remark ========== ISO 14443 규정 =============================================
	@remark   1.NVB										
	@remark		Upper 4 bit(Byte Count) : PCD에서 보내어지는 모든 valid data bit를 8로 나눈 정수값을 지정 
	@remark		(SEL, NVB 포함) (2~7) 		
	@remark		Lower 4 bit(Bit Count) : PCD에서 보내어지는 모든 valid data bit를 8로 나눈 값의 나머지를 지정 
	@remark		(SEL, NVB 포함) (0~7)
	@remark ==================================================================
*/
//------------------------------------------------------------------------------
BYTE Iso14443aAnticollisionRespond(BYTE bStatus)
{
	BYTE bCnt;
	BYTE FifoLength;
	BYTE bMask;
	BYTE CollisionPosition;
	BYTE CollisionByteCnt;
	BYTE CollisionBitCnt;
	
	if(bStatus == IRQ_RX_COMPLETED)
	{
		//No Collision
		if(gIsoCollisionSet)
		{					
			//카드가 2장 이상 들어와서 Data를 모두 읽었을 때의 처리
			CollisionByteCnt = (gIso14443aNVB >> 4)-2;
			CollisionBitCnt = gIso14443aNVB & 0x0F;
	
			if((CollisionByteCnt == 0) || (CollisionByteCnt > 4))
			{
				return (STATUS_FAIL);
			}

			//최대 수신 Byte = 5 Byte (UID(4)+BCC(1))
			FifoLength = 0x05 - (CollisionByteCnt-1);		

			if(FifoLength == 0)
			{
				return (STATUS_FAIL);
			}
	
			TrfGetRegisterData(FIFO, gTrfFifoDataBuf, FifoLength);

			if(CollisionByteCnt)
			{
				CollisionPosition = CollisionByteCnt-1;

//				bMask = MakeMaskUnbrokenBit(CollisionBitCnt);
//				gIso14443aTempUidBuf[bCollisionPosition] |= (gTrfFifoDataBuf[bCollisionPosition] & bMask);

				//위와 같이 별도의 Mask Data를 만들어서 OR해도 되지만, 
				//실제 깨진 Bit 이후부터 Data가 들어오기 때문에 
				//아래와 같이 그냥 OR만 해도 문제 없음.
				gIso14443aTempUidBuf[CollisionPosition] |= gTrfFifoDataBuf[CollisionPosition];
			}

			for(bCnt = CollisionByteCnt; bCnt < 5; bCnt++)
			{
				gIso14443aTempUidBuf[bCnt] = gTrfFifoDataBuf[bCnt];
			}
		}
		else
		{
			TrfGetRegisterData(FIFO_CONTROL, &FifoLength, 1);
			FifoLength &= 0x0F;				//How many bytes loaded in FIFO were not read out yet
			
			//TRF7970의 경우에는 1을 더할 필요가 없음, TRF7960은 Length에서 1을 더해서 FIFO를 읽어야 함.
//			if(FifoLength >= 4)			//For TRF7960
			if(FifoLength >= 5)		//For TRF7970
			{
				//Anticollision의 경우 모두 수신했을 경우가 5 Byte임
				FifoLength = 5;
				TrfGetRegisterData(FIFO, gIso14443aTempUidBuf, FifoLength);
			}
		}

		if(isUidBccOk(gIso14443aTempUidBuf) == STATUS_SUCCESS)
		{
			return (STATUS_SUCCESS);
		}
		else
		{
			return (STATUS_FAIL);
		}
	}
	else if(bStatus == IRQ_COLLISION_ERROR)
	{	
		//Collision error found
		gIsoCollisionSet = 1;

		TrfGetRegisterData(COLLISION_POSITION, &CollisionPosition, 1);

		// 1. Finish Byte count save
		if(CollisionPosition < 0x20)
		{
			// 규정에 맞지 않아 에러 처리
			return (STATUS_FAIL);
		}
		//The PCD shall set NVB only to values defined in Table 8 except that for byte counts 6 and 7 only bit count of 0
		//is allowed. A PCD setting NVB to any forbidden value is not compliant with this standard.
		//A PCD setting the byte count (b8 to b5) to any value outside the range 2 to 7 is not compliant with this
		//standard. A PCD setting the bit count (b4 to b1) > 7 for byte count equal 2 to 5 or setting the bit count (b4 to
		//b1) to any value other than 0 for byte count equal 6 or 7 is not compliant with this standard.
		else if((CollisionPosition == 0x60) || (CollisionPosition == 0x70))
		{
			// 처리할 수 있는 ID 이상이 되었기 때문에 추가 UID 처리, 실제의 경우 확인이 어려워 테스트 못해 봄.			
			AntiColVariInit();

			CascadeLevelSet(2);
			return (STATUS_PROCESSING);
		}

		gIso14443aNVB = CollisionPosition;	

		CollisionByteCnt = (gIso14443aNVB >> 4)-2;			//SEL, NVB 제외	
		CollisionBitCnt = gIso14443aNVB & 0x0F;

		// 정상적인 경우가 아닐 경우 FAIL 처리
		if((CollisionByteCnt > 3) || (CollisionBitCnt > 7)) 
		{
			return (STATUS_FAIL);
		}

		// 2. Splite Bit count save
		// 3. Next NVB save

		//깨진 Data 다음부터 읽기 위해 (처리 범위를 넘지 않기 위해 Mask)
		gIso14443aNVB = (gIso14443aNVB+1) & 0x77;
				
		if(gIso14443aNVB & 0x0F)
		{
			CollisionBitCnt++; 	
		}
		else
		{
			gIso14443aNVB += 0x10;

			CollisionBitCnt = 0x00;
		}
		CollisionByteCnt++;
		
		// 4. FIFO Read
		//Anticollision이 발생한 Byte까지 Read
		TrfGetRegisterData(FIFO, gTrfFifoDataBuf, CollisionByteCnt);

		// 5. Save Get UID
		for(bCnt = 0; bCnt < CollisionByteCnt; bCnt++)
		{
			//Anticollision이 발생한 Byte까지의 Data 저장
			gIso14443aTempUidBuf[bCnt] = gTrfFifoDataBuf[bCnt];
		}

		if(CollisionBitCnt)
		{
			//Anticollision이 발생한 Byte의 Anticollision이 발생한 bit까지의 Data 저장
			bMask = MakeMaskBrokenBit(CollisionBitCnt);
		
			gIso14443aTempUidBuf[CollisionByteCnt-1] &= bMask;	
		}
	}

	return (STATUS_PROCESSING);
}




//------------------------------------------------------------------------------
/** 	@brief	Send Select
	@param 	[gIso14443aCascadeLevel : Cascade Level 결정 	
	@return 	None
	@remark Select 처리를 위한 명령 전송
	@remark 4~7 byte의 UID Read
	@remark ISO14443A 규격 참조
	@remark 
	@remark ========== ISO 14443 규정 =================================
	@remark     If no further collision occurs, the PCD shall assign NVB	with the value of '70'.
	@remark     This value defines that the PCD will transmit the complete UID CLn.
	@remark 	SELECT Command => SEL+NVB+UID CLn+CRC_A	
	@remark ======================================================
*/
//------------------------------------------------------------------------------
void Iso14443aSelectSend(void)
{
	TrfSetRegister(ISO_CONTROL, 0x08);					//ISO Control (Address 01) = 0x08
														//	Rx CRC,
														//	ISO14443A, bit rate 106kbit/s

	gTrfFifoDataBuf[0] = 0x8F; 	// Reset Command
	gTrfFifoDataBuf[1] = 0x91; 	// Transmit CRC Command
	gTrfFifoDataBuf[2] = 0x3D; 	// Address of Register TX_LENGTH_BYTE_1
	gTrfFifoDataBuf[3] = 0x00; 	// Data of TX_LENGTH_BYTE_1
	gTrfFifoDataBuf[4] = 0x70; 	// Data of TX_LENGTH_BYTE_2

	gTrfFifoDataBuf[5] = gIso14443aCascadeLevel;		// Select
	gTrfFifoDataBuf[6] = 0x70;							// 0x70 is NVB.
	gTrfFifoDataBuf[7] = gIso14443aTempUidBuf[0];		// ISO14443A UID
	gTrfFifoDataBuf[8] = gIso14443aTempUidBuf[1];		
	gTrfFifoDataBuf[9] = gIso14443aTempUidBuf[2];		
	gTrfFifoDataBuf[10] = gIso14443aTempUidBuf[3];		
	gTrfFifoDataBuf[11] = gIso14443aTempUidBuf[4];		// ISO14443A UID BCC
	
	TrfSendInventoryCommand(gTrfFifoDataBuf, 12);	
}


//------------------------------------------------------------------------------
/** 	@brief	Select Respond Process
	@param 	None
	@return 	[SAK_FAIL] : 처리 실패
	@return 	[SAK_UID_NOT_COMPLETE] : UID not complete
	@return 	[SAK_UID_COMPLETE] : UID complete
	@remark Select에 응답한 결과 처리
	@remark 4~7 byte의 UID Read
	@remark ISO14443A 규격 참조
	@remark 
	@remark ========== ISO 14443 규정 ====================================
	@remark    SAK(Select Acknowledge) : NVB가 40 valid data bit로 지정되었거나 모든 data bit가 
	@remark 	UID CLn과 일치하였을 때 PICC가 보냄
	@remark 		bit3=1(& 0x04) => Cascade bit set;UID not complete	
	@remark 		bit6=1(& 0x20) => UID complete, PICC compliant with ISO/IEC 14443-4
	@remark 		bit3=0 && bit6=0 => UID complete, PICC not compliant with ISO/IEC 14443-4
	@remark =========================================================
*/
//------------------------------------------------------------------------------
BYTE Iso14443aSelectRespond(void)
{
	BYTE FifoLength;
	
	TrfGetRegisterData(FIFO_CONTROL, &FifoLength, 1);
	FifoLength &= 0x0F;

	// TRF7970에서는 불필요, TRF7960에서도 사용해야 함.
//	FifoLength++;
	
	if(FifoLength > 12 || FifoLength == 0)
	{
		return (SAK_FAIL);
	}
	
	TrfGetRegisterData(FIFO, gTrfFifoDataBuf, FifoLength);

	//SAK Check
//#ifdef 	IREVO_CARD_ONLY
	if(IsCardOpenUidSupported() == false)
	{
	// 마이페어 1K만 통과
	if(gTrfFifoDataBuf[0] == 0x08)
	{
		//UID complete
		return (SAK_UID_COMPLETE);
	}
	else if((gTrfFifoDataBuf[0] & 0x24) == 0x04)
	{
		//ISO14443-4 지원하지 않는 7byte 카드
		return (SAK_UID_NOT_COMPLETE);
	}
	else
	{
		return (SAK_FAIL);
	}
	}
//#else
	else
	{
	// OPEN UID, 중국, 한국을 제외한 해외향은 모두 통과 되도록 수정.(해외마케팅 요청)
	if(gTrfFifoDataBuf[0] & 0x04)
	{
		// ISO14443-4 지원하지 않거나 하지 않는  7byte 카드
		return (SAK_UID_NOT_COMPLETE);
	}
	else
	{
		//UID complete
		return (SAK_UID_COMPLETE);
	}
	}
//#endif
}



