//------------------------------------------------------------------------------
/** 	@file		PackTxFunctions_T1.h
	@brief	Communication Pack TX Process Functions
*/
//------------------------------------------------------------------------------


#ifndef __PACKTXFUNCTIONS_T1_INCLUDED
#define __PACKTXFUNCTIONS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




extern WORD gbPackTxByteNum;
extern WORD gbPackTxByteCnt;
extern BYTE gbPackTxDataBuf[MAX_PACK_BUF_SIZE];
extern bool gfAckWait;

extern BYTE gbPackTxRandomKeybuffer[16];
extern BYTE gbpackModeSuspend;


extern BYTE gbPackLowBatteryReportMode; 
extern BYTE gbPackLowBatteryReportTimer1s;
extern WORD gbPackLockStatusReportTimer;

#define	MAX_PACK_TX_QUEUE_SIZE		12


// gPackTxProcessStep�� 
enum{
	PACK_TX_WAITING_MODE = 0,
	PACK_TX_PACKET_CHECKING_MODE,
	PACK_TX_EN_SIG_TI_SENDING_WAIT,
	PACK_TX_EN_SIG_TI_SENDING_MODE,
	PACK_TX_EN_SIG_TWU_SENDING_MODE,
	PACK_TX_PACKET_SENDING_MODE,
	PACK_TX_ACK_WAITING_MODE,
	PACK_TX_ENCRYPT_MODE

//	PACK_TX_START_CHECK,	
//	PACK_TX_START_ENSIG,
//	PACK_TX_SENDING_ENSIG,	
//	PACK_TX_START_DATA,
//	PACK_TX_SENDING_DATA,	
//	PACK_TX_ACK_WAITING,
};



BYTE Get_Checksum_BCC(BYTE *buf, BYTE len);
WORD Get_Checksum_CRC16(BYTE *buf, BYTE len);
void PackTx_AddChecksum(BYTE *buf, WORD *len);

void PackTx_QueueInit(void);
bool PackTx_QueueIsEmpty(void);
bool PackTx_QueueIsFull(void);
void PackTxWaitTime_EnQueue(BYTE TxWaitTime);
void PackTx_EnQueue(void);
void PackTx_EnQueueAtFirst(void);
void PackTx_DeQueueStart(void);
void PackTx_DeQueueEnd(void);
void PackTx_MakePacketSinglePort(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength);
void PackTx_MakePacket(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength);
void PackTx_MakePacketForInitial(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength);
void PackTx_MakeAndSendPacketNow(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength);
void PackTx_MakeNack(BYTE* bpData , BYTE bMajor_Code, BYTE bMinor_Code);
void PackTx_MakeAlarmPacket(BYTE bAlarmType, BYTE bAlarmLevel_H, BYTE bAlarmLevel_L);



void PackDataTransmitProcess(void);

void PackTxEnSigSetDefault(void);
void PackTxEnSigTimeSet(BYTE* pTimeData);

void PackTxProcessStepClear(void);
BYTE GetPackTxProcessStep(void);


void PackTxProcessTimeCounter(void);

void PackTxWaitTimeCounter(void);

void PackTxAckWaitTimeCounter(void);

extern BYTE gPackTxAckWaitTimer100ms;
void PackTxAckClearCheck(BYTE* bpData);
void PackTxAckWaitCallback(void);




void PackTxEndProcess(void);


void PackTxDataProcess(void);





void CopySlotNumberForPack(WORD SlotNumber);
WORD GetSlotNumberForPack(void);
void CopyCredentialDataForPack(BYTE* bDataBuf, BYTE bNum);
void GetCredentialDataForPack(BYTE* bDataBuf, BYTE bNum);

void PackTxEventPincodeAdded(WORD wUserSlot);
void PackTxEventPincodeDeleted(WORD wUserSlot);
void PackTxEventCredentialAdded(BYTE CredentialType, WORD wUserSlot);
void PackTxEventCredentialDeleted(BYTE CredentialType, WORD wUserSlot);

void PackAlarmReportInnerForcedLockOpenCloseFailSend(void);

void PackTxEncapsulateData(BYTE *bQueueSendingData , uint16_t length);
void PackTxGetCheckSumOfExisting(void);
BYTE PackTxEncryptionKeyExchange(BYTE* bCheckSumData);

void PackAlarmReportLowBatterySet(void);
void PackAlarmReportLowBatteryCounter(void);
void PackTxAlarmReportLowBatteryProcess(void);
void ZWaveZigbeeLockStatusAlarmRepot(void);
void PackTxLockOperationbyAccessoryAckSend(BYTE Lowbuff, BYTE Highbuff);





#ifdef	DDL_CFG_ADD_REMOCON_LINK_FUNCTION
void PackTxRemoconLinkSend(BYTE bData);
#endif

#endif


