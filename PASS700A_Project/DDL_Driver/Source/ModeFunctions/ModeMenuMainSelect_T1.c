//------------------------------------------------------------------------------
/** 	@file		ModeMenuMode_T1.c
	@version 0.1.00
	@date	2016.04.19
	@brief	Menu Mode 
	@remark	메뉴 모드
	@see	MainModeProcess_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.19		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gMenuSelectKeyTempSave = TENKEY_NONE;			/**< 설정된 메뉴 값을 저장하기 위한 변수  */
BYTE gSelectedSlotNumber = 0;


BYTE GetSupportCredentialCount(void)
{
	/* 현재 지원 하는 Credential 의 수를 return 한다 */
	BYTE bSupportCredential = 0;

#ifdef 	DDL_CFG_RFID	
	bSupportCredential++;
#endif 

#ifdef 	DDL_CFG_FP	
	bSupportCredential++;
#endif 

#ifdef 	DDL_CFG_IBUTTON	
	bSupportCredential++;
#endif 

#ifdef 	DDL_CFG_DS1972_IBUTTON	
	bSupportCredential++;
#endif 

	return bSupportCredential;
}
	
void ModeGotoMenuAdvancedMain()
{
	gbMainMode = MODE_MENU_MAIN_SELECT;
	gbModePrcsStep = 0;
}

void ModeGotoMenuNormalMain()
{
	gbMainMode = MODE_MENU_MAIN_SELECT;
	gbModePrcsStep = 1;
}


void ModeGotoMenuMainSelect()
{
	if(gbManageMode == _AD_MODE_SET)
	{
		ModeGotoMenuAdvancedMain();
	}
	else
	{
		ModeGotoMenuNormalMain();
	}
}



void ReInputOrModeMove(void(*BackwardMenuMode)(void))
{
	if(GetInputKeyCount() == 0)
	{		
		//	번호 입력 없이 바로 star버튼 입력 시 이전 모드로 이동
		BackwardMenuMode();
	}
	else
	{					
		// 번호 입력 중 star버튼 입력시 번호 재입력으로 이동. 
		gbModePrcsStep = 0;
	}
	TenKeyVariablesClear();
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
}



void ReInputOrModeMove_1(void(*BackwardMenuMode)(void), void(*ReInputProcess)(void))
{
	if(GetInputKeyCount() == 0)
	{		
		//	번호 입력 없이 바로 star버튼 입력 시 이전 모드로 이동
		BackwardMenuMode();
	}
	else
	{					
		// 번호 입력 중 star버튼 입력시 번호 재입력으로 이동. 
		ReInputProcess();
	}
	TenKeyVariablesClear();
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
}


void GotoPreviousOrComplete(void(*BackwardMenuMode)(void))
{
	switch(gbInputKeyValue)
	{
		case TENKEY_STAR:
			BackwardMenuMode();
			break;

		case FUNKEY_REG:
//			FeedbackModeCompleted(1, VOL_CHECK);
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
			break;

		default:
			if(GetModeTimeOut()== 0)
			{
//				FeedbackModeCompleted(1, VOL_CHECK);
				FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
				ModeClear();
			}
			break;
	}
}



void MenuSelectKeyTempSaveNTimeoutReset(WORD VoiceData, BYTE MenuKey)
{
	FeedbackTenKeyBlink(VoiceData, VOL_CHECK, MenuKey);

	gMenuSelectKeyTempSave = MenuKey; 
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
}



void AssignAdvancedMainSelectedMenu(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		case TENKEY_1:		//	Master Code변경. 
			ModeGotoMasterCodeRegister();
			break;
	
		case TENKEY_2:		// 비밀번호 개별 등록/삭제
			ModeGotoOneUserCodeRegDelCheck();
			break;
		
		case TENKEY_3:		// 카드/지문/터치키 개별 등록/삭제
			if(GetSupportCredentialCount() > 1)
			{
				ModeGotoMenuAdvancedCredentialSelect();
			}
			else 
			{
#ifdef	DDL_CFG_RFID
				ModeGotoOneCardRegDelCheck();
#endif
#ifdef	DDL_CFG_FP
				ModeGotoOneFingerRegDelCheck();
#endif

#ifdef	DDL_CFG_IBUTTON
				ModeGotoOneGMTouchKeyRegDelCheck();
#endif

#ifdef	DDL_CFG_DS1972_IBUTTON
				ModeGotoOneDS1972TouchKeyRegDelCheck();
#endif
			}
			break;
			
		case TENKEY_4:		// 1회용 비밀번호 등록 /삭제. 
			ModeGotoOnetimeCodeRegDelCheck();
			break;
	
		case TENKEY_5:		// 인증 수단 전체 삭제.
			ModeGotoAllCredentialsDelete();
			break;		
		
		case TENKEY_6:		// 메뉴 세팅. 
			ModeGotoMenuLockSetting();
			break;

#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
		case TENKEY_7:		//CBA key 교환 
			ModeGotoExchangeKeySet();
			break;
#endif 			

#ifndef __DDL_MODEL_YMH70A			
		case TENKEY_8:		//	리모컨 개별 등록/삭제
			ModeGotoOneRemoconRegDelCheck();
			break;

#ifndef __DDL_MODEL_B2C15MIN							
		case TENKEY_9:		// 연동기 등록/삭제
			ModeGotoRfLinkModuleRegDelCheck();
			break;
#endif 			
#endif	
		default:
			// 테스트 모드 진입을 위해 
			// 메뉴키 입력 없이 #키가 바로 입력되고, 3초 동안 유지되면 테스트 모드 진입
			SetModeTimeOut(30);					// 3sec. 3초간 #누름 유지시 TEST모드 진입을 위함. 
			gbModePrcsStep++;
			break;
	}
}

void MenuAdvancedMainSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		case TENKEY_1:			// Master Code변경
//			MenuSelectKeyTempSaveNTimeoutReset(46, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_MPIN_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_2:			// 비밀번호 개별 등록/삭제
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(GetPackCBARegisterSet())
			{
				// Advanced Mode에서 등록된 CBA 모듈이 장착될 경우 CBA 등록/삭제를 제외한 나머지 메뉴 설정 모드는 모두 disable
				break;
			}
#endif

//			MenuSelectKeyTempSaveNTimeoutReset(6, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_PIN_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_3:			// 카드/지문/터치키 개별 등록/삭제
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(GetPackCBARegisterSet())
			{
				// Advanced Mode에서 등록된 CBA 모듈이 장착될 경우 CBA 등록/삭제를 제외한 나머지 메뉴 설정 모드는 모두 disable
				break;
			}
#endif

			if(GetSupportCredentialCount() > 1)
			{
				MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_DIGITALKEY_SHARP, gbInputKeyValue);
			}
			else 
			{		
#ifdef	DDL_CFG_FP
//				MenuSelectKeyTempSaveNTimeoutReset(67, gbInputKeyValue); //지문 등록/삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
				MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_FINGER_SHARP, gbInputKeyValue); //지문 등록/삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
#else
//				MenuSelectKeyTempSaveNTimeoutReset(59, gbInputKeyValue);	
				MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_CARD_SHARP, gbInputKeyValue);	
#endif
			}
			break;

		case TENKEY_4:			// 1회용 비밀번호 등록 /삭제. 
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(GetPackCBARegisterSet())
			{
				// Advanced Mode에서 등록된 CBA 모듈이 장착될 경우 CBA 등록/삭제를 제외한 나머지 메뉴 설정 모드는 모두 disable
				break;
			}
#endif

//			MenuSelectKeyTempSaveNTimeoutReset(19, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_OPIN_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_5:			// 인증수단 전체 삭제
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(GetPackCBARegisterSet())
			{
				// Advanced Mode에서 등록된 CBA 모듈이 장착될 경우 CBA 등록/삭제를 제외한 나머지 메뉴 설정 모드는 모두 disable
				break;
			}
#endif

//			MenuSelectKeyTempSaveNTimeoutReset(54, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_ALLCRE_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_6:			// 메뉴 세팅. 
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(GetPackCBARegisterSet())
			{
				// Advanced Mode에서 등록된 CBA 모듈이 장착될 경우 CBA 등록/삭제를 제외한 나머지 메뉴 설정 모드는 모두 disable
				break;
			}
#endif

//			MenuSelectKeyTempSaveNTimeoutReset(17, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_LOCK_SET_MODE_SHARP, gbInputKeyValue);	
			break;

#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
		case TENKEY_7:
			if(gbModuleProtocolVersion >= 0x30 || gbInnerModuleProtocolVersion >= 0x30)
			{
				MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_BT_SHARP, gbInputKeyValue);	// CBA key 교환 
			}
			else
			{
				TimeExpiredCheck();
			}
			break;
#endif 			

#ifndef __DDL_MODEL_YMH70A	
		case TENKEY_8:
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(GetPackCBARegisterSet())
			{
				// Advanced Mode에서 등록된 CBA 모듈이 장착될 경우 CBA 등록/삭제를 제외한 나머지 메뉴 설정 모드는 모두 disable
				break;
			}
#endif			

			if(PackConnectedCheck() || InnerPackConnectedCheck())
			{
//				MenuSelectKeyTempSaveNTimeoutReset(20, gbInputKeyValue);	
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU) || defined (__DDL_MODEL_B2C15MIN)||defined (__DDL_MODEL_YMH70A)
				MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_SMART_LIVING_SHARP, gbInputKeyValue);	// 스마트리빙 등록/삭제 모드입니다. 계속 하시려면 샵 버튼을 누르세요. 
#else 				
				MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_REMOCON_SHARP, gbInputKeyValue);	
#endif 
			}
			else
			{
				TimeExpiredCheck();
			}
			break;

#ifndef __DDL_MODEL_B2C15MIN	
		case TENKEY_9:
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(GetPackCBARegisterSet())
			{
				// Advanced Mode에서 등록된 CBA 모듈이 장착될 경우 CBA 등록/삭제를 제외한 나머지 메뉴 설정 모드는 모두 disable
				break;
			}
#endif

			if(PackConnectedCheck() || InnerPackConnectedCheck())
			{
//				MenuSelectKeyTempSaveNTimeoutReset(25, gbInputKeyValue);	
				MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_HOMENET_SHARP, gbInputKeyValue);	
			}
			else
			{
				TimeExpiredCheck();
			}
			break;
#endif 
#endif			
		case TENKEY_SHARP:
			AssignAdvancedMainSelectedMenu();
			break;
	
		case TENKEY_STAR:
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}



}


void AssignNormalMainSelectedMenu(void)
{
	switch(gMenuSelectKeyTempSave)
	{
#if defined (DDL_CFG_MS)
#ifdef	DDL_CFG_RFID
		case TENKEY_1:		// Manager Card 등록 삭제 
		case TENKEY_2:		// Master Card 등록 삭제
		case TENKEY_3:		// User Card 등록 삭제		
#endif 		
		case TENKEY_4:		// Manager Code 등록 삭제 		
		case TENKEY_5:		// Master Code 등록 삭제 		
		case TENKEY_6:		// User Code 등록 삭제 
			MS_Set_Selected_Item(gMenuSelectKeyTempSave);
			MS_ModeGotoCardCodeRegDelCheck();
			break;

		case TENKEY_7:		// 메뉴 세팅
			MS_Set_Selected_Item(gMenuSelectKeyTempSave);

			MS_ModeGotoAuthorityVerify();
			break;		
#else 
		case TENKEY_1:		// 방문자 번호 등록/삭제
			ModeGotoVisitorCodeRegDelCheck();
			break;
		
		case TENKEY_2:		// 1회용 비밀번호
			ModeGotoOnetimeCodeRegDelCheck();
			gbModePrcsStep = 0;
			break;
	
		case TENKEY_3:		
			if(GetSupportCredentialCount() > 1)
			{
				ModeGotoMenuNormalCredentialSelect();
			}
			else 
			{
#ifdef	DDL_CFG_RFID
                        //카드 전체 삭제
				ModeGotoAllCardDelete();
#endif
#ifdef	DDL_CFG_FP
                        // 지문 전체 삭제
				ModeGotoNormalAllFingerDelete();
#endif
#ifdef	DDL_CFG_IBUTTON
						//터치키 전체 삭제
				ModeGotoAllGMTouchKeyDelete();
#endif

#ifdef	DDL_CFG_DS1972_IBUTTON
			//터치키 전체 삭제
				ModeGotoAllDS1972TouchKeyDelete();
#endif
			}
			break;
	
		case TENKEY_4:		// 메뉴 세팅
			ModeGotoMenuLockSetting();
			break;		

//#ifndef __DDL_MODEL_PANPAN_FC2A	//PANPAN은 NormalMode에서 CBA Key 교환 메뉴와 리모컨 등록/삭제 메뉴를 지원 하지 않는다.(키패드 있는 모델은 모두 지원 해야 하지 않는가?)

#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
		case TENKEY_7:		//CBA key 교환 
			ModeGotoExchangeKeySet();
			break;
#endif 		

//#endif	//__DDL_MODEL_PANPAN_FC2A #endif			

#ifndef __DDL_MODEL_YMH70A
		case TENKEY_8:		//	리모컨 등록/삭제
			ModeGotoRemoconRegDelCheck();
			break;

#ifndef __DDL_MODEL_B2C15MIN			
		case TENKEY_9:		// 연동기 등록/삭제
			ModeGotoRfLinkModuleRegDelCheck();
			break;
#endif
#endif

#endif 	
	
		// 테스트 모드 진입을 위해 
		default:
			// 테스트 모드 진입을 위해 
			// 메뉴키 입력 없이 #키가 바로 입력되고, 3초 동안 유지되면 테스트 모드 진입
			SetModeTimeOut(30);					// 3sec. 3초간 #누름 유지시 TEST모드 진입을 위함. 
			gbModePrcsStep++;
			break;
	}
}


void MenuNormalMainSelectProcess(void)
{
#if defined (DDL_CFG_MS)
	BYTE bTemp = 0x00;
#endif 

	switch(gbInputKeyValue)
	{
#if defined (DDL_CFG_MS)
		/* code size 줄일라고 ... 근데 이게 줄어 드나 ...  */
#ifdef	DDL_CFG_RFID
		case TENKEY_1:			// Manager Card 등록 삭제 
		case TENKEY_2:			// Master Card 등록 삭제 
		case TENKEY_3:			// User Card 등록 삭제 
#endif 		
		case TENKEY_4:			// Manager Code 등록 삭제 
		case TENKEY_5:			// Master Code 등록 삭제 
		case TENKEY_6:			// User Code 등록 삭제 
			bTemp = (gbInputKeyValue == TENKEY_1) ? MANAGER_CARD_REG_DEL:
					(gbInputKeyValue == TENKEY_2) ? MASTER_CARD_REG_DEL:
					(gbInputKeyValue == TENKEY_3) ? USER_CARD_REG_DEL:
					(gbInputKeyValue == TENKEY_4) ? MANAGER_CODE_REG_DEL:
					(gbInputKeyValue == TENKEY_5) ? MASTER_CODE_REG_DEL:
					(gbInputKeyValue == TENKEY_6) ? USER_CODE_REG_DEL:0x00;
						
			if(Ms_Credential_Info.bCredential_Info_Set_Complete & bTemp)
			{
				if(gbInputKeyValue < TENKEY_4)
					MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_CARD_SHARP, gbInputKeyValue);		
				else 
					MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_PIN_SHARP, gbInputKeyValue);							
			}
			else 
			{
				TimeExpiredCheck();
			}
			break;	

		case TENKEY_7:			// 록 세팅 메뉴
			MenuSelectKeyTempSaveNTimeoutReset(AVML_LOCK_SET_MODE_SHARP, gbInputKeyValue);		//설정 모드 입니다. 계속하시려면 샵 버튼을 누르세요.
			break;			
#else 
		case TENKEY_1:			// 방문자 번호 등록/삭제
//			MenuSelectKeyTempSaveNTimeoutReset(10, gbInputKeyValue);		//방문자 비밀번호 등록 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_VPIN_SHARP, gbInputKeyValue);		//방문자 비밀번호 등록 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			break;

		case TENKEY_2:			// 1회용 비밀번호
//			MenuSelectKeyTempSaveNTimeoutReset(19, gbInputKeyValue);		//일회용 비밀번호 등록/삭제 모드 입니다. 계속하시려면 샵 버튼을 누르세요.
			MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_OPIN_SHARP, gbInputKeyValue);		//일회용 비밀번호 등록/삭제 모드 입니다. 계속하시려면 샵 버튼을 누르세요.
			break;

		case TENKEY_3:			// 카드/지문 전체 삭제
			if(GetSupportCredentialCount() > 1)
			{
				MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_ALLCRE_SHARP, gbInputKeyValue);
			}
			else 
			{
#ifdef	DDL_CFG_RFID
//				MenuSelectKeyTempSaveNTimeoutReset(57, gbInputKeyValue);		//카드키 전체삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
				MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_ALLCARD_SHARP, gbInputKeyValue);		//카드키 전체삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
#endif

#ifdef	DDL_CFG_FP
//				MenuSelectKeyTempSaveNTimeoutReset(69, gbInputKeyValue);		//지문 (전체) 삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
				MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_ALLFINGER_SHARP, gbInputKeyValue);		//지문 (전체) 삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
#endif			

#ifdef	DDL_CFG_IBUTTON
			//적당한 음원이 현재 없음
//				MenuSelectKeyTempSaveNTimeoutReset(57, gbInputKeyValue);		
				MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_ALLCARD_SHARP, gbInputKeyValue);		
#endif	

#ifdef	DDL_CFG_DS1972_IBUTTON
			//적당한 음원이 현재 없음
				//MenuSelectKeyTempSaveNTimeoutReset(57, gbInputKeyValue);
				MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_ALLCARD_SHARP, gbInputKeyValue);		
#endif		
			}
                        break;

		case TENKEY_4:			// 록 세팅 메뉴
//			MenuSelectKeyTempSaveNTimeoutReset(17, gbInputKeyValue);		//설정 모드 입니다. 계속하시려면 샵 버튼을 누르세요.
			MenuSelectKeyTempSaveNTimeoutReset(AVML_LOCK_SET_MODE_SHARP, gbInputKeyValue);		//설정 모드 입니다. 계속하시려면 샵 버튼을 누르세요.
			break;
#endif 

#ifdef	_NEW_YALE_ACCESS_UI
#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
		case TENKEY_7:
			if(gbModuleProtocolVersion >= 0x30 || gbInnerModuleProtocolVersion >= 0x30)
			{
				MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_BT_SHARP, gbInputKeyValue);	// CBA key 교환 
			}
			else
			{
				TimeExpiredCheck();
			}
			break;
#endif 			
#endif	// _NEW_YALE_ACCESS_UI

#ifndef __DDL_MODEL_YMH70A
		case TENKEY_8:
#if !defined (DDL_CFG_MS)
			if(PackConnectedCheck() || InnerPackConnectedCheck())
			{
//				MenuSelectKeyTempSaveNTimeoutReset(20, gbInputKeyValue);	
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU) || defined (__DDL_MODEL_B2C15MIN)
				MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_SMART_LIVING_SHARP, gbInputKeyValue);	// 스마트리빙 등록/삭제 모드입니다. 계속 하시려면 샵 버튼을 누르세요. 
#else
				MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_REMOCON_SHARP, gbInputKeyValue);	// 스마트리빙 등록/삭제 모드입니다. 계속 하시려면 샵 버튼을 누르세요. 
#endif 				
			}
			else
#endif  				
			{
				TimeExpiredCheck();
			}			
			break;

#ifndef __DDL_MODEL_B2C15MIN
		case TENKEY_9:
#if !defined (DDL_CFG_MS)			
			if(PackConnectedCheck() || InnerPackConnectedCheck())
			{
//				MenuSelectKeyTempSaveNTimeoutReset(25, gbInputKeyValue);	
				MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_HOMENET_SHARP, gbInputKeyValue);	
			}
			else
#endif 				
			{
				TimeExpiredCheck();
			}			
			break;
#endif 			
#endif

		case TENKEY_SHARP:
			AssignNormalMainSelectedMenu();
			break;

#if defined (DDL_CFG_MS)
		case TENKEY_STAR:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;			
		case FUNKEY_REG:
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
			break;
#else 
		case TENKEY_STAR:
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;			
#endif 
		default:
			TimeExpiredCheck();
			break;
	}
}



void TestModeEntryConditionCheck(void)
{
	if(gbNewKeyBuffer != TENKEY_SHARP)
	{
		gbModePrcsStep--;
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
	else
	{
		if(GetModeTimeOut()== 0)
		{
			ModeGotoTestMenu();
		}
	}
}

//------------------------------------------------------------------------------
/** 	@brief	Menu Select Mode
	@param	None
	@return 	None
	@remark 메뉴 모드 시작
*/
//------------------------------------------------------------------------------
void ModeMenuMainSelect(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackAdvancedMenuOn(2, VOL_CHECK);
			FeedbackAdvancedMenuOn(AVML_SELECT_MENU, VOL_CHECK);

			gMenuSelectKeyTempSave = TENKEY_NONE;
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep = 11;
			break;
			
		case 1:
//			FeedbackNormalMenuOn(2, VOL_CHECK);
			FeedbackNormalMenuOn(AVML_SELECT_MENU, VOL_CHECK);

			gMenuSelectKeyTempSave = TENKEY_NONE;
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep = 15;
			break;
		
		case 11:
			MenuAdvancedMainSelectProcess();
			break;

		// 테스트 모드 진입을 위해 
		case 12:
		case 16:
			TestModeEntryConditionCheck();
			break;

		//Menu Mode in Normal mode	
		case 15:							
			MenuNormalMainSelectProcess();	
			break;

		default:
			ModeClear();
			break;
	}
}

#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
void ModeGotoExchangeKeySet(void)
{
	gbMainMode = MODE_EXCHANGE_KEY_SET;
	gbModePrcsStep = 0;
}

void ModeExchangeKeySet(void)
{
	BYTE bTmpkeyNull[16]  = {0x00,};

	switch(gbModePrcsStep)
	{
		case 0:
#ifdef DDL_AAAU_REG_BUTTON_UI		//FDS는 등록 버튼을 누르면 암호화 키를 교환 하게 되는데 경보 발생시 등록 버튼을 이용한 경보를 해제를 하기 위함.		
			if(AlarmStatusCheck() == STATUS_SUCCESS)
			{
				ModeGotoAlarmClearByKey(FUNKEY_REG);
				break;
			}
#endif			
			if(gbModuleProtocolVersion >= 0x30 || gbInnerModuleProtocolVersion >= 0x30)
			{
				if(gbModuleProtocolVersion >= 0x30)
				{
					RandomNumberGenerator(gbPackTxRandomKeybuffer,16);
					PackTx_MakePacketSinglePort(F0_ENCRYPTION_KEY_CHANGE, ES_LOCK_EVENT, &gbComCnt, gbPackTxRandomKeybuffer,16);
					gbMenuExchangeKey = 0x01;
				}

				if(gbInnerModuleProtocolVersion >= 0x30)				
				{
					RandomNumberGenerator(gbInnerPackTxRandomKeybuffer,16);
					InnerPackTx_MakePacketSinglePort(F0_ENCRYPTION_KEY_CHANGE, ES_LOCK_EVENT, &gbInnerComCnt, gbInnerPackTxRandomKeybuffer,16);
					gbMenuInnerExchangeKey = 0x01;
				}				

				gbModePrcsStep = 2;
				SetModeProcessTime(200);
			}
			else
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
			}
		break;

		case 2: 		
			if(GetModeProcessTime() == 0x00)
			{
				gbMenuInnerExchangeKey = gbMenuExchangeKey = 0x00;				
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;
			}

			if(memcmp(gbPackTxRandomKeybuffer,bTmpkeyNull,16) == 0x00 && gbMenuExchangeKey == 0x01)
			{
				// gbPackTxRandomKeybuffer 가 0x00 이 되면 완료 
				uint8_t bTmp = PA_RF_MODULE_REGISTER_TIMEOUT;
				
				gbModuleMode = PACK_ID_CONFIRMED;

				if(gfPackTypeCBABle)
				{
					PackTx_MakePacketSinglePort(F0_RF_MODULE_REGISTER, ES_LOCK_EVENT, &gbComCnt, &bTmp, 1);
				}
				gbMenuExchangeKey = 0x00;
			}

			if(memcmp(gbInnerPackTxRandomKeybuffer,bTmpkeyNull,16) == 0x00 && gbMenuInnerExchangeKey == 0x01)
			{
				// gbPackTxRandomKeybuffer 가 0x00 이 되면 완료 
				
				uint8_t bTmp = PA_RF_MODULE_REGISTER_TIMEOUT;
				
				gbInnerModuleMode = PACK_ID_CONFIRMED;

				if(gfInnerPackTypeCBABle)
				{
					InnerPackTx_MakePacketSinglePort(F0_RF_MODULE_REGISTER, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 1);
				}
				gbMenuInnerExchangeKey = 0x00;
			}

			if(gbMenuExchangeKey == 0x00 && gbMenuInnerExchangeKey == 0x00)
			{
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
#ifndef	_NEW_YALE_ACCESS_UI
				SetPackCBARegisterSet(1);
#endif	// _NEW_YALE_ACCESS_UI
#endif

				FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
				ModeClear();
			}
			break;	

		default:
			ModeClear();
			break;

	}
}
#endif 


