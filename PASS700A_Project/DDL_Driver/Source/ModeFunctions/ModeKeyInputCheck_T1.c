//------------------------------------------------------------------------------
/** 	@file		ModeKeyInputCheck_T1.c
	@version 0.1.00
	@date	2016.04.08
	@brief	Key Input Wait Mode 
	@remark	 대기 상태에서 키 입력이 있는 지 확인하는 모드
	@see	MainModeProcess_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.08		by Jay
			- 4byte UID 카드 및 7byte UID 카드를 모두 읽어서 해당 Data를 출력
*/
//------------------------------------------------------------------------------

#include "Main.h"


void KeyOpenCloseProcess(void);
void ModeGotoTenKeyWakeupCheck(void);
void KeyRegisterProcess(void);

																			
//------------------------------------------------------------------------------
/** 	@brief	Card Process Time Counter
	@param	None
	@return 	None
	@remark 카드 읽기 처리 중에 사용하는 Timer, Timer Tick에서 호출하여 사용
	@remark 2ms Tick, 0~500ms 설정 동작
*/
//------------------------------------------------------------------------------
void ModeKeyInputCheck(void)
{
#if defined (DDL_AAAU_AUTOLOCK_HOLD) || defined (USED_SECOND_MOTOR)
	BYTE bTmp , bTtmp;
#endif	//DDL_AAAU_AUTOLOCK_HOLD #endif

	// 3분락 수행 중일 경우 MainMode 처리하지 않음
	if(GetTamperProofPrcsStep())
	{
#ifdef 	DDL_CFG_IBUTTON
		if(GMTouchKeyInputCheck() == STATUS_SUCCESS)
		{
			GMTouchKeyInputClear();
		}	
#endif
		return;
	}

	switch(gbInputKeyValue)
	{
		case FUNKEY_OPCLOSE:
		// KC 인증을 위한 카드 출력 ON 처리
#ifdef	DDL_TEST_SET_FOR_CARD
			if(IsCardPowerOnForTest() == true)		break;
#endif

			gfMute = 0;

#ifdef	LOCK_TYPE_DEADBOLT
			if(HandingLockProcessStartCheck())	break;
#endif

#ifdef	DDL_AAAU_AUTOLOCK_HOLD //호주 및 뉴질랜드 전용 autolock hold 기능
			bTmp = MotorSensorCheck();	
			bTtmp = MotorSensorCheck_Second();
			if(bTmp == SENSOR_CLOSE_STATE && bTtmp == SENSOR_CLOSE_STATE)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
			}
			else
			{
				gbMainMode = MODE_OPERATION_SELECT; 	
				gbModePrcsStep = 100;
				SetModeProcessTime(250);
			}
#else	//DDL_AAAU_AUTOLOCK_HOLD #else

/*#ifdef DDL_DPS_CHECK_BEFORE_WORK	//문이 열려 있으면 OC버튼 동작 금지
			if(gfDoorSwOpenState)	//문이 열려 있으면 에러 처리	
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
			}
			else
			{
				KeyOpenCloseProcess();
			}
#else	//DDL_DPS_CHECK_BEFORE_WORK #else*/			
			KeyOpenCloseProcess();
//#endif	//DDL_DPS_CHECK_BEFORE_WORK #endif

#endif	//DDL_AAAU_AUTOLOCK_HOLD #endif
			break;

		case FUNKEY_REG:
			// KC 인증을 위한 카드 출력 ON 처리
#ifdef	DDL_TEST_SET_FOR_CARD
			if(IsCardPowerOnForTest() == true)
			{
				CardPowerOffForTest();
			}
			else
			{
				// 카드 출력이 ON될 경우에는 등록 모드 동작하지 않음. 
				BuzzerSetting(gcbBuzNum, VOL_CHECK);
				CardPowerOnForTest();
				return;
			}
#endif

			gfMute = 0;
			
#ifdef DDL_AAAU_REG_BUTTON_UI //호주 및 뉴질랜드 등록 버튼 전용 UI
			if(gbModuleProtocolVersion >= 0x30 || gbInnerModuleProtocolVersion >= 0x30)
			{				
				//if(GetPackCBARegisterSet() == 0x01)	//GetPackCBARegisterSet() 리턴값이 1이면 CBA Pack과 키교환이 완료된 상태
				//{	
				//	FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				//	ModeClear();
				//}
				//else
				//{
					MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_BT_SHARP, gbInputKeyValue);	// CBA key 교환 
					ModeGotoExchangeKeySet();	
				//}
			}
			else
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
			}
#else	//DDL_AAAU_REG_BUTTON_UI #else
			KeyRegisterProcess();
#endif	//DDL_AAAU_REG_BUTTON_UI #endif			
			break;
		
		case TENKEY_STAR:
#if defined (TKEY_CIRCLE_BTN_TYPE) && defined(DDL_CFG_FP_TCS4K)
			gfMute = 0;
			ModeGotoFingerVerify(); 
			break;
#endif			
		case TENKEY_MULTI:
		// KC 인증을 위한 카드 출력 ON 처리
#ifdef	DDL_TEST_SET_FOR_CARD
			if(IsCardPowerOnForTest() == true)		break;
#endif

			gfMute = 0;

#ifdef	LOCK_TYPE_DEADBOLT
			if(HandingLockProcessStartCheck())	break;
#endif

			ModeGotoTenKeyWakeupCheck();
			break;

#ifdef	DDL_CFG_MUTE_KEY
		case FUNKEY_MUTE:
		// KC 인증을 위한 카드 출력 ON 처리
#ifdef	DDL_TEST_SET_FOR_CARD
			if(IsCardPowerOnForTest() == true)		break;
#endif
			
			if(AlarmStatusCheck() == STATUS_FAIL)
			{
 				gfMute = 1;
			}

			ModeGotoTenKeyWakeupCheck();
			break;
#endif

		default:

#ifdef 	DDL_CFG_IBUTTON
			GMTouchKeyDetectionProcess();
#endif

#ifdef 	DDL_CFG_DS1972_IBUTTON

		DS1972TouchKeyDetectionProcess();
#endif

#ifdef	_MASTERKEY_IBUTTON_SUPPORT
		ModeGotoSpecialModeMasterKeyVerify();
#endif
			break;
	}
}


void ModeGotoTenKeyWakeupCheck(void)
{
	gbMainMode = MODE_TENEKY_WAKEUP_CHECK;
	gbModePrcsStep = 0;
}




void ModeGotoRegisterCheck(void)
{
	gbMainMode = MODE_REGISTER_CHECK;
	gbModePrcsStep = 0;
}





void KeyOpenCloseProcess(void)
{
	PCErrorClearSet();

	if(AlarmStatusCheck() == STATUS_SUCCESS)
	{
		if(gfHighTempAlarm)
		{
			//고온 경보 중일 경우에는 열림 방향으로만 동작
			StartMotorOpen();
		}		

		ModeGotoAlarmClearByKey(FUNKEY_OPCLOSE);
	}
	else
	{
		ModeGotoOpenClose();
	}
}




void OneTouchMotorClosed(void)
{
	// 키패드에 의한 닫힘 Event 전송
	PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x02);

	ModeClear();
}


void KeyTenKeyWakeUpProcess(void)
{
	BYTE bTmp;

	switch(gbModePrcsStep)
	{
		case 0:
			if(AlarmStatusCheck() == STATUS_SUCCESS)
			{
				ModeGotoPINVerify();
			}	
			else
			{
				bTmp = MotorSensorCheck();
				switch(bTmp)
				{
					case SENSOR_OPENLOCK_STATE:
					case SENSOR_CLOSELOCK_STATE:
					case SENSOR_CENTERLOCK_STATE:
					case SENSOR_LOCK_STATE:
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER)
						if(bTmp != SENSOR_OPENLOCK_STATE)
						{
							FeedbackLockInOpenAllowState();	
							// 내부강제잠금 안내음이 완전하게 나오지 않고 잘려서 수정
							gbModePrcsStep = 10;
//							ModeGotoPINVerify();
						}
						else 
						{
							PackAlarmReportInnerForcedLockOpenCloseFailSend();
							FeedbackLockIn();
							ModeClear();
						}

#elif defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN)
						if(bTmp == SENSOR_CLOSELOCK_STATE)
						{
							FeedbackLockInOpenAllowState();	
							ModeGotoPINVerify();
						}
						else if(bTmp == SENSOR_OPENLOCK_STATE)
						{
#ifdef	CLOSE_BYPINCODE//내부 강제 잠김 상태에서 내부장제잠김 알람 출력후 PIN Verify mode로 진입 하게 하기 위함.
							FeedbackLockInOpenAllowState();	
							ModeGotoPINVerify();
#else	//__DDL_MODEL_PANPAN_FC2A	 #else							
							StartMotorClose();
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
							BatteryCheck();
#else
							FeedbackMotorClose();
#endif
							gbModePrcsStep++;
#endif	//__DDL_MODEL_PANPAN_FC2A	#endif
						}
						else 
						{
							PackAlarmReportInnerForcedLockOpenCloseFailSend();
							FeedbackLockIn();
							ModeClear();
						}
#else 
						PackAlarmReportInnerForcedLockOpenCloseFailSend();
						FeedbackLockIn();
						ModeClear();
#endif 
						break;
				
					case SENSOR_OPEN_STATE:
					case SENSOR_OPENCEN_STATE:
						// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
						if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
						{
							FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
							ModeClear();
							break;							
						}

						
#ifdef	MOTOR_CLOSED_IN_OPEN_STATUS
						
#ifndef	P_SNS_EDGE_T 	
//중국 사양이어서 문 열림 상태에서 멀티 터치를 이용한 문 닫힘 기능이 제한 되지만 PANPAN은 비밀번호 입력 모드로 전환 됨, MOTOR_CLOSED_IN_OPEN_STATUS를 선언 하지 않고 default에서 처리 해도 되지만 중국 모델이기에 혼란을 피하고 싶었음 
						ModeGotoPINVerify();
#else	//__DDL_MODEL_PANPAN_FC2A #else

						if(gfDoorSwOpenState)	//문이 열려있는 경우
						{
					        ModeGotoPINVerify();
						}
						else	//문이 닫혀있는경우 
						{
			                StartMotorClose();
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorClose(); 
#endif
			                gbModePrcsStep++;
						}
#endif	//__DDL_MODEL_PANPAN_FC2A #endif

#else
						StartMotorClose();
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
						BatteryCheck();
#else
						FeedbackMotorClose(); 
#endif
						gbModePrcsStep++;
#endif
						break;
				
					default:	
						ModeGotoPINVerify();
						break;					
				}
			}
			break;

		case 1:
			MotorCloseCompleteCheck(OneTouchMotorClosed, 2);
			break;

		case 2:
			if(GetLedMode())	break;
			
			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER) 
		case 10:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )
			{
				break;
			}

			ModeGotoPINVerify();
			break;			
#endif 

		default:
			ModeClear();
			break;
	}		
}


void KeyRegisterProcess(void)
{
/*
	gAutoLockTryCnt = 0;

	if(gbMotorMode != 0)			break;	

	if(FactoryResetHistoryCheck())
	{
		gbMainMode = MODE_MENU_MASTERCODE_REGISTER;
		gbModePrcsStep = 0;
		break;
	}

	gfMute = 0;
	TKeyInitial();

	bTmp = AlarmStatusCheck();
	if(bTmp == STATUS_SUCCESS)
	{
		gbAlarmClrInputKey = gbInputKeyValue;
		gbMainMode = MODE_ALARM_CLR_BY_KEY;
		gbModePrcsStep = 0;
	}
	else
*/
	{
		ModeGotoRegisterCheck();
	}
}

																			
void ModeRegisterCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			if(AlarmStatusCheck() == STATUS_SUCCESS)
			{
				ModeGotoAlarmClearByKey(FUNKEY_REG);
				break;
			}

			if(gbManageMode == _AD_MODE_SET)
			{
// mode 에 상관없이 AD 모드중 비번이 없으면 setting 가능 하게 				
//#if	defined (DDL_CFG_ADVANCED_DEFAULT) || defined (DDL_CFG_ADVANCED_ONLY)
				if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
				{
					ModeGotoMasterCodeRegister();
					break;
				}
//#endif
				
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
			}
			else
			{
#if defined (DDL_CFG_MS)
				/* MS 사양은 등록 버튼 입력시 무조건 main menu mode 로 */
				ModeGotoMenuNormalMain();
#else 

#if defined (DDL_CFG_RFID) && defined(DDL_CFG_FP) 
				FeedbackKeyPadLedOn(AVML_REG_FINGER_PIN_CARD_R, VOL_CHECK);
				CardGotoReadStart();
#else 
#ifdef	DDL_CFG_RFID
//				FeedbackKeyPadLedOn(3, VOL_CHECK);
				FeedbackKeyPadLedOn(AVML_IN_0410PIN_R_CARD, VOL_CHECK);

				CardGotoReadStart();
#endif

#ifdef	DDL_CFG_FP  //hyojoon_20160831 PFM-3000 add
//				FeedbackKeyPadLedOn(235, VOL_CHECK);		// 네자리에서 열자리의 ..., 설정을 완료하려면 R..., 지문등록을 원시면 별 버튼을 누르세요.
				FeedbackKeyPadLedOn(AVML_IN_0410PIN_R_FINGER_C, VOL_CHECK);		// 네자리에서 열자리의 ..., 설정을 완료하려면 R..., 지문등록을 원시면 별 버튼을 누르세요.
#endif
#endif 

#if defined	DDL_CFG_IBUTTON || defined DDL_CFG_DS1972_IBUTTON
//				FeedbackKeyPadLedOn(3, VOL_CHECK);
				FeedbackKeyPadLedOn(AVML_IN_0410PIN_R_CARD, VOL_CHECK);
#endif

#ifdef	DDL_CFG_DS1972_IBUTTON
				iButtonGotoRegModeStart();
#endif 

				TenKeyVariablesClear();
				
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
				gbModePrcsStep++;
#endif 				
			}
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0);

#ifdef	DDL_CFG_RFID
					CardGotoReadStop();
#endif

#ifdef	DDL_CFG_DS1972_IBUTTON
					iButtonGotoReadStop();
#endif 
					ModeGotoPincodeRegister();
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_NONE:
#ifdef	DDL_CFG_RFID
					if(GetCardReadStatus()== CARDREAD_SUCCESS)
					{
						ModeGotoCardRegister();
						break;
					}
#endif

#ifdef 	DDL_CFG_IBUTTON
					if(GMTouchKeyInputCheck() == STATUS_SUCCESS)
					{
						ModeGotoGMTouchKeyRegister();
						break;
					}
#endif

#ifdef 	DDL_CFG_DS1972_IBUTTON
					if(DS1972TouchKeyInputCheck() == STATUS_SUCCESS)
					{
						ModeGotoDS1972TouchKeyRegister();
						break;
					}
#endif

#ifndef	DDL_TEST_SET_FOR_CARD
					TimeExpiredCheck();
#endif 
					break;
					
#ifdef	DDL_CFG_FP  //hyojoon_20160831 PFM-3000 add
				case TENKEY_STAR:
//					FeedbackTenKeyInput(0, gbInputKeyValue);
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);
					ModeGotoFingerRegister();			//지문 등록 모드로 진입
					break;
#endif

				default:
#ifdef	DDL_TEST_SET_FOR_CARD
					if(IsCardPowerOnForTest() == true)
					{
						gfCardPowerOnForTest = 0;
					}
#endif 				
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}
			break;

		default:
			ModeClear();
			break;
	}
}

#ifdef DDL_AAAU_AUTOLOCK_HOLD //호주 및 뉴질랜드 전용 autolock hold 기능
void ModeOperationSelect(void)
{
	BYTE bTmp , bTtmp;
	if( gbNewOCFunctionKey == TENKEY_NONE && !P_SW_O_C_T)
	{
		if(AutoLockSetCheck() == 1)	//자동 잠김이 설정 되었을 경우
		{
			if(gfDoorSwOpenState)	//문이 열려있는 경우
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
			}
			else					//문이 닫혀 있는 경우
			{		
				bTmp = AutolockHoldSettingLoad();
				if(bTmp == AUTOLOCKHOLD)
				{
					//AutolockHoldSetting(AUTOLOCKUNHOLD);	//2019년20일 이후 Safty mode로 전환 되면 Autolock Unhold로 전환 되는 UI로 변경 
					//LedSetting(gcbNoDimSafety, LED_DISPLAY_OFF);
				//--------------------------------------------------------
					//AUTOLOCKUNHOLD 설정과 동시에 Safety mode로 바로 전환 하기 위함
					//주석으로 막혀 있다면 자동 잠김 시간이 지난후 Safety mode로 전환
					PCErrorClearSet();
					StartMotorClose();	
					gbMainMode = MODE_OPEN_CLOSE;
					gbModePrcsStep = MODEPRCS_OPEN_BYKEY_COMPLETE_CHECK;					
				//--------------------------------------------------------
				}
				else
				{
					AutolockHoldSetting(AUTOLOCKHOLD);		
					PackTx_MakeAlarmPacket(AL_MANUAL_UNLOCKED, 0x00, 0x03);
					//LedSetting(gcbNoDimPassage, LED_DISPLAY_OFF);
					gAutolockHoldClearTimer1s = 9;	//약 10 
					gbAutolockHoldAutoClear = 1;
					PCErrorClearSet();
					StartMotorOpen();	
					gbMainMode = MODE_OPEN_CLOSE;
					gbModePrcsStep = MODEPRCS_OPEN_BYKEY_COMPLETE_CHECK;	
					//ModeClear();
				}
				
			}
		}		
		else					//자동 잠김이 미 설정 되었을 경우
		{
			if(gfDoorSwOpenState)	//문이 열려있는 경우
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
			}
			else					//문이 닫혀 있는 경우
			{
				bTtmp = MotorSensorCheck_Second();
				if(bTmp == SENSOR_CLOSE_STATE)	//Secure Mode일 경우 에러 처리
				{
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
				}
				else
				{
					KeyOpenCloseProcess();
				}
			}
		}
	}
	
}
#endif //DDL_AAAU_AUTOLOCK_HOLD #endif	



