//------------------------------------------------------------------------------
/** 	@file		ModeMenuOnetimeCodeRegDel_T1.c
	@version 0.1.00
	@date	2016.04.26
	@brief	Onetime Code Register/Delete Mode
	@remark	일회용  비밀번호 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.26		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"



void ModeGotoOnetimeCodeRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONETIMECODE_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoOnetimeCodeRegister(void)
{
	gbMainMode = MODE_MENU_ONETIMECODE_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoOnetimeCodeDelete(void)
{
	gbMainMode = MODE_MENU_ONETIMECODE_DELETE;
	gbModePrcsStep = 0;
}


void OnetimeCodeModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// Onetime Code 등록
		case 1: 	
			ModeGotoOnetimeCodeRegister();
			break;
	
		// Onetime Code 삭제
		case 3: 		
			ModeGotoOnetimeCodeDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuOnetimeCodeModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// Onetime Code 등록
		case TENKEY_1:			
//			MenuSelectKeyTempSaveNTimeoutReset(12, gbInputKeyValue);	//register a onetime code, press the hash to continue. 
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_OPIN_SHARP, gbInputKeyValue);	//register a onetime code, press the hash to continue. 
			break;

		// Onetime Code 삭제
		case TENKEY_3:		
//			MenuSelectKeyTempSaveNTimeoutReset(13, gbInputKeyValue);	//Delete a onetime code, press the hash to continue
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_OPIN_SHARP, gbInputKeyValue);	//Delete a onetime code, press the hash to continue
			break;

		case TENKEY_SHARP:
			OnetimeCodeModeSetSelected();
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




//------------------------------------------------------------------------------
/** 	@brief	Pincode Register Mode Start
	@param	None
	@return 	None
	@remark 비밀번호 등록 모드 시작
*/
//------------------------------------------------------------------------------
void ModeOnetimeCodeRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			Feedback13MenuOn(43, VOL_CHECK);			// press 1 to register a code press 3 to delete a code
			Feedback13MenuOn(AVML_REGPIN1_DELPIN3, VOL_CHECK);			// press 1 to register a code press 3 to delete a code

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOnetimeCodeModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}



void RegisterOnetimeCode(void)
{
	UpdatePincodeToMemory(ONETIME_CODE);

	PackTxEventPincodeAdded(RET_TEMP_CODE_MATCH);

#ifdef	DDL_CFG_DIMMER
	FeedbackModeCompletedLedOff(VOICE_MIDI_CANCLE, VOL_CHECK);

	SetModeProcessTime(150);
	PrepareDataForDisplayPincode();
	gbModePrcsStep++;
#else
//	FeedbackModeCompletedKeepMode(1, VOL_CHECK);
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
	gbModePrcsStep+=2;
#endif
}




//------------------------------------------------------------------------------
/** 	@brief	Register User Code Process
	@param	None
	@return 	None
	@remark 입력한 비밀번호 등록을 위한 확인 과정 및 등록 과정
*/
//------------------------------------------------------------------------------
void ProcessRegisterOnetimeCode(void)
{
	switch(PincodeVerify(0))
	{
		case RET_TEMP_CODE_MATCH:
		case RET_NO_MATCH:
			//# 버튼음 출력
#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;
	
		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
//			FeedbackErrorModeKeepOn(109, VOL_CHECK);		//	That code is already in use
			FeedbackErrorModeKeepOn(AVML_ALREADY_USE_PIN, VOL_CHECK);		//	That code is already in use
	//		ModeClear();
	
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			break;				
	}
}



void OnetimeCodeRegDelProcess(void)
{
//	FeedbackModeCompletedKeepMode(39, VOL_CHECK);
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
	
	gbModePrcsStep++;
}



void ModeOnetimeCodeRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(14, VOL_CHECK);			//Enter only 4 digits onetime code,  then press the hash to continue
			FeedbackKeyPadLedOn(AVML_IN_OPIN_SHARP, VOL_CHECK);			//Enter only 4 digits onetime code,  then press the hash to continue

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;
			break;
			
		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
					if(TenKeySave(gPinInputMinLength, gbInputKeyValue, 0) == TENKEY_INPUT_OVER) 
#else
#ifdef	P_VOICE_RST // defined (DDL_CFG_SUPPORT_CHINESE)
					if(TenKeySave(gPinInputMinLength, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)	
#else 
					if(TenKeySave(MAX_FIX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
#endif 
#endif
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					ProcessRegisterOnetimeCode();
					break;		

				case TENKEY_STAR:
					ReInputOrModeMove(ModeGotoOnetimeCodeRegDelCheck);
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			RegisterOnetimeCode();
			break;

// PASSWORD DISPLAY RTN
		case 3:
			DisplayRegisteredPincode(OnetimeCodeRegDelProcess);
			break;
			
		case 4:
			GotoPreviousOrComplete(ModeGotoOnetimeCodeRegDelCheck);
			break;
		
		default:
			ModeClear();
			break;
	}
}



void OnetimeCodeDelete(void)
{
#if defined (_USE_IREVO_CRYPTO_)
	BYTE bTemp[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	EncryptDecryptKey(bTemp, 8);
	RomWrite(bTemp, ONETIME_CODE, 8);
#else 	
	RomWriteWithSameData(0xFF, ONETIME_CODE, 8);
#endif 
}



void ProcessDeleteOnetimeCode(void)
{
	BYTE bTmp;

	bTmp = PincodeVerify(0);
	switch(bTmp)
	{
		case RET_MASTER_CODE_MATCH:
		case 1:
			if((gbManageMode == _AD_MODE_SET) && (bTmp != RET_MASTER_CODE_MATCH))
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 
				ModeClear();
				break;				
			}
			
			if((gbManageMode != _AD_MODE_SET) && (bTmp != 1))
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 
				ModeClear();
				break;				
			}

#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;

		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	
			ModeClear();
			break;				
	}
}



void DeleteOnetimeCode(void)
{
	OnetimeCodeDelete();

//	FeedbackModeCompletedKeepMode(39, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
	gbModePrcsStep++;

	PackTxEventPincodeDeleted(RET_TEMP_CODE_MATCH);
}



void ModeOnetimeCodeDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			if(gbManageMode == _AD_MODE_SET)
			{
				FeedbackKeyPadLedOn(AVML_IN_MPIN_FOR_DELPIN_SHARP, VOL_CHECK);			//Enter the master code to delete the passcode, then press the hash to continue
			}
			else
			{
//				FeedbackKeyPadLedOn(16, VOL_CHECK);			//Enter the user code to delete the passcode, then press the hash to continue
				FeedbackKeyPadLedOn(AVML_FOR_DELPIN_IN_PIN_SHARP, VOL_CHECK);			//Enter the user code to delete the passcode, then press the hash to continue
			}

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					ProcessDeleteOnetimeCode();
					break;		

				case TENKEY_STAR:
					ReInputOrModeMove(ModeGotoOnetimeCodeRegDelCheck);
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;
		
		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			DeleteOnetimeCode();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoOnetimeCodeRegDelCheck);
			break;

		default:
			ModeClear();
			break;
	}
}



