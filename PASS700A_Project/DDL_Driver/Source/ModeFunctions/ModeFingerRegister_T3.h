#ifndef		_MODE_FINGER_REGISTER_H_
#define		_MODE_FINGER_REGISTER_H_


#ifdef	_MODE_FINGER_REGISTER_C_
BYTE		no_fingers;
BYTE 	FingerReadBuff[4];
BYTE 	FingerWriteBuff[4];
#else
extern	BYTE	no_fingers;
extern	BYTE 	FingerReadBuff[4];
extern	BYTE 	FingerWriteBuff[4];
#endif

void	ModeGotoFingerRegister( void );
void	ModeFingerRegister( void );

#ifdef	BLE_N_SUPPORT
void	ModeGotoFingerRegisterByBleN( void );
void	ModeFingerRegisterByBleN( void );
#endif

#endif

