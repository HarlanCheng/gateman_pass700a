 #include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	FILE *fpin, *fpout;
	unsigned char Temp = 0;
	unsigned long len = 0, i = 0;
    unsigned char Buf[5] = {0};


    //命令格式: mergebin file1 file2 file3
    //将 file1(bootloader) 跟 file2(APP) 合并成file3
    
    //打开file3
	if( (fpout = fopen(argv[3], "wb")) == NULL)
	{
		printf("Can't open %s\n", argv[3]);
		exit(1);
	}

    //打开file1
	if( (fpin = fopen(argv[1], "rb")) == NULL)
	{
		printf("Can't open %s\n", argv[1]);
		exit(1);
	}

    fseek(fpin, 0, SEEK_SET); //定位到文件的最前面
    fseek(fpin, 0, SEEK_END); //定位到文件的最后面
    len  = ftell(fpin); //ftell获得该文件指示符此时的偏移量,此时已经是在文件末尾,故能获得文件的大小
    fseek(fpin, 0, SEEK_SET); //定位到文件的最前面
    printf("\r\n File 1 len = %d", len);
    
    //写第一个文件
    for(i = 0; i < len; i++)
    {
        fread(&Temp, 1, 1, fpin);
        fwrite(&Temp, 1, 1, fpout);
    }
    fclose(fpin);


    //打开file2
	if( (fpin = fopen(argv[2], "rb")) == NULL)
	{
		printf("Can't open %s\n", argv[2]);
		exit(1);
	}

    fseek(fpin, 0, SEEK_SET); //定位到文件的最前面
    fseek(fpin, 0, SEEK_END); //定位到文件的最后面
    len  = ftell(fpin); //ftell获得该文件指示符此时的偏移量,此时已经是在文件末尾,故能获得文件的大小
    fseek(fpin, 0, SEEK_SET); //定位到文件的最前面
    printf("\r\n File 2 len = %d", len);
    
    //写第二个文件
    for(i = 0; i < len; i++)
    {
        fread(&Temp, 1, 1, fpin);
        fwrite(&Temp, 1, 1, fpout);
    }
    fclose(fpin);
    
	fclose(fpout);
	return 0;
}














