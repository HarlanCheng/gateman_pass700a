
#include "Main.h"

const BYTE gcbiButtonCRC[256]={
	  0,  94, 188, 226,  97,  63, 221, 131, 194, 156, 126,  32, 163, 253,  31,  65,
	157, 195,  33, 127, 252, 162,  64,  30,  95,   1, 227, 189,  62,  96, 130, 220,
	 35, 125, 159, 193,  66,  28, 254, 160, 225, 191,  93,   3, 128, 222,  60,  98,
	190, 224,   2,  92, 223, 129,  99,  61, 124,  34, 192, 158,  29,  67, 161, 255,
	 70,  24, 250, 164,  39, 121, 155, 197, 132, 218,  56, 102, 229, 187,  89,   7,
	219, 133, 103,  57, 186, 228,   6,  88,  25,  71, 165, 251, 120,  38, 196, 154,
	101,  59, 217, 135,   4,  90, 184, 230, 167, 249,  27,  69, 198, 152, 122,  36,
	248, 166,  68,  26, 153, 199,  37, 123,  58, 100, 134, 216,  91,   5, 231, 185,
	140, 210,  48, 110, 237, 179,  81,  15,  78,  16, 242, 172,  47, 113, 147, 205,
	 17,  79, 173, 243, 112,  46, 204, 146, 211, 141, 111,  49, 178, 236,  14,  80,
	175, 241,  19,  77, 206, 144, 114,  44, 109,  51, 209, 143,  12,  82, 176, 238,
	 50, 108, 142, 208,  83,  13, 239, 177, 240, 174,  76,  18, 145, 207,  45, 115,
	202, 148, 118,  40, 171, 245,  23,  73,   8,  86, 180, 234, 105,  55, 213, 139,
	 87,   9, 235, 181,  54, 104, 138, 212, 149, 203,  41, 119, 244, 170,  72,  22,
	233, 183,  85,  11, 136, 214,  52, 106,  43, 117, 151, 201,  74,  20, 246, 168,
	116,  42, 200, 150,  21,  75, 169, 247, 182, 232,  10,  84, 215, 137, 107,  53
};

void  IBUTTON_DATA_MODE(BYTE Type)
{
	GPIO_InitTypeDef GPIO_InitStruct;

#ifdef	_MASTERKEY_IBUTTON_SUPPORT
	GPIO_InitStruct.Pin = MASTERKEY_Pin;
	GPIO_InitStruct.Mode = (Type == OUTPUT ? GPIO_MODE_OUTPUT_PP : (Type == INPUT) ? GPIO_MODE_INPUT : GPIO_MODE_IT_FALLING);
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(MASTERKEY_GPIO_Port, &GPIO_InitStruct);
#else
	GPIO_InitStruct.Pin = IBUTTON_COVER_RFID_GND_Pin;
	GPIO_InitStruct.Mode = (Type == OUTPUT ? GPIO_MODE_OUTPUT_PP : (Type == INPUT) ? GPIO_MODE_INPUT : GPIO_MODE_IT_FALLING);
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(IBUTTON_COVER_RFID_GND_GPIO_Port, &GPIO_InitStruct);
#endif
}


void Delay5usTime()
{
	uint32_t		i; 
 	// 약 5.8us 
	for( i=0; i< 10 ; i++ )
		__NOP();	
}

BYTE iButtonReset(void)
{
	BYTE bTmp;
	BYTE bTmp1 = 0x00;	
	uint32_t wTimerInitCnt = 0x00;
	BYTE LowDetect = 0;
#if 0		//for test 	
	static uint32_t resetcounter = 0;
#endif 

	//Reset Pulse
	P_IBUTTON(0);
	IBUTTON_DATA_MODE(OUTPUT);		
	Delay(SYS_TIMER_500US); //Trstl 480us ~ 640us
	P_IBUTTON(1);
	IBUTTON_DATA_MODE(INPUT);

	Delay(SYS_TIMER_20US);  //Tpdh 15 ~ 60us  

	SysTimerStart(&wTimerInitCnt);
	
	while(1)
	{
		bTmp1++;
		bTmp = SysTimerEndCheck(wTimerInitCnt,SYS_TIMER_500US);	//Tpdl 60 ~ 240us 
		if(bTmp)
		{
#if 0		//for test 
			if(gbiButtonMode)
			{
				resetcounter++;
				bTmp1 = 0;
				Delay(SYS_TIMER_20US);
				return FAIL;
			}
#endif 				
			return FAIL;
		}

		//High level checking after Presence Pulse Low level Checking 
		if(!P_IBUTTON_T)
		{			
			LowDetect = 1;
		}
		
		if(P_IBUTTON_T && LowDetect)
		{
#if 0		//for test 		
			resetcounter = 0;
#endif 
			break;
		}
	}
	return OK;                                              		
}


void iButtonByteRead(BYTE * bpData, BYTE bNum)
{
	BYTE bCnt;
	BYTE bCnt1;
	BYTE bTmp;

//	__disable_irq();
	for(bCnt = 0; bCnt < bNum; bCnt++)
	{
		bCnt1 = 8;
		bTmp = 0;
		while(bCnt1--)
		{
			bTmp = bTmp >> 1;
			P_IBUTTON(0);                    			
			IBUTTON_DATA_MODE(OUTPUT); // 이 과정에서 delay 생김 

			IBUTTON_DATA_MODE(INPUT);	// 이 과정에서 delay 생김 

			if(P_IBUTTON_T)        
				bTmp |= 0x80;

			Delay(SYS_TIMER_30US);
		}
		*bpData = bTmp;
		bpData++;
	}
//	__enable_irq();

}

void iButtonByteWrite(BYTE * bpData, BYTE bNum)
{
	BYTE bTmp;
	BYTE bCnt;
	BYTE bCnt1;

//	__disable_irq();

	for(bCnt = 0; bCnt < bNum; bCnt++)
	{
		bTmp = *bpData;
		bpData++;
		bCnt1 = 8;
		while(bCnt1--)
		{
			P_IBUTTON(0);
			IBUTTON_DATA_MODE(OUTPUT); 						
			if(bTmp & 0x01)
			{                        		
				//Write Data 1
				//P_IBUTTON(1);
				IBUTTON_DATA_MODE(INPUT);
				Delay(SYS_TIMER_50US);
		     	}
		     	else
		     	{								
		     		//Write Data 0
				Delay((SYS_TIMER_50US+SYS_TIMER_30US));
				IBUTTON_DATA_MODE(INPUT);
				//P_IBUTTON(1);
				//Delay5usTime();
			}
			bTmp = bTmp >> 1;
		}
	}
//	__enable_irq();
}


BYTE iButtonReadRom(BYTE * bpData)
{
	BYTE bTmp;
	BYTE bCnt;

	bTmp = iButtonReset();
	if(bTmp == FAIL)	
	{
		bTmp = iButtonReset();
		if(bTmp == FAIL)			
			return FAIL;
	}


	bTmp = 0x33;                  		//Read ROM Command
	iButtonByteWrite(&bTmp, 1);

	iButtonByteRead(gbiButtonMemTmp, 8);

	bTmp = iButtonReset();
	if(bTmp == FAIL)	
	{
		bTmp = iButtonReset();
		if(bTmp == FAIL)			
			return FAIL;
	}

	bTmp = 0;
	for(bCnt = 0; bCnt < 7; bCnt++)
	{
		//Received Data CRC Check
		bTmp = bTmp ^ gbiButtonMemTmp[bCnt]; 	
		bTmp = gcbiButtonCRC[bTmp];
	}

	if(bTmp != gbiButtonMemTmp[7])   
		return FAIL;

	for(bCnt = 1; bCnt < 7; bCnt++)
	{                        		
		if(gbiButtonMemTmp[bCnt] != 0xFF)      
		{
			memcpy(bpData, 	gbiButtonMemTmp, 8);
			return OK;
		}
	}

	return FAIL;
}


BYTE iButtonWriteMem(BYTE * bpData, BYTE bAdrs, BYTE bNum)
{
	BYTE bTmp;
	BYTE bESTmp;
	BYTE bCmd[5];
	
	bTmp = iButtonReset();
	if(bTmp == FAIL)	
	{
		bTmp = iButtonReset();
		if(bTmp == FAIL)			
			return FAIL;
	}

	bCmd[0] = 0xCC;                           	//Skip ROM Command
	bCmd[1] = 0x0F;                           	//Write Scratchpad Command
	bCmd[2] = bAdrs;                          	//Start Address
	bCmd[3] = 0x00;                           	//Start Address
	iButtonByteWrite(bCmd, 4);

	iButtonByteWrite(bpData, bNum);

	bTmp = iButtonReset();
	if(bTmp == FAIL)	
	{
		bTmp = iButtonReset();
		if(bTmp == FAIL)			
			return FAIL;
	}

	bCmd[0] = 0xCC;                           	//Skip ROM Command
	bCmd[1] = 0xAA;                           	//Read Scratchpad Command
	iButtonByteWrite(bCmd, 2);

	iButtonByteRead(gbiButtonMemTmp, bNum+3);	//For get E/S Byte

	if(gbiButtonMemTmp[0] != bAdrs)	
		return FAIL;						//Address Check
	if(gbiButtonMemTmp[1] != 0x00)	
		return FAIL;						//Address Check
	if((gbiButtonMemTmp[2] & 0xA0) != 0)	
		return FAIL;						//AA, PF Check

	bESTmp = gbiButtonMemTmp[2] & 0x07;

	bTmp = (BYTE)memcmp(&gbiButtonMemTmp[3], bpData, bNum);
	if(bTmp != 0)		return FAIL;

	bTmp = iButtonReset();
	if(bTmp == FAIL)	
	{
		bTmp = iButtonReset();
		if(bTmp == FAIL)
			return FAIL;
	}

	bCmd[0] = 0xCC;                           	//Skip ROM Command
	bCmd[1] = 0x55;                           	//Copy Scratchpad Command
	bCmd[2] = bAdrs;                         	//Start Address
	bCmd[3] = 0x00;                           	//Start Address
	bCmd[4] = bESTmp;                     	//E/S Byte
	iButtonByteWrite(bCmd, 5);

	P_IBUTTON(1);								
	IBUTTON_DATA_MODE(OUTPUT); 	
	
	//12.5ms Write Delay
	Delay((SYS_TIMER_10MS)/*+(SYS_TIMER_3MS)*/);					

	IBUTTON_DATA_MODE(INPUT);			


	gbiButtonMemTmp[0] = 0;
	iButtonByteRead(gbiButtonMemTmp, 1);

	if(gbiButtonMemTmp[0] != 0xAA)	
		return FAIL;            				//if it is not AAh then Write fail

	if(bTmp == FAIL)	
	{
		bTmp = iButtonReset();
		if(bTmp == FAIL)			
			return FAIL;
	}

	return OK;
}


BYTE iButtonReadMem(BYTE * bpData, BYTE bAdrs, BYTE bNum)
{
	BYTE bTmp;
	BYTE bCmd[4];

	bTmp = iButtonReset();
	if(bTmp == FAIL)	
	{
		bTmp = iButtonReset();
		if(bTmp == FAIL)			
			return FAIL;
	}

	bCmd[0] = 0xCC;                           	//Skip ROM Command
	bCmd[1] = 0xF0;                           	//Read Memory Command
	bCmd[2] = bAdrs;                         	//Start Address
	bCmd[3] = 0x00;                           	//Start Address
	iButtonByteWrite(bCmd, 4);

	iButtonByteRead(gbiButtonMemTmp, bNum);

//	iButtonReset();

	bTmp = iButtonReset();
	if(bTmp == FAIL)
	{
		bTmp = iButtonReset();
		if(bTmp == FAIL)			
			return FAIL;
	}

	bCmd[0] = 0xCC;                           	//Skip ROM Command
	bCmd[1] = 0xF0;                           	//Read Memory Command
	bCmd[2] = bAdrs;                         	//Start Address
	bCmd[3] = 0x00;                           	//Start Address
	iButtonByteWrite(bCmd, 4);

	iButtonByteRead(bpData, bNum);

	bTmp = iButtonReset();
	if(bTmp == FAIL)
	{
		bTmp = iButtonReset();
		if(bTmp == FAIL)			
			return FAIL;
	}

	bTmp = (BYTE)memcmp(gbiButtonMemTmp, bpData, bNum);
	if(bTmp == 0)
	{
		return OK;
	}

	return FAIL;
}


