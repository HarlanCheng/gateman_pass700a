#include	"main.h"

#ifdef RTC_PCF85063

uint16_t gPackRTCCorrectionTimer = 0;


#define PCF85063_ADDR			0xA3

#define RTC_CONTROL1			0x00

#define STOP_BIT				(1<< 5)


#define OSC_OFF					(1<< 7)

#if 0
#define CONTROL1_EXT_TEST		(1<< 7) // 0 : normal mode 1 : external clock test mode
#define CONTROL1_STOP			(1<< 5) // 0 : RTC clock runs 1 : RTC clock is stopped; all RTC divider chain flip-flops are asynchronously set logic 0
#define CONTROL1_SR			(1<< 4) // 0 : no software reset 1 : initiate software reset[2]; this bit always returns a 0 when read
#define CONTROL1_CIE			(1<< 2) // 0 : no correction interrupt generated 1 : interrupt pulses are generated at every correction cycle
#define CONTROL1_12_24			(1<< 1) // 0 : normal mode 1 : external clock test mode
#define CONTROL1_CAP_SEL		1		// 0 : 7pF 1 : 12.5pF

#define CONTROL1_INIT			0x58


#define RTC_CONTROL2			0x01
#define CONTROL2_MI			(1<< 5)
#define CONTROL2_HMI			(1<< 4)
#define CONTROL2_TF			(1<< 3)

#define RTC_OFFSET				0x02
#define OFFSET_MODE			(1<< 7)

#define RTC_RAMBYTE			0x03
#endif

/*Time and date */
#define RTC_TIMEBASE				0x04
#define RTC_SECONDS				0x04
#define RTC_SECONDS_MASK		0x7F

#define RTC_MINUTES				0x05
#define RTC_MINUTES_MASK		0x7F

#define RTC_HOURS				0x06
#define RTC_HOURS_MASK			0x3F

#define RTC_DAYS					0x07
#define RTC_DAYS_MASK			0x3F

#define RTC_WEEKDAYS			0x08
#define RTC_WEEKDAYS_MASK		0x07

#define RTC_MOUNTHS				0x09
#define RTC_MOUNTHS_MASK		0x1F

#define RTC_YEARS				0x0A
#define RTC_YEARS_MASK			0xFF


#define WEEKSDAYS_SUN			0x00
#define WEEKSDAYS_MON			0x01
#define WEEKSDAYS_TUE			0x02
#define WEEKSDAYS_WED			0x03
#define WEEKSDAYS_THU			0x04
#define WEEKSDAYS_FRI			0x05
#define WEEKSDAYS_SAT			0x06

ddl_i2c_t ddl_rtc_i2c_dev;

void RTC_I2C_Write(uint16_t regi , uint8_t* bpData , uint16_t length)
{
	ddl_i2c_write(&ddl_rtc_i2c_dev, regi,I2C_MEMADD_SIZE_8BIT, bpData, length);	
}

void RTC_I2C_Read(uint16_t regi , uint8_t* bpData , uint16_t length)
{
	ddl_i2c_read(&ddl_rtc_i2c_dev, regi, I2C_MEMADD_SIZE_8BIT, bpData, length);
}

uint8_t ConvertWeekDays(uint8_t data)
{
	/* N protocol */
	uint8_t result;
		
	if(data & 0x01)
		result = WEEKSDAYS_SAT;
	else if(data & 0x02)
		result = WEEKSDAYS_FRI;
	else if(data & 0x04)
		result = WEEKSDAYS_THU;
	else if(data & 0x08)
		result = WEEKSDAYS_WED;
	else if(data & 0x10)
		result = WEEKSDAYS_TUE;
	else if(data & 0x20)
		result = WEEKSDAYS_MON;
	else if(data & 0x40)
		result = WEEKSDAYS_SUN;
	else
		result = WEEKSDAYS_SAT;		

	return result;
}

uint8_t Timecheck(uint8_t* buffer)
{
	uint16_t Year = (uint16_t)(buffer[0] << 8 | buffer[1]);


	if(Year <= 0x2000)
	{
		/* 통신팩 RTC 가 reset 된경우 */
		return 0;
	}

	return 1;
}

void TimeCorrection(void)
{
	uint8_t tmp = 0x00;

	// 12 시간 43,200s / 1.5s = 28,800
	if((gbModuleMode == PACK_ID_CONFIRMED && (gbModuleProtocolVersion >= 0x29)) ||
		(gbInnerModuleMode == PACK_ID_CONFIRMED && (gbInnerModuleProtocolVersion >= 0x29)))
	{
		if(gPackRTCCorrectionTimer++ >= 28000) //계산상으론 12시간이 28800 번이지만 800 뺀다 
		{
			gPackRTCCorrectionTimer = 0;
			gbWakeUpMinTime10ms = 50;
			
			if(gbModuleMode == PACK_ID_CONFIRMED && (gbModuleProtocolVersion >= 0x29))
			{
				PackTx_MakePacketSinglePort(F0_GET_TIME_AND_DATE, ES_LOCK_EVENT, &gbComCnt,&tmp,0);
			}
			else
			{
				InnerPackTx_MakePacketSinglePort(F0_GET_TIME_AND_DATE, ES_LOCK_EVENT, &gbInnerComCnt,&tmp,0);
			}
		}
	}
}

void GetEXRTCTime(uint8_t* buffer)
{
	RTC_I2C_Read(RTC_TIMEBASE,buffer,7);

	//buffer[0] &= RTC_SECONDS_MASK;
	buffer[1] &= RTC_MINUTES_MASK;
	buffer[2] &= RTC_HOURS_MASK;
	buffer[3] &= RTC_DAYS_MASK;
	buffer[4] &= RTC_WEEKDAYS_MASK;
	buffer[5] &= RTC_MOUNTHS_MASK;
	buffer[6] &= RTC_YEARS_MASK;	
}

void SetEXRTCTime(uint8_t* buffer)
{
#if defined (_USE_LOGGING_MODE_)
	uint8_t bTmpArray[4];
	uint8_t reset = 0x00;

	if(buffer[0] == 0xFF) 
	{
		/* No battery */
		reset = 0x01;
		buffer[0] = 0x00;
	}

	bTmpArray[0] = 0xFF;
	bTmpArray[1] = 0x00; 
	bTmpArray[2] = 0x00;

	if(reset)
		bTmpArray[3] = 0xB5; 
	else
		bTmpArray[3] = 0xA5; 
	
	SaveLog(bTmpArray,4);
#endif
	
	RTC_I2C_Write(RTC_TIMEBASE,buffer,7);

#if defined (_USE_LOGGING_MODE_)

	if(reset)	
		bTmpArray[3] = 0xB6; 
	else
		bTmpArray[3] = 0x00; 
	
	SaveLog(bTmpArray,4);
#endif
}


void ClearRtcOscbit(void)
{
	uint8_t buffer[1];
	RTC_I2C_Read(RTC_SECONDS,buffer, 1);
	buffer[0] &= RTC_SECONDS_MASK;
	RTC_I2C_Write(RTC_SECONDS,buffer, 1); 
}

void SoftResetRTC(void)
{
	uint8_t buffer[1];
	buffer[0] = 0x58;
	RTC_I2C_Write(RTC_CONTROL1, buffer, 1); 

	ClearRtcOscbit();
}

void ClearRtcStopbit(void)
{
	uint8_t buffer[2];
	buffer[0] = 0x00;
	buffer[1] = 0x00;
	RTC_I2C_Write(RTC_CONTROL1, buffer, 2); //ClearStopbit( RTC Run ) : not software reset Control1 = 0x00, Control2 = 0x00
}

uint8_t GetRtcStopBit(void)
{	
	uint8_t buffer[1];
	RTC_I2C_Read(RTC_CONTROL1, buffer, 1);

	if((buffer[0] & STOP_BIT) == STOP_BIT) return 1;
	else return 0;	
}

uint8_t GetRtcOscBit(void)
{	
	uint8_t buffer[1];
	RTC_I2C_Read(RTC_SECONDS, buffer, 1);

	if((buffer[0] & OSC_OFF) == OSC_OFF) return 1;
	else return 0;	
}

uint8_t GetRtcSecond(void)
{	
	uint8_t buffer[1];
	RTC_I2C_Read(RTC_SECONDS, buffer, 1);
	return buffer[0];	
}





#if 0
void RTCInit(void)
{
	EX_RTCTimeDef clock = {0x00,};
	
	ddl_rtc_i2c_dev.type = DDL_I2C_MCU_TYPE1;
	ddl_rtc_i2c_dev.slave_id = PCF85063_ADDR;
	ddl_rtc_i2c_dev.dev.mcu.hi2c = gh_mcu_i2c;

	clock.time.Seconds = 0x80;
	
	GetEXRTCTime(&clock.Timebuffer[0]);

	if(clock.time.Seconds & OSC_OFF)	// OSC 가 작동 하지 않거나 , battery 날아간 경우 ,
	{
		/* 2020.01.01 00:00:00 WEEKSDAYS_WED */
		/* 시간 다시 설정 해서 OSC정상이면 Go */

		clock.time.Seconds = 0xFF;
		clock.time.Minutes = 0x00;
		clock.time.Hours = 0x00;		
		clock.time.Days = 0x01;
		clock.time.WeekDays = WEEKSDAYS_WED;
		clock.time.Mounth = 0x01;
		clock.time.Years = 0x20;		

		SetEXRTCTime(&clock.Timebuffer[0]);

		clock.time.Seconds = 0x80;

		GetEXRTCTime(&clock.Timebuffer[0]);
		
		if(clock.time.Seconds & OSC_OFF)	
		{
			gfRtcErr = 1;
		}
	}
	else if (clock.time.Seconds == 0x00) //  i2c 가 error 
	{
		/* I2C 가 동작 한하는 경우 second 값이 0 으로 나옴 */
		/* RTC 없어도 아래 처럼 될듯 */
		uint8_t count = 12;

		while(count--)
		{
			/* 1초 이상 기다림 */
			Delay(SYS_TIMER_100MS);
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
		}

		GetEXRTCTime(&clock.Timebuffer[0]);

		if(clock.time.Seconds == 0x00)	
		{
			gfRtcErr = 1;
		}
	}
}
#endif
void RTCInit(void)
{
	EX_RTCTimeDef clock = {0x00,};
	uint8_t bControl1_StopBit;
	uint8_t bSecond_OscBit;	
	uint8_t tmpSecond_old;
	uint8_t tmpSecond_new;
	
	ddl_rtc_i2c_dev.type = DDL_I2C_MCU_TYPE1;
	ddl_rtc_i2c_dev.slave_id = PCF85063_ADDR;
	ddl_rtc_i2c_dev.dev.mcu.hi2c = gh_mcu_i2c;

	clock.time.Seconds = 0x80;
	GetEXRTCTime(&clock.Timebuffer[0]);

	bControl1_StopBit = GetRtcStopBit();
	bSecond_OscBit = GetRtcOscBit();

	if(bControl1_StopBit == 0 && bSecond_OscBit == 0)
	{
		//정상
		if(clock.time.Seconds == 0x00)
		{
			gfRtcErr = 1;
		}
		else								//시간이 가는 것임 
		{					
			gfRtcErr = 0;
		}		
	}
	else if(bControl1_StopBit == 1 && bSecond_OscBit == 0)
	{
		//일단 비정상
		gfRtcErr = 1;
		//복구 시도 
		//OSC정상이므로 StopBit만Clear해서 Run시킴
		ClearRtcStopbit();
	}
	else if(bControl1_StopBit == 0 && bSecond_OscBit == 1)
	{
		//일단 비정상
		gfRtcErr = 1;
		ClearRtcOscbit();
	}
	else //if(bControl1_StopBit == 1 && bSecond_OscBit == 1)
	{
 #if defined (_USE_LOGGING_MODE_)
		uint8_t bTmpArray[4];
		bTmpArray[0] = 0xFF;
		bTmpArray[1] = 0x00; 
		bTmpArray[2] = 0x00;
		bTmpArray[3] = 0xB5; 
		SaveLog(bTmpArray,4);
#endif
		//일단 비정상
		gfRtcErr = 1;
		//복구 시도 
		//Softreset 해줘야 함 데이터가 이상해서 
		SoftResetRTC();
	}

	if(gfRtcErr)
	{
		uint8_t count = 12;

		GetEXRTCTime(&clock.Timebuffer[0]);
		tmpSecond_old = clock.time.Seconds;

		while(count--)
		{
			/* 1초 이상 기다림 */
			Delay(SYS_TIMER_100MS);
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
		}
		
		GetEXRTCTime(&clock.Timebuffer[0]);
		tmpSecond_new = clock.time.Seconds;
		if(tmpSecond_old == tmpSecond_new) //시간이 안가는 것임
		{
			gfRtcErr = 1;
		}
		else								//시간이 가는 것임 
		{					
			gfRtcErr = 0;
			
		}
		
	}	
}
#endif 
