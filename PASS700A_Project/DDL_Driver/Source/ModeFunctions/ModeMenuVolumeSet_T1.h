//------------------------------------------------------------------------------
/** 	@file		ModeMenuVolumeSet_T1.h
	@brief	Programmable Volume Setting
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUVOLUMESET_T1_INCLUDED
#define __MODEMENUVOLUMESET_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"



#ifdef	LOCKSET_VOLUME_SET

void ModeGotoVolumeSetting(void);
void ModeVolumeSetting(void);


#endif

#endif


