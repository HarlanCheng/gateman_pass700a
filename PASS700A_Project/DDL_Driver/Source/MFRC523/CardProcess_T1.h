//------------------------------------------------------------------------------
/** 	@file		CardProcess_T1.h
	@brief	Card UID Read Functions 
*/
//------------------------------------------------------------------------------

#ifndef __CARDPROCESS_T1_INCLUDED
#define __CARDPROCESS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"
#include "RC523Functions_T1.h"

#define _USE_RFID_DETECTION_IREVO

//==============================================================================
//      S U C C E S S                                                                                            
// name Success
//==============================================================================

//------------------------------------------------------------------------------
/*! 	\def	MAX_CARD_ANTI_SIZE		
	Anticollision 알고리즘 수행을 통해 읽어들일 수 있는 최대 카드 수
	
	\def	MAX_CARD_UID_SIZE		
	읽어들일 수 있는 카드 UID의 최대 Byte 수

	\def	CARD_LOOP_DEFAULT_COUNT		
	입력된 카드의 UID를 읽어들이기 위한 전체 과정 반복 회수,
	READY 또는 ACTIVE 상태에서 REQA 전송할 경우에는 다시 IDLE 상태가 되기 때문에 최소 2회는 반복해야 함.
*/
//------------------------------------------------------------------------------
#define	MAX_CARD_ANTI_SIZE		5
#define	MAX_CARD_UID_SIZE		8
#define	MAX_CARD_DATA_SIZE		10
#define MAX_RFID_PIN_SIZE		8
#define	MIN_CARD_UID_SIZE		4


//sjc_RC523 add start
#define	MIN_CARDUID_LENGTH		4
#define	MAX_CARDUID_LENGTH		8
//sjc_RC523 add end


#define	CARD_LOOP_DEFAULT_COUNT		3

#ifdef _USE_RFID_DETECTION_IREVO
#define CARD_DETECT_ADC_MAX	5
#define CD_CALIBRATION_MAX		5

#define ADC_REFERENCE_VOLTAGE			3300			// 3.3V = 3300mv
#define ADC_RESOULUTION			12			// 12bit

#define CARD_DETECT_TH			100			// 100mv
#endif

typedef enum
{
	CARD_DETECT_IDLE = 0,
	CARD_DETECT_TX_ON,
	CARD_DETECT_CALIBRATION
}CardDetectionPrs;




//------------------------------------------------------------------------------
/*! 	\def	CARDREAD_SUCCESS		
	카드 읽기 결과 - 카드 UID 읽기 성공

	\def	CARDREAD_NO_CARD		
	카드 읽기 결과 - 카드 떨어짐

	\def	CARDREAD_FAIL		
	카드 읽기 결과 - 카드 UID 읽기 실패

	\def	CARDREAD_INNER_FORCED_LOCK		
	카드 읽기 결과 - 내부강제잠금 설정 상태

///	제품에 따라 설정 추가 가능
*/

enum{
	CARDREAD_SUCCESS = 1,
	CARDREAD_NO_CARD,
	CARDREAD_FAIL,
	CARDSET_SUCCESS,
	CARDSTATUS_FAIL,
	MKSREAD_SUCCESS,
	CARDMKS_EMER_FAIL,
	CARDREAD_INNER_FORCED_LOCK,


	CARDREAD_SUPPORT_CLASSIC_SUCCESS,					//  key A를 가지고 Block 1을 읽음. 즉 지원 가능한 카드인지 확인함. 
	CARDREAD_READ_COUNT_SUCCESS,					
	CARDREAD_WRITE_COUNT_SUCCESS,

	CARDREAD_VERIFY_FID_SUCCESS,
	CARDREAD_VERIFY_FID_FAIL,
	CARDREAD_WRITE_FID_SUCCESS,
	CARDREAD_WRITE_FID_FAIL,

	CARDREAD_SUPPORT_DESFIRE_SUCCESS,
	CARDREAD_SUPPORT_DESFIRE_FAIL,
	CARDREAD_DESFIRE_UPDATE_INITCARD_SUCCESS,
	CARDREAD_DESFIRE_UPDATE_INITCARD_FAIL,
	CARDREAD_DESFIRE_READ_SUCCESS,
	CARDREAD_DESFIRE_READ_FAIL,
};

//------------------------------------------------------------------------------




#define	CARDREAD_ING			250



#define	MAX_REG_CARD_SLOT		6				// FID저장을 위해 카드 한장당 가능한 SLOT 수.. 즉 한카드당 등록 가능한 도어락 수
#define	CARD_DETECTION_CHECK_CNT	5
//------------------------------------------------------------------------------
/*! 	\def	UID_NONE		
	TempSaveCompletedUID 함수의 Return 값 - UID가 모두 00h이거나 FFh일 경우 
	
	\def	UID_AVAILABLE		
	TempSaveCompletedUID 함수의 Return 값 - UID가 사용 가능할 경우 

	\def	UID_DUPICATED		
	TempSaveCompletedUID 함수의 Return 값 - UID가 이미 입력된 카드의 것과 동일할 경우 
*/
//------------------------------------------------------------------------------
#define	UID_NONE			0
#define	UID_AVAILABLE		1
#define	UID_DUPICATED		2


//gCardMode
enum{
	CARDMODE_POWER_ONOFF = 1,		//CardModePowerOnOff();	
	CARDMODE_SELECT,				//CardModeSelect();

	// Mifare Classic
	CARDMODE_READ_RESULT,			//CardModeReadResult();
	CARDMODE_CARD_AWAY_CHK,			//CardModeAwayFromReaderCheck();

	// Mifare Desfire
	CARDMODE_RATS,
	CARDMODE_DESFIRE_SELECT_APP,
	CARDMODE_DESFIRE_AUTHENTICATE,
	CARDMODE_DESFIRE_READ_FILE_00,
	CARDMODE_DESFIRE_READ_FILE_01,
	CARDMODE_DESFIRE_WRITE_FILE_01_STEP1,
	CARDMODE_DESFIRE_WRITE_FILE_01_STEP2,
};

//gCardProcessStep
enum
{
	//CardModePowerOnOff 함수에서의 ProcessStep
	CARDPRCS_IDLE_STATE = 0,
	CARDPRCS_RFID_POWERON,
	CARDPRCS_RFID_POWEROFF,
	CARDPRCS_RFID_TXON,
	CARDPRCS_RFID_TXOFF,
	CARDPRCS_READ_STOP,

	//CardModeSelect 함수에서의 ProcessStep	
	CARDPRCS_REQA_SEND,	
	CARDPRCS_WUPA_SEND,
	CARDPRCS_REQA_RESPOND,
	CARDPRCS_ANTICOLLISION_SEND,
	CARDPRCS_ANTICOLLISION_RESPOND,
	CARDPRCS_SELECT_SEND,
	CARDPRCS_SELECT_RESPOND,

	//CardModeReadResult 함수에서의 ProcessStep
	CARDPRCS_MIFARE_WAIT,							// 13
	
	CARDPRCS_MIFARE_PROCESS,
	CARDPRCS_MIFARE_KEYA_SEND,
	CARDPRCS_MIFARE_KEYA_RESPOND,
	CARDPRCS_MIFARE_GET_BLOCK_KEY_SEND,
	CARDPRCS_MIFARE_GET_BLOCK_KEY_RESPONSE,
	CARDPRCS_MIFARE_READ_COUNT_SEND,
	CARDPRCS_MIFARE_READ_COUNT_RESPONSE,			// 20
	CARDPRCS_MIFARE_WRITE_REG_COUNT_SEND,
	CARDPRCS_MIFARE_WRITE_REG_COUNT_RESPONSE,
	CARDPRCS_MIFARE_WRITE_REG_COUNT_VERIFY,

	CARDPRCS_MIFARE_READ_REG_DATA_START,
	CARDPRCS_MIFARE_READ_REG_DATA_SEND,
	CARDPRCS_MIFARE_READ_REG_DATA_RESPONSE,

	CARDPRCS_MIFARE_READ_AUTH_DATA_START,
	CARDPRCS_MIFARE_READ_AUTH_DATA_SEND,
	CARDPRCS_MIFARE_READ_AUTH_DATA_RESPONSE,
	CARDPRCS_MIFARE_READ_AUTH_DATA_NEXT_CHECH,



	CARDPRCS_MIFARE_WRITE_AUTH_DATA_SEND,
	CARDPRCS_MIFARE_WRITE_AUTH_DATA_RESPONSE,

	CARDPRCS_MIFARE_WRITE_DATA_SEND,
	CARDPRCS_MIFARE_WRITE_DATA_RESPONSE,	
	CARDPRCS_MIFARE_WRITE_DATA_VERIFY,
	//CardModeAwayFromReaderCheck 함수에서의 ProcessStep
	CARDPRCS_AWAY_CHECK,
	CARDPRCS_AWAY_REQA_SEND,
	CARDPRCS_AWAY_REQA_RESPOND,

	// Issuing a Request for Answer to Select
	CARDPRCS_RATS_SEND,
	CARDPRCS_RATS_RESPONSE,

	// Desfire
	CARDPRCS_DESFIRE_SELECT_APP_SEND,
	CARDPRCS_DESFIRE_SELECT_APP_RESPOND,
	CARDPRCS_DESFIRE_AUTH_READY,
	CARDPRCS_DESFIRE_AUTH_SEND_STEP1,
	CARDPRCS_DESFIRE_AUTH_RSP_STEP1,
	CARDPRCS_DESFIRE_AUTH_SEND_STEP2,
	CARDPRCS_DESFIRE_AUTH_RSP_STEP2,
	CARDPRCS_DESFIRE_READ_FILE00_SEND,
	CARDPRCS_DESFIRE_READ_FILE00_RSP,
	CARDPRCS_DESFIRE_READ_FILE01_SEND,
	CARDPRCS_DESFIRE_READ_FILE01_RSP,
	CARDPRCS_DESFIRE_WRITE_FILE01_STEP1_SEND,
	CARDPRCS_DESFIRE_WRITE_FILE01_STEP1_RSP,
	CARDPRCS_DESFIRE_WRITE_FILE01_STEP2_SEND,
	CARDPRCS_DESFIRE_WRITE_FILE01_STEP2_RSP,
};

// Desfire card type
typedef enum
{
	DESFIRE_CARD_NORMAL = 0, // normal card
	DESFIRE_CARD_ANTISHOP_ACTIVATE, // antishoplifting card
	DESFIRE_CARD_ANTISHOP_DEACTIVATE, // init card
	DESFIRE_CARD_ANTISHOP_TEMPORARY_DEACTIVATE // init test card: This is used at the shipment inspection
}DesfireCardType;

extern BYTE gCardMifareData[MAX_CARD_DATA_SIZE];
extern BYTE gCardAllUidBuf[MAX_CARD_ANTI_SIZE][MAX_CARD_UID_SIZE];
extern BYTE gCardReadStatus;
extern DesfireCardType gDesfireCardType;

void SetupCard(void);

void CardDetectionProcess(void);

void CardProcessTimeCounter(void);
void CardProcessTimeClear(void);
void CardAwayCheckTimeCounter(void);
BYTE GetCardMode(void);

//void CardTxOn(void);

void CardGotoReadStart(void);
void CardGotoReadStop(void);
void CardGotoAwayFromReaderCheck(void);

/*//sjc_RC523 add 주석 시작
void CardGotoReadFidCountStart(void);
void CardGotoWriteFidCountStart(void);
void CardGotoWriteFidDataStart(BYTE* bFid);
void CardGotoReadFidDataStart(BYTE bBlock);
BYTE GetCardFidBlock(void);

void CardGotoDesfireSelectAppProcessBySIO(BYTE ProcessStep);;
void CardGotoDesfireAuthProcessBySIO(BYTE ProcessStep);;
void CardGotoDesfireReadFile00ProcessBySIO(BYTE ProcessStep);
void CardGotoDesfireReadFile01ProcessBySIO(BYTE ProcessStep);
void CardGotoDesfireWriteFile01Step1ProcessBySIO(BYTE ProcessStep);
void CardGotoDesfireWriteFile01Step2ProcessBySIO(BYTE ProcessStep);
*///sjc_RC523 add 주석 끝

void CardReadStatusClear(void);
BYTE GetCardReadStatus(void);
BYTE GetCardInputNumber(void);

void CardModeProcess(void);

///////////////////card detection function ///////////////////////////

CardDetectionPrs GetCardDetectionMode(void);


void UpdateCardUidToMemoryByModule(BYTE* pCardUid, BYTE SlotNumber);
BYTE DataChecksumCal(BYTE *bpData, BYTE bNum);


#endif

