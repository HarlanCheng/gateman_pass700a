#include "Main.h"


#ifdef	BLE_N_SUPPORT

BYTE gbBLENTmpData[20];
BYTE gfBLENAllLockOut; 

#ifdef	__FRONT_BLE_CONTROL__
BYTE gbFrontBLENStatus;
BYTE gbFrontBLENStatusCheckTimer100ms;
#endif 

#define MAX_CREDENTIAL_TYPE 6


BYTE MakeGetRegisteredCredential(BYTE bType);
BYTE MakeGetSetting(void);
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
BYTE CheckOccupiedData(BYTE bType,BYTE bSlotNo);
BYTE CheckOccupiedUserCode(BYTE bSlotNo);
BYTE CheckOccupiedCard(BYTE bSlotNo);
BYTE CheckOccupiedFingerprint(BYTE bSlotNo);
BYTE CheckOccupiedFace(BYTE bSlotNo);
BYTE CheckOccupiedIButton(BYTE bSlotNo);
BYTE CheckOccupiedIris(BYTE bSlotNo);
BYTE MakeCredentialSlotWithSchedule(BYTE bType, BYTE bIndex);
BYTE MakeSlotDataWithSchdule(BYTE bType, BYTE bCredentialNumber, BYTE bIndex);

BYTE MakeGetSettingSubEvent(void);
BYTE CheckPinCodeSet(WORD wAddress);

BYTE MakeGetSupportedLanguages(void);
BYTE GetCurrentLanguageSet(void);
uint16_t GetSupportedLanguages(void);



#endif


void BLEN_Module_Test_Start(BYTE bType)
{
	BYTE tmpArray[2];

	tmpArray[0] = 0x76;
	tmpArray[1] = (bType == MENU_TESTMODE_BLE_APP) ? 0xFE : 0xFF;
 
	if(gfPackTypeiRevoBleN)
	{
		PackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_ACK, &gbComCnt, tmpArray,2);
	}
	else 	if(gfInnerPackTypeiRevoBleN)
	{
		InnerPackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_ACK, &gbInnerComCnt, tmpArray,2);
	}
	else
	{
		__NOP();
	}
}




void BLEN_Module_App_Test_Start(uint8_t BleAppTestKeyVal)
{
	BYTE tmpArray[3];

	tmpArray[0] = 0x76;
	tmpArray[1] = 0xFE;
	tmpArray[2] = BleAppTestKeyVal;
 
	if(gfPackTypeiRevoBleN)
	{
		PackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_ACK, &gbComCnt, tmpArray,3);
	}
	else 	if(gfInnerPackTypeiRevoBleN)
	{
		InnerPackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_ACK, &gbInnerComCnt, tmpArray,3);
	}
	else
	{
		__NOP();
	}
}



void BLENTxStartScanSend(BYTE bSource)
{
	BYTE tmpArray[3];

	tmpArray[0] = SUBEV_START_SCAN;
	tmpArray[1] = bSource;
	tmpArray[2] = (gfBLENAllLockOut ? 0x01 : 0x00);
 
	if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)
	{
		PackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_EVENT, &gbComCnt, tmpArray, 3);
	}
	else 	if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
	{
		InnerPackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 3);
	}
	else
	{
		__NOP();
	}
}


void BLENTxStopScanSend(void)
{
	BYTE tmpArray[1];

	tmpArray[0] = SUBEV_STOP_SCAN;

	if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN) 
	{
		PackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_EVENT, &gbComCnt, tmpArray, 1);
	}
	else if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN) 
	{
		InnerPackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 1);
	}
	else
	{
		__NOP();
	}

}

void BLENTxIdCompareDatasend(BYTE bSource)
{
	BYTE tmpArray[2];

	tmpArray[0] = SUBEV_GET_COMPARE_SAVED_MODULE_ID;
	tmpArray[1] = bSource;

	if(gfPackTypeiRevoBleN)
	{
		PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], tmpArray, 2);
	}
	else	if(gfInnerPackTypeiRevoBleN) 
	{
		InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], tmpArray, 2);			
	}
	else
	{
		__NOP();
	}
}


void BLENTxGetSettingNackSend(void)
{
	BYTE tmpArray[1];

	tmpArray[0] = SUBEV_GET_SETTING;

	if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)	
	{
		PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], tmpArray, 1);
 	}
	else 	if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
	{
		InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], tmpArray, 1);
	}
	else
	{
		__NOP();
	}
}

BYTE BLEPackIDCheck(void)
{
	BYTE bTmp;
	BYTE InnerbTmp;	
	BYTE SavedIdBuf[5];
	BYTE InnerSavedIdBuf[5];	
	BYTE bResult;

	RomRead(SavedIdBuf, PACK_ID, 4);
	RomRead(InnerSavedIdBuf, INNER_PACK_ID, 4);	
	
	bTmp = (BYTE)memcmp(SavedIdBuf, &gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1], 4);
	InnerbTmp = (BYTE)memcmp(InnerSavedIdBuf, &gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA+1], 4);	

	if(DataCompare(SavedIdBuf, 0xFF, 4) == _DATA_IDENTIFIED || DataCompare(SavedIdBuf, 0x00, 4) == _DATA_IDENTIFIED)
	{
		bTmp = 0xFF;
	}

	if(DataCompare(InnerSavedIdBuf, 0xFF, 4) == _DATA_IDENTIFIED || DataCompare(InnerSavedIdBuf, 0x00, 4) == _DATA_IDENTIFIED)
	{
		InnerbTmp = 0xFF;
	}
	
	if(bTmp == 0 || InnerbTmp == 0)
	{
		bResult = SAME_WITH_SAVED_ONE;
	}
	else
	{
		bResult = DIFFERENT_WITH_SAVED_ONE;			
	}

	if(InnerbTmp == 0)
	{
		gbInnerModuleMode = PACK_ID_CONFIRMED; 	
	}
	else if (bTmp== 0)
	{
		gbModuleMode = PACK_ID_CONFIRMED; 
	}

	return bResult;
}



/* 0x74 에 0xB4 의 7th byte 에 0xFF 가 오면 등록 모드 취소로 */
BYTE BLENCredentialCancleCheck(BYTE bType)
{
	BYTE bTmp = 0x00; 

 	switch(bType)
	{
#ifdef 	DDL_CFG_RFID
		case CREDENTIALTYPE_CARD :
			if(gbMainMode == MODE_CARD_REGISTER 
				||gbMainMode == MODE_MENU_ONECARD_REGISTER
				||gbMainMode == MODE_CARD_REGISTER_BYBLEN
				||gbMainMode == MODE_MENU_ONECARD_REGISTER_BYBLEN)
			{
				CardGotoReadStop();
				bTmp = 0x01;
			}	
			else 
			{
				bTmp = 0x00;
			}
			break;
#endif

#ifdef 	DDL_CFG_FP //hyojoon_20160831 PFM-3000 add 
		case CREDENTIALTYPE_FINGERPRINT :
#if defined (DDL_CFG_FP_TCS4K)
			if(gbMainMode == MODE_MENU_ONEFINGER_REGISTER_BYBLEN ||
				gbMainMode == MODE_MENU_FINGER_REGISTER_BYBLEN||
				gbMainMode == MODE_FINGER_PT_OPEN ||
				gbMainMode == MODE_MENU_FINGER_REGISTER ||
				gbMainMode == MODE_MENU_ONEFINGER_REGISTER)
#elif defined (DDL_CFG_FP_PFM_3000) ||defined (DDL_CFG_FP_TS1071M)
			if(gbMainMode == MODE_MENU_ONEFINGER_REGISTER_BYBLEN ||
				gbMainMode == MODE_MENU_FINGER_REGISTER_BYBLEN||
				gbMainMode == MODE_MENU_FINGER_REGISTER ||
				gbMainMode == MODE_MENU_ONEFINGER_REGISTER)
#elif defined (DDL_CFG_FP_HFPM) || defined (DDL_CFG_FP_INTEGRATED_FPM)				
			if(gbMainMode == MODE_MENU_FINGER_REGISTER ||	gbMainMode == MODE_MENU_ONEFINGER_REGISTER)
#endif 

			{
#if defined (DDL_CFG_FP_HFPM)
				HFPMTxEventEnroll(0xFF);
#elif  defined (DDL_CFG_FP_INTEGRATED_FPM)
				NFPMTxEventStop(0x00);
				Delay(SYS_TIMER_30MS);
#endif 
				BioModuleOff();
				bTmp = 0x01;
			}
			else 
			{
				bTmp = 0x00;
			}
			break;			
#endif


		case CREDENTIALTYPE_GMTOUCHKEY :
#if defined (DDL_CFG_DS1972_IBUTTON)			
			if(gbMainMode == MODE_DS1972_TOUCHKEY_REGISTER_BYBLEN ||
				gbMainMode == MODE_ONEDS1972_TOUCHKEY_REGISTER_BYBLEN)
			{
				iButtonModeClear();
				bTmp = 0x01;
			}
			else 
			{
				bTmp = 0x00;
			}

#elif defined (DDL_CFG_IBUTTON)
			if(gbMainMode == MODE_GMTOUCHKEY_REGISTER_BYBLEN ||
				gbMainMode == MODE_MENU_ONEGMTOUCHKEY_REGISTER_BYBLEN)
			{
				GMTouchKeyInputClear();
				bTmp = 0x01;
			}
			else 
			{
				bTmp = 0x00;
			}

#endif
			break;


		default : 
			bTmp = 0x00;
			break;
	}
	
	return bTmp;
 }


void BLENSubCommandProcess(BYTE bSubEvent)
{
	BYTE bNum;

	switch(bSubEvent)
	{
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
		case SUBEV_GET_CREDENTIAL_SLOT_WITH_SCHEDULE:
			if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)
			{
				bNum = MakeCredentialSlotWithSchedule(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1], gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2]);
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);							
			}
			else if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
			{
				bNum = MakeCredentialSlotWithSchedule(gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA+1], gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA+2]);
				InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);							
			}
			else
			{
				__NOP();
			}
			break;

		case SUBEV_GET_SETTING_SUB_EVENT:
			if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)
			{
				bNum = MakeGetSettingSubEvent();
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);							
			}
			else if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
			{
				bNum = MakeGetSettingSubEvent();
				InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);							
			}
			else
			{
				__NOP();
			}
			break;

		case SUBEV_GET_SUPPORTED_LANGUAGES:
			if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)
			{
				bNum = MakeGetSupportedLanguages();
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);								
			}
			else if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
			{
				bNum = MakeGetSupportedLanguages();
				InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);								
			}
			else
			{
				__NOP();
			}
			break;
#endif
	
		case SUBEV_GET_REGISTERD_CREDENTIAL: 	
			if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)
			{
				bNum = MakeGetRegisteredCredential(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);			
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else	if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
			{
				bNum = MakeGetRegisteredCredential(gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);			
				InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else
			{
				__NOP();
			}
			break;

		case SUBEV_GET_SETTING:
			// 알람중일 떄  설정 못 들어 가게 
			bNum = AlarmStatusCheck();
			if(bNum == STATUS_SUCCESS){ 
				BLENTxGetSettingNackSend();
				break;
			}
				
			bNum = MakeGetSetting();	
			
			if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)
			{
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else 	if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
			{
				InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else
			{
				__NOP();
			}

			// 3분 락만 해제 하고 허수는 못하게 
			if(GetTamperProofPrcsStep())
			{
				TamperCountClear();
			}
			
			break;

		case SUBEV_ALL_CREDENTIAL_LOCKOUT:
			if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)
			{
				if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1] == 0x01) 
					gfBLENAllLockOut = 1; //enable 
				else 
					gfBLENAllLockOut = 0; //disable
			}
			else 	if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
			{
				if(gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA+1] == 0x01) 
					gfBLENAllLockOut = 1; //enable 
				else 
					gfBLENAllLockOut = 0; //disable
			}
			else
			{
				__NOP();
			}

			SetAllLockOutStatus((BYTE)gfBLENAllLockOut);
			
			bNum = 0x01;
			if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)
			{
				gbBLENTmpData[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else 	if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
			{
				gbBLENTmpData[0] = gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA];
				InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else
			{
				__NOP();
			}
			break;

		case SUBEV_GET_COMPARE_SAVED_MODULE_ID:
			BLENTxIdCompareDatasend(BLEPackIDCheck());
			break;
		
		case SUBEV_CHANGE_CREDENTIAL_REGISTRATION:
			if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)
			{			
				if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+3] == BLE_CREDENCIAL_REGISTER)
				{
					BLENTxChangeCredentialRegistrationMode(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1], gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2]);
				}
				else if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+3] == BLE_CREDENCIAL_REG_CANCLE)
				{
					if(BLENCredentialCancleCheck(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]))
					{ 	
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						ModeClear();
					}
				}
 				bNum = 0x01;
				gbBLENTmpData[0] = bSubEvent;
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else 	if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
			{
				if(gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA+3] == BLE_CREDENCIAL_REGISTER)
				{
					BLENTxChangeCredentialRegistrationMode(gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA+1], gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA+2]);
				}
				else if(gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA+3] == BLE_CREDENCIAL_REG_CANCLE)
				{
					if(BLENCredentialCancleCheck(gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA+1]))
					{ 	
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						ModeClear();
					}
				}
	 			bNum = 0x01;
				gbBLENTmpData[0] = bSubEvent;
				InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else
			{
				__NOP();
			}
			break;

		case SUBEV_GET_LOCK_OPERATION_MODE:
#ifdef BLE_JIG_MODE
			gbBLENTmpData[0] = SUBEV_GET_LOCK_OPERATION_MODE;
			if(gfPackTypeiRevoBleN) //등록 하기 전이므로 flag 만 비교 
			{
				bNum = 3;
				gbBLENTmpData[1] = (gbManageMode == _AD_MODE_SET) ? 0xFF : 0xFE;
				gbBLENTmpData[2] = 0x01; // JIG 수 만큼 늘릴 것 
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else 	if(gfInnerPackTypeiRevoBleN) //등록 하기 전이므로 flag 만 비교 
			{
				bNum = 3;
				gbBLENTmpData[1] = (gbManageMode == _AD_MODE_SET) ? 0xFF : 0xFE;
				gbBLENTmpData[2] = 0x01; // JIG 수 만큼 늘릴 것 
				InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else
			{
				__NOP();
			}
#else 
			if(gfPackTypeiRevoBleN) //등록 하기 전이므로 flag 만 비교 
			{
				bNum = 2;
				gbBLENTmpData[0] = SUBEV_GET_LOCK_OPERATION_MODE;

				// 일반 모드의 값이 0이 아닌 0xBB로 회신해야 해서, 해당 내용을 Converting하는 함수 추가하여 처리
				gbBLENTmpData[1] = GetCurrentOperationMode();
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else 	if(gfInnerPackTypeiRevoBleN) //등록 하기 전이므로 flag 만 비교 
			{
				bNum = 2;
				gbBLENTmpData[0] = SUBEV_GET_LOCK_OPERATION_MODE;

				// 일반 모드의 값이 0이 아닌 0xBB로 회신해야 해서, 해당 내용을 Converting하는 함수 추가하여 처리
				gbBLENTmpData[1] = GetCurrentOperationMode();
				InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], gbBLENTmpData, bNum);
			}
			else
			{
				__NOP();
			}
#ifdef	__FRONT_BLE_CONTROL__
			gbFrontBLENStatusCheckTimer100ms = 5;
			gInnerPackConnectionMode = 4;
#endif 			
			
#endif 						
			break;

		default :
			if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN )
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_UNKNOWN_MESSAGE,0x00); 	// Nack 송신 준비 
			}
			else 	if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
			{
				InnerPackTx_MakeNack(gbInnerPackRxDataCopiedBuf,NACK_UNKNOWN_MESSAGE,0x00); 	// Nack 송신 준비 
			}
			else
			{
				__NOP();
			}

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;
	}
}	



WORD CredentialOccupiedCheck(WORD wStartAdd, BYTE bNum, BYTE bLimit)
{
	BYTE bCnt;
	BYTE bTmp;
	BYTE bTmp1[8];
	WORD bResult = 0;

	for(bCnt = 0; bCnt < bLimit; bCnt++)
	{
		// CARD_UID = TOUCHKEY_UID 인 조건에서 운영
		if(wStartAdd == CARD_UID)
		{	
			bTmp = 0xFF;
			RomRead(bTmp1, wStartAdd+((WORD)bNum*(WORD)bCnt), 8);

#if	defined (DDL_CFG_DS1972_IBUTTON) || defined (DDL_CFG_IBUTTON)
			;
#else 
#if defined (_USE_IREVO_CRYPTO_)
			EncryptDecryptKey(bTmp1, 8);
#endif 
#endif
			if(DataCompare(bTmp1, 0xFF, 8) != 0)
			{
				bTmp = 0x01; // bTmp1[0];// 아무 값이나 0xFF 만 아니면 ...
			}
		}
		else 
		{
		// bNum : Credential의 저장된 최소 Byte 간격 단위 
			RomRead(&bTmp, wStartAdd+((WORD)bNum*(WORD)bCnt), 1);
#if defined (_USE_IREVO_CRYPTO_)
			if(wStartAdd == USER_CODE)
			{
				EncryptDecryptKey(&bTmp,1);
			}
#endif 			
		}
		
		if(bTmp != 0xFF)
		{
			// Slot Number 1의 상태가 Bit 0에 저장
			// Slot Number 8의 상태가 Bit 7에 저장
			// Occupied일 경우 1, Available일 경우 0
			//bResult |= (0x01 >> bCnt);

			switch(wStartAdd)
			{
#if	defined (DDL_CFG_DS1972_IBUTTON) || defined (DDL_CFG_IBUTTON)
				case TOUCHKEY_UID :

					if(bLimit >= 40)
					{
						if(bCnt < 8) // 1 ~ 8
							gbBLENTmpData[7] |= (0x01 << bCnt);
						else if(bCnt < 16) 
							gbBLENTmpData[6] |= (0x01 << (bCnt-8));
						else if(bCnt < 24)
							gbBLENTmpData[5] |= (0x01 << (bCnt-16));
						else if(bCnt < 32)
							gbBLENTmpData[4] |= (0x01 << (bCnt-24));
						else 
							gbBLENTmpData[3] |= (0x01 << (bCnt-32));

						break;
					}
#endif 					
				case USER_CODE :
#if (SUPPORTED_USERCODE_NUMBER == 100)	
					{
						if(bCnt < 8) // 1 ~ 8
							gbBLENTmpData[15] |= (0x01 << bCnt);
						else if(bCnt < 16) 
							gbBLENTmpData[14] |= (0x01 << (bCnt-8));
						else if(bCnt < 24)
							gbBLENTmpData[13] |= (0x01 << (bCnt-16));
						else if(bCnt < 32)
							gbBLENTmpData[12] |= (0x01 << (bCnt-24));
						else if(bCnt < 40)
							gbBLENTmpData[11] |= (0x01 << (bCnt-32));
						else if(bCnt < 48)
							gbBLENTmpData[10] |= (0x01 << (bCnt-40));
						else if(bCnt < 56)
							gbBLENTmpData[9] |= (0x01 << (bCnt-48));
						else if(bCnt < 64)
							gbBLENTmpData[8] |= (0x01 << (bCnt-56));
						else if(bCnt < 72)
							gbBLENTmpData[7] |= (0x01 << (bCnt-64));
						else if(bCnt < 80)
							gbBLENTmpData[6] |= (0x01 << (bCnt-72));
						else if(bCnt < 88)
							gbBLENTmpData[5] |= (0x01 << (bCnt-80));
						else if(bCnt < 96)
							gbBLENTmpData[4] |= (0x01 << (bCnt-88));
						else 
							gbBLENTmpData[3] |= (0x01 << (bCnt-96));
					}		
#else 					
					{
						WORD bTmp2;

						bTmp2 = 0x01;
						bTmp2  = (bTmp2 << bCnt);
						bResult |= bTmp2;
					}
#endif 					
					break;

#ifdef	DDL_CFG_FP  //hyojoon_20160831 PFM-3000 add
#if defined (DDL_CFG_FP_TCS4K)
				case BIO_ID :
					WORD bTmp2;
					
					if(gbManageMode==_AD_MODE_SET)
					{
						// finger 는 advanced mode 일때  BCD type 으로 slot number 를 저장 하니 
						bTmp2 = bTmp & 0xF0;
						bTmp2 = (bTmp2 >> 4) * 10;
						bTmp = bTmp2 + (bTmp & 0x0F); //BCD 를 hex 로 바꾸고 
						bTmp2 = 0x01;
						bTmp2  = (bTmp2 << (bTmp-1));
						bResult |= bTmp2;
					}
					else 
					{
						//normal mode 는 순차 적으로 쌓이니 count 만큼만 
						bTmp = GetNoFingerFromEeprom(); 
						for(bCnt = 0 ; bCnt < bTmp; bCnt++)
						{
							bResult |= 0x01 << bCnt ;
						}
						bCnt = bLimit; //위에 for 문 안돌게 
					}
					break;
#endif 					
#if (_MAX_REG_BIO == 20) && (defined (DDL_CFG_FP_TS1071M) || defined (DDL_CFG_FP_HFPM) || defined (DDL_CFG_FP_INTEGRATED_FPM))  // 20개 일때 
				case BIO_ID :
					WORD bTmp2;
					
					if(gbManageMode==_AD_MODE_SET)
					{
						bTmp2 = 0x01;
						bTmp2  = (bTmp2 << bCnt);
						bResult |= bTmp2;
					}
					else 
					{
						//normal mode 는 순차 적으로 쌓이니 count 만큼만 
						bTmp = GetNoFingerFromEeprom(); 
						for(bCnt = 0 ; bCnt < bTmp; bCnt++)
						{
							bResult |= 0x01 << bCnt ;
						}
						bCnt = bLimit; //위에 for 문 안돌게 
					}
					break;
#endif 					
#if (_MAX_REG_BIO == 40) && (defined (DDL_CFG_FP_TS1071M) ||defined (DDL_CFG_FP_PFM_3000)  || defined (DDL_CFG_FP_INTEGRATED_FPM))  // 40 개 일때 
				case BIO_ID :
					if(gbManageMode==_AD_MODE_SET)
					{
						if(bCnt < 8) // 1 ~ 8
							gbBLENTmpData[7] |= (0x01 << bCnt);
						else if(bCnt < 16) 
							gbBLENTmpData[6] |= (0x01 << (bCnt-8));
						else if(bCnt < 24)
							gbBLENTmpData[5] |= (0x01 << (bCnt-16));
						else if(bCnt < 32)
							gbBLENTmpData[4] |= (0x01 << (bCnt-24));
						else 
							gbBLENTmpData[3] |= (0x01 << (bCnt-32));
					}
					else 
					{
						//normal mode 는 순차 적으로 쌓이니 count 만큼만 
						bTmp = GetNoFingerFromEeprom(); 
						for(bCnt = 0 ; bCnt < bTmp ; bCnt++)
						{
							if(bCnt < 8) // 1 ~ 8
								gbBLENTmpData[7] |= (0x01 << bCnt);
							else if(bCnt < 16) 
								gbBLENTmpData[6] |= (0x01 << (bCnt-8));
							else if(bCnt < 24)
								gbBLENTmpData[5] |= (0x01 << (bCnt-16));
							else if(bCnt < 32)
								gbBLENTmpData[4] |= (0x01 << (bCnt-24));
							else 
								gbBLENTmpData[3] |= (0x01 << (bCnt-32));
						}
						bCnt = bLimit; //위에 for 문 안돌게 						
					}
				break;
#endif
#if (_MAX_REG_BIO == 100) && (defined (DDL_CFG_FP_TS1071M) ||defined (DDL_CFG_FP_PFM_3000)  || defined (DDL_CFG_FP_INTEGRATED_FPM))  // 100 개 일때 
				case BIO_ID :
					if(gbManageMode==_AD_MODE_SET)
					{
							if(bCnt < 8) // 1 ~ 8
								gbBLENTmpData[15] |= (0x01 << bCnt);
							else if(bCnt < 16) 
								gbBLENTmpData[14] |= (0x01 << (bCnt-8));
							else if(bCnt < 24)
								gbBLENTmpData[13] |= (0x01 << (bCnt-16));
							else if(bCnt < 32)
								gbBLENTmpData[12] |= (0x01 << (bCnt-24));
							else if(bCnt < 40)
								gbBLENTmpData[11] |= (0x01 << (bCnt-32));
							else if(bCnt < 48)
								gbBLENTmpData[10] |= (0x01 << (bCnt-40));
							else if(bCnt < 56)
								gbBLENTmpData[9] |= (0x01 << (bCnt-48));
							else if(bCnt < 64)
								gbBLENTmpData[8] |= (0x01 << (bCnt-56));
							else if(bCnt < 72)
								gbBLENTmpData[7] |= (0x01 << (bCnt-64));
							else if(bCnt < 80)
								gbBLENTmpData[6] |= (0x01 << (bCnt-72));
							else if(bCnt < 88)
								gbBLENTmpData[5] |= (0x01 << (bCnt-80));
							else if(bCnt < 96)
								gbBLENTmpData[4] |= (0x01 << (bCnt-88));
							else 
								gbBLENTmpData[3] |= (0x01 << (bCnt-96));
					}
					else 
					{
						//normal mode 는 순차 적으로 쌓이니 count 만큼만 
						bTmp = GetNoFingerFromEeprom(); 
						for(bCnt = 0 ; bCnt < bTmp ; bCnt++)
						{
							if(bCnt < 8) // 1 ~ 8
								gbBLENTmpData[15] |= (0x01 << bCnt);
							else if(bCnt < 16) 
								gbBLENTmpData[14] |= (0x01 << (bCnt-8));
							else if(bCnt < 24)
								gbBLENTmpData[13] |= (0x01 << (bCnt-16));
							else if(bCnt < 32)
								gbBLENTmpData[12] |= (0x01 << (bCnt-24));
							else if(bCnt < 40)
								gbBLENTmpData[11] |= (0x01 << (bCnt-32));
							else if(bCnt < 48)
								gbBLENTmpData[10] |= (0x01 << (bCnt-40));
							else if(bCnt < 56)
								gbBLENTmpData[9] |= (0x01 << (bCnt-48));
							else if(bCnt < 64)
								gbBLENTmpData[8] |= (0x01 << (bCnt-56));
							else if(bCnt < 72)
								gbBLENTmpData[7] |= (0x01 << (bCnt-64));
							else if(bCnt < 80)
								gbBLENTmpData[6] |= (0x01 << (bCnt-72));
							else if(bCnt < 88)
								gbBLENTmpData[5] |= (0x01 << (bCnt-80));
							else if(bCnt < 96)
								gbBLENTmpData[4] |= (0x01 << (bCnt-88));
							else 
								gbBLENTmpData[3] |= (0x01 << (bCnt-96));
							
						}
						bCnt = bLimit; //위에 for 문 안돌게 						
					}
				break;
#endif

#endif

#ifdef	DDL_CFG_RFID
				case CARD_UID :
#if (SUPPORTED_USERCARD_NUMBER == 100)	
					{
						if(bCnt < 8) // 1 ~ 8
							gbBLENTmpData[15] |= (0x01 << bCnt);
						else if(bCnt < 16) 
							gbBLENTmpData[14] |= (0x01 << (bCnt-8));
						else if(bCnt < 24)
							gbBLENTmpData[13] |= (0x01 << (bCnt-16));
						else if(bCnt < 32)
							gbBLENTmpData[12] |= (0x01 << (bCnt-24));
						else if(bCnt < 40)
							gbBLENTmpData[11] |= (0x01 << (bCnt-32));
						else if(bCnt < 48)
							gbBLENTmpData[10] |= (0x01 << (bCnt-40));
						else if(bCnt < 56)
							gbBLENTmpData[9] |= (0x01 << (bCnt-48));
						else if(bCnt < 64)
							gbBLENTmpData[8] |= (0x01 << (bCnt-56));
						else if(bCnt < 72)
							gbBLENTmpData[7] |= (0x01 << (bCnt-64));
						else if(bCnt < 80)
							gbBLENTmpData[6] |= (0x01 << (bCnt-72));
						else if(bCnt < 88)
							gbBLENTmpData[5] |= (0x01 << (bCnt-80));
						else if(bCnt < 96)
							gbBLENTmpData[4] |= (0x01 << (bCnt-88));
						else 
							gbBLENTmpData[3] |= (0x01 << (bCnt-96));
					}
#else 
					{
						// credential 수가 33 이상 부터 32bit 가 넘어가 64 bit 변수를 선언해 봤지만 32bit 넘어서면 저장 못하는 
						// 것으로 봐서 64bit 변수 선언이 불가능 한듯 
						// card 는 40개 까지 저장이 가능 하니 여기서 바로 채우는 것으로 변경 

						if(bCnt < 8) // 1 ~ 8
							gbBLENTmpData[7] |= (0x01 << bCnt);
						else if(bCnt < 16) 
							gbBLENTmpData[6] |= (0x01 << (bCnt-8));
						else if(bCnt < 24)
							gbBLENTmpData[5] |= (0x01 << (bCnt-16));
						else if(bCnt < 32)
							gbBLENTmpData[4] |= (0x01 << (bCnt-24));
						else 
							gbBLENTmpData[3] |= (0x01 << (bCnt-32));
					}
#endif 
					break;
#endif					
				default : break;
			}
		}
	}
	return (bResult);
}	

WORD CredentialOccupiedCheck100Support(WORD wStartAdd, BYTE bNum, BYTE bLimit)
{
		BYTE bCnt;
		BYTE bTmp;
		BYTE bTmp1[8];
		WORD bResult = 0;							
		BYTE gbSlotBit[13] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; 	
	
		gbBLENTmpData[1] = 0xA2;	//Total Index 0xA#, 100개면 총 인덱스 2개  (index 1, index 2) 
		gbBLENTmpData[2] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2];  //REQ index 요청 인덱스 

		if(wStartAdd == USER_CODE)
		{
			gbBLENTmpData[3] = CREDENTIALTYPE_PINCODE;  		//Credential Type
			gbBLENTmpData[4] = SUPPORTED_USERCODE_NUMBER;		// Number of Supported	
		}
#ifdef DDL_CFG_RFID
		else if(wStartAdd == CARD_UID)
		{
			gbBLENTmpData[3] = CREDENTIALTYPE_CARD; 		//Credential Type
			gbBLENTmpData[4] = SUPPORTED_USERCARD_NUMBER;		// Number of Supported
		}
#endif		
#ifdef DDL_CFG_FP
		else if(wStartAdd == BIO_ID)
		{	
			gbBLENTmpData[3] = CREDENTIALTYPE_FINGERPRINT;  	//Credential Type
			gbBLENTmpData[4] = SUPPORTED_FINGERPRINT_NUMBER;	// Number of Supported	
		}
		
#endif
		for(bCnt = 0; bCnt < bLimit; bCnt++)
		{
			// CARD_UID = TOUCHKEY_UID 인 조건에서 운영
			if(wStartAdd == CARD_UID)
			{	
				bTmp = 0xFF;
				RomRead(bTmp1, wStartAdd+((WORD)bNum*(WORD)bCnt), 8);
#if defined (_USE_IREVO_CRYPTO_)
				EncryptDecryptKey(bTmp1, 8);
#endif 
				if(DataCompare(bTmp1, 0xFF, 8) != 0)
				{
					bTmp = 0x01; // bTmp1[0];// 아무 값이나 0xFF 만 아니면 ...
				}
			}
			else 
			{
			// bNum : Credential의 저장된 최소 Byte 간격 단위 
				RomRead(&bTmp, wStartAdd+((WORD)bNum*(WORD)bCnt), 1);
#if defined (_USE_IREVO_CRYPTO_)
				if(wStartAdd == USER_CODE)
				{
					EncryptDecryptKey(&bTmp,1);
				}
#endif 			
			}
			
			if(bTmp != 0xFF)
			{
				// Slot Number 1의 상태가 Bit 0에 저장
				// Slot Number 8의 상태가 Bit 7에 저장
				// Occupied일 경우 1, Available일 경우 0
				//bResult |= (0x01 >> bCnt);

				switch(wStartAdd)
				{
					case USER_CODE :
#if (SUPPORTED_USERCODE_NUMBER == 100)	
						{
							if(bCnt < 8) // 1 ~ 8
								gbSlotBit[12] |= (0x01 << bCnt);
							else if(bCnt < 16) 
								gbSlotBit[11] |= (0x01 << (bCnt-8));
							else if(bCnt < 24)
								gbSlotBit[10] |= (0x01 << (bCnt-16));
							else if(bCnt < 32)
								gbSlotBit[9] |= (0x01 << (bCnt-24));
							else if(bCnt < 40)
								gbSlotBit[8] |= (0x01 << (bCnt-32));
							else if(bCnt < 48)
								gbSlotBit[7] |= (0x01 << (bCnt-40));
							else if(bCnt < 56)
								gbSlotBit[6] |= (0x01 << (bCnt-48));
							else if(bCnt < 64)
								gbSlotBit[5] |= (0x01 << (bCnt-56));
							else if(bCnt < 72)
								gbSlotBit[4] |= (0x01 << (bCnt-64));
							else if(bCnt < 80)
								gbSlotBit[3] |= (0x01 << (bCnt-72));
							else if(bCnt < 88)
								gbSlotBit[2] |= (0x01 << (bCnt-80));
							else if(bCnt < 96)
								gbSlotBit[1] |= (0x01 << (bCnt-88));
							else 
								gbSlotBit[0] |= (0x01 << (bCnt-96));
						}		
#else 		
						{
							WORD bTmp2;

							bTmp2 = 0x01;
							bTmp2  = (bTmp2 << bCnt);
							bResult |= bTmp2;
						}
#endif 					
						break;

#ifdef	DDL_CFG_FP  //hyojoon_20160831 PFM-3000 add
#if defined (DDL_CFG_FP_TCS4K)
					case BIO_ID :
						WORD bTmp2;
						
						if(gbManageMode==_AD_MODE_SET)
						{
							// finger 는 advanced mode 일때  BCD type 으로 slot number 를 저장 하니 
							bTmp2 = bTmp & 0xF0;
							bTmp2 = (bTmp2 >> 4) * 10;
							bTmp = bTmp2 + (bTmp & 0x0F); //BCD 를 hex 로 바꾸고 
							bTmp2 = 0x01;
							bTmp2  = (bTmp2 << (bTmp-1));
							bResult |= bTmp2;
						}
						else 
						{
							//normal mode 는 순차 적으로 쌓이니 count 만큼만 
							bTmp = GetNoFingerFromEeprom(); 
							for(bCnt = 0 ; bCnt < bTmp; bCnt++)
							{
								bResult |= 0x01 << bCnt ;
							}
							bCnt = bLimit; //위에 for 문 안돌게 
						}
						break;
#endif 					
#if (_MAX_REG_BIO == 100) && (defined (DDL_CFG_FP_TS1071M) ||defined (DDL_CFG_FP_PFM_3000)  || defined (DDL_CFG_FP_INTEGRATED_FPM))  // 100 개 일때 
					case BIO_ID :
						if(gbManageMode==_AD_MODE_SET)
						{
							if(bCnt < 8) // 1 ~ 8
								gbSlotBit[12] |= (0x01 << bCnt);
							else if(bCnt < 16) 
								gbSlotBit[11] |= (0x01 << (bCnt-8));
							else if(bCnt < 24)
								gbSlotBit[10] |= (0x01 << (bCnt-16));
							else if(bCnt < 32)
								gbSlotBit[9] |= (0x01 << (bCnt-24));
							else if(bCnt < 40)
								gbSlotBit[8] |= (0x01 << (bCnt-32));
							else if(bCnt < 48)
								gbSlotBit[7] |= (0x01 << (bCnt-40));
							else if(bCnt < 56)
								gbSlotBit[6] |= (0x01 << (bCnt-48));
							else if(bCnt < 64)
								gbSlotBit[5] |= (0x01 << (bCnt-56));
							else if(bCnt < 72)
								gbSlotBit[4] |= (0x01 << (bCnt-64));
							else if(bCnt < 80)
								gbSlotBit[3] |= (0x01 << (bCnt-72));
							else if(bCnt < 88)
								gbSlotBit[2] |= (0x01 << (bCnt-80));
							else if(bCnt < 96)
								gbSlotBit[1] |= (0x01 << (bCnt-88));
							else 
								gbSlotBit[0] |= (0x01 << (bCnt-96));

						}
						else 
						{
							//normal mode 는 순차 적으로 쌓이니 count 만큼만 
							bTmp = GetNoFingerFromEeprom(); 
							for(bCnt = 0 ; bCnt < bTmp ; bCnt++)
							{
								if(bCnt < 8) // 1 ~ 8
									gbSlotBit[12] |= (0x01 << bCnt);
								else if(bCnt < 16) 
									gbSlotBit[11] |= (0x01 << (bCnt-8));
								else if(bCnt < 24)
									gbSlotBit[10] |= (0x01 << (bCnt-16));
								else if(bCnt < 32)
									gbSlotBit[9] |= (0x01 << (bCnt-24));
								else if(bCnt < 40)
									gbSlotBit[8] |= (0x01 << (bCnt-32));
								else if(bCnt < 48)
									gbSlotBit[7] |= (0x01 << (bCnt-40));
								else if(bCnt < 56)
									gbSlotBit[6] |= (0x01 << (bCnt-48));
								else if(bCnt < 64)
									gbSlotBit[5] |= (0x01 << (bCnt-56));
								else if(bCnt < 72)
									gbSlotBit[4] |= (0x01 << (bCnt-64));
								else if(bCnt < 80)
									gbSlotBit[3] |= (0x01 << (bCnt-72));
								else if(bCnt < 88)
									gbSlotBit[2] |= (0x01 << (bCnt-80));
								else if(bCnt < 96)
									gbSlotBit[1] |= (0x01 << (bCnt-88));
								else 
									gbSlotBit[0] |= (0x01 << (bCnt-96));
								
							}
							bCnt = bLimit; //위에 for 문 안돌게 						
						}
					break;
#endif
	
#endif
	
#ifdef	DDL_CFG_RFID
					case CARD_UID :
						{
							if(bCnt < 8) // 1 ~ 8
								gbSlotBit[12] |= (0x01 << bCnt);
							else if(bCnt < 16) 
								gbSlotBit[11] |= (0x01 << (bCnt-8));
							else if(bCnt < 24)
								gbSlotBit[10] |= (0x01 << (bCnt-16));
							else if(bCnt < 32)
								gbSlotBit[9] |= (0x01 << (bCnt-24));
							else if(bCnt < 40)
								gbSlotBit[8] |= (0x01 << (bCnt-32));
							else if(bCnt < 48)
								gbSlotBit[7] |= (0x01 << (bCnt-40));
							else if(bCnt < 56)
								gbSlotBit[6] |= (0x01 << (bCnt-48));
							else if(bCnt < 64)
								gbSlotBit[5] |= (0x01 << (bCnt-56));
							else if(bCnt < 72)
								gbSlotBit[4] |= (0x01 << (bCnt-64));
							else if(bCnt < 80)
								gbSlotBit[3] |= (0x01 << (bCnt-72));
							else if(bCnt < 88)
								gbSlotBit[2] |= (0x01 << (bCnt-80));
							else if(bCnt < 96)
								gbSlotBit[1] |= (0x01 << (bCnt-88));
							else 
								gbSlotBit[0] |= (0x01 << (bCnt-96));						
						}
						break;
#endif					
					default : break;
				}
			}
		}

#if (SUPPORTED_USERCARD_NUMBER == 100) || (_MAX_REG_BIO == 100) || (SUPPORTED_USERCODE_NUMBER == 100)		
		if(gbBLENTmpData[2] == 0x01)			//index 1이면 보낼수 잇는 슬롯 바이트 9 바이트 = 100개중 72
		{
			gbBLENTmpData[5] = 72; //(0x48)		//72개 슬롯 정보 (1번 ~72번 슬롯)
			memcpy(&gbBLENTmpData[6], &gbSlotBit[4], 9); // TX 패킷으로 복사 
			bResult = 15; //total length
		}
		else if(gbBLENTmpData[2] == 0x02)		//index 2이면 보낼수 잇는 슬롯 바이트 9 바이트 = 100개중 28
		{
			gbBLENTmpData[5] = 28; //(0x1C) 		//100개중 나머지 28개 (73번 ~ 100번 슬롯)				
			memcpy(&gbBLENTmpData[6], &gbSlotBit[0], 4); // TX 패킷으로 복사 
			bResult = 10; //total length
		}
		else
		{
			gbBLENTmpData[5] = 0x00;	//나머지 인덱스 0, 3이상은 정보가 없음
			bResult = 6; //total length
		}
#endif	
		return (bResult);
}



BYTE MakeCredentialOccupiedData(WORD wStartAddress, BYTE bNum)  
{
	WORD wAddress;
	WORD wResultData;

	// Slot Number 1의 상태가 전체 BYTE의 Bit 0에 저장
	// Slot Number 최상위의 상태가 BYTE의 Bit 최상위에 저장
	// Ex.) 전체 지원 수는 20, Slot 1,2,3,4,5,9,10,11이 occupied일 경우 0x00071F Data 3 byte 추가
	
	if((gbBLENTmpData[2] != 0) && (gbBLENTmpData[2] <= 8))
	{
		wAddress = wStartAddress;	
		//gbBLENTmpData[3] = CredentialOccupiedCheck(wAddress, bNum, gbBLENTmpData[2]); 

		if(gbManageMode==_AD_MODE_SET)
		{
			wAddress = wStartAddress;
			wResultData = CredentialOccupiedCheck(wAddress, bNum, gbBLENTmpData[2]);
			wResultData &= 0x000000FF;	
			gbBLENTmpData[3] = (BYTE)wResultData;
			
			// 4 byte Length
			return 4;
		}
		else
		{
			return 4;
		}
	}	
	else if(gbBLENTmpData[2] <= 16)
	{
		/*wAddress = wStartAddress;	
		gbBLENTmpData[4] = CredentialOccupiedCheck(wAddress, bNum, 8); 
		wAddress = wStartAddress+8;	
		gbBLENTmpData[3] = CredentialOccupiedCheck(wAddress, bNum, (gbBLENTmpData[2]-8)); */


		if(gbManageMode==_AD_MODE_SET)
		{
			wAddress = wStartAddress;
			wResultData = CredentialOccupiedCheck(wAddress, bNum, gbBLENTmpData[2]);
			wResultData &= 0x0000FFFF;	
			gbBLENTmpData[3] = (BYTE)(wResultData/0x100);
			wResultData &= 0x000000FF;	
			gbBLENTmpData[4] = (BYTE)wResultData;
		
			// 5 byte Length
			return 5;
		}
	}	
	else if(gbBLENTmpData[2] <= 24)
	{
		/*wAddress = wStartAddress;	
		gbBLENTmpData[5] = CredentialOccupiedCheck(wAddress, bNum, 8); 
		wAddress = wStartAddress+8;	
		gbBLENTmpData[4] = CredentialOccupiedCheck(wAddress, bNum, 8);	
		wAddress = wStartAddress+16;	
		gbBLENTmpData[3] = CredentialOccupiedCheck(wAddress, bNum, (gbBLENTmpData[2]-16));	*/

		
		//wResultData = CredentialOccupiedCheck(BIO_SN, 2, 20);

		
//		if(gbManageMode==_MODE_SET)
//		{
			wAddress = wStartAddress;
			wResultData = CredentialOccupiedCheck(wAddress, bNum, gbBLENTmpData[2]);
			wResultData &= 0x00FFFFFF;		
			gbBLENTmpData[3] =  (BYTE)(wResultData/0x10000);
			wResultData &= 0x0000FFFF;	
			gbBLENTmpData[4] = (BYTE)(wResultData/0x100);
			wResultData &= 0x000000FF;	
			gbBLENTmpData[5] = (BYTE)wResultData;
	
			// 6 byte Length
			return 6;
//		}

	}	
	else if(gbBLENTmpData[2] <= 32)
	{
		/*wAddress = wStartAddress;	
		gbBLENTmpData[6] = CredentialOccupiedCheck(wAddress, bNum, 8); 
		wAddress = wStartAddress+8;	
		gbBLENTmpData[5] = CredentialOccupiedCheck(wAddress, bNum, 8);	
		wAddress = wStartAddress+16;	
		gbBLENTmpData[4] = CredentialOccupiedCheck(wAddress, bNum, 8);	
		wAddress = wStartAddress+24;	
		gbBLENTmpData[3] = CredentialOccupiedCheck(wAddress, bNum, (gbBLENTmpData[2]-24));	*/
			
		if(gbManageMode==_AD_MODE_SET)
		{
			wAddress = wStartAddress;
			wResultData = CredentialOccupiedCheck(wAddress, bNum, gbBLENTmpData[2]);
			wResultData &= 0xFFFFFFFF;
			gbBLENTmpData[3] =  (BYTE)(wResultData/0x1000000);
			wResultData &= 0x00FFFFFF;	
			gbBLENTmpData[4] =  (BYTE)(wResultData/0x10000);
			wResultData &= 0x0000FFFF;	
			gbBLENTmpData[5] = (BYTE)(wResultData/0x100);
			wResultData &= 0x000000FF;				
			gbBLENTmpData[6] = (BYTE)wResultData;	
			// 7 byte Length
			return 7;
		}
	}	
	else if(gbBLENTmpData[2] <= 40)
	{
		/*wAddress = wStartAddress;	
		gbBLENTmpData[7] = CredentialOccupiedCheck(wAddress, bNum, 8); 
		wAddress = wStartAddress+8;	
		gbBLENTmpData[6] = CredentialOccupiedCheck(wAddress, bNum, 8);	
		wAddress = wStartAddress+16;	
		gbBLENTmpData[5] = CredentialOccupiedCheck(wAddress, bNum, 8);	
		wAddress = wStartAddress+24;	
		gbBLENTmpData[4] = CredentialOccupiedCheck(wAddress, bNum, 8);	
		wAddress = wStartAddress+32;	
		gbBLENTmpData[3] = CredentialOccupiedCheck(wAddress, bNum, (gbBLENTmpData[2]-32));	*/

		wAddress = wStartAddress;
		gbBLENTmpData[3] = 0x00;
		gbBLENTmpData[4] = 0x00;
		gbBLENTmpData[5] = 0x00;
		gbBLENTmpData[6] = 0x00;
		gbBLENTmpData[7] = 0x00;
		wResultData = CredentialOccupiedCheck(wAddress, bNum, gbBLENTmpData[2]);
#if 0
		wResultData &= 0xFFFFFFFFFF;
		gbBLENTmpData[3] = (BYTE)(wResultData/0x100000000);
		wResultData &= 0x00FFFFFFFF;
		gbBLENTmpData[4] = (BYTE)(wResultData/0x1000000);
		wResultData &= 0x0000FFFFFF;
		gbBLENTmpData[5] = (BYTE)(wResultData/0x10000);
		wResultData &= 0x000000FFFF;
		gbBLENTmpData[6] = (BYTE)(wResultData/0x100);
		wResultData &= 0x00000000FF;
		gbBLENTmpData[7] = (BYTE)wResultData;
#endif 
		// 8 byte Length
		return 8;
	}	
	else if(gbBLENTmpData[2] <= 100)
	{
		wAddress = wStartAddress;
		gbBLENTmpData[3] = 0x00;
		gbBLENTmpData[4] = 0x00;
		gbBLENTmpData[5] = 0x00;
		gbBLENTmpData[6] = 0x00;
		gbBLENTmpData[7] = 0x00;
		gbBLENTmpData[8] = 0x00;
		gbBLENTmpData[9] = 0x00;
		gbBLENTmpData[10] = 0x00;
		gbBLENTmpData[11] = 0x00;
		gbBLENTmpData[12] = 0x00;
		gbBLENTmpData[13] = 0x00;
		gbBLENTmpData[14] = 0x00;	
		gbBLENTmpData[15] = 0x00;

		wResultData = CredentialOccupiedCheck100Support(wAddress, bNum, gbBLENTmpData[2]);		
		return wResultData;  //index 1이면 length : 15, index 2 이면 10
	}	

	return 0;
}
	



BYTE MakeGetRegisteredCredential(BYTE bType)
{
	BYTE bTmp;
	
	gbBLENTmpData[0] = SUBEV_GET_REGISTERD_CREDENTIAL;
	gbBLENTmpData[1] = bType;

	gbBLENTmpData[2] = 0;
	if(bType == CREDENTIALTYPE_PINCODE)
	{
		// Advanced Mode일 경우에는 User Code 지원 수는 30
		// Normal Mode일 경우에는 User Code 지원 수는 2 (User Code+Visitor Code)
		// 지원 수에서 Master Code 및 One Time Code는 제외
		if(gbManageMode == _AD_MODE_SET)
		{
			gbBLENTmpData[2] = SUPPORTED_USERCODE_NUMBER;
		}
		else
		{
			gbBLENTmpData[2] = 2;
		}

		// PINCODE는 8byte 간격 단위로 정보가 저장되어 있음. 
		bTmp = MakeCredentialOccupiedData(USER_CODE, 8);	
		return (bTmp);
	}
#ifdef	DDL_CFG_RFID
	else if(bType == CREDENTIALTYPE_CARD)
	{
		gbBLENTmpData[2] = SUPPORTED_USERCARD_NUMBER;

		// CARD는 4byte 간격 단위로 정보가 저장되어 있음. 
		bTmp = MakeCredentialOccupiedData(CARD_UID, MAX_CARD_UID_SIZE); 
		return (bTmp);
	}
#endif
#ifdef	DDL_CFG_FP  //hyojoon_20160831 PFM-3000 add
	else if(bType == CREDENTIALTYPE_FINGERPRINT)
	{
		gbBLENTmpData[2] = SUPPORTED_FINGERPRINT_NUMBER;
		// FINGERPRINT는 2byte 간격 단위로 정보가 저장되어 있음. 
		bTmp = MakeCredentialOccupiedData(BIO_ID, 2); 
		return (bTmp);
	}
#endif
#if defined	(DDL_CFG_DS1972_IBUTTON) 
	else if(bType == CREDENTIALTYPE_DS1972TOUCHKEY)
	{
		gbBLENTmpData[2] = SUPPORTED_DS1972KEY_NUMBER;
		// DS1972는 8byte 간격 단위로 정보가 저장되어 있음. 
		bTmp = MakeCredentialOccupiedData(TOUCHKEY_UID,MAX_KEY_UID_SIZE); 
		return (bTmp);
	}
#endif
#if defined	(DDL_CFG_IBUTTON) 
	else if(bType == CREDENTIALTYPE_GMTOUCHKEY)
	{
		gbBLENTmpData[2] = SUPPORTED_USERKEY_NUMBER;
		// Gateman TouchKey는 8byte 간격 단위로 정보가 저장되어 있음. 
		bTmp = MakeCredentialOccupiedData(TOUCHKEY_UID,MAX_KEY_UID_SIZE); 
		return (bTmp);
	}
#endif

	// 추가 Data가 없을 경우 3byte 회신
	return 3;
}	




BYTE VolumeSettingCheckForBLEN(void)
{
	BYTE bTmp;
	BYTE RetVal = 0;
	
	bTmp = VolumeSettingCheck();
#if	defined (DDL_CFG_BUZ_VOL_TYPE1)
	// 0x11 : Silent mode but soft setting possible
	// 0x12 : Low volume but soft setting possible
	// 0x13 : High volume but soft setting possible
	if(bTmp == VOL_SILENT)
	{
		// 무음 모드	
		RetVal = 0x11;
	}
	else if(bTmp == VOL_HIGH)
	{
		// 고음 모드
		RetVal = 0x13;
	}
	else
	{
		// 중간음 모드
		RetVal = 0x12;
	}
#elif defined (DDL_CFG_BUZ_VOL_TYPE2)
	// 0x01 : Silent mode ON but no soft setting possible
	// 0x02 : Low volume but no soft setting possible
	// 0x03 : High volume but no soft setting possible
	if(bTmp == VOL_SILENT)
	{
		// 무음 모드	
		RetVal = 0x01;
	}
	else if(bTmp == VOL_HIGH)
	{
		// 고음 모드
		RetVal = 0x03;
	}
	else
	{
		// 중간음 모드
		RetVal = 0x02;
	}
#else
	// 0xFF : Volume not support
	RetVal = 0xFF;
#endif

	return (RetVal);
}


BYTE GetNumberOfOccupiedUserCode(void)
{
	BYTE bCnt;
	BYTE bTmp;
	BYTE bNum = 0;
	
	if(gbManageMode == _AD_MODE_SET)
	{
		for(bCnt = 0; bCnt < SUPPORTED_USERCODE_NUMBER; bCnt++)
		{
			RomRead(&bTmp, USER_CODE+((WORD)bCnt*8), 1);

#if defined (_USE_IREVO_CRYPTO_)
			EncryptDecryptKey(&bTmp,1);
#endif 	

			if(bTmp != 0xFF)
			{
				bNum++;	
			}
		}
	}
	else
	{
		for(bCnt = 0; bCnt < 2; bCnt++)
		{
			RomRead(&bTmp, USER_CODE+((WORD)bCnt*8), 1);
#if defined (_USE_IREVO_CRYPTO_)
			EncryptDecryptKey(&bTmp,1);
#endif 	
			if(bTmp != 0xFF)
			{
				bNum++;	
			}
		}
	}	

	return (bNum);
}	

BYTE GetNumberOfOccupiedCard(void)
{
#ifdef	DDL_CFG_RFID
	BYTE bCnt;
	BYTE bNum = 0;
	BYTE bBuf[8];

	for(bCnt = 0; bCnt < SUPPORTED_USERCARD_NUMBER; bCnt++)
	{
		RomRead(bBuf, CARD_UID+((WORD)bCnt*MAX_CARD_UID_SIZE), 8);
#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(bBuf,8);
#endif 	
		if(DataCompare(bBuf, 0xFF, 8) != 0)
			bNum++;
	}
	return bNum;
#else
	return 0;
#endif
}

BYTE GetNumberOfOccupiedFingerprint(void)
{
#ifdef	DDL_CFG_FP  //hyojoon_20160831 PFM-3000 add
	BYTE bCnt;
	BYTE bTmp;
	BYTE bNum = 0;

	for(bCnt = 0; bCnt < SUPPORTED_FINGERPRINT_NUMBER; bCnt++)
	{
		RomRead(&bTmp, BIO_ID+((WORD)bCnt*2), 1);
		if(bTmp != 0xFF)
		{
			bNum++;
		}
	}
	return (bNum);
#else
	return 0;
#endif
}

/* hyojoon dummy 함수 나중에 쓰면 채워야 함 */ 
BYTE GetNumberOfOccupiedFace(void)
{
	return (0);
}

BYTE GetNumberOfOccupiedIButton(void)
{
#if defined	(DDL_CFG_DS1972_IBUTTON)
	BYTE Temp = 0x00;
	Temp = GetNoDS1972TouchKeyFromEeprom();
	return Temp;
#elif	defined (DDL_CFG_IBUTTON)
	BYTE bCnt;
	BYTE bNum = 0;
	BYTE bBuf[8];
	
	for(bCnt = 0; bCnt < SUPPORTED_USERKEY_NUMBER; bCnt++)
	{
		RomRead(bBuf, TOUCHKEY_UID+((WORD)bCnt*MAX_KEY_UID_SIZE), 8);
		if(DataCompare(bBuf, 0xFF, 8) != 0)
			bNum++;
	}
	return bNum;
#else 
	return (0);
#endif 
}

BYTE GetNumberOfOccupiedIris(void)
{
	return (0);
}

/* hyojoon 일단 function pointer 로 ... .유연 하게 */
/* protocol 의 순서에  유의 할것 */
BYTE (*const NumberOfOccupied_EachCredentialTbl[MAX_CREDENTIAL_TYPE])(void) = {
	GetNumberOfOccupiedUserCode,
	GetNumberOfOccupiedCard,
	GetNumberOfOccupiedFingerprint,
	GetNumberOfOccupiedIButton,
	GetNumberOfOccupiedFace,	
	GetNumberOfOccupiedIris
};

BYTE MakeGetSetting(void)
{
	BYTE bTmp;
	BYTE bCnt = 0;
	BYTE CredencialType_Index = 0;
		
	/* Sub event type */
	gbBLENTmpData[bCnt++] = SUBEV_GET_SETTING;

	// 1. Settings (1 byte)
	bTmp = 0;
	// Bit 0 : Operation Mode
	//	0 : Normal	1: Advance
	if(gbManageMode == _AD_MODE_SET)
	{
		bTmp |= 0x01;
	}	
	
	// Bit 1 : Left/Right Handle
	//	0 : Left	1: Right
	//bTmp |= 0x02;

	// Bit 2 : Auto Close
	//	0 : Off	1: On
	if(AutoLockSetCheck())
	{
		bTmp |= 0x04;
	}	
	// Bit 3 : Left/Right Handle support
	//bTmp |= 0x08;
	//	0 : NO	1: YES

	// Bit 4 : Left/Right soft mode (Software setting possible)
	//bTmp |= 0x10;
	//	0 : NO	1: YES

	// Bit 5 : Auto relock support
	bTmp |= 0x20;
	//	0 : NO	1: YES

	// Bit 4 : Auto relock  soft mode (Software setting possible)
	//	0 : NO	1: YES
#ifndef P_SW_AUTO_T
	bTmp |= 0x40;
#endif

	gbBLENTmpData[bCnt++] = bTmp;
	
	// 2. Volume Level (1 byte)
	// 0x01 : Silent mode ON
	// 0x02 : Low volume
	// 0x03 : High volume
	// 0x11 : Silent mode but no soft setting possible
	// 0x12 : Low volume but no soft setting possible
	// 0x13 : High volume but no soft setting possible
	// 0xFF : Volume not support

	gbBLENTmpData[bCnt++] = VolumeSettingCheckForBLEN();

	// 3. Credential Support Type (1 byte)
	bTmp = 0;
	// Bit 0 : PinCode
	// Bit 1 : Card
	// Bit 2 : Finger
	// Bit 3 : Face
	// Bit 4 : I-Button
	// Bit 5 : Iris

#if defined (DDL_CFG_RFID) && defined(DDL_CFG_FP) //hyojoon_20160831 PFM-3000 add
	bTmp = 0x01 |0x02 |0x04;
#elif defined (DDL_CFG_RFID)
	bTmp = 0x01 | 0x02;
#elif defined (DDL_CFG_FP) //hyojoon_20160831 PFM-3000 add
	bTmp = 0x01 | 0x04;
#elif defined (DDL_CFG_IBUTTON) ||defined (DDL_CFG_DS1972_IBUTTON)
	bTmp = 0x01 |0x08;
#else
	bTmp = 0x01;
#endif
/*
	switch (gbtypevalue)
	{
		case  DDL_SET_RF :  // PIN + Card 
			bTmp = 0x01 | 0x02;
		break;

		case  DDL_SET_BIO :  // PIN + Finger
			bTmp = 0x01 | 0x04;
		break;

		case  DDL_SET_RF_BIO :  // PIN + Card + Finger
			bTmp = 0x01 |0x02 |0x04;
		break;

		default :
			bTmp = 0x01;
		break;
	}
*/
	CredencialType_Index = bCnt++; // 문서 변하지 않는 한 3 일턴데.... 밑에 비교 하는 구문이 있으니 보기 좋게 
	gbBLENTmpData[CredencialType_Index] = bTmp;
	
	//Language Setting (3 bytes)
	// 0x01 : Korean		-> Bit 0
	// 0x02 : English		-> Bit 1
	// 0x03 : Chinese		-> Bit 2
	// 0x04 : Portuguese	-> Bit 3
	// 0x05 : Spanish		-> Bit 4
	// 0x06 : Taiwanese		-> Bit 5
	// 0x07 : Turkish		-> Bit 6
	// 0x08 : Russian		-> Bit 7
	// 0x09 : Frech		-> Bit 8
	// 0x0A : Japanese		-> Bit 9	
	// 0x0B : Hindi		-> Bit 10
	// 0x0FF : No Voice	

	// 4. Current Language setting (1 byte)
#if	defined (LOCKSET_LANGUAGE_SET)

	bTmp = 0;
	LanguageLoad();
	switch (LanguageCheck())
	{
		case _KOREAN_MODE:
			bTmp = 0x01;
			break;
			
		case _SPANISH_MODE:
			bTmp = 0x05;
			break;

		case _PORTUGUESE_MODE:
			bTmp = 0x04;
			break;

		case _ENGLISH_MODE:
			bTmp = 0x02;
			break;

		case _CHINESE_MODE:
			bTmp = 0x03;
			break;

		case _TAIWANESE_MODE:
			bTmp = 0x06;
			break;

		default :
			bTmp = 0x03;
			break;

	}
	gbBLENTmpData[bCnt++] = bTmp;    	/* Current Language */ 
	gbBLENTmpData[bCnt++] = 0;          /*  respected languages */
#if	defined (DDL_CFG_LANGUAGE_SET_TYPE1) || defined (DDL_CFG_LANGUAGE_SET_TYPE3)
	gbBLENTmpData[bCnt++] = 0x01 | 0x02 |0x04 |0x20 ; /*  respected languages */	
#elif defined DDL_CFG_LANGUAGE_SET_TYPE2
	gbBLENTmpData[bCnt++] = 0x01 | 0x02 | 0x04 | 0x08 |0x10 |0x20; /*  respected languages */
#endif 

#elif	defined (P_VOICE_RST) 
	// 1개의 언어만 지원하고 언어 설정은 지원하지 않는 경우
	bTmp = 0;
	LanguageLoad();
	switch (LanguageCheck())
	{
		case _SPANISH_MODE:
			bTmp = 0x05;
			break;

		case _PORTUGUESE_MODE:
			bTmp = 0x04;
			break;

		case _ENGLISH_MODE:
			bTmp = 0x02;
			break;

		case _CHINESE_MODE:
			bTmp = 0x03;
			break;

		case _KOREAN_MODE:
			bTmp = 0x01;
			break;

		default :
			bTmp = 0x01;
			break;

	}
	gbBLENTmpData[bCnt++] = bTmp;		/* Current Language */ 
	gbBLENTmpData[bCnt++] = 0;			/*	respected languages */
	gbBLENTmpData[bCnt++] = (1 << (bTmp-1)); /*  respected languages */

#else 
	// No Voice
	gbBLENTmpData[bCnt++] = 0xFF;	 	/* Current Language */ 
	gbBLENTmpData[bCnt++] = 0xFF;		/*	respected languages */
	gbBLENTmpData[bCnt++] = 0xFF; 		/*  respected languages */

#endif

	// 5. ~ 6 Min Max code length 

#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
	gbBLENTmpData[bCnt++] = gPinInputMinLength; // Min Pincode 

#else

#ifdef	P_VOICE_RST // defined (DDL_CFG_SUPPORT_CHINESE)
	gbBLENTmpData[bCnt++] = gPinInputMinLength; // Min Pincode 
#else 	
	gbBLENTmpData[bCnt++] = MIN_PIN_LENGTH;	// Min Pincode 
#endif 	

#endif

	gbBLENTmpData[bCnt++] = MAX_PIN_LENGTH;	// Max Pincode 	

	//hyojoon.kim Credential Type Count 순서에 맞게 차곡 차곡 
	for(bTmp = 0; bTmp < MAX_CREDENTIAL_TYPE ; bTmp++)
	{
		if(gbBLENTmpData[CredencialType_Index] & (0x01 << bTmp)) // Credential Support Type 을 check 
		{
			gbBLENTmpData[bCnt++] = NumberOfOccupied_EachCredentialTbl[bTmp]();
		}
	}

	return bCnt;
}	

#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
BYTE CheckOccupiedData(BYTE bType, BYTE bSlotNo)
{	
	BYTE bResult;
	switch(bType)
	{
		case CREDENTIALTYPE_PINCODE:
			bResult = CheckOccupiedUserCode(bSlotNo);
			break;
				
#ifdef	DDL_CFG_RFID
		case CREDENTIALTYPE_CARD:			
			bResult = CheckOccupiedCard(bSlotNo);
			break;
#endif

#ifdef	DDL_CFG_FP
		case CREDENTIALTYPE_FINGERPRINT:
			bResult = CheckOccupiedFingerprint(bSlotNo);
			break;
#endif		

#if defined (DDL_CFG_DS1972_IBUTTON) |defined(DDL_CFG_IBUTTON)
		case CREDENTIALTYPE_GMTOUCHKEY: 
		case CREDENTIALTYPE_DS1972TOUCHKEY: 
			bResult = CheckOccupiedIButton(bSlotNo);
			break;  
#endif
		default :
			bResult = 0;
			break;
			
	}
	return bResult;		
}

BYTE CheckOccupiedUserCode(BYTE bSlotNo)
{	
	BYTE bTmp;
	bSlotNo-=1;	
	RomRead(&bTmp, USER_CODE+((WORD)bSlotNo*8), 1);
#if defined (_USE_IREVO_CRYPTO_)
	EncryptDecryptKey(&bTmp,1);
#endif 	
	if(bTmp != 0xFF)return 1;
	return 0;
}

BYTE CheckOccupiedCard(BYTE bSlotNo)
{
#ifdef	DDL_CFG_RFID
	BYTE bBuf[8];
	
	bSlotNo-=1;	
	RomRead(bBuf, CARD_UID+((WORD)bSlotNo*MAX_CARD_UID_SIZE), 8);
#if defined (_USE_IREVO_CRYPTO_)
	EncryptDecryptKey(bBuf,8);
#endif 	
	if(DataCompare(bBuf, 0xFF, 8) != 0)return 1;
	else return 0;
#else
	return 0;
#endif
}

BYTE CheckOccupiedFingerprint(BYTE bSlotNo)
{
#ifdef	DDL_CFG_FP
	BYTE bTmp;
	
	bSlotNo-=1;	
	RomRead(&bTmp, BIO_ID+((WORD)bSlotNo*2), 1);
	if(bTmp != 0xFF)return 1;
	else return 0;
#else
	return 0;
#endif
}

/* hyojoon dummy 함수 나중에 쓰면 채워야 함 */ 
BYTE CheckOccupiedFace(BYTE bSlotNo)
{
	bSlotNo-=1;	
	return (0);
}

BYTE CheckOccupiedIButton(BYTE bSlotNo)
{
#if defined	(DDL_CFG_DS1972_IBUTTON)
	BYTE Temp = 0x00;
	bSlotNo-=1; 

	RomRead(&Temp, (WORD)KEY_NUM+(WORD)bSlotNo, 1);					
	if(Temp != 0xFF)return 1;
	else return 0
		
	
	return 0;
#elif	defined (DDL_CFG_IBUTTON)
	BYTE bBuf[8];

	bSlotNo -= 1;
	
	RomRead(bBuf, TOUCHKEY_UID+((WORD)bSlotNo*MAX_KEY_UID_SIZE), 8);
	if(DataCompare(bBuf, 0xFF, 8) != 0)return 1;
	else return 0;
#else 
	return 0;
#endif 
}

BYTE CheckOccupiedIris(BYTE bSlotNo)
{
	bSlotNo-=1;	
	return (0);
}


BYTE MakeSlotDataWithSchdule(BYTE bType, BYTE bCredentialNumber, BYTE bReqIndex)
{
	BYTE bSlotData[10] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

	BYTE bCnt = 0;				//루프 카운트
	BYTE bByteIndex = 0;		//슬롯 데이터 배열 인덱스, 뒤에서 부터 채움 
	BYTE bUserStatus, bScheduleStatus, bCredentialOpccupied = 0x00;	//슬롯 정보 확인을 위한 변수 	
	BYTE bUserID[2]; 	// bUserID[0] = Type : 0x00(Pin), 0x10(Card), 0x20(FP), bUserID[1] = SlotNo : 0x01~0xFF
	BYTE bLenghth;		// 슬롯 데이터 길이(=TX 슬롯 데이터 사이즈)
	BYTE bLoopMaxCnt;	// 확인할 슬롯 데이터 카운트  
	BYTE bAvailableIndex = 0; //슬롯 갯수 연산,각 인덱스의 슬롯 개수 연산, 1인덱스 최대 정보 MAX 20slot     
	BYTE bSlotCntOfEndIndex = bCredentialNumber%20;	//20개가 되지 않는 slot정보

	bAvailableIndex = bCredentialNumber / 20;		//몇개의 인덱스가 필요한지 연산 ex)100개 등록 지원일 경우 인덱스 5개
	if(bSlotCntOfEndIndex != 0)bAvailableIndex += 1;	//몇개의 인덱스가 필요한지 연산 ex)30개 등록 지원일 경우 인덱스 2개

	if(bReqIndex > bAvailableIndex) //허용 인덱스 이상을 요청 하면... ex)인덱스가 총1~5번(100개 지원)인데 6번 인덱스를 요구하면..
	{
		gbBLENTmpData[3] = 0;	//StartSlotNo.
		gbBLENTmpData[4] = 0;	//EndSlotNo.
		return 0; //Slot Data 없음
	}
	else if((bReqIndex == bAvailableIndex) && (bSlotCntOfEndIndex != 0)) //슬롯을 몇개 만들지.. 연산
	{
		bLoopMaxCnt = bSlotCntOfEndIndex;		//마지막 인덱스는 몇개 슬롯? 
		bLenghth = bLoopMaxCnt/2;
		if((bLoopMaxCnt % 2)!= 0)bLenghth += 1;
	}
	else //if(bReqIndex < bAavailableIndex)	
	{										
		bLoopMaxCnt = 20;	//MaxSlotData	
		bLenghth = bLoopMaxCnt/2;
	}
	
	bUserID[0] = (bType<<4)&0xF0;	//USER ID[0], type의 Type은 00 10 20 30, Credential Type은 00 01 02 03 이라 맞춰줌 
	bUserID[1] = 1+(20*(bReqIndex-1)); //USER ID[1], SlotNo는 1번 부터 시작, index1 : 1~20, index2 : 21~40, index3 : 41~60 ..... 
	gbBLENTmpData[3] = bUserID[1];	//Start Slot

	bByteIndex = (bLoopMaxCnt / 2) - 1;	//배열 10개 [0]~[9] 중에 뒤에서 부터 채울 것이므로 
	if((bLoopMaxCnt % 2) != 0)bByteIndex += 1;
	
	for(bCnt = 0; bCnt < bLoopMaxCnt; bCnt++)
	{	
		bUserStatus = GetUserStatus((WORD)bUserID[1]);
		bScheduleStatus = GetUserSchduleStatus(bUserID);
		bCredentialOpccupied = CheckOccupiedData(bType, bUserID[1]);
		
		if((bUserID[1]%2)==1)	//Low 4-bit slot data 1, 3, 5, 7, 9 .....
		{		
			if(bUserStatus == USER_STATUS_OCC_ENABLED)bSlotData[bByteIndex]|= 0x08; // 3bit			
			if((bScheduleStatus & 0x40)==0x40)bSlotData[bByteIndex]|= 0x04;			// 2bit
			if((bScheduleStatus & 0xA0)==0xA0)bSlotData[bByteIndex]|= 0x02;			// 1bit
			if(bCredentialOpccupied)bSlotData[bByteIndex]|= 0x01;					// 0bit
		}
		else						//High 4-bit slot data 2, 4, 6, 8, 10 .....
		{		
			if(bUserStatus == USER_STATUS_OCC_ENABLED)bSlotData[bByteIndex]|= 0x80;	// 7bit				
			if((bScheduleStatus & 0x40)==0x40)bSlotData[bByteIndex]|= 0x40;			// 6bit
			if((bScheduleStatus & 0xA0)==0xA0)bSlotData[bByteIndex]|= 0x20;			// 5bit
			if(bCredentialOpccupied)bSlotData[bByteIndex]|= 0x10;					// 4bit
			bByteIndex--;
		}		
		gbBLENTmpData[4] = bUserID[1]; //EndSlot
		bUserID[1]++; 
	}
		
	memcpy(&gbBLENTmpData[5], &bSlotData[0] ,bLenghth);
	return bLenghth;
	
}

BYTE MakeCredentialSlotWithSchedule(BYTE bType, BYTE bIndex)
{	
	BYTE bTmp;
	gbBLENTmpData[0] = SUBEV_GET_CREDENTIAL_SLOT_WITH_SCHEDULE;

	switch(bType)
	{
		case CREDENTIALTYPE_PINCODE:
			if(GetCurrentOperationMode()==_AD_MODE_SET)
			{
				gbBLENTmpData[1] = bType;
				gbBLENTmpData[2] = SUPPORTED_USERCODE_NUMBER;
				bTmp = MakeSlotDataWithSchdule(bType,gbBLENTmpData[2], bIndex);
			}
			else
			{
				gbBLENTmpData[1] = bType;
				gbBLENTmpData[2] = 2;
				bTmp = MakeSlotDataWithSchdule(bType,gbBLENTmpData[2], bIndex);
			}			
			break;
			
#ifdef	DDL_CFG_RFID
		case CREDENTIALTYPE_CARD:
			gbBLENTmpData[1] = bType;
			gbBLENTmpData[2] = SUPPORTED_USERCARD_NUMBER;
			bTmp = MakeSlotDataWithSchdule(bType,gbBLENTmpData[2], bIndex);

			break;
#endif

#ifdef	DDL_CFG_FP
		case CREDENTIALTYPE_FINGERPRINT:
			gbBLENTmpData[1] = bType;
			gbBLENTmpData[2] = SUPPORTED_FINGERPRINT_NUMBER;
			bTmp = MakeSlotDataWithSchdule(bType,gbBLENTmpData[2], bIndex);

			break;
#endif		

#if defined (DDL_CFG_DS1972_IBUTTON) |defined(DDL_CFG_IBUTTON)
		case CREDENTIALTYPE_GMTOUCHKEY: 
			gbBLENTmpData[1] = bType;
			gbBLENTmpData[2] = SUPPORTED_USERKEY_NUMBER
			break;

		case CREDENTIALTYPE_DS1972TOUCHKEY: 
			gbBLENTmpData[1] = bType;
			gbBLENTmpData[2] = SUPPORTED_DS1972KEY_NUMBER;
			break;
#endif				

		default : 
			break;

	}
	return bTmp + 5 ;
}

BYTE MakeGetSettingSubEvent(void)
{
	BYTE bTmp = 0x00;
	BYTE bCnt = 0;
	BYTE bTmpSettingData[3] = {0x00, 0x00, 0x00};

//BIT 0[2]  : D2_ADVANCED_MODE_ENABLE
	if(GetCurrentOperationMode()==_AD_MODE_SET)
	{
		bTmpSettingData[2] |= D2_ADVANCED_MODE_ENABLE;
	}

//BIT 1[2]  : D2_ONETIME_PINCODE_SUPPORT
//BIT 2[2]  : D2_ONETIME_PINCODE_SET
	bTmpSettingData[2] |= D2_ONETIME_PINCODE_SUPPORT;
	if(CheckPinCodeSet(ONETIME_CODE))bTmpSettingData[2] |= D2_ONETIME_PINCODE_SET;

//BIT 3[2]	 : D2_VISITOR_PINCODE_SUPPORT
//BIT 4[2]  : D2_VISITOR_PINCODE_SET
	if(GetCurrentOperationMode() !=_AD_MODE_SET)
	{
		bTmpSettingData[2] |= D2_VISITOR_PINCODE_SUPPORT;
		if(CheckPinCodeSet(USER_CODE+8))bTmpSettingData[2] |= D2_VISITOR_PINCODE_SET;
	}

//BIT 5[2]  : D2_AUTO_RELOCK_SUPPORT
//BIT 6[2]  : D2_AUTO_RELOCK_ENABLE
//BIT 7[2]  : D2_AUTO_RELOCK_SOFT_MODE
//BIT 8[1]  : D2_AUTO_RELOCK_TIME_SET_SUPPORT
	bTmpSettingData[2] |= D2_AUTO_RELOCK_SUPPORT;
	if(AutoLockSetCheck())bTmpSettingData[2] |= D2_AUTO_RELOCK_ENABLE;
#ifndef P_SW_AUTO_T
	bTmpSettingData[2] |= D2_AUTO_RELOCK_SOFT_MODE;
#endif
	//bTmpSettingData[1] |= D2_AUTO_RELOCK_TIME_SET_SUPPORT; //SmartLeverLock만 지원 

//BIT 9[1]  : D2_DOUBLE_CHECK_LOCK_SUPPORT
//BIT 10[1] : D2_DOUBLE_CHECK_LOCK_ENABLE
//BIT 11[1] : D2_DOUBLE_CHECK_LOCK_SOFTMODE
#if defined(DDL_CFG_TOUCH_OC)
	bTmpSettingData[1] |= D2_DOUBLE_CHECK_LOCK_SUPPORT;
	if(gfSafeOCEn == 0xBB)bTmpSettingData[2] |= D2_DOUBLE_CHECK_LOCK_ENABLE;
	bTmpSettingData[1] |= D2_DOUBLE_CHECK_LOCK_SOFTMODE;
#endif

//BIT 12[1] : D2_EMERGENCY_UNLOCK_SUPPORT
//BIT 13[1] : D2_EMERGENCY_UNLOCK_ENABLE
//BIT 14[1] : D2_EMERGENCY_UNLOCK_SOFT_MODE
	//SmartLeverLock(GRS-LA150) Front Reset하여 Lock Open하는 경우 
#if defined(DDL_CFG_EMERGENCY_UNLOCK)
	bTmpSettingData[1] |= D2_EMERGENCY_UNLOCK_SUPPORT;	
	bTmpSettingData[1] |= D2_EMERGENCY_UNLOCK_ENABLE;
	//bTmpSettingData[2] |= D2_EMERGENCY_UNLOCK_SOFT_MODE;
#endif

//BIT 15[1] : TBD 1
//BIT 16[0] : TBD 2
//BIT 17[0] : TBD 3
//BIT 18[0] : TBD 4
//BIT 19[0] : TBD 5
//BIT 20[0] : TBD 6
//BIT 21[0] : TBD 7
//BIT 22[0] : TBD 8
//BIT 23[0] : TBD 9

	gbBLENTmpData[0] = SUBEV_GET_SETTING_SUB_EVENT;

	gbBLENTmpData[1] = bTmpSettingData[0];
	gbBLENTmpData[2] = bTmpSettingData[1];
	gbBLENTmpData[3] = bTmpSettingData[2];

	gbBLENTmpData[4] = VolumeSettingCheckForBLEN();

//CredentialSupportType
	bTmp |= D2_TYPE_PINCODE;

#if defined(DDL_CFG_RFID)
	bTmp |= D2_TYPE_CARD ;
#endif

#if defined(DDL_CFG_FP)
	bTmp |= D2_TYPE_FINGER;
#endif

#if defined(DDL_CFG_IBUTTON)||defined(DDL_CFG_DS1972_IBUTTON)
	bTmp |= D2_TYPE_IBUTTON;
#endif

/*
#if defined(DDL_CFG_FACE)	//지원 모델 없음
	bTmp |= D2_TYPE_FACE
#endif

#if defined(DDL_CFG_IRIS)	//지원 모델 없음
	bTmp |= D2_TYPE_IRIS
#endif
*/
	gbBLENTmpData[5] = bTmp;
	gbBLENTmpData[6] = GetCurrentLanguageSet();
	if(gbBLENTmpData[6] == LANGUAGE_CHINESE)gbBLENTmpData[7] = 6;
	else gbBLENTmpData[7] = MIN_PIN_LENGTH;
	gbBLENTmpData[8] = MAX_PIN_LENGTH;

	if((bTmp & D2_TYPE_PINCODE) == D2_TYPE_PINCODE)
	{
		if(gbManageMode == _AD_MODE_SET)gbBLENTmpData[9] = SUPPORTED_USERCODE_NUMBER;
		else gbBLENTmpData[9] = 2;		
	}
	if((bTmp & D2_TYPE_CARD) == D2_TYPE_CARD)
	{
		bCnt++;	
		gbBLENTmpData[9+bCnt] = SUPPORTED_USERCARD_NUMBER;		
	}
	if((bTmp & D2_TYPE_FINGER) == D2_TYPE_FINGER)
	{
		bCnt++;
		gbBLENTmpData[9+bCnt] = SUPPORTED_FINGERPRINT_NUMBER;		
	}
	if((bTmp & D2_TYPE_IBUTTON) == D2_TYPE_IBUTTON)
	{
		bCnt++;
		gbBLENTmpData[9+bCnt] = SUPPORTED_DS1972KEY_NUMBER;
	}
/*
	if((bTmp & D2_TYPE_FACE) == D2_TYPE_FACE)
	{
		bCnt++;
		gbBLENTmpData[9+bCnt] = SUPPORTED_FACE_NUMBER;

	}
	if((bTmp & D2_TYPE_IRIS) == D2_TYPE_IRIS)
	{
		bCnt++;
		gbBLENTmpData[9+bCnt] = SUPPORTED_IRIS_NUMBER;
	}
*/	
	return 10+bCnt;
}

BYTE CheckPinCodeSet(WORD wAddress) //wAddress : ONETIME_CODE or USER_CODE+8(VISITOR)
{
	BYTE bCnt;
	BYTE bTmpArray[8];

	RomRead(bTmpArray, wAddress, 8);
#if defined (_USE_IREVO_CRYPTO_)
	EncryptDecryptKey(bTmpArray, 8);
#endif
	for(bCnt = 0; bCnt < 8; bCnt++)
	{
		if(bTmpArray[bCnt] != 0xFF)return 1; //Set
	}
	return 0;	//Not Set
}

BYTE MakeGetSupportedLanguages(void)
{
	uint16_t tmp = 0;

	gbBLENTmpData[0] = SUBEV_GET_SUPPORTED_LANGUAGES;
	tmp = GetSupportedLanguages();
	gbBLENTmpData[1] = ((BYTE)(tmp >> 8) & 0xFF);
	gbBLENTmpData[2] = ((BYTE)(tmp & 0xFF));
	return 3; //Length
}

BYTE GetCurrentLanguageSet(void)
{
	WORD bTmp;	
	BYTE bResult;
	
	bTmp = LanguageCheck();
	switch(bTmp)
	{
		case _KOREAN_MODE:
			bResult = LANGUAGE_KOREAN;
			break;

		case _ENGLISH_MODE:
			bResult = LANGUAGE_ENGLISH;
			break;
		
		case _CHINESE_MODE:
			bResult = LANGUAGE_CHINESE;
			break;
		
		case _PORTUGUESE_MODE:
			bResult = LANGUAGE_PORTUGUESE;
			break;
		
		case _SPANISH_MODE:
			bResult = LANGUAGE_SPANISH;
			break;
	
		case _TAIWANESE_MODE:
			bResult = LANGUAGE_TAIWANESE;
			break;
		
		case _TURKISH_MODE:
			bResult = LANGUAGE_TURKISH;
			break;
		
		case _RUSSIAN_MODE:
			bResult = LANGUAGE_RUSSIAN;
			break;

		case _FRENCH_MODE:
			bResult = LANGUAGE_FRANCE;
			break;
	
		case _JAPANESE_MODE:
			bResult = LANGUAGE_JAPANESE;
			break;
		
		case _HINDI_MODE:
			bResult = LANGUAGE_HINDI;
			break;
		
		default :
			bResult = 0xFF;
			break;
	}

	return bResult;
}

uint16_t GetSupportedLanguages(void)
{
//D3_SUPPORT_KOREAN | D3_SUPPORT_ENGLISH | D3_SUPPORT_CHINESE | D3_SUPPORT_PORTUGUESE | D3_SUPPORT_SPANISH | D3_SUPPORT_TAIWANESE | D3_SUPPORT_TURKISH| D3_SUPPORT_RUSSIAN| D3_SUPPORT_FRANCE | D3_SUPPORT_JAPANESE | D3_SUPPORT_HINDI;
#if	defined (DDL_CFG_LANGUAGE_SET_TYPE1) || defined (DDL_CFG_LANGUAGE_SET_TYPE3)
	return (D3_SUPPORT_KOREAN | D3_SUPPORT_ENGLISH | D3_SUPPORT_CHINESE | D3_SUPPORT_TAIWANESE);
#elif defined(DDL_CFG_LANGUAGE_SET_TYPE2)
	return (D3_SUPPORT_KOREAN | D3_SUPPORT_ENGLISH | D3_SUPPORT_CHINESE | D3_SUPPORT_PORTUGUESE | D3_SUPPORT_SPANISH | D3_SUPPORT_TAIWANESE);
#else
	return (0x0000);
#endif
}
#endif


void BLENTxChangeCredentialRegistrationMode(BYTE bType , BYTE bSlotNumber)
{
	if(gbManageMode == _AD_MODE_SET)	//Advence Mode
	{
		switch(bType)
		{
			case CREDENTIALTYPE_BLE:
				gPinInputKeyFromBeginBuf[0] = ConvertByteToInputKey(bSlotNumber);
				ModeGotoOneRemoconRegisterByBleN();
				break;
		
			case CREDENTIALTYPE_PINCODE:
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;

			case CREDENTIALTYPE_CARD:
#ifdef	DDL_CFG_RFID
				gPinInputKeyFromBeginBuf[0] = ConvertByteToInputKey(bSlotNumber);
				ModeGotoOneCardRegisterByBleN();
#else
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
#endif
				break;

			case CREDENTIALTYPE_FINGERPRINT:
#ifdef	DDL_CFG_FP //hyojoon_20160831 PFM-3000 add
				//BLE hex 로 slot number 가 들어고 저장은 BCD 로 
				gPinInputKeyFromBeginBuf[0] = ConvertByteToInputKey(bSlotNumber);
				BioModuleOff();
				Delay(SYS_TIMER_15MS);
				ModeGotoOneFingerRegisterByBleN();
#else
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
#endif				
				break;

			case CREDENTIALTYPE_GMTOUCHKEY:	
#if defined (DDL_CFG_DS1972_IBUTTON)
				gPinInputKeyFromBeginBuf[0] = ConvertByteToInputKey(bSlotNumber);
				ModeGotoOneDS1972TouchKeyRegisterByBleN();
#elif defined (DDL_CFG_IBUTTON)
				gPinInputKeyFromBeginBuf[0] = ConvertByteToInputKey(bSlotNumber);
				ModeGotoOneGMTouchKeyRegisterByBleN();
#else
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
#endif				
				break;

			default : 
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;

		}
	}
	else		//Easy Mode
	{
		switch(bType)
		{
			case CREDENTIALTYPE_BLE:
				ModeGotoRemoconRegisterByBleN();
				break;
				
			case CREDENTIALTYPE_PINCODE:				
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;
				
			case CREDENTIALTYPE_CARD:
#ifdef	DDL_CFG_RFID
				ModeGotoCardRegisterByBleN();
#else
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
#endif				
				break;
				
			case CREDENTIALTYPE_FINGERPRINT:	
#ifdef	DDL_CFG_FP //hyojoon_20160831 PFM-3000 add
				BioModuleOff();
				Delay(SYS_TIMER_15MS);
				ModeGotoFingerRegisterByBleN();
#else
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
#endif				
				break;

			case CREDENTIALTYPE_GMTOUCHKEY:	
#ifdef DDL_CFG_DS1972_IBUTTON
				ModeGotoDS1972TouchKeyRegisterByBleN();
#elif defined (DDL_CFG_IBUTTON)
				ModeGotoGMTouchKeyRegisterByBleN();
#else
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
#endif				
				break;

			default : 
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;

		}
	}
}


void BLENTxModeChangeSubEventSend(BYTE mode) //hyojoon.kim_BLE
{
	BYTE tmpArray[2];

	tmpArray[0] = 0x02;
	tmpArray[1] = mode;

	if(gbModuleMode == PACK_ID_CONFIRMED && gfPackTypeiRevoBleN)
	{
		PackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_EVENT, &gbComCnt, tmpArray, 2);
	}


	if(gbInnerModuleMode == PACK_ID_CONFIRMED && gfInnerPackTypeiRevoBleN)
	{
		InnerPackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 2);
	}
}



BYTE AllLockOutStatusCheck(BYTE bSlotNumber) //hyojoon.kim all lock out check BLE 0x74 , 0xB7 
{
	BYTE bResult;
	
	if(gfBLENAllLockOut == 0x01)
	{
		if(gbManageMode ==_AD_MODE_SET && bSlotNumber == RET_MASTER_CODE_MATCH) //master mode 
			bResult = FALSE; // advanced mode 에서 master code 만 
		else if(gbManageMode == 0 && bSlotNumber == 0x01)
			bResult = FALSE; // normal mode 에서 USER_CODE_SLOT 만 
		else 
			bResult = TRUE; // 나머지 credential 은 AllLockOutStatusCheck 을 호출 할떄 slotnumber 를 0xff 로 넣어서 TRUE 나오게 
	}
	else 
		bResult = FALSE;
	
	return bResult;
}

void LoadAllLockOutStatus(void) //hyojoon.kim
{
	BYTE bTmp = 0x00;
	RomRead(&bTmp,ALL_LOCK_OUT, 1);
	if(bTmp == 0xFF) //ROM 이 비어 있으면 
		bTmp = 0x00;
	gfBLENAllLockOut = bTmp;
}

void SetAllLockOutStatus(BYTE OnOff) //hyojoon.kim
{
	BYTE bTmp = 0x00;
	bTmp = OnOff;
	RomWrite(&bTmp, (WORD)ALL_LOCK_OUT, 1);	
	gfBLENAllLockOut = bTmp;
}

#ifdef	__FRONT_BLE_CONTROL__
BYTE InnerPackFrontBLENProcess(void)
{
	BYTE RetVal = 0;

	switch(gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA])
	{
		case SUBEV_START_STOP_FRONT_BLE:
			FrontBLENStartStopAckProcess(gbInnerPackRxDataCopiedBuf[5],gbInnerPackRxDataCopiedBuf[6]);
			RetVal = 1;
			break;

		default:
			break;
	}	

	return (RetVal);
}

void FrontBLENStartStopAckProcess(BYTE command , BYTE data)
{
	switch (command)
	{
		case SUBEV_DATA_START_MODULE:
		{
			/* Start Front BLE Module ACK */
			if(data == 0x00)
			{
				gbFrontBLENStatus = 0x00; // ENABLE 성공
				gbInnerModuleMode = PACK_ID_NOT_CONFIRMED;
			}
			else 
			{
				gbFrontBLENStatus = 0xFF; // ENABLE 실패
				gbInnerModuleMode = 0x00; 
			}
		}
		break;

		case SUBEV_DATA_STOP_MODULE:
		{
			/* Stop Front BLE Module ACK */	
			if(data == 0x00)
			{
				BYTE InnerSavedIdBuf[4];	
				memset(InnerSavedIdBuf,0xFF,4);
				RomWrite(InnerSavedIdBuf, INNER_PACK_ID, 4);
				gbInnerModuleMode = 0x00; 
				gbFrontBLENStatus = 0xFF; // DISABLE  성공 
			}
			else 
			{
				gbFrontBLENStatus = 0x00; // DISABLE  실패 
			}			
		}
		break;

		case SUBEV_DATA_CHECK_MODULE:
		{
			/* Check Front BLE Module status ACK */
			if(data == 0x00)
			{
				if(gfPackTypeiRevoBleN)
				{
					/* enable 상태 인데 main ble 가 있으면 stop */
					BLENTxFrontBLEStartStopCheckSend(SUBEV_DATA_STOP_MODULE);
				}
				gbFrontBLENStatus = 0x00; // ENABLE 상태 
			}
			else 
			{
				if(gfPackTypeiRevoBleN == 0x00)
				{
					/* disable 상태 인데 main BLE 가 없으면 start */
					gbInnerModuleMode = PACK_ID_NOT_CONFIRMED; 
					BLENTxFrontBLEStartStopCheckSend(SUBEV_DATA_START_MODULE);
				}
				gbFrontBLENStatus = 0xFF; // DISABLE 상태 
				gbInnerModuleMode = 0x00;
			}
		}
		break;


		default :
		break;
	}
}

void BLENTxFrontBLEStartStopCheckSend(BYTE command)
{
	BYTE tmpArray[2];

	tmpArray[0] = SUBEV_START_STOP_FRONT_BLE;
	tmpArray[1] = command;
 
	if(gfInnerPackTypeiRevoBleN)
	{
		InnerPackTx_MakePacketSinglePort(EV_IREVO_SUB_EVENT, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 2);
	}
}
#endif 

BYTE PackRxBleNCmdProcess(void)	
{
	BYTE RetVal = 0;

	switch(gbPackRxDataCopiedBuf[PACK_F_EVENT])
	{
		// BLE N Command 내용 확인
		case EV_IREVO_SUB_EVENT: 	
			BLENSubCommandProcess(gbPackRxDataCopiedBuf[PACK_F_ADDDATA]);
			RetVal = 1;
			break;

		default : 
			RetVal = 0;
			break;			
	}

	return (RetVal);
}

BYTE InnerPackRxBleNCmdProcess(void)	
{
	BYTE RetVal = 0;

	switch(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT])
	{
		// BLE N Command 내용 확인
		case EV_IREVO_SUB_EVENT: 	
			BLENSubCommandProcess(gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA]);
			RetVal = 1;
			break;

		default : 
			RetVal = 0;
			break;			
	}

	return (RetVal);
}


BYTE GetCurrentOperationMode(void)
{
	BYTE bMode = 0xBB;
	
	if(gbManageMode == _AD_MODE_SET)
	{
		bMode = _AD_MODE_SET;
	}

	return (bMode);	
}


#endif	//BLE_N_SUPPORT


