//------------------------------------------------------------------------------
/** 	@file		ModeCardVerify_T1.c
	@version 0.1.00
	@date	2016.04.20
	@brief	Card Verify Mode 
	@remark	 입력한 카드를 저장된 카드와 비교하여 처리하는 모드
	@see	MainModeProcess_T1.c
	@see	CardProcess_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.20		by Jay
			- 신규 UI에 따른 처리
			[입력한 카드 처리 단계] 
			1.입력한 카드를 저장된 카드와 비교
			2.일치하는 카드에 의한 모터 열림
			3.외부강제잠금 설정

		V0.1.01 2016.04.22		by Jay
			- ModeCardVerify 함수에서 case 0 처리 내용 추가하고, 
			기존 내용은 MODEPRCS_CARD_VERIFY 단계로 이동
			=> 이렇게 하지 않을 경우 CardProcess 루틴의 단계가 삭제되지 않고 계속 구동되는 문제 있음
			
*/
//------------------------------------------------------------------------------

#include "Main.h"



//------------------------------------------------------------------------------
/** 	@brief	Pincode Verify Mode Start
	@param	None
	@return 	None
	@remark 비밀번호 인증 모드 시작
*/
//------------------------------------------------------------------------------
void ModeGotoCardVerify(void)
{
	gbMainMode = MODE_CARD_VERIFY;
	gbModePrcsStep = 0;
}	



void ModeGotoInputCardVerify(void)
{
	gbMainMode = MODE_CARD_VERIFY;
	gbModePrcsStep = MODEPRCS_CARD_VERIFY;
}



//------------------------------------------------------------------------------
/** 	@brief 	Compare Input Card UID with Saved UID
	@param	[InputCardNum] : 비교할 카드의 수 입력
	@return 	[_DATA_NOT_IDENTIFIED] : 동일한 UID가 없을 경우
	@return 	[1~(SUPPORTED_USERCARD_NUMBER+1)] : 동일한 UID가 있는 경우 Slot Number 회신
	@remark 입력된 카드의 UID를 저장된 UID와 비교 
*/
//------------------------------------------------------------------------------
BYTE CardVerify(BYTE InputCardNum)
{
	BYTE bCnt;
	BYTE LoopCnt;
	BYTE CardUidData[MAX_CARDUID_LENGTH];

	CopySlotNumberForPack(0);
	memset(CardUidData, 0xFF, MAX_CARDUID_LENGTH);
	CopyCredentialDataForPack(CardUidData, 8);

#if defined (DDL_CFG_MS)

	for(bCnt = 0; bCnt < MS_GetSupported_Credential_Number(SUM_OF_CARD) ; bCnt++)
	{
		RomRead(CardUidData,MS_GetSupported_Credential_Start_Address(MANAGER_CARD_INDEX)+(MAX_CARD_UID_SIZE*(WORD)bCnt), MAX_CARD_UID_SIZE);

#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(CardUidData,8);
#endif 	
		if(DataCompare(CardUidData, 0xFF, MAX_CARD_UID_SIZE) == _DATA_IDENTIFIED)
		{
			continue;
		}

		for(LoopCnt = 0; LoopCnt < InputCardNum; LoopCnt++)
		{
			if(memcmp(CardUidData, gCardAllUidBuf[LoopCnt], (MAX_CARD_UID_SIZE-1)) == 0) // 7 자리만 비교 
			{
				CopySlotNumberForPack(bCnt+1);
				CopyCredentialDataForPack(CardUidData, MAX_CARD_UID_SIZE);
				MS_Set_Verified_Type(CardUidData[7]);
				return (bCnt+1);
			}
		}
	}
#else 
	for(bCnt = 0; bCnt < SUPPORTED_USERCARD_NUMBER; bCnt++)
	{
		RomRead(CardUidData, CARD_UID+(MAX_CARD_UID_SIZE*(WORD)bCnt), MAX_CARD_UID_SIZE);

#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(CardUidData,8);
#endif 	
		if(DataCompare(CardUidData, 0xFF, MAX_CARD_UID_SIZE) == _DATA_IDENTIFIED)
		{
			continue;
		}

		for(LoopCnt = 0; LoopCnt < InputCardNum; LoopCnt++)
		{
			if(memcmp(CardUidData, gCardAllUidBuf[LoopCnt], MAX_CARD_UID_SIZE) == 0)
			{
				CopySlotNumberForPack(bCnt+1);
				CopyCredentialDataForPack(CardUidData, MAX_CARD_UID_SIZE);
				
				return (bCnt+1);
			}
		}
	}
#endif 
	CopyCredentialDataForPack(gCardAllUidBuf[0], MAX_CARD_UID_SIZE);

	return _DATA_NOT_IDENTIFIED;
}


//------------------------------------------------------------------------------
/** 	@brief 	Verify Input Card
	@param	None
	@return 	None
	@remark 입력된 카드 인증 확인 
*/
//------------------------------------------------------------------------------
void InputCardVerify(void)
{
	BYTE bTmp;

	PCErrorClearSet();

	bTmp = CardVerify(GetCardInputNumber());

#if defined (DDL_CFG_MS)
	if(!MS_GetSupported_Authority(Ms_Credential_Info.bVerifiedType,MOTOR_OPEN_ALLOW)) 
	{
		/* 문여는 권한 없으면 못열게 */
		bTmp = _DATA_NOT_IDENTIFIED;
	}
#endif 	
	
#ifdef	_MASTER_CARD_SUPPORT
	if(bTmp == _DATA_NOT_IDENTIFIED)
	{
		// 일본향 마스터 카드의 경우에는 무선 송수신 내용에 영향 받지 않음.
		bTmp = MasterCardVerify(GetCardInputNumber());

		if(bTmp != 0xFB)
		{
			TamperCountIncrease();
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
			return;
		}
	}
	else
	{
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER)
		// 마스터 카드가 아닌 일반 카드는 내부강제잠금 상태에서 문 열림 안됨
		bTmp = MotorSensorCheck();
		switch(bTmp)
		{
			case SENSOR_OPENLOCK_STATE:
			case SENSOR_CLOSELOCK_STATE:
			case SENSOR_CENTERLOCK_STATE:
			case SENSOR_LOCK_STATE:
				PackAlarmReportInnerForcedLockOpenCloseFailSend();
				FeedbackLockIn();
				gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
				return;
		
			default:
				break;
		}
#endif	// DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER

#ifdef	BLE_N_SUPPORT
		/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */ 
		if(AllLockOutStatusCheck(0xFF))
		{
			FeedbackAllCodeLockOut();
	
			gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
			return;
		}
#endif
	}
#else	// _MASTER_CARD_SUPPORT
	if(bTmp == _DATA_NOT_IDENTIFIED)
	{
		TamperCountIncrease();
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
		return;
	}					

#ifdef	BLE_N_SUPPORT
	/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */ 
	if(AllLockOutStatusCheck(0xFF))
	{
		FeedbackAllCodeLockOut();

		gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
		return;
	}
#endif

#endif	// _MASTER_CARD_SUPPORT	


#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
	{
		if(ScheduleEnableCheck_Ble30(CREDENTIALTYPE_CARD, (GetSlotNumberForPack()-1)) != 0)
		{
			gbModePrcsStep = MODEPRCS_CARD_GET_TIME;
			return;
		}
	}
#endif

	if(AlarmStatusCheck() == STATUS_SUCCESS)
	{
		DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
		TamperCountClear();

		AlarmGotoAlarmClear();
		gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
	}
	else
	{
//#if defined (__DDL_MODEL_FRONTDOORSUITE)
#ifdef	CLOSE_BYCREDENTIAL//Card 인증이 정상이면 무조건 모터 open이 아니고 센서 값에 따라 모터 동작을 결정 한다.(PANPAN , Sandman)
			bTmp = MotorSensorCheck();
			switch(bTmp)
			{
				case SENSOR_CLOSE_STATE:
				case SENSOR_CLOSECEN_STATE:
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN	//PANPAN의 경우 내부 강제 잠김 상태에서도 문을 열거나 닫을수  있기 때문
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CLOSECENLOCK_STATE:	
#endif//DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN #endif		

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorOpen();
#endif//FEEDBACK_AFTER_MOTOR_BACKTURN #endif
					StartMotorOpen();

#ifdef USED_SECOND_MOTOR	//FDS , SCREENDOOR의 경우 제어해야 할 모터가 두개 이 기 때문 
					StartMotorOpen_Second();
#endif	//USED_SECOND_MOTOR #endif

					DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
					TamperCountClear();
					gbModePrcsStep = MODEPRCS_OPEN_BYCARD_COMPLETE_CHECK;
				break;
					
				case SENSOR_OPEN_STATE: 
				case SENSOR_OPENCEN_STATE:					
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN	//PANPAN의 경우 내부 강제 잠김 상태에서도 문을 열거나 닫을수  있기 때문					
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_OPENCENLOCK_STATE:	
#endif	//DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN #endif	
				default:
/*		
//2020_04_28 Card를 이용한 문 잠김은 DPS상태를 참조 하지 않는것으로 사양 변경
#ifdef DDL_DPS_CHECK_BEFORE_WORK	//FDS , SCREENDOOR의 경우 문이 열려 있으면 Card를 이용한 해정 금지가 UI임
					if(gfDoorSwOpenState)	//문이 열려 있으면 에러 처리	
					{
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
						break;
					}
#endif	//DDL_DPS_CHECK_BEFORE_WORK #endif		
*/
#if 0
//FDS를 처음 진행 할때 아래 주석과 같은 예외 조항을 만들었지만 필요 없는것 같아 주석 처리 시작 
					else
					{						
						bTemp = GetFinalMotorStatus_Second();
						StartMotorClose();
						if(bTemp == FINAL_MOTOR_STATE_OPEN_ERROR || bTemp == FINAL_MOTOR_STATE_CLOSE_ERROR)	
						{
							StartMotorClose_Second();
						}
						DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
						TamperCountClear();
						gbModePrcsStep = MODEPRCS_CLOSE_BYCARD_COMPLETE_CHECK;
					}
//#endif	//__DDL_MODEL_FRONTDOORSUITE #endif	
					//FDS를 처음 진행 할때 아래 주석과 같은 예외 조항을 만들었지만 필요 없는것 같아 주석 처리 	
#endif //#if  #endif
					
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorClose();
#endif		
					StartMotorClose();

					DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
					TamperCountClear();

					gbModePrcsStep = MODEPRCS_CLOSE_BYCARD_COMPLETE_CHECK;				
					break;					

			}	
#else	//CLOSE_BYCREDENTIAL #else

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		BatteryCheck();
#else
		FeedbackMotorOpen();
#endif
		// Card Unlock Event 전송
		StartMotorOpen();

		DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
		TamperCountClear();

		gbModePrcsStep = MODEPRCS_OPEN_BYCARD_COMPLETE_CHECK;
#endif	//CLOSE_BYCREDENTIAL #endif
	}
}



//------------------------------------------------------------------------------
/** 	@brief 	Motor Open Complete Process By Card
	@param	None
	@return 	None
	@remark 인증된 카드에 의한 모터 열림 완료된 후 동작 처리 
*/
//------------------------------------------------------------------------------
void MotorOpenCompleteByCard(void)
{
	BYTE bTmpArray[MAX_CARDUID_LENGTH+2];

#if defined (_USE_LOGGING_MODE_)
	bTmpArray[0] = EV_USER_CREDENTIAL_UNLOCK;
	bTmpArray[1] = CREDENTIALTYPE_CARD; 
	bTmpArray[2] = 0x00;	
	bTmpArray[3] = GetSlotNumberForPack();
	SaveLog(bTmpArray,4);
#endif
	
#ifdef	DDL_CFG_ADD_REMOCON_LINK_FUNCTION
	PackTxRemoconLinkSend(0x02);
#endif

#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN) \
	|| defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER)	
#ifndef	P_SNS_LOCK_T
	InnerForcedLockClear();	
#endif 
#endif 
	// Pack Module로 카드에 의한 열림 Event 내용 전송 
	bTmpArray[0] = CREDENTIALTYPE_CARD;
	bTmpArray[1] = GetSlotNumberForPack();

#ifdef	_MASTER_CARD_SUPPORT
	// 일본향 제품의 Master Card에 의한 열림은 전송하지 않음.
	if(bTmpArray[1] == 0xFB)
	{
		gbModePrcsStep = MODEPRCS_LOCKOUTINIT_BY_CARD;
	}
#endif

	GetCredentialDataForPack(&bTmpArray[2], MAX_CARDUID_LENGTH);
	PackTx_MakePacket(EV_USER_CREDENTIAL_UNLOCK, ES_LOCK_EVENT, &gbComCnt, bTmpArray, (MAX_CARDUID_LENGTH+2));

	gbModePrcsStep = MODEPRCS_LOCKOUTINIT_BY_CARD;

#if defined (DDL_CFG_MS)
	if(MS_Get_Exclusive_Type() == MS_EXCLUSICE_TYPE_1 || MS_Get_Exclusive_Type() == MS_EXCLUSICE_TYPE_3)
	{
		/* V100_F_MS_LH 이거나 , S170_MS_INCHEON_DORMITORY  인경우 */
		/* MS_Do_Exclusive_Function 를 호출 해서 특수 사양을 처리 한다 */
		MS_Do_Exclusive_Function();
	}
#endif 
	
}

#ifdef CLOSE_BYCREDENTIAL	//Card를 이용한 문 닫힘 
//------------------------------------------------------------------------------
/** 	@brief 	Motor Close Complete Process By Card
	@param	None
	@return 	None
	@remark 인증된 카드에 의한 모터 열림 완료된 후 동작 처리 
*/
//------------------------------------------------------------------------------
void MotorCloseCompleteByCard(void)
{
	BYTE bTmpArray[MAX_CARDUID_LENGTH+3];
	BYTE bTmp , bTemp;
	
#ifdef	DDL_CFG_ADD_REMOCON_LINK_FUNCTION
	PackTxRemoconLinkSend(0x03);
#endif

#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN) \
	|| defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER)	
#ifndef	P_SNS_LOCK_T
	InnerForcedLockClear();	
#endif 
#endif 

	// Pack Module로 카드에 의한 열림 Event 내용 전송 
	bTmpArray[0] = CREDENTIALTYPE_CARD;
	bTmpArray[1] = GetSlotNumberForPack();

#ifdef USED_SECOND_MOTOR	//모터 두개 대응
	bTmp = GetFinalMotorStatus();
	bTemp = GetFinalMotorStatus_Second();	

	if(bTmp ==  FINAL_MOTOR_STATE_CLOSE_ING || bTemp == FINAL_MOTOR_STATE_CLOSE_ING)
	{
		return;
	}
					
	if(bTmp == FINAL_MOTOR_STATE_CLOSE &&  bTemp == FINAL_MOTOR_STATE_CLOSE)
	{
		bTmpArray[2] = 0x81;	//Secure Mode
#ifdef	DDL_CFG_ADD_REMOCON_LINK_FUNCTION		
		PackTxRemoconLinkSend(0x03);
#endif
	}		
	else if(bTmp == FINAL_MOTOR_STATE_CLOSE)
	{
		bTmpArray[2] = 0x80;	//Safety Mode
	}
#else
	bTmpArray[2] = 0x80;//Lock
#endif	

#ifdef	_MASTER_CARD_SUPPORT
	// 일본향 제품의 Master Card에 의한 열림은 전송하지 않음.
	if(bTmpArray[1] == 0xFB)
	{
		gbModePrcsStep = MODEPRCS_LOCKOUTINIT_BY_CARD;
	}
#endif

	GetCredentialDataForPack(&bTmpArray[3], MAX_CARDUID_LENGTH);
	PackTx_MakePacket(EV_USER_CREDENTIAL_LOCK, ES_LOCK_EVENT, &gbComCnt, bTmpArray, (MAX_CARDUID_LENGTH+3));

#ifdef USED_SECOND_MOTOR //모터 두개 대응
	if(bTmpArray[2] == 0x81)		//Secure Mode	//2019년 12월 12일 전 까지 gbMainMotorFeedbackFlag를 이용하여 구분 했었음
	{
		gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
	}
	else
	{
		gbModePrcsStep = MODEPRCS_LOCKOUTINIT_BY_CARD;
	}
#else
	gbModePrcsStep = MODEPRCS_LOCKOUTINIT_BY_CARD;
#endif

#if defined (DDL_CFG_MS)
	if(MS_Get_Exclusive_Type() == MS_EXCLUSICE_TYPE_1 || MS_Get_Exclusive_Type() == MS_EXCLUSICE_TYPE_3)
	{
		/* V100_F_MS_LH 이거나 , S170_MS_INCHEON_DORMITORY  인경우 */
		/* MS_Do_Exclusive_Function 를 호출 해서 특수 사양을 처리 한다 */
		MS_Do_Exclusive_Function();
	}
#endif 
	
}
#endif

//------------------------------------------------------------------------------
/** 	@brief 	Out Forced Lock Cancel Condition By Card
	@param	None
	@return 	None
	@remark 외부강제잠금 설정 모드에서 빠져나가기 위한 조건
	@remark 접촉된 카드가 떨어질 경우 외부강제잠금 설정 종료
*/
//------------------------------------------------------------------------------
void CancelConditionOfOutForcedLockByCard(void)
{
	if(GetCardReadStatus() == CARDREAD_NO_CARD)
	{
		FeedbackBuzStopLedOff();
		ModeClear();
	}	
}



//------------------------------------------------------------------------------
/** 	@brief 	Out Forced Lock Complete Process By Card
	@param	None
	@return 	None
	@remark 인증된 카드에 의한 외부강제잠금 완료된 후 동작 처리 
*/
//------------------------------------------------------------------------------
void OutForcedLockSetByCard(void)
{
	BYTE bTmpArray[11];

	DDLStatusFlagSet(DDL_STS_OUTLOCK);

//	if(gbModuleMode == RF_TRX_MODULE_2WAY)
	{
		// Pack Module로 Event 내용 전송 
		bTmpArray[0] = 0xFF;		// No Code Data
		bTmpArray[1] = CREDENTIALTYPE_CARD;
		bTmpArray[2] = GetSlotNumberForPack();
		memset(&bTmpArray[3], 0xFF, 8);
		GetCredentialDataForPack(&bTmpArray[3], MAX_CARDUID_LENGTH);
		PackTx_MakePacket(EV_ARM_REQEST, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 11);
		
		SetModeProcessTime(100);
		gbModePrcsStep = MODEPRCS_LOCKOUTSET_SEND_CHK;
		
	}
}


//------------------------------------------------------------------------------
/** 	@brief	Pincode Verify Mode Process
	@param	None
	@return 	None
	@remark 카드 인증 모드 처리 과정
*/
//------------------------------------------------------------------------------
void ModeCardVerify(void)
{
	BYTE bTmp;
#ifdef	CLOSE_BYCREDENTIAL	//카드에 의한 모터 잠금을 수행 하기 위해 모터 센서 값을 받는 변수
	BYTE bTemp;
#endif
	
	switch(gbModePrcsStep)
	{
		case 0:
			if(AlarmStatusCheck() == STATUS_SUCCESS)
			{
				gbModePrcsStep++;
				SetModeProcessTime(300);
			}
			else
			{
				gbModePrcsStep++;
				SetModeProcessTime(300);
				//verify 후 내부강제잠금 센서확인 하도록 수정 
#if 0
				// 경보 상태가 아니면 등록된 카드, 등록되지 않은 카드 모두 내부강제잠금 처리
				bTmp = MotorSensorCheck();
				switch(bTmp)
				{
					case SENSOR_OPENLOCK_STATE:
					case SENSOR_CLOSELOCK_STATE:
					case SENSOR_CENTERLOCK_STATE:
					case SENSOR_LOCK_STATE:
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER) 
						FeedbackLockInOpenAllowState();
						gbModePrcsStep++;
						SetModeProcessTime(300);
#elif defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN)
						if(bTmp == SENSOR_CLOSELOCK_STATE || bTmp == SENSOR_OPENLOCK_STATE)
						{
							FeedbackLockInOpenAllowState();
							gbModePrcsStep++;
							SetModeProcessTime(300);
						}
						else 
						{
							PackAlarmReportInnerForcedLockOpenCloseFailSend();
							FeedbackLockIn();
							gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
						}
#else 
						PackAlarmReportInnerForcedLockOpenCloseFailSend();
						FeedbackLockIn();
						gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
#endif 						
						break;
				
					default:
						gbModePrcsStep++;
						SetModeProcessTime(300);
						break;
				}
#endif				
			}
			break;


#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN) || defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER) 
		case 99:
		    if(GetBuzPrcsStep() || GetVoicePrcsStep() )
			{
				break;
			}

			bTmp = GetCardReadStatus(); 
			if(bTmp==CARDREAD_SUCCESS)
			{
				ModeGotoInputCardVerify();
				CardReadStatusClear();
			}else if(bTmp == CARDREAD_NO_CARD)
			{
				//FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//중국 방도 테스트 대응 - 카드 인증 에러를 제외하고 에러음 제거2019년10월11일 심재철  
				ModeClear();
				CardReadStatusClear();
			}
			
			if(GetModeProcessTime() == 0x00)
			{				
				//FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//중국 방도 테스트 대응 - 카드 인증 에러를 제외하고 에러음 제거2019년10월11일 심재철  
				ModeClear();
				CardReadStatusClear();
			}			
			break;
#endif
		case 1:
			bTmp = GetCardReadStatus(); 
			if(bTmp == CARDREAD_SUCCESS)
			{
				JigInputDataSave(0x00, gCardAllUidBuf[0], MAX_CARD_UID_SIZE);
				bTmp = MotorSensorCheck();
				switch(bTmp)
				{
					case SENSOR_OPENLOCK_STATE:
					case SENSOR_CLOSELOCK_STATE:
					case SENSOR_CENTERLOCK_STATE:
					case SENSOR_LOCK_STATE:
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER) 
							FeedbackLockInOpenAllowState();	
							SetModeProcessTime(300);
							gbModePrcsStep = 99;						

#elif defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN)
						if(bTmp == SENSOR_CLOSELOCK_STATE || bTmp == SENSOR_OPENLOCK_STATE)
						{
							FeedbackLockInOpenAllowState();		
							SetModeProcessTime(300);
							gbModePrcsStep = 99;
						}
						else 
						{
         					PackAlarmReportInnerForcedLockOpenCloseFailSend();
							FeedbackLockIn();
							gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
							CardReadStatusClear();
						}
#else 
						PackAlarmReportInnerForcedLockOpenCloseFailSend();
						FeedbackLockIn();
						gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
						CardReadStatusClear();
#endif 						
						break;				
					default:
						ModeGotoInputCardVerify();						
						CardReadStatusClear();
						break;
				}
				
			}
			else if(bTmp == CARDREAD_NO_CARD)
			{
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN) \
	|| defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER) 
				//FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//중국 방도 테스트 대응 - 카드 인증 에러를 제외하고 에러음 제거2019년10월11일 심재철  
#endif 
				ModeClear();
				CardReadStatusClear();
			}			
			
			if(GetModeProcessTime() == 0x00)
			{				
				//FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//중국 방도 테스트 대응 - 카드 인증 에러를 제외하고 에러음 제거2019년10월11일 심재철  
				ModeClear();
				CardReadStatusClear();
			}
			break;				
		
		case MODEPRCS_CARD_VERIFY:
			InputCardVerify();
			break;


#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
		case MODEPRCS_CARD_GET_TIME:
			TimeDataClear();

			PackTx_MakePacket(F0_GET_TIME_AND_DATE, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);

			// Time Data가 1초 내에 회신되지 않으면 에러 처리
			gModeTimeOutTimer100ms = 10;

			gbModePrcsStep = MODEPRCS_CARD_GET_TIME_CHECK;
			break;

		case MODEPRCS_CARD_GET_TIME_CHECK:
			if(IsGetTimeDataCompleted() == true)
			{
				gbModePrcsStep = MODEPRCS_CARD_SCHEDULE_VERIFY;
			}
			else if(gModeTimeOutTimer100ms == 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
			}
			break;			

		case MODEPRCS_CARD_SCHEDULE_VERIFY:
			if(ScheduleVerify_Ble30(CREDENTIALTYPE_CARD, gbTimeBuff_Ble30, (BYTE)GetSlotNumberForPack()) == 1)
			{
				if(AlarmStatusCheck() == STATUS_SUCCESS)
				{
					DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
					TamperCountClear();
			
					AlarmGotoAlarmClear();
					gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
				}
				else
				{
#ifdef	CLOSE_BYCREDENTIAL	
//카드 인증이 정상이면 무조건 모터 open이 아니고 센서 값에 따라 모터 동작을 결정 한다.
//SCHEDULE 기능 테스트 후 Define을  __DDL_MODEL_PANPAN_FC2A -> CLOSE_BYCREDENTIAL으로 바꿀지 확인
//PANPAN에서 Card SCHEDULE을 테스트 하지 못했고 FDS도 2020년03월11까지 SCHEDULE을 테스트 하지 못했음
					bTemp = MotorSensorCheck();
					switch(bTemp)
					{					
						case SENSOR_CLOSE_STATE:
						case SENSOR_CLOSECEN_STATE:
						case SENSOR_CLOSELOCK_STATE:
						case SENSOR_CLOSECENLOCK_STATE:
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
							BatteryCheck();
#else
							FeedbackMotorOpen();
#endif
							// Card Unlock Event 전송
							StartMotorOpen();

#ifdef USED_SECOND_MOTOR	//FDS , SCREENDOOR의 경우 제어해야 할 모터가 두개 이 기 때문 
							StartMotorOpen_Second();
#endif	//USED_SECOND_MOTOR #endif

					
							DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
							TamperCountClear();
					
							gbModePrcsStep = MODEPRCS_OPEN_BYCARD_COMPLETE_CHECK;
							break;
			
						case SENSOR_OPEN_STATE:
						case SENSOR_OPENCEN_STATE:
						case SENSOR_OPENLOCK_STATE:
						case SENSOR_OPENCENLOCK_STATE:
						default : //case SENSOR_NOT_STATE, case SENSOR_LOCK_STATE, case SENSOR_CENTER_STATE,case SENSOR_CENTERLOCK_STATE
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
							BatteryCheck();
#else
							FeedbackMotorClose();
#endif		
							StartMotorClose();
			
							DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
							TamperCountClear();
			
							gbModePrcsStep = MODEPRCS_CLOSE_BYCARD_COMPLETE_CHECK;				
							break;	
						
					}	
#else	//__DDL_MODEL_PANPAN_FC2A #else

				
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorOpen();
#endif
					// Card Unlock Event 전송
					StartMotorOpen();
			
					DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
					TamperCountClear();
			
					gbModePrcsStep = MODEPRCS_OPEN_BYCARD_COMPLETE_CHECK;
#endif	//__DDL_MODEL_PANPAN_FC2A #endif					
				}
			}
			else
			{
				TamperCountIncrease();

				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
			}
			break;
#endif	// DDL_CFG_BLE_30_ENABLE


		case MODEPRCS_OPEN_BYCARD_COMPLETE_CHECK:
			MotorOpenCompleteCheck(MotorOpenCompleteByCard, MODEPRCS_LOCKOUTSET_BY_CARD_ERROR);
			break;
			
#ifdef CLOSE_BYCREDENTIAL		//Card를 이용한 모터 닫힘 동작 수행후 이후 센서 확인 및 다음 스텝 결정
		case MODEPRCS_CLOSE_BYCARD_COMPLETE_CHECK:
			MotorCloseCompleteCheck(MotorCloseCompleteByCard, MODEPRCS_LOCKOUTSET_BY_CARD_ERROR);
			break;	
#endif	//CLOSE_BYCREDENTIAL #endif		
						
		case MODEPRCS_LOCKOUTINIT_BY_CARD:
//SecureMode 사용 여부를 결정 하는 루틴 주석 시작			
//gbSecureModeUsed <--- 해당 변수의 설정 상태에 따라 SecureMode 전환 여부를 결정 하게 되는데 이 기능이 
//Specification에서 제거 되기 때문에 주석 처리 하지만 호주 프로젝트의 특성상 부활 가능 성이 있기 때문에 삭제 하지 않고 주석 처리 함
/*#ifdef	__DDL_MODEL_FRONTDOORSUITE
			gbSecureModeUsed = 0x01;
			if(gbSecureModeUsed == 0x01)
			{
				WaitStartOutForcedLockSetting(MODEPRCS_LOCKOUTSET_BY_CARD);
			}
			else
			{
				WaitStartOutForcedLockSetting(MODEPRCS_CARD_AWAY_CHK);
			}
#else	//__DDL_MODEL_FRONTDOORSUITE #else
			WaitStartOutForcedLockSetting(MODEPRCS_LOCKOUTSET_BY_CARD);
//#endif	//__DDL_MODEL_FRONTDOORSUITE #endif*/	
//SecureMode 사용 여부를 결정 하는 루틴 주석 끝

			WaitStartOutForcedLockSetting(MODEPRCS_LOCKOUTSET_BY_CARD);
			break;

		case MODEPRCS_LOCKOUTSET_BY_CARD:
			WaitSomeTimeByLongContact(CancelConditionOfOutForcedLockByCard, MODEPRCS_LOCKOUTSET_BY_CARD_CHK);
			break;
		
		case MODEPRCS_LOCKOUTSET_BY_CARD_CHK:
#ifdef	USED_SECOND_MOTOR	//모터 두개 대응(Secure mode 포함)
			MotorCloseCompleteCheck(MotorCloseCompleteByCard, MODEPRCS_LOCKOUTSET_BY_CARD_ERROR);
#else	//__DDL_MODEL_FRONTDOORSUITE #else
			MotorCloseCompleteCheck(OutForcedLockSetByCard, MODEPRCS_LOCKOUTSET_BY_CARD_ERROR);
#endif	//__DDL_MODEL_FRONTDOORSUITE #endif

			break;
			
		case MODEPRCS_LOCKOUTSET_SEND_CHK:
			// 외부강제잠금에 의한 잠김 신호가 없어 아래 신호 추가 전송
			if(GetModeProcessTime()== 0)
			{
				PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);

				gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
			}
/*
			if(gbPackCommResult == PACK_RESULT_ACK_OK)
			{
//				FeedbackModeCompleted(1, FEEDBACK_MODE_STAY);

				gbLockOperatingLogData[0] = 0x02;
				PackTxAlarmReportSend(REPORT_MANUAL_LOCK, PACK_TX_NO_STANDBY);

				gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
			}
			else if(gwModePrcsTimer10ms == 0)
			{
//				FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_STAY);

				gbLockOperatingLogData[0] = 0x02;
				PackTxAlarmReportSend(REPORT_MANUAL_LOCK, PACK_TX_NO_STANDBY);

				gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;
			}
			
			if(gbPackCommResult != 0)
			{
				gbPackCommResult = 0;
			}
*/
			break;

		case MODEPRCS_LOCKOUTSET_BY_CARD_ERROR:
			if(GetLedMode())	break;
			
			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			else if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}

			gbModePrcsStep = MODEPRCS_CARD_AWAY_CHK;

		case MODEPRCS_CARD_AWAY_CHK:
#ifdef DDL_TEST_SET_FOR_CARD
			CardProcessTimeClear();
			ModeClear();
#else 
			if(GetCardReadStatus()== CARDREAD_NO_CARD)
			{
				ModeClear();
			}	
#endif 			
			break;

		default:
			ModeClear();
			break;
	}
}




