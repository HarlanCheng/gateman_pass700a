//------------------------------------------------------------------------------
/** 	@file		RemoconFunctions_T1.c
	@version 0.1.01
	@date	2016.07.26
	@brief	Remote controller Command Functions
	@see	
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.06.09		by Jay
			- 리모컨 Application 함수

		V0.1.01 2016.07.26		by Jay
			- BLE-N 기능 추가
*/
//------------------------------------------------------------------------------

#include "Main.h"

BYTE PackRemoconCmdProcess(void)
{
	BYTE RetVal = 0;

	if(gbModuleMode == 0)	return 0;

	switch(gbPackRxDataCopiedBuf[PACK_F_EVENT])
	{
		case EV_REGDEVICE_IN:
			SetPackProcessResult(PACK_RMC_REGDEVICE_IN);
			RemoconSlotNumberSet(gbPackRxDataCopiedBuf[PACK_PACKET_RMC_SLOTNUMBER]);
			SubRemoconSlotNumberSet(gbPackRxDataCopiedBuf[PACK_PACKET_RMC_SLOTNUMBER]);			
			RetVal = 1;
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
			break;

		default:
			break;
	}

	return (RetVal);
}



BYTE PackRemoconAckProcess(void)
{
	BYTE RetVal = 0;

	if(gbModuleMode == 0)	return 0;

	switch(gbPackRxDataCopiedBuf[PACK_F_EVENT])
	{
		case EV_ALLREG_DEV_END:
			SetPackProcessResult(PACK_RMC_ALLREG_DEV_END);
			RetVal = 1;
			break;

		case EV_ALL_REMOVE:
			SetPackProcessResult(PACK_RMC_ALL_REMOVE_END);
			RetVal = 1;
			break;

		case EV_ALL_REGISTRATION:
			SetPackProcessResult(PACK_RMC_ALL_REGISTRATION_ACK);
			RetVal = 1;
			break;
			
		default:
			break;
	}

	return (RetVal);
}

void PackTxConnectionSend(BYTE bEvent, BYTE bType, BYTE bSlot)
{
	BYTE		tmpArray[2];
	
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU) // 두개가 다 있는 경우 8번의 8번 하위에서 remocon 등록을 하니 
	if(gbModuleMode ||gbInnerModuleMode )
	{
		if(gfPackTypeiRevoBleN ||gfInnerPackTypeiRevoBleN)
		{
			if(bSlot && (bType == MODULE_TYPE_1_ONEWAY))
			{
				tmpArray[0] = bType;
				tmpArray[1] = bSlot;
#if 1 // 듀얼 팩 지원 제품 동일 팩 삽입시 내부 팩을 우선 순위높게 
#ifdef	__FRONT_BLE_CONTROL__
				if(gfInnerPackTypeiRevoBleN && gbFrontBLENStatus == 0x00)
#else 
				if(gfInnerPackTypeiRevoBleN)
#endif 
				{
					InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 2);					
				}
				else if(gfPackTypeiRevoBleN)
				{
					PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 2);
				}

#else 
				if(gfPackTypeiRevoBleN)
					PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 2);

				if(gfInnerPackTypeiRevoBleN)
					InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 2);					
#endif 				
			}
			else
			{
				tmpArray[0] = bType;
#if 1 // 듀얼 팩 지원 제품 동일 팩 삽입시 내부 팩을 우선 순위높게 
#ifdef	__FRONT_BLE_CONTROL__
				if(gfInnerPackTypeiRevoBleN && gbFrontBLENStatus == 0x00)
#else 
				if(gfInnerPackTypeiRevoBleN)
#endif 					
				{
					InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 1);
				}
				else if(gfPackTypeiRevoBleN)
				{
					PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 1);
				}
#else 
				if(gfPackTypeiRevoBleN)
					PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 1);

				if(gfInnerPackTypeiRevoBleN)
					InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 1);
#endif 				
			}	

			PackProcessResultClear();
		}
	}
#else //pack 하나만 지원 하는 경우 인데 uart 1 uart2 를 다 쓸 수 있도록 
	if(gbModuleMode)
	{
		if(gfPackTypeiRevo || gfPackTypeiRevoBleN)
		{
			if(bSlot && (bType == MODULE_TYPE_1_ONEWAY))
			{
				tmpArray[0] = bType;
				tmpArray[1] = bSlot;
				PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 2);
			}
			else
			{
				tmpArray[0] = bType;
				PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 1);
			}	

			PackProcessResultClear();
		}
	}
	else if(gbInnerModuleMode)
	{
		if(gfInnerPackTypeiRevo ||gfInnerPackTypeiRevoBleN)
		{
			if(bSlot && (bType == MODULE_TYPE_1_ONEWAY))
			{
				tmpArray[0] = bType;
				tmpArray[1] = bSlot;
				InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 2);
			}
			else
			{
				tmpArray[0] = bType;
				InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 1);
			}	

			PackProcessResultClear();
		}
	}
	else 
	{
		__NOP();
	}	
#endif 
}

void PackTxConnectionSendForRF(BYTE bEvent, BYTE bType, BYTE bSlot)
{
	BYTE		tmpArray[2];
	
	if(gbModuleMode ||gbInnerModuleMode )
	{
		{
			if(bSlot && (bType == MODULE_TYPE_1_ONEWAY))
			{
				tmpArray[0] = bType;
				tmpArray[1] = bSlot;

				if(gfInnerPackTypeiRevo)
				{
					InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 2);
				}
				else 	if(gfPackTypeiRevo)
				{
					PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 2);
				}
				else 
				{
					__NOP();
				}
			}
			else
			{
				tmpArray[0] = bType;

				if(gfInnerPackTypeiRevo)
				{
					InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 1);
				}
				else if(gfPackTypeiRevo)
				{
					PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 1);
				}
				else 
				{
					__NOP();
				}
			}	
			PackProcessResultClear();
		}
	}
}


void SubPackTxConnectionSend(BYTE bEvent, BYTE bType, BYTE bSlot)
{
	BYTE		tmpArray[2];
	
	if(gbModuleMode ||gbInnerModuleMode )
	{
		if(gfPackTypeiRevo || gfInnerPackTypeiRevo)
		{
			if(bSlot && (bType == MODULE_TYPE_1_ONEWAY))
			{
				tmpArray[0] = bType;
				tmpArray[1] = bSlot;
#if 1 // 듀얼 팩 지원 제품 동일 팩 삽입시 내부 팩을 우선 순위높게 
				if(gfInnerPackTypeiRevo)
				{
					InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 2);					
				}
				else if(gfPackTypeiRevo)
				{
					PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 2);					
				}
#else 
				if(gfPackTypeiRevo)
					PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 2);

				if(gfInnerPackTypeiRevo)
					InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 2);					
#endif 				
			}
			else
			{
				tmpArray[0] = bType;
#if 1 // 듀얼 팩 지원 제품 동일 팩 삽입시 내부 팩을 우선 순위높게 
				if(gfInnerPackTypeiRevo)
				{
					InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 1);
				}
				else if(gfPackTypeiRevo)
				{
					PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 1);
				}
#else 
				if(gfPackTypeiRevo)
					PackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, 1);

				if(gfInnerPackTypeiRevo)
					InnerPackTx_MakePacketSinglePort(bEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, 1);
#endif 				
			}	

			PackProcessResultClear();
		}
	}
}


BYTE InnerPackRemoconCmdProcess(void)
{
	BYTE RetVal = 0;

	if(gbInnerModuleMode == 0)	return 0;

	switch(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT])
	{
		case EV_REGDEVICE_IN:
			SetInnerPackProcessResult(PACK_RMC_REGDEVICE_IN);
			RemoconSlotNumberSet(gbInnerPackRxDataCopiedBuf[PACK_PACKET_RMC_SLOTNUMBER]);
			SubRemoconSlotNumberSet(gbInnerPackRxDataCopiedBuf[PACK_PACKET_RMC_SLOTNUMBER]);			
			RetVal = 1;
			InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], gbInnerPackTxAddNone, 0);
			break;

		default:
			break;
	}

	return (RetVal);
}



BYTE InnerPackRemoconAckProcess(void)
{
	BYTE RetVal = 0;

	if(gbInnerModuleMode == 0)	return 0;

	switch(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT])
	{
		case EV_ALLREG_DEV_END:
			SetInnerPackProcessResult(PACK_RMC_ALLREG_DEV_END);
			RetVal = 1;
			break;

		case EV_ALL_REMOVE:
			SetInnerPackProcessResult(PACK_RMC_ALL_REMOVE_END);
			RetVal = 1;
			break;

		case EV_ALL_REGISTRATION:
			SetInnerPackProcessResult(PACK_RMC_ALL_REGISTRATION_ACK);
			RetVal = 1;
			break;

		default:
			break;
	}

	return (RetVal);
}


