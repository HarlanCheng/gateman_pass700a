//------------------------------------------------------------------------------
/** 	@file		ModeTestModeProcess_T1.c
	@brief	Test Mode Package
*/
//------------------------------------------------------------------------------

#ifndef __MODETESTMODEPROCESS_T1_INCLUDED
#define __MODETESTMODEPROCESS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"

#define	MENU_LONG_SLEEP			0x00

#define	MENU_TESTMODE_AGING			0x61
#define	MENU_TESTMODE_VOICE			0x66
#ifdef	BLE_N_SUPPORT
#define	MENU_TESTMODE_BLE_APP_KEY		0x87
#define	MENU_TESTMODE_BLE_APP			0x88
#define	MENU_TESTMODE_BLE_RF			0x89
#endif 

#define	MENU_TESTMODE_BATTERY_LIFE_CYCLE_AGING			0x67

#ifdef 	DDL_CFG_RFID
#define	MENU_TESTMODE_OPENUID_SET			0x68
#endif 


// 지문 주키 제품을 위한 Master iButton 등록 모드
#define	MENU_SPECIALMODE_MASTERKEY_REG	0x51

#if defined (DDL_CFG_MS)
#define	MS_MENU_CREDENTIAL_INFO_SET	0x52
#endif 

#ifdef STD_GA_374_2019
extern uint8_t Disable_GA374;
#endif 

#if defined (FACOTRY_PINCODE_TEST_MODE)
#define	MENU_PIOCODE_TEST_MODE		0x77
void ModePincoeTestMode(void);
#endif 


#if defined (_USE_LOGGING_MODE_)
#define	MENU_LOGGING_MODE			0x74
#define	MENU_TIME_SET_MODE			0x75

void ModeLoggingProcess(void);
void ModeMenuTimeSet(void);
uint8_t GetLoggingMode(void);
void SaveLog(uint8_t* recordData,uint8_t size);
void GetRTCTime(RTC_TimeTypeDef* ptime , RTC_DateTypeDef* pdate);
void DeleAlarmReportLog(void);
#endif 

void ModeGotoTestMenu(void);
void TestMenuSelectProcess(void);
void ModeTestMenu(void);
void ModeTestAging(void);
void ModeTestGetCenter(void);
void ModeTestBLEN(void);
void ModeBatteryLifeCycleCounter(void);
void ModeBatteryLifeCycleTest(void);

#ifdef 	DDL_CFG_RFID
void ModeTestOpenUidSet(void);
#endif 

BYTE GetTestModeTime(void);


//=============================================================
//	TEST FUNCTIONS
//=============================================================

// 테스트 외에는 아래 내용은 모두 주석 처리 가능
#endif


