//------------------------------------------------------------------------------
/** 	@file		ModePincodeRegister_T1.c
	@version 0.1.00
	@date	2016.04.19
	@brief	Pincode Register Mode 
	@remark	 User Code 등록 모드
	@see	MainModeProcess_T1.c
	@see	PincodeFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.19		by Jay
			- 신규 UI에 따른 처리
			- User Code를 등록하기 위한 모드 구현
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gDisplayPincodeBuf[MAX_PIN_LENGTH>>1];			/**< 등록된 비밀번호 Display를 위한 버퍼, 등록된 비밀번호 Data 저장  */
BYTE gDisplayPincodeCnt = 0;						/**< 등록된 비밀번호 Display를 위한 변수, 표시되는 비밀번호 자리 수 확인  */



//------------------------------------------------------------------------------
/** 	@brief	Pincode Register Mode Start
	@param	None
	@return 	None
	@remark 비밀번호 등록 모드 시작
*/
//------------------------------------------------------------------------------
void ModeGotoPincodeRegister(void)
{
	gbMainMode = MODE_MENU_PIN_REGISTER;
	gbModePrcsStep = 0;
}


//------------------------------------------------------------------------------
/** 	@brief	Prepare Data For Display Registered Pincode
	@param	None
	@return 	None
	@remark 등록한 비밀번호 Display를 위한 내용 준비
*/
//------------------------------------------------------------------------------
void PrepareDataForDisplayPincode(void)
{
	gDisplayPincodeCnt = 0;
	CopyPincodeForDisplay(gDisplayPincodeBuf);
}


//------------------------------------------------------------------------------
/** 	@brief	Register User Code Process
	@param	None
	@return 	None
	@remark 입력한 비밀번호 등록을 위한 확인 과정 및 등록 과정
*/
//------------------------------------------------------------------------------
#if defined (DDL_CFG_MS)

void ProcessRegisterUserCode(void)
{
	BYTE bTemp = 0x00;

	switch(PincodeVerify(0))
	{
		case RET_NO_MATCH:
			MS_Get_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,&bTemp);
			UpdatePincodeToMemory(MS_GetSupported_Credential_Start_Address(Ms_Credential_Info.bSelected_Item)+(bTemp*8));
			bTemp++; // ?? 컴파일러가 미친듯... 
			MS_Set_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,bTemp);
#ifdef	DDL_CFG_DIMMER
			FeedbackModeCompletedLedOff(VOICE_MIDI_CANCLE, VOL_CHECK);
	
			SetModeProcessTime(150);
			PrepareDataForDisplayPincode();
			gbModePrcsStep++;
#else
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			gbModePrcsStep = 2;
#endif
			break;
	
		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			FeedbackErrorModeKeepOn(AVML_ALREADY_USE_PIN, VOL_CHECK);		//	That code is already in use
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			break;				
	}
}

#else

void ProcessRegisterUserCode(void)
{
	switch(PincodeVerify(0))
	{
		case 1:
		case RET_NO_MATCH:
			UpdatePincodeToMemory(USER_CODE);

			// 기본 비밀번호가 등록될 경우 모터 잠김 금지를 위한 Factory Reset 정보 초기화
			FactoryResetClear();

			// User status 영역 
			SetUserStatus(1, USER_STATUS_OCC_ENABLED);

		// 도어록에서 비밀번호를 새로 등록하거나 변경하는 경우에는 기존 Schedule 관련 정보 모두 초기화
			// Schedule status 영역 삭제 
			SetScheduleStatus(1, SCHEDULE_STATUS_DISABLE);
		
			// Schedule 영역 삭제 
			RomWriteWithSameData(0xFF, USER_SCHEDULE, 4); 

			PackTxEventPincodeAdded(0x01);

#ifdef	DDL_CFG_DIMMER
			FeedbackModeCompletedLedOff(VOICE_MIDI_CANCLE, VOL_CHECK);
	
			SetModeProcessTime(150);
			PrepareDataForDisplayPincode();
			gbModePrcsStep++;
#else
//			FeedbackModeCompleted(1, VOL_CHECK);
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
#endif 			

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			ScheduleDelete_Ble30(CREDENTIALTYPE_PINCODE, 1);
#endif
			break;
	
		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
//			FeedbackErrorModeKeepOn(109, VOL_CHECK);		//	That code is already in use
			FeedbackErrorModeKeepOn(AVML_ALREADY_USE_PIN, VOL_CHECK);		//	That code is already in use
	//		ModeClear();
	
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			break;				
	}
}

#endif 

void PincodeRegisterCompleteProcess(void)
{
//	FeedbackModeCompleted(1, VOL_CHECK);
	FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
#if  defined (DDL_CFG_MS)
	gbModePrcsStep = 2;
#else 
	ModeClear();
#endif 
}


//------------------------------------------------------------------------------
/** 	@brief	Display Process for Registered Pincode
	@param	None
	@return 	None
	@remark 등록한 비밀번호를 Display하는 과정
*/
//------------------------------------------------------------------------------
void DisplayRegisteredPincode(void (*CompleteFunction)(void))
{
	BYTE bDispalyData;
	
	if(GetModeProcessTime()) 	return;
	
	bDispalyData = gDisplayPincodeBuf[gDisplayPincodeCnt >> 1];
	if(gDisplayPincodeCnt & 0x01)
	{
		bDispalyData &= 0x0F;
	}
	else
	{
		bDispalyData >>= 4;
	}

	// case 1: PIN : 1234	-> 12 34 FF FF
	// case 2: PIN : 12345	-> 12 34 50 FF 
//			if((bTmp == 0x0F) || (bTmp == 0x00)) 
	if((bDispalyData == 0x0F) || (gDisplayPincodeCnt == MAX_PIN_LENGTH))
	{
		CompleteFunction();	
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
	else
	{
		FeedbackPincodeDisplay(bDispalyData);

		gDisplayPincodeCnt++;
		SetModeProcessTime(50);	

	}
}


//------------------------------------------------------------------------------
/** 	@brief	Pincode Register Mode
	@param	None
	@return 	None
	@remark User Code 등록 모드
*/
//------------------------------------------------------------------------------
void ModePINRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0)== TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case FUNKEY_REG:
					
#ifdef	DDL_TEST_SET_FOR_CARD
					if(IsCardPowerOnForTest() == true)
					{
						gfCardPowerOnForTest = 0;
					}
#endif 				
					ProcessRegisterUserCode();
					break;		

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

// PASSWORD DISPLAY RTN
		case 1:
			DisplayRegisteredPincode(PincodeRegisterCompleteProcess);
			break;

#if defined (DDL_CFG_MS)
		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
		
			if(MS_Get_Exclusive_Type() == MS_EXCLUSICE_TYPE_2)
			{
				/* 영남대*/
				MS_Do_Exclusive_Function();
			}
			ModeGotoMenuMainSelect();
			break;
#endif 

		default:
			ModeClear();
			break;
	}
}



