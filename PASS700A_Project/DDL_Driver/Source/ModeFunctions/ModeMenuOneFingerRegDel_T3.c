#define		_MODE_MENU_ONEFINGER_REGDEL_C_

#include	"main.h"

void		ModeGotoOneFingerRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGDEL;
	gbModePrcsStep = 0;
}


void		ModeGotoOneFingerRegister(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGISTER;
	gbModePrcsStep = 0;
#ifdef	BLE_N_SUPPORT
	gbEnterMode = 0x00;
#endif	
}

void		ModeGotoOneFingerDelete(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_DELETE;
	gbModePrcsStep = 0;
#ifdef	BLE_N_SUPPORT
	gbEnterMode = 0x00;
#endif	
}

#ifdef	BLE_N_SUPPORT
void ModeGotoOneFingerRegisterByBleN(void)
{
	gPinInputKeyCnt = 2;
	gbEnterMode = 0x01;	
	FingerSlotNumberToRegister();
	gbMainMode = MODE_MENU_ONEFINGER_REGISTER;
	gbModePrcsStep = 2;		
}


void ModeGotoOneFingerDeleteByBleN(void)
{
	gPinInputKeyCnt = 2;
	gbEnterMode = 0x01;
	FingerSlotNumberToDelete();	
	gbMainMode = MODE_MENU_ONEFINGER_DELETE;
	gbModePrcsStep = 2;		
}
#endif 

void		OneFingerModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 지문 개별 등록
		case 1: 	
			ModeGotoOneFingerRegister();
			break;
	
		// 지문 개별 삭제
		case 3: 		
			ModeGotoOneFingerDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}

void		MenuOneFingerModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 지문 개별 등록
		case TENKEY_1:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_FINGER_SHARP, gbInputKeyValue);	//지문등록 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			break;

		// 지문 개별 삭제
		case TENKEY_3:		
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_FINGER_SHARP, gbInputKeyValue);	//지문삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			break;

		case TENKEY_SHARP:
			OneFingerModeSetSelected();
			break;
									
		case TENKEY_STAR:
			if(GetSupportCredentialCount() > 1)
				ModeGotoMenuAdvancedCredentialSelect();
			else 
				ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}

void		ModeOneFingerRegDelCheck(void )
{
	switch(gbModePrcsStep)
	{
		case 0:
			Feedback13MenuOn(AVML_REGFINGER1_DELFINGER3, VOL_CHECK);		//지문 등록은 1번, 지문 삭제는 3번을 누르세요.
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOneFingerModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}


//==============================================================================//
// Comment	: 지문 번호가 메모리에 존재하는지 검사
// Return 	: 0:없음 	1:존재 함.
//==============================================================================//
BYTE	FingerSlotVerify()
{
	BYTE	bTmp, bCnt;

	for(bCnt = 0; bCnt < _MAX_REG_BIO; bCnt++){
		RomRead( &bTmp, (WORD)BIO_ID+(2*bCnt), 1);

		if ( bTmp == gSelectedSlotNumber ) {		// EEPROM에 저장된 slot 정보와 비교
			gbFID = bCnt;
			return 1;
		}else{
			gbFID=0xff;
		}
	}

	return 0;	
}

void FingerSlotNumberToRegister(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();		//slot number는 1~20
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= _MAX_REG_BIO))
	{
		TenKeyVariablesClear();

		bTmp = FingerSlotVerify();		// 지문 번호 일치 하는 것 확인	..
		if(bTmp == 0){			// 지문 번호 일치 하는 것 없음
			gbModePrcsStep++;
		}
		else {					// 지문 번호 일치 하는 것 존재함 ..
			FeedbackError(AVML_ALREADY_USE_SLOT, VOL_CHECK);		//이미 사용하고 있는 사용자번호입니다.
			gbModePrcsStep = 30;		// reinput
		}
	}
	else
	{
		gSelectedSlotNumber = 0xFF;
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}
}

void BackwardFunctionForOneFingerRegDel(void)
{
	ReInputOrModeMove(ModeGotoOneFingerRegDelCheck);
}

//------------------------------------------------------------------------------
/** 	@brief	One Finger Print Register Mode 
	@remark	User Code 등록 모드
	@see	MainModeProcess_T1.c
	@see	FingerPFM_3000.c
	@see	PFM_3000_UART.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.08.31		by Hyojoon
PFM-3000 / TSM1071M normal slot number 관리 
Enroll 시에 slot number 를 정해서 보내게 되면 Finger Module 에서는 전달 한 slotnumber 를 return 한다.  
ex) slot number 를 0x14 를 보내면 
ROM index 는 0 번 부터 시작 하므로 

gbFID = module return 값 - 1;  // ROM 의 index 로 사용 
FingerWriteBuff = gbFID;          // gbFID 받을 값을 gbFID-1 index 에 gbFID 로 저장 not BCD

개발자 마다 slot 관리가 틀려서 TCS4K 의 경우 slot number 를 BCD 로 저장 했는데 그럴 필요 없음 
신규 플렛 폼의 PFM-3000 및 앞으로 개발 할 TSM 의 경우 hex 값을 저장 아래 소스와 같이... 

ROM
--------------------------------
|               BIO_ID + gbFID                  |
--------------------------------
|FingerWriteBuff[0] | FingerWriteBuff[1] |
--------------------------------

*/
//------------------------------------------------------------------------------


void ModeOneFingerRegister(void)
{
	BYTE bTmp;
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if ( no_fingers >= _MAX_REG_BIO ) {			// 이미 최대로 가득 차 있으면 개별 등록 거부.
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		// Error, 저장공간이 부족합니다.
				gbModePrcsStep=40;					// 개별 등록 삭제 메뉴로 가기
				break;
			}
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//개별 비밀번호 관리를 위하여 사용자 번호 두자리를 입력하세요, 계속하시려면 샵 버튼을 누르세요.
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gSelectedSlotNumber = 0;
			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(FingerSlotNumberToRegister, BackwardFunctionForOneFingerRegDel,1);
			break;

		case 2:
			AudioFeedback(AVML_ENTER_FINGER, gcbBuzSta, VOL_CHECK);			// 등록하실 지문을 차례로 입력 하세요.
			SetModeTimeOut(_MODE_TIME_OUT_7S);
			LedModeRefresh();
			gbModePrcsStep++;
			break;

		case 3:
			HFPMTxEventEnroll(gSelectedSlotNumber);
			SetModeTimeOut(10);
			gbModePrcsStep = 5;
			break;
		
		case 5:
			if(GetModeTimeOut() == 0){
				FeedbackError(AVML_TIMEOUT, VOL_CHECK); 	//	times up
				HFPMTxEventEnroll(0xFF);
				gbModePrcsStep = 200;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[3] == 0x02 && gbFingerPrint_rxd_buf[4] == IREVO_HFPM_SUB_EVENT_FP_ENROLL)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case IREVO_HFPM_RESULT_CODE_STATUS_OK:
							SetModeTimeOut(_MODE_TIME_OUT_20S);
							gbModePrcsStep = 6;
							gbFingerLongTimeWait100ms = 10;
							dbDisplayOn = 0x00;
							gbFingerRegisterDisplayStep = 0;
							break;
						
						case IREVO_HFPM_RESULT_CODE_PACKET_ERR: 
						case IREVO_HFPM_RESULT_CODE_CHECK_SUM_ERR:
						case IREVO_HFPM_RESULT_CODE_SLOT_EMPTY: 
						case IREVO_HFPM_RESULT_CODE_PROCESSING_ENROLL:	
						default :
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 
							HFPMTxEventEnroll(0xFF);
							gbModePrcsStep = 200;
						break;
					}
				}		
			}			
			break;
			
		case 6:
			if(GetModeTimeOut() == 0){
				FeedbackError(AVML_TIMEOUT, VOL_CHECK); 	//	times up
				HFPMTxEventEnroll(0xFF);
				gbModePrcsStep = 200;
				break;
			}			

			if(gbInputKeyValue == FUNKEY_REG)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 
				HFPMTxEventEnroll(0xFF);
				gbModePrcsStep = 200;
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == IREVO_HFPM_SUB_EVENT_FP_ENROLL_STATUS_ALARM)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x01:	//1번째 지문 입력 완료		
							AudioFeedback(AVML_REG_FINGER01_REIN,gcbBuzNum, VOL_CHECK); // 지문이 한번 입력 되었습니다. 동일 지문을 다시 입력하세요.
							HFPMTxEventEnrollAlarm();
							gbFingerRegisterDisplayStep = 1;
							break;
							
						case 0x02:	//2번째 지문 입력 완료
							HFPMTxEventEnrollAlarm();
							AudioFeedback(AVML_REG_FINGER02_REIN, gcbBuzNum, VOL_CHECK); 
							gbFingerRegisterDisplayStep = 2;
							break;
							
						case 0x03:	//3번째 지문 입력 완료	
							HFPMTxEventEnrollAlarm();
							Delay(SYS_TIMER_15MS); // uart 가 짤리네 pack 이랑 겹침 
							bTmp = FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID = gbFingerPrint_rxd_buf[6];
							gbModePrcsStep = 20;
							break;	

						case 0xFF:
							FeedbackError(AVML_TIMEOUT, VOL_CHECK); 	//	times up
							HFPMTxEventEnroll(0xFF);
							gbModePrcsStep = 200;
							break;

						case 0x00:	//지문 입력 대기 않함
						default:
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); //미디 에러음 출력
							ModeClear();
							break;
					}
				}
			}
			else 
			{
				FingerPrintRegisterDisplay();
			}
			break;

		case 7: 		// 등록 연속  or 종료...시..
			switch(gbInputKeyValue){				//등록 버튼 눌린후 키가 입력되어 비밀번호 입력 시작

				case FUNKEY_REG:						// 등록을 종료 하려면....눌러...
					AudioFeedback(VOICE_STOP,	gcbBuzNum,VOL_CHECK);//(음성) into음 
					gbModePrcsStep = 10;
					break;
				
				case TENKEY_STAR:
					// 추가 등록
					gbModePrcsStep = 0; 	
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_CHECK);//(음성) //Swipe the finger print 3 times.
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(GetModeTimeOut() == 0)
					{
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
				
				default:
					break;
			}
			break;

		case 10:
			if(GetBuzPrcsStep())	break;
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			BioOffModeClear();
			break;

		case 20:
			SetModeTimeOut(10);
			HFPMTxEventPermanentKey();
			gbModePrcsStep = 21;
			break;

		case 21:
			if(GetModeTimeOut() == 0){
				FeedbackError(AVML_TIMEOUT, VOL_CHECK); 	//	times up
				gbModePrcsStep = 200;
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == IREVO_HFPM_SUB_EVENT_PERMANENT_KEY &&
					gbFingerPrint_rxd_buf[5] == 0x01 && gbFingerPrint_rxd_buf[6] == 0x00)
				{
					HFPMSavePermanentKey();

					bTmp = gbFID;
					gbFID--; //slot number 를 저장할 ROM index 처리 
					no_fingers++; // 등록 수 증가..
					RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
					RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
					ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, ConvertFingerprintSlotNumber(gbFID));
#endif

					RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
					no_fingers = FingerReadBuff[0];

					memset(bTmpArray, 0x00, 8);
					CopyCredentialDataForPack(bTmpArray, 8);
					PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)bTmp);

#ifdef	BLE_N_SUPPORT
					if(no_fingers >= _MAX_REG_BIO ||gbEnterMode)
#else 
					if(no_fingers >= _MAX_REG_BIO)
#endif 						
					{
						gbEnterMode = 0x00;
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					else 
					{
						FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);	 //추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
						gbModePrcsStep = 7;
						SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					}
				}
				else
				{
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 	//	times up
					gbModePrcsStep = 200;
				}
			}
			break;


		case 30:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ModeGotoOneFingerRegister();
			break;

		case 40:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ModeGotoOneFingerRegDelCheck();
			break;

		case 200:		 
		default:			
			BioOffModeClear();
			break;
	}
}



void FingerSlotNumberToDelete(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();		//slot number는 1~20
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= _MAX_REG_BIO))
	{
		TenKeyVariablesClear();
			gbModePrcsStep++;
#ifdef	BLE_N_SUPPORT
		if(gbEnterMode == 0x00)
#endif 	
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);	//# 버튼에 대한 효과음
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}
}



void ModeOneFingerDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_FOR_DEL_SHARP, VOL_CHECK);			//삭제를 원하시는 사용자 번호를 입력하세요. 계속하시려면 샵 버튼을 누르세요.
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gSelectedSlotNumber = 0;
			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(FingerSlotNumberToDelete, BackwardFunctionForOneFingerRegDel,0);
			break;

		case 2:
			HFPMTxEventDelete(gSelectedSlotNumber);
			SetModeTimeOut(_MODE_TIME_OUT_20S);
			gbModePrcsStep++;
			break;

		case 3:
			if(GetModeTimeOut() == 0){
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == IREVO_HFPM_SUB_EVENT_FP_DELETE)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case IREVO_HFPM_RESULT_CODE_STATUS_OK:	//명령 성공 
							gbModePrcsStep = 4;
							break;		

						case IREVO_HFPM_RESULT_CODE_PACKET_ERR:	
						case IREVO_HFPM_RESULT_CODE_CHECK_SUM_ERR:	
						case IREVO_HFPM_RESULT_CODE_SLOT_EMPTY:	
						default:
							FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}				
			}
			break;
		
		case 4:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			no_fingers--;
			RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
			FingerWriteBuff[0]=0xff;
			FingerWriteBuff[1]=0xff;
			gbFID = gSelectedSlotNumber;
			gbFID--;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, ConvertFingerprintSlotNumber(gbFID));
#endif

			RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	

			DDLStatusFlagClear(DDL_STS_FAIL_BIO);
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gbModePrcsStep = 5;
			break;

		case 5:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;			//버튼음 마침을 기다림

			PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R2, VOL_CHECK);		//완료되었습니다. 다른 설정을 원하시면 별표를 누르시고, 종료를 원하시면 R버튼을 누르세요.
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
#ifdef	BLE_N_SUPPORT
			if(gbEnterMode)
			{
				gbEnterMode = 0x00;
				FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
				BioOffModeClear();
			}
			else 
#endif 
			{
			gbModePrcsStep++;
			}
			break;

		case 6:			// 등록 연속  or 종료...시..
			switch(gbInputKeyValue)
			{
				case FUNKEY_REG:
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					BioOffModeClear();
					break;
				case TENKEY_STAR:
					// 추가 삭제
					gbModePrcsStep = 0; 	
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_CHECK);
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(gModeTimeOutTimer100ms == 0){
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
				
				default:
					break;
			}
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
		default:
			BioOffModeClear();
			break;
	}
}

