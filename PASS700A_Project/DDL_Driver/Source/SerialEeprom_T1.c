//------------------------------------------------------------------------------
/** 	@file		SerialEeprom_T1.c
	@version 0.1.04
	@date	2016.05.02
	@brief	Doorlock EEPROM 24C08 Interface Functions
	@see	MX_i2c_lapper.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00	2016.03.29		by Jay
			- 24C08 EEPROM 구동에 적합하도록 설계
			- 모든 마이컴에 사용될 수 있도록 설계
			- WRITE 수행 시 PAGE 단위 생각할 필요 없게 하기 위해  자동 계산하도록 설계

		V0.1.01 2016.03.30		by Youn
			- 기존에 RomWrite, RomRead 함수 내에서 선언하여 사용하던 것을 
			        ddl_i2c_t ddl_eeprom_i2c_dev; 전역 변수로 처리하여 추가 
			- slaveid가 ddl_i2c_t에 인자로 포함되어 ddl_i2c_read, ddl_i2c_read 함수에서 관련 내용 수정

		V0.1.02 2016.03.30		by Jay
			- RomWrite, RomRead 함수에서 ddl_i2c_write, ddl_i2c_read 사용시 address를 (wAdrs & 0xFF)처리하여 
		               보내던 부분을 wAdrs로 수정, slaveid 구현 방법이 변경되면서 해당 내용도 변경 필요 
		       - I2C 설정 부분이 RomInitial에 포함되었기 때문에 해당 함수를 반드시 초기에 수행 필요 

		V0.1.03 2016.04.15		by Jay
			- RomWriteWithSameData 함수 추가
				특정 영역을 동일한 Data로 채우고자 할 경우 사용
				예를 들면, 00h나 FFh로 모두 채울 때 사용 가능
		       - AutoUnitTest_SerialEeprom에 TEST4 내용 추가 및 내용 정리		
		       	테스트 함수는 필요할 때만 실행하도록 처리 

		V0.1.04 2016.05.02		by Jay
			- RomWrite 및 RomWriteWithSameData 함수에서 Data와 Address가 Write Page 단위보다 작을 경우
			  Data Size를 잘못 계산하는 문제 수정 (예를 들면, Address가 0x02이고, 1byte를 쓸 경우 3byte를 쓰는 것으로 
			  잘못 연산되어 처리됨.
			- 위 수정 내용 테스트를 위해 AutoUnitTest_SerialEeprom에 TEST5, TEST6 내용 추가
*/
//------------------------------------------------------------------------------

#include	"Main.h"

uint32_t gwRomWriteTimerInitCnt = 0;	/**< Waiting EEPROM Writing Time */

ddl_i2c_t ddl_eeprom_i2c_dev;

#ifdef EX_EEPROM
#define PAGE_WRITE_BYTE		128
#define EEPROM_MAX_ADDRESS	0x3FFFFFF //0x3FF	
#endif 

#ifdef _USE_ST32_EEPROM_


#define WRITE_UNIT 0x04 // 4byte 


#if defined	(DDL_CFG_WATCHDOG) && defined (_USE_ST32_EEPROM_)
void SetReadProtect(void)
{
	FLASH_OBProgramInitTypeDef OBdata;
	
	HAL_FLASHEx_OBGetConfig(&OBdata);
	OBdata.RDPLevel = OB_RDP_LEVEL_0;
	OBdata.OptionType = OPTIONBYTE_RDP;
	HAL_FLASH_OB_Unlock();	
	HAL_FLASHEx_OBProgram(&OBdata);
	HAL_FLASH_OB_Launch();		
	HAL_FLASH_OB_Lock();
}
#endif 

void RomInitial(void)
{
#ifdef EX_EEPROM
	BYTE bBuf;
	BYTE bTmp;

	ddl_eeprom_i2c_dev.type = DDL_I2C_MCU_TYPE1;
	ddl_eeprom_i2c_dev.slave_id = I2C_EEPROM_ADDRESS;
	ddl_eeprom_i2c_dev.dev.mcu.hi2c = gh_mcu_i2c;

	RomRead(&bTmp, (WORD)EX_ROM_TEST, 1);
	bBuf = bTmp + 5;
	RomWrite(&bBuf, (WORD)EX_ROM_TEST, 1);            
	RomRead(&bTmp, (WORD)EX_ROM_TEST, 1);


	// 에러 Flag 처리 내용 추가 필요
	gfEepromErr = 0;
	if(bBuf != bTmp)
	{
		gfEepromErr = 1;
	}
#else 
#ifndef RTC_PCF85063
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = I2C_SCL_Pin | I2C_SDA_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(I2C_SDA_GPIO_Port, &GPIO_InitStruct);
	gfEepromErr = 0;
#endif 	
#if 0
	RomRead(&bTmp, (WORD)ROM_TEST, 1);
	bBuf = bTmp + 5;
	RomWrite(&bBuf, (WORD)ROM_TEST, 1);            
	RomRead(&bTmp, (WORD)ROM_TEST, 1);

	// 에러 Flag 처리 내용 추가 필요
	gfEepromErr = 0;
	if(bBuf != bTmp)
	{
		gfEepromErr = 1;
	}
#endif 	
#endif 
}

#ifdef EX_EEPROM
void ExRomPageWrite(BYTE *bpData, WORD wAdrs, BYTE bNum)
{
	BYTE bTmp;

	if(bNum == 0 || gfEepromErr)
		return;

	while(1)
	{
		bTmp = SysTimerEndCheck(gwRomWriteTimerInitCnt, SYS_TIMER_10MS);
		if(bTmp)		break;
	}

	ddl_i2c_write(&ddl_eeprom_i2c_dev, wAdrs, I2C_MEMADD_SIZE_16BIT, bpData, bNum);

	//Write Cycle Time (~10ms)
	SysTimerStart(&gwRomWriteTimerInitCnt);		// 10ms Waiting before next access 			
}
#endif 

void RomWrite(BYTE *bpData, WORD wAdrs, WORD wNum)
{
#ifdef EX_EEPROM

	if(wAdrs & EX_EEPROM)
	{
		WORD wCnt;
		WORD wPage;
		BYTE bByte;
		WORD wTmp;

		wAdrs &= ~(EX_EEPROM);
		
		// Writing 할 Data가 Max Address 범위를 넘어 갈 경우 Writing 수행하지 않음
		if((wAdrs+wNum) > EEPROM_MAX_ADDRESS || gfEepromErr)
		{
			//에러 처리 필요 ??
			return;
		}	

		if(wAdrs == 0)
		{
			wPage = wNum /PAGE_WRITE_BYTE;
			bByte = (BYTE)(wNum % PAGE_WRITE_BYTE);

			// Writing Data가 Page 단위보다 많은 경우 Page 단위로 분할하여 Writing 수행
			for(wCnt = 0; wCnt < wPage; wCnt++)
			{
				ExRomPageWrite(bpData, wAdrs, PAGE_WRITE_BYTE);
				bpData += PAGE_WRITE_BYTE;
				wAdrs += PAGE_WRITE_BYTE;
			}
		}
		else
		{
			wTmp = wAdrs /PAGE_WRITE_BYTE;
			wPage = (wAdrs+wNum)/PAGE_WRITE_BYTE;
			if(wPage > wTmp)
			{
				bByte = (BYTE)((wAdrs+wNum) % PAGE_WRITE_BYTE);
				
				// Page 단위보다 작은 Byte 먼저 분할하여 Writing 수행
				wTmp++;
				ExRomPageWrite(bpData, wAdrs, (BYTE)((wTmp*PAGE_WRITE_BYTE)-wAdrs));
				bpData += (BYTE)((wTmp*PAGE_WRITE_BYTE)-wAdrs); 
				wAdrs += ((wTmp*PAGE_WRITE_BYTE)-wAdrs);

				// Writing Data가 Page 단위보다 많은 경우 Page 단위로 분할하여 Writing 수행
				for(wCnt = 0; wCnt < (wPage-wTmp); wCnt++)
				{
					ExRomPageWrite(bpData, wAdrs, PAGE_WRITE_BYTE);
					bpData += PAGE_WRITE_BYTE;
					wAdrs += PAGE_WRITE_BYTE;
				}
			}
			else
			{
				bByte = (BYTE)wNum;
			}	
		}

		// 남은 Page 단위보다 작은 Byte Writing 수행
		ExRomPageWrite(bpData, wAdrs, bByte);
	}		
	else
	{
		WORD wCnt;
		DWORD address = (wAdrs + FLASH_EEPROM_BASE);
		WORD wTmp;
		WORD wTmp1;
		DWORD wData; 

#ifdef	P_VOICE_RST	
		if(gh_mcu_tim_voice->State == HAL_TIM_STATE_BUSY)
		{
			Delay(SYS_TIMER_100MS);
		}
#endif 

		// STM32 EEPROM 주소를 넘어서는 경우 RESET 발생 하므로 예외 처리 
		if(address > FLASH_EEPROM_END || wNum == 0 ||((address+(wNum-1)) > FLASH_EEPROM_END))
			return ;

		wTmp = wNum / WRITE_UNIT; // 4byte 
		wTmp1 = wNum % WRITE_UNIT; // 나머지 

		// 4byte 단위 write 
		if(wTmp)
		{
			for(wCnt = 0; wCnt < wTmp ; wCnt++)
			{
				wData = (DWORD)((*(bpData+3) << 24) |(*(bpData+2) << 16)  |(*(bpData+1) << 8)  |*bpData);			
				HAL_FLASHEx_DATAEEPROM_Unlock(); //속도 차가 안나서 딱 쓸때만 unlock 하도록 
				if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,wData) != HAL_OK)
				{
					HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,wData);
				}
				HAL_FLASHEx_DATAEEPROM_Lock();			
				address += WRITE_UNIT;
				bpData += WRITE_UNIT;
			}
		}

		if(wTmp1)
		{
			for(wCnt = 0; wCnt < wTmp1 ; wCnt++)
			{
				HAL_FLASHEx_DATAEEPROM_Unlock();		
				if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTBYTE,address,*bpData)!= HAL_OK)
				{
					HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTBYTE,address,*bpData);
				}
				HAL_FLASHEx_DATAEEPROM_Lock();						
				bpData++;
				address++;
			}
		}
	}	
#else 
	WORD wCnt;
	DWORD address = (wAdrs + FLASH_EEPROM_BASE);
	WORD wTmp;
	WORD wTmp1;
	DWORD wData; 

#ifdef P_VOICE_RST	
	if(gh_mcu_tim_voice->State == HAL_TIM_STATE_BUSY)
	{
		Delay(SYS_TIMER_100MS);
	}
#endif 

	// STM32 EEPROM 주소를 넘어서는 경우 RESET 발생 하므로 예외 처리 
	if(address > FLASH_EEPROM_END || wNum == 0 ||((address+(wNum-1)) > FLASH_EEPROM_END))
		return ;

	wTmp = wNum / WRITE_UNIT; // 4byte 
	wTmp1 = wNum % WRITE_UNIT; // 나머지 

	// 4byte 단위 write 
	if(wTmp)
	{
		for(wCnt = 0; wCnt < wTmp ; wCnt++)
		{
			wData = (DWORD)((*(bpData+3) << 24) |(*(bpData+2) << 16)  |(*(bpData+1) << 8)  |*bpData);			
			HAL_FLASHEx_DATAEEPROM_Unlock(); //속도 차가 안나서 딱 쓸때만 unlock 하도록 
			if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,wData) != HAL_OK)
			{
				HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,wData);
			}
			HAL_FLASHEx_DATAEEPROM_Lock();			
			address += WRITE_UNIT;
			bpData += WRITE_UNIT;
		}
	}

	if(wTmp1)
	{
		for(wCnt = 0; wCnt < wTmp1 ; wCnt++)
		{
			HAL_FLASHEx_DATAEEPROM_Unlock();		
			if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTBYTE,address,*bpData)!= HAL_OK)
			{
				HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTBYTE,address,*bpData);
			}
			HAL_FLASHEx_DATAEEPROM_Lock();						
			bpData++;
			address++;
		}
	}
#endif 
}	

void RomRead(BYTE *bpData, WORD wAdrs, WORD wNum)
{
#ifdef EX_EEPROM
	if(wAdrs & EX_EEPROM)
	{
		/* 외부 EEPROM */
		BYTE bTmp;

		if(gfEepromErr)
			return;

		wAdrs &= ~(EX_EEPROM);

		// Read 할 Data가 Max Address 범위를 넘어 갈 경우 Read 수행하지 않음
		if((wAdrs+wNum) > EEPROM_MAX_ADDRESS)
		{
			//에러 처리 필요 ??
			return;
		}	

		while(1)
		{
			bTmp = SysTimerEndCheck(gwRomWriteTimerInitCnt, SYS_TIMER_10MS);
			if(bTmp)		break;
		}

		ddl_i2c_read(&ddl_eeprom_i2c_dev, wAdrs, I2C_MEMADD_SIZE_16BIT, bpData, wNum);
	}	
	else
	{
		WORD wCnt;
		DWORD address = (wAdrs + FLASH_EEPROM_BASE);
		/* 내부 EEPROM */
		if(address > FLASH_EEPROM_END || wNum == 0 ||((address+(wNum-1)) > FLASH_EEPROM_END))
			return;

		for(wCnt = 0; wCnt < wNum ; wCnt++)
		{
			*bpData = *(__IO uint8_t*)(address);
			bpData++;
			address++;
		}
	}	
#else
	WORD wCnt;
	DWORD address = (wAdrs + FLASH_EEPROM_BASE);

	/* 내부 EEPROM */
	if(address > FLASH_EEPROM_END || wNum == 0 ||((address+(wNum-1)) > FLASH_EEPROM_END))
		return;

	for(wCnt = 0; wCnt < wNum ; wCnt++)
	{
		*bpData = *(__IO uint8_t*)(address);
		bpData++;
		address++;
	}
#endif 
}

void RomWriteWithSameData(BYTE bData, WORD wAdrs, WORD wNum)
{
#ifdef EX_EEPROM
	if(wAdrs & EX_EEPROM)
	{
		WORD wCnt;
		WORD wPage;
		BYTE bByte;
		WORD wTmp;
		BYTE bDataBuf[PAGE_WRITE_BYTE];

		wAdrs &= ~(EX_EEPROM);
		
		// Writing 할 Data가 Max Address 범위를 넘어 갈 경우 Writing 수행하지 않음
		if((wAdrs+wNum) > EEPROM_MAX_ADDRESS || gfEepromErr)
		{
			//에러 처리 필요 ??
			return;
		}	

		memset(bDataBuf, bData, PAGE_WRITE_BYTE);
		
		if(wAdrs == 0)
		{
			wPage = wNum /PAGE_WRITE_BYTE;
			bByte = (BYTE)(wNum % PAGE_WRITE_BYTE);

			// Writing Data가 Page 단위보다 많은 경우 Page 단위로 분할하여 Writing 수행
			for(wCnt = 0; wCnt < wPage; wCnt++)
			{
				ExRomPageWrite(bDataBuf, wAdrs, PAGE_WRITE_BYTE);
				wAdrs += PAGE_WRITE_BYTE;
			}
		}
		else
		{
			wTmp = wAdrs /PAGE_WRITE_BYTE;
			wPage = (wAdrs+wNum)/PAGE_WRITE_BYTE;

			if(wPage > wTmp)
			{
				bByte = (BYTE)((wAdrs+wNum) % PAGE_WRITE_BYTE);

				// Page 단위보다 작은 Byte 먼저 분할하여 Writing 수행
				wTmp++;
				ExRomPageWrite(bDataBuf, wAdrs, (BYTE)((wTmp*PAGE_WRITE_BYTE)-wAdrs));
				wAdrs += ((wTmp*PAGE_WRITE_BYTE)-wAdrs);

				// Writing Data가 Page 단위보다 많은 경우 Page 단위로 분할하여 Writing 수행
				for(wCnt = 0; wCnt < (wPage-wTmp); wCnt++)
				{
					ExRomPageWrite(bDataBuf, wAdrs, PAGE_WRITE_BYTE);
					wAdrs += PAGE_WRITE_BYTE;
				}
			}
			else
			{
				bByte = (BYTE)wNum;
			}	
		}

		// 남은 Page 단위보다 작은 Byte Writing 수행
		ExRomPageWrite(bDataBuf, wAdrs, bByte);
	}
	else
	{
		WORD wCnt;
		DWORD address = (wAdrs + FLASH_EEPROM_BASE);
		WORD wTmp;
		WORD wTmp1;
		DWORD wData; 		

#ifdef	P_VOICE_RST	
		if(gh_mcu_tim_voice->State == HAL_TIM_STATE_BUSY)
		{
			Delay(SYS_TIMER_100MS);
		}
#endif 
		
		if(address > FLASH_EEPROM_END ||wNum == 0 ||((address+(wNum-1)) > FLASH_EEPROM_END))
			return;

		wTmp = wNum / WRITE_UNIT; // 4byte 
		wTmp1 = wNum % WRITE_UNIT; // 나머지 
		wData = (DWORD)((bData << 24) |(bData << 16)  |(bData << 8)  |bData)  ; //same data 니까 어짜피 같은 값이므로 한번만 setting 

		// 4byte 단위 write 
		if(wTmp)
		{
			for(wCnt = 0; wCnt < wTmp ; wCnt++)
			{
#ifdef	DDL_CFG_WATCHDOG
				RefreshIwdg();
#endif			
				HAL_FLASHEx_DATAEEPROM_Unlock();
				if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,wData) != HAL_OK)
				{
					HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,wData);
				}
				HAL_FLASHEx_DATAEEPROM_Lock();				
				address += WRITE_UNIT;
			}
		}

		if(wTmp1)
		{
			for(wCnt = 0; wCnt < wTmp1 ; wCnt++)
			{
#ifdef	DDL_CFG_WATCHDOG
				RefreshIwdg();
#endif			
				HAL_FLASHEx_DATAEEPROM_Unlock();
				if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTBYTE,address,bData) != HAL_OK)
				{
					HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTBYTE,address,bData);
				}
				HAL_FLASHEx_DATAEEPROM_Lock();							
				address++;
			}
		}
	}			
#else 
	WORD wCnt;
	DWORD address = (wAdrs + FLASH_EEPROM_BASE);
	WORD wTmp;
	WORD wTmp1;
	DWORD wData; 

#ifdef	P_VOICE_RST	
	if(gh_mcu_tim_voice->State == HAL_TIM_STATE_BUSY)
	{
		Delay(SYS_TIMER_100MS);
	}
#endif 
	
	if(address > FLASH_EEPROM_END ||wNum == 0 ||((address+(wNum-1)) > FLASH_EEPROM_END))
		return;

	wTmp = wNum / WRITE_UNIT; // 4byte 
	wTmp1 = wNum % WRITE_UNIT; // 나머지 
	wData = (DWORD)((bData << 24) |(bData << 16)  |(bData << 8)  |bData)  ; //same data 니까 어짜피 같은 값이므로 한번만 setting 

	// 4byte 단위 write 
	if(wTmp)
	{
		for(wCnt = 0; wCnt < wTmp ; wCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif			
			HAL_FLASHEx_DATAEEPROM_Unlock();
			if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,wData) != HAL_OK)
			{
				HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,wData);
			}
			HAL_FLASHEx_DATAEEPROM_Lock();				
			address += WRITE_UNIT;
		}
	}

	if(wTmp1)
	{
		for(wCnt = 0; wCnt < wTmp1 ; wCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif			
			HAL_FLASHEx_DATAEEPROM_Unlock();
			if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTBYTE,address,bData) != HAL_OK)
			{
				HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTBYTE,address,bData);
			}
			HAL_FLASHEx_DATAEEPROM_Lock();							
			address++;
		}
	}
#endif 	
}	


#if 0
BYTE gbTestDataBuf[0x1000];		/**< Writing Test Buffer */
BYTE gbTestTmpBuf[0x1000];		/**< Reading Test Buffer */
BYTE Error;

#define ERROR_ADDRESS 0xFFF

BYTE AutoUnitTest_SerialEepromForST_EEPROM(void)
{
	WORD wCnt;
	WORD RandSeed;
	WORD RandAddress = 0x0000;
	static BYTE Flag = 0x00;
	WORD i = 0x00;

// TEST 4 대용량 테스트 
	memset(gbTestDataBuf,0x00, 0x1000);
	memset(gbTestTmpBuf,0x00, 0x1000);

	for( i = 0 ; i <  0xFFF ; i++)
	{
		BYTE Temp;

		Temp = (i%3);
		
		if(Temp == 1)
			gbTestDataBuf[i] = 0xFF;
		else if(Temp == 2)
			gbTestDataBuf[i] = 0xAA;
		else 
			gbTestDataBuf[i] = 0xBB;
	}

	RomWrite(gbTestDataBuf,0x00,0xFFF);   
	RomRead(gbTestTmpBuf,0x00,0xFFF);

	if(memcmp(gbTestDataBuf, gbTestTmpBuf,0xFFF) !=0)
	{
		RomRead(&Error,ERROR_ADDRESS,1);
		Error++;
		RomWrite(&Error,ERROR_ADDRESS,1);
		Flag = 1;
	}
	RandSeed = SystickLoad();
	srand(RandSeed);

#if 0
	// TEST 1 대용량 테스트 
	for(wCnt = 0; wCnt < 0xFFB; wCnt++)
	{
		gbTestDataBuf[wCnt] = (BYTE)wCnt;
		gbTestTmpBuf[wCnt] = 0;
	}	

	RomWrite(gbTestDataBuf, 0, 0xFFB);
	RomRead(gbTestTmpBuf, 0, 0xFFB);

	if(memcmp(gbTestDataBuf, gbTestTmpBuf, 1))
	{
		RomRead(&Error,ERROR_ADDRESS,1);
		Error++;
		RomWrite(&Error,ERROR_ADDRESS,1);
	}		
#endif 
	// TEST 2 RAND write 
	for(BYTE i = 1 ; i <  200 ; i++)
	{
		memset(gbTestDataBuf,0x00, 0x1000);
		memset(gbTestTmpBuf,0x00, 0x1000);
		
		RandAddress = (rand() % 0xF30);
		memset(gbTestDataBuf,(BYTE)RandAddress, i);

#ifdef	DDL_CFG_WATCHDOG
		RefreshIwdg();
#endif
	
		RomWrite(gbTestDataBuf,RandAddress, i);
		RomRead(gbTestTmpBuf,RandAddress, i);

		if(memcmp(gbTestDataBuf, gbTestTmpBuf,i))
		{
			RomRead(&Error,ERROR_ADDRESS,1);
			Error++;
			RomWrite(&Error,ERROR_ADDRESS,1);
			Flag = 1;
		}
	}

	if(!Flag)
		BuzzerSetting(gcbBuzNum, VOL_CHECK);	
	else 
		BuzzerSetting(gcbBuzErr, VOL_CHECK);
		
#if 0
	RomWriteWithSameData(0xFF, 0, 0xFFB);
	RomRead(gbTestTmpBuf, 0, 0xFFB);
	
	if(DataCompare(gbTestTmpBuf, 0xFF, 0xFFB) == _DATA_NOT_IDENTIFIED)
	{
		RomRead(&Error,ERROR_ADDRESS,1);
		Error++;
		RomWrite(&Error,ERROR_ADDRESS,1);
	}		
#endif 

	return 1;
}
#endif 

#else 
//------------------------------------------------------------------------------
/** 	@brief	EEPROM Read/Write Test 
	@param 	 
	@return 	없음
	@remark	Reset 이후 EEPROM 통신 테스트를 위해 지정된 번지에 Access
	@remark 1. Read the value from 'ROM_TEST'
	@remark 2. Add 5 to the value
	@remark 3. Write the value to 'ROM_TEST'
	@remark 4. Read the value from 'ROM_TEST'
	@remark 5. Compare between the values checking if both are the same.
	@remark I2C 설정 부분이 포함되어 있기 때문에 다른 함수 사용을 위해서는 반드시 해당 함수를 실행 초기에 수행해 주어야 함
*/
//------------------------------------------------------------------------------
void RomInitial(void)
{
	BYTE bBuf;
	BYTE bTmp;

	ddl_eeprom_i2c_dev.type = DDL_I2C_MCU_TYPE1;
	ddl_eeprom_i2c_dev.slave_id = I2C_EEPROM_ADDRESS;
	ddl_eeprom_i2c_dev.dev.mcu.hi2c = gh_mcu_i2c;

	RomRead(&bTmp, (WORD)ROM_TEST, 1);
	bBuf = bTmp + 5;
	RomWrite(&bBuf, (WORD)ROM_TEST, 1);            
	RomRead(&bTmp, (WORD)ROM_TEST, 1);


	// 에러 Flag 처리 내용 추가 필요
	gfEepromErr = 0;
	if(bBuf != bTmp)
	{
		gfEepromErr = 1;
	}
}

// 16 byte 단위 PAGE Write 가능
#define PAGE_WRITE_BYTE		16
#define EEPROM_MAX_ADDRESS	0x3FF	

//------------------------------------------------------------------------------
/** 	@brief	Page Write Data to EEPROM
	@param 	[Input] bpData : Write할 Data Buffer Pointer
	@param 	[Input] wAdrs : Write할 EEPROM  Start Address
	@param 	[Input] bNum : Write할 Data 개수 ( BYTE 단위 )
	@return 	없음
	@remark	EEPROM 특정 번지에 원하는 Data들을 Write함
	@remark EEPROM Write 후에는 약 12ms 정도의 Writing Time 필요
*/
//------------------------------------------------------------------------------
void RomPageWrite(BYTE *bpData, WORD wAdrs, BYTE bNum)
{
	BYTE bTmp;

	if(bNum == 0)
		return;

	while(1)
	{
		bTmp = SysTimerEndCheck(gwRomWriteTimerInitCnt, SYS_TIMER_10MS);
		if(bTmp)		break;
	}

	ddl_i2c_write(&ddl_eeprom_i2c_dev, wAdrs, I2C_MEMADD_SIZE_8BIT, bpData, bNum);

	//Write Cycle Time (~10ms)
	SysTimerStart(&gwRomWriteTimerInitCnt);		// 10ms Waiting before next access 			
}



//------------------------------------------------------------------------------
/** 	@brief	Write Data to EEPROM
	@param 	[Input] bpData : Write할 Data Buffer Pointer
	@param 	[Input] wAdrs : Write할 EEPROM Address
	@param 	[Input] wNum : Write할 Data 개수 ( BYTE 단위 )
	@return 	없음
	@remark	EEPROM 특정 번지에 원하는 Data들을 Write함
	@remark 입력된 Data를 자동으로 Page 단위로 나누어 Writing함
*/
//------------------------------------------------------------------------------
void RomWrite(BYTE *bpData, WORD wAdrs, WORD wNum)
{
	WORD wCnt;
	WORD wPage;
	BYTE bByte;
	WORD wTmp;
	
	// Writing 할 Data가 Max Address 범위를 넘어 갈 경우 Writing 수행하지 않음
	if((wAdrs+wNum) > EEPROM_MAX_ADDRESS)
	{
		//에러 처리 필요 ??
		return;
	}	

	if(wAdrs == 0)
	{
		wPage = wNum /PAGE_WRITE_BYTE;
		bByte = (BYTE)(wNum % PAGE_WRITE_BYTE);

		// Writing Data가 Page 단위보다 많은 경우 Page 단위로 분할하여 Writing 수행
		for(wCnt = 0; wCnt < wPage; wCnt++)
		{
			RomPageWrite(bpData, wAdrs, PAGE_WRITE_BYTE);
			bpData += PAGE_WRITE_BYTE;
			wAdrs += PAGE_WRITE_BYTE;
		}
	}
	else
	{
		wTmp = wAdrs /PAGE_WRITE_BYTE;
		wPage = (wAdrs+wNum)/PAGE_WRITE_BYTE;
		if(wPage > wTmp)
		{
			bByte = (BYTE)((wAdrs+wNum) % PAGE_WRITE_BYTE);
			
			// Page 단위보다 작은 Byte 먼저 분할하여 Writing 수행
			wTmp++;
			RomPageWrite(bpData, wAdrs, (BYTE)((wTmp*PAGE_WRITE_BYTE)-wAdrs));
			bpData += (BYTE)((wTmp*PAGE_WRITE_BYTE)-wAdrs); 
			wAdrs += ((wTmp*PAGE_WRITE_BYTE)-wAdrs);

			// Writing Data가 Page 단위보다 많은 경우 Page 단위로 분할하여 Writing 수행
			for(wCnt = 0; wCnt < (wPage-wTmp); wCnt++)
			{
				RomPageWrite(bpData, wAdrs, PAGE_WRITE_BYTE);
				bpData += PAGE_WRITE_BYTE;
				wAdrs += PAGE_WRITE_BYTE;
			}
		}
		else
		{
			bByte = (BYTE)wNum;
		}	
	}

	// 남은 Page 단위보다 작은 Byte Writing 수행
	RomPageWrite(bpData, wAdrs, bByte);
}	


//------------------------------------------------------------------------------
/** 	@brief	Write Data to EEPROM
	@param 	[Input] bpData : Read한 Data를 저장할  Buffer Pointer
	@param 	[Input] wAdrs : Read할 EEPROM Start Address
	@param 	[Input] wNum : Read할 Data 개수 ( WORD 단위 )
	@return 	없음
	@remark	EEPROM 특정 번지의 Data들을 Read함
*/
//------------------------------------------------------------------------------
void RomRead(BYTE *bpData, WORD wAdrs, WORD wNum)
{
	BYTE bTmp;

	// Read 할 Data가 Max Address 범위를 넘어 갈 경우 Read 수행하지 않음
	if((wAdrs+wNum) > EEPROM_MAX_ADDRESS)
	{
		//에러 처리 필요 ??
		return;
	}	

	while(1)
	{
		bTmp = SysTimerEndCheck(gwRomWriteTimerInitCnt, SYS_TIMER_10MS);
		if(bTmp)		break;
	}

	ddl_i2c_read(&ddl_eeprom_i2c_dev, wAdrs, I2C_MEMADD_SIZE_8BIT, bpData, wNum);
}


//------------------------------------------------------------------------------
/** 	@brief	Write Same Data to EEPROM
	@param 	[Input] bData : Write할 Data
	@param 	[Input] wAdrs : Write할 EEPROM Address
	@param 	[Input] wNum : Write할 Data 개수 ( BYTE 단위 )
	@return 	없음
	@remark	EEPROM 특정 번지에 원하는 하나의 값인 Data를 Write함
	@remark 입력된 Data를 자동으로 Page 단위로 나누어 Writing함
	@remark 예를 들면, 특정 영역을 모두 00h나 FFh로 채우고자 할 경우에 사용
*/
//------------------------------------------------------------------------------
void RomWriteWithSameData(BYTE bData, WORD wAdrs, WORD wNum)
{
	WORD wCnt;
	WORD wPage;
	BYTE bByte;
	WORD wTmp;
	BYTE bDataBuf[PAGE_WRITE_BYTE];
	
	// Writing 할 Data가 Max Address 범위를 넘어 갈 경우 Writing 수행하지 않음
	if((wAdrs+wNum) > EEPROM_MAX_ADDRESS)
	{
		//에러 처리 필요 ??
		return;
	}	

	memset(bDataBuf, bData, PAGE_WRITE_BYTE);
	
	if(wAdrs == 0)
	{
		wPage = wNum /PAGE_WRITE_BYTE;
		bByte = (BYTE)(wNum % PAGE_WRITE_BYTE);

		// Writing Data가 Page 단위보다 많은 경우 Page 단위로 분할하여 Writing 수행
		for(wCnt = 0; wCnt < wPage; wCnt++)
		{
			RomPageWrite(bDataBuf, wAdrs, PAGE_WRITE_BYTE);
			wAdrs += PAGE_WRITE_BYTE;
		}
	}
	else
	{
		wTmp = wAdrs /PAGE_WRITE_BYTE;
		wPage = (wAdrs+wNum)/PAGE_WRITE_BYTE;

		if(wPage > wTmp)
		{
			bByte = (BYTE)((wAdrs+wNum) % PAGE_WRITE_BYTE);

			// Page 단위보다 작은 Byte 먼저 분할하여 Writing 수행
			wTmp++;
			RomPageWrite(bDataBuf, wAdrs, (BYTE)((wTmp*PAGE_WRITE_BYTE)-wAdrs));
			wAdrs += ((wTmp*PAGE_WRITE_BYTE)-wAdrs);

			// Writing Data가 Page 단위보다 많은 경우 Page 단위로 분할하여 Writing 수행
			for(wCnt = 0; wCnt < (wPage-wTmp); wCnt++)
			{
				RomPageWrite(bDataBuf, wAdrs, PAGE_WRITE_BYTE);
				wAdrs += PAGE_WRITE_BYTE;
			}
		}
		else
		{
			bByte = (BYTE)wNum;
		}	
	}

	// 남은 Page 단위보다 작은 Byte Writing 수행
	RomPageWrite(bDataBuf, wAdrs, bByte);
}	
#endif 

//=============================================================
//	TEST FUNCTIONS
//=============================================================

// 테스트 외에는 아래 내용은 모두 주석 처리 가능

#if 0

BYTE gbTestDataBuf[0x3FF];		/**< Writing Test Buffer */
BYTE gbTestTmpBuf[0x3FF];		/**< Reading Test Buffer */


//------------------------------------------------------------------------------
/** 	@brief	Test Function for EEPROM Routine
	@param 	없음
	@return 	[1] 테스트 성공
	@return 	[0] 테스트 실패
	@remark	EEPROM 함수들이 정상 동작하는지 확인하는 함수, 별도 테스트 과정에서만 사용
	@remark TEST 1 : 시작번지 0번지부터 1023개의 Data가 사용 함수에 의해 제대로 Write/Read 되는지 확인
	@remark TEST 2 : 시작번지 5번지부터 1013개의 Data가 사용 함수에 의해 제대로 Write/Read 되는지 확인
	@remark TEST 3 : 시작번지와 Data의 개수가 1023이 넘을 경우 사용 함수에 의해 제대로 Write/Read 처리하지 않는지 확인
	@remark TEST 4 : 시작번지 0번지부터 1023개의 Data가 모두 FFh로 제대로 저장되는지 확인
	@remark TEST 5 : Data의 개수가 1개일 때 사용 함수에 의해 제대로 Write/Read 처리하지 않는지 확인
	@remark TEST 6 : Data의 개수가 10개이며, Page 단위를 넘어갈 경우 사용 함수에 의해 제대로 Write/Read 처리하지 않는지 확인
	@remark TEST 함수 : RomWrite, RomRead, RomWriteWithSameData
*/
//------------------------------------------------------------------------------
BYTE AutoUnitTest_SerialEeprom(void)
{
	WORD wCnt;

	// TEST 1
	for(wCnt = 0; wCnt < 0x3FF; wCnt++)
	{
		gbTestDataBuf[wCnt] = (BYTE)wCnt;
		gbTestTmpBuf[wCnt] = 0;
	}	

	RomWrite(gbTestDataBuf, 0, 0x3FF);
	RomRead(gbTestTmpBuf, 0, 0x3FF);

	if(memcmp(gbTestDataBuf, gbTestTmpBuf, 0x3FF))
	{
		return 0;
	}		

	// TEST 2
	memset(gbTestTmpBuf, 0x00, 0x3FF);

	RomWrite(gbTestDataBuf, 5, (0x3FF-10));
	RomRead(gbTestTmpBuf, 5, (0x3FF-10));

	if(memcmp(gbTestDataBuf, gbTestTmpBuf, (0x3FF-10)))
	{
		return 0;
	}		

	memset(gbTestTmpBuf, 0x00, 0x3FF);

	// TEST 3
	RomWrite(gbTestDataBuf, 1, 0x3FF);
	RomRead(gbTestTmpBuf, 1, 0x3FF);

	if(memcmp(gbTestDataBuf, gbTestTmpBuf, 0x3FF) == 0)
	{
		return 0;
	}		

	// TEST 4
	memset(gbTestTmpBuf, 0x00, 0x3FF);

	RomWriteWithSameData(0xFF, 0, 0x3FF);
	RomRead(gbTestTmpBuf, 0, 0x3FF);
	
	if(DataCompare(gbTestTmpBuf, 0xFF, 0x3FF) == _DATA_NOT_IDENTIFIED)
	{
		return 0;
	}		


	// TEST 5
	memset(gbTestTmpBuf, 0x00, 0x3FF);

	RomRead(gbTestTmpBuf, 0, 16);

	RomWrite(&gbTestDataBuf[10], 2, 1);
	RomRead(gbTestTmpBuf+16, 0, 16);
	
	if(gbTestTmpBuf[16+2] != gbTestDataBuf[10])
	{
		return 0;
	}		

	if(memcmp(gbTestTmpBuf, gbTestTmpBuf+16, 2) == 1)
	{
		return 0;
	}		

	if(memcmp(gbTestTmpBuf+3, gbTestTmpBuf+16+3, 13) == 1)
	{
		return 0;
	}		

	// TEST 6
	memset(gbTestTmpBuf, 0x00, 0x3FF);

	RomRead(gbTestTmpBuf, 0, 32);

	RomWrite(&gbTestDataBuf[10], 9, 10);
	RomRead(gbTestTmpBuf+32, 0, 32);
	
	if(memcmp(&gbTestTmpBuf[32+9], &gbTestDataBuf[10], 10) == 1)
	{
		return 0;
	}		

	if(memcmp(gbTestTmpBuf, gbTestTmpBuf+32, 8) == 1)
	{
		return 0;
	}		

	if(memcmp(gbTestTmpBuf+19, gbTestTmpBuf+32+19, 13) == 1)
	{
		return 0;
	}		


	return 1;
}

#endif


