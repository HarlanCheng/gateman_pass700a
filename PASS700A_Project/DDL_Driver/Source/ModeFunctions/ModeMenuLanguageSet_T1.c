//------------------------------------------------------------------------------
/** 	@file		ModeMenuLanguageSet_T1.c
	@version 0.1.01
	@date	2016.11.21
	@brief	Language Setting 
	@remark	언어 설정 모드 
	@see	ModeMenuMainSelect_T1.c
	@see	ModeMenuLockSetting_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.08.12		by hyojoon_20160802
		Ddl_config-xx.h  에 DDL_CFG_LANGUAGE_SET_TYPE1 define 을 선언 해야 한다. 
		DDL_CFG_LANGUAGE_SET_TYPE1 은 프로그램 사양서-J0-메뉴 모드-Advanced Mode 참고  
		DDL_CFG_LANGUAGE_SET_TYPE1
		1.LANGUAGE_KOREAN 1. LANGUAGE_CHINESE 		
		DDL_CFG_LANGUAGE_SET_TYPE2
		LANGUAGE_KOREAN 
		LANGUAGE_CHINESE
		LANGUAGE_ENGLISH
		LANGUAGE_SPANISH
		LANGUAGE_PORTUGUESE
		LANGUAGE_TAIWANESE

		V0.1.01 2016.11.21		by Jay
		DDL_CFG_LANGUAGE_SET_TYPE1
		-> 한국어, 중국어 음성 지원 (중국 생산 기준으로 중국어 기본)
		DDL_CFG_LANGUAGE_SET_TYPE3 추가
		-> 한국어, 중국어 음성 지원 (한국 생산 기준으로 한국어 기본)
*/
//------------------------------------------------------------------------------

#include "Main.h"

void ModeGotoLanguageSetting(void)
{
	gbMainMode = MODE_MENU_LANGUAGE_SET;
	gbModePrcsStep = 0;
}

void LanguageSetSelected(void)
{
	BYTE bTmpArray[2];
	bTmpArray[0] = 0x05;	
	
	switch(gMenuSelectKeyTempSave)
	{
#if	defined (DDL_CFG_LANGUAGE_SET_TYPE1) || defined (DDL_CFG_LANGUAGE_SET_TYPE3)
		case TENKEY_1: 	
			LanguageSetting(LANGUAGE_KOREAN);
			bTmpArray[1] = LANGUAGE_KOREAN;
			PackTx_MakePacket(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep++;
			break;

		case TENKEY_2:	
			LanguageSetting(LANGUAGE_CHINESE);
			bTmpArray[1] = LANGUAGE_CHINESE;
			PackTx_MakePacket(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep++;
			break;

		case TENKEY_3: 	
			LanguageSetting(LANGUAGE_ENGLISH);
			bTmpArray[1] = LANGUAGE_ENGLISH;			
			PackTx_MakePacket(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);			
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep++;
			break;

		case TENKEY_4:
			LanguageSetting(LANGUAGE_TAIWANESE);
			bTmpArray[1] = LANGUAGE_TAIWANESE;			
			PackTx_MakePacket(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);			
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep++;
			break;
#elif defined DDL_CFG_LANGUAGE_SET_TYPE2
		case TENKEY_1: 	
			LanguageSetting(LANGUAGE_KOREAN);
			bTmpArray[1] = LANGUAGE_KOREAN;			
			PackTx_MakePacket(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);			
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep++;
			break;

		case TENKEY_2:	
			LanguageSetting(LANGUAGE_CHINESE);
			bTmpArray[1] = LANGUAGE_CHINESE;			
			PackTx_MakePacket(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);			
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep++;
			break;

		case TENKEY_3: 	
			LanguageSetting(LANGUAGE_ENGLISH);
			bTmpArray[1] = LANGUAGE_ENGLISH;			
			PackTx_MakePacket(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);			
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep++;
			break;

		case TENKEY_4:	
			LanguageSetting(LANGUAGE_TAIWANESE);
			bTmpArray[1] = LANGUAGE_TAIWANESE;			
			PackTx_MakePacket(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);			
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep++;
			break;

		case TENKEY_5:	
			LanguageSetting(LANGUAGE_SPANISH);
			bTmpArray[1] = LANGUAGE_SPANISH;			
			PackTx_MakePacket(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);			
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep++;
			break;

		case TENKEY_6:
			LanguageSetting(LANGUAGE_PORTUGUESE);
			bTmpArray[1] = LANGUAGE_PORTUGUESE;			
			PackTx_MakePacket(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);			
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep++;
			break;
#endif 
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 이전 모드로 이동
			gbModePrcsStep--;
			break;
	}			
}

void MenuLanguageSetSelectProcess(void)
{
	switch(gbInputKeyValue)
	{

#if	defined (DDL_CFG_LANGUAGE_SET_TYPE1) || defined (DDL_CFG_LANGUAGE_SET_TYPE3)
		case TENKEY_1:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_KOREAN_SHARP,gbInputKeyValue);
			break;

		case TENKEY_2:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_CHINESE_SHARP,gbInputKeyValue);
			break;

		case TENKEY_3:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_ENGLISH_SHARP,gbInputKeyValue);
			break;
		
		case TENKEY_4:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_TAIWANESE_SHARP,gbInputKeyValue);
			break;

#elif defined DDL_CFG_LANGUAGE_SET_TYPE2
		case TENKEY_1:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_KOREAN_SHARP,gbInputKeyValue);
			break;

		case TENKEY_2:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_CHINESE_SHARP,gbInputKeyValue);
			break;

		case TENKEY_3:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_ENGLISH_SHARP,gbInputKeyValue);
			break;

		case TENKEY_4:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_TAIWANESE_SHARP,gbInputKeyValue);			
		break;

		case TENKEY_5:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_SPANISH_SHARP,gbInputKeyValue);			
		break;

		case TENKEY_6:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_PORTGUESE_SHARP,gbInputKeyValue);
		break;
#endif 

		case TENKEY_SHARP:		 //# 버튼음 출력
#ifdef	P_VOICE_RST	
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
				FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuLockSetting();
			break;
			
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;

		default:
			TimeExpiredCheck();
			break;
	}	
}




//------------------------------------------------------------------------------
/** 	@brief	Language Setting Mode
	@param	None
	@return 	None
	@remark  언어 설정 모드 시작
*/
//------------------------------------------------------------------------------
void ModeLanguageSetting(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
#if	defined (DDL_CFG_LANGUAGE_SET_TYPE1) || defined (DDL_CFG_LANGUAGE_SET_TYPE3)
			Feedback1234MenuOn(AVML_IN_LANG_CODE, VOL_CHECK);
#elif defined DDL_CFG_LANGUAGE_SET_TYPE2
			Feedback123456MenuOn(AVML_IN_LANG_CODE, VOL_CHECK);
#endif 
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuLanguageSetSelectProcess();
			break;	
	
		case 2:
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
			{
				if(GetBuzPrcsStep() || GetVoicePrcsStep()) 	break;
			}

			LanguageSetSelected();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoMenuLockSetting);
			break;

		default:
			ModeClear();
			break;
	}
}

