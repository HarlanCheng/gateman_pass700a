//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneRemoconRegDel_T1.c
	@version 0.1.00
	@date	2016.06.09
	@brief	One Remocon Register/Delete Mode
	@remark	개별 리모컨 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.06.09		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


// 실제 메모리에 저장되는게 아니라 별도 배치
#define	SUPPORTED_REMOCON_NUMBER		5
#define	SUPPORTED_BLEN_NUMBER			8


void ModeGotoOneRemoconRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONEREMOCON_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoOneRemoconRegister(void)
{
	gbMainMode = MODE_MENU_ONEREMOCON_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoOneRemoconDelete(void)
{
	gbMainMode = MODE_MENU_ONEREMOCON_DELETE;
	gbModePrcsStep = 0;
}


void OneRemoconModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 리모컨 개별 등록
		case 1: 	
			ModeGotoOneRemoconRegister();
			break;
	
		// 리모컨 개별 삭제
		case 3: 		
			ModeGotoOneRemoconDelete();
			break;
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
		case 8: 		
			ModeGotoOneSubRemoconRegDelCheck();
			break;

#ifdef DDL_CFG_ADD_REMOCON_LINK_FUNCTION
		// 리모컨 등록 신호 출력
		case 9: 		
			ModeGotoLockOpenConnectionRegister();
			break;
#endif 	//DDL_CFG_ADD_REMOCON_LINK_FUNCTION

#endif 	//DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU

		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuOneRemoconModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 리모컨 개별 등록
		case TENKEY_1:			
//			MenuSelectKeyTempSaveNTimeoutReset(22, gbInputKeyValue);	
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
			if(!gfPackTypeiRevoBleN && !gfInnerPackTypeiRevoBleN)
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				gMenuSelectKeyTempSave = TENKEY_NONE; 
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
			else 
			{
//				MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_REMOCON_SHARP, gbInputKeyValue);	// register a remote control, press the hash to continue.	
				MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_BT_SHARP , gbInputKeyValue);// 블루투스 등록 모드 입니다. 계속 하시려면 샵 버튼을 누르세요 
			}
#else 
#ifdef __DDL_MODEL_B2C15MIN
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_BT_SHARP, gbInputKeyValue); // register a remote control, press the hash to continue.	
#else 
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_REMOCON_SHARP, gbInputKeyValue);	
#endif 
#endif 
			break;

		// 리모컨 개별 삭제
		case TENKEY_3:		
//			MenuSelectKeyTempSaveNTimeoutReset(24, gbInputKeyValue);	
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
			if(!gfPackTypeiRevoBleN && !gfInnerPackTypeiRevoBleN)
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				gMenuSelectKeyTempSave = TENKEY_NONE; 
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
			else 
			{
//				MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_REMOCON_SHARP, gbInputKeyValue);	// register a remote control, press the hash to continue.	
				MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_BT_SHARP , gbInputKeyValue);	// 블루투스 삭제 모드입니다. 계속 하시려면 샵 버튼을 누르세요	
			}
#else 
#ifdef __DDL_MODEL_B2C15MIN
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_BT_SHARP, gbInputKeyValue);
#else 
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_REMOCON_SHARP, gbInputKeyValue);	
#endif 
#endif 
			break;

#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
		case TENKEY_8:
//			MenuSelectKeyTempSaveNTimeoutReset(24, gbInputKeyValue);	
			if(!gfPackTypeiRevo && !gfInnerPackTypeiRevo)
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				gMenuSelectKeyTempSave = TENKEY_NONE; 
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
			else 
			{
			MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_REMOCON_SHARP, gbInputKeyValue);	
			}			
			break;

#ifdef DDL_CFG_ADD_REMOCON_LINK_FUNCTION
		case TENKEY_9:
			if(!gfInnerPackTypeiRevo)
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				gMenuSelectKeyTempSave = TENKEY_NONE; 
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
			else 
			{
				MenuSelectKeyTempSaveNTimeoutReset(VOICE_MIDI_BUTTON, gbInputKeyValue);
			}
			break;
#endif 	//DDL_CFG_ADD_REMOCON_LINK_FUNCTION

#endif 	//DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU

		case TENKEY_SHARP:
			OneRemoconModeSetSelected();
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




void ModeOneRemoconRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			Feedback13MenuOn(21, VOL_CHECK);
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
			//Feedback138MenuOn(AVML_REGREMOCON1_DELREMOCON3, VOL_CHECK);
			Feedback138MenuOn(AVML_REGBT1_DELBT3_REGREMOTE_8, VOL_CHECK);						// 블루투스 등록은 1번 , 블루투스 삭제는 3번 , 리모컨 등록은 8번을 누르세요. 
#else 
#ifdef __DDL_MODEL_B2C15MIN
			Feedback13MenuOn(AVML_SELECT_MENU, VOL_CHECK);			// press 1 to register a remote control, press 3 to delete a remote control
#else 
			Feedback13MenuOn(AVML_REGREMOCON1_DELREMOCON3, VOL_CHECK);
#endif 
#endif 
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOneRemoconModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}




void RemoconSlotNumberToRegister(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();

	bTmp = SUPPORTED_REMOCON_NUMBER;
	if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)	
	{
		bTmp = SUPPORTED_BLEN_NUMBER;
	}
		
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= bTmp))
	{
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU) || defined (__DDL_MODEL_B2C15MIN)
		FeedbackKeyPadLedOn(AVML_SMART_LIVING_APP_MSG, VOL_CHECK);
#else 
		FeedbackKeyPadLedOn(AVML_PRESS_REMOCONSET, VOL_CHECK); 			//리모콘에 있는 등록버튼을 누르세요
#endif 		

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_60S);

		gbModePrcsStep++;

		PackTxConnectionSend(EV_ALL_REGISTRATION, MODULE_TYPE_1_ONEWAY, gSelectedSlotNumber);
	}
	else
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}



void OneRemoconInputCheckForRegister(void)
{
	BYTE bTmp,bInnerTmp;
	
	switch(gbInputKeyValue)
	{
                case TENKEY_SHARP :
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		case TENKEY_STAR:
			PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);

			gbModePrcsStep = 0;
			break;

		default:
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
			bTmp = GetPackProcessResult();
			bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
			bTmp = GetPackProcessResult();
			bInnerTmp = 0;
#elif defined (P_BT_WAKEUP_T)
			bTmp = 0;
			bInnerTmp = GetInnerPackProcessResult();
#endif 

			if(bTmp == PACK_RMC_REGDEVICE_IN || bInnerTmp == PACK_RMC_REGDEVICE_IN)
			{
//				if(GetRemoconSlotNumber() == gSelectedSlotNumber)
				{
					gbModePrcsStep++;
				}			
			}
			
			if(bTmp != 0 || bInnerTmp != 0)
			{
				PackProcessResultClear();
			}

			TimeExpiredCheck();

			if(GetModeTimeOut() == 0)
			{
				PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);
			}	
			break;
	}
}



void InputOneRemoconProcessLoop(void)
{
	// 입력한 Slot Number 기준이 아닌 실제 등록되는 Slot Number 표시로 변경
//	FeedbackCredentialInput(VOICE_MIDI_BUTTON2, gSelectedSlotNumber);
	FeedbackCredentialInput(VOICE_MIDI_BUTTON2, GetRemoconSlotNumber());
	
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

	gbModePrcsStep++;
}



void ProcessRegisterOneRemocon(void)
{
	switch(gbInputKeyValue)
	{
		case TENKEY_SHARP:
			PackTxConnectionSend(EV_ALLREG_DEV_END, MODULE_TYPE_1_ONEWAY, 0);
	
			SetModeTimeOut(10);

			//# 버튼음 출력
#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;			

		case TENKEY_STAR:
			PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);

			ReInputOrModeMove(ModeGotoOneRemoconRegister);
			break;
		
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				
	
		default:
			if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)
			{
				// BLE-N의 경우 입력이 있을 경우에는 #을 누르지 않고 TimeOut이 되더라도 등록 처리하도록 프로그램 수정	
				if(GetModeTimeOut() == 0)
				{
					PackTxConnectionSend(EV_ALLREG_DEV_END, MODULE_TYPE_1_ONEWAY, 0);
			
					SetModeTimeOut(10);
					gbModePrcsStep++;
					break;			
				}
			}
			else
			{
				TimeExpiredCheck();
			}

			if(GetModeTimeOut() == 0)
			{
				PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);
			}	
			break;
	}
}



void BackwardFunctionForOneRemoconRegDel(void)
{
	ReInputOrModeMove(ModeGotoOneRemoconRegDelCheck);
}



void RegisterOneRemocon(void)
{
	BYTE bTmp,bInnerTmp;
	BYTE bTmpArray[8];
	
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)	
	bTmp = GetPackProcessResult();
	bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
	bTmp = GetPackProcessResult();
	bInnerTmp = 0;
#elif defined (P_BT_WAKEUP_T)
	bTmp = 0;
	bInnerTmp = GetInnerPackProcessResult();
#endif

	if(bTmp == PACK_RMC_ALLREG_DEV_END || bInnerTmp == PACK_RMC_ALLREG_DEV_END)
	{
		if(bTmp)
		PackSaveID();

		if(bInnerTmp)
			InnerPackSaveID();

//		FeedbackModeCompletedKeepMode(42, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
		FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

		if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)
		{
			memset(bTmpArray, 0xFF, 8);
			CopyCredentialDataForPack(bTmpArray, 8);
			Delay(SYS_TIMER_30MS);
			PackTxEventCredentialAdded(CREDENTIALTYPE_BLE, GetRemoconSlotNumber());
		}
		
		gbModePrcsStep++;
	}
	
	if(bTmp != 0 || bInnerTmp != 0)
	{
		PackProcessResultClear();
	}

	TimeExpiredCheck();
}

void ModeOneRemoconRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(8, VOL_CHECK);			//enter the number of the user to register the user number not the code, then press the hash to continue.
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//enter the number of the user to register the user number not the code, then press the hash to continue.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(RemoconSlotNumberToRegister, BackwardFunctionForOneRemoconRegDel,1);
			break;

		case 2:
			OneRemoconInputCheckForRegister();
			break;
		
		case 3:
			InputOneRemoconProcessLoop();
			break;

		case 4:
			ProcessRegisterOneRemocon();
			break;

		case 5:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			RegisterOneRemocon();
			break;

		case 6:
			GotoPreviousOrComplete(ModeGotoOneRemoconRegister);
			break;
		
		default:
			ModeClear();
			break;
	}
}





void ProcessDeleteOneRemocon(void)
{
	BYTE bTmp,bInnerTmp;
	
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
	bTmp = GetPackProcessResult();
	bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
	bTmp = GetPackProcessResult();
	bInnerTmp = 0;
#elif defined (P_BT_WAKEUP_T)
	bTmp = 0;
	bInnerTmp = GetInnerPackProcessResult();
#endif

	if(bTmp == PACK_RMC_ALL_REMOVE_END ||bInnerTmp == PACK_RMC_ALL_REMOVE_END)
	{
//		FeedbackModeCompletedKeepMode(58, VOL_CHECK);		//completed Press the star for an additional deletion or press the "R" button to finish
		FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R2, VOL_CHECK);		//completed Press the star for an additional deletion or press the "R" button to finish
	
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		
		if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)
		{
			Delay(SYS_TIMER_30MS);
			PackTxEventCredentialDeleted(CREDENTIALTYPE_BLE, gSelectedSlotNumber);
		}

		gbModePrcsStep++;
	}
	
	if(bTmp != 0 || bInnerTmp != 0)
	{
		PackProcessResultClear();
	}

	TimeExpiredCheck();
}



void RemoconSlotNumberToDelete(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();

	bTmp = SUPPORTED_REMOCON_NUMBER;
	if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)	
	{
		bTmp = SUPPORTED_BLEN_NUMBER;
	}
		
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= bTmp))
	{						
		PackTxConnectionSend(EV_ALL_REMOVE, MODULE_TYPE_1_ONEWAY, gSelectedSlotNumber);
		
		SetModeTimeOut(15);

		//# 버튼음 출력
#ifdef	P_VOICE_RST	
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
		gbModePrcsStep++;
	}
	else
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}



void ModeOneRemoconDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(49, VOL_CHECK);			//enter the number of the user to delete the user number not the code, then press the hash to continue.
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_FOR_DEL_SHARP, VOL_CHECK);			//enter the number of the user to delete the user number not the code, then press the hash to continue.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;

		case 1:
			SelectSlotNumber(RemoconSlotNumberToDelete, BackwardFunctionForOneRemoconRegDel,0);
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessDeleteOneRemocon();
			break;
			
		case 3:
			GotoPreviousOrComplete(ModeGotoOneRemoconDelete);
			break;

		default:
			ModeClear();
			break;
	}
}





#ifdef	BLE_N_SUPPORT
void ModeGotoOneRemoconRegisterByBleN(void)
{
	gbMainMode = MODE_MENU_ONEREMOCON_REGISTER_BYBLEN;
	gbModePrcsStep = 0;
}


void ModeGotoOneRemoconDeleteByBleN(void)
{
	gbMainMode = MODE_MENU_ONEREMOCON_DELETE_BYBLEN;
	gbModePrcsStep = 0;
}



void RemoconSlotNumberToRegisterByBleN(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
//		FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		ModeClear();

		TenKeyVariablesClear();
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();

	bTmp = SUPPORTED_REMOCON_NUMBER;
	if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)	
	{
		bTmp = SUPPORTED_BLEN_NUMBER;
	}
		
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= bTmp))
	{
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU) || defined (__DDL_MODEL_B2C15MIN)
		FeedbackKeyPadLedOn(AVML_SMART_LIVING_APP_MSG, VOL_CHECK);
#else 
		FeedbackKeyPadLedOn(AVML_PRESS_REMOCONSET, VOL_CHECK); 			//리모콘에 있는 등록버튼을 누르세요
#endif 		

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_60S);

		gbModePrcsStep++;

		PackTxConnectionSend(EV_ALL_REGISTRATION, MODULE_TYPE_1_ONEWAY, gSelectedSlotNumber);
	}
	else
	{
//		FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		ModeClear();

		TenKeyVariablesClear();
	}
}


void OneRemoconInputCheckForRegisterByBleN(void)
{
	BYTE bTmp,bInnerTmp;
	
	switch(gbInputKeyValue)
	{
                case TENKEY_SHARP: 
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		case TENKEY_STAR:
			PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;

		default:
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
			bTmp = GetPackProcessResult();
			bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
			bTmp = GetPackProcessResult();
			bInnerTmp = 0;
#elif defined (P_BT_WAKEUP_T)
			bTmp = 0;
			bInnerTmp = GetInnerPackProcessResult();
#endif

			if(bTmp == PACK_RMC_REGDEVICE_IN ||bInnerTmp == PACK_RMC_REGDEVICE_IN)
			{
//				if(GetRemoconSlotNumber() == gSelectedSlotNumber)
				{
					gbModePrcsStep++;
				}			
			}
			
			if(bTmp != 0 || bInnerTmp != 0)
			{
				PackProcessResultClear();
			}

			TimeExpiredCheck();

			if(GetModeTimeOut() == 0)
			{
				PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);
			}	
			break;
	}
}


void ModeOneRemoconRegisterByBleN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			// 바로 전송하면 동작하지 않는 경우 발생
			SetModeTimeOut(5);
			gbModePrcsStep++;
			break;

		case 1:
			if(GetModeTimeOut())	break;

			gPinInputKeyCnt = 2;
			RemoconSlotNumberToRegisterByBleN();
			break;
			
		case 2:
			OneRemoconInputCheckForRegisterByBleN();
			break;
		
		case 3:
			InputOneRemoconProcessLoop();
			SetModeTimeOut(5);
			break;

		case 4:
			if(GetModeTimeOut())	break;
			
			PackTxConnectionSend(EV_ALLREG_DEV_END, MODULE_TYPE_1_ONEWAY, 0);
	
			SetModeTimeOut(10);
			gbModePrcsStep++;
			break;

		case 5:
			RegisterOneRemocon();
			break;

		case 6:
//			FeedbackModeCompleted(1, VOL_CHECK);		//completed
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
			break;

		default:
			ModeClear();
			break;
	}
}



void RemoconSlotNumberToDeleteByBleN(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
//		FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		ModeClear();

		TenKeyVariablesClear();
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();

	bTmp = SUPPORTED_REMOCON_NUMBER;
	if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)	
	{
		bTmp = SUPPORTED_BLEN_NUMBER;
	}
		
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= bTmp))
	{						
		PackTxConnectionSend(EV_ALL_REMOVE, MODULE_TYPE_1_ONEWAY, gSelectedSlotNumber);
		
		SetModeTimeOut(15);

		//# 버튼음 출력
#ifdef	P_VOICE_RST	
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
		gbModePrcsStep++;
	}
	else
	{
//		FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		ModeClear();

		TenKeyVariablesClear();
	}
}


void ModeOneRemoconDeleteByBleN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			// 바로 전송하면 동작하지 않는 경우 발생
			SetModeTimeOut(5);
			gbModePrcsStep++;
			break;

		case 1:
			if(GetModeTimeOut())	break;

			gPinInputKeyCnt = 2;
			RemoconSlotNumberToDeleteByBleN();
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessDeleteOneRemocon();
			break;
			
		case 3:
//			FeedbackModeCompleted(1, VOL_CHECK);		//completed
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
			break;
			
		default:
			ModeClear();
			break;
	}
}



#endif



