
#define		_MODE_FINGER_REGISTER_C_
//------------------------------------------------------------------------------
/** 	@file		ModeFingerRegister_T4.c
	@version 0.1.00
	@date	2018.08.22
	@brief	FingerPrint Register Mode 
		V0.1.00 2018.08.22		by Hyojoon
*/
//------------------------------------------------------------------------------

#include "Main.h"

//------------------------------------------------------------------------------
/** 	@brief	FingerPrint Register Mode Start
	@param	None
	@return 	None
	@remark 지문 등록 모드 시작
*/
//------------------------------------------------------------------------------
void	ModeGotoFingerRegister( void )
{
	gbMainMode = MODE_MENU_FINGER_REGISTER;
	gbModePrcsStep = 0;
#ifdef	BLE_N_SUPPORT
	gbEnterMode = 0x00;
#endif 
}

#ifdef	BLE_N_SUPPORT
void	ModeGotoFingerRegisterByBleN( void )
{
	gbMainMode = MODE_MENU_FINGER_REGISTER;
	gbModePrcsStep = 0;
	gbEnterMode = 0x01;
}
#endif 

void	ModeFingerRegister( void )
{
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if(no_fingers >= _MAX_REG_BIO){
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		//(음성)no more space to memorize.
				BioOffModeClear();
				break;
			}
#if (_MAX_REG_BIO == 100)
			else{
				AudioFeedback(AVML_ENTER_FINGER, gcbBuzSta, VOL_CHECK);			// 등록하실 지문을 차례로 입력 하세요.
			}
#else 
			else if(no_fingers==0){
				AudioFeedback(AVML_ENTER_FINGER, gcbBuzSta, VOL_CHECK);			// 등록하실 지문을 차례로 입력 하세요.
			}
			else{
				AudioFeedback((AVML_FINGER01_REG_MSG-1+no_fingers), gcbBuzSta, VOL_CHECK);	//(음성) n개의 지문이 등록 되었습니다. 등록하실 지문을 차례로 세번 입력하세요.
			}
#endif 
#ifdef	DDL_CFG_NO_DIMMER//디밍 없는 지문 모델을 위해 추가 sjc	
#else
			LedModeRefresh();
#endif

			FingerReadBuff[0] = 0;		// 1의 자리
			FingerReadBuff[1] = 0;		// 10의 자리
			FingerReadBuff[2] = 0;		// 두자리 카운트
			FingerReadBuff[3] = 0;		// 임시 값 저장

			if(no_fingers == 0)
			{
			}
			else if(no_fingers < 10)
			{
				FingerReadBuff[0] = no_fingers;
			}
			else 
			{
				FingerReadBuff[1] = no_fingers / 10;
				FingerReadBuff[0] = no_fingers % 10;
			}			
			gbModePrcsStep = 1;
			break;
			
		case 1:
			if(FingerReadBuff[2] >= 2)
			{
				FingerReadBuff[0] = 0;		// 1의 자리
				FingerReadBuff[1] = 0;		// 10의 자리
				FingerReadBuff[2] = 0;		// 두자리 카운트
				FingerReadBuff[3] = 0;		// 임시 값 저장
				gbModePrcsStep = 3;				
			}
			else
			{
				if(FingerReadBuff[2] == 0)
					FingerReadBuff[3] = FingerReadBuff[1];
				else
					FingerReadBuff[3] = FingerReadBuff[0];
#ifdef	DDL_CFG_NO_DIMMER//디밍 없는 지문 모델을 위해 추가 sjc		
				if(FingerReadBuff[2] == 0) //LED Display를 한번만 하기 위해 sjc
				{
					LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
					LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
				}				
#else
				LedPWLedSetting(FingerReadBuff[3]);
#endif
				gbModePrcsStep = 2;
			}
			break;
			
		case 2:
			if(GetLedMode()) break;
			FingerReadBuff[2]++;
			gbModePrcsStep  = 1;
			break;
			
		case 3:
			BioModuleON();
			SetModeTimeOut(10); //1s wait ATR
			gbModePrcsStep = 4;
			break;

		case 4:
			if(GetModeTimeOut() == 0)
			{
				/* ATR 이 1s 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep  = 100;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;

				// Device status check 
				if(gbFingerPrint_rxd_buf[12] != 0x00)
				{
					FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					gbModePrcsStep  = 100;
					break;
				}
				//Get ALT 
				gbfingerALT = gbFingerPrint_rxd_buf[11];
				// save Device UID
				RomWrite(&gbFingerPrint_rxd_buf[3],HFPM_MODULE_ID,8);

				gbModePrcsStep = 5;
				SetModeTimeOut(gbfingerALT);
				NFPMTxEventEnroll(0x00);
			}
			break;

		case 5:	
			if(GetModeTimeOut() == 0)
			{
				/* ALT 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep = 100;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;

				// Enroll ack 처리 
				// Get Max Tempalte number
				if(gbFingerPrint_rxd_buf[2] != NFPM_EVNET_ENROLL || 
					(gbFingerPrint_rxd_buf[3] < 0x01 && gbFingerPrint_rxd_buf[3] > 0x08) ||
					gbFingerPrint_rxd_buf[4] !=0x00 )
				{
					if(gbFingerPrint_rxd_buf[4] !=0x00)
					{	
						uint8_t bTemp[2] = {0x00,0x00};

						/* Goodix 에서 메모리가 무족한 경우 */
						FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);
						
						/* 100 을 기록 하고 number 를 다시 구성 한다 */
						no_fingers = 100U;
						RomWrite(&no_fingers, (WORD)KEY_NUM, 1);

						for (uint8_t i = 0 ; i <  _MAX_REG_BIO; i++)
						{
							bTemp[0]++;
							bTemp[1]++;
							RomWrite(bTemp, (WORD)(BIO_ID+(2*i)), 2); 
						}
					}
					else
					{
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					}
					gbModePrcsStep = 100;
					break;
				}
				
				gbfingerMaxTemplateNumber = gbFingerPrint_rxd_buf[3];
				gbModePrcsStep = 6;
				dbDisplayOn = 0x00;
				gbFingerRegisterDisplayStep = 0;
				SetModeTimeOut(_MODE_TIME_OUT_7S); // 등록 모드 대기 상태 이므로 7초 
			}
			break;
			
		case 6:
			if(GetModeTimeOut() == 0)
			{
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);
				gbModePrcsStep = 100;
				break;
			}

			if(gbInputKeyValue == FUNKEY_REG){
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				gbModePrcsStep = 100;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				switch(gbFingerPrint_rxd_buf[3]) // template number 
				{
					case 0x01:
					case 0x02:
					case 0x03:
					case 0x04:
					case 0x05:
					case 0x06:
					case 0x07:
					case 0x08:					
					{
						switch(gbFingerPrint_rxd_buf[4])
						{
							case 0x00: // parameter ok 
							{
								AudioFeedback((AVML_REG_FINGER01_REIN+gbFingerPrint_rxd_buf[3]-1),gcbBuzNum, VOL_CHECK);
								SetModeTimeOut(_MODE_TIME_OUT_7S);
								NFPMTxEventEnrollAlarm(gbFingerPrint_rxd_buf[3]);
								gbFingerRegisterDisplayStep = gbFingerPrint_rxd_buf[3];	
								#ifdef	DDL_CFG_NO_DIMMER	//디밍 없는 지문 모델을 위해 추가 sjc										
									LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
									LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
								#endif
							}
							break;

							case 0xFF: // enroll complete
							{
								SetModeTimeOut(_MODE_TIME_OUT_20S);
								gbFID = gbFingerPrint_rxd_buf[5];
								gbModePrcsStep = 7;
							}
							break;

							case 0x04: // dirty finger
							case 0x05: // repeat finger
							{
								AudioFeedback(AVML_REG_FINGER_ERR,gcbBuzNum, VOL_CHECK);
								SetModeTimeOut(_MODE_TIME_OUT_20S);
							}
							break;	

							default: // error 
							{
								FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
								gbModePrcsStep = 100;
							}
							break;
							
						}
					}
					break;

					default:
					{
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
						gbModePrcsStep = 100;
					}
					break;					
				}
			}		
			else 
			{
				FingerPrintRegisterDisplay();
			}
			break;
			
		case 7:		
			if(GetModeTimeOut() == 0)
			{
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep = 100;
				break;
			}

			FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID;
			gbFID--;
			no_fingers++;
			RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				
			RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); 

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, ConvertFingerprintSlotNumber(gbFID));
#endif
			
			memset(bTmpArray, 0x00, 8);
			CopyCredentialDataForPack(bTmpArray, 8);
			PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)FingerWriteBuff[0]);

#ifdef	BLE_N_SUPPORT
			if(no_fingers >= _MAX_REG_BIO || gbEnterMode == 0x01)
#else 			
			if(no_fingers >= _MAX_REG_BIO)
#endif 				
			{ 				
				AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);
				LedSetting(gcbLedOk, LED_DISPLAY_OFF);
				gbModePrcsStep = 100;
				break;
			}
			
			FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);
			NFPMTxEventStop(0x00);
			SetModeTimeOut(gbfingerALT);
			gbModePrcsStep = 8;
			break;

		case 8:
			if(GetModeTimeOut() == 0 ||gbFingerPrint_rxd_end)
			{
				BioModuleOff();
				gbFingerPrint_rxd_end = 0;
				gbModePrcsStep = 9;
				SetModeTimeOut(_MODE_TIME_OUT_20S);
			}
			break;
		
		case 9:
			switch(gbInputKeyValue)
			{
				case FUNKEY_REG:
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					BioOffModeClear();
					break;
				
				case TENKEY_STAR:
					gbModePrcsStep = 0;		
					AudioFeedback(VOICE_MIDI_INTO,gcbBuzSta, VOL_CHECK);
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_NONE:
					if(GetModeTimeOut() == 0)
					{
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
				
				default:
				break;
			}
			break;

		case 100:
			NFPMTxEventStop(0x00);
			SetModeTimeOut(gbfingerALT);
			gbModePrcsStep = 101;
			break;

		case 101:
			if(GetModeTimeOut() == 0)
			{
				BioOffModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				// Rx 만 받으면 그냥 off 
				gbFingerPrint_rxd_end = 0;
				BioOffModeClear();	
				break;
			}
		break;

		default:
			gbModePrcsStep = 100;
			break;

	}

}

