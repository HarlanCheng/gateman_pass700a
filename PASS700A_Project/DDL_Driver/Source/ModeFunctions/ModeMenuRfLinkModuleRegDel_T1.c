//------------------------------------------------------------------------------
/** 	@file		ModeMenuRfLinkModuleRegDel_T1.c
	@version 0.1.00
	@date	2016.07.07
	@brief	RF Link Module and ZWave&Zigbee Module Register/Delete Mode
	@remark	연동기 및 ZWave/Zigbee 모듈 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.07.07		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"




void ModeGotoRfLinkModuleRegDelCheck(void)
{
	gbMainMode = MODE_MENU_RFLINKMODULE_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoRfLinkModuleRegister(void)
{
	RemoconSlotNumberSet(0);

	gbMainMode = MODE_MENU_RFLINKMODULE_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoRfLinkModuleDelete(void)
{
	gbMainMode = MODE_MENU_RFLINKMODULE_DELETE;
	gbModePrcsStep = 0;
}

void RfLinkModuleSoftReset(void)
{
	BYTE bTmp;

	// 아이레보 통신팩이거나 1번/3번이 눌린 경우에는 처리하지 않음.
	//if(gfPackTypeiRevo || gfPackTypeiRevoBleN ||gfInnerPackTypeiRevo || gfInnerPackTypeiRevoBleN || gbInputKeyValue)		break;
			
	// Soft Reset 전송

#if 1 // 듀얼 팩 지원 제품 동일 팩 삽입시 내부 팩을 우선 순위높게 
	if(gfInnerPackTypeZBZWModule)
	{
		InnerPackTx_MakePacketSinglePort(EV_SOFTWARE_RESET, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 0);				
	}
	else if(gfPackTypeZBZWModule)
	{
		PackTx_MakePacketSinglePort(EV_SOFTWARE_RESET, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);
	}
#else 
	if(gfPackTypeZBZWModule)
		PackTx_MakePacketSinglePort(EV_SOFTWARE_RESET, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);

	if(gfInnerPackTypeZBZWModule)
		InnerPackTx_MakePacketSinglePort(EV_SOFTWARE_RESET, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 0);				
#endif 	

	// Soft Reset 후 현재 등록/삭제 상태 업데이트하기 위해 2s 조회 시도 200ms 로는 갱신이 되지 않는다 
	SetModeProcessTime(200);
	gbModePrcsStep++;
	gMenuSelectKeyTempSave = TENKEY_NONE;
}



void RfLinkModuleModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
#ifdef _ZWAVE_SOFT_RESET_MENU_9_
		// SoftReset (locally reset)
		case 9: 		
			FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);
			RfLinkModuleSoftReset();
			break;
#else 
		// SoftReset (locally reset)
		case 0: 		
			RfLinkModuleSoftReset();
			break;
#endif 			
			
		// 홈네트워크 등록
		case 1: 	
			ModeGotoRfLinkModuleRegister();
			break;
	
		// 홈네트워크 삭제
		case 3: 		
			ModeGotoRfLinkModuleDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuRfLinkModuleModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 홈네트워크 등록
		case TENKEY_1:			
			if(((gbModuleMode ==PACK_ID_CONFIRMED) &&  gfPackTypeZBZWModule && gfPackZBZWRegisterd) ||
				((gbInnerModuleMode ==PACK_ID_CONFIRMED) && gfInnerPackTypeZBZWModule && gfInnerPackZBZWRegisterd))
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				
				gMenuSelectKeyTempSave = TENKEY_NONE; 
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
				break;
			}
				
//			MenuSelectKeyTempSaveNTimeoutReset(27, gbInputKeyValue);	// register a controller, press the hash to continue. 	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_HOMENET_SHARP, gbInputKeyValue);	// register a controller, press the hash to continue. 	
			break;

		// 홈네트워크 삭제
		case TENKEY_3:		
			if(((gbModuleMode ==PACK_ID_CONFIRMED) && gfPackTypeZBZWModule && !gfPackZBZWRegisterd) ||
				((gbInnerModuleMode ==PACK_ID_CONFIRMED) && gfInnerPackTypeZBZWModule && !gfInnerPackZBZWRegisterd)
			)
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				
				gMenuSelectKeyTempSave = TENKEY_NONE; 
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
				break;
			}

//			MenuSelectKeyTempSaveNTimeoutReset(29, gbInputKeyValue);	// Delete the controller, press the hash to continue. 
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_HOMENET_SHARP, gbInputKeyValue);	// Delete the controller, press the hash to continue. 
			break;

#ifdef _ZWAVE_SOFT_RESET_MENU_9_
		case TENKEY_9:
			MenuSelectKeyTempSaveNTimeoutReset(VOICE_MIDI_BUTTON, gbInputKeyValue);
			break;
#else 
		case TENKEY_0:
			if( gMenuSelectKeyTempSave == TENKEY_1 || gMenuSelectKeyTempSave == TENKEY_3)
				break;
			gMenuSelectKeyTempSave = gbInputKeyValue; 
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			break;
#endif 			

		case TENKEY_SHARP:
			RfLinkModuleModeSetSelected();
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




void ModeRfLinkModuleRegDelCheck(void)
{
	BYTE bTmp;
	
	switch(gbModePrcsStep)
	{
		case 0:
#if 1 // 듀얼 팩 지원 제품 동일 팩 삽입시 내부 팩을 우선 순위높게 
			if(gfInnerPackTypeZBZWModule)
			{
				InnerPackTx_MakePacketSinglePort(F0_RFM_CHECK, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 0);
			}
			else if(gfPackTypeZBZWModule)
			{
				PackTx_MakePacketSinglePort(F0_RFM_CHECK, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);
			}
#else 
			if(gfPackTypeZBZWModule)
			{
				PackTx_MakePacketSinglePort(F0_RFM_CHECK, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);
			}

			if(gfInnerPackTypeZBZWModule)
			{
				InnerPackTx_MakePacketSinglePort(F0_RFM_CHECK, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 0);
			}
#endif 			
			
//			Feedback13MenuOn(26, VOL_CHECK);				// press 1 to register a controller, press 3 to delete a controller
#ifdef _ZWAVE_SOFT_RESET_MENU_9_
			Feedback139MenuOn(AVML_REGHOMENET1_DELHOMENET3, VOL_CHECK);				// press 1 to register a controller, press 3 to delete a controller
#else 
			Feedback13MenuOn(AVML_REGHOMENET1_DELHOMENET3, VOL_CHECK);				// press 1 to register a controller, press 3 to delete a controller
#endif 			

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuRfLinkModuleModeSelectProcess();
			break;	
	
		case 2:
			// Soft Reset 후 현재 등록/삭제 상태 업데이트하기 위해 200ms 후 조회 시도
			if(GetModeProcessTime()) 	break;

#if 1 // 듀얼 팩 지원 제품 동일 팩 삽입시 내부 팩을 우선 순위높게 
			if(gfInnerPackTypeZBZWModule)
			{
				InnerPackTx_MakePacketSinglePort(F0_RFM_CHECK, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 0);
			}
			else if(gfPackTypeZBZWModule)
			{
				PackTx_MakePacketSinglePort(F0_RFM_CHECK, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);
			}
#else 
			if(gfPackTypeZBZWModule)
				PackTx_MakePacketSinglePort(F0_RFM_CHECK, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);

			if(gfInnerPackTypeZBZWModule)
				InnerPackTx_MakePacketSinglePort(F0_RFM_CHECK, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 0);
#endif 		

#ifdef _ZWAVE_SOFT_RESET_MENU_9_
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);	
			ModeClear();
#else 
			//조회 후 다시 원래의 모드로 이동
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gbModePrcsStep--;
#endif 			
			break;

		default:
			ModeClear();
			break;
	}

}




void ProcessRegisterRfLinkModule(void)
{
	if(GetRemoconSlotNumber() == 0)
	{
		PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);

		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		ModeClear();
	}
	else
	{
		PackTxConnectionSendForRF(EV_ALLREG_DEV_END, MODULE_TYPE_TWOWAY, 0);

		SetModeTimeOut(10);

		
#ifdef	P_VOICE_RST	
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
		gbModePrcsStep++;
	}
}


void RegisterRfLinkModule(void)
{
	BYTE bTmp,bInnerTmp;
	
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
	bTmp = GetPackProcessResult();
	bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
	bTmp = GetPackProcessResult();
	bInnerTmp = 0;
#elif defined (P_BT_WAKEUP_T)
	bTmp = 0;	
	bInnerTmp = GetInnerPackProcessResult();
#endif 

	if(bTmp == PACK_RMC_ALLREG_DEV_END || bInnerTmp == PACK_RMC_ALLREG_DEV_END)
	{
		if(bTmp)
			PackSaveID();

		if(bInnerTmp)
			InnerPackSaveID();

//		FeedbackModeCompletedKeepMode(42, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
		FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);		//completed, Press the star for setting other options or press the "R" button to finish
	
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	
		gbModePrcsStep++;
	}
	
	if(bTmp != 0 || bInnerTmp != 0)
	{
		PackProcessResultClear();
	}

	TimeExpiredCheck();
}


void RfLinkModuleRegDelProcess(void)
{
//	FeedbackModeCompletedKeepMode(39, VOL_CHECK);
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
	
	gbModePrcsStep++;
}


void ModeRfLinkModuleRegister(void)
{
	BYTE bTmp,bInnerTmp;

	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(28, VOL_CHECK);			// Press the registration button on the controller
			FeedbackKeyPadLedOn(AVML_PRESS_HOMENETREG, VOL_CHECK);			// Press the registration button on the controller

			SetModeTimeOut(_MODE_TIME_OUT_60S);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;

			RemoconSlotNumberSet(0);

			PackTxConnectionSendForRF(EV_ALL_REGISTRATION, MODULE_TYPE_TWOWAY, 0);

#if 1 // 듀얼 팩 지원 제품 동일 팩 삽입시 내부 팩을 우선 순위높게 
			if(gfInnerPackTypeZBZWModule)
			{
				InnerPackTx_MakePacketSinglePort(F0_RF_MODULE_REGISTER, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 0);
			}
			else if(gfPackTypeZBZWModule)
			{
				PackTx_MakePacketSinglePort(F0_RF_MODULE_REGISTER, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);
			}
#else 
			if(gfPackTypeZBZWModule)
			{
				PackTx_MakePacketSinglePort(F0_RF_MODULE_REGISTER, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);
			}

			if(gfInnerPackTypeZBZWModule)
			{
				InnerPackTx_MakePacketSinglePort(F0_RF_MODULE_REGISTER, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 0);
			}
#endif 			
			break;
			
		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_SHARP:
					ProcessRegisterRfLinkModule();
					break;		

				case TENKEY_STAR:
					PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);

					ReInputOrModeMove(ModeGotoRfLinkModuleRegDelCheck);
					break;

				case FUNKEY_REG:
				case FUNKEY_OPCLOSE:
					PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);

					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
				
				default:
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
					bTmp = GetPackProcessResult();
					bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
					bTmp = GetPackProcessResult();
					bInnerTmp = 0;
#elif defined (P_BT_WAKEUP_T)
					bTmp = 0;
					bInnerTmp = GetInnerPackProcessResult();
#endif 

					if(bTmp == PACK_RMC_REGDEVICE_IN || bInnerTmp == PACK_RMC_REGDEVICE_IN)
					{
						FeedbackCredentialInput_1(VOICE_MIDI_BUTTON2, GetRemoconSlotNumber());

						SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					}
					else if(bTmp == PACK_HCP_JOIN_UNJOIN_OK ||bInnerTmp == PACK_HCP_JOIN_UNJOIN_OK)
					{
						// 아이레보 모듈을 제외하고 다른 모듈은 등록 완료될 경우 별도로 # 버튼 누르지 않아도 자동 종료 처리
//						FeedbackModeCompletedKeepMode(42, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
						FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
						
						TenKeyVariablesClear();
						SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
						
						gbModePrcsStep+=2;
					}
					else if(bTmp == PACK_HCP_JOIN_UNJOIN_FAIL||bInnerTmp == PACK_HCP_JOIN_UNJOIN_FAIL)
					{
						// 등록 실패 결과가 전송될 경우 에러 모드 종료 처리
						PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);

						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						ModeClear();
					}

					if(bTmp != 0 || bInnerTmp != 0)
					{
						PackProcessResultClear();
					}
						
					TimeExpiredCheck();

					if(GetModeTimeOut() == 0)
					{
						PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);
					}						
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			RegisterRfLinkModule();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoRfLinkModuleRegDelCheck);
			break;
		
		default:
			ModeClear();
			break;
	}
}





void ProcessDeleteRfLinkModule(void)
{
	BYTE bTmp;

	bTmp = PincodeVerify(0);
	switch(bTmp)
	{
		case 1:
		case RET_MASTER_CODE_MATCH:
			if((gbManageMode == _AD_MODE_SET) && (bTmp == 1))
			{
				PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);
						
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 	
				ModeClear();
				break;				
			}			
			
			PackTxConnectionSendForRF(EV_ALL_REMOVE, MODULE_TYPE_TWOWAY, 0);

			SetModeTimeOut(50);

#if 1 // 듀얼 팩 지원 제품 동일 팩 삽입시 내부 팩을 우선 순위높게 
			if(gfInnerPackTypeZBZWModule)
			{
				InnerPackTx_MakePacketSinglePort(F0_RF_MODULE_REGISTER, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 0);
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
			else if(gfPackTypeZBZWModule)
			{
				PackTx_MakePacketSinglePort(F0_RF_MODULE_REGISTER, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
#else 
			if(gfPackTypeZBZWModule)
			{
				PackTx_MakePacketSinglePort(F0_RF_MODULE_REGISTER, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}

			if(gfInnerPackTypeZBZWModule)
			{
				InnerPackTx_MakePacketSinglePort(F0_RF_MODULE_REGISTER, ES_LOCK_EVENT, &gbInnerComCnt, &bTmp, 0);
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
#endif 
			
#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;

		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
			PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);
					
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);
					
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);		
			ModeClear();
			break;				
	}
}



void DeleteRfLinkModule(void)
{
	BYTE bTmp,bInnerTmp;
	
	switch(gbInputKeyValue)
	{
		case TENKEY_STAR:
		case TENKEY_SHARP:
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			if(gfPackTypeZBZWModule || gfInnerPackTypeZBZWModule)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;
			}
			break;
			
		default:
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
			bTmp = GetPackProcessResult();
			bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
			bTmp = GetPackProcessResult();
			bInnerTmp = 0;
#elif defined (P_BT_WAKEUP_T)
			bTmp = 0; 
			bInnerTmp = GetInnerPackProcessResult();
#endif 

			if(bTmp == PACK_RMC_ALL_REMOVE_END || bInnerTmp == PACK_RMC_ALL_REMOVE_END)
			{
//				FeedbackModeCompletedKeepMode(58, VOL_CHECK);		// Completed, Press the star for an additional deletion or press the "R"button to finish
				FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);		// Completed, Press the star forsetting other options or press the "R"button to finish
			
				TenKeyVariablesClear();
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			
				gbModePrcsStep++;
			}
			else if(bTmp == PACK_HCP_JOIN_UNJOIN_OK || bInnerTmp == PACK_HCP_JOIN_UNJOIN_OK) 
			{
				// 아이레보 모듈을 제외하고 다른 모듈은 등록 완료될 경우 별도로 # 버튼 누르지 않아도 자동 종료 처리
//				FeedbackModeCompletedKeepMode(58, VOL_CHECK);		// Completed, Press the star for an additional deletion or press the "R"button to finish
				FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);		// Completed, Press the star for setting other options or press the "R"button to finish
				
				TenKeyVariablesClear();
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
				
				gbModePrcsStep++;
			}
			else if(bTmp == PACK_HCP_JOIN_UNJOIN_FAIL|| bInnerTmp == PACK_HCP_JOIN_UNJOIN_FAIL) 
			{
				// 삭제 실패 결과가 전송될 경우 에러 모드 종료 처리
				PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);

				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
			}
			
			if(bTmp != 0 || bInnerTmp != 0)
			{
				PackProcessResultClear();
			}

			TimeExpiredCheck();
			
			if(GetModeTimeOut() == 0)
			{
				PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);
			}						
			break;
	}		

	TimeExpiredCheck();
}




void ModeRfLinkModuleDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			if(gbManageMode == _AD_MODE_SET)
			{
				FeedbackKeyPadLedOn(AVML_IN_MPIN_SHARP, VOL_CHECK);			//Enter the master code, then press the hash to continue
			}
			else
			{
//				FeedbackKeyPadLedOn(15, VOL_CHECK);			//Enter the user code, then press the hash to continue
				FeedbackKeyPadLedOn(AVML_IN_PIN_SHARP, VOL_CHECK);			//Enter the user code, then press the hash to continue
			}
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:		//# 버튼음 출력
					// ZWave, Zigbee 모듈은 삭제 처리가 바로 안 될 수도 있기 때문에 해당 부분 처리 필요
					FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);

					ProcessDeleteRfLinkModule();
					break;		

				case TENKEY_STAR:
					PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);

					ReInputOrModeMove(ModeGotoRfLinkModuleRegDelCheck);
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					PackTxConnectionSendForRF(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_TWOWAY, 0);

					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			DeleteRfLinkModule();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoRfLinkModuleRegDelCheck);
			break;

		default:
			ModeClear();
			break;
	}
}




