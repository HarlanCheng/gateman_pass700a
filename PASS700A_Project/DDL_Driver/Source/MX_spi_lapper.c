
#define		_MX_SPI_LAPPER_C_

//#include "stm32l1xx_hal.h"
#include "main.h"
#include "MX_spi_lapper.h"


/******************************************************************************
*	ddl_i2c_t 변수 초기화 예
*
*
*	MCU I2C를 사용할 경우
*
*		ddl_i2c_t	my_i2c_dev;
*
*		my_i2c_dev.type = DDL_I2C_MCU;
*		my_i2c_dev.dev.mcu.hi2c = gh_mcu_i2c;
*
*
*	gpio I2C를 사용할 경우
*
*		ddl_i2c_t	my_i2c_dev;
*
*		my_i2c_dev.type = DDL_I2C_GPIO;
*		my_i2c_dev.dev.gpio.SCL_GPIOx		= DIM_SCL_GPIO_Port;		//mxconstants.h에 선언되어 있음 
*		my_i2c_dev.dev.gpio.SCL_GPIO_Pin	= DIM_SCL_Pin;
*		my_i2c_dev.dev.gpio.SDA_GPIOx		= DIM_SDA_GPIO_Port;
*		my_i2c_dev.dev.gpio.SDA_GPIO_Pin	= DIM_SDA_Pin;
*
*******************************************************************************/


//------------------------------------------------------------------------------
/** 	@file		MX_spi_lapper.c
	@version 0.1.00
	@date	2016.04.04
	@brief	SPI Interface Functions
	@see	Stm32l1xx_hal_spi.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.01	2016.04.04		by Jay

*/
//------------------------------------------------------------------------------



/*****************************************************************************/
void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
#ifdef	DDL_CFG_RFID
	ModeClear();
#endif 
}


// NSS는 Output Hardware로 설정할 경우 아래 함수에서 자동 처리
//		Output Software로 설정할 경우 아래 함수 호출 부분에서 추가 처리 필요
// NSS를 사용하지 않을 경우 별도 처리 필요 없음
void	ddl_spi_write( ddl_spi_t *ddl_spi, uint8_t *data, uint16_t length )
{
	if(HAL_SPI_Transmit(ddl_spi->dev.mcu.hspi, data, length, MCU_SPI_TIMEOUT) != HAL_OK)
	{
		/* Soft NSS 제어 일경우 여기 까지 왔다면 이미 PIN 설정은 되어 있는 상태 */
		/* CLK , MOSI , MISO 는 HAL_SPI_DeInit 와 HAL_SPI_Init 에서 다시 설정 */
		/* SPI 초기화 후 같은 data 를 한번 더 보내고 안되면 callback 에서 error 처리 */
		if(ddl_spi !=NULL && data !=NULL && length != 0) //parameter 가 정상 인데 HAL_OK 가 아니라면 
		{
			HAL_SPI_DeInit(ddl_spi->dev.mcu.hspi);
			HAL_SPI_Init(ddl_spi->dev.mcu.hspi);
			Delay( SYS_TIMER_100US );
			if(HAL_SPI_Transmit(ddl_spi->dev.mcu.hspi, data, length, MCU_SPI_TIMEOUT) != HAL_OK)
			{
				HAL_SPI_ErrorCallback(ddl_spi->dev.mcu.hspi);
			}
		}
		else 
		{
			HAL_SPI_ErrorCallback(ddl_spi->dev.mcu.hspi);
		}
	}
}

void	ddl_spi_read( ddl_spi_t *ddl_spi, uint8_t *data, uint16_t length )
{
	if(HAL_SPI_Receive(ddl_spi->dev.mcu.hspi, data, length, MCU_SPI_TIMEOUT) != HAL_OK)
	{
		/* Soft NSS 제어 일경우 여기 까지 왔다면 이미 PIN 설정은 되어 있는 상태 */
		/* CLK , MOSI , MISO 는 HAL_SPI_DeInit 와 HAL_SPI_Init 에서 다시 설정 */
		/* SPI 초기화 후 같은 data 를 한번 더 보내고 안되면 callback 에서 error 처리 */
		if(ddl_spi !=NULL && data !=NULL && length != 0) //parameter 가 정상 인데 HAL_OK 가 아니라면 
		{
			HAL_SPI_DeInit(ddl_spi->dev.mcu.hspi);
			HAL_SPI_Init(ddl_spi->dev.mcu.hspi);
			Delay( SYS_TIMER_100US );		
			if(HAL_SPI_Receive(ddl_spi->dev.mcu.hspi, data, length, MCU_SPI_TIMEOUT) != HAL_OK)
			{
				HAL_SPI_ErrorCallback(ddl_spi->dev.mcu.hspi);
			}
		}
		else 
		{
			HAL_SPI_ErrorCallback(ddl_spi->dev.mcu.hspi);
		}
	}
}




