#ifndef		_FINGER_RTC_PCF85063TP_H_
#define		_FINGER_RTC_PCF85063TP_H_

typedef union
{
	struct 
	{
		uint8_t  Seconds;
		uint8_t  Minutes;
		uint8_t  Hours;
		uint8_t  Days;
		uint8_t  WeekDays;
		uint8_t  Mounth;
		uint8_t  Years;
	}time;
	uint8_t  Timebuffer[7];
}EX_RTCTimeDef;

uint8_t ConvertWeekDays(uint8_t data);
uint8_t Timecheck(uint8_t* buffer);
void TimeCorrection(void);
void GetEXRTCTime(uint8_t* buffer);
void SetEXRTCTime(uint8_t* buffer);
void RTCInit(void);
#endif

