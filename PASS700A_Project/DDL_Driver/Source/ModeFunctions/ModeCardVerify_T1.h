//------------------------------------------------------------------------------
/** 	@file		ModeCardVerify_T1.c
	@brief	Card Verify Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODECARDVERIFY_T1_INCLUDED
#define __MODECARDVERIFY_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


//------------------------------------------------------------------------------
// gbModePrcsStep에서 참조 - 카드 인증 처리 세부 단계
//------------------------------------------------------------------------------
enum{
	MODEPRCS_CARD_VERIFY = 2,

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	MODEPRCS_CARD_GET_TIME,
	MODEPRCS_CARD_GET_TIME_CHECK,	
	MODEPRCS_CARD_SCHEDULE_VERIFY,
#endif

	MODEPRCS_OPEN_BYCARD_COMPLETE_CHECK,
#ifdef CLOSE_BYCREDENTIAL//Card를 이용한 문 닫힘 
	MODEPRCS_CLOSE_BYCARD_COMPLETE_CHECK,
#endif	
	MODEPRCS_LOCKOUTINIT_BY_CARD,
	MODEPRCS_LOCKOUTSET_BY_CARD, 
	MODEPRCS_LOCKOUTSET_BY_CARD_CHK,
	MODEPRCS_LOCKOUTSET_BY_CARD_ERROR,
	MODEPRCS_CARD_AWAY_CHK = 14,	//ModePincodeVerify_T1.h에 선언된 MODEPRCS_LOCKOUTSET_SEND_CHK가 12의 값을 가지고 있는데 MODEPRCS_LOCKOUTSET_SEND_CHK를 ModeCardVerify에서 사용 하기 때문에 에러가 발생 하여 13으로 강제로 선언 함
};


void ModeGotoCardVerify(void);
void ModeGotoInputCardVerify(void);
void ModeCardVerify(void);


BYTE CardVerify(BYTE InputCardNum);


#endif


