#include	"Main.h"


uint32_t	rtc_ts_count = 0;

uint32_t LongSleepOn = 0x00000000;
uint32_t LongSleepkeep = 0x00000000;

// #define __PRINT_PIN_CONFIG__

// user defined RTC wakeup ISR callback function
//void	HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
void	HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc)
{
	rtc_ts_count ++;

	/* Clear Wake Up Flag */
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
}



// model별 stopmode 진입여부를 결정하는 함수
// return 0 : stopmode로 진입
// return 1 : stopmode로 들어가지 않는다.
__weak	uint32_t		StopModeBeforeCallback( void )
{
	uint32_t		ret=0;

	if(GetBuzPrcsStep())		ret = 1;
	if(GetLedMode())			ret = 1;
	if(gbWakeUpMinTime10ms) 	ret = 1;

	return	ret;
}


// model별 stopmode  동안 설정되어 있는 내용들 등록
__weak	void		StopModePrepareCallback( void )
{
}




// model별 stopmode  빠져 나 온뒤 처리할 일 등록
__weak	void		StopModeAfterCallback( void )
{
}


#ifdef  __PRINT_PIN_CONFIG__

void PrintfModeInfo(GPIO_TypeDef  *GPIOx)
{
	uint32_t TempModer = 0x00000000;
	uint32_t TempPupdr = 0x00000000;
	uint32_t TempOdr = 0x00000000;	
//	uint32_t TempIdr = 0x00000000;		
	
	BYTE i = 0;
	BYTE PortValue = 0x00;
	BYTE Direction = 0x00;
	SBYTE GpioGroup;
	
	TempModer = GPIOx->MODER;
	TempPupdr = GPIOx->PUPDR;	
	TempOdr = GPIOx->ODR;	
//	TempIdr = GPIOx->IDR;	

	if(GPIOx == GPIOA)
		GpioGroup = 'A';
	else 	if(GPIOx == GPIOB)
		GpioGroup = 'B';
	else 	if(GPIOx == GPIOC)	
		GpioGroup = 'C';	
	else 	if(GPIOx == GPIOD)
		GpioGroup = 'D';
	else 	if(GPIOx == GPIOE)	
		GpioGroup = 'E';	
	else 	if(GPIOx == GPIOH)
		GpioGroup = 'H';	

	for(i = 0 ; i < 16 ; i++)
	{
		PortValue = TempModer & 0x03;
#if 1
		switch(PortValue)
		{
			case 0x00:
				printf("InPut Mode , ");
				Direction = 1;
			break;

			case 0x01:
				printf("Output Push Pull , ");
				Direction = 2;				
			break;

			case 0x02:
				printf("Alternate function mode , ");
			break;

			case 0x03:
				printf("Analog mode , ");
			break;

			default : 
			break;
		}
	
		PortValue = TempPupdr & 0x03;

		switch(PortValue)
		{
			case 0x00:
				printf("No pull-up and no pull-down , ");
			break;

			case 0x01:
				printf("Pull-up , ");
			break;

			case 0x02:
				printf("Pull-down , ");
			break;

			case 0x03:
				printf("N/A , ");
			break;

			default : 
			break;
		}

		if(Direction == 1)
			printf("%s\r\n",(HAL_GPIO_ReadPin(GPIOx,0x01<<i) == 1 ? "HIGH" : "LOW"));	
		else if(Direction == 2)
			printf(", %s\r\n",((TempOdr & 0x1) == 1 ? "HIGH" : "LOW"));	
		else 
			printf(", \r\n");
		
		Direction = 0;
#else 
		switch(PortValue)
		{
			case 0x00:
				printf("Gpio %2d InPut      Pin State [%s] \n",i,(HAL_GPIO_ReadPin(GPIOx,0x01<<i) == 1 ? "H" : "L"));
			break;

			case 0x01:
				printf("Gpio %2d OutPut     Pin State [%s] \n",i,(HAL_GPIO_ReadPin(GPIOx,0x01<<i) == 1 ? "H" : "L"));
			break;

			case 0x02:
				printf("Gpio %2d Alternate  Pin State [%s] \n",i,(HAL_GPIO_ReadPin(GPIOx,0x01<<i) == 1 ? "H" : "L"));
			break;

			case 0x03:
				printf("Gpio %2d Analog     Pin State [%s] \n",i,(HAL_GPIO_ReadPin(GPIOx,0x01<<i) == 1 ? "H" : "L"));
			break;

			default : 
			break;
		}
#endif 		
		TempModer = TempModer >> 2;
		TempPupdr = TempPupdr >> 2;
		TempOdr = TempOdr >> 1;

		if(GpioGroup == 'H' && i == 2) //H 는  0 1 2 번 까지 보고 나가는 걸로 
			break;
	}
}
void ReadGPIOModeRegister(void)
{
	static WORD WaitInit = 0x0000;

	if(WaitInit == 2)
	{
		SetupUartforPrintf(gh_mcu_uart_com);
		InitPrintfService(NULL,NULL,NULL);
		
		printf("0x%08X , 0x%08X  , 0x%08X , 0x%08X \r\n",GPIOA->MODER, GPIOA->PUPDR,GPIOA->IDR,GPIOA->ODR);
		printf("0x%08X , 0x%08X  , 0x%08X , 0x%08X \r\n",GPIOB->MODER, GPIOB->PUPDR,GPIOB->IDR,GPIOB->ODR);
		printf("0x%08X , 0x%08X  , 0x%08X , 0x%08X \r\n",GPIOC->MODER, GPIOC->PUPDR,GPIOC->IDR,GPIOC->ODR);
		printf("0x%08X , 0x%08X  , 0x%08X , 0x%08X \r\n",GPIOD->MODER, GPIOD->PUPDR,GPIOD->IDR,GPIOD->ODR);		
		printf("0x%08X , 0x%08X  , 0x%08X , 0x%08X \r\n",GPIOE->MODER, GPIOE->PUPDR,GPIOE->IDR,GPIOE->ODR);		
		printf("0x%08X , 0x%08X  , 0x%08X , 0x%08X \r\n\r\n\r\n",GPIOH->MODER, GPIOH->PUPDR,GPIOD->IDR,GPIOH->ODR);
		
		PrintfModeInfo(GPIOA);
		PrintfModeInfo(GPIOB);	
		PrintfModeInfo(GPIOC);		
		PrintfModeInfo(GPIOD);
		PrintfModeInfo(GPIOE);
		PrintfModeInfo(GPIOH);
		WaitInit = 0xFFFF;
	}else if(WaitInit == 0xFFFF)
		__NOP();
	else 
		WaitInit++;
}

#endif 


void EnterSleepMode(void)
{
	uint8_t jigmode = PowerDownModeForJig();
	
	if(LongSleepOn == MAGIC_ENTER_LONG_SLEEP || jigmode == STATUS_SUCCESS)
	{
		LongSleepkeep = MAGIC_KEEP_LONG_SLEEP;
		while(LongSleepkeep == MAGIC_KEEP_LONG_SLEEP || jigmode == STATUS_SUCCESS)
		{
			HAL_RTCEx_SetWakeUpTimer_IT( gh_mcu_rtc, DDL_CFG_RTC_WAKUPTIME, RTC_WAKEUPCLOCK_RTCCLK_DIV16 );
			HAL_PWR_EnterSTOPMode( PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI );		
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
		}
	}
	else
	{
		HAL_PWR_EnterSTOPMode( PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI );
	}
}


void		StopMode( void )
{	
	//Stop Mode로 진입할 것인지 판별하는 callback 함수를 호출
	if ( StopModeBeforeCallback() )
	{
		if(GetAbnormalSleepCheckTimer() == 0x00)
		{
			SetAbnormalSleepCheckTimer(1800u);
		}
		return;
	}
	SetAbnormalSleepCheckTimer(0x00);

	StopModePrepareCallback();

	/* Disable Wake-up timer */
	HAL_RTCEx_DeactivateWakeUpTimer( gh_mcu_rtc );

	/*## Configure the Wake up timer ###########################################*/
	/*	RTC Wakeup Interrupt Generation:
		Wakeup Time Base = (RTC_WAKEUPCLOCK_RTCCLK_DIV /(LSI))
		Wakeup Time = Wakeup Time Base * WakeUpCounter 
					= (RTC_WAKEUPCLOCK_RTCCLK_DIV /(LSI)) * WakeUpCounter
		==> WakeUpCounter = Wakeup Time / Wakeup Time Base
	
		To configure the wake up timer to 10s the WakeUpCounter is set to 23125:
			RTC_WAKEUPCLOCK_RTCCLK_DIV = RTCCLK_Div16 = 16 
			Wakeup Time Base = 16 /37KHz = 0.4324 ms
			Wakeup Time = 10s
			==> WakeUpCounter = 10s/0.4324ms = 23125
	*/

	/* Enable Wake-up timer */
//	HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 23125, RTC_WAKEUPCLOCK_RTCCLK_DIV16);		// stop for 10 sec
	HAL_RTCEx_SetWakeUpTimer_IT( gh_mcu_rtc, DDL_CFG_RTC_WAKUPTIME, RTC_WAKEUPCLOCK_RTCCLK_DIV16 );

// for debugging
//P_FIRE_EN(1);
#ifndef	DDL_CFG_WATCHDOG
	HAL_DBGMCU_EnableDBGStopMode();
	HAL_SuspendTick(); 	
#endif 

#ifdef  __PRINT_PIN_CONFIG__
	ReadGPIOModeRegister();
#endif 

	/*## Enter Stop Mode #######################################################*/
	EnterSleepMode();

#ifndef	DDL_CFG_WATCHDOG
	HAL_ResumeTick();
#endif 

// for debugging
//P_FIRE_EN(0);

/* After wake-up from STOP reconfigure the system clock */
	SystemClock_Config();

	/* Disable Wake-up timer */
	HAL_RTCEx_DeactivateWakeUpTimer( gh_mcu_rtc );

//	BuzzerSetting( gcbBuzOpn, VOL_HIGH );
	
	SystickVariableInit();	

	StopModeAfterCallback();

	AutoRelockTime13Counter();
}


