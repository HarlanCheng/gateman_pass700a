#include	"Main.h"

#ifdef DDL_CFG_RFID_PARALLEL

WORD gbPortABuff;

void SetupParallel(void)
{

}

void ParallelPortGPIOSet(BYTE bData)
{

	GPIO_InitTypeDef		GPIO_InitStruct;

	GPIO_InitStruct.Pin = 	RFID_IO00_Pin | 
						RFID_IO01_Pin |
						RFID_IO02_Pin |
						RFID_IO03_Pin |
						RFID_IO04_Pin |
						RFID_IO05_Pin |
						RFID_IO06_Pin |
						RFID_IO07_Pin; 
					   	
	if(bData == OUTPUT)	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	else					GPIO_InitStruct.Mode = GPIO_MODE_INPUT;

	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(RFID_IO00_GPIO_Port, &GPIO_InitStruct);	
}

void ParallelPortGPIOWrite(WORD wData)
{
	gbPortABuff = RFID_IO00_GPIO_Port->IDR; // HAL 에 register 를 통으로 읽는게 없네... 
	gbPortABuff &= 0xFF00;
	gbPortABuff |= wData;		
	RFID_IO00_GPIO_Port->ODR = gbPortABuff;	//write
}

void ParallelSOF(void)
{
	ParallelPortGPIOSet(OUTPUT);
	P_RFID_DATA7(0);						

	P_RFID_CLK(1);
	P_RFID_DATA7(1);						

	P_RFID_CLK(0);
}

void ParallelEOF(void)
{
	P_RFID_CLK(0);
	P_RFID_DATA7(1);						

	P_RFID_CLK(1);							
	P_RFID_DATA7(0);						
	P_RFID_CLK(0);	
}



void ParallelEOFContinuous(void)
{
	P_RFID_CLK(0);							
	P_RFID_DATA7(0);	
	P_RFID_DATA7(1);
	P_RFID_DATA7(0);
}




void ParallelDataSend(BYTE *bData, BYTE bSize)
{
	do{
		ParallelPortGPIOWrite((WORD)*bData);
		P_RFID_CLK(1);		
		__NOP();//Delay(SYS_TIMER_10US); //SYS_TIMER_1US
		__NOP();
		P_RFID_CLK(0);
		bData++;
		bSize--;
	}while(bSize);
}

void ParallelDataReceive(BYTE* bpData, BYTE bSize)
{
	ParallelPortGPIOSet(INPUT);

	do{
		P_RFID_CLK(1);
		*bpData = (BYTE)(RFID_IO00_GPIO_Port->IDR);
		P_RFID_CLK(0);				
		bpData++;
		bSize--;
	}while(bSize);

	ParallelPortGPIOSet(OUTPUT);
}

/*
void ParallelSOF(void)
{
	ParallelPortGPIOSet(OUTPUT);
	ParallelPortGPIOWrite(0x0000);

	P_RFID_CLK(1);
	ParallelPortGPIOWrite(0x00FF);

	P_RFID_CLK(0);
	ParallelPortGPIOWrite(0x0000);
}

void ParallelEOF(void)
{
	P_RFID_CLK(0);
	ParallelPortGPIOSet(OUTPUT);
	ParallelPortGPIOWrite(0x00FF);

	P_RFID_CLK(1);							
	ParallelPortGPIOWrite(0x0000);
	P_RFID_CLK(0);	
}



void ParallelEOFContinuous(void)
{
	P_RFID_CLK(0);							
	ParallelPortGPIOSet(OUTPUT);
	ParallelPortGPIOWrite(0x0000);
	P_RFID_DATA7(1);
	P_RFID_DATA7(0);
}



void ParallelDataSend(BYTE *bData, BYTE bSize)
{
	WORD temp;

	do{
		temp = (WORD)*bData;
		ParallelPortGPIOWrite(temp);
		P_RFID_CLK(1);		
		__NOP();//Delay(SYS_TIMER_10US); //SYS_TIMER_1US
		__NOP();
		P_RFID_CLK(0);
		bData++;
		bSize--;
	}while(bSize);
}

void ParallelDataReceive(BYTE* bpData, BYTE bSize)
{
	ParallelPortGPIOSet(INPUT);

	do{
		P_RFID_CLK(1);
		gbPortABuff = RFID_IO00_GPIO_Port->IDR;
		*bpData = (BYTE)gbPortABuff;
		P_RFID_CLK(0);				
		bpData++;
		bSize--;
	}while(bSize);
}
*/

void CardPinSetAnalog(void)
{
	
	GPIO_InitTypeDef		GPIO_InitStruct;

	GPIO_InitStruct.Pin = 	RFID_IO00_Pin | 
						RFID_IO01_Pin |
						RFID_IO02_Pin |
						RFID_IO03_Pin |
						RFID_IO04_Pin |
						RFID_IO05_Pin |
						RFID_IO06_Pin |
						RFID_IO07_Pin; 


	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(RFID_IO00_GPIO_Port, &GPIO_InitStruct);	
}

#endif 

