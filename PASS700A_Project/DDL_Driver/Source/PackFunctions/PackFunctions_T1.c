//------------------------------------------------------------------------------
/** 	@file		PackFunctions_T1.c
	@version 0.1.01
	@date	2016.07.26
	@brief	Communication Pack Process Functions
	@see	
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.06.10		by Jay
			- 통신팩 장착에 대한 처리 함수

		V0.1.01 2016.07.26		by Jay
			- BLE-N 기능 추가
*/
//------------------------------------------------------------------------------

#include "Main.h"


FLAG PackFlag;								

BYTE gPackConnectionMode = 0;
BYTE gbPackStatus = 0;
BYTE gbPackChkTimer100ms = 0;
BYTE gbPackChkCnt = 0;

BYTE gbComCnt = 0;	// 통신 패킷 Count(제3바이트)


BYTE gbModuleMode = 0;
BYTE gbModuleProtocolVersion = 0;
BYTE gbEncryptionType = 0 ;


BYTE gbPackModeTimer10ms = 0;
BYTE gPackProcessResult = 0;

#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
BYTE gPackCBARegisterSet = 0;
#endif


BYTE PackConnectedCheck(void)
{
#if defined (P_COM_DDL_EN_T)
	if(P_COM_DDL_EN_T)	
	{
		return 1;
	}
#endif 
	return 0;	
}

void PackSaveID(void)
{
	RomWrite(&gbPackRxDataCopiedBuf[PACK_PACKET_RMC_SLOTNUMBER], PACK_ID, 4);
	gbModuleMode = PACK_ID_CONFIRMED; 
}

BYTE GetPackProcessResult(void)
{
	return gPackProcessResult;
}

void PackProcessResultClear(void)
{
	gPackProcessResult = 0;
	InnerPackProcessResultClear();
}

void DelayForiRevoPackModule(void)
{
	if(gfPackTypeiRevo || gfPackTypeiRevoBleN ||gfInnerPackTypeiRevo || gfInnerPackTypeiRevoBleN)
	{
		Delay(SYS_TIMER_20MS);	
	}
}

#if defined (P_COM_DDL_EN_T)
void PackModuleModeClear(void)
{
//	OffGpioCommPack();

	gbModuleMode = 0;
	gbPackChkCnt = 0;
}

void PackCheckTimeCounter(void)
{
  if(gbPackChkTimer100ms)	       --gbPackChkTimer100ms;
}


void PackModeTimeCounter(void)
{
	if(gbPackModeTimer10ms) 	--gbPackModeTimer10ms;
}


void SetPackModeTimeCount(BYTE SetData)
{
	gbPackModeTimer10ms = SetData;
}


BYTE GetPackModeTimeCount(void)
{
	return (gbPackModeTimer10ms);
}



void PackWakeupInitial(void)
{
	PackRxDataClear();

	SetPackModeTimeCount(20);

	PackRxProcessWakeUp();

 /*	
	if(gbPackMode != PACK_ACKWAIT_MODE)
	{
		gbPackMode = PACK_RX_DATA_WAITING;

		CommPackRxInterruptRequest();
	}
*/
}



void PackDeviceInitialSend(void)
{
//	BYTE bCnt;
	BYTE		tmpArray[50];

//	gbPackTxDataBuf[PACK_F_EVENT] = 0x44;
//	gbPackTxDataBuf[PACK_F_SOURCE] = 0xB0;
//	gbPackTxDataBuf[PACK_F_COUNT] = 0x00;
	
//	gbPackTxDataBuf[PACK_F_LENGTH_ADDDATA] = 0x1A;
//	gbPackTxDataBuf[PACK_F_LOCKTYPE] = 0x70;
//	gbPackTxDataBuf[PACK_F_LOCKTYPE+1] = 0x71;
//	gbPackTxDataBuf[PACK_F_ENCRYPTIONTYPE] = 0x00;
//
//	for(bCnt = 0; bCnt < 16; bCnt++)
//	{
//		gbPackTxDataBuf[PACK_F_DEVICEID+bCnt] = bCnt+1;
//	}
//	

//	gbPackTxDataBuf[PACK_F_CHECKSUMTYPE] = 0x00;	 // N-Protocol V2	
//	gbPackTxDataBuf[PACK_F_RESERVED] = 0xFF;
//	gbPackTxDataBuf[PACK_F_RESERVED+1] = 0xFF;
//	gbPackTxDataBuf[PACK_F_WAKEUPTIMING] = 0x00; // Tl Time : Default (100us)
//	gbPackTxDataBuf[PACK_F_WAKEUPTIMING+1] = 0x00; // Twu Time : Default (150us)
//	gbPackTxDataBuf[PACK_F_WAKEUPTIMING+2] = 0x00; // Units and Multiplier
//	gbPackTxDataBuf[PACK_F_PROTOCOLVERSION] = 0x20;
//
//	gbPackTxByteNum = 30;

//	PackTxProcessStepStart(0);

	tmpArray[PACK_F_LOCKTYPE] = 0x70;
	tmpArray[PACK_F_LOCKTYPE+1] = 0x71;
#if (DDL_CFG_BLE_30_ENABLE >= 0x30)	
	tmpArray[PACK_F_ENCRYPTIONTYPE] = 0x01;
#else 
	tmpArray[PACK_F_ENCRYPTIONTYPE] = 0x00;
#endif 

//	for(bCnt = 0; bCnt < 16; bCnt++)
//	{
//		tmpArray[PACK_F_DEVICEID+bCnt] = bCnt+1;
//	}

	RomRead(&tmpArray[PACK_F_DEVICEID], (WORD)DOORLOCK_ID, 16);  
	
	tmpArray[PACK_F_CHECKSUMTYPE] = 0x00;		// N-Protocol V2
	tmpArray[PACK_F_RESERVED] = 0xFF;
	tmpArray[PACK_F_RESERVED+1] = 0xFF;
	tmpArray[PACK_F_WAKEUPTIMING] = 0x00; // Tl Time : Default (100us)
	tmpArray[PACK_F_WAKEUPTIMING+1] = 0x00; // Twu Time : Default (150us)
	tmpArray[PACK_F_WAKEUPTIMING+2] = 0x00; // Units and Multiplier

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	tmpArray[PACK_F_PROTOCOLVERSION] = DDL_CFG_BLE_30_ENABLE;
#else
	tmpArray[PACK_F_PROTOCOLVERSION] = 0x20;
#endif

	PackTx_MakePacketForInitial(F0_RF_MODULE_DEVICE_INFORMATION, ES_LOCK_EVENT, &gbComCnt, &tmpArray[PACK_F_LOCKTYPE], 26);
}




void PackConnectionStart(void)
{
	// 생산 지그 연결 확인을 위해 1회는 Device Information Exchange 시도
	P_COM_HCP_EN(1);

	PackTxEnSigSetDefault();

	SetupCommPack_UART();
	//반드시 UART Setup 후에 Service callback 함수 등록을 해야 정상적으로 동작됨. 
	InitCommPackService();

	gbPackChkCnt = 0;
	gPackConnectionMode = 1;
}


void PackSendRunApplicaionAck(void)
{
	BYTE bTemp = 0x00;

	if(PackConnectedCheck())
	{
		P_COM_HCP_EN(1);
		PackTxEnSigSetDefault();
		SetupCommPack_UART();
		InitCommPackService();
		gbModuleMode = PACK_ID_CONFIRMED;
		PackTx_MakeAndSendPacketNow(F0_RUN_APPLICATION,ES_LOCK_ACK,&gbComCnt,&bTemp, 0);
		PackTx_QueueInit();
		gbModuleMode = 0x00;	
	}
}


void PackConnectionStartCheck(void)
{
	if(PackConnectedCheck())
	{
		PackConnectionStart();
	}
	else
	{
		// 해당 함수가 여기에 있을 경우 팩 장착이 되어 있지 않으면 계속 Load가 걸려 카드 읽기 처리하는데 문제가 됨.
		// PackModuleModeClear 함수 내에 위치하도록 수정
		//OffGpioCommPack();

		gPackConnectionMode = 0;
	}
}


void PackDisconnectionCheck(void)
{
	gbPackChkTimer100ms = 5;
	gPackConnectionMode = 5;
}


#define	PACK_CONNECTION_TRY_COUNT		10		// 응답 없을 경우 총 10회 시도
#define	PACK_CONNECTION_TRY_INTERVAL	5		// 500 ms 간격으로 확인	

// 통신팩이 장착되어 있을 경우 500ms 간격으로 10회 반복하여 Device Information Exchange 시도
// 위 시도는 도어록의 전원이 인가된 후 500ms ~ 2s 사이에 첫 시도가 진행되어야 함.
void PackConnectionProcess(void)
{
	switch(gPackConnectionMode)
	{
		case 0:
			// 점검이 끝난 뒤, 상태 체크중
			if(gbModuleMode)
			{
				if(PackConnectedCheck() == 0)
				{
					PackDisconnectionCheck();
				}
			}
			else if(gbPackChkCnt == 0)
			{
				PackConnectionStartCheck();
			}
			break;
		
		case 1:
			if(gbPackChkTimer100ms)			break;
			PackDeviceInitialSend();
			PackRxDataClear();

			gbPackChkTimer100ms = PACK_CONNECTION_TRY_INTERVAL;

			gPackConnectionMode++;
			break;
		
		case 2:
			if(gbModuleMode || (JigModeStatusCheck() == STATUS_SUCCESS))
			{
				// UART PROCESS 에서 Pack 체크가 되었는지 기다린다.
				gbPackChkCnt++;
				gPackConnectionMode = 0;
			}
			else if(gbPackChkTimer100ms == 0)	// time out이면 
			{
				gbPackChkCnt++;

				if(gbPackChkCnt >= PACK_CONNECTION_TRY_COUNT)
				{
					// UART 실패후 DDL 에서 Device Check 송신한다.
					gPackConnectionMode = 0;
				}
				else
				{
					if(PackConnectedCheck())
					{
						// UART 실패후 DDL 에서 Device Check 송신한다.
						gPackConnectionMode--;
					
					}
					else
					{
						gbPackChkCnt = 0;
						gPackConnectionMode = 0;
					}
				}
			}
			break;

		case 5:
			// Pack 떨어짐. 채터링
			if(PackConnectedCheck() == 0)
			{
				if(gbPackChkTimer100ms == 0)
				{
					// 탈착 처리
#ifdef	__FRONT_BLE_CONTROL__
					if(gbFrontBLENStatus == 0xFF && gfInnerPackTypeiRevoBleN)
					{
						gbInnerModuleMode = PACK_ID_NOT_CONFIRMED; 
						BLENTxFrontBLEStartStopCheckSend(SUBEV_DATA_START_MODULE);
					}
#endif 
					OffGpioCommPack();

					PackModuleModeClear();

					gPackConnectionMode = 0;

					PackTypeVariableClear();					

					if(AlarmStatusCheck() == STATUS_SUCCESS)		break;

#if 0 // pack 제거시 UI 적으로 아무것도 안한다. 
					if(GetTamperProofPrcsStep())
					{
						TamperCountClear();
					}
#ifdef	DDL_CFG_DIMMER
					LedSetting(gcbLedOff, LED_DISPLAY_OFF);
#endif
						
#ifdef	DDL_CFG_NO_DIMMER
					LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
#endif

#ifdef	DDL_CFG_FP  //hyojoon_20160831 PFM-3000 add
					BioModuleOff();
#endif
					ModeClear();
					
//지문 모델의 경우 음성안내가 길기 때문에 팩이 빠지면 중간에 음성을 끊고 완료음을 내도록 한다
#ifndef	DDL_CFG_FP //hyojoon_20160831 PFM-3000 add
					if(GetBuzPrcsStep())	break;
					if(GetVoicePrcsStep())	break;
#endif

					//FeedbackModeClear();
#endif 
				}
			}
			else
			{
				gPackConnectionMode = 0;
			}
			break;

		default:
			gPackConnectionMode = 0;
			break;
	}
}




void		InitCommPackService( void )
{
	RegisterCommPack_INT_Callback( CommPack_uart_txcplt_callback, CommPack_uart_rxcplt_callback );
	CommPackRxInterruptRequest();
	PackTx_QueueInit();
}





void PackModeClear(void)
{
	PackRxDataClear();
	PackRxProcessStepClear();

//	gbPackMode = 0;
	gbPackModeTimer10ms = 0;
}



BYTE PackModeCheck(void)
{
	if(gPackConnectionMode)		return (STATUS_SUCCESS);

	return (STATUS_FAIL);
}



void PackTypeVariableClear(void)
{
	extern uint8_t gPackTxAckWaitIndex;
	extern uint8_t gbAckWaitBuffer[ACK_WAIT_BUFFER_SIZE][MAX_PACK_BUF_SIZE+3];
		
	gfPackTypeiRevo = 0;
	gfPackTypeiRevoBleN = 0;
	gfPackTypeZBZWModule = 0;
	gfPackTypeCBABle = 0;
	gbEncryptionType = 0x00;
	gbModuleProtocolVersion = 0;

	gPackTxAckWaitTimer100ms = 0;
	gPackTxAckWaitIndex= 0;
	for(uint8_t i = 0 ; i <ACK_WAIT_BUFFER_SIZE ; i++)
	{
		gbAckWaitBuffer[i][0] = 0;
	}
}
	
 
BYTE PackTypeCheck(BYTE TypeData)
{
	BYTE RetVal = 0;

	// Pack Typ을 초기화하는 부분이 없어 중복 설정될 수 있는 가능성 때문에 추가
	PackTypeVariableClear();

	switch(TypeData)
	{
		case PACK_TYPE_JIG:
			JigModeStart();

			PackRxDataClear();
			PackRxProcessStepClear();
			SetJigModeID(&gbPackRxDataCopiedBuf[PACK_F_DEVICEID]);

			RetVal = PACK_TYPE_JIG;
			break;

		case PACK_TYPE_IREVO:
			gfPackTypeiRevo = 1;
			RetVal = PACK_TYPE_IREVO;
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			gbEncryptionType = gbPackRxDataCopiedBuf[PACK_F_ENCRYPTIONTYPE];
			gbModuleProtocolVersion = gbPackRxDataCopiedBuf[PACK_F_PROTOCOLVERSION] ;
#else
			gbEncryptionType = 0x00;
			gbModuleProtocolVersion =0x00;
#endif 	

			break;

#ifdef	BLE_N_SUPPORT
		case PACK_TYPE_IREVO_BLEN:
			gfPackTypeiRevoBleN = 1;
			RetVal = PACK_TYPE_IREVO_BLEN;
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			gbEncryptionType = gbPackRxDataCopiedBuf[PACK_F_ENCRYPTIONTYPE];
			gbModuleProtocolVersion = gbPackRxDataCopiedBuf[PACK_F_PROTOCOLVERSION] ;
#else 
			gbEncryptionType = 0x00;
			gbModuleProtocolVersion =0x00;
#endif 
			
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			if(gbModuleProtocolVersion >= 0x29)
			{
				gfPackTypeiRevoBleN30 = 1;
			}
#endif	// DDL_CFG_BLE_30_ENABLE
			break;
#endif	// BLE_N_SUPPORT

		case PACK_TYPE_CBA_BLE:
			gfPackTypeCBABle = 1;
			RetVal = PACK_TYPE_CBA_BLE;
			gbEncryptionType = gbPackRxDataCopiedBuf[PACK_F_ENCRYPTIONTYPE];
			gbModuleProtocolVersion = gbPackRxDataCopiedBuf[PACK_F_PROTOCOLVERSION] ;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			if(gbModuleProtocolVersion >= 0x29)
			{
				gfPackTypeiRevoBleN30 = 1;
			}			
#endif
			break;

		default :
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			gbEncryptionType = gbPackRxDataCopiedBuf[PACK_F_ENCRYPTIONTYPE];		
			gbModuleProtocolVersion = gbPackRxDataCopiedBuf[PACK_F_PROTOCOLVERSION] ;		
#else 
			gbEncryptionType = 0x00;
			gbModuleProtocolVersion =0x00;
#endif 
			gfPackTypeZBZWModule = 1;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			if(gbModuleProtocolVersion >= 0x29)
			{
				gfPackTypeiRevoBleN30 = 1;
			}
#endif 
			break;
	}

#if (DDL_CFG_BLE_30_ENABLE == 0x29)
	gbModuleProtocolVersion = 0x00;
	gbEncryptionType = 0x00;
#endif 

	return (RetVal);
}


void PackIDCheck(BYTE *pDeviceIdBuf)
{
	BYTE SavedIdBuf[5];
	
	if(gfPackTypeiRevo || gfPackTypeiRevoBleN)
	{
		RomRead(SavedIdBuf, PACK_ID, 4);
		
		if(memcmp(SavedIdBuf, pDeviceIdBuf, 4) == 0)
		{
			gbModuleMode = PACK_ID_CONFIRMED; 
		}
		else
		{
			gbModuleMode = PACK_ID_NOT_CONFIRMED; 
		}
	}
	else
	{
		gbModuleMode = PACK_ID_CONFIRMED; 
	}
}


void PackDeviceCheck(void)
{
	PackIDCheck(&gbPackRxDataCopiedBuf[PACK_F_DEVICEID]);
#if 0 // pack 삽입시 아무것도 안한다.  
	if(AlarmStatusCheck() == STATUS_SUCCESS)		return;
	if(GetMainMode())		return;
	
	if(GetBuzPrcsStep())	return;
	if(GetVoicePrcsStep())	return;
	
	//FeedbackModeClear();
#endif 	
}

void SetPackProcessResult(BYTE bResult)
{
	gPackProcessResult = bResult;
}


// Power Down 모드 실행 전에 PACK 모듈 장착이 되어 있지 않은 상태면 해당 UART Pin Analog로 설정하기 위해 내용 추가
void ChangePackPinToAnalog(void)
{
	extern uint8_t gbPrintfOn;
	
	if((!P_COM_DDL_EN_T) && (gbModuleMode == 0) && (gbPrintfOn == 0x00))
	{
	 	OffGpioCommPack();
	
		PackModuleModeClear();
	}
}
#endif 


#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
BYTE GetPackCBARegisterSet(void)
{
	RomRead(&gPackCBARegisterSet, CBA_SET, 1);

	if(gPackCBARegisterSet == 0xAA)		return 1;

	return 0;
}


void SetPackCBARegisterSet(BYTE bValue)
{
	if(bValue == 1)
	{
		gPackCBARegisterSet = 0xAA;
	}
	else
	{
		gPackCBARegisterSet = 0;
	}	

	RomWrite(&gPackCBARegisterSet, CBA_SET, 1);
}
#endif


#ifdef	_NEW_YALE_ACCESS_UI
uint8_t gYaleAccessMode = 0;	


void SetYaleAccessMode(uint8_t Yale_Access_Mode)
{
	gYaleAccessMode = Yale_Access_Mode;
}


uint8_t GetYaleAccessMode(void)
{
	return (gYaleAccessMode);
}


bool Is_Yale_Access_Deleted(void)
{
	if((gYaleAccessMode == YALE_ACCESS_MODE_DELETED) || gbpackModeSuspend)
	{
		return (true);
	}

	return (false);
}
#endif	// _NEW_YALE_ACCESS_UI



