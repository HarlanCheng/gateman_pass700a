//------------------------------------------------------------------------------
/** 	@file		Buzzer.c
	@version 0.1.00
	@date	2016.03.31
	@brief	Doorlock Buzzer Interface Functions
	@see	Buzzer.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00	2016.03.31		by Jay
			- Buzzer 구동 내용
*/
//------------------------------------------------------------------------------

#include	"Main.h"


#define	_BUZ_TIMER_START		HAL_TIM_PWM_Start( gh_mcu_tim_buzzer, TIM_CHANNEL_4)
#define	_BUZ_TIMER_STOP		HAL_TIM_PWM_Stop( gh_mcu_tim_buzzer, TIM_CHANNEL_4)


#define BUZZER_FOREVER 0xFFFFFFFF //32bit 
//#define TOP 500

uint32_t gdwBuzTimerInitCnt = 0;


WORD gbBuzPrcsStep = 0;						
WORD gbBuzNum = 0;					
WORD gbBuzCnt = 0;					

BYTE gbFirstVoice = 0x00;

WORD const* gbpBuzBuf;							


const WORD gcbBuzOff[] = {
	BZ_ENDD
};
/**< Buzzer Off  */
	
const WORD gcbBuzPwr[]={
	BZ_FA6N-2, 10, 1,
	BZ_SO6N-2, 10, 1,
	BZ_RA6N-2, 10, 1,
	BZ_SI6N-2, 10, 1,
	BZ_DO7N-2, 10, 1,
	BZ_RE7N-2, 10, 1,
	BZ_ME7N-2, 10, 1,
	BZ_FA7N-2, 10, 1,
	BZ_FA7N-2, 10, 1,
	BZ_ME7N-2, 10, 1,
	BZ_RE7N-2, 10, 1,
	BZ_DO7N-2, 10, 1,
	BZ_SI6N-2, 10, 1,
	BZ_RA6N-2, 10, 1,
	BZ_SO6N-2, 10, 1,
	BZ_FA6N-2, 10, 0,
	BZ_ENDD
};
/**< 초기화음  */


const WORD gcbBuzOpn[]={
	BZ_SO6U, 75, 10,
	BZ_ME6N, 75, 10,
	BZ_DO7N, 75, 0,
	BZ_ENDD
};
/**< 열림음  */


const WORD gcbBuzCls[]={
	BZ_DO7N, 75, 12,
	BZ_DO7N, 50, 5,
	BZ_SO6U, 75, 0,
	BZ_ENDD
};
/**< 닫힘음  */


const WORD gcbBuzReg[] = {
	BZ_ME6N, 75, 10,
	BZ_RA6N, 75, 10,
	BZ_DO7N, 75, 10,
	BZ_ENDD
};
/**< 완료음  */


const WORD gcbBuzSta[]={
	BZ_RA6N, 50, 15,
	BZ_FA6U, 50, 0,
	BZ_ENDD
};
/**< 모드진입음  */


const WORD gcbBuzNum[]={
	BZ_SO7N, 25, 0,
	BZ_ENDD
};
/**< 수용음  */

#if defined (DDL_CFG_MS)
const WORD gcbBuzEnterVerify[]={
	BZ_SO7N, 25, 15,
	BZ_FA7N, 25, 15,
	BZ_ME7N, 25, 15,	
	BZ_RE7N, 25, 15,	
	BZ_DO7N, 25, 15,
	BZ_ENDD
};
#endif 


const WORD gcbBuzErr[]={
	BZ_DO7U, 20, 12,
	BZ_RA6N, 20, 12,
	BZ_DO7U, 20, 12,
	BZ_RA6N, 20, 12,
	BZ_DO7U, 20, 12,
	BZ_RA6N, 20, 12,
	BZ_DO7U, 20, 12,
	BZ_RA6N, 20, 12,
	BZ_DO7U, 20, 12,
	BZ_RA6N, 20, 0,
	BZ_ENDD
};
/**< 에러음/경고음  */



const WORD gcbBuzErr3[]={
	BZ_DO7U, 25, 15,
	BZ_DO7U, 25, 15,
	BZ_DO7U, 25, 20,
	BZ_ENDD
};
/**< 강제잠금음  */



const WORD gcbBuzMag[]={
	BZ_SO6U, 25, 10,
	BZ_SO6U, 25, 127,
	BZ_SO6U, 25, 10,
	BZ_SO6U, 25, 0,
	BZ_ENDD
};
/**< 문닫힘감지음  */



const WORD gcbBuzLow[]={
	BZ_ME6N, 50, 12,
	BZ_SO6N, 50, 12,
	BZ_SO6N, 50, 25,
	BZ_ME6N, 50, 12,
	BZ_SO6N, 50, 12,
	BZ_SO6N, 50, 25,
	BZ_RA6N, 50, 12,
	BZ_RA6N, 50, 12,
	BZ_RA6N, 50, 12,
	BZ_RA6N, 50, 12,
	BZ_RA6N, 100, 0,
	BZ_ENDD
};
/**< 저전압음  */



const WORD gcbBuzWar[]={
	BZ_WAR_START-(BZ_WAR_STEP*0),	10,	0,
	BZ_WAR_START-(BZ_WAR_STEP*1),	 9,	0,
	BZ_WAR_START-(BZ_WAR_STEP*2),	 8,	0,
	BZ_WAR_START-(BZ_WAR_STEP*3),	 7,	0,
	BZ_WAR_START-(BZ_WAR_STEP*4),	 6,	0,
	BZ_WAR_START-(BZ_WAR_STEP*5),	 6,	0,
	BZ_WAR_START-(BZ_WAR_STEP*6),	 7,	0,
	BZ_WAR_START-(BZ_WAR_STEP*7),	 8,	0,
	BZ_WAR_START-(BZ_WAR_STEP*8),	 9,	0,
	BZ_WAR_START-(BZ_WAR_STEP*9),	10,	0,
	BZ_ENDD
};
/**< 침입경보음  */



const WORD gcbBuzBroken[]={
	BZ_FA7U, 130, 0,
	BZ_FA7N, 130, 100,
	BZ_ENDD
};
/**< 파손경보음  */



const WORD gcbBuzFire[]={
	BZ_FA7N, 50, 5,
	BZ_FA7N, 50, 5,
	BZ_FA7N, 50, 5,  
	BZ_FA7N, 50, 100,
	BZ_ENDD
};
/**< 고온경보음  */


const WORD gcbBuzInlock[]={
	BZ_ME7N, 25, 0,
	BZ_ME7N, 1, 0,
	BZ_DO7U, 50, 0,
	BZ_DO7U, 1, 0,
	BZ_ME7N, 25, 0,
	BZ_ME7N, 2, 0,
	BZ_DO7U, 50, 0,
	BZ_DO7U, 1, 0,
	BZ_ENDD
};



const WORD gcbBuzSysFrontNoConnect[]={				//Front 에러용
	BZ_DO6N,40,10,
	BZ_RE6U,40,10,
	BZ_ME6N,40,10,
	BZ_FA6U,45,0,
	BZ_ENDD
};


const WORD gcbBuzSysErr4[]={		
	BZ_SO6N,30,30,
	BZ_SO6N,30,30,
	BZ_SO6N,30,30,
	BZ_SO6N,30,0,
	BZ_ENDD
};
/**< 시스템에러음_4, 메모리 IC(EEPROM) 통신 이상  */



const WORD gcbBuzSysErr5[]={		
	BZ_SO6N,100,0,
	BZ_ENDD
};
/**< 시스템에러음_5, 터치 센서 통신 이상  */



const WORD gcbBuzSysErr6[]={		
	BZ_SO6N,100,30,
	BZ_SO6N,30,0,
	BZ_ENDD
};
/**< 시스템에러음_6, 모터 센서 이상  */



const WORD gcbBuzSysErr7[]={		
	BZ_SO6N,100,30,
	BZ_SO6N,30,30,
	BZ_SO6N,30,0,
	BZ_ENDD
};
/**< 시스템에러음_7, 카드 감지 회로 이상  */



const WORD gcbBuzSysErr8[]={		
	BZ_SO6N,100,30,
	BZ_SO6N,30,30,
	BZ_SO6N,30,30,
	BZ_SO6N,30,0,
	BZ_ENDD
};
/**< 시스템에러음_8, RTC IC 통신 이상  */



const WORD gcbBuzSysErr9[]={		
	BZ_SO6N,100,30,
	BZ_SO6N,30,30,
	BZ_SO6N,30,30,
	BZ_SO6N,30,30,
	BZ_SO6N,30,0,
	BZ_ENDD
};
/**< 시스템에러음_9, 좌수/우수 설정 에러  */

const WORD gcbBuzSysErr10[]={		
	BZ_SO6N,100,30,
	BZ_SO6N,100,0,
	BZ_ENDD
};
/**< 시스템에러음_10, 통신팩 에러  */

const WORD gcbBuzSysErr11[]={		
	BZ_SO6N,100,30,
	BZ_SO6N,100,30,
	BZ_SO6N,30,0,	
	BZ_ENDD
};
/**< 시스템에러음_11, 지문 모듈 에러  */

const WORD gcbBuzSysErr12[]={		
	BZ_SO6N,100,30,
	BZ_SO6N,100,30,
	BZ_SO6N,30,30,	
	BZ_SO6N,30,0,	
	BZ_ENDD
};
/**< 시스템에러음_12, 지문 저장 개수 에러  */

const WORD gcbBuzPackLedControl[]={
	BZ_SO7N, BUZZER_FOREVER, 0,
	BZ_ENDD
};

void SetupBuzzer(void)
{
#ifdef DDL_CFG_NO_BUZZER
	return ;
#else 
	GPIO_InitTypeDef GPIO_InitStructure;

	// buzzer TIM의 PWM 출력을 일단 gpio로 바꾸어 소리가 나지 않게 둔다.
	GPIO_InitStructure.Pin =  BU_FREQ_Pin;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init( BU_FREQ_GPIO_Port, &GPIO_InitStructure);
#endif 	
}


void Setfreq(WORD freq) // 1 to 4000 Hz
{
#ifdef DDL_CFG_NO_BUZZER
		return ;
#else 

	uint32_t	period = 80000 / freq; // compute period as function of 60KHz ticks

/***
	 TIM3->ARR = period - 1;
	 TIM3->CCR4 = period / 2; // channel 2 50/50
***/
	gh_mcu_tim_buzzer->Instance->ARR = period -1;
	gh_mcu_tim_buzzer->Instance->CCR4 = period /2;	// duty 50%

/*
From Datasheet (page 398)


17.3.9 PWM mode

Pulse width modulation mode allows you to generate a signal with a frequency determined
by the value of the TIMx_ARR register and a duty cycle determined by the value of the
TIMx_CCRx register.

The PWM mode can be selected independently on each channel (one PWM per OCx
output) by writing 110 (PWM mode 1) or ‘111 (PWM mode 2) in the OCxM bits in the
TIMx_CCMRx register. You must enable the corresponding preload register by setting the
OCxPE bit in the TIMx_CCMRx register, and eventually the auto-reload preload register (in
upcounting or center-aligned modes) by setting the ARPE bit in the TIMx_CR1 register.

As the preload registers are transferred to the shadow registers only when an update event
occurs, before starting the counter, you have to initialize all the registers by setting the UG
bit in the TIMx_EGR register.

*/
	gh_mcu_tim_buzzer->Instance->EGR = TIM_EGR_UG;		 //PWM 설정을 바꾸고 그 설정으로 PWM shadow register들을 update할 것을 지정한다
#endif 	
}


void BuzzerOff(void)
{
#ifdef DDL_CFG_NO_BUZZER
		return ;
#else  

#ifdef	DDL_CFG_BUZ_VOL_TYPE1
	P_BU_VO_MID(0);
#endif

	P_BU_VO_HIGH(0);
	_BUZ_TIMER_STOP;
	P_BU_FREQ(0);

	gbBuzCnt = 0;								
	gbBuzNum = 0;
	gbBuzPrcsStep = 0;							
#endif 	
}


#if 0
//Buzzer 동작은 4ms를 주기로 계속 반복 처리하도록 되어 있음.
uint32_t	buzz_log[30], buzz_log_l[30], buzz_log_id;
#define	MK_BUZZ_LOG	{ \
		buzz_log[buzz_log_id] = HAL_GetTick(); \
		buzz_log_l[buzz_log_id] = __LINE__; \
		if (buzz_log_id<29) buzz_log_id++; \
}

int	buzzer_p;
#else

#define	MK_BUZZ_LOG

#endif

// 부저 느려짐에 대한 오차 보상한 값을 재정의하여 사용
#define	BUZ_SYS_TIMER_4MS	(SYS_TIMER_4MS - 50)

void BuzzerProcess(void)
{
#ifdef DDL_CFG_NO_BUZZER
	return ;
#else  
	WORD wTmp;

	if(!gbBuzPrcsStep)		return;

	// 0.5초 간격으로 자동 잠김을 확인하게 함으로써 부저 동작이 느려지거나 빨라지는 문제 개선, 
	//	다시 4ms 동작으로 재설정하되, 오차 보상한 값을 별도 정의하여 처리
	wTmp = SysTimerEndCheck(gdwBuzTimerInitCnt, BUZ_SYS_TIMER_4MS);
	//신규 보조키 에서 부저음을 기존 NEC 모델과 비슷한 주기로 내기위해 조정한 값
	//20160705 현재, 부저음을 내면서 ddl_main()을 1회 수행하는 시간이 대략 1.09~1.1msec 이다.
//	wTmp = SysTimerEndCheck(gdwBuzTimerInitCnt, SYS_TIMER_3MS);		// for 3msec
	if(wTmp == 0)
		return;

	SysTimerStart(&gdwBuzTimerInitCnt);

	switch(gbBuzPrcsStep)
	{
		case 0:
			break;
 
		//출력할 부저 주파수 로드 및 부저 ON 시간 로드 단계
		case 1:
			//출력할 부저 주파수 로드
			wTmp = *gbpBuzBuf; 

			if(wTmp	== BZ_ENDD)
			{
				//부저 종료 값일 경우 부저 종료
				BuzzerOff();
 
				if(gbFirstVoice == 0xFF)
				{
					gbFirstVoice = 0;
				}
			}
			else
			{ 						
				//출력할 부저 주파수 설정
				Setfreq(wTmp);
				_BUZ_TIMER_START;

				gbpBuzBuf++;
				//부저 ON 시간 로드
				gbBuzNum = *gbpBuzBuf; 
				gbpBuzBuf++;
				gbBuzCnt = 0;
				if(BUZZER_FOREVER == gbBuzNum)
				{
					gbBuzPrcsStep = 5;
				}
				else
				{
					gbBuzPrcsStep++;
				}
				MK_BUZZ_LOG;
			}
			break;

		//부저 ON 시간 계산
		case 2:
			if(gbBuzCnt >= gbBuzNum) {
				gbBuzPrcsStep++;
				MK_BUZZ_LOG;
			}
			else
				gbBuzCnt++;
			break;
 
		//부저 OFF 시간 로드
		case 3:
			_BUZ_TIMER_STOP;
			P_BU_FREQ(0);

			//부저 OFF 시간 로드
			gbBuzNum = *gbpBuzBuf; 
			gbpBuzBuf++;
			gbBuzCnt = 0;
			gbBuzPrcsStep++;
			MK_BUZZ_LOG;
			break;
 
		//부저 OFF 시간 계산
		case 4:
			if(gbBuzCnt < gbBuzNum)
			{
				gbBuzCnt++;
			}
			else {
				gbBuzPrcsStep = 1;
				MK_BUZZ_LOG;
			}
			break;

		case 5:
			__NOP();
		break;

		default:
			BuzzerOff();
			break;
	}
#endif 	
}



#ifdef	DDL_CFG_BUZ_VOL_TYPE1

//Buzzer 출력 설정 시에는
//출력하고자 하는 Buzzer 종류(bpBuz), 출력하고자 하는 Buzzer 볼륨 적용 여부(bMode)
//bMode의 bit0은 매너모드시 처리 여부, bit1은 볼륨 적용 여부
void BuzzerSetting(WORD const* bpBuz, WORD bMode)
{
//	GPIO_InitTypeDef GPIO_InitStructure;
#ifdef DDL_CFG_NO_BUZZER
	return ;
#else  

	BYTE bTmp;
	
	bTmp = VolumeSettingCheck();

	if((bMode & MANNER_CHK ) && ((bTmp == VOL_SILENT) || gfMute)) 
	{
		return;
	}

	P_BU_VO_MID(0);
	P_BU_VO_HIGH(0);

/*
	if((bMode & VOL_HIGH) || (bTmp == VOL_HIGH)) 	
	{
		P_BU_VO_HIGH(1);
	}
	else 
	{
		P_BU_VO_MID(1);
	}
*/
	if(bMode & VOL_HIGH)
	{
		P_BU_VO_HIGH(1);
	}
	else if(bTmp == VOL_HIGH)
	{
		P_BU_VO_MID(1);
	}

	gbBuzCnt = 0;
	gbBuzNum = 0;
	gbpBuzBuf = bpBuz;
	gbBuzPrcsStep = 1;

//	buzz_log_id = 0;
//	MK_BUZZ_LOG;
#endif 
}

#endif			//DDL_CFG_BUZ_VOL_TYPE1



#ifdef	DDL_CFG_BUZ_VOL_TYPE2

//Buzzer 출력 설정 시에는
//출력하고자 하는 Buzzer 종류(bpBuz), 출력하고자 하는 Buzzer 볼륨 적용 여부(bMode)
//bMode의 bit0은 매너모드시 처리 여부, bit1은 볼륨 적용 여부
void BuzzerSetting(WORD const* bpBuz, WORD bMode)
{
//	GPIO_InitTypeDef GPIO_InitStructure;
#ifdef DDL_CFG_NO_BUZZER
	return ;
#else 

	BYTE bTmp;
	
	bTmp = VolumeSettingCheck();

	if((bMode & MANNER_CHK ) && ((bTmp == VOL_SILENT) || gfMute)) 
	{
		return;
	}

	if((bMode & VOL_HIGH)) 	
	{
		// Buzzer High가 별도로 존재(경고음, 경보음)
		P_BU_VO_HIGH(1);
	}
	else 
	{
		P_BU_VO_HIGH(0);
	}

	gbBuzCnt = 0;
	gbBuzNum = 0;
	gbpBuzBuf = bpBuz;
	gbBuzPrcsStep = 1;

//	buzz_log_id = 0;
//	MK_BUZZ_LOG;
#endif 
}

#endif			//DDL_CFG_BUZ_VOL_TYPE2


BYTE GetBuzPrcsStep(void)
{
#ifdef DDL_CFG_NO_BUZZER
	return 0;
#else 
	return (gbBuzPrcsStep);
#endif 
}


#if 1

static TIM_HandleTypeDef gh_feedback_tim;

const WORD gcbBuzdeletefeedback[]={
	BZ_FA6N-2, 25, 2,
	BZ_SO6N-2, 25, 2,
	BZ_RA6N-2, 25, 2,
	BZ_SI6N-2, 25, 2,
	BZ_DO7N-2, 25, 2,
	BZ_RE7N-2, 25, 2,
	BZ_ME7N-2, 25, 2,
	BZ_FA7N-2, 25, 2,
	BZ_FA7N-2, 25, 2,
	BZ_ME7N-2, 25, 2,
	BZ_RE7N-2, 25, 2,
	BZ_DO7N-2, 25, 2,
	BZ_SI6N-2, 25, 2,
	BZ_RA6N-2, 25, 2,
	BZ_SO6N-2, 25, 2,
	BZ_FA6N-2, 25, 10,
	BZ_ENDD
};

void TIM6_IRQHandler(void)
{
	HAL_TIM_IRQHandler(&gh_feedback_tim);
}

void BuzzerFeedbackinit(void)
{
	TIM_MasterConfigTypeDef sMasterConfig;

	gh_feedback_tim.Instance = TIM6;
	gh_feedback_tim.Init.Prescaler = (SystemCoreClock/1000)-1; // 1ms 
	gh_feedback_tim.Init.CounterMode = TIM_COUNTERMODE_UP;
	gh_feedback_tim.Init.Period = 10-1; // 1ms * 10 = 10ms 마다 IRQ 발생 

	__HAL_RCC_TIM6_CLK_ENABLE();
	HAL_NVIC_SetPriority(TIM6_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(TIM6_IRQn);
	
	HAL_TIM_Base_Init(&gh_feedback_tim);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&gh_feedback_tim, &sMasterConfig);	
}

void BuzzerFeedbackStart(void)
{
	BuzzerFeedbackinit();
	HAL_TIM_Base_Start_IT(&gh_feedback_tim);
}

void BuzzerFeedbackStop(void)
{
	HAL_TIM_Base_Stop_IT(&gh_feedback_tim);		
	HAL_TIM_Base_DeInit(&gh_feedback_tim);
	__HAL_RCC_TIM6_CLK_DISABLE();
	HAL_NVIC_DisableIRQ(TIM6_IRQn);
	BuzzerOff();
}

void BuzzerFeedbackCallback(void)
{
	WORD wTmp;

	switch(gbBuzPrcsStep)
	{
		case 0:
			BuzzerSetting(gcbBuzdeletefeedback, VOL_CHECK);
			break;
 
		//출력할 부저 주파수 로드 및 부저 ON 시간 로드 단계
		case 1:
			//출력할 부저 주파수 로드
			wTmp = *gbpBuzBuf; 

			if(wTmp	== BZ_ENDD)
			{
				//부저 종료 값일 경우 부저 종료
				BuzzerOff();
 
				if(gbFirstVoice == 0xFF)
				{
					gbFirstVoice = 0;
				}
			}
			else
			{ 						
				//출력할 부저 주파수 설정
				Setfreq(wTmp);
				_BUZ_TIMER_START;

				gbpBuzBuf++;
				//부저 ON 시간 로드
				gbBuzNum = *gbpBuzBuf; 
				gbpBuzBuf++;
				gbBuzCnt = 0;
				if(BUZZER_FOREVER == gbBuzNum)
				{
					gbBuzPrcsStep = 5;
				}
				else
				{
					gbBuzPrcsStep++;
				}
				MK_BUZZ_LOG;
			}
			break;

		//부저 ON 시간 계산
		case 2:
			if(gbBuzCnt >= gbBuzNum) {
				gbBuzPrcsStep++;
				MK_BUZZ_LOG;
			}
			else
				gbBuzCnt++;
			break;
 
		//부저 OFF 시간 로드
		case 3:
			_BUZ_TIMER_STOP;

#if !defined (DDL_CFG_NO_BUZZER)	
			P_BU_FREQ(0);
#endif 

			//부저 OFF 시간 로드
			gbBuzNum = *gbpBuzBuf; 
			gbpBuzBuf++;
			gbBuzCnt = 0;
			gbBuzPrcsStep++;
			MK_BUZZ_LOG;
			break;
 
		//부저 OFF 시간 계산
		case 4:
			if(gbBuzCnt < gbBuzNum)
			{
				gbBuzCnt++;
			}
			else {
				gbBuzPrcsStep = 1;
				MK_BUZZ_LOG;
			}
			break;

		case 5:
			__NOP();
		break;

		default:
			BuzzerOff();
			break;
	}
}


#endif 


