#ifndef		_DDL_CONFIG_H_
#define		_DDL_CONFIG_H_


/* Model define start */

//#define 		__DDL_MODEL_MEIJI
//#define		__DDL_MODEL_PASS_110_BF
//#define		__DDL_MODEL_YDM7116_YDM7216
//#define		__DDL_MODEL_YMF30P
//#define		__DDL_MODEL_YMF40P
//#define		__DDL_MODEL_YMF30A
//#define		__DDL_MODEL_YMF40A
//#define		__DDL_MODEL_YMI70
//#define		__DDL_MODEL_A20_IH_MS
//#define		__DDL_MODEL_A330_FH
//#define		__DDL_MODEL_GRAB100_FH
//#define		__DDL_MODEL_VULCAN_F
//#define		__DDL_MODEL_WV40_DMP_JAPAN
//#define		__DDL_MODEL_YDM_4109A
//#define		__DDL_MODEL_YDM_4115A
//#define		__DDL_MODEL_ZEN_F 
//#define		__DDL_MODEL_YDM7116_YDM7216_GOODIX
//#define		__DDL_MODEL_YDM7116_YDM7216_CSL
//#define		__DDL_MODEL_A20_IH
//#define		__DDL_MODEL_B2C15MIN
//#define		__DDL_MODEL_E300_FH
//#define		__DDL_MODEL_J20_IH
//#define		__DDL_MODEL_Rose2_IH
//#define		__DDL_MODEL_PASS_110_F
//#define		__DDL_MODEL_WB_200
//#define		__DDL_MODEL_WE30_DMP
//#define		__DDL_MODEL_WF_20_DMP
//#define		__DDL_MODEL_WF_200
//#define		__DDL_MODEL_EDGE2_DMP
//#define		__DDL_MODEL_WK_20_DMP
//#define		__DDL_MODEL_WP_200
//#define		__DDL_MODEL_WS_200
//#define		__DDL_MODEL_WV40_DMP
//#define		__DDL_MODEL_WV_200
//#define		__DDL_MODEL_YDG413
//#define		__DDL_MODEL_YDG413A
//#define		__DDL_MODEL_YDM_3115A
//#define		__DDL_MODEL_YDM_3109A
//#define		__DDL_MODEL_YDM_3109P
//#define		__DDL_MODEL_YDM_4109P
//#define		__DDL_MODEL_Z10_IH
//#define		__DDL_MODEL_CURVY100_FH
//#define		__DDL_MODEL_PASS700
//#define		__DDL_MODEL_R200_CH
//#define		__DDL_MODEL_YDD424_P
//#define		__DDL_MODEL_YDR30G
//#define		__DDL_MODEL_YDR30GA
//#define		__DDL_MODEL_YDR50G
//#define		__DDL_MODEL_YDR50GA
//#define		__DDL_MODEL_GRL_GR120
//#define		__DDL_MODEL_J20_DM_MS
//#define		__DDL_MODEL_YDM7220 //__DDL_MODEL_YDM4109_FACE
//#define		__DDL_MODEL_YMH70A 
//#define		__DDL_MODEL_YMH71
//#define		__DDL_MODEL_GBP_YG120
//#define		__DDL_MODEL_GRP_YG12B
//#define	       	__DDL_MODEL_GRP_YG14B
//#define		__DDL_MODEL_MONO_FH //#define		__DDL_MODEL_NOVA
//#define		__DDL_MODEL_CURVY100_FHA
//#define		__DDL_MODEL_AK900
//#define		 __DDL_MODEL_YCK241
//#define		 __DDL_MODEL_YCK271RB
//#define		 __DDL_MODEL_YCK371
//#define		 __DDL_MODEL_YCK341
//#define		 __DDL_MODEL_A20A_IH
//#define		 __DDL_MODEL_ROSE2A_IH
//#define		 __DDL_MODEL_J20A_DM
//#define		__DDL_MODEL_YDM7116A
//#define		__DDL_MODEL_YDM7216A
//#define		__DDL_MODEL_YDM7111A
//#define		__DDL_MODEL_YDM3211A
//#define		__DDL_MODEL_YDM4111A
//#define		__DDL_MODEL_YDM7316_KA_CSL
//#define		__DDL_MODEL_YDM4109_ST_M60G // = __DDL_MODEL_YDM4316_KA_M60G
//#define		__DDL_MODEL_YDM7222_KA_CSL
//#define		__DDL_MODEL_YDM4222_KA_CSL
//#define		__DDL_MODEL_YDM3109_ST_M60G // = __DDL_MODEL_YDM3316_KA_M60G
//#define		__DDL_MODEL_YDM3222_KA_M60G
//#define		__DDL_MODEL_YSJ731
//#define		__DDL_MODEL_YSJ741
//#define		__DDL_MODEL_PANPAN_FC2A
//#define		__DDL_MODEL_PANPAN_FC5A
//#define		__DDL_MODEL_YDM_3115V
#define		__DDL_MODEL_PASS700A
//#define		 __DDL_MODEL_GRAB100_KCY
//#define		 __DDL_MODEL_SCREENDOOR
//#define			__DDL_MODEL_X500_FH




 /* Model define end */



/* Model config start */
#if defined	(__DDL_MODEL_MEIJI)
#include	"ddl_config-meiji.h"
#include	"DefinePin-meiji.h"

#elif defined	(__DDL_MODEL_PASS_110_BF)
#include	"ddl_config-pass110-bf.h"
#include	"DefinePin-pass110-bf.h"

#elif defined	(__DDL_MODEL_YDM7116_YDM7216)
#include	"ddl_config-ydm7116_ydm7216.h"
#include	"DefinePin-ydm7116_ydm7216.h"

#elif defined	(__DDL_MODEL_YMF30P)
#include	"ddl_config-ymf30p.h"
#include	"DefinePin-ymf30p.h"

#elif defined	(__DDL_MODEL_YMF40P)
#include	"ddl_config-ymf40p.h"
#include	"DefinePin-ymf40p.h"

#elif defined	(__DDL_MODEL_YMF30A)
#include	"ddl_config-ymf30a.h"
#include	"DefinePin-ymf30a.h"

#elif defined	(__DDL_MODEL_YMF40A)
#include	"ddl_config-ymf40a.h"
#include	"DefinePin-ymf40a.h"

#elif defined	(__DDL_MODEL_YMI70)
#include	"ddl_config-ymi70.h"
#include	"DefinePin-ymi70.h"

#elif defined	(__DDL_MODEL_A20_IH_MS)
#include	"ddl_config-a20-ih_MS.h"
#include	"DefinePin-a20-ih_MS.h"

#elif defined	(__DDL_MODEL_A330_FH)
#include	"ddl_config-a330-fh.h"
#include	"DefinePin-a330-fh.h"

#elif defined	(__DDL_MODEL_GRAB100_FH)
#include	"ddl_config-grab100-fh.h"
#include	"DefinePin-grab100_fh.h"

#elif defined	(__DDL_MODEL_VULCAN_F)
#include	"ddl_config-vulcan-f.h"
#include	"DefinePin-vulcan-f.h"

#elif defined	(__DDL_MODEL_WV40_DMP_JAPAN)
#include	"ddl_config-wv40-japan.h"
#include	"DefinePin-a20-ih.h"

#elif defined	(__DDL_MODEL_YDM_3115A)
#include	"ddl_config-ydm3115a.h"
#include	"DefinePin-ydm3115a.h"

#elif defined	(__DDL_MODEL_YDM_3115V)
#include	"ddl_config-ydm3115v.h"
#include	"DefinePin-ydm3115v.h"

#elif defined	(__DDL_MODEL_YDM_3109A)
#include	"ddl_config-ydm3109a.h"
#include	"DefinePin-ydm3109a.h"

#elif defined	(__DDL_MODEL_YDM_4109A)
#include	"ddl_config-ydm4109a.h"
#include	"DefinePin-ydm4109a.h"

#elif defined	(__DDL_MODEL_YDM_4115A)
#include	"ddl_config-ydm4115a.h"
#include	"DefinePin-ydm4115a.h"

#elif defined	(__DDL_MODEL_ZEN_F)
#include	"ddl_config-zenf.h"
#include	"DefinePin-zenf.h"

#elif defined	(__DDL_MODEL_YDM7116_YDM7216_GOODIX)
#include	"ddl_config-ydm7116_ydm7216_goodix.h"
#include	"DefinePin-ydm7116_ydm7216_goodix.h"

#elif defined	(__DDL_MODEL_YDM7116_YDM7216_CSL)
#include	"ddl_config-ydm7116_ydm7216_goodix_CSL.h"
#include	"DefinePin-ydm7116_ydm7216_goodix_CSL.h"

#elif defined	(__DDL_MODEL_YCK241)
#include	"ddl_config-yck241.h"
#include	"DefinePin-yck241.h"

#elif defined	(__DDL_MODEL_YCK271RB)
#include	"ddl_config-yck271rb.h"
#include	"DefinePin-yck271rb.h"

#elif defined	(__DDL_MODEL_YCK371)
#include	"ddl_config-yck371.h"
#include	"DefinePin-yck371.h"

#elif defined	(__DDL_MODEL_YCK341)
#include	"ddl_config-yck341.h"
#include	"DefinePin-yck341.h"

#elif defined	(__DDL_MODEL_A20_IH)
#include	"ddl_config-a20-ih.h"
#include	"DefinePin-a20-ih.h"

#elif defined	(__DDL_MODEL_B2C15MIN)
#include	"Ddl_config-b2c15min.h"
#include	"DefinePin-b2c15min.h"

#elif defined	(__DDL_MODEL_E300_FH)
#include	"ddl_config-e300-fh.h"
#include	"DefinePin-e300-fh.h"

#elif defined	(__DDL_MODEL_J20_IH)
#include	"ddl_config-j20-ih.h"
#include	"DefinePin-a20-ih.h"

#elif defined	(__DDL_MODEL_Rose2_IH)
#include	"ddl_config-rose2-ih.h"
#include	"DefinePin-a20-ih.h"

#elif defined	(__DDL_MODEL_PASS_110_F) || defined (__DDL_MODEL_WB_200)
#ifdef __DDL_MODEL_WB_200
#define	__DDL_MODEL_PASS_110_F
#endif 
#include	"ddl_config-pass110-f.h"
#include	"DefinePin-pass110-f.h"

#elif defined	(__DDL_MODEL_WE30_DMP)
#include	"ddl_config-we30-dmp.h"
#include	"DefinePin-a20-ih.h"

#elif defined	(__DDL_MODEL_WF_20_DMP)
#include	"ddl_config-wf-20.h"
#include	"DefinePin-wf-20.h"

#elif defined	(__DDL_MODEL_WF_200)
#include	"ddl_config-wf-200.h"
#include	"DefinePin-wf-200.h"

#elif defined	(__DDL_MODEL_EDGE2_DMP) || defined (__DDL_MODEL_WK_20_DMP)
#ifdef __DDL_MODEL_WK_20_DMP
#define	__DDL_MODEL_EDGE2_DMP
#endif 
#include	"ddl_config-edge2-dmp.h"
#include	"DefinePin-a20-ih.h"

#elif defined	(__DDL_MODEL_WP_200)
#include	"ddl_config-wp-200.h"
#include	"DefinePin-wp-200.h"

#elif defined	(__DDL_MODEL_WS_200)
#include	"ddl_config-ws-200.h"
#include	"DefinePin-ws-200.h"

#elif defined	(__DDL_MODEL_WV40_DMP)
#include	"ddl_config-we40-dmp.h"
#include	"DefinePin-a20-ih.h"

#elif defined	(__DDL_MODEL_WV_200)
#include	"ddl_config-wv-200.h"
#include	"DefinePin-wv-200.h"

#elif defined	(__DDL_MODEL_YDG413)
#include	"ddl_config-ydg413.h"
#include	"DefinePin-ydg413.h"

#elif defined	(__DDL_MODEL_YDG413A)
#include	"ddl_config-ydg413a.h"
#include	"DefinePin-ydg413a.h"

#elif defined	(__DDL_MODEL_YDM_3109P)
#include	"Ddl_config-ydm3109p.h"
#include	"DefinePin-ydm3109p.h"

#elif defined	(__DDL_MODEL_YDM_4109P)
#include	"Ddl_config-ydm4109p.h"
#include	"DefinePin-ydm4109p.h"

#elif defined	(__DDL_MODEL_Z10_IH)
#include	"ddl_config-z10-ih.h"
#include	"DefinePin-z10-ih.h"

#elif defined	(__DDL_MODEL_CURVY100_FH)
#include	"ddl_config-curvy100-fh.h"
#include	"DefinePin-curvy100-fh.h"

#elif defined	(__DDL_MODEL_PASS700)
#include	"ddl_config-curvy100-fh.h"
#include	"DefinePin-pass700.h"

#elif defined	(__DDL_MODEL_R200_CH)
#include	"ddl_config-R200-CH.h"
#include	"DefinePin-R200-CH.h"

#elif defined	(__DDL_MODEL_YDD424_P)
#include	"ddl_config-ydd424p.h"
#include	"DefinePin-ydd424p.h"

#elif defined	(__DDL_MODEL_YDR30G)
#include	"ddl_config-ydr30g.h"
#include	"DefinePin-ydr30g.h"

#elif defined	(__DDL_MODEL_YDR30GA)
#include	"ddl_config-ydr30ga.h"
#include	"DefinePin-ydr30ga.h"

#elif defined	(__DDL_MODEL_YDR50G)
#include	"ddl_config-ydr50g.h"
#include	"DefinePin-ydr50g.h"

#elif defined	(__DDL_MODEL_GRL_GR120)
#include	"Ddl_config-grl-gr120.h"
#include	"DefinePin-grl-gr120.h"

#elif defined	(__DDL_MODEL_J20_DM_MS)
#include	"ddl_config-j20-dm-ms.h"
#include	"DefinePin-j20-dm-ms.h"

#elif defined	(__DDL_MODEL_YDM7220)
#include	"ddl_config-ydm7220.h"
#include	"DefinePin-ydm7220.h"

#elif defined	(__DDL_MODEL_YMH70A)
#include	"ddl_config-ymh70a.h"
#include	"DefinePin-ymh70a.h"

#elif defined	(__DDL_MODEL_YMH71)
#include	"ddl_config-ymh71.h"
#include	"DefinePin-ymh71.h"

#elif defined	(__DDL_MODEL_GBP_YG120)
#include	"ddl_config-gbp-yg120(HIO).h"
#include	"DefinePin-gbp-yg120(HIO).h"

#elif defined	(__DDL_MODEL_GRP_YG12B)
#include	"ddl_config-grp-yg12B.h"
#include	"DefinePin-grp-yg12B.h"

#elif defined	(__DDL_MODEL_GRP_YG14B)
#include	"ddl_config-grp-yg14b.h"
#include	"DefinePin-grp-yg14b.h"

#elif defined	(__DDL_MODEL_MONO_FH) || defined (__DDL_MODEL_NOVA)
#ifdef __DDL_MODEL_NOVA
#define	__DDL_MODEL_MONO_FH
#endif 
#include	"ddl_config-nova.h"
#include	"DefinePin-nova.h"

#elif defined	(__DDL_MODEL_CURVY100_FHA)
#include	"ddl_config-curvy100-fhA.h"
#include	"DefinePin-curvy100-fhA.h"

#elif defined	(__DDL_MODEL_AK900)
#include	"ddl_config-ak900.h"
#include	"DefinePin-ak900.h"

#elif defined	(__DDL_MODEL_A20A_IH)
#include	"ddl_config-a20a-ih.h"
#include	"DefinePin-a20a-ih.h"

#elif defined	(__DDL_MODEL_ROSE2A_IH)
#include	"ddl_config-rose2a-ih.h"
#include	"DefinePin-rose2a-ih.h"

#elif defined	(__DDL_MODEL_J20A_DM)
#include	"ddl_config-j20a-dm.h"
#include	"DefinePin-j20a-dm.h"

#elif defined	(__DDL_MODEL_YDM7116A)
#include	"ddl_config-YDM7116A.h"
#include	"DefinePin-YDM7116A.h"

#elif defined	(__DDL_MODEL_YDM7216A)
#include	"ddl_config-YDM7216A.h"
#include	"DefinePin-YDM7216A.h"

#elif defined	(__DDL_MODEL_YDM7111A)
#include	"ddl_config-ydm7111a.h"
#include	"DefinePin-ydm7111a.h"

#elif defined	(__DDL_MODEL_YDM3211A)
#include	"ddl_config-ydm3211a.h"
#include	"DefinePin-ydm3211a.h"

#elif defined	(__DDL_MODEL_YDM4111A)
#include	"ddl_config-ydm4111a.h"
#include	"DefinePin-ydm4111a.h"

#elif defined	(__DDL_MODEL_YDM7316_KA_CSL)
#include	"ddl_config-ydm7316_ka_csl.h"
#include	"DefinePin-ydm7316_ka_csl.h"

#elif defined	(__DDL_MODEL_YDM4316_KA_M60G)
#include	"ddl_config-ydm4316_ka_m60g.h"
#include	"DefinePin-ydm4316_ka_m60g.h"

#elif defined	(__DDL_MODEL_YDM4109_ST_M60G)
#include	"ddl_config-ydm4109_st_m60g.h"
#include	"DefinePin-ydm4109_st_m60g.h"

#elif defined	(__DDL_MODEL_YDM7222_KA_CSL)
#include	"ddl_config-ydm7222_ka_csl.h"
#include	"DefinePin-ydm7222_ka_csl.h"

#elif defined	(__DDL_MODEL_YDM4222_KA_CSL)
#include	"ddl_config-ydm4222_ka_csl.h"
#include	"DefinePin-ydm4222_ka_csl.h"

#elif defined	(__DDL_MODEL_YDM3316_KA_M60G)
#include	"ddl_config-ydm3316_ka_m60g.h"
#include	"DefinePin-ydm3316_ka_m60g.h"

#elif defined	(__DDL_MODEL_YDM3109_ST_M60G)
#include	"ddl_config-ydm3109_st_m60g.h"
#include	"DefinePin-ydm3109_st_m60g.h"

#elif defined	(__DDL_MODEL_YDM3222_KA_M60G)
#include	"ddl_config-ydm3222_ka_m60g.h"
#include	"DefinePin-ydm3222_ka_m60g.h"

#elif defined	(__DDL_MODEL_YSJ731)
#include	"ddl_config-YSJ731.h"
#include	"DefinePin-YSJ731.h"

#elif defined	(__DDL_MODEL_YSJ741)
#include	"ddl_config-YSJ741.h"
#include	"DefinePin-YSJ741.h"

#elif defined	(__DDL_MODEL_PANPAN_FC2A)
#include	"ddl_config-panpan_fc2a.h"
#include	"DefinePin-panpan_fc2a.h"

#elif defined	(__DDL_MODEL_PANPAN_FC5A)
#include	"ddl_config-panpan_fc5a.h"
#include	"DefinePin-panpan_fc5a.h"

#elif defined	(__DDL_MODEL_PASS700A)
#include	"ddl_config-pass700a.h"
#include	"DefinePin-pass700a.h"

#elif defined	(__DDL_MODEL_GRAB100_KCY)
#include	"ddl_config-grab100-kcy.h"
#include	"DefinePin-grab100_kcy.h"

#elif defined	(__DDL_MODEL_SCREENDOOR)
#include	"ddl_config-screendoor.h"
#include	"DefinePin-screendoor.h"

#elif defined	(__DDL_MODEL_X500_FH)
#include	"ddl_config-x500-fh.h"
#include	"DefinePin-x500-fh.h"


#else 
#error "Should define Model Define"
#endif 
/* Model config end */

#if 0
// comment for product, un-comment for debugging
#undef		DDL_CFG_WATCHDOG
#endif 

#endif
