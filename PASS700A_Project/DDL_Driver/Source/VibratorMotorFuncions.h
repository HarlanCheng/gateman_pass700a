#ifndef		_VIBRATOR_MOTOR_FEEDBACK_H_
#define _VIBRATOR_MOTOR_FEEDBACK_H_


enum{
	/*HapticList 의 순서와 맞춰 주세요*/
	VM_FEEDBACK_VERIFY_OK  = 0x00,
	VM_FEEDBACK_VERIFY_FAIL  = 0x01,
	VM_FEEDBACK_MAX = 0xFF
};

extern WORD gbVMTimer10ms;
extern BYTE gbVMPrcsStep;

void VibratorMotorFeedback(BYTE Index);
void VibratorMotorProcess(void);
#endif

