//------------------------------------------------------------------------------
/** 	@file		PackTxFunctions_T1.c
	@version 0.1.00
	@date	2016.06.13
	@brief	Communication Pack TX Process Functions
	@see	
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.06.13		by Jay
			- 통신팩을 TX Data 전송 처리에 대한 함수
*/
//------------------------------------------------------------------------------

#include "Main.h"


WORD gbPackTxByteCnt = 0;		// 송신한 바이트 수 
WORD gbPackTxByteNum = 0;	// 송신할 바이트 수 
BYTE gbPackTxDataBuf[MAX_PACK_BUF_SIZE];

BYTE gbPackTxDataQueue[MAX_PACK_TX_QUEUE_SIZE][MAX_PACK_BUF_SIZE];
BYTE gbPackTxEnDataSendbuffer[MAX_PACK_BUF_SIZE];
WORD gbPackTxEnDataByteNum = 0;

BYTE gbPackTxRandomKeybuffer[16];
BYTE gbpackModeSuspend = 0x00;


BYTE gbPackTxWaitTimeQueue[MAX_PACK_TX_QUEUE_SIZE];
WORD gbPackTxQueue_front = 0;		// 송신 Queue의 맨 앞부분
WORD gbPackTxQueue_rear = 0;		// 송신 Queue의 맨 뒷부분
WORD gbPackTxQueue_sending = 0;	// 송신중인 Queue index
BYTE gbPackTxByteNum_Queue = 0;	// 송신할 Queue의 바이트 수
BYTE gbPackTxAddNone[10];
bool gfAckWait = false;				// true : 패킷 송신후 응답(Ack, Nack) 대기중
bool gfQueueSending = false;		// true : 패킷 송신중
bool gfDirectSending = false;			// true : Main Loop - PackTxDataProcess 기다리지 않고 직접 송신중

#define	PACK_TX_ENQUEUE_FIRSTLY		0x00
#define	PACK_TX_ENQUEUE_LASTLY		0xFF
BYTE gbPackTxEnqueueOrder = PACK_TX_ENQUEUE_LASTLY;	// 0x00 : Pack Tx Queue에 추가시 맨 앞에 추가(먼저 보낼 내용 추가시 사용), 0xFF : 맨 뒤에 추가(보통 상태)

BYTE gbRetryCount = 0x00;
BYTE gbAckCompleteData[2] = {0x00,0x00};


/*
#define	PACK_TX_ENSIG_TLUS_DEFAULT		300
#define	PACK_TX_ENSIG_TLMS_DEFAULT		0
#define	PACK_TX_ENSIG_TWUUS_DEFAULT		500
#define	PACK_TX_ENSIG_TWUMS_DEFAULT		0
*/


#define	PACK_TX_ENSIG_TLUS_DEFAULT		100
#define	PACK_TX_ENSIG_TLMS_DEFAULT		0
#define	PACK_TX_ENSIG_TWUUS_DEFAULT		150
#define	PACK_TX_ENSIG_TWUMS_DEFAULT		0



BYTE gPackTxProcessStep = 0;
WORD gPackTxEnSigTimer2ms = 0;

WORD gPackTxEnSigTl1us = 0;
WORD gPackTxEnSigTl1ms = 0;
WORD gPackTxEnSigTwu1us = 0;
WORD gPackTxEnSigTwu1ms = 0;


WORD gPackTxAckWaitTimer10ms = 0;
BYTE gPackTxWaitTimer10ms = 0;

BYTE gPackTxAckWaitIndex;
BYTE gPackTxAckWaitTimer100ms;
/*
gbAckWaitBuffer[][timer,count,Datalength,Data .........]
*/
BYTE gbAckWaitBuffer[ACK_WAIT_BUFFER_SIZE][MAX_PACK_BUF_SIZE+3];

//------------------------------------------------------------------------------------
// 저전압 경고 신호 전송
//	문 열림/닫힘 신호와 충돌하지 않도록 저전압일 때 문 열림/닫힘 후 5초 후에 전송

BYTE gbPackLowBatteryReportMode = 0; 
BYTE gbPackLowBatteryReportTimer1s = 0;
WORD gbPackLockStatusReportTimer = 0;




BYTE Get_Checksum_BCC(BYTE *buf, BYTE len)
{
	BYTE i;
	BYTE x = 0;

	for(i = 0 ;  i < len; i++)
	{
		x ^= buf[i];
	}
	return x;
}



WORD Get_Checksum_CRC16(BYTE *buf, BYTE len)
{
	WORD crc = 0xffff; // initial value
	WORD x;
	BYTE i;

	for(i = 0; i < len; i++)
	{
		x = ((crc >> 8) ^ buf[i]) & 0xff;
		x ^= x >> 4;
		crc = (crc << 8) ^ (x << 12) ^ (x << 5) ^ x;
	}
	return crc;
}

bool PackTx_QueueIsFull(void)
{
	if(((gbPackTxQueue_rear+1) % MAX_PACK_TX_QUEUE_SIZE) == gbPackTxQueue_front)
		return true;
	else
		return false;
}

void PackTx_MakeNack(BYTE* bpData , BYTE bMajor_Code, BYTE bMinor_Code)	// RF Module이 잘못된 명령을 보낸 경우 Negative Ack를 보냄
{
	BYTE		tmpArray[3] = {0,0,0};
	BYTE		tmpSize = 0;
	
	if(gbModuleMode == 0)		return;

	if(gbModuleProtocolVersion >= 0x30)
	{
		tmpArray[0] = bMajor_Code;
		tmpArray[1] = bMinor_Code;
		tmpSize = 2;
	}
	else
	{	
		tmpArray[0] = 0x00;
		tmpSize = 0;
	}	

	PackTx_MakePacketSinglePort(bpData[PACK_F_EVENT], ES_LOCK_NACK, &bpData[PACK_F_COUNT], tmpArray, tmpSize);
}

void PackTx_MakePacketSinglePort(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength)
{
	BYTE bCnt;

	if(gbModuleMode)
	{
		gbPackTxDataBuf[PACK_F_EVENT] = bEvent;
		gbPackTxDataBuf[PACK_F_SOURCE] = bEventSrc;
		if(bEventSrc == ES_LOCK_EVENT)	(*bCount)++;			// Event 패킷일 때만 Count 증가
		gbPackTxDataBuf[PACK_F_COUNT] = (*bCount);
		gbPackTxDataBuf[PACK_F_LENGTH_ADDDATA] = bLength ;	// checksum(1바이트) 길이 추가 
		for(bCnt = 0; bCnt < bLength; bCnt++)
		{
			gbPackTxDataBuf[4 + bCnt] = *bpData;
			bpData++;
		}
		gbPackTxByteNum = 4 + bLength;
		PackTx_AddChecksum(gbPackTxDataBuf, &gbPackTxByteNum);

		if((!gfDirectSending) &&	!PackTx_QueueIsFull())	PackTx_EnQueue();
	}
}

void PackTx_MakePacket(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength)
{
	BYTE bCnt;
	BYTE* bpInner;

	bpInner = bpData;

	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		gbPackTxDataBuf[PACK_F_EVENT] = bEvent;
		gbPackTxDataBuf[PACK_F_SOURCE] = bEventSrc;

		if(bEventSrc == ES_LOCK_EVENT)	
		{
			//(*bCount)++;			// Event 패킷일 때만 Count 증가
			gbComCnt++;
		}

		gbPackTxDataBuf[PACK_F_COUNT] = gbComCnt;
		gbPackTxDataBuf[PACK_F_LENGTH_ADDDATA] = bLength ;	// checksum(1바이트) 길이 추가 

		for(bCnt = 0; bCnt < bLength; bCnt++)
		{
			gbPackTxDataBuf[4 + bCnt] = *bpData;
			bpData++;
		}
		
		gbPackTxByteNum = 4 + bLength;
		
		PackTx_AddChecksum(gbPackTxDataBuf, &gbPackTxByteNum);

		if((!gfDirectSending) &&	!PackTx_QueueIsFull())
		{
			PackTx_EnQueue();
		}
	}

	bpData = bpInner;

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		gbInnerPackTxDataBuf[PACK_F_EVENT] = bEvent;
		gbInnerPackTxDataBuf[PACK_F_SOURCE] = bEventSrc;

		if(bEventSrc == ES_LOCK_EVENT)	
		{
			//(*bCount)++;			// Event 패킷일 때만 Count 증가
			gbInnerComCnt++;
		}

		gbInnerPackTxDataBuf[PACK_F_COUNT] = gbInnerComCnt;
		gbInnerPackTxDataBuf[PACK_F_LENGTH_ADDDATA] = bLength ; // checksum(1바이트) 길이 추가 

		for(bCnt = 0; bCnt < bLength; bCnt++)
		{
			gbInnerPackTxDataBuf[4 + bCnt] = *bpData;
			bpData++;
		}
		
		gbInnerPackTxByteNum = 4 + bLength;
		
		InnerPackTx_AddChecksum(gbInnerPackTxDataBuf, &gbInnerPackTxByteNum);

		if((!gfDirectSending) &&	!InnerPackTx_QueueIsFull())
		{
			InnerPackTx_EnQueue();
		}
	}
}


void PackTx_EnQueue(void)		// Queue에 추가 하기 : 방금 준비한 데이터를 Queue 맨 뒤에 추가하기 (순서대로 처리)
{
	BYTE bCnt;

	gbPackTxQueue_rear = (gbPackTxQueue_rear + 1) % MAX_PACK_TX_QUEUE_SIZE;
	for(bCnt = 0; bCnt < gbPackTxByteNum; bCnt++)
	{
		gbPackTxDataQueue[gbPackTxQueue_rear][bCnt] = gbPackTxDataBuf[bCnt];
	}
	// 바로 전송
	gbPackTxWaitTimeQueue[gbPackTxQueue_rear] = 0;
}

void PackTxWaitTime_EnQueue(BYTE TxWaitTime)
{
	gbPackTxWaitTimeQueue[gbPackTxQueue_rear] = TxWaitTime;
}

void PackTx_AddChecksum(BYTE *buf, WORD *len)
{
	buf[*len] = Get_Checksum_BCC(buf,*len);
	*len = *len + 1;
}

void PackAlarmReportLowBatterySet(void)
{
	gbPackLowBatteryReportMode = 1;
	gbPackLowBatteryReportTimer1s = 5;
}



void PackAlarmReportLowBatteryCounter(void)
{
	if(gbPackLowBatteryReportTimer1s)	--gbPackLowBatteryReportTimer1s;
}


void PackTxAlarmReportLowBatteryProcess(void)
{
	switch(gbPackLowBatteryReportMode)
	{
		case 1:
			if(gbPackLowBatteryReportTimer1s)	break;

			if(GetPackTxProcessStep())		break;
			if(GetPackRxProcessStep())		break;
			if(GetInnerPackTxProcessStep()) break;			
			if(GetInnerPackRxProcessStep())		break;						

			PackTx_MakeAlarmPacket(AL_LOW_BATTERY, 0x00, 0x01);
			gbPackLowBatteryReportMode = 0;
			break;

		default:
			break;
	}			
}

void ZWaveZigbeeLockStatusAlarmRepot(void)
{
	BYTE	tmpArray[3] = {0,0,0};
	BYTE	tmpSize = 0;
	BYTE 	tmpEvent = 0x00;

	//gbPackLockStatusReportTimer 는 stop mode 에서 빠져 나올때 한번 증가 한다.
	//stopmode -> stopmode 시간은 약 1.5s (card 모델 기준)
	// 3 시간 10800s / 1.5s = 7200
	if(((gbModuleMode ==PACK_ID_CONFIRMED) && gfPackTypeZBZWModule) ||
				((gbInnerModuleMode ==PACK_ID_CONFIRMED) && gfInnerPackTypeZBZWModule))
	{
		if(gbPackLockStatusReportTimer++ >= 7200)	//3시간
		{
			gbPackLockStatusReportTimer = 0;
			gbWakeUpMinTime10ms = 50;	
			
			if((gbModuleMode ==PACK_ID_CONFIRMED) && gfPackTypeZBZWModule)
			{
				if(gbModuleProtocolVersion >= 0x30)
				{
					tmpArray[0] = AL_LOCK_STATUS_REPORT;
					tmpArray[1] = 0x00;
					tmpArray[2] = LockStsCheck();
					tmpSize = 3;
					tmpEvent = F0_ALARM_REPORT_EX;
				}
				else
				{
					tmpArray[0] = AL_LOCK_STATUS_REPORT;
					tmpArray[1] = LockStsCheck();
					tmpSize = 2;
					tmpEvent = F0_ALARM_REPORT;
				}
				PackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, tmpSize);
			}
			else
			{
				if(gbInnerModuleProtocolVersion >= 0x30)
				{
					tmpArray[0] = AL_LOCK_STATUS_REPORT;
					tmpArray[1] = 0x00;
					tmpArray[2] = LockStsCheck();
					tmpSize = 3;
					tmpEvent = F0_ALARM_REPORT_EX;
				}
				else
				{
					tmpArray[0] = AL_LOCK_STATUS_REPORT;
					tmpArray[1] = LockStsCheck();
					tmpSize = 2;
					tmpEvent = F0_ALARM_REPORT;
				}
				InnerPackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, tmpSize);
			}
		}
	}
}


BYTE GetPackTxProcessStep(void)
{
	return (gPackTxProcessStep); 
}

#if defined (P_COM_DDL_EN_T)

void PackTx_QueueInit(void)
{
	gbPackTxQueue_front = 0;
	gbPackTxQueue_rear = 0;
	gbPackTxAddNone[0] = 0;
	gfAckWait = false;
//	gbPackTxEnqueueOrder = PACK_TX_ENQUEUE_LASTLY;
}



bool PackTx_QueueIsEmpty(void)
{
	if(gbPackTxQueue_front == gbPackTxQueue_rear)
		return true;
	else
		return false;
}

void PackTx_EnQueueAtFirst(void)	// Queue 맨 앞에 추가 하기 :  방금 준비한 데이터를 가장 먼저 보내기 위해 보낼 데이터를 Queue 맨 앞에 추가하기 
{
//	BYTE bCnt;
//
//	if(PackTx_QueueIsFull())
//	{
//		for(bCnt = 0; bCnt < gbPackTxByteNum; bCnt++)
//			gbPackTxDataQueue[gbPackTxQueue_front][bCnt] = gbPackTxDataBuf[bCnt];
//		gbPackTxQueue_front = (gbPackTxQueue_front + (MAX_PACK_TX_QUEUE_SIZE - 1)) % MAX_PACK_TX_QUEUE_SIZE;
//		gbPackTxQueue_rear = (gbPackTxQueue_rear + (MAX_PACK_TX_QUEUE_SIZE - 1)) % MAX_PACK_TX_QUEUE_SIZE;
//	}
//	else
//	{
//		for(bCnt = 0; bCnt < gbPackTxByteNum; bCnt++)
//			gbPackTxDataQueue[gbPackTxQueue_front][bCnt] = gbPackTxDataBuf[bCnt];
//		gbPackTxQueue_front = (gbPackTxQueue_front + (MAX_PACK_TX_QUEUE_SIZE - 1)) % MAX_PACK_TX_QUEUE_SIZE;
//	}
}



void PackTx_DeQueueStart(void)	// Dequeue 시작(송신 준비)
{
	gbPackTxQueue_sending = (gbPackTxQueue_front + 1) % MAX_PACK_TX_QUEUE_SIZE;
	gbPackTxByteNum_Queue = gbPackTxDataQueue[gbPackTxQueue_sending][3] + 5;	// Additional Data이외에 5바이트(Event, Source, Count, length, Checksum) 추가
}



void PackTx_DeQueueEnd(void)	// Dequeue 마무리
{
	gbPackTxQueue_front = (gbPackTxQueue_front + 1) % MAX_PACK_TX_QUEUE_SIZE;
//	gbSendingWait[1] = true;
}

void PackTx_MakePacketForInitial(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength)
{
	BYTE bCnt;

	gbPackTxDataBuf[PACK_F_EVENT] = bEvent;
	gbPackTxDataBuf[PACK_F_SOURCE] = bEventSrc;
	if(bEventSrc == ES_LOCK_EVENT)	(*bCount)++;			// Event 패킷일 때만 Count 증가
	gbPackTxDataBuf[PACK_F_COUNT] = (*bCount);
	gbPackTxDataBuf[PACK_F_LENGTH_ADDDATA] = bLength ;	// checksum(1바이트) 길이 추가 
	for(bCnt = 0; bCnt < bLength; bCnt++)
	{
		gbPackTxDataBuf[4 + bCnt] = *bpData;
		bpData++;
	}
	gbPackTxByteNum = 4 + bLength;
	PackTx_AddChecksum(gbPackTxDataBuf, &gbPackTxByteNum);

//	if(gbPackTxEnqueueOrder == PACK_TX_ENQUEUE_FIRSTLY)
//	{
//		PackTx_EnQueueAtFirst();
//	}
//	else
//	{
		if((!gfDirectSending) &&	!PackTx_QueueIsFull())	PackTx_EnQueue();
//	}
}



// 실행 시간이 2ms를 초과할 수 있으므로 메인 루프 타이밍을 깰 수 있기 때문에 오래 걸리는 명령에 대한 Ack송신시에만 사용할 것
void PackTx_MakeAndSendPacketNow(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength)
{
	/* PackTx_MakeAndSendPacketNow 한수는 현재 TI TWU 제어를 못하고 있다 */
	/* 꼭 바로 uart 를 사용 해야 해서 이함수를 사용할 경우 TI TWU 제어 부분 구현 할것 */
	/* 현재는 queue 에 무조건 쌓도록 하여 해당 항수를 호출 하는 부분을 없엠 */ 
	
	if(gbModuleMode == 0)		return;

	gfDirectSending = true;	// Main Loop - PackTxDataProcess 기다리지 않고 직접 송신중
	PackTx_MakePacket(bEvent, bEventSrc, bCount, bpData, bLength);
	UartSendCommPack(&gbPackTxDataBuf[PACK_F_EVENT], gbPackTxByteNum);
	while(1)			// 5바이트 패킷 송신시 2.7ms 소요(Debugger CycleCounter로 측정한 것)
	{
		if(!gfDirectSending)		break;
#ifdef	DDL_CFG_WATCHDOG
		RefreshIwdg();
#endif
	}
}

void PackTx_MakeAlarmPacket(BYTE bAlarmType, BYTE bAlarmLevel_H, BYTE bAlarmLevel_L)		// Alarm Event Packet 준비
{
	BYTE		tmpArray[4] = {0,0,0,0};
	BYTE		tmpSize = 0;
	BYTE 	tmpEvent = 0x00;

#if defined (_USE_LOGGING_MODE_)
	tmpArray[0] = F0_ALARM_REPORT;
	tmpArray[1] = bAlarmType;
	tmpArray[2] = bAlarmLevel_H; 
	tmpArray[3] = bAlarmLevel_L;
	SaveLog(tmpArray,4);
#endif 
	
	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbModuleProtocolVersion >= 0x30)
		{
			tmpArray[0] = bAlarmType;
			tmpArray[1] = bAlarmLevel_H;
			tmpArray[2] = bAlarmLevel_L;
			tmpSize = 3;
			tmpEvent = F0_ALARM_REPORT_EX;
		}
		else 
		{
			tmpArray[0] = bAlarmType;
			tmpArray[1] = bAlarmLevel_L;
			tmpSize = 2;
			tmpEvent = F0_ALARM_REPORT;
		}
		PackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray,tmpSize);			
		PackTxWaitTime_EnQueue(10);		
	}

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbInnerModuleProtocolVersion >= 0x30)
		{
			tmpArray[0] = bAlarmType;
			tmpArray[1] = bAlarmLevel_H;
			tmpArray[2] = bAlarmLevel_L;
			tmpSize = 3;
			tmpEvent = F0_ALARM_REPORT_EX;
		}
		else 
		{
			tmpArray[0] = bAlarmType;
			tmpArray[1] = bAlarmLevel_L;
			tmpSize = 2;
			tmpEvent = F0_ALARM_REPORT;
		}
		InnerPackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray,tmpSize);			
		InnerPackTxWaitTime_EnQueue(10);	
	}
}

void PackDataTransmit(void)	//ST에서 사용 하는 함수 : void PackDataTransmitProcess(void)
{
//	PackTxEndProcess();
}



void PackDataTransmitProcess(void)	// 1 Packet 송신시마다 호출됨
{
	if(JigModeStatusCheck() == STATUS_SUCCESS)
	{
		//__no_operation();
		if(!PackTx_QueueIsEmpty())
		{
			PackTx_QueueInit();
		}
	}
	else
	{
		if(!gfDirectSending)		PackTx_DeQueueEnd();		// PackConnectionMode 아니면  Tx Queue 처리 
		gfDirectSending = false;
	}
	gfQueueSending = false;	// 송신 완료 표시 
}



void PackTxEnSigSetDefault(void)
{
	gPackTxEnSigTl1us = PACK_TX_ENSIG_TLUS_DEFAULT;
	gPackTxEnSigTl1ms = PACK_TX_ENSIG_TLMS_DEFAULT;
	gPackTxEnSigTwu1us = PACK_TX_ENSIG_TWUUS_DEFAULT;
	gPackTxEnSigTwu1ms = PACK_TX_ENSIG_TWUMS_DEFAULT;
}



void PackTxEnSigTimeSet(BYTE* pTimeData)
{
	BYTE bTmp;

	BYTE TlTime;
	BYTE TlUnitDefine;
	BYTE TlMutiPlier;

	BYTE TwuTime;
	BYTE TwuUnitDefine;
	BYTE TwuMutiPlier;

	if(DataCompare(pTimeData, 0x00, 3) == 0)
	{
		PackTxEnSigSetDefault();
		return;
	}

	if(DataCompare(pTimeData, 0xFF, 3) == 0)
	{
		PackTxEnSigSetDefault();
		return;
	}

	bTmp = (pTimeData[2] & 0xF0) >> 4;
	TlUnitDefine = bTmp & 0x08;
	TlMutiPlier = bTmp & 0x07;

	TlTime = pTimeData[0]; 

	if(TlUnitDefine)
	{
		gPackTxEnSigTl1us = 0;
		gPackTxEnSigTl1ms = TlTime * TlMutiPlier;
	}
	else
	{
		gPackTxEnSigTl1us = TlTime * TlMutiPlier;
		gPackTxEnSigTl1ms = 0;
	}

	
	bTmp = (pTimeData[2] & 0x0F);
	TwuUnitDefine = bTmp & 0x08;
	TwuMutiPlier = bTmp & 0x07;

	TwuTime = pTimeData[1]; 

	if(TwuUnitDefine)
	{
		gPackTxEnSigTwu1us = 0;
		gPackTxEnSigTwu1ms = TwuTime * TwuMutiPlier;
	}
	else
	{
		gPackTxEnSigTwu1us = TwuTime * TwuMutiPlier;
		gPackTxEnSigTwu1ms = 0;
	}
}




#define	PACK_TX_ENSIG_MINIMUM_TIME		100		// Delay로 처리하는 최소 처리 단위 100ms


WORD PackTxEnSigDelayProcess(WORD EnSigTxus, WORD EnSigTxms, BYTE CompensationValue)
{
	WORD EnSigTimer2ms = 0;

	if(EnSigTxus)
	{
		//CubeMx code를 사용하다보니 1usec 단위의 delay를 구현 못해서 10usec로 구현한 단위로 변환
		EnSigTxus = EnSigTxus/10;

		// 처리 시간 포함 등으로 인해 Delay가 맞지 않아 보정이 필요한 경우 10us 단위의 보정 값을 반영하여 계산
		if(EnSigTxus > (WORD)CompensationValue)
		{
			EnSigTxus -= (WORD)CompensationValue;
		}
		
		Delay(SYS_TIMER_10US*EnSigTxus);
	}
	else if(EnSigTxms < PACK_TX_ENSIG_MINIMUM_TIME)
	{
		//보조키 통합하면서 만든 Delay()의 최대 지연시간은 0.65535 sec, PACK_TX_ENSIG_MINIMUM_TIME 이 시간을 넘지 않도로 한다.
		Delay(SYS_TIMER_1MS*EnSigTxms);
	}
	else
	{
		// 1ms 정도는 무시 (PACK_TX_ENSIG_MINIMUM_TIME 이 100ms 정도로 큰 경우에 해당)
/*
		if(EnSigTxms & 0x01)
		{
			// 홀수일 경우 1ms를 Delay 함수로 처리
			Delay(SYS_TIMER_1MS);
			EnSigTxms--;
		}		
*/
		// Delay 최소 시간 보다 클 경우에는 Time Count 처리로 수행
		EnSigTimer2ms = (EnSigTxms >> 1);
	}

	return (EnSigTimer2ms);
}



void PackTxProcessStepStart(WORD AckWaitTime)
{
//	BYTE bCnt;
//	BYTE bTmp;
//	
//	bTmp = 0;
//	for(bCnt = 0; bCnt < gbPackTxByteNum; bCnt++)
//	{
//		bTmp ^= gbPackTxDataBuf[bCnt];
//	}
//	gbPackTxDataBuf[gbPackTxByteNum] = bTmp;
//
//	gbPackTxByteNum++;
//	gbPackTxByteCnt = 0;
//
//	gPackTxProcessStep = PACK_TX_PACKET_SENDING_MODE;
//	gPackTxAckWaitTimer10ms = AckWaitTime;
}



void PackTxProcessStepClear(void)
{
//	gPackTxProcessStep = PACK_TX_WAITING_MODE; 
}

void PackTxProcessTimeCounter(void)
{
	if(gPackTxEnSigTimer2ms) 	--gPackTxEnSigTimer2ms;
}


void PackTxWaitTimeCounter(void)
{
	if(gPackTxWaitTimer10ms)		--gPackTxWaitTimer10ms;
}



void PackTxAckWaitTimeCounter(void)
{
	if(gPackTxAckWaitTimer10ms)		--gPackTxAckWaitTimer10ms;
}
	
void PackTxAckClearCheck(BYTE* bpData)
{
	BYTE i;
	BYTE Temp = 0x00;

	if(gbModuleProtocolVersion >= 0x30)
	{
		for(i = 0x00 ; i < ACK_WAIT_BUFFER_SIZE ; i++)
		{
			if(gbAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] == 0x00)
			{
				//timer 가 0 이면 buffer 비어 있는것 
				Temp++;
				continue;
			}
			else 
			{
				if(gbAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX] == bpData[0] && ES_MODULE_ACK == bpData[1] &&
					gbAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+2] == bpData[2])
				{
					//ack 왔으니 timer , count clear
					Temp++;
					gbAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] = gbAckWaitBuffer[i][ACK_WAIT_COUNT_INDEX] = 0x00;
				}
			}
		}

		if(Temp == ACK_WAIT_BUFFER_SIZE)
		{
			// buffer 가 다 비었음 sleep 모드로 
			gPackTxAckWaitTimer100ms = 0x00;
		}
	}
}

void PackTxAckWaitCallback(void)
{
	BYTE i;

	if(gbModuleProtocolVersion >= 0x30)
	{
		if(gPackTxAckWaitTimer100ms)
		{
			--gPackTxAckWaitTimer100ms; // 이 timer 는 늘 최신으로 갱신 된다 

			for(i = 0x00 ; i < ACK_WAIT_BUFFER_SIZE ; i++)
			{
				if(gbAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] == 0x00)
				{
					//timer 가 0 이면 buffer 비어 있는것
					continue;
				}
				else
				{
					// timer 감소 
					gbAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX]--;

					if(gbAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] == 0x00)
					{
						if(gbAckWaitBuffer[i][ACK_WAIT_COUNT_INDEX])
						{
							//timer 가 만료 되었고 , count 가 남아 있으면 tx queue 에 넣는다 
							BYTE bCnt;

							gbAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] = 0xFF;

							// PackTx_EnQueue() 을 쓰면 for 을 두번 돌아야 함 
							gbPackTxQueue_rear = (gbPackTxQueue_rear + 1) % MAX_PACK_TX_QUEUE_SIZE;
							for(bCnt = 0; bCnt < gbAckWaitBuffer[i][ACK_WAIT_DATA_LENGTH_INDEX] ; bCnt++)
							{
								gbPackTxDataQueue[gbPackTxQueue_rear][bCnt] = gbAckWaitBuffer[i][bCnt+3];
							}
							gbPackTxWaitTimeQueue[gbPackTxQueue_rear] = 0;
						}
						else
						{		
							// timer 도 끝났고 ack 도 못받은 경우 
#ifdef	_NEW_YALE_ACCESS_UI
							if((gbAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE)
								|| (gbAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
								|| (gbAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+PACK_F_EVENT] == F0_RFM_CHECK))
#else	// _NEW_YALE_ACCESS_UI
							if(gbAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE ||
	 							gbAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
#endif	// _NEW_YALE_ACCESS_UI
							{
								// F0_ENCRYPTION_KEY_CHANGE  F0_ENCRYPTION_GET_CHECKSUM 를 최종 실패 하면 
								// susped mode 
								gbpackModeSuspend = 0x01;
								gbModuleMode = PACK_ID_NOT_CONFIRMED; 						
							}
						}
					}
				}
			}		
		}
	}
	else
	{
		gPackTxAckWaitTimer100ms = 0; 
	}
}

BYTE PackTxAckCheck(BYTE* bpData)
{
	BYTE i;

	for(i = 0x00 ; i < ACK_WAIT_BUFFER_SIZE ; i++)
	{
		if(gbAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] == 0x00)
		{
			//timer 가 0 이면 buffer 비어 있는것
			continue;
		}
		else
		{
			if(memcmp(&gbAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX],bpData,3) == 0x00)
			{
				// buffer 에 보냈던 것이 있으니 count , timer 갱신 
				gbAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] = gPackTxAckWaitTimer100ms;
				gbAckWaitBuffer[i][ACK_WAIT_COUNT_INDEX]--; // = gbAckWaitBuffer[i][ACK_WAIT_COUNT_INDEX] - 0x01;
				return 0xFF;
			}
		}
	}
	return 0x00;
}

void PackTxEndProcess(void)
{
//	if(gPackTxAckWaitTimer10ms)
//	{
//		gPackTxProcessStep = PACK_TX_WAITING_MODE;
//	}
//	else
//	{
//		gPackTxProcessStep = PACK_TX_WAITING_MODE;
//	}
}





void PackTx_EnSigSendingStart(void)
{
	P_COM_HCP_EN(0);
	gPackTxEnSigTimer2ms = PackTxEnSigDelayProcess(gPackTxEnSigTl1us, gPackTxEnSigTl1ms, 0);
	if(gPackTxEnSigTimer2ms)	// Ti가 2ms 이상이면 
	{
		gPackTxProcessStep = PACK_TX_EN_SIG_TI_SENDING_MODE;
	}
	else						// Ti가 2ms 미만이면 
	{
		P_COM_HCP_EN(1);
		// us 단위에서는 Uart 시작까지 40us 처리 시간이 소요되어 보정 값 사용
		gPackTxEnSigTimer2ms = PackTxEnSigDelayProcess(gPackTxEnSigTwu1us, gPackTxEnSigTwu1ms, 4);
		if(gPackTxEnSigTimer2ms)	// Twu가 2ms 이상이면 
		{
			gPackTxProcessStep = PACK_TX_EN_SIG_TWU_SENDING_MODE;
		}
		else						// Twu가 2ms 미만이면 
		{
//			UartSendCommPack(gbPackTxDataBuf, gbPackTxByteNum);
			gPackTxAckWaitTimer10ms = 10;

			if(gbModuleProtocolVersion >= 0x30 && gbPackTxDataQueue[gbPackTxQueue_sending][0] != 0x44 && gbEncryptionType == 0x01)
			{
				UartSendCommPack(gbPackTxEnDataSendbuffer, gbPackTxEnDataByteNum);
			}
			else
			{
				UartSendCommPack(&gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT], gbPackTxByteNum_Queue);
			}
			gfQueueSending = true;		// 송신 진행중 표시
			gPackTxProcessStep = PACK_TX_PACKET_SENDING_MODE;
		}
	}
}




void PackTxDataProcess(void)
{
	switch(gPackTxProcessStep)
	{
		case PACK_TX_WAITING_MODE:	// TX 대기중
			// TX QUEUE에 패킷 준비되면  TX 패킷 확인중으로 이동
			if((!PackTx_QueueIsEmpty()) && (!gfAckWait))
			{
				PackTx_DeQueueStart();

				if(gbModuleProtocolVersion >= 0x30 && gbPackTxDataQueue[gbPackTxQueue_sending][0] != 0x44 && gbEncryptionType == 0x01) //AES128 ECB
				{
					gPackTxProcessStep = PACK_TX_ENCRYPT_MODE;
				}	
				else
				{
					gPackTxProcessStep = PACK_TX_PACKET_CHECKING_MODE;
				}
			}
			break;


		case PACK_TX_PACKET_CHECKING_MODE:	// TX 패킷 확인중 
			// 보낼 패킷이 명령이면 RX 대기중이 되면 EN 신호 발생 시작 후 TX EN 신호 발생중으로 이동
			if(gbPackTxDataQueue[gbPackTxQueue_sending][1] == ES_LOCK_EVENT)
			{
				if(gPackRxProcessStep == PACK_RX_WAITING_MODE)
				{
//					PackTx_EnSigSendingStart();

					gPackTxWaitTimer10ms = gbPackTxWaitTimeQueue[gbPackTxQueue_sending]; 
					gPackTxProcessStep = PACK_TX_EN_SIG_TI_SENDING_WAIT;
				}
			}
			// 보낼 패킷이 ACK이면  EN 신호 발생 시작 후 TX EN 신호 송신중으로 이동
			else
			{
//				PackTx_EnSigSendingStart();

				gPackTxWaitTimer10ms = gbPackTxWaitTimeQueue[gbPackTxQueue_sending]; 
				gPackTxProcessStep = PACK_TX_EN_SIG_TI_SENDING_WAIT;
			}
			break;

		case PACK_TX_EN_SIG_TI_SENDING_WAIT:
			if(gPackTxWaitTimer10ms)	break;

			PackTx_EnSigSendingStart();
			break;

		case PACK_TX_EN_SIG_TI_SENDING_MODE:		// TX EN 신호 Ti 송신중
			// Pack Wakeup 신호(Ti) 완료이면 TX EN 신호 Twu 송신중으로 이동
			if(gPackTxEnSigTimer2ms)	break;

			P_COM_HCP_EN(1);
			// Ti 가 100ms 넘어갈 경우 Twu 를 위해 다시 timming 계산 
			gPackTxEnSigTimer2ms = PackTxEnSigDelayProcess(gPackTxEnSigTwu1us, gPackTxEnSigTwu1ms, 4);
			gPackTxProcessStep = PACK_TX_EN_SIG_TWU_SENDING_MODE;
			break;


		case PACK_TX_EN_SIG_TWU_SENDING_MODE:		// TX EN 신호 Twu 송신중
			// Pack Wakeup 신호(Twu) 완료이면 패킷 첫바이트 송신 후 TX 패킷 송신중으로 이동
			if(gPackTxEnSigTimer2ms)	break;

			gPackTxAckWaitTimer10ms = 10;

			if(gbModuleProtocolVersion >= 0x30 && gbPackTxDataQueue[gbPackTxQueue_sending][0] != 0x44 && gbEncryptionType == 0x01)
			{
				UartSendCommPack(gbPackTxEnDataSendbuffer, gbPackTxEnDataByteNum);
			}
			else 
			{
				UartSendCommPack(&gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT], gbPackTxByteNum_Queue);
			}
			gfQueueSending = true;		// 송신 진행중 표시
			gPackTxProcessStep = PACK_TX_PACKET_SENDING_MODE;
			break;

#if 1 // retry 
		case PACK_TX_PACKET_SENDING_MODE:	
			// TX send 중 
			// TX send 가 gPackTxAckWaitTimer10ms 0 되기 전에 완료시 gfQueueSending true -> false 보 변경됨 
			// Tx send 가 완료가 되면 Lock event 인지 판단 lock event 면 ack wait 상태로 천이 
			
			if(gPackTxAckWaitTimer10ms == 0)	{
				// Tx complete 가 안된 경우 
				gfQueueSending = false;
				gbRetryCount = 0;
				gPackTxProcessStep = PACK_TX_WAITING_MODE;
				break;
			}
			
			if(gfQueueSending == false)
			{
				// 0x44 는 retry 처리를 따로 하므로 제외 
				// ES_LOCK_EVENT 만 처리 
				// smart living 2.0 이상만 retry 처리 
				if(gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_SOURCE] == ES_LOCK_EVENT &&
					gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT] != 0x44 &&
					(gbModuleProtocolVersion >= 0x30))
				{
					// 내가 보낸 event 인지 gbAckCompleteData 저장 
					// 5초 간격으로 2회 retry  
					gfAckWait = true;	

#ifdef	_NEW_YALE_ACCESS_UI
					if((gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE) 
						|| (gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
						|| (gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT] == F0_RFM_CHECK))
#else
					if(gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE ||
						gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
#endif
					{
						// F0_ENCRYPTION_KEY_CHANGE  F0_ENCRYPTION_GET_CHECKSUM 는 
						// 500ms
						gPackTxAckWaitTimer100ms = 5; 
					}					
					else 
					{
						gPackTxAckWaitTimer100ms = 50;
					}

					if(PackTxAckCheck(&gbPackTxDataQueue[gbPackTxQueue_sending][0]) == 0x00)
					{
						//buffer 에없는 새로운 packet 이면 buffer 에 넣고 간다 
						gbAckWaitBuffer[gPackTxAckWaitIndex][ACK_WAIT_TIMER_INDEX] = gPackTxAckWaitTimer100ms; //timer 
						gbAckWaitBuffer[gPackTxAckWaitIndex][ACK_WAIT_COUNT_INDEX] = 2; //retry count 
						gbAckWaitBuffer[gPackTxAckWaitIndex][ACK_WAIT_DATA_LENGTH_INDEX] = gbPackTxByteNum_Queue; //length
						memcpy(&gbAckWaitBuffer[gPackTxAckWaitIndex][ACK_WAIT_DATA_START_INDEX],&gbPackTxDataQueue[gbPackTxQueue_sending][0],gbPackTxByteNum_Queue);
						gPackTxAckWaitIndex++;
						if(gPackTxAckWaitIndex > ACK_WAIT_BUFFER_SIZE)
						{
							gPackTxAckWaitIndex = 0x00;
						}
					}
					gfAckWait = false;
					gbRetryCount = 0;
					gPackTxProcessStep = PACK_TX_WAITING_MODE;
				}
				else														// 송신 내용이 응답(Ack, Nack)이면 
				{
					gfAckWait = false;
					gbRetryCount = 0;
					gPackTxProcessStep = PACK_TX_WAITING_MODE;
				}
			}
			break;
#if 0 // 구형 retry 
		case PACK_TX_ACK_WAITING_MODE:		// 응답(Ack, Nack) 대기중
			// ACK 수신완료이거나 TX TIME OUT이면 재전송 
			if(gPackTxAckWaitTimer10ms == 0){

				if(gbRetryCount > 1)
				{
					gbRetryCount = 0; 	// 2번 retry 완료 

					if(gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE ||
 						gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
					{
						// F0_ENCRYPTION_KEY_CHANGE  F0_ENCRYPTION_GET_CHECKSUM 를 최종 실패 하면 
						// susped mode 
						gbpackModeSuspend = 0x01;
						gbModuleMode = PACK_ID_NOT_CONFIRMED; 						
					}
				}
				else
				{
					// 1 회 2회 보내는 경우 
					gbRetryCount++;
					gbWakeUpMinTime10ms = 2; //sleep 에 안들어가게 하기 위해 

					// 여기 까지 왔으면 이미 dequeue 상태 					
					// queue 를 재 정리 하는것 보다 보낸 tx index 를 다시 조정 
					if(gbPackTxQueue_front)
					{
						gbPackTxQueue_front--;
					}
					else
					{
						gbPackTxQueue_front = MAX_PACK_TX_QUEUE_SIZE;
					}
				}
				// index 를 조정 했으니 PACK_TX_WAITING_MODE 보내서 다시 send 하도록 
				gfAckWait = false;
				gPackTxProcessStep = PACK_TX_WAITING_MODE;
				break;
			}
			
			if(gfAckWait == false)	
			{
				// 보낸 event 의 ack 를 성공 적으로 받으면 다음 frame 보낼 수 있도록 
				gPackTxAckWaitTimer10ms = 0;
				gbRetryCount = 0;
				gPackTxProcessStep = PACK_TX_WAITING_MODE;
			}
			
			break;
#endif 

			case PACK_TX_ENCRYPT_MODE:	
				PackTxEncapsulateData(&gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_EVENT],gbPackTxByteNum_Queue);
				gPackTxProcessStep = PACK_TX_PACKET_CHECKING_MODE;
				break;

#else 
		case PACK_TX_PACKET_SENDING_MODE:	// TX 패킷 송신중
			// TX 패킷 송신완료이고  보낸 패킷이 명령이면 ACK 수신대기중으로 이동
			// TX 패킷 송신완료이고  보낸 패킷이 ACK이면 TX 대기중으로 이동
			if(gPackTxAckWaitTimer10ms == 0)	gfQueueSending = false;
			if(gfQueueSending == false)
			{
				if(gbPackTxDataQueue[gbPackTxQueue_sending][PACK_F_SOURCE] == ES_LOCK_EVENT)
				{
					gfAckWait = true;	// 응답 대기중 표시
					gPackTxAckWaitTimer10ms = 50;
					gPackTxProcessStep = PACK_TX_ACK_WAITING_MODE;
				}
				else														// 송신 내용이 응답(Ack, Nack)이면 
				{
					gPackTxProcessStep = PACK_TX_WAITING_MODE;
				}
			}
			break;


		case PACK_TX_ACK_WAITING_MODE:		// 응답(Ack, Nack) 대기중
			// ACK 수신완료이거나 TX TIME OUT이면 TX 대기중으로 이동
			if(gPackTxAckWaitTimer10ms == 0)	gfAckWait = false;
			if(gfAckWait == false)	gPackTxProcessStep = PACK_TX_WAITING_MODE;
			
			break;
#endif 

		default:
			gPackTxProcessStep = PACK_TX_WAITING_MODE;
			break;
	}
}

WORD gSlotNumberForPack = 0;

void CopySlotNumberForPack(WORD SlotNumber)
{
	gSlotNumberForPack = SlotNumber;
}


WORD GetSlotNumberForPack(void)
{
	return (gSlotNumberForPack);
}



BYTE gCredentialDataForPackBuf[16];

void CopyCredentialDataForPack(BYTE* bDataBuf, BYTE bNum)
{
	BYTE bCnt;

	if(bNum > 16)	return;

	for(bCnt = 0; bCnt < bNum; bCnt++)
	{
		gCredentialDataForPackBuf[bCnt] = bDataBuf[bCnt];
	}
}


void GetCredentialDataForPack(BYTE* bDataBuf, BYTE bNum)
{
	BYTE bCnt;

	if(bNum > 16)	return;

	for(bCnt = 0; bCnt < bNum; bCnt++)
	{
		bDataBuf[bCnt] = gCredentialDataForPackBuf[bCnt];
	}
}



void PackTxEventPincodeAdded(WORD wUserSlot)
{
	BYTE i, bTmpArray[MAX_PIN_LENGTH+3];
	BYTE bBcdArray[8], bAsciiArray[16];
	BYTE tmpSize = 0;
	BYTE tmpEvent = 0;

#if defined (_USE_LOGGING_MODE_)
	bTmpArray[0] = F0_USER_ADDED;
	bTmpArray[1] = 0x00;
	bTmpArray[2] = ((wUserSlot >> 8) & 0xFF);
	bTmpArray[3] = (uint8_t)wUserSlot;
	SaveLog(bTmpArray,4);
#endif

	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbModuleProtocolVersion >= 0x30)
		{
			if(wUserSlot == RET_MASTER_CODE_MATCH)
			{
				wUserSlot = RET_2BYTE_MASTER_CODE_MATCH;
			}
			else if(wUserSlot == RET_TEMP_CODE_MATCH)
			{
				wUserSlot = 0xFFF0;
			}

			bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
			bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
			bTmpArray[2] = USER_STATUS_OCC_ENABLED;
			GetCredentialDataForPack(bBcdArray, 8);

			BcdToAscii(bBcdArray, bAsciiArray);
			for(i = 0; i < MAX_PIN_LENGTH; i++)
			{
				if(bAsciiArray[i] == 0xFF)	break;
				bTmpArray[i + 3] = bAsciiArray[i];
			}
			tmpSize = i+3;
			tmpEvent = F0_USER_ADDED_EX;
		}
		else 
		{
			bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
			bTmpArray[1] = USER_STATUS_OCC_ENABLED;
			GetCredentialDataForPack(bBcdArray, 8);

			BcdToAscii(bBcdArray, bAsciiArray);
			for(i = 0; i < MAX_PIN_LENGTH; i++)
			{
				if(bAsciiArray[i] == 0xFF)	break;
				bTmpArray[i + 2] = bAsciiArray[i];
			}
			tmpSize = i+2;
			tmpEvent = F0_USER_ADDED;
		}
		PackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbComCnt, bTmpArray,tmpSize);		
	}

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbInnerModuleProtocolVersion >= 0x30)
		{
			if(wUserSlot == RET_MASTER_CODE_MATCH)
			{
				wUserSlot = RET_2BYTE_MASTER_CODE_MATCH;
			}
			else if(wUserSlot == RET_TEMP_CODE_MATCH)
			{
				wUserSlot = 0xFFF0;
			}

			bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
			bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
			bTmpArray[2] = USER_STATUS_OCC_ENABLED;
			GetCredentialDataForPack(bBcdArray, 8);

			BcdToAscii(bBcdArray, bAsciiArray);
			for(i = 0; i < MAX_PIN_LENGTH; i++)
			{
				if(bAsciiArray[i] == 0xFF)	break;
				bTmpArray[i + 3] = bAsciiArray[i];
			}
			tmpSize = i+3;
			tmpEvent = F0_USER_ADDED_EX;
		}
		else 
		{
			bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
			bTmpArray[1] = USER_STATUS_OCC_ENABLED;
			GetCredentialDataForPack(bBcdArray, 8);

			BcdToAscii(bBcdArray, bAsciiArray);
			for(i = 0; i < MAX_PIN_LENGTH; i++)
			{
				if(bAsciiArray[i] == 0xFF)	break;
				bTmpArray[i + 2] = bAsciiArray[i];
			}
			tmpSize = i+2;
			tmpEvent = F0_USER_ADDED;
		}
		InnerPackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbInnerComCnt, bTmpArray,tmpSize);		
	}
}


void PackTxEventPincodeDeleted(WORD wUserSlot)
{
	BYTE bTmpArray[4];
	BYTE tmpSize = 0;
	BYTE tmpEvent = 0;	

#if defined (_USE_LOGGING_MODE_)
	bTmpArray[0] = F0_USER_DELETED;
	bTmpArray[1] = 0x00;
	bTmpArray[2] = ((wUserSlot >> 8) & 0xFF);
	bTmpArray[3] = (uint8_t)wUserSlot;
	SaveLog(bTmpArray,4);
#endif

	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbModuleProtocolVersion >= 0x30)
		{
			bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
			bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
			tmpSize = 2;
			tmpEvent = F0_USER_DELETED_EX;
		}
		else 
		{
			bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
			tmpSize = 1;
			tmpEvent = F0_USER_DELETED;			
		}
		PackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbComCnt, bTmpArray,tmpSize);		
	}

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbInnerModuleProtocolVersion >= 0x30)
		{
			bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
			bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
			tmpSize = 2;
			tmpEvent = F0_USER_DELETED_EX;
		}
		else 
		{
			bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
			tmpSize = 1;
			tmpEvent = F0_USER_DELETED;
		}
		InnerPackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbInnerComCnt, bTmpArray,tmpSize);		
	}
}



void PackTxEventCredentialAdded(BYTE CredentialType, WORD wUserSlot)
{
	BYTE bTmpArray[12];
	BYTE tmpSize = 0;

#if defined (_USE_LOGGING_MODE_)
	bTmpArray[0] = EV_USER_CARD_ADDED;
	bTmpArray[1] = CredentialType;
	bTmpArray[2] = ((wUserSlot >> 8) & 0xFF); 
	bTmpArray[3] = (uint8_t)wUserSlot;
	SaveLog(bTmpArray,4);
#endif 

	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		bTmpArray[0] = CredentialType;
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		bTmpArray[2] = USER_STATUS_OCC_ENABLED;
		if(wUserSlot == 0xFF)
		{
			memset(&bTmpArray[3], 0xFF, 8);
		}
		else
		{
			GetCredentialDataForPack(&bTmpArray[3], 8);
		}
		tmpSize = 11;
		PackTx_MakePacketSinglePort(EV_USER_CARD_ADDED, ES_LOCK_EVENT, &gbComCnt, bTmpArray,tmpSize);		
	}

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		bTmpArray[0] = CredentialType;
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		bTmpArray[2] = USER_STATUS_OCC_ENABLED;
		if(wUserSlot == 0xFF)
		{
			memset(&bTmpArray[3], 0xFF, 8);
		}
		else
		{
			GetCredentialDataForPack(&bTmpArray[3], 8);
		}
		tmpSize = 11;
		InnerPackTx_MakePacketSinglePort(EV_USER_CARD_ADDED, ES_LOCK_EVENT, &gbInnerComCnt, bTmpArray,tmpSize);		
	}
}

void PackTxEventCredentialDeleted(BYTE CredentialType, WORD wUserSlot)
{
	BYTE bTmpArray[4];
	BYTE tmpSize = 0;

#if defined (_USE_LOGGING_MODE_)
	bTmpArray[0] = EV_USER_CARD_DELETED;
	bTmpArray[1] = CredentialType;
	bTmpArray[2] = ((wUserSlot >> 8) & 0xFF);
	bTmpArray[3] = (uint8_t)wUserSlot;
	SaveLog(bTmpArray,4);
#endif 

	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		bTmpArray[0] = CredentialType;
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		tmpSize = 2;
		PackTx_MakePacketSinglePort(EV_USER_CARD_DELETED, ES_LOCK_EVENT, &gbComCnt, bTmpArray,tmpSize);		
	}

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		bTmpArray[0] = CredentialType;
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		tmpSize = 2;
		InnerPackTx_MakePacketSinglePort(EV_USER_CARD_DELETED, ES_LOCK_EVENT, &gbInnerComCnt, bTmpArray,tmpSize);		
	}
}




void PackAlarmReportInnerForcedLockOpenCloseFailSend(void) //hyojoon.kim 
{
	BYTE bdata = 0x00;
	
	/* PackAlarmReportInnerForcedLockOpenCloseFailSend 는 FeedbackLockOut 이후에 만 불리니 gbMotorSensorStatus 로 
	열림 닫힘만 체크, 여기 왔다는 얘긴 내부 강제 상태 인데 open 이나 close 하려 하는 것임  */
	
	switch (MotorSensorCheck()) 
	{
		case SENSOR_CLOSELOCK_STATE : //모터 닫힘 상태 에서 열려고 함 
			bdata = 0x02; 
			break;

		case SENSOR_OPENLOCK_STATE : 
		case SENSOR_CENTERLOCK_STATE:
		case SENSOR_LOCK_STATE:
			bdata = 0x01;  			//모터 열림 상태 에서 닫으려고 함 

		default : 
			break;
	}

	if(bdata)
	{
		PackTx_MakeAlarmPacket(AL_UNLOCK_FAIL_BY_FORCED_LOCKED, 0x00, bdata);
	}
}

#endif 


#ifdef	DDL_CFG_ADD_REMOCON_LINK_FUNCTION
void PackTxRemoconLinkSend(BYTE bData)
{
	PackTx_MakePacket(EV_REMOCON_CMD_SEND, ES_LOCK_EVENT, &gbComCnt, &bData, 1);
}
#endif

void PackTxEncapsulateData(BYTE *bQueueSendingData , uint16_t length)
{
	BYTE bTmpArray[48] = {0x00,};	// zero padding 하기 귀찮아서... 
	BYTE bTmpkey[16]  = {0x00,};
	uint32_t Encryptsize = ((length /16U) + ((length % 16 == 0) ? 0 : 1)) * 16; // AES128 를 돌릴 size 판단 
	
	memcpy(bTmpArray,bQueueSendingData,length); // 원래 size 만큼 복사 

	if(gbMenuExchangeKey == 0x01 && gbpackModeSuspend)
	{
		//key 가 맞지 않아 suspend 상태 이면서 menu 로 강제 key 교환 하는 경우 
		//suspend 상태가 아니고 key 교환을 하는 경우는 자기가 가지고 있는 key 로 한다 
		bTmpkey[0] = 0x59;
		bTmpkey[1] = 0x61;
		bTmpkey[2] = 0x6c;
		bTmpkey[3] = 0x65;
		bTmpkey[4] = 0x49;
		bTmpkey[5] = 0x72;
		bTmpkey[6] = 0x65;
		bTmpkey[7] = 0x76;
		bTmpkey[8] = 0x6f;
		bTmpkey[9] = 0x53;
		bTmpkey[10] = 0x65;
		bTmpkey[11] = 0x63;
		bTmpkey[12] = 0x75;
		bTmpkey[13] = 0x72;
		bTmpkey[14] = 0x65;
		bTmpkey[15] = 0x64;
	}
	else
	{
		RomRead(bTmpkey,COM_PACK_ENCRYPT_KEY,16);
		
#ifdef _USE_IREVO_CRYPTO_
		EncryptDecryptKey(bTmpkey,sizeof(bTmpkey));
#endif 
	}

	/* 실제 보낼 data 를 만들어줌 */
	gbPackTxEnDataSendbuffer[PACK_F_EVENT] = F0_ENCRYPTION_MESSAGE;
	
	if(bQueueSendingData[PACK_F_SOURCE] ==  ES_LOCK_NACK)
	{
		gbPackTxEnDataSendbuffer[PACK_F_SOURCE] = ES_LOCK_ACK; // E1 에 대한 NACK 존재 불가 
	}
	else
	{
		gbPackTxEnDataSendbuffer[PACK_F_SOURCE] = bQueueSendingData[PACK_F_SOURCE];
	}
	gbPackTxEnDataSendbuffer[PACK_F_COUNT] = bTmpArray[PACK_F_COUNT];
	gbPackTxEnDataSendbuffer[PACK_F_LENGTH_ADDDATA] = Encryptsize;

#ifdef _USE_STM32_CRYPTO_LIB_
	DataEncrypt(bTmpArray,Encryptsize,bTmpkey,&gbPackTxEnDataSendbuffer[PACK_F_ADDDATA]); // AES128 ECB 	
#endif 

	gbPackTxEnDataByteNum = Encryptsize + 4;

	PackTx_AddChecksum(gbPackTxEnDataSendbuffer, &gbPackTxEnDataByteNum);

}

void PackTxGetCheckSumOfExisting(void)
{
	BYTE bTmpArray = 0x00;
	PackTx_MakePacketSinglePort(F0_ENCRYPTION_GET_CHECKSUM, ES_LOCK_EVENT, &gbComCnt,&bTmpArray,0);
}

BYTE PackTxEncryptionKeyExchange(BYTE* bCheckSumData)
{
	BYTE bTmpArray[16] = {0x00,};
	WORD TmpCheckSum = (WORD)(((bCheckSumData[0] << 8) & 0xFF00) | bCheckSumData[1]);

	RomRead(bTmpArray,COM_PACK_ENCRYPT_KEY,16);
	
#ifdef _USE_IREVO_CRYPTO_
	EncryptDecryptKey(bTmpArray,sizeof(bTmpArray));
#endif 

	if((Get_Checksum_CRC16(bTmpArray,16) & 0xFFFF) != TmpCheckSum)
	{
		gbpackModeSuspend = 0x01;
		gbModuleMode = PACK_ID_NOT_CONFIRMED;
		return -1;
	}

	gbpackModeSuspend = 0x00;
	RandomNumberGenerator(gbPackTxRandomKeybuffer,16);
	PackTx_MakePacketSinglePort(F0_ENCRYPTION_KEY_CHANGE, ES_LOCK_EVENT, &gbComCnt, gbPackTxRandomKeybuffer,16);
	
	return 0;
}
void PackTxLockOperationbyAccessoryAckSend(BYTE Lowbuff, BYTE Highbuff)
{
	BYTE bTmpArray[2];
	
	bTmpArray[0] = Lowbuff;
	bTmpArray[1] = Highbuff;
	if(gfPackTypeCBABle)
	{
		PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 2);	
	}
	else 	if(gfInnerPackTypeCBABle)
	{
		InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 2);	
	}
	else
	{
		__NOP();
	}
}



