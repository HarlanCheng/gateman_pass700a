//------------------------------------------------------------------------------
/** 	@file		ModeAlarmClearByKey_T1.c
	@version 0.1.00
	@date	2016.04.29
	@brief	Alarm Clear By Long Key
	@remark	내부 버튼 길게 눌러서 경보 해제
	@see	AlarmFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.29		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"



BYTE gAlarmClrInputKey = 0;


void ModeGotoAlarmClearByKey(BYTE SetKey)
{
	gAlarmClrInputKey = SetKey;

	gbMainMode = MODE_ALARM_CLR_BY_KEY;
	gbModePrcsStep = 0;
}





void ModeAlarmClearByKey(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(VOICE_MIDI_INTO, VOL_CHECK);

			SetModeProcessTime(500);

			gbModePrcsStep++;
			break;

		case 1:
			if(GetModeProcessTime())		
			{
				if(gbNewFunctionKey == FUNKEY_REG || gbNewOCFunctionKey == FUNKEY_OPCLOSE) //OChyojoon.kim
				{
					break;
				}
				else 
				{
					ModeClear();
				}
			}
			else
			{
				if(AlarmStatusCheck() == STATUS_SUCCESS)
				{
					AlarmGotoAlarmClear();
				}

				ModeClear();
			}
			break;

		default:
			ModeClear();
			break;
	}
}



