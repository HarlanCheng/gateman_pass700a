//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneRemoconRegDel_T1.h
	@brief	One Remocon Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUONEREMOCONREGDEL_T1_INCLUDED
#define __MODEMENUONEREMOCONREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoOneRemoconRegDelCheck(void);
void ModeOneRemoconRegDelCheck(void);
void ModeOneRemoconRegister(void);
void ModeOneRemoconDelete(void);


#ifdef	BLE_N_SUPPORT
void ModeGotoOneRemoconRegisterByBleN(void);
void ModeGotoOneRemoconDeleteByBleN(void);
void ModeOneRemoconRegisterByBleN(void);
void ModeOneRemoconDeleteByBleN(void);
#endif

#endif


