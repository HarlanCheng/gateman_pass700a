//------------------------------------------------------------------------------
/** 	@file		ModeMenuCylinderKeyAlarmSet_T1.h
	@brief	Programmable CylinderKey Alarm Setting
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUCYLINDERKEYALARMSET_T1_INCLUDED
#define __MODEMENUCYLINDERKEYALARMSET_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"



void ModeGotoCylinderKeyAlarmSetting(void);
void ModeCylinderKeyAlarmSetting(void);



#endif


