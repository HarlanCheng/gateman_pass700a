#ifndef		_FINGER_IREVO_NFPM_H_
#define		_FINGER_IREVO_NFPM_H_

#include "DefineMacro.h"
//#include "DefinePin.h"


extern FLAG BioFlag;

/* gbBioTimer10ms 다른 지문 모듈과 같은 방식으로 gbBioTimer10ms 제어 하지 말것 */
extern WORD gbBioTimer10ms;

extern BYTE gbFingerPrint_rxd_end;
extern BYTE gbFingerPrint_rxd_buf[40];

extern BYTE gbfingerALT;
extern BYTE gbfingerMaxTemplateNumber;

extern BYTE gbCoverAccessDelaytime100ms;
extern BYTE gbFingerLongTimeWait100ms;
extern BYTE gbFID;
extern BYTE gbFingerRegisterDisplayStep;
extern BYTE dbDisplayOn;
#ifdef	BLE_N_SUPPORT
extern BYTE gbEnterMode;
#endif 


#define NFPM_STX_INDEX			0
#define NFPM_LENGTH_INDEX		1
#define NFPM_EVENT_INDEX		2
#define NFPM_SUB_DATA_INDEX	3


#define NFPM_STX						0xFA
#define NFPM_ATR						0x50
#define NFPM_EVNET_ENROLL			0x51
#define NFPM_EVNET_ENROLL_ALARM	0x52
#define NFPM_EVNET_VERIFY			0x53
#define NFPM_EVNET_DELETE			0x54
#define NFPM_EVNET_GET_LIST			0x55
#define NFPM_EVNET_STOP_MODULE	0x56


void	Register_NFPM_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) );
void NFPMRepairUart(void);
void NFPM_uart_rxcplt_callback(UART_HandleTypeDef *huart);
void NFPM_uart_txcplt_callback(UART_HandleTypeDef *huart);
void	InitialSetupFingerModule( void );
BYTE GetNoFingerFromEeprom( void );
void NFPMRxDataClear(void);
void FingerPrintModuleCheck(void);
void FingerPrintTxDataSend(BYTE Length);
void NFPMTxEventEnroll(BYTE SlotNumber);
void NFPMTxEventEnrollAlarm(BYTE TemplateNumber);
void NFPMTxEventVerify(BYTE Parameter);
void NFPMTxEventDelete(BYTE SlotNumber);
void NFPMTxEventSlotInfo(BYTE Parameter);
void NFPMTxEventStop(BYTE Parameter);
void NFPMBioTimerCallback(void);
void BioCommCheck(BYTE bsave);
void FPCoverSwProcess(void);
void BioModuleON( void );
void BioModuleOff( void );
void BioOffModeClear( void );
void FingerPrintRegisterDisplay(void);
#endif

