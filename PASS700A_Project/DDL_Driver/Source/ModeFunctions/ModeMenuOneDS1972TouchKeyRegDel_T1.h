//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneDS1972TouchKeyRegDel_T1.h
	@brief	DS1972 Touch key Individual Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUONECARDREGDEL_T1_INCLUDED
#define __MODEMENUONECARDREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoOneDS1972TouchKeyRegDelCheck(void);
void ModeOneDS1972TouchKeyRegDelCheck(void);
void ModeOneDS1972TouchKeyRegister(void);
void ModeOneDS1972TouchKeyDelete(void);
void ProcessDeleteOneDS1972TouchKey(BYTE SlotNumber);
BYTE GetNoDS1972TouchKeyFromEeprom( void );


#ifdef	BLE_N_SUPPORT
void ModeGotoOneDS1972TouchKeyRegisterByBleN(void);
void ModeOneDS1972TouchKeyRegisterByBleN(void);
#endif 

#endif


