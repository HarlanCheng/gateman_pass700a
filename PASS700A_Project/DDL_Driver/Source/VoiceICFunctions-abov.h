#ifndef __VOICEICFUNCIONS_INCLUDED
#define __VOICEICFUNCIONS_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"

#include "abov_msg_index.h"


extern FLAG VoiceFlag;
#define	gfVoiceOut		VoiceFlag.STATEFLAG.Bit0						//Voice 처리 상태(1:처리 중, 0:처리 끝)
#define	gfVoiceReq		VoiceFlag.STATEFLAG.Bit1						//전송 요청
#define	gfVoiceData		VoiceFlag.STATEFLAG.Bit2						//Voice Command 전송 상태(1:전송 중, 0:전송 끝)
#define	gfVoiceNew		VoiceFlag.STATEFLAG.Bit3						//voice Command 새로 들어옴.
#define	gfVoiceReset	VoiceFlag.STATEFLAG.Bit4




#define _VOICE_HIGH	7			// 08년 11월 20일 확정 13
#define _VOICE_MID	3			// 08년 11월 20일 확정 8
#define _VOICE_LOW	2			// 08년 11월 20일 확정 8


#define _STOP_MODE		 	0
#define	_MIDI_MODE			0xFF00

#define	_KOREAN_MODE		0x0100
#define	_CHINESE_MODE		0x0200
#define	_ENGLISH_MODE		0x0300
#define	_SPANISH_MODE		0x0400
#define	_PORTUGUESE_MODE	0x0500
#define _TAIWANESE_MODE		0x0600

#define	_FRENCH_MODE		0x0700
#define	_RUSSIAN_MODE		0x0A00
#define	_TURKISH_MODE		0x0B00

#define	_JAPANESE_MODE		0x0C00 //지원 미정 추후 수정 될 수 있음
#define	_HINDI_MODE			0x0D00 //지원 미정 추후 수정 될 수 있음
#define	_CHINESE_GM_MODE	0x0E00 // 중국어 Gateman 브랜드 제품용
			
#define	VOICE_STOP					(_MIDI_MODE | 0)
#define	VOICE_MIDI_BUTTON			(_MIDI_MODE | 1)
#define	VOICE_MIDI_ERROR			(_MIDI_MODE | 2)
#define	VOICE_MIDI_INTO			(_MIDI_MODE | 3)
#define	VOICE_MIDI_CANCLE			(_MIDI_MODE | 4)
#define	VOICE_MIDI_OPEN			(_MIDI_MODE | 5)
#define	VOICE_MIDI_CLOSE			(_MIDI_MODE | 6)
#define	VOICE_MIDI_FINGER			(_MIDI_MODE | 7)
#define	VOICE_MIDI_AUTOLOCK		(_MIDI_MODE | 8)
#define	VOICE_MIDI_LOWBATT		(_MIDI_MODE | 9)
#define	VOICE_MIDI_BUTTON2		(_MIDI_MODE | 10)


//==============================================================================//
//	VOICE IC(UTEK H6164P)							//
//==============================================================================//
extern BYTE gbVoiceStep;
enum{
	VOICE_RESET_ON = 1,
	VOICE_RESET_OFF,
	VOICE_VOLUME_CHECK,
	VOICE_SYNCH_SEND,
	VOICE_DATA_SEND,
	VOICE_BUSY_CHECK,
	VOICE_END_WAIT,
	VOICE_STABLE_WAIT
};

extern WORD gwVoiceTimer2ms;


extern BYTE gbVoiceCnt;
extern BYTE gbVoiceData[4];
extern BYTE gbBuzSet;
extern BYTE gbVoiceReqData;


extern BYTE MidiAddress;
extern WORD gwLanguageMode;



void SetupVoice(void);
void VoiceSetting(WORD bData, BYTE bMode);
void VoiceDataProcess(void);
void VoiceICInitial(void);
void LanguageSetting(BYTE bMode);
void LanguageLoad(void);
void LanguageSetDefault(void);
WORD LanguageCheck(void);

void VoiceCallback(void);
BYTE GetVoicePrcsStep(void);


uint8_t ChineseGatemanCheck(void);


#endif
