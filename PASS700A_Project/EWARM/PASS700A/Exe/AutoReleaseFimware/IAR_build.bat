@echo off

::1 查找IAR路径
if exist "C:\Program Files (x86)\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe" (
set IAR="C:\Program Files (x86)\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe")

if exist "D:\Program Files (x86)\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe" (
set IAR="D:\Program Files (x86)\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe")

if exist "E:\Program Files (x86)\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe" (
set IAR="E:\Program Files (x86)\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe")

if exist "C:\Program Files\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe" (
set IAR= "C:\Program Files\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe") 

if exist "D:\Program Files\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe" (
set IAR="D:\Program Files\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe") 

if exist "E:\Program Files\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe" (
set IAR="E:\Program Files\IAR Systems\Embedded Workbench 8.2\common\bin\IarBuild.exe") 

::2 获取当前路径
cd ..\
set BatPath=%cd%
cd ..\..\

::3 编译
%IAR%	PASS700A.ewp -clean PASS700A
%IAR%	PASS700A.ewp PASS700A -parallel 4 
::%IAR%	PASS700A.ewp -build PASS700A 


::4 生成文件
cd %BatPath%
0_AfterBuild.bat


exit



