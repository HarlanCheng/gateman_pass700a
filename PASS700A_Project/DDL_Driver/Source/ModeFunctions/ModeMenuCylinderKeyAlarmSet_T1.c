//------------------------------------------------------------------------------
/** 	@file		ModeMenuCylinderKeyAlarmSet_T1
	@version 0.1.00
	@date	2019.07.29
	@brief	Programmable Auto Lock Setting
	@remark		@remark	실린더키(기계식 열쇠)사용 알람 설정 모드
	@see	ModeMenuMainSelect_T1.c
	@see	ModeMenuLockSetting_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2019.07.29		by Sim
*/
//------------------------------------------------------------------------------

#include "Main.h"

void ModeGotoCylinderKeyAlarmSetting(void)
{
	gbMainMode = MODE_MENU_CYLINDERKEY_ALARM_SET;
	gbModePrcsStep = 0;
}




void CylinderKeyAlarmSetSelected(void)
{
	
	BYTE bTmp;
	switch(gMenuSelectKeyTempSave)
	{
		//CylinderKeyAlarm Enable
		case 1: 				
			bTmp = 0x01;
			RomWrite(&bTmp, (WORD)CYLINDER_KEY_ALARM_SET, 1);

			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);

			gbModePrcsStep++;
			break;
	
		//CylinderKeyAlarm Disable
		case 3: 	
			bTmp = 0x00;
			RomWrite(&bTmp, (WORD)CYLINDER_KEY_ALARM_SET, 1);

			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);

			gbModePrcsStep++;
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 이전 모드로 이동
			gbModePrcsStep--;
			break;
	}			
}



void MenuCylinderKeyAlarmSetSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		//CylinderKeyAlarm Enable
		case TENKEY_1:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_TURNON_CONT_SHARP, gbInputKeyValue);		
			break;

		//CylinderKeyAlarm Disable
		case TENKEY_3:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_TURNOFF_CONT_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_SHARP:		 //# 버튼음 출력
#ifdef	P_VOICE_RST	
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
				FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuLockSetting();
			break;			

		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;

		default:
			TimeExpiredCheck();
			break;
	}	
}




//------------------------------------------------------------------------------
/** 	@brief	Auto Lock Setting Mode
	@param	None
	@return 	None
	@remark 자동잠김/수동잠김 설정 모드 시작
*/
//------------------------------------------------------------------------------
void ModeCylinderKeyAlarmSetting(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			Feedback13MenuOn(AVML_TURNON1_TURNOFF3, VOL_CHECK);

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuCylinderKeyAlarmSetSelectProcess();
			break;	
	
		case 2:
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
			{
				if(GetBuzPrcsStep() || GetVoicePrcsStep()) 	break;
			}

			CylinderKeyAlarmSetSelected();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoMenuLockSetting);
			break;

		default:
			ModeClear();
			break;
	}

}



