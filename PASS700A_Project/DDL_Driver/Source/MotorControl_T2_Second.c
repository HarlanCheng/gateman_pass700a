//------------------------------------------------------------------------------
/** 	@file		MotorControl_T2.c
	@version 0.1.01
	@date	2016.09.05
	@brief	Doorlock Motor Control Engine
	@see	N:\1. 기술연구소\05 회로팀\04 사내 표준 규격\00 회로 설계 규격\펌웨어\하드웨어제어관련\Motor\
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00	2016.04.01		by Jay
			- 기존 MotorControl.c 프로그램에서 open 일 때와 close 일 때의 TO 설정을 다르게 할 수 있도록 
			   TO_open, TO_close로 분리하여 따로 처리하게끔 알고리즘 수정
		       - MotorOpenCloserTimeSetting 함수와 MotorBackTurnTimeSetting 함수도 각각 _open, _close 경우로 분리
		         (AL-Mecha 대응에 사용)
			- 신규 보조키 IH, DM+ 에도 Motor_RIM 파일만 수정하면 사용 가능하지만, 일단 분리하여 함수 구성

		V0.1.01 2016.09.05		by Jay
			- Open, Close 수행 중 데드볼트 걸림 발생할 경우 Back Turn 동작하도록 추가 

*/
//------------------------------------------------------------------------------

#define		_MOTOR_CONTROL_C_Second

#include	"main.h"

//모델에 1개의 모터(모티스)가 있으므로 제품에 들어간 모터 특성을 정의한 ddl_motor_ctrl_t를 등록한다.
ddl_motor_ctrl_t_Second	* local_motor_ctrl_Second;
#if 0
#ifdef M_CLOSE_PWMCH1_Pin//second pwm_주석 by sjc

#define	_MOTOR_PWM_START		HAL_TIM_PWM_Start_IT( gh_mcu_motor_timer9, (local_motor_ctrl_Second->Direction == MCTRL_DIR_OPEN_Second ? TIM_CHANNEL_2 : TIM_CHANNEL_1))
#define	_MOTOR_PWM_STOP_CH1		HAL_TIM_PWM_Stop_IT( gh_mcu_motor_timer9, TIM_CHANNEL_1)
#define	_MOTOR_PWM_STOP_CH2		HAL_TIM_PWM_Stop_IT( gh_mcu_motor_timer9, TIM_CHANNEL_2)

uint16_t Get_PWM_Direction(void)
{
	return local_motor_ctrl_Second->Direction;
}

void Motor_PWM_Init(uint16_t Mdirection)
{
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;
	TIM_OC_InitTypeDef sConfigOC;
	GPIO_InitTypeDef GPIO_InitStruct;

	local_motor_ctrl_Second->Direction = Mdirection;
	
	gh_mcu_motor_timer9->Instance = TIM9;
	gh_mcu_motor_timer9->Init.Prescaler = 0;
	gh_mcu_motor_timer9->Init.CounterMode = TIM_COUNTERMODE_UP;
	gh_mcu_motor_timer9->Init.Period = 1000;
	gh_mcu_motor_timer9->Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	HAL_TIM_Base_Init(gh_mcu_motor_timer9);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(gh_mcu_motor_timer9, &sClockSourceConfig);

	HAL_TIM_PWM_Init(gh_mcu_motor_timer9);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(gh_mcu_motor_timer9, &sMasterConfig);

	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = PWM_START_DUTY*10;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;

	HAL_TIM_PWM_ConfigChannel(gh_mcu_motor_timer9, &sConfigOC, (local_motor_ctrl_Second->Direction == MCTRL_DIR_OPEN_Second ? TIM_CHANNEL_2 : TIM_CHANNEL_1)); //open GPIO14 clsoe GPIO13
	SetMotorPWMFreq(); 
	#ifdef USED_SECOND_MOTOR
	SetMotorPWMFreq_Second();
	#endif

//	HAL_TIM_MspPostInit(gh_mcu_motor_timer9);

	if(local_motor_ctrl_Second->Direction == MCTRL_DIR_OPEN_Second)
	{
		GPIO_InitStruct.Pin = M_OPEN_PWMCH2_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF3_TIM9;
		HAL_GPIO_Init(M_OPEN_PWMCH2_GPIO_Port, &GPIO_InitStruct);
#if 0		
		GPIO_InitStruct.Pin = M_CLOSE_PWMCH1_Pin;//second pwm_주석 by sjc
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
#endif 		
	}
	else 
	{
		GPIO_InitStruct.Pin = M_CLOSE_PWMCH1_Pin;//second pwm_주석 by sjc
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF3_TIM9;
		HAL_GPIO_Init(M_CLOSE_PWMCH1_GPIO_Port, &GPIO_InitStruct);
#if 0				
		GPIO_InitStruct.Pin = M_OPEN_PWMCH2_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);		
#endif 		
	}
	tim9_count = 0;
	Delay(SYS_TIMER_2MS);
	_MOTOR_PWM_START;
}

void Motor_PWM_DeInit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	// 언제 어디서 걸릴지 모르니 두 체널 다 timer stop 
	_MOTOR_PWM_STOP_CH1;
	_MOTOR_PWM_STOP_CH2;
	
	HAL_TIM_Base_DeInit(gh_mcu_motor_timer9);
	HAL_TIM_PWM_DeInit(gh_mcu_motor_timer9);
	HAL_TIM_Base_MspDeInit(gh_mcu_motor_timer9);
	
	GPIO_InitStruct.Pin = M_CLOSE_PWMCH1_Pin |M_OPEN_PWMCH2_Pin;//second pwm_주석 by sjc
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	P_M_CLOSE_2(0);
	P_M_OPEN_1(0);

	tim9_count = 0;
	local_motor_ctrl_Second->Direction = 0xFFFF;
}
#endif 
#endif

//2개의 no-op function
static	void		void_nop_function_Second( void )
{
}

static	uint32_t		uint32_nop_function_Second( void )
{
	return	0;
}



uint32_t		register_motor_ctrl_Second( ddl_motor_ctrl_t_Second *mctrl )
{
	if ( (mctrl->motor_open_Second == NULL) || (mctrl->motor_close_Second == NULL) ||
		(mctrl->motor_stop_Second == NULL) || (mctrl->motor_hold_Second == NULL) ) {
		return	0;
	}
	local_motor_ctrl_Second = mctrl;

 //함수 포인터 중 NULL인 것을 코드의 간결성을 위해 no-op function으로 대치한다.
	if ( !mctrl->init_sensor_hw_Second )	mctrl->init_sensor_hw_Second	= void_nop_function_Second;
	if ( !mctrl->sensor_on_Second )		mctrl->sensor_on_Second		= void_nop_function_Second;
	if ( !mctrl->sensor_off_Second )		mctrl->sensor_off_Second		= void_nop_function_Second;

	if ( !mctrl->is_motor_open_Second )	mctrl->is_motor_open_Second	= uint32_nop_function_Second;
	if ( !mctrl->is_motor_close_Second )	mctrl->is_motor_close_Second	= uint32_nop_function_Second;
	if ( !mctrl->is_motor_center_Second )	mctrl->is_motor_center_Second	= uint32_nop_function_Second;
	if ( !mctrl->is_motor_lock_Second )	mctrl->is_motor_lock_Second    	= uint32_nop_function_Second;

#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
	mctrl->MotorException_Second = 0x00;
	mctrl->Direction = 0xFFFF;	//변수명을 바꾸지 않음 추후 PWM을 사용해야 하는 경우 신중하게 바꿔야함 2018_06_07 심재철
#endif 

	return	1;
}


void		emergence_motor_stop_Second( void )
{
	local_motor_ctrl_Second->motor_process_state_Second = MPROC_STOP_Second;

	(*local_motor_ctrl_Second->motor_hold_Second)();
	Delay( SYS_TIMER_100MS );
	(*local_motor_ctrl_Second->motor_stop_Second)();
	gwMotorCtrlTimer2ms_Second = 0;				// clear MotorCtrl timer
}



static	uint16_t	get_next_mproc_state_Second( void )
{
	uint16_t		curr_state = local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second;
	uint16_t		curr_dir = local_motor_ctrl_Second->motor_process_state_Second & MCTRL_DIR_MASK_Second;
	uint16_t		next_state = MPROC_STOP_Second;

	switch( curr_state ) {
//		case MPROC_STOP_Second:
//		case MPROC_POST_Second:
//			next_state = MPROC_STOP_Second;
//			break;

		case MPROC_PRE_Second:
			next_state = MPROC_TO_1_Second;
			break;

		case MPROC_TO_1_Second:
		case MPROC_TO_2_Second:
		case MPROC_TO_3_Second:
		case MPROC_TO_4_Second:
		case MPROC_TO_5_Second:
		case MPROC_TO_6_Second:
		case MPROC_TO_7_Second:
		case MPROC_TO_8_Second:
			next_state = curr_state + 1;

			switch ( local_motor_ctrl_Second->motor_process_state_Second & MCTRL_DIR_MASK_Second ) {
				case	MCTRL_DIR_OPEN_Second:
					while ( (local_motor_ctrl_Second->TO_open_Second[ next_state - MPROC_TO_1_Second ] == 0) && (next_state<MPROC_TO_9_Second) )
						next_state ++;

					if ( (next_state == MPROC_TO_9_Second) && ( local_motor_ctrl_Second->TO_open_Second[next_state - MPROC_TO_1_Second] == 0 ) )
						next_state = MPROC_POST_Second;
					break;

				case	MCTRL_DIR_CLOSE_Second:
					while ( (local_motor_ctrl_Second->TO_close_Second[ next_state - MPROC_TO_1_Second ] == 0) && (next_state<MPROC_TO_9_Second) )
						next_state ++;

					if ( (next_state == MPROC_TO_9_Second) && ( local_motor_ctrl_Second->TO_close_Second[next_state - MPROC_TO_1_Second] == 0 ) )
						next_state = MPROC_POST_Second;
					break;
			}					
			break;

		case MPROC_TO_9_Second:
			next_state = MPROC_POST_Second;
	}

	next_state |= curr_dir;
	return	next_state;
}

static	uint16_t	get_mctrl_to_Second( void )
{
	uint16_t		curr_state = local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second;

	switch ( local_motor_ctrl_Second->motor_process_state_Second & MCTRL_DIR_MASK_Second ) {
		case	MCTRL_DIR_OPEN_Second:
			if ( MPROC_TO_1_Second <= curr_state && curr_state <= MPROC_TO_9_Second )
				return	local_motor_ctrl_Second->TO_open_Second[ curr_state - MPROC_TO_1_Second ];
			else
				return	0;
	
		case	MCTRL_DIR_CLOSE_Second:
			if ( MPROC_TO_1_Second <= curr_state && curr_state <= MPROC_TO_9_Second )
				return	local_motor_ctrl_Second->TO_close_Second[ curr_state - MPROC_TO_1_Second ];
			else
				return	0;
	}
	return	0;
}


//2msec 주기로 호출될 것을 기대
void		motor_control_open_type1_Second( void )
{
	switch( local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second ) {
		case MPROC_STOP_Second:
			return;

		case MPROC_PRE_Second:
			(*local_motor_ctrl_Second->sensor_on_Second)();
			Delay( SYS_TIMER_1MS );					// photo sensor stable time

#if defined (M_CLOSE_PWMCH1_Pin) ||defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
			if ( (*local_motor_ctrl_Second->is_motor_open_Second)() && local_motor_ctrl_Second->MotorException_Second == 0x00) {		// if already open
				/* 센서가 open 상태 이나 MotorException_Second 값이 셋팅이 안되어 그냥 빠져 나가는 단계 */
				(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
				local_motor_ctrl_Second->motor_process_state_Second = MPROC_TO_9_Second |MCTRL_DIR_OPEN_Second;
				gwMotorCtrlTimer2ms_Second = 1;				// clear MotorCtrl timer
				SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_OPEN);//SetFinalMotorStatus_Second_20180611by_sjc
				break;
			}
			else 
			{
				/* 센서 open 상태 상관없이 일반적으로 타는 루틴*/
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle	
			}
#else 
			if ( (*local_motor_ctrl_Second->is_motor_open_Second)() ) {		// if already open
				(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
				local_motor_ctrl_Second->motor_process_state_Second = MPROC_TO_9_Second |MCTRL_DIR_OPEN_Second;
				gwMotorCtrlTimer2ms_Second = 1;				// clear MotorCtrl timer
				SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_OPEN);//SetFinalMotorStatus_Second_20180611by_sjc
				break;
			}
#endif 

			local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();		// get next step
			gwMotorCtrlTimer2ms_Second =  get_mctrl_to_Second();
			if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_1_Second )
				(*local_motor_ctrl_Second->motor_open_Second)();			// do motor open
			else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_2_Second ) {
				(*local_motor_ctrl_Second->motor_open_Second)();			// do motor open

				if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
				{
					gwMotorCtrlTimer2ms_Second = MotorOpenCloseTimeSetting_open_Second();
				}
			}
			break;

		case MPROC_TO_1_Second:
			if ( gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_OPEN_ERROR);//SetFinalMotorStatus_Second_20180611by_sjc

				local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_TO_5_Second;
				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();
				
				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_6_Second ) {
					(*local_motor_ctrl_Second->motor_close_Second)(); 		// do motor close
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_7_Second ) {
					(*local_motor_ctrl_Second->motor_close_Second)(); 		// do motor close
				
					if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
					{
						gwMotorCtrlTimer2ms_Second = MotorBackTurnTimeSetting_open_Second();
					}
				}
				else {
					(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
					(*local_motor_ctrl_Second->motor_hold_Second)();			// make motor to idle
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
				}
			}
			else if ( (*local_motor_ctrl_Second->is_motor_open_Second)() ) {		// if open detected
#ifndef P_SNS_CENTER_T
				(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
#endif 				

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();

				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_2_Second ) {
					// Open 센서 감지된 상태에서 더 밀어주는 구간이므로 Motor Stop 수행 안함.	
					if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
					{
						gwMotorCtrlTimer2ms_Second = MotorOpenCloseTimeSetting_open_Second();
					}
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
					if(tim9_count < PWM_BACKTURN_SKIP_COUNT && local_motor_ctrl_Second->MotorException_Second == 0xAA)
					{
						/* close 시도중에 open 을 시도 하는 경우 중 PWM 이 충분한 시간을 유지 못하고 온경우 */
						/* 즉 open 시도후 바로 open sensor 에 근접한 경우 */
						/* open 상태로 PWM_HOLD_OPEN_TIME ms 유지후 backturn 을 하지 않는다 */
						/* tim9_count < PWM_BACKTURN_SKIP_COUNT 이값은 mecha 특성에 따라 측정 한다 */
						/* gwMotorCtrlTimer2ms_Second = PWM_HOLD_OPEN_TIME; 역시 mecha 특성에 따라 측정 */
						gwMotorCtrlTimer2ms_Second = PWM_HOLD_OPEN_TIME;
					}
					else 
					{
						local_motor_ctrl_Second->MotorException_Second = 0x00;
					}
#endif 					
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_3_Second ) {
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_4_Second ) {
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
				}
				
				SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_OPEN);//SetFinalMotorStatus_Second_20180611by_sjc
			}
			break;

		case MPROC_TO_2_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();

				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_3_Second ) {
					// 더 밀어주는 구간이 끝나고 Hold 하기 전 Delay 구간일 경우 아무런 수행 없음
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_4_Second ) {
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_3_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();

				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_4_Second ) {
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_4_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();
				
				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_5_Second ) {
					// Hold 구간이 끝나고 Back-Turn 하기 전 Delay 구간일 경우 아무런 수행 없음
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_6_Second ) {
					(*local_motor_ctrl_Second->motor_close_Second)(); 		// do motor close
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_7_Second ) {
				
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
					if(local_motor_ctrl_Second->MotorException_Second == 0xAA)
					{
						/* backturn skip */ 
						local_motor_ctrl_Second->MotorException_Second = 0x00;
					}
					else 
					{
						(*local_motor_ctrl_Second->motor_close_Second)(); 		// do motor close					
					}
#else 
					(*local_motor_ctrl_Second->motor_close_Second)(); 		// do motor close
#endif 				
					if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
					{
						gwMotorCtrlTimer2ms_Second = MotorBackTurnTimeSetting_open_Second();
					}
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_5_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();
				
				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_6_Second ) {
					(*local_motor_ctrl_Second->motor_close_Second)(); 		// do motor close
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_7_Second ) {
					(*local_motor_ctrl_Second->motor_close_Second)(); 		// do motor close

					if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
					{
						gwMotorCtrlTimer2ms_Second = MotorBackTurnTimeSetting_open_Second();
				}
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_8_Second ) {
					//	Back-Turn 한 상태에서 더 밀어주는 구간 이후 Hold 하기 전 Delay 구간일 경우 아무런 수행 없음.	
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_9_Second ) {
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_6_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
					(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
					(*local_motor_ctrl_Second->motor_hold_Second)();			// make motor to idle
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
				
					SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_OPEN_ERROR);//SetFinalMotorStatus_Second_20180611by_sjc
			}
			else if ( (*local_motor_ctrl_Second->is_motor_center_Second)() ) {	// if center detected
				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();

				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_7_Second ) {
					// Back-Turn이 끝나고 더 밀어 주는 구간일 경우 아무런 수행 없음
					if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
					{
						gwMotorCtrlTimer2ms_Second = MotorBackTurnTimeSetting_open_Second();
					}
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_8_Second ) {
					// Back-Turn이 끝나고 Hold 수행 전 Delay 구간일 경우 모터 멈춤
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_9_Second ) {
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_7_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();
				
				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_8_Second ) {
					// Back-Turn 이후 더 밀어주는 구간이 끝나고 Hold 하기 전 Delay 구간일 경우 모터 멈춤
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_9_Second ) {
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_8_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();
				
				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_9_Second ) {
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_9_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_POST_Second;
			}
			break;


		case MPROC_POST_Second:
		default:
			(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
			(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
			local_motor_ctrl_Second->motor_process_state_Second = MPROC_STOP_Second;
			gwMotorCtrlTimer2ms_Second = 0;				// clear MotorCtrl timer
			LockStatusCheckClear();//LockStatusCheckClear_Second()_20180611by_sjc
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
			local_motor_ctrl_Second->MotorException_Second = 0x00;
#endif 
			break;
	}
}


void		motor_control_close_type1_Second( void )
{
	switch( local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second ) {
		case MPROC_STOP_Second:
			return;

		case MPROC_PRE_Second:
			(*local_motor_ctrl_Second->sensor_on_Second)();

			Delay( SYS_TIMER_1MS );					// photo sensor stable time

#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
			(*local_motor_ctrl_Second->motor_stop_Second)();
#endif 
			
			if ( (*local_motor_ctrl_Second->is_motor_close_Second)() ) {		// if already open
				(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
				local_motor_ctrl_Second->motor_process_state_Second = MPROC_STOP_Second;
				gwMotorCtrlTimer2ms_Second = 0;				// clear MotorCtrl timer

				SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_CLOSE);//SetFinalMotorStatus_Second_20180611by_sjc
				break;
			}

			local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();		// get next step
			gwMotorCtrlTimer2ms_Second =  get_mctrl_to_Second();
			if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_1_Second )
				(*local_motor_ctrl_Second->motor_close_Second)();			// do motor close
			else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_2_Second ) {
				(*local_motor_ctrl_Second->motor_close_Second)();			// do motor close

				if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
				{
					gwMotorCtrlTimer2ms_Second = MotorOpenCloseTimeSetting_close();
				}
			}
			break;

		case MPROC_TO_1_Second:
			if ( gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_CLOSE_ERROR);//SetFinalMotorStatus_Second_20180611by_sjc

				local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_TO_5_Second;
				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();
				
				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_6_Second ) {
					(*local_motor_ctrl_Second->motor_open_Second)();			// do motor open
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_7_Second ) {
					(*local_motor_ctrl_Second->motor_open_Second)();			// do motor open
				
					if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
					{
						gwMotorCtrlTimer2ms_Second = MotorBackTurnTimeSetting_close_Second();
					}
				}
				else {
					(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
					(*local_motor_ctrl_Second->motor_hold_Second)();			// make motor to idle
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;
				}
			}
			else if ( (*local_motor_ctrl_Second->is_motor_close_Second)() ) {		// if open detected
#ifndef P_SNS_CENTER_T
				(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
#endif 				

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();

				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_2_Second ) {
					// Close 센서 감지된 상태에서 더 밀어주는 구간이므로 Motor Stop 수행 안함.	
					if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
					{
						gwMotorCtrlTimer2ms_Second = MotorOpenCloseTimeSetting_close();
					}
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_3_Second ) {
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_4_Second ) {
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;
				}

				SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_CLOSE);//SetFinalMotorStatus_Second_20180611by_sjc
			}
			break;

		case MPROC_TO_2_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();

				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_3_Second ) {
					// 더 밀어주는 구간이 끝나고 Hold 하기 전 Delay 구간일 경우 아무런 수행 없음
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_4_Second ) {
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_3_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();

				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_4_Second ) {
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_4_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();
				
				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_5_Second ) {
					// Hold 구간이 끝나고 Back-Turn 하기 전 Delay 구간일 경우 아무런 수행 없음
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_6_Second ) {
					(*local_motor_ctrl_Second->motor_open_Second)();			// do motor open
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_7_Second ) {
					(*local_motor_ctrl_Second->motor_open_Second)();			// do motor open
				
					if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
					{
						gwMotorCtrlTimer2ms_Second = MotorBackTurnTimeSetting_close_Second();
					}
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_5_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();
				
				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_6_Second ) {
					(*local_motor_ctrl_Second->motor_open_Second)();			// do motor open
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_7_Second ) {
					(*local_motor_ctrl_Second->motor_open_Second)();			// do motor open

					if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
					{
						gwMotorCtrlTimer2ms_Second = MotorBackTurnTimeSetting_close_Second();
				}
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_8_Second ) {
					//  Back-Turn 한 상태에서 더 밀어주는 구간 이후 Hold 하기 전 Delay 구간일 경우 아무런 수행 없음.	
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_9_Second ) {
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_6_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
				(*local_motor_ctrl_Second->motor_hold_Second)();			// make motor to idle
				local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;

				SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_CLOSE_ERROR);//SetFinalMotorStatus_Second_20180611by_sjc
			}
			else if ( (*local_motor_ctrl_Second->is_motor_center_Second)() ) { 	// if center detected
				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();

				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_7_Second ) {
					// Back-Turn이 끝나고 더 밀어 주는 구간일 경우 아무런 수행 없음
					if(gwMotorCtrlTimer2ms_Second == _USE_MOTOR_TIME_SET_Second)
					{
						gwMotorCtrlTimer2ms_Second = MotorBackTurnTimeSetting_close_Second();
					}
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_8_Second ) {
					// Back-Turn이 끝나고 Hold 수행 전 Delay 구간일 경우 모터 멈춤
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_9_Second ) {
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_7_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();
				
				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_8_Second ) {
					// Back-Turn 이후 더 밀어주는 구간이 끝나고 Hold 하기 전 Delay 구간일 경우 모터 멈춤
				}
				else if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_9_Second ) {
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_8_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = get_next_mproc_state_Second();
				gwMotorCtrlTimer2ms_Second =	get_mctrl_to_Second();
				
				if ( (local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) == MPROC_TO_9_Second ) {
					(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
					(*local_motor_ctrl_Second->motor_hold_Second)();			// do motor break
				}
				else {
					local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;
				}
			}
			break;

		case MPROC_TO_9_Second:
			if (gwMotorCtrlTimer2ms_Second == 0 ) {		// Timeout
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle

				local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_POST_Second;
			}
			break;

		case MPROC_POST_Second:
		default:
			(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
			(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
			local_motor_ctrl_Second->motor_process_state_Second = MPROC_STOP_Second;
			gwMotorCtrlTimer2ms_Second = 0;				// clear MotorCtrl timer
				
			break;
	}
}


void		StartMotorOpen_Second( void )	// StartMotorOpen_Second()_20180611by_sjc
{
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
	if((local_motor_ctrl_Second->motor_process_state_Second & MCTRL_DIR_MASK_Second) == MCTRL_DIR_CLOSE_Second)
	{
		local_motor_ctrl_Second->MotorException_Second = 0xAA;
	}
#endif 	
	local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_OPEN_Second | MPROC_PRE_Second;

	SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_OPEN_ING);//SetFinalMotorStatus_Second_20180611by_sjc
}

void		StartMotorClose_Second( void )	//StartMotorClose_Second()_20180611by_sjc
{
	local_motor_ctrl_Second->motor_process_state_Second = MCTRL_DIR_CLOSE_Second | MPROC_PRE_Second;

	SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_CLOSE_ING);//SetFinalMotorStatus_Second_20180611by_sjc
}



void		MotorControlProcess_Second( void )
{
	PCErrorClear();

	if ( !gwMotorCtrlTimer2ms_Second && !(local_motor_ctrl_Second->motor_process_state_Second & MCTRL_STATE_MASK_Second) )
		return;

	switch ( local_motor_ctrl_Second->motor_process_state_Second & MCTRL_DIR_MASK_Second ) {
		case	MCTRL_DIR_OPEN_Second:
			if(PCErrorClearCheck() == STATUS_FAIL)
			{
				(*local_motor_ctrl_Second->motor_stop_Second)();			// make motor to idle
				(*local_motor_ctrl_Second->sensor_off_Second)();			// turn off sensor power
				local_motor_ctrl_Second->motor_process_state_Second = MPROC_STOP_Second;
				gwMotorCtrlTimer2ms_Second = 0;				// clear MotorCtrl timer
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
				local_motor_ctrl_Second->MotorException_Second = 0x00;
#endif 
				FeedbackSystemError(SYSTEM_ERR_MOTOR_SENSOR);
				return;
			}
			
			motor_control_open_type1_Second();
			break;
		case	MCTRL_DIR_CLOSE_Second:
			motor_control_close_type1_Second();
			break;
	}
}


void		Test_MotorControl_Second( void )
{
/*
	(*local_motor_ctrl_Second->sensor_on)();
	Delay( SYS_TIMER_1MS );

	if ( (*local_motor_ctrl_Second->is_motor_open_Second)() )
		StartMotorClose_Second();//StartMotorClose_Second()_20180611by_sjc
	else if ( (*local_motor_ctrl_Second->is_motor_close_Second)() )
		StartMotorOpen_Second();	// StartMotorOpen_Second()_20180611by_sjc

	(*local_motor_ctrl_Second->sensor_off)();
*/
}


//------------------------------------------------------------------------------
/** 	@brief	Get current motor process state
	@param 	None 
	@return 	모터 구동 process의 local_motor_ctrl_Second->motor_process_state_Second 값을 리턴한다.
	@remark 현재 모터 구동 상태를 가져 온다. mask를 씌우지 않았으므로 Open/Close상태도 파악할 수 있다.
*/
//------------------------------------------------------------------------------
uint16_t GetMotorPrcsStep_Second(void)//GetMotorPrcsStep_Second()_20180611by_sjc
{
	return	local_motor_ctrl_Second->motor_process_state_Second;
}



__weak uint32_t MotorOpenCloseTimeSetting_Second(void)//MotorOpenCloseTimeSetting_Second()_20180611by_sjc
{
	return 0;
}


__weak uint32_t MotorBackTurnTimeSetting_Second(void)//MotorBackTurnTimeSetting_Second()_20180611by_sjc
{
	return 0;
}


__weak uint32_t MotorOpenCloseTimeSetting_open_Second(void)
{
	return 0;
}


__weak uint32_t MotorOpenCloseTimeSetting_close(void)
{
	return 0;
}

__weak uint32_t MotorBackTurnTimeSetting_open_Second(void)
{
	return 0;
}

__weak uint32_t MotorBackTurnTimeSetting_close_Second(void)
{
	return 0;
}


//============================================================



BYTE gFinalMotorStatus_Second = 0;					/**< 최종 모터 구동 결과 내용 저장   */


//------------------------------------------------------------------------------
/** 	@brief	Set motor running result
	@param 	[FinalStatus] 최종 모터 구동 내용 
	@return 	None
	@remark 마지막에 모터를 구동한 결과 내용을 저장함 
*/
//------------------------------------------------------------------------------
void SetFinalMotorStatus_Second(BYTE FinalStatus)//SetFinalMotorStatus_Second_20180611by_sjc
{
	gFinalMotorStatus_Second = FinalStatus;
}


//------------------------------------------------------------------------------
/** 	@brief	Get previous motor running result
	@param 	None 
	@return 	[gFinalMotorStatus_Second] 최종 모터 구동 내용 저장
	@remark 마지막에 모터를 구동한 결과 내용을 읽어 옴 
*/
//------------------------------------------------------------------------------
BYTE GetFinalMotorStatus_Second(void)//GetFinalMotorStatus_Second_20180611by_sjc
{
	return (gFinalMotorStatus_Second);
}



//============================================================


BYTE gMotorSensorStatus_Second = 0;	/**< 현재 Motor Sensor 값 저장  */


//------------------------------------------------------------------------------
/** 	@brief	Motor Sensor Status Variable Clear
	@param 	None 
	@return 	None
	@remark 반드시 Main Loop 상에서 매 Loop마다 1회 구동되도록 구현
*/
//------------------------------------------------------------------------------
void MotorSensorClear_Second(void)
{
	gMotorSensorStatus_Second = 0;
}



//------------------------------------------------------------------------------
/** 	@brief	Current Motor Sensor Check
	@param 	None 
	@return 	[gMotorSensorStatus_Second] 현재 Sensor 상태 값 회신
	@remark Main Loop 상에서 모터 구동 경우를 제외하고는 Loop 당 Sensor는 1회만 구동
	@remark  Sensor Power On 할 때마다 1ms 정도의 Delay 시간이 필요해 Loop 동작을 방해할 뿐 아니라,
	@remark  Loop 동작 시간이 그리 길지 않기 때문에 그동안 1회만 구동해도 실제 Sensor 확인에는 문제가 
	@remark  없기 때문에 이와 같이 구동
*/
//------------------------------------------------------------------------------
BYTE MotorSensorCheck_Second(void)//MotorSensorCheck_Second_20180611by_sjc
{
	if(gMotorSensorStatus_Second == 0)
	{
		(*local_motor_ctrl_Second->sensor_on_Second)();
		Delay( SYS_TIMER_1MS );

#if defined (P_SNS_OPEN_T_SECOND) ||defined (P_SNS_CLUTCH_T)
		if((*local_motor_ctrl_Second->is_motor_open_Second)())
		{
			gMotorSensorStatus_Second |= SENSOR_OPEN_STATE;
		}
#endif

#if defined (P_SNS_CLOSE_T_SECOND) ||defined (P_SNS_CLUTCH_T)
		if((*local_motor_ctrl_Second->is_motor_close_Second)()) 	
		{
			gMotorSensorStatus_Second |= SENSOR_CLOSE_STATE;
		}
#endif	

#ifdef P_SNS_CENTER_T
		if((*local_motor_ctrl_Second->is_motor_center_Second)())	
		{
			gMotorSensorStatus_Second |= SENSOR_CENTER_STATE;
		}
#endif	

		if((*local_motor_ctrl_Second->is_motor_lock_Second)())	
		{
			gMotorSensorStatus_Second |= SENSOR_LOCK_STATE;
		}

// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 시작		
#ifdef		P_SNS_LOCK_T	
#ifdef	DDL_CFG_SNS_LOCK_ACTIVE_HIGH
		if(P_SNS_LOCK_T)
#else 
		if(!P_SNS_LOCK_T)
#endif 			
		{
			gbInnerForceLockDetectionValue = 0x00;	//내부 강제 잠금 Set
		}
		else
		{
			gbInnerForceLockDetectionValue = 0x01;	//내부 강제 잠금 Clear
		}
#endif
// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 끝

		
		// 모터 구동 중에 다른 루틴에서 MotorSensorCheck_Second 함수를 수행할 경우 Sensor를 끄게 되어
		// 모터 구동 중에 센서 확인이 되지 않아 에러 처리되는 문제 수정
		// 모터 구동 중에는 센서 끄지 않도록 처리하고, 모터 구동 함수의 MPROC_POST_Second 단계에서 sensor_off 추가 
		// (프로그램 구조 문제로 인해 센서가 안 꺼지고 Power Down Mode로 들어가는 일이 없도록 하기 위해)
		if(GetMotorPrcsStep_Second() == 0)//GetMotorPrcsStep_Second()_20180611by_sjc
		{
			(*local_motor_ctrl_Second->sensor_off_Second)();
		}

#ifdef P_SNS_CENTER_T
		if(gMotorSensorStatus_Second == SENSOR_OPENCENLOCK_STATE)
		{
			gMotorSensorStatus_Second = SENSOR_OPENLOCK_STATE;
		}
		else if(gMotorSensorStatus_Second == SENSOR_CLOSECENLOCK_STATE)
		{
			gMotorSensorStatus_Second = SENSOR_CLOSELOCK_STATE;
		}
#endif	
	}

	return (gMotorSensorStatus_Second);
}



void MotorStatusInitialCheck_Second(void)
{
	gfMotorSensorErr = 0;			

// 내부강제잠금 초기 상태 확인은 Motor 초기 상태 확인 부분에서 같이 하도록 처리
#ifndef		P_SNS_LOCK_T	
	InnerForcedLockSettingLoad_Second();	//InnerForcedLockSettingLoad_Second
#endif

	switch(MotorSensorCheck_Second())//MotorSensorCheck_Second_20180611by_sjc
	{
#ifdef P_SNS_CENTER_T
		case SENSOR_CLOSECEN_STATE:
#endif
		case SENSOR_CLOSE_STATE:
		case SENSOR_CLOSELOCK_STATE:
			SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_CLOSE);//SetFinalMotorStatus_Second_20180611by_sjc
			break;

		case SENSOR_SENSOR_ERROR:
			gfMotorSensorErr = 1;			
			break;

		case SENSOR_OPENLOCK_STATE:
// 스위치 형태가 아닌 내부강제잠금 설정 상태는 Motor가 열림 상태면 무조건 해제 처리			
#ifndef		P_SNS_LOCK_T	
			InnerForcedLockClear_Second();//InnerForcedLockClear_Second
#endif

		default:
			SetFinalMotorStatus_Second(FINAL_MOTOR_STATE_OPEN);//SetFinalMotorStatus_Second_20180611by_sjc
			break;
	}
}




#if 0
// 전기 충격에 의한 모터 해정 방지 처리 함수

WORD gwPCErrorCheck = 0;


void PCErrorClearSet(void)
{
	gwPCErrorCheck = 0x825A;
}


BYTE PCErrorClearCheck(void)
{
	if(gwPCErrorCheck == 0x825A)
	{
		return (STATUS_SUCCESS);
	}

	return (STATUS_FAIL);
}


void PCErrorClear(void)
{
	if(GetMainMode())			return;	
	if(GetMotorPrcsStep())		return;//GetMotorPrcsStep_Second()_20180611by_sjc
	if(GetPackRxProcessStep())	return;
	if(JigModeStatusCheck())	return;
	if(AlarmStatusCheck())		return;

	gwPCErrorCheck = 0;
}
#endif


