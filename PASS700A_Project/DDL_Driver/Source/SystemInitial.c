#define		_SYSTEM_INITIAL_C_

#include "Main.h"



/* Max number of HFCycles for higher accuracy */
#define HFCYCLES            0xFFFFF
/* HF_cycles x ref_freq / HFCLK_freq = (2^20 - 1) x 32768 / 14000000 = 2454 */
#define UP_COUNTER_TUNED    2454
/* Initial HFRCO TUNING value */
#define TUNINGMAX           0xFF

void ChangeModelCheckPinToAnalog(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

#if	defined(OP_RFID_Pin) && defined(OP_FP_Pin) && defined(OP_VOICE_Pin)
	/*Configure GPIO pins : OP_RFID_Pin OP_FP_Pin OP_VOICE_Pin 
							 SNS_EN_Pin */
	GPIO_InitStruct.Pin = OP_RFID_Pin|OP_FP_Pin|OP_VOICE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(OP_RFID_GPIO_Port, &GPIO_InitStruct);
#endif

	/*Configure GPIO pins : JIG_POINT_Pin */
	GPIO_InitStruct.Pin = JIG_POINT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(JIG_POINT_GPIO_Port, &GPIO_InitStruct);
}

#if	0
void ChangeModelCheckPinToOutput(BYTE RfPin, BYTE FpPin, BYTE VoicePin, BYTE JigPin)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/*Configure GPIO pins : OP_RFID_Pin OP_FP_Pin OP_VOICE_Pin 
							 SNS_EN_Pin */
	GPIO_InitStruct.Pin = OP_RFID_Pin|OP_FP_Pin|OP_VOICE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	(RfPin) ? P_OP_RFID(1) : P_OP_RFID(0);
	(FpPin) ?	P_OP_FP(1) : P_OP_FP(0);
	(VoicePin) ? P_OP_VOICE(1) : P_OP_VOICE(0);

//	P_OP_FP(0);
//	P_OP_VOICE(0);


	/*Configure GPIO pins : JIG_POINT_Pin */
	GPIO_InitStruct.Pin = JIG_POINT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
}
#endif

void ChangeDebugPinToAnalog(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = MCU_SW_DAT_Pin|MCU_SW_CLK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(MCU_SW_DAT_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = MCU_SW_OB_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(MCU_SW_OB_GPIO_Port, &GPIO_InitStruct);
}

#if defined	(DDL_CFG_CLUTCH) |	defined (DDL_CFG_CMPL_CLUTCH) ||	defined (DDL_CFG_ONEWAY_ONESENSOR_CLUTCH)
void ChangeMotorSensorPinToAnalog(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = SNS_CLUTCH_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_CLUTCH_GPIO_Port, &GPIO_InitStruct);

#ifdef	P_SNS_LOCK_T
	GPIO_InitStruct.Pin = SNS_LOCK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_LOCK_GPIO_Port, &GPIO_InitStruct);
#endif
}

void ChangeMotorSensorPinToInput(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = SNS_CLUTCH_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_CLUTCH_GPIO_Port, &GPIO_InitStruct);

#ifdef	P_SNS_LOCK_T
	GPIO_InitStruct.Pin = SNS_LOCK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_LOCK_GPIO_Port, &GPIO_InitStruct);
#endif
}

#else 
void ChangeMotorSensorPinToAnalog(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = SNS_OPEN_Pin|SNS_CLOSE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_OPEN_GPIO_Port, &GPIO_InitStruct);

#ifdef USED_SECOND_MOTOR //모터 두개 대응(Secure mode 포함)
	GPIO_InitStruct.Pin = SNS_OPEN_1_Pin|SNS_CLOSE_1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_OPEN_1_GPIO_Port, &GPIO_InitStruct);
#endif //USED_SECOND_MOTOR #endif
	

#ifdef	P_SNS_LOCK_T
	GPIO_InitStruct.Pin = SNS_LOCK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_LOCK_GPIO_Port, &GPIO_InitStruct);
#endif

#ifdef	SNS_CLOSE2_Pin
	GPIO_InitStruct.Pin = SNS_CLOSE2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_CLOSE2_GPIO_Port, &GPIO_InitStruct);
#endif

#ifdef	P_SNS_CENTER_T
	GPIO_InitStruct.Pin = SNS_CENTER_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_CENTER_GPIO_Port, &GPIO_InitStruct);
#endif
}


void ChangeMotorSensorPinToInput(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = SNS_OPEN_Pin|SNS_CLOSE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_OPEN_GPIO_Port, &GPIO_InitStruct);

#ifdef USED_SECOND_MOTOR //모터 두개 대응(Secure mode 포함)
	GPIO_InitStruct.Pin = SNS_OPEN_1_Pin|SNS_CLOSE_1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_OPEN_1_GPIO_Port, &GPIO_InitStruct);
#endif //USED_SECOND_MOTOR #endif

#ifdef	P_SNS_LOCK_T
	GPIO_InitStruct.Pin = SNS_LOCK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_LOCK_GPIO_Port, &GPIO_InitStruct);
#endif

#ifdef	SNS_CLOSE2_Pin
	GPIO_InitStruct.Pin = SNS_CLOSE2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_CLOSE2_GPIO_Port, &GPIO_InitStruct);
#endif

#ifdef	P_SNS_CENTER_T
	GPIO_InitStruct.Pin = SNS_CENTER_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SNS_CENTER_GPIO_Port, &GPIO_InitStruct);
#endif
}

#ifdef	LOCK_TYPE_DEADBOLT
void ChangeAdditionalPositionCheckPinToAnalog(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = MECHA_HALL1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(MECHA_HALL1_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = MECHA_HALL2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(MECHA_HALL2_GPIO_Port, &GPIO_InitStruct);
}


void ChangeAdditionalPositionCheckPinToInput(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	BYTE bTmp;

	bTmp = GetHandingLock();
	if(bTmp == HANDING_LEFT_SET)
	{
		GPIO_InitStruct.Pin = MECHA_HALL2_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(MECHA_HALL2_GPIO_Port, &GPIO_InitStruct);
	}
	else if(bTmp == HANDING_RIGHT_SET)
	{
		GPIO_InitStruct.Pin = MECHA_HALL1_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(MECHA_HALL1_GPIO_Port, &GPIO_InitStruct);
	}
}
#endif 
#endif 
#ifdef	DDL_CFG_RFID
void SPI_On(void)
{
#ifdef	DDL_CFG_RFID_PARALLEL
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = RFID_IRQ_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;	
	GPIO_InitStruct.Pull = GPIO_NOPULL;		
	HAL_GPIO_Init(RFID_IRQ_GPIO_Port, &GPIO_InitStruct);
#else 
	GPIO_InitTypeDef GPIO_InitStruct;

	//SPI pin을 gpio analog로 변경한 뒤 다시 SPI로 사용하기위해 pin들을 모두 Alternate function 기능을 활성화 해야한다. 
	GPIO_InitStruct.Pin = RFID_SPI_CLK_Pin|RFID_SPI_MISO_Pin|RFID_SPI_MOSI_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = RFID_IRQ_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	gh_mcu_spi->Instance = SPI1;
	gh_mcu_spi->Init.Mode = SPI_MODE_MASTER;
	gh_mcu_spi->Init.Direction = SPI_DIRECTION_2LINES;
	gh_mcu_spi->Init.DataSize = SPI_DATASIZE_8BIT;
	gh_mcu_spi->Init.CLKPolarity = SPI_POLARITY_LOW;
	gh_mcu_spi->Init.CLKPhase = SPI_PHASE_2EDGE;
	gh_mcu_spi->Init.NSS = SPI_NSS_SOFT;
	gh_mcu_spi->Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
	gh_mcu_spi->Init.FirstBit = SPI_FIRSTBIT_MSB;
	gh_mcu_spi->Init.TIMode = SPI_TIMODE_DISABLE;
	gh_mcu_spi->Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	gh_mcu_spi->Init.CRCPolynomial = 10;
	HAL_SPI_Init(gh_mcu_spi);

	Delay( SYS_TIMER_100US );
#endif 	
}

void SPI_Off(void)
{
#ifdef	DDL_CFG_RFID_PARALLEL
	GPIO_InitTypeDef		GPIO_InitStruct;

	ParallelPortGPIOSet(OUTPUT);
	ParallelPortGPIOWrite(0x0000);

	P_RFID_CLK(0);
	
	GPIO_InitStruct.Pin = RFID_IRQ_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(RFID_IRQ_GPIO_Port, &GPIO_InitStruct);
#else 
	// TI TRF7970A가 EN=0일 때 SPI 핀들이 모두 Output Low로 설정되어야 불필요한 누설 전류가 발생하지 않는다고
	//	되어 있어 해당 부분의 설정을 Analog Input 처리에서 Output 설정으로 변경

	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = RFID_SPI_CLK_Pin|RFID_SPI_MISO_Pin|RFID_SPI_MOSI_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = RFID_IRQ_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
#endif 	
}
#endif


#ifdef	DDL_CFG_TOUCHKEY
void Touch_I2C_On(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = TKEY_SCL_Pin|TKEY_SDA_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(TKEY_SCL_GPIO_Port, &GPIO_InitStruct);

	P_TKEY_SCL(1);
	P_TKEY_SDA(1);
}


void Touch_I2C_Off(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = TKEY_SCL_Pin|TKEY_SDA_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(TKEY_SCL_GPIO_Port, &GPIO_InitStruct);
}
#endif


#ifdef		DDL_CFG_DIMMER
void Dimming_I2C_On(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = DIM_SCL_Pin|DIM_SDA_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(DIM_SCL_GPIO_Port, &GPIO_InitStruct);

	P_DIM_SCL(1);
	P_DIM_SDA(1);
}


void Dimming_I2C_Off(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = DIM_SCL_Pin|DIM_SDA_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(DIM_SCL_GPIO_Port, &GPIO_InitStruct);
}
#endif


#ifdef	P_TOUCH_O_C_T
void Touch_O_C_On(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = TOUCH_O_C_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(TOUCH_O_C_GPIO_Port, &GPIO_InitStruct);
}


void Touch_O_C_Off(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = TOUCH_O_C_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(TOUCH_O_C_GPIO_Port, &GPIO_InitStruct);
}
#endif

//#ifdef	P_OP_RFID_T
void ModelCheck(void)
{
	ChangeModelCheckPinToAnalog();

#if 0
	BYTE TmpNewRf = 0;
	BYTE TmpNewBio = 0;
	BYTE TmpNewVoice = 0;
	BYTE TmpOldRf = 0;
	BYTE TmpOldBio = 0;
	BYTE TmpOldVoice = 0;
	BYTE TmpNewJig = 0;
	BYTE TmpOldJig = 0;
	BYTE count = 50;

	gbtypevalue = 0;
	gbVoicevalue = 0;

	//----------------------------------------------------------------------
	//	Check option resiters
	//----------------------------------------------------------------------
	while(count)
	{
		TmpNewRf = P_OP_RFID_T;
		TmpNewBio = P_OP_FP_T;
		TmpNewVoice = P_OP_VOICE_T;
		TmpNewJig = P_JIG_POINT_T;
		if(TmpNewRf == TmpOldRf && TmpNewBio == TmpOldBio && TmpNewVoice == TmpOldVoice && TmpNewJig == TmpOldJig)
		{
			count--;
		}
		else
		{
			count = 50;
			 TmpOldRf = TmpNewRf;
			 TmpOldBio = TmpNewBio;
			 TmpOldVoice = TmpNewVoice;
			 TmpOldJig = TmpNewJig;
		}
	}

	if(TmpOldRf)		gbtypevalue |= DDL_SET_RF;
	if(TmpOldBio)		gbtypevalue |= DDL_SET_BIO;
	gbVoicevalue = TmpOldVoice;

	ChangeModelCheckPinToOutput(TmpOldRf, TmpOldBio, TmpOldVoice, TmpOldJig);
	
//	WatchdogCntClear();
#endif
}
//#endif


void SystemInitial(void)
{
//#ifdef	P_OP_RFID_T
	ModelCheck();
#if defined (_USE_IREVO_CRYPTO_)
	GetSTM32Uid();
#endif 
	//if(gbVoicevalue == DDL_SET_VOICE)	gfVoiceSet = 1; 
	//else								gfVoiceSet = 0; 
//#endif

#ifdef	P_VOICE_BUSY_T
#if defined (DDL_CFG_MS)
	gfVoiceSet = 0; 
#else 
	gfVoiceSet = 1; 
#endif 
#endif
}

void BOR_Config(void)
{
#if 0
	uint32_t 	BOROptionBytes;
	uint32_t 	BORLevel;	

	// OB_BOR_OFF	  /*!< BOR is disabled at power down, the reset is asserted when the VDD power supply reachs the PDR(Power Down Reset) threshold (1.5V)*/
	// OB_BOR_LEVEL1  /*!< BOR Reset threshold levels for 1.7V - 1.8V VDD power supply */
	// OB_BOR_LEVEL2  /*!< BOR Reset threshold levels for 1.9V - 2.0V VDD power supply */
	// OB_BOR_LEVEL3  /*!< BOR Reset threshold levels for 2.3V - 2.4V VDD power supply */
	// OB_BOR_LEVEL4  /*!< BOR Reset threshold levels for 2.55V - 2.65V VDD power supply */
	// OB_BOR_LEVEL5  /*!< BOR Reset threshold levels for 2.8V - 2.9V VDD power supply */

	Delay(SYS_TIMER_10MS);			// 전원 안정화 시간

	BORLevel = OB_BOR_LEVEL1;		// Vdd가  falling 시에 1.7v이하일 때 reset 핀 low, rising 시에 1.8v이상이 되면 reset핀 high

	BOROptionBytes = FLASH_OB_GetBOR();

	if((BOROptionBytes & 0x0F) != BORLevel)
	{
		/* Unlocks the option bytes block access */
		FLASH_OB_Unlock();

		/* Clears the FLASH pending flags */
		FLASH_ClearFlag(FLASH_FLAG_EOP|FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_SIZERR | FLASH_FLAG_OPTVERR);

		/* Select the desired V(BOR) Level ---------------------------------------*/
		FLASH_OB_BORConfig(BORLevel); 

		/* Launch the option byte loading */
		FLASH_OB_Launch();  
	}
#endif
}




