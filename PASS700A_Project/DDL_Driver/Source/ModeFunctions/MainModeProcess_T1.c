//------------------------------------------------------------------------------
/** 	@file		MainModeProcess_T1.c
	@version 0.1.00
	@date	2016.04.08
	@brief	Main UI Process 
	@remark	 UI에 따른 기본 동작 모드 수행
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.08		by Jay
			- 4byte UID 카드 및 7byte UID 카드를 모두 읽어서 해당 Data를 출력
		
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gbMainMode = 0;												/**< 동작 모드  */								 
BYTE gbModePrcsStep = 0;								 			/**< 동작 모드 내에서 처리를 위한 단계  */
						

WORD gModeTimeOutTimer100ms = 0;
WORD gModePrcsTimer10ms = 0;

BYTE gbManageMode = 0;



void ModeTimeOutCounter(void)
{
	if(gModeTimeOutTimer100ms) 	--gModeTimeOutTimer100ms;
}


void ModeProcessTimeCounter(void)
{
	if(gModePrcsTimer10ms)		--gModePrcsTimer10ms;
}


void SetModeTimeOut(WORD ModeTimeout)
{
	gModeTimeOutTimer100ms = ModeTimeout;
}


WORD GetModeTimeOut(void)
{
	return (gModeTimeOutTimer100ms);
}


void SetModeProcessTime(WORD PrcsTime)
{
	gModePrcsTimer10ms = PrcsTime;
}


WORD GetModeProcessTime(void)
{
	return (gModePrcsTimer10ms);
}


void TimeExpiredCheck(void)
{
	if(gModeTimeOutTimer100ms == 0)
	{
//		FeedbackError(101, VOL_CHECK);		//  times up
		FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//  times up
		ModeClear();
	}
}


BYTE GetMainMode(void)
{
	return (gbMainMode);
}


																			
//------------------------------------------------------------------------------
/** 	@brief	Card Process Time Counter
	@param	None
	@return 	None
	@remark 카드 읽기 처리 중에 사용하는 Timer, Timer Tick에서 호출하여 사용
	@remark 2ms Tick, 0~500ms 설정 동작
*/
//------------------------------------------------------------------------------
void ModeClear(void)
{
	gbMainMode = 0;
	gbModePrcsStep = 0;

#if  defined (DDL_CFG_MS)
	Ms_Credential_Info.bSelected_Item = 0xFF;
#endif 

#ifdef	DDL_CFG_RFID
	if(GetCardMode())
	{
		CardGotoReadStop();
	}
#elif	defined (DDL_CFG_IBUTTON)
	GMTouchKeyInputClear();
#elif	defined (DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
	if(GetDS1972ModeProcess())
	{
		iButtonModeClear();
	}
#endif
	LockStatusCheckClear();
}



																			
void(*const MainModeProcess_Tbl[])(void) = {
	ModeKeyInputCheck,
	ModeOpenClose,
	KeyTenKeyWakeUpProcess,
	ModeRegisterCheck,
	ModePINRegister,					
	ModePINVerify,

#ifdef 	DDL_CFG_RFID
	ModeCardRegister,
	ModeCardVerify,
#endif

#ifdef 	DDL_CFG_FP_TCS4K
	ModeFingerVerify,
	ModeFingerRegister,
	ModeAllFingerDelete,
	ModeFingerPTOpen,

	ModeOneFingerRegDelCheck,
	ModeOneFingerRegister,
	ModeOneFingerDelete,
#elif defined	(DDL_CFG_FP_PFM_3000)  || defined (DDL_CFG_FP_TS1071M) || defined (DDL_CFG_FP_HFPM) || defined (DDL_CFG_FP_INTEGRATED_FPM) //hyojoon_20160831 PFM-3000 add
	ModeFingerVerify,
	ModeFingerRegister,
	ModeAllFingerDelete,

	ModeOneFingerRegDelCheck,
	ModeOneFingerRegister,
	ModeOneFingerDelete,
#endif

#ifdef 	DDL_CFG_IBUTTON
	ModeGMTouchKeyRegister,
	ModeGMTouchKeyVerify,
#endif

#if defined (DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
	ModeDS1972TouchKeyRegister,
	ModeDS1972TouchKeyVerify,
#endif

	ModeMenuMainSelect,

	ModeMenuCredentialSelect,

	ModeMenuLockSetting,

#ifdef	LOCKSET_HANDING_LOCK
	ModeHandingLockSetting,
#endif

#ifdef	LOCKSET_AUTO_RELOCK
	ModeAutoLockSetting,
#endif

#ifdef	LOCKSET_VOLUME_SET
	ModeVolumeSetting,
#endif

#ifdef	DDL_CFG_TOUCH_OC
	ModeSafeOCButtonSetting,
#endif

	ModeVisitorCodeRegDelCheck,
	ModeVisitorCodeRegister,
	ModeVisitorCodeDelete,


	ModeOnetimeCodeRegDelCheck,
	ModeOnetimeCodeRegister,
	ModeOnetimeCodeDelete,
#ifdef 	DDL_CFG_RFID
	ModeAllCardDelete,

	ModeOneCardRegDelCheck,
	ModeOneCardRegister,
	ModeOneCardDelete,

#endif


#ifdef 	DDL_CFG_IBUTTON
	ModeAllGMTouchKeyDelete,
	
	ModeOneGMTouchKeyRegDelCheck,
	ModeOneGMTouchKeyRegister,							   
	ModeOneGMTouchKeyDelete,								   
	
#endif

#ifdef 	DDL_CFG_DS1972_IBUTTON
	ModeAllDS1972TouchKeyDelete,
	
	ModeOneDS1972TouchKeyRegDelCheck,
	ModeOneDS1972TouchKeyRegister,							   
	ModeOneDS1972TouchKeyDelete,
#endif

	ModeRemoconRegDelCheck,
	ModeRemoconRegister,							 
	ModeRemoconDelete,	
	
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
	ModeSubRemoconRegDelCheck,
	ModeSubRemoconRegister,							 
	ModeSubRemoconDelete,				   
#endif 

#ifdef DDL_CFG_ADD_REMOCON_LINK_FUNCTION
	ModeLockOpenConnectionRegister,
#endif

	ModeOneRemoconRegDelCheck,
	ModeOneRemoconRegister,							 
	ModeOneRemoconDelete,				   

#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
	ModeOneSubRemoconRegDelCheck,
	ModeOneSubRemoconRegister,							 
	ModeOneSubRemoconDelete,
#endif 	

	ModeRfLinkModuleRegDelCheck,
	ModeRfLinkModuleRegister,							 
	ModeRfLinkModuleDelete,				   

	ModeAdvancedModeSet,
	ModeNormalModeSet,

	ModeMasterCodeRegister,

	ModeOneUserCodeRegDelCheck,
	ModeOneUserCodeRegister,
	ModeOneUserCodeDelete,

	ModeAllCredentialsDelete,	

	ModeAlarmClearByKey,

	ModePackLockOperation,


	ModeTestMenu,
	ModeTestAging,
#ifdef 	DDL_CFG_RFID	
	ModeTestOpenUidSet,
#endif 	

#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
	ModeExchangeKeySet,
#endif 	

#ifdef	LOCKSET_LANGUAGE_SET
	ModeLanguageSetting, 
#endif

#ifdef	LOCK_TYPE_DEADBOLT
	ModeHandingProcess,
#endif

#ifdef	BLE_N_SUPPORT
#ifdef 	DDL_CFG_RFID
	ModeCardRegisterByBleN,
	ModeOneCardRegisterByBleN,	
#endif
	
#if defined	(DDL_CFG_FP_TCS4K) || defined	(DDL_CFG_FP_PFM_3000)  || defined (DDL_CFG_FP_TS1071M) //hyojoon_20160831 PFM-3000 add
	ModeFingerRegisterByBleN,
	ModeAllFingerDeleteByBleN,
	ModeOneFingerRegisterByBleN,
	ModeOneFingerDeleteByBleN,
#endif

#if defined	(DDL_CFG_DS1972_IBUTTON)
	ModeDS1972TouchKeyRegisterByBleN,
	ModeOneDS1972TouchKeyRegisterByBleN,
#endif 

	ModeRemoconRegisterByBleN,
	ModeRemoconDeleteByBleN,				   

	ModeOneRemoconRegisterByBleN,
	ModeOneRemoconDeleteByBleN,


#ifdef 	DDL_CFG_IBUTTON
	ModeGMTouchKeyRegisterByBleN,
	ModeOneGMTouchKeyRegisterByBleN,
#endif
	ModeTestBLEN,
#endif	//BLE_N_SUPPORT

#ifdef	DDL_AAAU_AUTOLOCK_HOLD //호주 및 뉴질랜드 전용 autolock hold 기능
	ModeOperationSelect,		
#endif	//DDL_AAAU_AUTOLOCK_HOLD #endif

#if defined (DDL_CFG_MS)
	MS_ModeCardCodeRegDelCheck,
	MS_ModeAuthorityVerify,
	MS_ModeMenuCredentialInfoSet,
#endif 

#if defined (_MASTER_CARD_SUPPORT)
	ModeMasterCardRegister,								   
#endif

	ModeAbnormalProcess,
	ModePackSetLedProcess,
	ModeAllDataReset,

#ifdef P_SW_FACTORY_BROKEN_T
	ModeWaitFactoryResetComplete,
#endif 	

#ifdef	CYLINDER_KEY_ALARM//실린더 알람 설정 메뉴 단계 추가.
	ModeCylinderKeyAlarmSetting,
#endif

#if defined (_USE_LOGGING_MODE_)
	ModeLoggingProcess,
	ModeMenuTimeSet,
#endif
	ModeTestGetCenter,
#if defined (FACOTRY_PINCODE_TEST_MODE)
	ModePincoeTestMode,
#endif 

/*
	ModeOpenClose,
	ModePINVerify,
	ModeCardVerify,
	ModeRegisterCheck,
	ModeCredentialRegDelCheck,			// 5
	ModePINRegister,					
	ModeCardRegister,
	ModeMenuCheck,
	ModeRfLinkModuleRegister,
	ModeRfLinkModuleDelete, 			// 10
	ModeRemoconRegister,				
	ModeRemoconDelete,
	ModeLanguageSetting,
	ModeHandingLockSetting,
	ModeAutoLockSetting,				// 15
	ModeVolumeSetting,
	ModeRemoconRegDelCheck,
	ModeRfLinkModuleRegDelCheck,
	ModeOneRemoconRegDelCheck,	
	ModeOneRemoconRegister, 		
	ModeOneRemoconDelete,
	ModeTamperProofLockout, 	
	ModeAlarmClearByKey,
	ModeTestMenu,						// 40
	ModeTestAging,
	ModeTestVoice,
	ModeTestPackSoftReset,
*/
};


void MainModeProcess(void)
{
	MainModeProcess_Tbl[gbMainMode]();
}





