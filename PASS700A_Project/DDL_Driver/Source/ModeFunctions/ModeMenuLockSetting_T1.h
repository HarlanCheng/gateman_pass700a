//------------------------------------------------------------------------------
/** 	@file		ModeMenuLockSetting_T1.h
	@brief	Lock Setting Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENULOCKSETTING_T1_INCLUDED
#define __MODEMENULOCKSETTING_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


#ifdef	DDL_CFG_LOCK_SET_TYPE1
	#define LOCKSET_LANGUAGE_SET
//	#define	LOCKSET_HANDING_LOCK
#define LOCKSET_AUTO_RELOCK
#define	LOCKSET_VOLUME_SET
#endif	


#ifdef	DDL_CFG_LOCK_SET_TYPE2
//	#define LOCKSET_LANGUAGE_SET
//	#define	LOCKSET_HANDING_LOCK
	#define LOCKSET_AUTO_RELOCK
	#define	LOCKSET_VOLUME_SET
#endif	


#ifdef	DDL_CFG_LOCK_SET_TYPE3
//	#define LOCKSET_LANGUAGE_SET
//	#define	LOCKSET_HANDING_LOCK
	//#define LOCKSET_AUTO_RELOCK
	//#define	LOCKSET_VOLUME_SET
#endif	

#ifdef	DDL_CFG_LOCK_SET_TYPE4
	#define LOCKSET_LANGUAGE_SET
//	#define	LOCKSET_HANDING_LOCK
//	#define LOCKSET_AUTO_RELOCK
//	#define	LOCKSET_VOLUME_SET
#endif	

#ifdef	DDL_CFG_LOCK_SET_TYPE5
	#define	LOCKSET_LANGUAGE_SET
	#define	LOCKSET_HANDING_LOCK
//	#define LOCKSET_AUTO_RELOCK
//	#define	LOCKSET_VOLUME_SET
#endif

#ifdef	DDL_CFG_LOCK_SET_TYPE6
	#define	LOCKSET_LANGUAGE_SET
	#define	LOCKSET_HANDING_LOCK
	#define LOCKSET_AUTO_RELOCK
	#define	LOCKSET_VOLUME_SET
#endif


#ifdef	DDL_CFG_LOCK_SET_TYPE7
	#define LOCKSET_AUTO_RELOCK
#endif 

#ifdef	DDL_CFG_LOCK_SET_TYPE8
	#define LOCKSET_AUTO_RELOCK
	#define LOCKSET_LANGUAGE_SET
#endif 

void ModeGotoMenuLockSetting(void);
void ModeMenuLockSetting(void);



#endif


