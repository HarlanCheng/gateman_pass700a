#include "Main.h"




uint16_t		sns_o, sns_c;
void	test_Motor( void )
{
	uint32_t		dir = 0;

	P_SNS_EN( 0 );
	Delay(SYS_TIMER_1MS);

	if ( sns_o = P_SNS_OPEN_T )
		dir = 1;
#ifdef	P_SNS_CLOSE_T
	else if ( sns_c = P_SNS_CLOSE_T )
		dir = 2;


	if ( dir == 1 ) {
		_MOTOR_CLOSE;
		do {
			sns_c = P_SNS_CLOSE_T;
		}
		while( !sns_c );

		_MOTOR_STOP;
	}
#endif
	else if ( dir == 2 ) {
		_MOTOR_OPEN;
		do {
			sns_o = P_SNS_OPEN_T;
		}
		while( !sns_o );

		_MOTOR_STOP;
	}
	
	P_SNS_EN( 1 );


}

/******************************************************************/

#ifdef M_CLOSE_PWMCH1_Pin
void SetMotorPWMFreq(void) // 10% ~ 100%
{
	static WORD  TempDuty = 0xFFFF;
	uint16_t	Duty = 0x0000;
	uint32_t	period = 1000; //16Mhz / period = 16Khz

	//L-Mecha 와 YDD424+ deadbolt 확인시 
	// start duty 50% 정도에 각 구간별 PWM_DUTY_HOLD_COUNTER 을 정해서 
	// 총 6구간으로 나누고 각 구간을 지나가면 10% 씩 올려 준다 
	// PWM_DUTY_HOLD_COUNTER 는 100 count 당 6ms 정도 소요 
	// PWM_DUTY_HOLD_COUNTER 이 값이 높을 수록 전압 주저 않는 정도는 줄어 드나 그만큼 도는 속도가 느리게 된다. 
	// 적당히 잘 잡아서 사용 할것 

	if(tim9_count < PWM_DUTY_HOLD_COUNTER)
	{
		Duty = PWM_START_DUTY;
	}
	else if(tim9_count < (PWM_DUTY_HOLD_COUNTER*2))
	{
		Duty = PWM_START_DUTY+10; // 60%
	}
	else if(tim9_count < (PWM_DUTY_HOLD_COUNTER*3))
	{
		Duty = PWM_START_DUTY+20; // 70%
	}
	else if(tim9_count < (PWM_DUTY_HOLD_COUNTER*4))
	{
		Duty = PWM_START_DUTY+30; // 80%
	}
	else if(tim9_count < (PWM_DUTY_HOLD_COUNTER*5))
	{
		Duty = PWM_START_DUTY+40; // 90%
	}
	else if(tim9_count >= (PWM_DUTY_HOLD_COUNTER*5))
	{
		Duty = PWM_START_DUTY+51; // 100% 이상 
	}

	Duty *= 10; // period 가 1000 이라 10을 곱해서 100% 을 맞춘다 계산 편의 .... 

	if(TempDuty != Duty)
	{
		gh_mcu_motor_timer9->Instance->ARR = period; // 값이 변하지는 않지만 혹시 모르니 ...

		if(Get_PWM_Direction() == MCTRL_DIR_OPEN)
			gh_mcu_motor_timer9->Instance->CCR2 = Duty;	// open duty
		else 
			gh_mcu_motor_timer9->Instance->CCR1 = Duty;	// close duty

		gh_mcu_motor_timer9->Instance->EGR = TIM_EGR_UG;		 //PWM 설정을 바꾸고 그 설정으로 PWM shadow register들을 update할 것을 지정한다
		TempDuty = Duty;		
	}
}
#endif 

uint32_t AdditionalPositionCheck(void);



void		deadbolt_lock_sensor_on( void )
{
	P_SNS_EN( 0 );								//Sensor Power On (Acitive Low)
}


void		deadbolt_lock_sensor_off( void )
{
	P_SNS_EN( 1 );								//Sensor Power Off (Acitive Low)
}

void		deadbolt_lock_motor_open( void )
{
	BYTE bTmp;

	bTmp = GetHandingLock();
	if(bTmp == HANDING_LEFT_SET)
	{
		_MOTOR_CLOSE;
#ifdef M_CLOSE_PWMCH1_Pin
		Motor_PWM_Init(MCTRL_DIR_CLOSE);
#endif 
	}
	else if(bTmp == HANDING_RIGHT_SET)
	{
		_MOTOR_OPEN;
#ifdef M_CLOSE_PWMCH1_Pin
		Motor_PWM_Init(MCTRL_DIR_OPEN);
#endif 
	}
}

void		deadbolt_lock_motor_close( void )
{
	BYTE bTmp;

	bTmp = GetHandingLock();
	if(bTmp == HANDING_LEFT_SET)
	{
		_MOTOR_OPEN;
#ifdef M_CLOSE_PWMCH1_Pin
		Motor_PWM_Init(MCTRL_DIR_OPEN);
#endif 
	}
	else if(bTmp == HANDING_RIGHT_SET)
	{
		_MOTOR_CLOSE;
#ifdef M_CLOSE_PWMCH1_Pin
		Motor_PWM_Init(MCTRL_DIR_CLOSE);
#endif
	}
}

void		deadbolt_lock_motor_stop( void )
{
#ifdef M_CLOSE_PWMCH1_Pin
	Motor_PWM_DeInit();
#endif 	
	_MOTOR_STOP;
}

void		deadbolt_lock_motor_hold( void )
{
#ifdef M_CLOSE_PWMCH1_Pin
	Motor_PWM_DeInit();
#endif 
	_MOTOR_BREAK;
}

uint32_t		deadbolt_lock_is_motor_open( void )
{
	uint32_t bPosition;

	bPosition = P_SNS_OPEN_T;

	if(bPosition == 0)
	{
		bPosition = AdditionalPositionCheck();
	}

	return	(bPosition);
}

uint32_t		deadbolt_lock_is_motor_close( void )
{
	uint32_t bPosition;

// 좌우수가 잘못 설정될 경우 다시 설정할 수 있는 모드로 들어갈 수 없는 상태가 되어
//	Close Sensor는 좌우수 방향에 관계 없이 모두 확인하는 것으로 수정

/*
	BYTE bTmp;

	bTmp = GetHandingLock();
	if(bTmp == HANDING_LEFT_SET)
	{
		bPosition = P_SNS_CLOSE1_T;
	}
	else if(bTmp == HANDING_RIGHT_SET)
	{
		bPosition = P_SNS_CLOSE2_T;
	}
	else
*/
	{
		bPosition = P_SNS_CLOSE1_T | P_SNS_CLOSE2_T;
	}

	return	(bPosition);
}

#ifdef	P_SNS_CENTER_T
uint32_t		deadbolt_lock_is_motor_center( void )
{
	return	P_SNS_CENTER_T;
}
#endif

#ifdef	P_SNS_LOCK_T
uint32_t		deadbolt_lock_is_motor_lock( void )
{
	return	(!P_SNS_LOCK_T);
}
#endif



ddl_motor_ctrl_t		deadbolt_motor  = {
	.TO_open= { 1500, _USE_MOTOR_TIME_SET, 10, 25, 150, 0, _USE_MOTOR_TIME_SET, 0, 20},			// unit 2msec
	.TO_close= { 1500, _USE_MOTOR_TIME_SET, 10, 25, 150, 0, _USE_MOTOR_TIME_SET, 0, 20},			// unit 2msec

	.init_sensor_hw	= NULL,
	.sensor_on		= deadbolt_lock_sensor_on,
	.sensor_off		= deadbolt_lock_sensor_off,

	.motor_open		= deadbolt_lock_motor_open,
	.motor_close	= deadbolt_lock_motor_close,
	.motor_stop		= deadbolt_lock_motor_stop,
	.motor_hold		= deadbolt_lock_motor_hold,

	.is_motor_open	= deadbolt_lock_is_motor_open,
	.is_motor_close	= deadbolt_lock_is_motor_close,
	.is_motor_center	= NULL,
#ifdef		P_SNS_LOCK_T	
	.is_motor_lock	= rim_lock_is_motor_lock
#else
	.is_motor_lock	= InnerForcedLockCheck
#endif
};



void SetupBatteryPwrFactor(void);	

void	Init_Motor( void )
{
#ifdef	P_SNS_LEFT_RIGHT_T
	HandingLockAutoRun();
#endif

	register_motor_ctrl( &deadbolt_motor );

	MotorStatusInitialCheck();

	SetupBatteryPwrFactor();

#ifdef	LOCKSET_HANDING_LOCK
	GetHandingLock();
#endif 

#ifdef	LOCK_TYPE_DEADBOLT
	AdditionalPositionCheckOn();
#endif
}



//=============================================================
// Inner Forced Lock set by O/C button
//=============================================================

#ifdef		P_SNS_LOCK_T	

uint32_t InnerForcedLockCheck(void)
{
	if(!P_SNS_LOCK_T)	return 1;

	return 0;
}

#else

BYTE gbInLockEn = 0; 

void InnerForcedLockSet(void)
{
	gbInLockEn = 0xAA;
	RomWrite(&gbInLockEn, (WORD)INLOCK_STATE, 1);	
}

void InnerForcedLockClear(void)
{
	gbInLockEn = 0xBB;
	RomWrite(&gbInLockEn, (WORD)INLOCK_STATE, 1);	
}


void InnerForcedLockSettingLoad(void)
{
	RomRead(&gbInLockEn, (WORD)INLOCK_STATE, 1);		
	if(gbInLockEn != 0xAA)
	{
		gbInLockEn = 0xBB;
	}
}

uint32_t InnerForcedLockCheck(void)
{
	if(gbInLockEn == 0xAA)	return 1;

	return 0;
}

#endif





void AdditionalPositionCheckOn(void)
{
	BYTE bTmp;
	
	bTmp = GetHandingLock();
	if(bTmp == HANDING_LEFT_SET)
	{
		P_EN_MECHA_HALL1(0);
		P_EN_MECHA_HALL2(1);
	}
	else if(bTmp == HANDING_RIGHT_SET)
	{
		P_EN_MECHA_HALL2(0);
		P_EN_MECHA_HALL1(1);
	}
	else
	{
		P_EN_MECHA_HALL2(1);
		P_EN_MECHA_HALL1(1);
	}
}




uint32_t AdditionalPositionCheck(void)
{
	uint32_t bPosition = 0;
	BYTE bTmp;

	// 닫힘 센서가 감지되지 않는 상태에서 해당 센서는 유효
	if(deadbolt_lock_is_motor_close() == 0)
	{
		// 모터 구동 전 단계에서 해당 센서는 유효
		if((GetMotorPrcsStep() == 0) 
		// 모터 OPEN 구동 시작 단계에서 해당 센서는 유효
			|| (((GetMotorPrcsStep() & MCTRL_DIR_MASK ) == MCTRL_DIR_OPEN)
				&& ((GetMotorPrcsStep() & MCTRL_STATE_MASK) == MPROC_PRE)))
		{
			bTmp = GetHandingLock();
			if(bTmp == HANDING_LEFT_SET)
			{
				if(!P_MECHA_HALL2_T)	bPosition = 1;
			}
			else if(bTmp == HANDING_RIGHT_SET)
			{
				if(!P_MECHA_HALL1_T)	bPosition = 1;
			}
		}
	}
	
	return (bPosition);
}



float AdcToPwrVoltFactor;
float PwrVolt;


void SetupBatteryPwrFactor(void)			// 파워 전압 계산을 하기 위한 인자들 설정
{
///////////// 사용자 정의 변수 //////////////////////////////////////////////////////////////////////////////////////////////////////////
	float R1 = 5.6;						// 항상 소수점으로 표시 2 -> 2.0 			// 위쪽 분배 저항 R1,  (실제저항값) 나누기 (1000)해서 적기.. 5.6K옴  -> 5600/1000 = 5.6
	float R2 = 2.0;						// 항상 소수점으로 표시 2 -> 2.0			// 아래 분배 저항 R2,  (실제저항값) 나누기 (1000)해서 적기.. 2.0K옴  -> 2000/1000 = 2.0
	float AdcRefVolt = 3.3;				// ADC 레퍼런스 전압. Vref가 3.3V면 3.3, 3.0V면 3.0
	BYTE AdcResolutionBit = 12;			// 12비트 레졸루션이면 12 씀,.. 10비트 레졸류션이면 10
									// 무조건 오른쪽 정렬로 한다.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	float AdcToVoltFactor;				// ADC값을 전압으로 변환해주는 팩터
	float VoltToPwrFactor;				// 분배된 전압을 pwr 전압으로 변환해 주는 팩터
	WORD AdcResolution;				// 레졸루션 10진수 값
	BYTE bCnt;

	AdcResolution = 1;
	for(bCnt = 0; bCnt < AdcResolutionBit; bCnt++)
	{	
		AdcResolution = AdcResolution * 2;
	}

	AdcToVoltFactor = AdcRefVolt / ((float)AdcResolution);				// 0.00080566405
	VoltToPwrFactor = R2 / (R1 + R2);							// 전압 분배 값

	AdcToPwrVoltFactor = AdcToVoltFactor / VoltToPwrFactor;

}

void PwrVoltCalculation(void)						// 파워 전압 계산
{
	PwrVolt = ((float)GetBatteryAdcValue()) * AdcToPwrVoltFactor;		// 파워 전압 계산	
}

BYTE MotorTimeIndexCalculation(void)				// 모터 구동 타임의 배열 인덱스 설정
{
	BYTE bMotorTimeIndex;
	
	if(PwrVolt > 6.99)	PwrVolt = 6.9;						// 전압이 6.9v이상이면 무조건 6.9볼트로 고정, 6.9볼트 이상은 계산하지 않는다.
	if(PwrVolt < 3.0)	PwrVolt = 3.0;						// 전압이 3v이하면 무조건 3볼트로 고정, 3볼트 이하는 계산하지 않는다.

	bMotorTimeIndex = (BYTE)((PwrVolt * 10.0) - 30.0);		// volt에서 30을 뺌, Index의 범위를 0~39(총 40종류, 3.0V, 3.1V,3.2V ~ 6.9V)까지 하기 위해서 30을 뺌

	if(bMotorTimeIndex > 39) bMotorTimeIndex = 39;		// 배열의 인덱스가 됨

	return bMotorTimeIndex;
}


BYTE MotorTimeGenerate(BYTE ArryIndex, BYTE MinTime10ms, BYTE MaxTime10ms)
{
	BYTE bCnt;
	BYTE bFinalMotorTime;
	float bTimeInterval;
	float bTime;

	bFinalMotorTime = 0;

	if(MinTime10ms > MaxTime10ms)
	{
		return 0;
	}

	if(MinTime10ms == MaxTime10ms)
	{
		return MinTime10ms;
	}

	bCnt = MaxTime10ms - MinTime10ms;
	bTimeInterval = ((float)bCnt) / (40.0 - 1.0);		// -1을 하는 이유는 40단계이지만 개수는 39개로 함.

	bTime = (float)MinTime10ms;	

	for(bCnt = 40; bCnt > 0; bCnt--)				// 직선보간법	
	{
		if( ArryIndex == (bCnt - 1) )
		{
			bFinalMotorTime = (BYTE)bTime;
			break;
		}
		
		bTime = bTime + bTimeInterval;
	}

	return bFinalMotorTime;

}

// 현재 파워 전압에 따른 모터 구동 시간 리턴 해줌
// Max, Min : 0~255
BYTE MotorTimeSetting(BYTE MinTime10ms, BYTE MaxTime10ms)		// 이 함수 구동 시간 STM32L151VBXXA 32Mhz 기준 130us
{
	BYTE bTmp;

	PwrVoltCalculation();
	bTmp = MotorTimeIndexCalculation();						// 파워 전압에 따른 모터 시간 배열 인덱스 추출
	bTmp = MotorTimeGenerate(bTmp, MinTime10ms, MaxTime10ms);		// 파워 전압 대 모터 구동 시간 테이블 생성

	return bTmp;
}



uint32_t MotorOpenCloseTimeSetting_open(void)
{
	uint32_t RetTimeValue;
	
	RetTimeValue = (uint32_t)MotorTimeSetting(11, 36); 	// 180ms ~ 300ms

	// 10ms Timer 기준으로 나온 값을 2ms Timer에 사용할 수 있도록 처리
	RetTimeValue = RetTimeValue * 5;
	
	return (RetTimeValue);
}


uint32_t MotorOpenCloseTimeSetting_close(void)
{
	uint32_t RetTimeValue;
	
	if(GetHandingLock()== HANDING_LEFT_SET) 
	{
		RetTimeValue = (uint32_t)MotorTimeSetting(1, 8);	//  소결 1`차 대책	
	}
	else
	{
		RetTimeValue = (uint32_t)MotorTimeSetting(2, 9);	//  소결 1`차 대책	
	}	

	// 10ms Timer 기준으로 나온 값을 2ms Timer에 사용할 수 있도록 처리
	RetTimeValue = RetTimeValue * 5;
	
	return (RetTimeValue);
}





uint32_t MotorBackTurnTimeSetting_open(void)
{
	uint32_t RetTimeValue;
	
	RetTimeValue = (uint32_t)MotorTimeSetting(5, 13); 	// 50ms ~ 130ms   :  무부하 간혹 안됨 

	// 10ms Timer 기준으로 나온 값을 2ms Timer에 사용할 수 있도록 처리
	RetTimeValue = RetTimeValue * 5;
	
	return (RetTimeValue);
}
	

uint32_t MotorBackTurnTimeSetting_close(void)
{
	uint32_t RetTimeValue;
	
	RetTimeValue = (uint32_t)MotorTimeSetting(5, 13); 	// 50ms ~ 130ms   :  무부하 간혹 안됨 

	// 10ms Timer 기준으로 나온 값을 2ms Timer에 사용할 수 있도록 처리
	RetTimeValue = RetTimeValue * 5;
	
	return (RetTimeValue);
}



