//------------------------------------------------------------------------------
/** 	@file		ModeOpenCloseProcess_T1.c
	@brief	Open/Close Button Process 
*/
//------------------------------------------------------------------------------

#ifndef __MODEOPENCLOSEPROCESS_T1_INCLUDED
#define __MODEOPENCLOSEPROCESS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoOpenClose(void);
void ModeOpenClose(void);
void StartMotorToggle(void);
#ifdef USED_SECOND_MOTOR //모터 두개 대응(Secure mode 포함)
void StartMotorToggle_Second(void);
#endif



//------------------------------------------------------------------------------
// gbModePrcsStep에서 참조 - 열림/닫힘 버튼 처리 단계
//------------------------------------------------------------------------------
enum{
	MODEPRCS_OPENCLOSE_START_BYKEY = 0,
	MODEPRCS_OPEN_BYKEY_COMPLETE_CHECK,
	MODEPRCS_OPEN_BYKEY_COMPLETE_ERROR,

#ifndef		P_SNS_LOCK_T	
	MODEPRCS_INNER_LOCK_SET_CHECK,
	MODEPRCS_CLOSE_BYLOCKOUT_COMPLETE_CHECK,
	MODEPRCS_CLOSE_BYLOCKOUT_COMPLETE_ERROR,
#endif
	MODEPRCS_OPENCLOSE_START_CHECK_KEY //  Open/Close button OChyojoon.kim	
};	


#endif


