//------------------------------------------------------------------------------
/** 	@file		RemoconFunctions_T1.h
	@brief	Remote controller Command Functions
*/
//------------------------------------------------------------------------------


#ifndef __REMOCONFUNCTIONS_T1_INCLUDED
#define __REMOCONFUNCTIONS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"

BYTE PackRemoconCmdProcess(void);
BYTE PackRemoconAckProcess(void);
BYTE InnerPackRemoconCmdProcess(void);
BYTE InnerPackRemoconAckProcess(void);


void PackTxConnectionSend(BYTE bEvent, BYTE bType, BYTE bSlot);
void PackTxConnectionSendForRF(BYTE bEvent, BYTE bType, BYTE bSlot);
void SubPackTxConnectionSend(BYTE bEvent, BYTE bType, BYTE bSlot);


#define MODULE_TYPE_TWOWAY		0x01
#define MODULE_TYPE_ONEWAY		0x02
#define MODULE_TYPE_1_ONEWAY	0x03



#endif


