//------------------------------------------------------------------------------
/** 	@file		ModePincodeVerify_T1.c
	@brief	Pincode Verify Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEPINCODEVERIFY_T1_INCLUDED
#define __MODEPINCODEVERIFY_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


extern BYTE gOutForcedLockSettingCnt;
extern BYTE gbLockOperationbyAccessoryAckAccess;



void ModeGotoPINVerify(void);
void ModePINVerify(void);

void MotorOpenCompleteCheck(void (*CompleteFunction)(void), BYTE ErrorStep);
void WaitStartOutForcedLockSetting(BYTE NextStep);
void WaitSomeTimeByLongContact(void (*CancelCondition)(void), BYTE NextStep);
void MotorCloseCompleteCheck(void (*CompleteFunction)(void), BYTE ErrorStep);
void VerifiedProcessByPincode(void);



//------------------------------------------------------------------------------
// gbModePrcsStep에서 참조 - 비밀번호 인증 처리 세부 단계
//------------------------------------------------------------------------------
enum{
	MODEPRCS_MULTI_CHECK = 1,
	MODEPRCS_VERIFY_TENKEY_START,
	MODEPRCS_VERIFY_TENKEY_TEMPSAVE,
	MODEPRCS_SCHEDULE_CHECK,
	MODEPRCS_GET_TIME_CHECK,
	MODEPRCS_SCHEDULE_VERIFY,
	MODEPRCS_OPEN_START_BYPINCODE,
	MODEPRCS_OPEN_BYPINCODE_COMPLETE_CHECK,
#ifdef	CLOSE_BYPINCODE//Pincode를 이용한 모터 잠김을 처리 하기 위함.
	MODEPRCS_CLOSE_BYPINCODE_COMPLETE_CHECK,
#endif	
	MODEPRCS_LOCKOUTINIT_BY_PIN,
	MODEPRCS_LOCKOUTSET_BY_PIN, 
	MODEPRCS_LOCKOUTSET_BY_PIN_CHK,
	MODEPRCS_LOCKOUTSET_SEND_CHK,
	MODEPRCS_LOCKOUTSET_ERROR,
	MODEPRCS_LOCKOUTSET_END
};	


//------------------------------------------------------------------------------
// gPincodeVerifyMode에서 참조 - 비밀번호 인증 모드 단계
//------------------------------------------------------------------------------
enum{
	PINCODEMODE_ONETIMESILENT_CHECK = 0,
	PINCODEMODE_VERIFYTENKEY_STEP,
	PINCODEMODE_VERIFYSCHEDULE_STEP,
	PINCODEMODE_OPENPROCESS_STEP,
	PINCODEMODE_OUTFORCEDLOCKSET_STEP,
};	


extern BYTE gfMute;


#endif


