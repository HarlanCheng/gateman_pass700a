#define		_MODE_FINGER_VERITY_C_


#include "Main.h"

void MotorOpenCompleteByFinger(void)
{
	BYTE bTmpArray[10];

	CopySlotNumberForPack((WORD)ConvertFingerprintSlotNumber(gbFID));

#if defined (_USE_LOGGING_MODE_)
	bTmpArray[0] = EV_USER_CREDENTIAL_UNLOCK;
	bTmpArray[1] = CREDENTIALTYPE_FINGERPRINT; 
	bTmpArray[2] = 0x00;	
	bTmpArray[3] = GetSlotNumberForPack();
	SaveLog(bTmpArray,4);
#endif

	bTmpArray[0] = CREDENTIALTYPE_FINGERPRINT;
	bTmpArray[1] = GetSlotNumberForPack();
	memset(&bTmpArray[2], 0x00, 8);
	PackTx_MakePacket(EV_USER_CREDENTIAL_UNLOCK, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 10);

	gbModePrcsStep = 4;
}
#ifdef	CLOSE_BYCREDENTIAL//지문을 이용한 모터 잠김 정보 송신
void MotorCloseCompleteByFinger(void)
{
	BYTE bTmpArray[11];

	CopySlotNumberForPack((WORD)ConvertFingerprintSlotNumber(gbFID));

	bTmpArray[0] = CREDENTIALTYPE_FINGERPRINT;
	bTmpArray[1] = GetSlotNumberForPack();
	bTmpArray[2] = 0x80;	//Lock
	memset(&bTmpArray[3], 0x00, 8);
	PackTx_MakePacket(EV_USER_CREDENTIAL_LOCK, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 11);

	gbModePrcsStep = 4;
}
#endif

void OutForcedLockSetByFinger(void)
{
	BYTE bTmpArray[11];

	DDLStatusFlagSet(DDL_STS_OUTLOCK);

	BioModuleOff();
	bTmpArray[0] = 0xFF;		// No Code Data
	bTmpArray[1] = CREDENTIALTYPE_FINGERPRINT;
	bTmpArray[2] = GetSlotNumberForPack();
	memset(&bTmpArray[3], 0xFF, 8);
	PackTx_MakePacket(EV_ARM_REQEST, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 11);
	SetModeProcessTime(100);
	gbModePrcsStep = 9;
}

//------------------------------------------------------------------------------
/** 	@brief	FingerPrint Register Mode Start
	@param	None
	@return 	None
	@remark 지문 인증 모드 시작
*/
//------------------------------------------------------------------------------
void	ModeGotoFingerVerify( void )
{
	gbMainMode = MODE_FINGER_VERIFY;
	gbModePrcsStep = 0;
}

void ModeFingerVerify(void)
{
	BYTE bTmp;
#ifdef	CLOSE_BYCREDENTIAL//지문에 의한 모터 잠금을 수행 하기 위해 모터 센서 값을 받는 변수
	BYTE bTemp;
#endif	
	switch(gbModePrcsStep)
	{
		case 0:
			bTmp = MotorSensorCheck();
			switch(bTmp)
			{
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CENTERLOCK_STATE:
				case SENSOR_LOCK_STATE:
				case SENSOR_CLOSECENLOCK_STATE:
					if(AlarmStatusCheck() == STATUS_SUCCESS) // 경보가 있으면 경보 일단 해제 위해 인증 모드로 
					{
						BioModuleON();
						gbModePrcsStep = 1;	
						SetModeTimeOut(5);
					}
					else // 경보가 없으면 내부강제잠금 확인 
					{
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
						if(bTmp == SENSOR_CLOSELOCK_STATE)
						{
							BioModuleON();							
							FeedbackLockInOpenAllowState();
							gbModePrcsStep = 1;	
							SetModeTimeOut(50);
						}
						else if(bTmp == SENSOR_OPENLOCK_STATE)
						{
							bTmp = AlarmStatusCheck();
							if(bTmp == STATUS_FAIL) {			//경보발생 상황이 아니면 문 닫힘 동작

								// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
								if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
								{
									FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
									ModeClear();
									break;							
								}
#ifdef	MOTOR_CLOSED_IN_OPEN_STATUS

#ifdef	CLOSE_BYCREDENTIAL	//내부 강제 잠김 상태에서 네부장제잠김 알람 출력후 지문 Verify mode로 진입 하게 하기 위함.
								BioModuleON();
								FeedbackLockInOpenAllowState();
								gbModePrcsStep = 1; 
								SetModeTimeOut(50);
								break;
#else	//__DDL_MODEL_PANPAN_FC2A_P #else
								if(gfDoorSwOpenState)	//문이 열려있는 경우
								{		
									FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
									ModeClear();
									break;			
								}
#endif	//__DDL_MODEL_PANPAN_FC2A_P #endif

#endif
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN								
								BatteryCheck();
#else
								FeedbackMotorClose(); 
#endif
								StartMotorClose();

								gbModePrcsStep = 30;		// wait for motor operatation finished
								break;
							}
						}
						else
						{
							PackAlarmReportInnerForcedLockOpenCloseFailSend();
							FeedbackLockIn();
							ModeClear();
						}
#else 		
						PackAlarmReportInnerForcedLockOpenCloseFailSend();
						FeedbackLockIn();
						ModeClear();
#endif 						
					}
					break;

#if 0
				case SENSOR_OPEN_STATE:
				case SENSOR_OPENCEN_STATE:
					bTmp = AlarmStatusCheck();
					if(bTmp == STATUS_FAIL) {			//경보발생 상황이 아니면 문 닫힘 동작

						// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
						if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
						{
							FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
							ModeClear();
							break;							
						}
#ifdef	MOTOR_CLOSED_IN_OPEN_STATUS
						if(gfDoorSwOpenState)	//문이 열려있는 경우
						{		
							FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
							ModeClear();
							break;			
						}
#endif								

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
						BatteryCheck();
#else
						FeedbackMotorClose();
#endif
						StartMotorClose();

						gbModePrcsStep = 30;		// wait for motor operatation finished
						break;
					}
					//경보발생 상황이면 지문 인증 절차 진행. "break" 구문 없음에 유의
#endif 
				default:	
					BioModuleON();
					gbModePrcsStep = 1;	
					SetModeTimeOut(10);
					break;					
			}
			break;
			
		case 1:
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )
				break;
#endif 

			if(GetModeTimeOut() == 0)
			{
				/* ATR 이 1s 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep	= 100;

				if(gbJigTimer1s){
					memset(gbJigInputDataBuf, 0xFF, 9);
					gbJigInputDataBuf[0] = 0x00;
					gbJigInputDataBuf[1] = 0x01;
					gbJigInputDataBuf[2] = 0x00;				
				}				
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				BYTE TempUID[8] = {0x00,};
				
				gbFingerPrint_rxd_end = 0;

				if(gbJigTimer1s){
					memset(gbJigInputDataBuf, 0xFF, 9);
					gbJigInputDataBuf[0] = 0x00;
					gbJigInputDataBuf[1] = 0x01;
					gbJigInputDataBuf[2] = 0x01;				
				}
	
				// Device status check 
				if(gbFingerPrint_rxd_buf[12] != 0x00)
				{
					FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					gbModePrcsStep	= 100;
					break;
				}

				//Get ALT 
				gbfingerALT = gbFingerPrint_rxd_buf[11];
				SetModeTimeOut(_MODE_TIME_OUT_7S);//verify 는 7sec 
				RomRead(TempUID,HFPM_MODULE_ID,8);

				if(memcmp(&gbFingerPrint_rxd_buf[3],TempUID,8) != 0x00)
				{
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					TamperCountIncrease();
					gbModePrcsStep = 100;
					break;
				}
				else 
				{
					gbModePrcsStep = 2;
					NFPMTxEventVerify(0x00);
				}
			}
		break;

		case 2:	
			if(GetModeTimeOut() == 0)
			{
				/* ALT 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep	= 100;
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				BYTE VerifyResult[4] = {NFPM_STX,0x03,NFPM_EVNET_VERIFY,0x00};
				
				gbFingerPrint_rxd_end = 0;

				gbFID = gbFingerPrint_rxd_buf[4];
				
				if(memcmp(gbFingerPrint_rxd_buf,VerifyResult,4) != 0x00 || gbFID == 0xFF)
				{
					/* 등록된 지문이 아니거나 uart 가 잘못된 경우 */
					/* slotnumber 가 이상한 경우 */	
					/* 
						gbFingerPrint_rxd_buf[3]
						0x00 : verify ok 
						0x01 : verify fail
						0x02 : No fingerprint
						0x03 : No live fingerprint (ex : fake fingerprint)
						0x04 ~ 0xFF : reserved 
					*/
					if( gbFingerPrint_rxd_buf[3] != 0x02)
					{
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						TamperCountIncrease();
					}
					gbModePrcsStep	= 100;
					break;
				}
				
				PCErrorClearSet();
				SetModeTimeOut(100);
#ifdef	BLE_N_SUPPORT
				/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */ 
				if(AllLockOutStatusCheck(0xFF))
				{
					FeedbackAllCodeLockOut();
					gbModePrcsStep = 100;
					break;
				}
#endif

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
				{
					if(ScheduleEnableCheck_Ble30(CREDENTIALTYPE_FINGERPRINT, (ConvertFingerprintSlotNumber(gbFID)-1)) != 0)
					{
						gbModePrcsStep = 251;
						break;
					}
				}
#endif	// DDL_CFG_BLE_30_ENABLE
				bTmp = AlarmStatusCheck();
				if(bTmp == STATUS_SUCCESS)
				{
					AlarmGotoAlarmClear();
					gbModePrcsStep = 100;
				}
				else
				{
#ifdef	CLOSE_BYCREDENTIAL	
//지문 인증이 정상이면 무조건 모터 open이 아니고 센서 값에 따라 모터 동작을 결정 한다.
					bTemp = MotorSensorCheck();
					switch(bTemp)
					{					
						case SENSOR_CLOSE_STATE:
						case SENSOR_CLOSECEN_STATE:
						case SENSOR_CLOSELOCK_STATE:
						case SENSOR_CLOSECENLOCK_STATE:
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
							BatteryCheck(); 
#else
							FeedbackMotorOpen();
#endif
							StartMotorOpen();
							gbCoverAccessDelaytime100ms = 10;
							gbModePrcsStep = 3;

							break;
			
						case SENSOR_OPEN_STATE:
						case SENSOR_OPENCEN_STATE:
						case SENSOR_OPENLOCK_STATE:
						case SENSOR_OPENCENLOCK_STATE:
						default : //case SENSOR_NOT_STATE, case SENSOR_LOCK_STATE, case SENSOR_CENTER_STATE,case SENSOR_CENTERLOCK_STATE
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
							BatteryCheck(); 
#else
							FeedbackMotorClose();
#endif
							StartMotorClose();
							gbCoverAccessDelaytime100ms = 10;
							gbModePrcsStep = 20;	
							break;	
						
					}		
#else	//__DDL_MODEL_PANPAN_FC2A #else
				
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck(); 
#else
					FeedbackMotorOpen();
#endif
					StartMotorOpen();
					gbCoverAccessDelaytime100ms = 10;
					gbModePrcsStep = 3;
					
#endif	//__DDL_MODEL_PANPAN_FC2A #endif				
				}
				DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
				TamperCountClear();
				NFPMTxEventStop(0x00);
			}	
			break;
	
		case 3:
			MotorOpenCompleteCheck(MotorOpenCompleteByFinger,30);
			break;
#ifdef	CLOSE_BYCREDENTIAL	//지문을 이용한 모터 닫힘 동작 수행후 이후 센서 확인 및 다음 스텝 결정
		case 20:
			MotorCloseCompleteCheck(MotorCloseCompleteByFinger,30);
			break;	
#endif			
			
		case 4:
			if(GetLedMode() || GetBuzPrcsStep()) break;
			gbModePrcsStep = 5;
			break;
			
		case 5:	
#ifdef DDL_CFG_FP_COVER_ACTIVE_HIGH	
			if(P_IBUTTON_COVER_T)
#else 
			if(!P_IBUTTON_COVER_T)
#endif 
			{
				gbModePrcsStep =6;
			}
			else
			{
				LedSetting(gcbLedOff, LED_DISPLAY_OFF);
				BioOffModeClear();
			}
			break;
		
		case 6:
			if(GetBuzPrcsStep() || GetVoicePrcsStep())		break;
			gbFingerLongTimeWait100ms = 24;
			gbModePrcsStep = 7;
			break;

		case 7:
			if(gbFingerLongTimeWait100ms == 17)
			{
				gbFingerLongTimeWait100ms--;
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);
				LedSetting(gcbLedOutLock1, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 11)
			{
				gbFingerLongTimeWait100ms--;
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);
				LedSetting(gcbLedOutLock2, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 5)
			{
				gbFingerLongTimeWait100ms--;
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);
				LedSetting(gcbLedOutLock3, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 0){ 				//외부강제잠금
				LedSetting(gcbLedOutLock4, LED_KEEP_DISPLAY);
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorClose();
#endif
				StartMotorClose();
				gbModePrcsStep = 8;
				break;
			}
#ifdef DDL_CFG_FP_COVER_ACTIVE_HIGH	
			if(!P_IBUTTON_COVER_T)
#else
			if(P_IBUTTON_COVER_T)
#endif 				
			{
				LedSetting(gcbLedOff, 0);
				BioOffModeClear();
			}
			break;
			
		case 8:
			MotorCloseCompleteCheck(OutForcedLockSetByFinger,30);			
			break;
		
		case 9: 	
			if(GetModeProcessTime()== 0)
			{
				PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
				BioOffModeClear();
			}
			break;

		case 30:
#if 0
			if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
			bTmp = GetFinalMotorStatus();

			switch( bTmp ) 
			{
				case	FINAL_MOTOR_STATE_OPEN_ING:
				case	FINAL_MOTOR_STATE_CLOSE_ING:
					break;

				case	FINAL_MOTOR_STATE_OPEN_ERROR:
					gbModePrcsStep++;

					FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);

					// Pack Module로 Event 내용 전송 
					PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x01);
					break;
			
				case	FINAL_MOTOR_STATE_CLOSE_ERROR:
					gbModePrcsStep++;

					FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
			
					// Pack Module로 Event 내용 전송 
					PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x00);
					break;

				case	FINAL_MOTOR_STATE_OPEN:
					// 지문 인증 과정에서 해당 부분으로 Pack으로 Event 전송할 경우 없음.
					BioOffModeClear();
					break;

				case	FINAL_MOTOR_STATE_CLOSE:
					// Manual Lock Event 전송
					PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					FeedbackMotorClose();
#endif 
					gbModePrcsStep = 32;
					//BioOffModeClear();
					break;
			}
		break;

		case 31:
			if(GetLedMode())		break;
			
			if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}
			else if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}
			BioOffModeClear();
			break;

		case 32:
			if(GetLedMode())		break;

			BioOffModeClear();
			break;


		case 100:
			NFPMTxEventStop(0x00);
			SetModeTimeOut(gbfingerALT);
			gbModePrcsStep = 101;
			break;
		
		case 101:
			if(GetModeTimeOut() == 0)
			{
				BioOffModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				// Rx 만 받으면 그냥 off 
				gbFingerPrint_rxd_end = 0;
				BioOffModeClear();	
				break;
			}
		break;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
		case 251:
			TimeDataClear();

			PackTx_MakePacket(F0_GET_TIME_AND_DATE, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);

			// Time Data가 1초 내에 회신되지 않으면 에러 처리
			gModeTimeOutTimer100ms = 10;

			gbModePrcsStep++;
			break;

		case 252:
			if(IsGetTimeDataCompleted() == true)
			{
				gbModePrcsStep++;
			}
			else if(gModeTimeOutTimer100ms == 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				BioOffModeClear();
			}
			break;			

		case 253:
			if(ScheduleVerify_Ble30(CREDENTIALTYPE_FINGERPRINT, gbTimeBuff_Ble30, (gbFID)) == 1)
			{
				bTmp = AlarmStatusCheck();
				if(bTmp == STATUS_SUCCESS)
				{
					AlarmGotoAlarmClear();
					gbModePrcsStep = 100;
				}
				else
				{
#ifdef	CLOSE_BYCREDENTIAL
//지문 인증이 정상이면 무조건 모터 open이 아니고 센서 값에 따라 모터 동작을 결정 한다.
//SCHEDULE 기능 테스트 후 Define을	__DDL_MODEL_PANPAN_FC2A -> DDL_CREDENTIAL_LOCK으로 바꿀지 확인
//PANPAN에서 Card SCHEDULE을 테스트 하지 못했고 FDS도 2020년03월11까지 SCHEDULE을 테스트 하지 못했음
					bTemp = MotorSensorCheck();
					switch(bTemp)
					{					
						case SENSOR_CLOSE_STATE:
						case SENSOR_CLOSECEN_STATE:
						case SENSOR_CLOSELOCK_STATE:
						case SENSOR_CLOSECENLOCK_STATE:
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
							BatteryCheck(); 
#else
							FeedbackMotorOpen();
#endif
							StartMotorOpen();
							gbCoverAccessDelaytime100ms = 10;
							gbModePrcsStep = 3;

							break;
			
						case SENSOR_OPEN_STATE:
						case SENSOR_OPENCEN_STATE:
						case SENSOR_OPENLOCK_STATE:
						case SENSOR_OPENCENLOCK_STATE:
						default : //case SENSOR_NOT_STATE, case SENSOR_LOCK_STATE, case SENSOR_CENTER_STATE,case SENSOR_CENTERLOCK_STATE
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
							BatteryCheck(); 
#else
							FeedbackMotorClose();
#endif
							StartMotorClose();
							gbCoverAccessDelaytime100ms = 10;
							gbModePrcsStep = 20;	
							break;	
						
					}
#else	//__DDL_MODEL_PANPAN_FC2A #else
				
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck(); 
#else
					FeedbackMotorOpen();
#endif
					StartMotorOpen();
					gbCoverAccessDelaytime100ms = 10;
					gbModePrcsStep = 3;
#endif	//__DDL_MODEL_PANPAN_FC2A #endif						
				}
				DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
				TamperCountClear();
				NFPMTxEventStop(0x00);
			}
			else
			{
				TamperCountIncrease();

				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				BioOffModeClear();
			}
			break;
#endif	// DDL_CFG_BLE_30_ENABLE

		default:
			gbModePrcsStep = 100;
			break;
	}
}




BYTE ConvertFingerprintSlotNumber(BYTE ConvertingData)
{
	return (ConvertingData);
}




