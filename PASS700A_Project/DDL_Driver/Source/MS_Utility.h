#ifndef	__MS_INCLUDED
#define	__MS_INCLUDED

/* 하기 UI display 와 상관 없는 spec 상 mask  */
#define MANAGER_CARD_REG_DEL_ALLOW 		(0x01)
#define MANAGER_CODE_REG_DEL_ALLOW 		(0x02)
#define MASTER_CARD_REG_DEL_ALLOW 			(0x04)
#define MASTER_CODE_REG_DEL_ALLOW			(0x08)
#define USER_CARD_REG_DEL_ALLOW 			(0x10)
#define USER_CODE_REG_DEL_ALLOW 			(0x20)
#define MENU_MODE_ALLOW 					(0x40)
#define MOTOR_OPEN_ALLOW		 			(0x80)
#define ALL_AUTHORITY 						(0xFF)

/* 하기 define 은 UI 때문에 bit 위치가 다르다 */
#define MANAGER_CARD_REG_DEL (1)
#define MASTER_CARD_REG_DEL (1 << 1)
#define USER_CARD_REG_DEL (1 << 2)
#define MANAGER_CODE_REG_DEL (1 << 3)
#define MASTER_CODE_REG_DEL (1 << 4)
#define USER_CODE_REG_DEL (1 << 5)
#define SETTING_MENU (1 << 6)

/* 인증 수단의 key 정보에 마지막 8byte 에 저장 되는 내용 */
#define CREDENTIAL_NUMBER_4		0x01
#define CREDENTIAL_NUMBER_5		0x02
#define CREDENTIAL_NUMBER_6		0x03
#define CREDENTIAL_NUMBER_7		0x04
#define CREDENTIAL_NUMBER_8		0x05
#define CREDENTIAL_NUMBER_9		0x06
#define CREDENTIAL_NUMBER_10	0x07

#define CREDENTIAL_TYPE_MANAGER_CARD	0x00
#define CREDENTIAL_TYPE_MASTER_CARD	0x10
#define CREDENTIAL_TYPE_USER_CARD		0x20
#define CREDENTIAL_TYPE_MANAGER_CODE	0x30
#define CREDENTIAL_TYPE_MASTER_CODE	0x40
#define CREDENTIAL_TYPE_USER_CODE		0x50


#define MANAGER_CARD_INDEX	0x00
#define MASTER_CARD_INDEX	0x01
#define USER_CARD_INDEX		0x02
#define MANAGER_CODE_INDEX	0x03
#define MASTER_CODE_INDEX	0x04
#define USER_CODE_INDEX		0x05 
#define SETTING_MENU_INDEX	0x06 
#define CREDENTIAL_INFO_SET	0x07 


#define SUM_OF_CARD		0xAA 
#define SUM_OF_CODE		0xBB 

#define CODE_LENGTH_OFFSET 0x03



#define MS_CREDENTIAL_SIZE (((MS_CREDENTIAL_DATA_100 -MS_CREDENTIAL_DATA_1) + 8))
#define MS_CREDENTIAL_NUM_SIZE (((MS_USER_CODE_NUM -MS_MANAGER_CARD_NUM) + 1))

/* MS 사양중에 특수 사양에 대한 처리 */
/* MS 일반 setting */
#define	MS_NO_SPECIAL_TYPE	0			

/* manger card 로 moter open 완료시 user card 삭제 user code 삭제 후 user code "0000" 으로 setting */
#define	MS_EXCLUSICE_TYPE_1	1			

/* Master card 를 이용해 user code 를 변경 할 경우(등록 / 삭제 둘다) user card 를 초기화  */
#define	MS_EXCLUSICE_TYPE_2	2

/* Master card 를 이용해  motor open 을 완료시 user card 전체 */
#define	MS_EXCLUSICE_TYPE_3	3

/* 사양 복잡함 미구현 상태 */
#define	MS_EXCLUSICE_TYPE_4	4

/* 사용 하지 않음 MS_NO_SPECIAL_TYPE 과 동일 */
#define	MS_NOT_USED				255

typedef struct
{
	//------------------------------------------------------------------------------
	/*! 	\def	MS_MANAGER_CARD_INFO	~		MS_USER_CODE_INFO
		8 byte * 1
		각 인증 수단의 정보 설정
		인증 수단의 개수가 0이거나 255일 경우에는 모두 0으로 처리한다.
		인증 수단의 개수가 0이거나 255일 경우에는 나머지 권한 설정 byte는 모두 무시된다.
		각 인증 수단의 개수 총합은 최대 지원 개수를 넘을 수 없다.
		해당 설정은 개발 단계에서 정의하여 이관하는 방법도 있고, 양산 단계에서 정의하여 출하하는 방법도 있음.
	
		|	Byte1	|	Byte2	Byte3	Byte4	Byte5	Byte6	Byte7	Byte8	|
		|인증 수단의 수 |						권한 설정								|
	
		Byte 1 : 인증 수단의 개수 (1~254)
	
		우선 순위가 더 높은 인증 수단을 등록 가능하도록 설정하더라도, 실제 처리되지 않는다. 예를 들면, User Card의 권한을 “Manager Card 등록 가능”으로 설정한다 하더라도, 실제 동작되지 않는다.
		권한 설정 2 ~ 권한 설정 7까지는 추후 정의 (권한 설정 Default : 0xFF)
		Byte 2 ~ Byte 8 : 권한 설정 1 ~ 7 (0~7 Bit)
		Bit 0 : Manager Card	등록 / 삭제 
		Bit 1 : Manager Code	등록 / 삭제 
		Bit 2 : Master Card 	등록 / 삭제 
		Bit 3 : Master Code 	등록 / 삭제 
		Bit 4 : User Card		등록 / 삭제 
		Bit 5 : User Code		등록 / 삭제 
		Bit 6 : Menu 진입 가능 
		Bit 7 : Motor Open 가능 

		아래 변수 순서 및 위치 변경 하지 말것 EEPROM 에서 read 시에 순차적으로 불리게 해놨음 
	*/
	BYTE bManager_Card_Info[8];
	BYTE bMaster_Card_Info[8];
	BYTE bUser_Card_Info[8];
	BYTE bManager_Code_Info[8];
	BYTE bMaster_Code_Info[8];
	BYTE bUser_Code_Info[8];
	//---------------------------------------------------------------------------	
	/* bCredential_Info_Set_Complete 는 
	Bit 0 : Manager_Card setting 유무 
	Bit 1 : Master_Card setting 유무 
	Bit 2 : User_Card setting 유무 
	Bit 3 : Manager_Code setting 유무 
	Bit 4 : Master_Code setting 유무 
	Bit 5 : User_Code setting 유무 
	Bit 6 : Reserved
	Bit 7 : Reserved
	bCredential_Info_Set_Complete 가 0x00 이면 MS 사양 setting 되지 않은 상태로 본다 

	*/
	BYTE bCredential_Info_Set_Complete;
	//---------------------------------------------------------------------------
	/*
	MS 사양에서는 인증 key 를 저장 할때 고정된 위치를 사용 할 수 없다
	상위 인증이 있는지 없는지 , 혹은 몇개나 있는지 알 수 없기에 
	wManager_Card_Start_Address 에 MS_CREDENTIAL_DATA_1 를 넣고 이 값을 기준으로 
	나머지 주소를 계산해서 쓴다.
	MS_CREDENTIAL_DATA_1 에는 지원 하는 인증 수단중 최상위 인증 수단의 첫번째 인증 key 값이 들어 간다
	
	ex) 
	#define	MS_MANAGER_CARD_INFO		0x3C0
	#define	MS_MASTER_CARD_INFO			0x3C8
	#define	MS_USER_CARD_INFO			0x3D0
	#define	MS_MANAGER_CODE_INFO		0x3D8
	#define	MS_MASTER_CODE_INFO			0x3E0
	#define	MS_USER_CODE_INFO			0x3E8

	MS_Get_Credential_Info() 함수에서 부팅 할때 주소를 계산 함수 참고 
	Card 정보를 낮은 주소에 배치 Pin 정보는 그 이후에 배치 
	
	Manager Card = MS_CREDENTIAL_DATA_1 
	Master Card
	User Card
	Manager Pin
	Master Pin
	User Pin

	인증 수단이 없으면 다음 인증 수단이 그 위치를 차지 
	*/
	WORD wCertification_Address[6];
	//---------------------------------------------------------------------------	
	/* main menu 에서 선택된 item */
	BYTE bSelected_Item;	

	/* Card 총 개수 */
	BYTE bSumofCard;

	/* Pin code 총 개수 */
	BYTE bSumofCode;	

	/* 인증 완료 한 Card 나 Code 의 credential type 정보 */
	BYTE bVerifiedType;

	/* 인증 완료 한 Card 나 Code 의 credential type 정보 */
	BYTE bExclusive;
}CredentialInfoTypeDef;


extern CredentialInfoTypeDef Ms_Credential_Info; 

void MS_All_Credential_Init(void);
void MS_Credential_Info_Init(void);
void MS_Exclusive_Functions_Init(void);
void MS_Get_Credential_Info(void);


void MS_ModeGotoCardCodeRegDelCheck(void);
void MS_CardCodeModeSetSelected(void);
void MS_MenuCardCodeModeSelectProcess(void);
void MS_ModeCardCodeRegDelCheck(void);
void MS_ModeGotoAuthorityVerify(void);
void MS_ModeAuthorityVerify(void);
void MS_ModeMenuCredentialInfoSet(void);

void MS_PinCodeDelete(void);
BYTE MS_Supported_Authority_Check(BYTE Selected);
BYTE MS_GetSupported_Authority(BYTE bVerifiedType , BYTE Authority_Mask);
BYTE MS_GetSupported_Credential_Number(BYTE Selected);
WORD MS_GetSupported_Credential_Start_Address(BYTE Selected);
void MS_Set_Selected_Item(BYTE SelectedItem);
void MS_Set_Registered_Credential_Num(BYTE Selected , BYTE data);
void MS_Get_Registered_Credential_Num(BYTE Selected , BYTE* data);
void MS_Set_Verified_Type(BYTE data);
BYTE MS_Get_Verified_Type(void);
BYTE MS_Get_Exclusive_Type(void);
void MS_Do_Exclusive_Function(void);
void MS_Add_Credential_Info(BYTE* address , BYTE Number);

#ifdef	DDL_CFG_DIMMER
WORD MS_Make_MainMenu_LED(void);
#endif 


#endif 

