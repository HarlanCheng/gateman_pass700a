
#define		_I2C_IOEXPANDER_FUNCTIONS_C_


#include	"main.h"

FLAG ExpanderPortVal;
FLAG ExpanderOutPortVal;


ddl_i2c_t		*h_expanderoutput_i2c_dev;
ddl_i2c_t		*h_expanderinput_i2c_dev;

//TCA6408A  Register Map Address
#define _TCS6408_ADDR_INPUT_PORT			0x00
#define _TCS6408_ADDR_OUTPUT_PORT			0x01
#define _TCS6408_ADDR_POLARITY_INVERSION	0x02
#define _TCS6408_ADDR_CONFIGURATION		0x03

BYTE	gbIoExpander_i2cbuff[4];
BYTE	gbIoExpander_i2c_Readbuff[1];
BYTE gbTouchKey_i2c_ReadWritebuff[4];


BYTE Test_value = 0;



void RegisterExpanderOutput( ddl_i2c_t *h_i2c_dev )
{
	h_expanderoutput_i2c_dev = h_i2c_dev;
}
void RegisterExpanderInput( ddl_i2c_t *h_i2c_dev )
{
	h_expanderinput_i2c_dev = h_i2c_dev;
}

void	IoExpanderOutPutInit( void )
{	
	P_I2C_SDA_IOEX(1);
	P_I2C_SCL_IOEX(1);
	P_IOEX_EN(1);
	Delay( SYS_TIMER_10MS );

	gbIoExpander_i2cbuff[ 0 ] = _TCS6408_ADDR_CONFIGURATION;
	gbIoExpander_i2cbuff[ 1 ] = 0x00;
	ddl_i2c_write( h_expanderoutput_i2c_dev, 0, 0, gbIoExpander_i2cbuff, 2 );		// write address	
	
}

void	IoExpanderInPutInit( void )
{
	P_I2C_SDA_IOEX(1);
	P_I2C_SCL_IOEX(1);
	//Delay( SYS_TIMER_10MS );

	gbTouchKey_i2c_ReadWritebuff[ 0 ] = _TCS6408_ADDR_CONFIGURATION;
	gbTouchKey_i2c_ReadWritebuff[ 1 ] = 0xFF;
	ddl_i2c_write( h_expanderinput_i2c_dev, 0, 0, gbTouchKey_i2c_ReadWritebuff, 2);		// write address	

	/*gbTouchKey_i2c_ReadWritebuff[ 0 ] = _TCS6408_ADDR_OUTPUT_PORT;
	gbTouchKey_i2c_ReadWritebuff[ 1 ] = 0x00;
	ddl_i2c_write( h_expanderinput_i2c_dev, 0, 0, gbTouchKey_i2c_ReadWritebuff, 2 );		// write address	*/
}


void IoExpanderOutPutControl(void)
{	
	P_IOEX_EN(1);
	
	gbIoExpander_i2cbuff[ 0 ] = _TCS6408_ADDR_OUTPUT_PORT;
	gbIoExpander_i2cbuff[ 1 ] = gbExpanderOutPortVal;
	ddl_i2c_write( h_expanderoutput_i2c_dev, 0, 0, gbIoExpander_i2cbuff, 2 );		// write address	
}

void IoExpanderInPutRead(void)
{
	gbTouchKey_i2c_ReadWritebuff[ 0 ] = _TCS6408_ADDR_INPUT_PORT;
	ddl_i2c_write( h_expanderinput_i2c_dev, 0, 0, gbTouchKey_i2c_ReadWritebuff, 1 );		// write address

	ddl_i2c_read( h_expanderinput_i2c_dev, 0, 0, &gbExpanderPortVal, 1 );			// read data
	
}


