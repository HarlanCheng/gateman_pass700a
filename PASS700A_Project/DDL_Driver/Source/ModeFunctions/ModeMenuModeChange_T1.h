//------------------------------------------------------------------------------
/** 	@file		ModeMenuModeChange_T1.c
	@brief	Normal/Advanced Mode Change
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUMODECHANGE_T1_INCLUDED
#define __MODEMENUMODECHANGE_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoChangeToAdvancedMode(void);
void ModeGotoChangeToNormalMode(void);
void ModeAdvancedModeSet(void);
void ModeNormalModeSet(void);


void LockWorkingModeLoad(void);
void LockWorkingModeDefaultSet(void);
void AdvancedModeSet(void);

void MasterCodeDelete(void);
void AllUserCodeDelete();




//	gbFactoryReset
#define	FACTORYRESET_REQ_BY_MANUAL		0x33
#define	FACTORYRESET_REQ_BY_RF			0x66
#define	FACTORYRESET_RUN_BY_MANUAL		0x97
#define	FACTORYRESET_RUN_BY_RF			0x5A
#define	FACTORYRESET_DONE_BY_MANUAL		0xAB
#define	FACTORYRESET_DONE_BY_RF			0xCA


void FactoryResetSet(BYTE bCode);
#ifdef P_SW_FACTORY_BROKEN_T
void ChangeFactoryPintoBrokenPinControl(BYTE OnOff);
#endif 
void FactoryResetCheck(void);
void FactoryResetRun(void);
BYTE FactoryResetProcessedCheck(void);
BYTE FactoryResetRunCheck(void); //hyojoon_20160922
void FactoryResetProcess(void);
void FactoryResetClear(void);

void FactoryResetDoneCheck(void);

#ifdef P_SW_FACTORY_BROKEN_T
void ModeWaitFactoryResetComplete(void);
#endif 
#endif


