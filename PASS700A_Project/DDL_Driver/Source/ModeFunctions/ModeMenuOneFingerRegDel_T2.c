#define		_MODE_MENU_ONEFINGER_REGDEL_C_

#include	"main.h"

#define _BIOSEC_OVER_WRITE_ (1)  /* 사용 중인 slot은 over write 하도록 수정 삭제 / 후 등록 요청 */


void		ModeGotoOneFingerRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGDEL;
	gbModePrcsStep = 0;
}


void		ModeGotoOneFingerRegister(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGISTER;
	gbModePrcsStep = 0;
}

void		ModeGotoOneFingerDelete(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_DELETE;
	gbModePrcsStep = 0;
}

void		OneFingerModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 지문 개별 등록
		case 1: 	
			ModeGotoOneFingerRegister();
			break;
	
		// 지문 개별 삭제
		case 3: 		
			ModeGotoOneFingerDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}

void		MenuOneFingerModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 지문 개별 등록
		case TENKEY_1:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_FINGER_SHARP, gbInputKeyValue);	//지문등록 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			break;

		// 지문 개별 삭제
		case TENKEY_3:		
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_FINGER_SHARP, gbInputKeyValue);	//지문삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			break;

		case TENKEY_SHARP:
			OneFingerModeSetSelected();
			break;
									
		case TENKEY_STAR:
			if(GetSupportCredentialCount() > 1)
				ModeGotoMenuAdvancedCredentialSelect();
			else 
				ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}

void		ModeOneFingerRegDelCheck(void )
{
	switch(gbModePrcsStep)
	{
		case 0:
			Feedback13MenuOn(AVML_REGFINGER1_DELFINGER3, VOL_CHECK);		//지문 등록은 1번, 지문 삭제는 3번을 누르세요.
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOneFingerModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}


//==============================================================================//
// Comment	: 지문 번호가 메모리에 존재하는지 검사
// Return 	: 0:없음 	1:존재 함.
//==============================================================================//
BYTE	FingerSlotVerify()
{
	BYTE	bTmp, bCnt;

	for(bCnt = 0; bCnt < _MAX_REG_BIO; bCnt++){
		RomRead( &bTmp, (WORD)BIO_ID+(2*bCnt), 1);

		if ( bTmp == gSelectedSlotNumber ) {		// EEPROM에 저장된 slot 정보와 비교
			gbFID = bCnt;
			return 1;
		}else{
			gbFID=0xff;
		}
	}

	return 0;	
}

void FingerSlotNumberToRegister(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();		//slot number는 1~20
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= _MAX_REG_BIO))
	{
		TenKeyVariablesClear();

		bTmp = FingerSlotVerify();		// 지문 번호 일치 하는 것 확인	..
		if(bTmp == 0){			// 지문 번호 일치 하는 것 없음
			// 해당 지문 번호 등록 시작.
			gbModePrcsStep++;
		}
		else
		{					// 지문 번호 일치 하는 것 존재함 ..
#if _BIOSEC_OVER_WRITE_  /* 사용 중인 slot은 over write 하도록 수정 삭제 / 후 등록 요청 */
						gbModePrcsStep = 30; 
#else 	
			FeedbackError(AVML_ALREADY_USE_SLOT, VOL_CHECK);		//이미 사용하고 있는 사용자번호입니다.
//			ModeClear();
			gbBioTimer10ms = 10;
			gbModePrcsStep = 30;		// reinput
#endif			
		}
	}
	else
	{
		gSelectedSlotNumber = 0xFF;
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}
}

void BackwardFunctionForOneFingerRegDel(void)
{
	ReInputOrModeMove(ModeGotoOneFingerRegDelCheck);
}

//------------------------------------------------------------------------------
/** 	@brief	One Finger Print Register Mode 
	@remark	User Code 등록 모드
	@see	MainModeProcess_T1.c
	@see	FingerPFM_3000.c
	@see	PFM_3000_UART.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.08.31		by Hyojoon
PFM-3000 / TSM1071M normal slot number 관리 
Enroll 시에 slot number 를 정해서 보내게 되면 Finger Module 에서는 전달 한 slotnumber 를 return 한다.  
ex) slot number 를 0x14 를 보내면 
ROM index 는 0 번 부터 시작 하므로 

gbFID = module return 값 - 1;  // ROM 의 index 로 사용 
FingerWriteBuff = gbFID;          // gbFID 받을 값을 gbFID-1 index 에 gbFID 로 저장 not BCD

개발자 마다 slot 관리가 틀려서 TCS4K 의 경우 slot number 를 BCD 로 저장 했는데 그럴 필요 없음 
신규 플렛 폼의 PFM-3000 및 앞으로 개발 할 TSM 의 경우 hex 값을 저장 아래 소스와 같이... 

ROM
--------------------------------
|               BIO_ID + gbFID                  |
--------------------------------
|FingerWriteBuff[0] | FingerWriteBuff[1] |
--------------------------------

*/
//------------------------------------------------------------------------------


void ModeOneFingerRegister(void)
{
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if ( no_fingers >= _MAX_REG_BIO ) {			// 이미 최대로 가득 차 있으면 개별 등록 거부.
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		// Error, 저장공간이 부족합니다.
				gbModePrcsStep=40;					// 개별 등록 삭제 메뉴로 가기
				break;
			}
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//개별 비밀번호 관리를 위하여 사용자 번호 두자리를 입력하세요, 계속하시려면 샵 버튼을 누르세요.
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gSelectedSlotNumber = 0;
			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(FingerSlotNumberToRegister, BackwardFunctionForOneFingerRegDel,1);
			break;

		case 2:
			AudioFeedback(AVML_ENTER_FINGER, gcbBuzSta, VOL_CHECK);			// 등록하실 지문을 차례로 입력 하세요.
			SetModeTimeOut(_MODE_TIME_OUT_7S);
			//gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			BioModuleON();
			LedModeRefresh();
			gbModePrcsStep++;
			break;


		case 3:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
			{				
				FingerPrintTxEncryptionKeyMakeFrame();
				SetModeTimeOut(_MODE_TIME_OUT_7S);
				gbModePrcsStep = 4;
			}
			else
			{
				FingerPrintRetry();
			}
			break;

		case 4:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[0] == 0xE2 &&	gbFingerPrint_rxd_buf[1] == 0xA1)
				{
					FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_SETSECURITY_LEVEL,SETSECURITY_LEVEL_VALUE_3,NO_FINGER_DATA);	//Enroll Command 송신 전 SETSECURITY_LEVEL을 3으로 먼저 설정 한다.
					gbModePrcsStep = 5;
					gbFingerRetryCounter = 0;
					SetModeTimeOut(_MODE_TIME_OUT_7S);
					//gbBioTimer10ms = 50;	//지문 등록 명령어 응답 대기 시간 100ms 설정 
					break;
				}
			}
			break;

		case 5: //SETSECURITY_LEVEL설정 확인 루틴
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == 0x86)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							SetModeTimeOut(_MODE_TIME_OUT_7S);
							//gbBioTimer10ms = BIO_WAKEUP_TM+750;
							gbModePrcsStep = 6;
							FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_ENROLL , gSelectedSlotNumber , FINGER_ALL_REGISTER);
							gbFingerPrint1TimeEnrollMode = 0;
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY						
						case 0x1F:	//명령 실패 						
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
							
						case 0x11:	//Slot이 비어 있지 않음
							BioModuleOff();
							AudioFeedback(AVML_ERR_NOSPACE,	gcbBuzSta, VOL_CHECK);//(음성)no more space to memorize.
							break;
					}
				}		
			}	
			break;
			
		case 6:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == 0x81)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							SetModeTimeOut(_MODE_TIME_OUT_20S);
							//gbBioTimer10ms = BIO_WAKEUP_TM+750;
							gbModePrcsStep = 7;
							gbFingerLongTimeWait100ms = 10;
							dbDisplayOn = 0x00;
							gbFingerRegisterDisplayStep = 0;
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY						
						case 0x1F:	//명령 실패 						
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
							
						case 0x11:	//Slot이 비어 있지 않음
							BioModuleOff();
							AudioFeedback(AVML_ERR_NOSPACE,	gcbBuzSta, VOL_CHECK);//(음성)no more space to memorize.
							break;
					}
				}		
			}			
			break;
			
		case 7: 	
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			switch(gbInputKeyValue){
				case FUNKEY_OPCLOSE:
				case FUNKEY_REG:
					gbModePrcsStep = 201;
					break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x87)
				{
					switch(gbFingerPrint_rxd_buf[5]&0x0F)
					{
						case 0x00:	//명령 성공 
							switch(gbFingerPrint_rxd_buf[5]&0xF0)
							{
								case 0x20:	//Command 수행중
								case 0x40:	//BUSY(지문 SCAN 대기중)
									switch(gbFingerPrint_rxd_buf[6]&0xF0)
									{
										case 0x10:	//등록
											SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
											switch(gbFingerPrint_rxd_buf[6]&0x0F)
											{
												case 0x01:	//1번째 지문 입력 완료		
													if(gbFingerPrint1TimeEnrollMode == 0)
													{
														AudioFeedback(AVML_REG_FINGER01_REIN, gcbBuzNum, VOL_CHECK);
														gbFingerRegisterDisplayStep = 1;
													}
													else
													{
														gbBioTimer10ms = 200;	//지문 등록 결과 Data 수신 대기 시간 1S 설정 
														gbModePrcsStep = 8;
													}
													break;
													
												case 0x02:	//2번째 지문 입력 완료
													AudioFeedback(AVML_REG_FINGER02_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 2;
													break;
													
												case 0x03:	//3번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER03_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 3;
													break;	
													
												case 0x04:	//4번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER04_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 4;
													break;	
													
												case 0x05:	//5번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER05_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 5;
													break;	
													
												case 0x06:	//6번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER06_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 6;
													break;	
													
												case 0x07:	//7번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER07_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 7;
													break;	
													
												case 0x08:	//8번째 지문 입력 완료	
													//gbFingerRegisterDisplayStep = 8;
													SetModeTimeOut(_MODE_TIME_OUT_7S);
													//gbBioTimer10ms = 200;	//지문 등록 결과 Data 수신 대기 시간 1S 설정 
													gbModePrcsStep = 8;
													break;	
													
												case 0x00:	//지문 입력 대기 않함
												default:
													BioModuleOff();
													FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); //미디 에러음 출력
													ModeClear();
													break;
											}
											break;
										case 0x00:	//명령 없음
										case 0x20:	//인증
										case 0x30:	//삭제
										case 0x40:	//지문 LIST 보고
										case 0x60:	//보안 레벨 변경 
										case 0x70:	//상태
										case 0x80:	//결과
										case 0x90:	//취소
										case 0xA0:	//Version
										default:
											gbModePrcsStep = 200;	 //Check the fingerprint module.
											break;
									}
									
									 break;
									 
								case 0x80:	//Idle 상태
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}							
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류							
						case 0x0F:	//명령 실패(원래 수신 되는 값은 0x1F이지만 하위 니블만 가공 하여 쓴다)
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
				else if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{								
								case 0x01:	//Enrollment 결과 성공
									BioModuleOff();
									FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID = gbFingerPrint_rxd_buf[7];
									gbFID--;//index 는 0번 부터 있으니 하나 제거 
									no_fingers++;					// 등록 수 증가..
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
									RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gSelectedSlotNumber);
#endif

									RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
									no_fingers = FingerReadBuff[0];

									memset(bTmpArray, 0x00, 8);
									CopyCredentialDataForPack(bTmpArray, 8);
									PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
			
									gbBioTimer10ms = BIO_WAKEUP_TM;
									if(no_fingers >= _MAX_REG_BIO){ 				// 20개 모두 등록시 등록 완료.
										AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
										LedSetting(gcbLedOk, LED_DISPLAY_OFF);
										BioModuleOff();
										ModeClear();
										break;
									}
									
									FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);	 //추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
									
									gbModePrcsStep = 9;
									SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
									break;
									
								case 0x81:	//Enrollment 결과 실패
									gbModePrcsStep = 100;
									break;
									
								case 0x00:	//결과 없음
								case 0x02:	//Verify 결과 성공
								case 0x03:	//Delete 결과 성공
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									BioModuleOff();
									FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
									break;
							}
							break;
							
						case 0x1E:	//BUSY 상태(현재 명령 무시) Cancer명령어를 송신 하고 다시 인증을 시도함 						
						case 0x1F:	//명령 실패
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						default:
							BioModuleOff();
							FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
							break;
					}
					break;
				}
			}
			FingerPrintRegisterDisplay();
			break;
		
		case 8: 
			if(GetModeTimeOut() == 0)
			{
				gbModePrcsStep=200;
				break;
			}
#if 0			
			if(gbBioTimer10ms == 0)
			{
				gbModePrcsStep = 200;	 //Check the fingerprint module.
				break;
			}
#endif 
			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{								
								case 0x01:	//Enrollment 결과 성공
									BioModuleOff();
									FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID = gbFingerPrint_rxd_buf[7];
									gbFID--;//index 는 0번 부터 있으니 하나 제거 
									no_fingers++;					// 등록 수 증가..
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
									RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gbFID);
#endif

									RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
									no_fingers = FingerReadBuff[0];

									memset(bTmpArray, 0x00, 8);
									CopyCredentialDataForPack(bTmpArray, 8);
									PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
			
									gbBioTimer10ms = BIO_WAKEUP_TM;
									if(no_fingers >= _MAX_REG_BIO){ 				// 20개 모두 등록시 등록 완료.
										AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
										LedSetting(gcbLedOk, LED_DISPLAY_OFF);
										BioModuleOff();
										ModeClear();
										break;
									}
									
									FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);	 //추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
									
									gbModePrcsStep = 9;
									SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
									break;
									
								case 0x81:	//Enrollment 결과 실패
									gbModePrcsStep = 100;
									break;
									
								case 0x00:	//결과 없음
								case 0x02:	//Verify 결과 성공
								case 0x03:	//Delete 결과 성공
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									BioModuleOff();
									FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
									break;
							}
							break;
							
						case 0x1E:	//BUSY 상태(현재 명령 무시) Cancer명령어를 송신 하고 다시 인증을 시도함 						
						case 0x1F:	//명령 실패
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						default:
							BioModuleOff();
							FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
							break;
					}
				}
#if 0				
				if(gbFingerPrint_rxd_buf[4] == 0x87)
				{
					switch(gbFingerPrint_rxd_buf[5]&0x0F)
					{
						case 0x00:	//명령 성공 
							switch(gbFingerPrint_rxd_buf[5]&0xF0)
							{
								case 0x20:	//Command 수행중
								case 0x40:	//BUSY(지문 SCAN 대기중)
									switch(gbFingerPrint_rxd_buf[6]&0xF0)
									{
										case 0x10:	//등록
											switch(gbFingerPrint_rxd_buf[6]&0x0F)
											{
												case 0x01:	//1번째 지문 입력 완료
												case 0x02:	//2번째 지문 입력 완료
												case 0x03:	//3번째 지문 입력 완료		
													gbModePrcsStep=200;
													break;	
													
												case 0x00:	//지문 입력 대기 않함
												case 0x08:	//Image 에러
												default:
													BioModuleOff();
													FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
													break;
											}
											break;
										case 0x00:	//명령 없음
										case 0x20:	//인증
										case 0x30:	//삭제
										case 0x40:	//지문 LIST 보고
										case 0x60:	//보안 레벨 변경 
										case 0x70:	//상태
										case 0x80:	//결과
										case 0x90:	//취소
										case 0xA0:	//Version
										default:
											gbModePrcsStep = 200;	 //Check the fingerprint module.
											break;
									}
									
									 break;
									 
								case 0x80:	//Idle 상태
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}							
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류							
						case 0x0F:	//명령 실패(원래 수신 되는 값은 0x1F이지만 하위 니블만 가공 하여 쓴다)
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
#endif 				
			}
			break;
	
		case 9:			// 등록 연속  or 종료...시..
			switch(gbInputKeyValue){
				case FUNKEY_REG:
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					BioOffModeClear();
					break;
//				case TENKEY_SHARP:
//					break;
				case TENKEY_STAR:
					// 추가 등록
					gbBioTimer10ms = 10;
					gbModePrcsStep = 0;		
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_HIGH);
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(gModeTimeOutTimer100ms	== 0){
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
				
				default:
					break;
			}
			break;

					//이미 등록된 slot에 등록하려 할 때 에러 내고, 재입력으로

#ifdef _BIOSEC_OVER_WRITE_
		case 30:
			BioModuleON();
			SetModeTimeOut(10);
			gbModePrcsStep = 31;
		break;

		case 31:
			if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
			{
				FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_DELETE , gSelectedSlotNumber , gSelectedSlotNumber);
				gbModePrcsStep = 32;
				gbFingerRetryCounter = 0;
			}
			else
			{
				FingerPrintRetry();
			}		
			break;

		case 32: 		
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK); 	//	times up
				ModeClear();
				break;
			}
		
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x83)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							gbModePrcsStep = 33;
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY 상태 					
						case 0x1F:	//명령 실패 						
						case 0x12:	//Slot에 Template이 없음(저장된 지문이 없음)
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}				
			}			
			break;

		case 33: 
					if(GetModeTimeOut() == 0)
					{
						BioModuleOff();
						FeedbackError(AVML_TIMEOUT, VOL_CHECK); 	//	times up
						ModeClear();
						break;
					}
					
					if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
					{		
						gbFingerPrint_rxd_end=0;
						if(gbFingerPrint_rxd_buf[4] == 0x88)
						{
							switch(gbFingerPrint_rxd_buf[5])
							{
								case 0x00:	//명령 성공
									switch(gbFingerPrint_rxd_buf[6])
									{
										case 0x03:	//Delete 결과 성공	
											no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
											no_fingers--;
											RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
											FingerWriteBuff[0]=0xff;
											FingerWriteBuff[1]=0xff;
											gbFID = gSelectedSlotNumber;
											gbFID--;
		
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
											ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gSelectedSlotNumber);
#endif
											RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	
		
											DDLStatusFlagClear(DDL_STS_FAIL_BIO);
											SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
											gbModePrcsStep = 2;
		
											// 지문 삭제 정보 전송
											// gbModePrcsStep = 6 에서 정보 전송 
											//PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
											// 지문 삭제 정보 전송								
											
											break;
											
										case 0x00:	//결과 없음
										case 0x01:	//Enrollment 결과 성공
										case 0x02:	//Verify 결과 성공
										case 0x81:	//Enrollment 결과 실패
										case 0x82:	//Verify 결과 실패
										case 0x83:	//Delete 결과 실패
										default:
											gbModePrcsStep = 200;	 //Check the fingerprint module.
											break;
									}
									break;
									
								case 0x01:	//전송 패킷 오류
								case 0x02:	//Checksum 오류
								case 0x1E:	//BUSY 상태
								case 0x1F:	//명령 실패
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}
						}
					}
					break;

		

#else
		case 30:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
			ModeGotoOneFingerRegister();
			break;
#endif			

		case 40:			//지문 등록이 최대로 된상태에서 개별 등록을 하려하면 에러 내고, 등록/삭제 메뉴로...
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
			ModeGotoOneFingerRegDelCheck();
			break;

		case 100:
			FeedbackError(AVML_ERR_FINGER_MSG1, VOL_CHECK); 	//The Fingerprint is not registered.
			BioOffModeClear();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;

		default:
			BioOffModeClear();
			break;
	}
}



void FingerSlotNumberToDelete(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();		//slot number는 1~20
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= _MAX_REG_BIO))
	{
		TenKeyVariablesClear();

		bTmp = FingerSlotVerify();		// 지문 번호 일치 하는 것 확인	..
		if(bTmp == 0){			// 지문 번호 일치 하는 것 없음
			//지우는 시늉만 한다.
			gbBioTimer10ms = 10;
			gbModePrcsStep = 6;		// reinput
		}
		else {					// 지문 번호 일치 하는 것 존재함 ..
			// 해당 번호 지문 삭제 시작.
			gbModePrcsStep++;
		}

		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);	//# 버튼에 대한 효과음
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}
}



void ModeOneFingerDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_FOR_DEL_SHARP, VOL_CHECK);			//삭제를 원하시는 사용자 번호를 입력하세요. 계속하시려면 샵 버튼을 누르세요.
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gSelectedSlotNumber = 0;
			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(FingerSlotNumberToDelete, BackwardFunctionForOneFingerRegDel,0);
			break;

		case 2:
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR
			BioModuleON();
			LedModeRefresh();
			gbModePrcsStep++;
			break;

		case 3:
			if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
			{
				FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_DELETE , gSelectedSlotNumber , gSelectedSlotNumber);
				gbModePrcsStep = 4;
				gbFingerRetryCounter = 0;
			}
			else
			{
				FingerPrintRetry();
			}		
			break;

		case 4: 		
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x83)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							gbModePrcsStep = 5;
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY 상태 					
						case 0x1F:	//명령 실패 						
						case 0x12:	//Slot에 Template이 없음(저장된 지문이 없음)
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}				
			}			
			break;

		case 5: 
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{
								case 0x03:	//Delete 결과 성공	
									no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
									no_fingers--;
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
									FingerWriteBuff[0]=0xff;
									FingerWriteBuff[1]=0xff;
									gbFID = gSelectedSlotNumber;
									gbFID--;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gSelectedSlotNumber);
#endif
									RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	

									DDLStatusFlagClear(DDL_STS_FAIL_BIO);
									SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
									gbModePrcsStep = 6;

									// 지문 삭제 정보 전송
									// gbModePrcsStep = 6 에서 정보 전송 
									//PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
									// 지문 삭제 정보 전송								
									
									break;
									
								case 0x00:	//결과 없음
								case 0x01:	//Enrollment 결과 성공
								case 0x02:	//Verify 결과 성공
								case 0x81:	//Enrollment 결과 실패
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}
							break;
							
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						case 0x1E:	//BUSY 상태
						case 0x1F:	//명령 실패
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
			}
			break;

		case 6:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;			//버튼음 마침을 기다림
			BioModuleOff();
			PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R2, VOL_CHECK);		//완료되었습니다. 다른 설정을 원하시면 별표를 누르시고, 종료를 원하시면 R버튼을 누르세요.
			gbModePrcsStep++;
			break;

		case 7:			// 등록 연속  or 종료...시..
			switch(gbInputKeyValue){
				case FUNKEY_REG:
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					BioOffModeClear();
					break;
				case TENKEY_STAR:
					// 추가 삭제
					gbBioTimer10ms = 10;
					gbModePrcsStep = 0; 	
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_CHECK);
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(gModeTimeOutTimer100ms	== 0){
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
				
				default:
					break;
			}
			break;

		case 30:			//이미 등록된 slot에 등록하려 할 때 에러 내고, 재입력으로
			//if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
			ModeGotoOneFingerDelete();
			break;


		case 40:			//등록된 지문이 없으면 에러 내고, 등록/삭제 메뉴로...
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ModeGotoOneFingerRegDelCheck();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;


		default:
			BioOffModeClear();
			break;
	}

}



#ifdef	BLE_N_SUPPORT
void ModeGotoOneFingerRegisterByBleN(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGISTER_BYBLEN;
	gbModePrcsStep = 0;
}


void ModeGotoOneFingerDeleteByBleN(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_DELETE_BYBLEN;
	gbModePrcsStep = 0;
}


void ModeOneFingerRegisterByBleN(void)
{
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if ( no_fingers >= _MAX_REG_BIO ) {			// 이미 최대로 가득 차 있으면 개별 등록 거부.
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		// Error, 저장공간이 부족합니다.
				gbModePrcsStep=40;					// 개별 등록 삭제 메뉴로 가기
				break;
			}

			gbModePrcsStep++;
			break;
			
		case 1:
			gPinInputKeyCnt = 2;
			FingerSlotNumberToRegister();
			break;

		case 2:
			AudioFeedback(AVML_ENTER_FINGER, gcbBuzSta, VOL_CHECK);			// 등록하실 지문을 차례로 입력 하세요.
//			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			BioModuleON();
			LedModeRefresh();
			gbModePrcsStep++;
			break;
		
		case 3:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
			{				
				FingerPrintTxEncryptionKeyMakeFrame();
				gbModePrcsStep = 4;
			}
			else
			{
				FingerPrintRetry();
			}
			break;

		case 4:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
#ifdef	BLE_N_SUPPORT
				PackTx_MakeAlarmPacket(REGISTERING_CREDENTIAL_MODE_ERROR, 0x00, 0x00);
#endif 
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[0] == 0xE2 &&	gbFingerPrint_rxd_buf[1] == 0xA1)
				{
					FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_SETSECURITY_LEVEL,SETSECURITY_LEVEL_VALUE_3,NO_FINGER_DATA);	//Enroll Command 송신 전 SETSECURITY_LEVEL을 3으로 먼저 설정 한다.
					gbModePrcsStep = 5;
					gbFingerRetryCounter = 0;
//					gbBioTimer10ms = 50;	//지문 등록 명령어 응답 대기 시간 100ms 설정 
					break;
				}
			}
			break;

		case 5: 
			if(GetModeTimeOut() == 0)
			{
#ifdef	BLE_N_SUPPORT
				PackTx_MakeAlarmPacket(REGISTERING_CREDENTIAL_MODE_ERROR, 0x00, 0x00);
#endif 
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == 0x86)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
//							gbBioTimer10ms = BIO_WAKEUP_TM+750;
							gbModePrcsStep = 6;
							FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_ENROLL , gSelectedSlotNumber , FINGER_ALL_REGISTER);
							gbFingerPrint1TimeEnrollMode = 0;
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY						
						case 0x1F:	//명령 실패 						
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
							
						case 0x11:	//Slot이 비어 있지 않음
							BioModuleOff();
							AudioFeedback(AVML_ERR_NOSPACE,	gcbBuzSta, VOL_CHECK);//(음성)no more space to memorize.
							break;
					}
				}		
			}	
			break;
			
		case 6:
			if(GetModeTimeOut() == 0)
			{
#ifdef	BLE_N_SUPPORT
				PackTx_MakeAlarmPacket(REGISTERING_CREDENTIAL_MODE_ERROR, 0x00, 0x00);
#endif 
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == 0x81)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
//							gbBioTimer10ms = BIO_WAKEUP_TM+750;
							SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
							gbModePrcsStep = 7;
							gbFingerLongTimeWait100ms = 10;
							dbDisplayOn = 0x00;
							gbFingerRegisterDisplayStep = 0;
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY						
						case 0x1F:	//명령 실패 						
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
							
						case 0x11:	//Slot이 비어 있지 않음
							BioModuleOff();
							AudioFeedback(AVML_ERR_NOSPACE,	gcbBuzSta, VOL_CHECK);//(음성)no more space to memorize.
							break;
					}
				}		
			}			
			break;
			
		case 7: 	
			if(GetModeTimeOut() == 0)
			{
#ifdef	BLE_N_SUPPORT
				PackTx_MakeAlarmPacket(REGISTERING_CREDENTIAL_MODE_ERROR, 0x00, 0x00);
#endif 
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			switch(gbInputKeyValue){
				case FUNKEY_OPCLOSE:
				case FUNKEY_REG:
					gbModePrcsStep = 201;
					break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x87)
				{
					switch(gbFingerPrint_rxd_buf[5]&0x0F)
					{
						case 0x00:	//명령 성공 
							switch(gbFingerPrint_rxd_buf[5]&0xF0)
							{
								case 0x20:	//Command 수행중
								case 0x40:	//BUSY(지문 SCAN 대기중)
									switch(gbFingerPrint_rxd_buf[6]&0xF0)
									{
										case 0x10:	//등록
											SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
											switch(gbFingerPrint_rxd_buf[6]&0x0F)
											{
												case 0x01:	//1번째 지문 입력 완료		
													AudioFeedback(AVML_REG_FINGER01_REIN, gcbBuzNum, VOL_CHECK);
													gbFingerRegisterDisplayStep = 1;
													break;
													
												case 0x02:	//2번째 지문 입력 완료
													AudioFeedback(AVML_REG_FINGER02_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 2;
													break;
													
												case 0x03:	//3번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER03_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 3;
													break;	
													
												case 0x04:	//4번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER04_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 4;
													break;	
													
												case 0x05:	//5번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER05_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 5;
													break;	
													
												case 0x06:	//6번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER06_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 6;
													break;	
													
												case 0x07:	//7번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER07_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 7;
													break;	
													
												case 0x08:	//8번째 지문 입력 완료	
													//gbFingerRegisterDisplayStep = 8;
													gbBioTimer10ms = 200;	//지문 등록 결과 Data 수신 대기 시간 1S 설정 
													gbModePrcsStep = 8;
													break;	
													
												case 0x00:	//지문 입력 대기 않함
												default:
													BioModuleOff();
													FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); //미디 에러음 출력
													ModeClear();
													break;
											}
											break;
										case 0x00:	//명령 없음
										case 0x20:	//인증
										case 0x30:	//삭제
										case 0x40:	//지문 LIST 보고
										case 0x60:	//보안 레벨 변경 
										case 0x70:	//상태
										case 0x80:	//결과
										case 0x90:	//취소
										case 0xA0:	//Version
										default:
											gbModePrcsStep = 200;	 //Check the fingerprint module.
											break;
									}
									
									 break;
									 
								case 0x80:	//Idle 상태
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}							
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류							
						case 0x0F:	//명령 실패(원래 수신 되는 값은 0x1F이지만 하위 니블만 가공 하여 쓴다)
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
				else if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{								
								case 0x01:	//Enrollment 결과 성공
									BioModuleOff();
									FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID = gbFingerPrint_rxd_buf[7];
									gbFID--;//index 는 0번 부터 있으니 하나 제거 
									no_fingers++;					// 등록 수 증가..
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
									RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gSelectedSlotNumber);
#endif
									RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
									no_fingers = FingerReadBuff[0];

									memset(bTmpArray, 0x00, 8);
									CopyCredentialDataForPack(bTmpArray, 8);
									PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
			
									gbBioTimer10ms = BIO_WAKEUP_TM;
									if(no_fingers >= _MAX_REG_BIO){ 				// 20개 모두 등록시 등록 완료.
										AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
										LedSetting(gcbLedOk, LED_DISPLAY_OFF);
										BioModuleOff();
										ModeClear();
										break;
									}
									
									FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);	 //추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
									
									gbModePrcsStep = 9;
									SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
									break;
									
								case 0x81:	//Enrollment 결과 실패
									gbModePrcsStep = 100;
									break;
									
								case 0x00:	//결과 없음
								case 0x02:	//Verify 결과 성공
								case 0x03:	//Delete 결과 성공
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									BioModuleOff();
									FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
									break;
							}
							break;
							
						case 0x1E:	//BUSY 상태(현재 명령 무시) Cancer명령어를 송신 하고 다시 인증을 시도함 						
						case 0x1F:	//명령 실패
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						default:
							BioModuleOff();
							FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
							break;
					}
					break;
				}
			}
			FingerPrintRegisterDisplay();
			break;
		
		case 8: 
			if(GetModeTimeOut() == 0)
			{
				gbModePrcsStep=200;
				break;
			}
			
			if(gbBioTimer10ms == 0)
			{
				gbModePrcsStep = 200;	 //Check the fingerprint module.
				break;
			}

			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{								
								case 0x01:	//Enrollment 결과 성공
									BioModuleOff();
									FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID = gbFingerPrint_rxd_buf[7];
									gbFID--;//index 는 0번 부터 있으니 하나 제거 
									no_fingers++;					// 등록 수 증가..
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
									RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gbFID);
#endif
									RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
									no_fingers = FingerReadBuff[0];

									AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK); // Full 20 fingerprints have been registered
									LedSetting(gcbLedOk, LED_DISPLAY_OFF);
									BioModuleOff();
									ModeClear();

									memset(bTmpArray, 0x00, 8);
									CopyCredentialDataForPack(bTmpArray, 8);
									PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
									break;
									
								case 0x81:	//Enrollment 결과 실패
									gbModePrcsStep = 100;
									break;
									
								case 0x00:	//결과 없음
								case 0x02:	//Verify 결과 성공
								case 0x03:	//Delete 결과 성공
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									BioModuleOff();
									FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
									break;
							}
							break;
							
						case 0x1E:	//BUSY 상태(현재 명령 무시) Cancer명령어를 송신 하고 다시 인증을 시도함 						
						case 0x1F:	//명령 실패
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						default:
							BioModuleOff();
							FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
							break;
					}
				}
#if 0				
				if(gbFingerPrint_rxd_buf[4] == 0x87)
				{
					switch(gbFingerPrint_rxd_buf[5]&0x0F)
					{
						case 0x00:	//명령 성공 
							switch(gbFingerPrint_rxd_buf[5]&0xF0)
							{
								case 0x20:	//Command 수행중
								case 0x40:	//BUSY(지문 SCAN 대기중)
									switch(gbFingerPrint_rxd_buf[6]&0xF0)
									{
										case 0x10:	//등록
											switch(gbFingerPrint_rxd_buf[6]&0x0F)
											{
												case 0x01:	//1번째 지문 입력 완료
												case 0x02:	//2번째 지문 입력 완료
												case 0x03:	//3번째 지문 입력 완료		
													gbModePrcsStep=200;
													break;	
													
												case 0x00:	//지문 입력 대기 않함
												case 0x08:	//Image 에러
												default:
													BioModuleOff();
													FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
													break;
											}
											break;
										case 0x00:	//명령 없음
										case 0x20:	//인증
										case 0x30:	//삭제
										case 0x40:	//지문 LIST 보고
										case 0x60:	//보안 레벨 변경 
										case 0x70:	//상태
										case 0x80:	//결과
										case 0x90:	//취소
										case 0xA0:	//Version
										default:
											gbModePrcsStep = 200;	 //Check the fingerprint module.
											break;
									}
									
									 break;
									 
								case 0x80:	//Idle 상태
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}							
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류							
						case 0x0F:	//명령 실패(원래 수신 되는 값은 0x1F이지만 하위 니블만 가공 하여 쓴다)
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
#endif 				
			}
			break;
			
#ifdef _BIOSEC_OVER_WRITE_
case 30:
	BioModuleON();
	SetModeTimeOut(10);
	gbModePrcsStep = 31;
break;

case 31:
	if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
	{
		FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_DELETE , gSelectedSlotNumber , gSelectedSlotNumber);
		gbModePrcsStep = 32;
		gbFingerRetryCounter = 0;
	}
	else
	{
		FingerPrintRetry();
	}		
	break;

case 32:		
	if(GetModeTimeOut() == 0)
	{
		BioModuleOff();
#ifdef	BLE_N_SUPPORT
		PackTx_MakeAlarmPacket(REGISTERING_CREDENTIAL_MODE_ERROR, 0x00, 0x00);
#endif 
		FeedbackError(AVML_TIMEOUT, VOL_CHECK); 	//	times up
		ModeClear();
		break;
	}

	if(gbFingerPrint_rxd_end)
	{
		gbFingerPrint_rxd_end = 0;
		if(gbFingerPrint_rxd_buf[4] == 0x83)
		{
			switch(gbFingerPrint_rxd_buf[5])
			{
				case 0x00:	//명령 성공 
					gbModePrcsStep = 33;
					break;
				
				case 0x01:	//전송 패킷 오류
				case 0x02:	//Check sum 오류	
				case 0x1E:	//BUSY 상태 					
				case 0x1F:	//명령 실패 						
				case 0x12:	//Slot에 Template이 없음(저장된 지문이 없음)
					gbModePrcsStep = 200;	 //Check the fingerprint module.
					break;
			}
		}				
	}			
	break;

case 33: 
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
#ifdef	BLE_N_SUPPORT
				PackTx_MakeAlarmPacket(REGISTERING_CREDENTIAL_MODE_ERROR, 0x00, 0x00);
#endif 
				FeedbackError(AVML_TIMEOUT, VOL_CHECK); 	//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{
								case 0x03:	//Delete 결과 성공	
									no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
									no_fingers--;
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
									FingerWriteBuff[0]=0xff;
									FingerWriteBuff[1]=0xff;
									gbFID = gSelectedSlotNumber;
									gbFID--;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gSelectedSlotNumber);
#endif
									RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	

									DDLStatusFlagClear(DDL_STS_FAIL_BIO);
									SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
									gbModePrcsStep = 2;

									// 지문 삭제 정보 전송
									// gbModePrcsStep = 6 에서 정보 전송 
									//PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
									// 지문 삭제 정보 전송								
									
									break;
									
								case 0x00:	//결과 없음
								case 0x01:	//Enrollment 결과 성공
								case 0x02:	//Verify 결과 성공
								case 0x81:	//Enrollment 결과 실패
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}
							break;
							
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						case 0x1E:	//BUSY 상태
						case 0x1F:	//명령 실패
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
			}
			break;
#endif			

		case 100:
			FeedbackError(AVML_ERR_FINGER_MSG1, VOL_CHECK); 	//The Fingerprint is not registered.
			BioOffModeClear();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;

		default:
			BioOffModeClear();
			break;
	}
}


void ModeOneFingerDeleteByBleN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			gbModePrcsStep++;
			break;
			
		case 1:
			gPinInputKeyCnt = 2;
			gSelectedSlotNumber = ConvertInputKeyToByte();
			gbModePrcsStep++;
			break;

		case 2:
			BioModuleON();
			LedModeRefresh();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gbModePrcsStep++;
			break;
		
		case 3:
			if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
			{
				FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_DELETE , gSelectedSlotNumber , gSelectedSlotNumber);
				gbModePrcsStep = 4;
				gbFingerRetryCounter = 0;
			}
			else
			{
				FingerPrintRetry();
			}		
			break;

		case 4: 		
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x83)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							gbModePrcsStep = 5;
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY 상태 					
						case 0x1F:	//명령 실패 						
						case 0x12:	//Slot에 Template이 없음(저장된 지문이 없음)
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}				
			}			
			break;

		case 5: 
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{
								case 0x03:	//Delete 결과 성공	
									BioModuleOff();
									no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
									no_fingers--;
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
									FingerWriteBuff[0]=0xff;
									FingerWriteBuff[1]=0xff;
									gbFID = gSelectedSlotNumber;
									gbFID--;
									AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK); 

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gSelectedSlotNumber);
#endif

									RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	

									DDLStatusFlagClear(DDL_STS_FAIL_BIO);
									SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
									LedSetting(gcbLedOk, LED_DISPLAY_OFF);
									BioModuleOff();
									// 지문 삭제 정보 전송
									PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
									// 지문 삭제 정보 전송								
									ModeClear();
									break;
									
								case 0x00:	//결과 없음
								case 0x01:	//Enrollment 결과 성공
								case 0x02:	//Verify 결과 성공
								case 0x81:	//Enrollment 결과 실패
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}
							break;
							
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						case 0x1E:	//BUSY 상태
						case 0x1F:	//명령 실패
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
			}
			break;
#if 0
		case 6:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;			//버튼음 마침을 기다림

			AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK); 	// Full 20 fingerprints have been registered
			LedSetting(gcbLedOk, LED_DISPLAY_OFF);
			BioModuleOff();
			ModeClear();
			break;
#endif 

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;


		default:
			BioOffModeClear();
			break;
	}

}
#endif




