//------------------------------------------------------------------------------
/** 	@file	TCS4K_UART.h
	@brief	UART functions for TCS4K communication
*/
//------------------------------------------------------------------------------


#ifndef		_TCS4K_UART_H_
#define		_TCS4K_UART_H_

#include "DefineMacro.h"
//#include "DefinePin.h"


#define		MCU_TCS4K_UART		USART3

void		SetupTCS4k_UART( void );
void 	EnableTCS4KTxAsGpio(void);
void 	OffGpioTCS4K(void);
void		RegisterTCS4K_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) );
void		UartSendTCS4K( BYTE data );

#endif

