#ifndef		_MODE_PACK_SET_LED_PROCESS_H_
#define		_MODE_PACK_SET_LED_PROCESS_H_


void PackSetLedMode(BYTE * data);
void PackGetLedMode(BYTE * data);
void 	PackLedControlCounter(void);
void PackLedBuzzercontrol(BYTE OnOff);
void	ModeGotoPackSetLedProcess( void );
void	ModePackSetLedProcess( void );


#endif


