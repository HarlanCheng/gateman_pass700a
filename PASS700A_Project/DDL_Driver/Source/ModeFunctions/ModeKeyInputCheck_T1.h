//------------------------------------------------------------------------------
/** 	@file		ModeKeyInputCheck_T1.h
	@brief	Key Input Wait Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEKEYINPUTCHECK_T1_INCLUDED
#define __MODEKEYINPUTCHECK_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeKeyInputCheck(void);


void KeyTenKeyWakeUpProcess(void);
void ModeGotoRegisterCheck(void);
void ModeRegisterCheck(void);

#ifdef DDL_AAAU_AUTOLOCK_HOLD //호주 및 뉴질랜드 전용 autolock hold 기능
void ModeOperationSelect(void);
#endif	//DDL_AAAU_AUTOLOCK_HOLD #endif


#endif


