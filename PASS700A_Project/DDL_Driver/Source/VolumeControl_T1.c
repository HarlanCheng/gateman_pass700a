
//------------------------------------------------------------------------------
/** 	@file		VolumeControl_T1.c
	@date	2016.03.31
	@brief	Programmable Doorlock Volume Control Functions
	@see	Buzzer.c
	@see	VoiceICFunctions-abov.c
	@see	SerialEeprom_T1.h
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.03.31		by Jay
			- 메뉴에 의해 설정되는 볼륨 처리
			- 하드웨어에 의한 볼륨 설정 내용과 동일한 함수 구조 사용
*/
//------------------------------------------------------------------------------
/*
	Reset 후 저장된 Volume 설정 값 Load 시 : VolumeSettingLoad 함수 수행
	Volume 설정 시 : VolumeSetting 함수 수행
	현재 Volume 정보 확인 시 : VolumeSettingCheck 함수 사용
*/

#include "Main.h"


//=============================================================
// Volume Control
//=============================================================

BYTE gbVolumeMode = 0;		/**< 현재의 볼륨 정보 */


#ifdef	DDL_CFG_BUZ_VOL_TYPE1

//------------------------------------------------------------------------------
/** 	@brief	Volume Value Load
	@param 	Output : gbVolumeMode
	@return 	없음
	@remark	설정된 볼륨 값을 변수로 읽어들인다.
	@remark	Default Value : VOL_MID - 중간음
*/
//------------------------------------------------------------------------------
void VolumeSettingLoad(void)
{
	RomRead(&gbVolumeMode, (WORD)VOICE_STATE, 1);
}

//------------------------------------------------------------------------------
/** 	@brief	Volume Value Set
	@param 	Input : bVal
	@return 	없음
	@remark	입력된 bVal 값을 VOICE_STATE에 저장한다.
*/
//------------------------------------------------------------------------------
void VolumeSetting(BYTE bVal)
{
	gbVolumeMode = bVal;
	RomWrite(&gbVolumeMode, (WORD)VOICE_STATE, 1);
}


//------------------------------------------------------------------------------
/** 	@brief	Load Volume Value
	@param 	Output : gbVolumeMode
	@return 	gbVolumeMode
	@remark	현재 설정된 Volume을 출력한다.
*/
//------------------------------------------------------------------------------
BYTE VolumeSettingCheck(void)
{
	return (gbVolumeMode);
}


#endif		//DDL_CFG_BUZ_VOL_TYPE1




#ifdef	DDL_CFG_BUZ_VOL_TYPE2


#define	ADCVAL_VOLUME_SILENT	3070	
#define	ADCVAL_VOLUME_LOW		1021


//#define	ADCVAL_VOLUME_SILENT	3000	
//#define	ADCVAL_VOLUME_LOW		1500


//------------------------------------------------------------------------------
/** 	@brief	Volume Value Load
	@param 	Output : gbVolumeMode
	@return 	없음
	@remark	설정된 볼륨 값을 변수로 읽어들인다.
	@remark	Default Value : VOL_MID - 중간음
*/
//------------------------------------------------------------------------------
void VolumeSettingLoad(void)
{

}

//------------------------------------------------------------------------------
/** 	@brief	Volume Value Set
	@param 	Input : bVal
	@return 	없음
	@remark	입력된 bVal 값을 VOICE_STATE에 저장한다.
*/
//------------------------------------------------------------------------------
void VolumeSetting(BYTE bVal)
{

}


//------------------------------------------------------------------------------
/** 	@brief	Load Volume Value
	@param 	Output : gbVolumeMode
	@return 	gbVolumeMode
	@remark	현재 설정된 Volume을 출력한다.
*/
//------------------------------------------------------------------------------
BYTE VolumeSettingCheck(void)
{
	WORD AdcValue;

	P_VOL_OUT(1);		
	Delay(SYS_TIMER_100US);

	AdcValue = ADCCheck(ADC_VOLUME_CHECK);	

	P_VOL_OUT(0);

	if(AdcValue > ADCVAL_VOLUME_SILENT)
	{
		gbVolumeMode = VOL_SILENT;
	}
	else if(AdcValue > ADCVAL_VOLUME_LOW)
	{
		gbVolumeMode = VOL_LOW;
	}
	else
	{
		gbVolumeMode = VOL_HIGH;
	}	

	return (gbVolumeMode);
}

#endif		//DDL_CFG_BUZ_VOL_TYPE2


