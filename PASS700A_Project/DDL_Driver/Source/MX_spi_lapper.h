#ifndef		_MX_SPI_LAPPER_H_
#define		_MX_SPI_LAPPER_H_


typedef	struct {
	uint8_t		flag;		// flag : reserved

	union {
		struct {
			SPI_HandleTypeDef	*hspi;
		} mcu;
	} dev;
}	ddl_spi_t;


#define	MCU_SPI_TIMEOUT		10


#ifdef		_MX_SPI_LAPPER_C_
#else
#endif

void	ddl_spi_write( ddl_spi_t *ddl_spi, uint8_t *data, uint16_t length );
void	ddl_spi_read( ddl_spi_t *ddl_spi, uint8_t *data, uint16_t length );


#endif
