//------------------------------------------------------------------------------
/** 	@file		BatteryCheckFunctions_T1.c
	@date	2016.05.03
	@brief	Battery Check with ADC
	@see	ADCFunctions.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.03		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


WORD gBatteryCheckTimer1s = 0;
WORD gBatteryCheckAdcValue = 0;


//#define	ADCVAL_BATTERY_LOW		1469				// 1.184V, 배터리 전압 4.5V
#ifdef STD_GA_374_2019
#define	ADCVAL_BATTERY_LOW_4P8		1567				// 배터리 전압 4.8 80% 
#define	ADCVAL_BATTERY_LOW_4P3		1403				// 배터리 전압 4.3

#else 
#define	ADCVAL_BATTERY_LOW		1403				// 배터리 전압 4.3V
#endif
//#define	ADCVAL_BATTERY_LOW		1300		// 약 3.9V

//지문 모듈이 들어간 장비는 4.2V, 그외는 3.9V 기준으로 측정한 값
//#define	ADCVAL_LOWBATT			1350		// 4.2V
//#define	ADCVAL_LOWBATT			1255		// 3.9V



// 주기적으로 배터리를 확인할 필요가 없어 시간 Count하는 부분은 삭제, 추후 사용 가능성 때문에 주석 처리
/*
void BatteryCheckTimeCount(void)
{
	if(gBatteryCheckTimer1s)		--gBatteryCheckTimer1s;
}

void BatteryCheckStart(void)
{
	// 외부에서 Interrupt로 깨어날 경우 바로 확인하기 위해
	gBatteryCheckTimer1s = 0;
}
*/


WORD GetBatteryAdcValue(void)
{
	return (gBatteryCheckAdcValue);
}



void BatteryCheck(void)
{
#ifdef STD_GA_374_2019
	uint32_t lowbatteryLevel;	
#endif 
	// 주기적으로 배터리를 확인할 필요가 없어 시간 Count하는 부분은 삭제, 외부에서 호출할 경우에만 확인
	// 현재는 Motor Open/Close 할 경우에만 확인
//	if(gBatteryCheckTimer1s)	return;
//	gBatteryCheckTimer1s = 600;

	LedForcedAllOff();
	Delay(SYS_TIMER_2MS);

	P_LOWBAT_EN(1);
	Delay(SYS_TIMER_100US);

	gBatteryCheckAdcValue = ADCCheck(ADC_BATTERY_CHECK);	

	P_LOWBAT_EN(0);

	gfLowBattery = 0;

#ifdef STD_GA_374_2019
	if(gwLanguageMode == _CHINESE_MODE)
	{
		lowbatteryLevel = ADCVAL_BATTERY_LOW_4P8;
	}
	else
	{
		lowbatteryLevel = ADCVAL_BATTERY_LOW_4P3;
	}
	
	if(gBatteryCheckAdcValue < lowbatteryLevel)	
	{
		gfLowBattery = 1;
	}	
#else	
	if(gBatteryCheckAdcValue < ADCVAL_BATTERY_LOW)	
	{
		gfLowBattery = 1;
	}
#endif 	
}

void BatteryCheck_No_LED_control(void)
{
#ifdef STD_GA_374_2019
	uint32_t lowbatteryLevel;	
#endif 

	P_LOWBAT_EN(1);
	Delay(SYS_TIMER_100US);

	gBatteryCheckAdcValue = ADCCheck(ADC_BATTERY_CHECK);	

	P_LOWBAT_EN(0);

	gfLowBattery = 0;

#ifdef STD_GA_374_2019
	if(gwLanguageMode == _CHINESE_MODE)
	{
		lowbatteryLevel = ADCVAL_BATTERY_LOW_4P8;
	}
	else
	{
		lowbatteryLevel = ADCVAL_BATTERY_LOW_4P3;
	}
	
	if(gBatteryCheckAdcValue < lowbatteryLevel)	
	{
		gfLowBattery = 1;
	}
#else	
	if(gBatteryCheckAdcValue < ADCVAL_BATTERY_LOW)	
	{
		gfLowBattery = 1;
	}
#endif 	
}


#define	BATT_6_0v		1959
#define	BATT_5_7v		1861
#define	BATT_5_0v		1632
#define	BATT_4_8v		1567
#define	BATT_4_6v		1502	
#define	BATT_4_5v		1469	


BYTE CalcBattLevel(WORD tBattValue)		// 저전압 기준을 5.0V(30%), 4.8V(15%), 4.5(10%)에 맞추고 구간별로 선형화함
{										// (비선형 함수인 배터리 방전 곡선을 2원 1차방정식으로 근사화 시킴)
	BYTE bResult;
	WORD wTmp;
	
	// 배터리 잔량(%) 계산 ------------------------------------------------------------------------------------------------
	// x : tBattValue(ADC value), y : gbBattLevel(%) 이라고 했을 때 두 점(x1, y1) = (5.7V, 80%), (x2, y2) = (6.0V, 100%)을 지나는 1차방정식은 y=(x-x1)*(y2-y1)/(x2-x1) + y1 이 된다
	if(tBattValue > BATT_5_7v)		// 100~80% 구간 
	{
//		wTmp = (WORD)(((double)tBattValue - (double)BATT_6_0v + 100. /20. * ((double)BATT_6_0v -(double)BATT_5_7v)) / ((double)BATT_6_0v -(double)BATT_5_7v ) * 20.);
		wTmp = (WORD)(((double)tBattValue - (double)BATT_5_7v) * (100. - 80.) / ((double)BATT_6_0v -(double)BATT_5_7v) + 80.);
		if(wTmp >  100) wTmp = 100; 		 // 100% 초과시 100%로 포화시킴 
		bResult = (BYTE)(wTmp & 0xFF);
	}
	else if(tBattValue > BATT_5_0v)// 80~21% 구간 
	{
//		wTmp = (WORD)(((double)tBattValue - (double)BATT_5_7v + 80. /60. * ((double)BATT_5_7v -(double)BATT_5_0v)) / ((double)BATT_5_7v -(double)BATT_5_0v ) * 60.);
		wTmp = (WORD)(((double)tBattValue - (double)BATT_5_0v) * (80. - 30.) / ((double)BATT_5_7v -(double)BATT_5_0v) + 30.);
		bResult = (BYTE)(wTmp & 0xFF);
	}
	else if(tBattValue > BATT_4_8v)// 20~14% 구간 
	{
//		wTmp = (WORD)(((double)tBattValue - (double)BATT_5_0v + 20. /7. * ((double)BATT_5_0v -(double)BATT_4_8v)) / ((double)BATT_5_0v -(double)BATT_4_8v ) * 7.);
		wTmp = (WORD)(((double)tBattValue - (double)BATT_4_8v) * (20. - 15.) / ((double)BATT_5_0v -(double)BATT_4_8v) + 15.);
		bResult = (BYTE)(wTmp & 0xFF);
	}
	else if(tBattValue > BATT_4_5v)// 13~7% 구간 
	{
//		wTmp = (WORD)(((double)tBattValue - (double)BATT_4_8v + 13. /7. * ((double)BATT_4_8v -(double)BATT_4_6v)) / ((double)BATT_4_8v -(double)BATT_4_6v ) * 7.);
		wTmp = (WORD)(((double)tBattValue - (double)BATT_4_6v) * (13. - 10.) / ((double)BATT_4_8v -(double)BATT_4_6v) + 10.);
		bResult = (BYTE)(wTmp & 0xFF);

		if(bResult < 10)	bResult = 10;
	}
	else	bResult = 10;				 // 4.5V 이하는 10%로 처리
	return bResult;
}



BYTE MakePercetageOfBatteryLevel(void)
{
	// ADC 최대 값과 Low Battery 기준 값의 차이를 100~Low Battery 퍼센트로 환산
	BYTE PercentOfBattery;

	PercentOfBattery = CalcBattLevel(gBatteryCheckAdcValue);

	return (PercentOfBattery);
}





