//------------------------------------------------------------------------------
/** 	@file		ISO14443AFunctions_T1.h
	@brief	4~7 bytes ISO14443A Card UID Read Functions 
*/
//------------------------------------------------------------------------------


#ifndef __ISO14443AFUNCTIONS_T1_INCLUDED
#define __ISO14443AFUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"


//=============================================================
//	ISO14443A COMMAND
//=============================================================
//------------------------------------------------------------------------------
/*! 	\def	ISO14443A_REQA		
	ISO14443A Command - REQA (26h)
	
	\def	ISO14443A_SEL_CASCADE1		
	ISO14443A Command - Select Cascade level 1 (93h)

	\def	ISO14443A_SEL_CASCADE2 	
	ISO14443A Command - Select Cascade level 2 (95h)

	\def	ISO14443A_HLTA 	
	ISO14443A Command - HLTA (50h)

*/
//------------------------------------------------------------------------------

#define	ISO14443A_REQA					0x26				//PICC가 있는지 검사하기 위한 Command 
#define	ISO14443A_WUPA					0x52

#define	ISO14443A_SEL_CASCADE1			0x93				//Select Cascade level 1
#define	ISO14443A_SEL_CASCADE2			0x95				//Select Cascade level 2

#define	ISO14443A_HLTA					0x50



//------------------------------------------------------------------------------
/*! 	\def	SAK_FAIL		
	Iso14443aSelectRespond 함수의 Return 값 
	Select 신호 전송 이후 수신한 SAK 관련하여 처리 실패
	
	
	\def	SAK_UID_NOT_COMPLETE		
	Iso14443aSelectRespond 함수의 Return 값 
	Select 신호 전송 이후 수신한 SAK가 UID not complete 내용을 포함

	\def	SAK_UID_COMPLETE		
	Iso14443aSelectRespond 함수의 Return 값 
	Select 신호 전송 이후 수신한 SAK가 UID complete 내용을 포함
*/
//------------------------------------------------------------------------------
#define	SAK_FAIL				0
#define	SAK_UID_NOT_COMPLETE	1
#define	SAK_UID_COMPLETE		2


void CardOpenUidSupportedInfoLoad(void);
void CardOpenUidSupportedInfoSave(BYTE SetValue);


void AntiColVariInit(void);
void CascadeLevelSet(BYTE Level);
void CopyCardUid(BYTE *TargetBuf, BYTE UidSize);

BYTE Iso14443aFindTag(BYTE *UidSize);
void Iso14443aReqaSend(BYTE Command);
BYTE Iso14443aReqaRespond(BYTE *UidSize);
BYTE Iso14443aAnticollisionSend(void);
BYTE Iso14443aAnticollisionRespond(BYTE bStatus);
void Iso14443aSelectSend(void);
BYTE Iso14443aSelectRespond(void);
void Iso14443aHalt(void);


#endif
