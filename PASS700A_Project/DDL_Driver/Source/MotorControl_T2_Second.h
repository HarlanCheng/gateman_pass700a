#ifndef	_MOTOR_CONTROL_T2_H_Second_
#define	_MOTOR_CONTROL_T2_H_Second_

#include "DefineMacro.h"
//#include "DefinePin.h"


enum	{
	MPROC_STOP_Second,			//사양서에는 없지만 정지된 상태를 정의 (동작전 또는 동작 마침 후 상태)
	MPROC_PRE_Second,
//사양서에 따른 구간을 state로 정의
	MPROC_TO_1_Second,
	MPROC_TO_2_Second,
	MPROC_TO_3_Second,
	MPROC_TO_4_Second,
	MPROC_TO_5_Second,
	MPROC_TO_6_Second,
	MPROC_TO_7_Second,
	MPROC_TO_8_Second,
	MPROC_TO_9_Second,
	MPROC_POST_Second
};


#define	NO_MPROC_STATE			9


#define	MCTRL_DIR_OPEN_Second			(0x01<<8)
#define	MCTRL_DIR_CLOSE_Second			(0x02<<8)
#define	MCTRL_DIR_MASK_Second			(0xFF00)

#define	MCTRL_STATE_MASK_Second			(0x00FF)



//------------------------------------------------------------------------------
/** 	@brief	Motor control을 위한 메인 structure
	@remark	TO1~TO9 는 모터 제어 구간 별 timeout값이다.(단위는 msec) 이 값이 0이면 그 구간은 존재하지 않는다.
	@remark structure내부의 함수 포인터들 중 motor 구동에 연관된 4개의 함수와
	@remark is_motor_open_Second, is_motor_close_Second 중 하나라도 NULL 일 경우, 등록되지 않는다.
	@remark 그 외 함수 포인터가 NULL 이면 그 동작은 정의 되어 있지 않고 함수 호출도 일어나지 않는다.
*/
//------------------------------------------------------------------------------
typedef		struct {
	uint16_t		motor_process_state_Second;				// state(bit0~7) and direction(bit8~15)

	uint16_t		TO_open_Second[NO_MPROC_STATE];				// TimeOut1 ~ TimeOut9	(unit msec)
	uint16_t		TO_close_Second[NO_MPROC_STATE];				// TimeOut1 ~ TimeOut9	(unit msec)

	void		(*init_sensor_hw_Second)( void );
	void		(*sensor_on_Second)( void );
	void		(*sensor_off_Second)( void );

	void		(*motor_open_Second)( void );
	void		(*motor_close_Second)( void );
	void		(*motor_stop_Second)( void );
	void		(*motor_hold_Second)( void );

	uint32_t		(*is_motor_open_Second)( void );		// sensor_open 감지
	uint32_t		(*is_motor_close_Second)( void );		// sensor_close 감지
	uint32_t		(*is_motor_center_Second)( void );		// sensor_center 감지
	uint32_t		(*is_motor_lock_Second)( void );		// sensor_lock 감지
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
	// 0x00 : 아무런 상태가 아닐때 
	// 0xAA : motor 가 close 동작중 open 시도를 할때 셋팅 
	//		이경우 open 시도시 open sensor 가 감지 되어도 open process 를 타게 한다 
	//		이때 PWM count 를 정보를 이용 보통 동작시 나오는 값과 차이가 있을 경우 open 방향으로 200ms 
	//		유지 해주고 backturn 은 skip 한다. 
	//		최초 문제는 모터가 닫히는 순간 open 을 시도 할 경우 모터 부하 상태서 풀리지 못하는 문제수정 
	//		PWM 사용시에만 수정 가능 하므로 PWM define 으로 묶는다. (HW timer 를 쓰면 되긴 함)
	uint8_t	MotorException_Second;
	// Motor 구동전 방향에 대한 setting MCTRL_DIR_OPEN_Second / MCTRL_DIR_CLOSE_Second
	uint16_t	Direction;
#endif 
} ddl_motor_ctrl_t_Second;


#ifdef	_MOTOR_CONTROL_C_Second

uint32_t		gwMotorCtrlTimer2ms_Second;		//2msec의 해상도를 갖는 Motor Control Timer

#else

extern	uint32_t		gwMotorCtrlTimer2ms_Second;

#endif

uint32_t		register_motor_ctrl_Second( ddl_motor_ctrl_t_Second *mctrl );
void		emergence_motor_stop_Second( void );
void		motor_control_open_type1_Second( void );
void		motor_control_close_type1_Second( void );
void		StartMotorOpen_Second( void );	// StartMotorOpen_Second()_20180611by_sjc
void		StartMotorClose_Second( void );//StartMotorClose_Second()_20180611by_sjc
void		MotorControlProcess_Second( void );

uint16_t GetMotorPrcsStep_Second(void);//GetMotorPrcsStep_Second()_20180611by_sjc

#ifdef M_CLOSE_PWMCH1_Pin//second pwm_주석 by sjc
uint16_t Get_PWM_Direction(void);
void Motor_PWM_Init(uint16_t Mdirection);
void Motor_PWM_DeInit(void);
#endif 


#define	_USE_MOTOR_TIME_SET_Second		0xFFFF


__weak uint32_t MotorOpenCloseTimeSetting_Second(void);//MotorOpenCloseTimeSetting_Second()_20180611by_sjc
__weak uint32_t MotorBackTurnTimeSetting_Second(void);//MotorBackTurnTimeSetting_Second()_20180611by_sjc
__weak uint32_t MotorOpenCloseTimeSetting_open_Second(void);
__weak uint32_t MotorOpenCloseTimeSetting_close(void);
__weak uint32_t MotorBackTurnTimeSetting_open_Second(void);
__weak uint32_t MotorBackTurnTimeSetting_close_Second(void);




//============================================================


//------------------------------------------------------------------------------
/*! 	\def	FINAL_MOTOR_STATE_OPEN		
	gFinalMotorStatus에 저장되는 값 
	최종 모터 구동 내용 - 열림 성공

	\def	FINAL_MOTOR_STATE_CLOSE		
	gFinalMotorStatus에 저장되는 값 
	최종 모터 구동 내용 - 닫힘 성공

	\def	FINAL_MOTOR_STATE_OPEN_ING		
	gFinalMotorStatus에 저장되는 값 
	최종 모터 구동 내용 - 현재 열림 동작 중

	\def	FINAL_MOTOR_STATE_CLOSE_ING		
	gFinalMotorStatus에 저장되는 값 
	최종 모터 구동 내용 - 현재 닫힘 동작 중

	\def	FINAL_MOTOR_STATE_OPEN_ERROR		
	gFinalMotorStatus에 저장되는 값 
	최종 모터 구동 내용 - 열림 동작 중 에러 

	\def	FINAL_MOTOR_STATE_CLOSE_ERROR		
	gFinalMotorStatus에 저장되는 값 
	최종 모터 구동 내용 - 닫힘 동작 중 에러 
*/
//------------------------------------------------------------------------------
#define	FINAL_MOTOR_STATE_OPEN			0x01
#define	FINAL_MOTOR_STATE_CLOSE			0x02
#define	FINAL_MOTOR_STATE_OPEN_ING		0x04
#define	FINAL_MOTOR_STATE_CLOSE_ING		0x08
#define	FINAL_MOTOR_STATE_OPEN_ERROR		0x10					
#define	FINAL_MOTOR_STATE_CLOSE_ERROR	0x20					


void SetFinalMotorStatus_Second(BYTE FinalStatus);//SetFinalMotorStatus_Second_20180611by_sjc
BYTE GetFinalMotorStatus_Second(void);//GetFinalMotorStatus_Second_20180611by_sjc


//------------------------------------------------------------------------------
/*! 	\def	SENSOR_NOT_STATE		
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 감지된 센서 없음

	\def	SENSOR_OPEN_STATE		
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 열림 센서 감지

	\def	SENSOR_CLOSE_STATE		
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 닫힘 센서 감지

	\def	SENSOR_CENTER_STATE		
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 중립 센서 감지

	\def	SENSOR_LOCK_STATE 	
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 내부강제잠금 센서 감지 
	(센서가 아닌 설정 방식도 동일한 값 사용)

	\def	SENSOR_OPENCEN_STATE	
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 열림 & 중립 센서 감지 

	\def	SENSOR_CLOSECEN_STATE	
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 닫힘 & 중립 센서 감지 

	\def	SENSOR_OPENLOCK_STATE	
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 열림 & 내부강제잠금 센서 감지 

	\def	SENSOR_CLOSELOCK_STATE	
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 닫힘 & 내부강제잠금 센서 감지 

	\def	SENSOR_CENTERLOCK_STATE	
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 중립 & 내부강제잠금 센서 감지 

	\def	SENSOR_OPENCENLOCK_STATE 
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 열림 & 중립 & 내부강제잠금 센서 감지 
	(SENSOR_OPENLOCK_STATE 으로 회신)

	\def	SENSOR_CLOSECENLOCK_STATE 
	gMotorSensorStatus에 저장되는 값
	현재 모터 센서 값 - 닫힘 & 중립 & 내부강제잠금 센서 감지 
	(SENSOR_CLOSELOCK_STATE 으로 회신)
*/
//------------------------------------------------------------------------------
#define	SENSOR_NOT_STATE		0x00
#define	SENSOR_OPEN_STATE		(1<<0)
#define	SENSOR_CLOSE_STATE		(1<<1)
#define	SENSOR_CENTER_STATE		(1<<2)
#define	SENSOR_LOCK_STATE		(1<<3)

#define	SENSOR_OPENCEN_STATE	(SENSOR_OPEN_STATE|SENSOR_CENTER_STATE)
#define	SENSOR_CLOSECEN_STATE	(SENSOR_CLOSE_STATE|SENSOR_CENTER_STATE)
#define	SENSOR_OPENLOCK_STATE	(SENSOR_OPEN_STATE|SENSOR_LOCK_STATE)
#define	SENSOR_CLOSELOCK_STATE	(SENSOR_CLOSE_STATE|SENSOR_LOCK_STATE)
#define	SENSOR_CENTERLOCK_STATE	(SENSOR_CENTER_STATE|SENSOR_LOCK_STATE)

#define	SENSOR_OPENCENLOCK_STATE	(SENSOR_OPEN_STATE|SENSOR_CENTER_STATE|SENSOR_LOCK_STATE)
#define	SENSOR_CLOSECENLOCK_STATE	(SENSOR_CLOSE_STATE|SENSOR_CENTER_STATE|SENSOR_LOCK_STATE)

#define	SENSOR_SENSOR_ERROR 	(SENSOR_OPEN_STATE|SENSOR_CLOSE_STATE)

void MotorSensorClear_Second(void);
BYTE MotorSensorCheck_Second(void);//MotorSensorCheck_Second_20180611by_sjc

void MotorStatusInitialCheck_Second(void);



void PCErrorClearSet(void);
BYTE PCErrorClearCheck(void);
void PCErrorClear(void);


#endif

