//------------------------------------------------------------------------------
/** 	\file		SerialEeprom_T1.h
	@brief	Doorlock EEPROM 24C08 Interface Header
	@see	MX_i2c_lapper.c
*/
//------------------------------------------------------------------------------

#ifndef __SERIALEEPROM_T1_INCLUDED
#define __SERIALEEPROM_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


void RomInitial(void);
void RomWrite(BYTE *bpData, WORD wAdrs, WORD wNum);
void RomRead(BYTE *bpData, WORD wAdrs, WORD bNum);
void RomWriteWithSameData(BYTE bData, WORD wAdrs, WORD wNum);
void SetReadProtect(void);



// 테스트 외에는 아래 내용은 모두 주석 처리 가능

#if 0
BYTE AutoUnitTest_SerialEeprom(void);
#endif

 
#endif
