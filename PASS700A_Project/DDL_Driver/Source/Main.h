#ifndef _MAIN_H_
#define _MAIN_H_

#include "stm32l1xx_hal.h"

// major config file
#include "ddl_config.h"

#include "DefineMacro.h"
#include "SystemInitial.h"
#include "SystemTick.h"
//#include "SystemTickLocal.h"

#include "MX_i2c_lapper.h"
#include "MX_uart_lapper.h"

#include "ddl_main.h"

#include "Buzzer.h"
//#include "DimmingFunctions.h"
#include "LedFunctions_T1.h"

#ifdef	DDL_CFG_TOUCHKEY
#include "TouchKeyFunctions.h"
#endif

#ifdef	DDL_CFG_PUSHKEY
#include "ADCKeyFunctions_T1.h"
#endif

#include "PowerDownMode.h"
#include "VoiceICFunctions-abov.h"
#include "ADCFunctions.h"
#include "KeyInput.h"
#include "FeedbackFunctions_T1.h"
#include "callback_function.h"

#if defined 	(DDL_CFG_RIM_GR14_S_MOTOR)
#include "Motor-RIM-GR14-S.h"
#elif defined (DDL_CFG_RIM_MOTOR)
#include "Motor-RIM.h"
#elif defined (DDL_CFG_L_MECHA)
#include "Motor-L_Mecha.h"
#elif defined (DDL_CFG_M60G_MECHA)
#include "Motor-M60G_Mecha.h"
#elif defined (DDL_CFG_CLUTCH)
#include "Motor-clutch.h"
#elif defined (DDL_CFG_DEADBOLT)
#include "Motor-Deadbolt.h"
#elif defined (DDL_CFG_CMPL_CLUTCH)
#include "Motor-cmpl-clutch.h"
#elif defined (DDL_CFG_Y_MECHA)
#include "Motor-y_Mecha.h"
#elif defined (DDL_CFG_A_MECHA)
#include "Motor-A_Mecha.h"
#elif defined (DDL_CFG_DUAL_CLUTCH)
#include "Motor-Dual_Sensor_Clutch.h"
#ifdef USED_SECOND_MOTOR
#include "Motor-Dual_Sensor_Clutch_Second.h"
#include "MotorControl_T2_Second.h"
#endif

#elif defined (DDL_CFG_ONEWAY_ONESENSOR_CLUTCH)
#include "Motor-OneWay_OneSendor_Clutch.h"
#ifdef USED_SECOND_MOTOR
#include "Motor-OneWay_OneSendor_Clutch_Second.h"
#include "MotorControl_T2_Second.h"
#endif

#elif defined (DDL_CFG_CSL_MECHA)
#include "Motor-CSL.h"
#elif defined (DDL_CFG_AL_MECHA)
#include "Motor-AL_Mecha.h"
#endif 
#include "MotorControl_T2.h"


#include "VolumeControl_T1.h"
#include "PincodeFunctions_T1.h"

#include "SerialEeprom_T1.h"
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
#include "MemoryMap_T2.h"
#else 
#include "MemoryMap_T1.h"
#endif 
#include "MainModeProcess_T1.h"

//#include "SisterModel.h"

#ifdef	DDL_CFG_FP_TCS4K
#include "FingerTCS4K.h"
#include "TCS4K_UART.h"
#elif defined (DDL_CFG_FP_PFM_3000) || defined (DDL_CFG_FP_TS1071M) //hyojoon_20160831 PFM-3000 add
#include "FingerPFM_3000.h"
#include "PFM_3000_UART.h"
#elif defined (DDL_CFG_FP_HFPM) 
#include "Finger_iRevo_HFPM.h"
#elif defined (DDL_CFG_FP_INTEGRATED_FPM) 
#include "Finger_goodix_FPM.h"
#endif


#ifdef	DDL_CFG_RFID
#include "CardProcess_T1.h"
#endif

#ifdef	DDL_CFG_IBUTTON 
#include "GMTouchKeyProcess_T1.h"
#endif

#if	defined (DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
#include "DS1972_IButtonProcess_T1.h"
#include "DS1972_Functions_T1.h"
#endif

#include "UtilityFunctions_T1.h"

#include "PincodeFunctions_T1.h"

#include "AutolockFunctions_T1.h"
#include "AlarmFunctions_T1.h"
#include "TamperlockoutFunctions_T1.h"


#ifdef	DDL_CFG_COVER_SWITCH
#include "TenKeyCoverSwitchFunctions_T1.h"
#endif


#include "BatteryCheckFunctions_T1.h"

#include "DebugFunctions_T1.h"
#include "ModeTestModeProcess_T1.h"

#include "JigTestFunctions_T1.h"

#include "MX_iwdg.h"
//#include "SisterModel.h"


#include "PackFunctions_T1.h"
#include "InnerPackFunctions_T1.h"

#if defined (_USE_IREVO_CRYPTO_) || defined (_USE_STM32_CRYPTO_LIB_)
#include "iRevo_Crypto.h" 
#endif 

#if defined (DDL_CFG_MS)
#include "MS_Utility.h"
#endif 
 

#include "ScheduleFunctions_T1.h"

#include "ModePackSetLEDProcess.h"

#ifdef	CYLINDER_KEY_ALARM	//실린더 알람 설정 메뉴 파일 로드 
#include "ModeMenuCylinderKeyAlarmSet_T1.h"
#endif

#if defined (RTC_PCF85063)
#include "RTC_PCF85063TP.h"
#endif 

#ifdef DDL_CFG_RFID_RC523
#include "ParallelFunctions_T2.h"
#include "MX_spi_lapper.h"
#include "RC523Functions_T1.h"
#include "ISO14443AFunctions_T2.h"
#endif

#ifdef DDL_CFG_IOEXPANDER
#include "I2Cexpander_T1.h"
#endif

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)

#if defined (__DDL_MODEL_GRAB100_FH) || defined (__DDL_MODEL_VULCAN_F) || defined (__DDL_MODEL_GRL_GR120)
#define	APP_ADRS_START_OFFSET		(uint32_t)0x2000
#else 
#define	APP_ADRS_START_OFFSET		(uint32_t)0x4000
#endif 

#define APPLICATION_ADDRESS   		(uint32_t)(0x08000000 + APP_ADRS_START_OFFSET)
#endif

__INLINE void ApplicationSetVectorTable(void)
{
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	SCB->VTOR = (__IO uint32_t )APPLICATION_ADDRESS;
	__enable_irq();
#else 
	__NOP();
#endif 	
}


extern void MX_SPI1_Init(void);
void SystemClock_Config(void);

#ifndef FALSE
#define FALSE               0
#endif 

#ifndef TRUE
#define TRUE                1
#endif 


#define MAGIC_ENTER_LONG_SLEEP	0xA342CD0E 
#define MAGIC_KEEP_LONG_SLEEP	0xF590C458


#ifdef	_MAIN_C_

/***
	아래 선언된 Private variables는 CubeMX에 의해 자동으로 만들어지는 변수이다.
	DDL관련 code에서는 좀 더 유연성을 갖기 위해 위 변수를 아래에 다른 이름의 포인터 변수에 한번 더 담아둔다.
	아래에서 정의된 MCU 기능은 아래 이름의 변수(gh_mcu_**)를 사용하도록 code를 작성한다.

	ADC_HandleTypeDef hadc;

	I2C_HandleTypeDef hi2c1;

	IWDG_HandleTypeDef hiwdg;

	RTC_HandleTypeDef hrtc;

	SPI_HandleTypeDef hspi1;

	TIM_HandleTypeDef htim2;
	TIM_HandleTypeDef htim3;
	TIM_HandleTypeDef htim4;

	UART_HandleTypeDef huart1;
	UART_HandleTypeDef huart3;
***/

ADC_HandleTypeDef	*gh_mcu_adc = &hadc;

I2C_HandleTypeDef	*gh_mcu_i2c = &hi2c1;

IWDG_HandleTypeDef	*gh_mcu_iwdg = &hiwdg;

RTC_HandleTypeDef	*gh_mcu_rtc = &hrtc;

#ifdef	DDL_CFG_RFID
#ifndef	DDL_CFG_RFID_PARALLEL
SPI_HandleTypeDef	*gh_mcu_spi = &hspi1;
#endif
#endif

//tim2는 voice 통신용 1kHz
TIM_HandleTypeDef	*gh_mcu_tim_voice = &htim2;

//tim3는 buzzer용 80kHz
TIM_HandleTypeDef	*gh_mcu_tim_buzzer = &htim3;

//tim4의 분해능은 10usec 로 설정되어 msec이하의 delay 및 timer 용으로 사용된다.
//counter가 16bit이기에 최대 delay 값은 655.35 msec(655350 usec) 가 될 수 있다.
TIM_HandleTypeDef	*gh_mcu_tim_usec = &htim4;

#if defined (M_CLOSE_PWMCH1_Pin) ||defined (PWM_BACKTURN_SKIP_COUNT)
TIM_HandleTypeDef	*gh_mcu_motor_timer9 = &htim9;
#endif 

#if defined (P_COM_DDL_EN_T) // PIN 확인 해서 만들어 줘야 함 
UART_HandleTypeDef	*gh_mcu_uart_com = &huart1;
#endif 

#if defined (P_BT_WAKEUP_T) // PIN 확인 해서 만들어 줘야 함 
UART_HandleTypeDef	*gh_mcu_uart_com2 = &huart2;
#endif 

#if defined	(DDL_CFG_FP_TCS4K) || defined	(DDL_CFG_FP_PFM_3000)  || defined (DDL_CFG_FP_TS1071M) || defined (DDL_CFG_FP_HFPM)  || defined (DDL_CFG_FP_INTEGRATED_FPM)//hyojoon_20160831 PFM-3000 add
UART_HandleTypeDef	*gh_mcu_uart_finger = &huart3;
#endif

#else

extern	ADC_HandleTypeDef	*gh_mcu_adc;

extern	I2C_HandleTypeDef	*gh_mcu_i2c;

extern	IWDG_HandleTypeDef	*gh_mcu_iwdg;

extern	RTC_HandleTypeDef	*gh_mcu_rtc;

#ifdef	DDL_CFG_RFID
#ifndef	DDL_CFG_RFID_PARALLEL
extern	SPI_HandleTypeDef	*gh_mcu_spi;
#endif
#endif

extern	TIM_HandleTypeDef	*gh_mcu_tim_voice;
extern	TIM_HandleTypeDef	*gh_mcu_tim_buzzer;
extern	TIM_HandleTypeDef	*gh_mcu_tim_usec;

#if defined (M_CLOSE_PWMCH1_Pin) ||defined (PWM_BACKTURN_SKIP_COUNT)
extern	TIM_HandleTypeDef	*gh_mcu_motor_timer9;
#endif 

#if defined (P_COM_DDL_EN_T) // PIN 확인 해서 만들어 줘야 함 
extern	UART_HandleTypeDef	*gh_mcu_uart_com;
#endif 

#if defined (P_BT_WAKEUP_T)
extern 	UART_HandleTypeDef	*gh_mcu_uart_com2;
#endif

#if defined	(DDL_CFG_FP_TCS4K) || defined	(DDL_CFG_FP_PFM_3000)  || defined (DDL_CFG_FP_TS1071M) || defined (DDL_CFG_FP_HFPM) || defined (DDL_CFG_FP_INTEGRATED_FPM) //hyojoon_20160831 PFM-3000 add //hyojoon_20160831 PFM-3000 add
extern	UART_HandleTypeDef	*gh_mcu_uart_finger;
#endif

#endif




#endif		//~_MAIN_H_

