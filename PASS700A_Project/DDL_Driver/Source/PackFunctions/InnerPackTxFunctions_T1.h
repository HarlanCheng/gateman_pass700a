//------------------------------------------------------------------------------
/** 	@file		PackTxFunctions_T1.h
	@brief	Communication Pack TX Process Functions
*/
//------------------------------------------------------------------------------


#ifndef __INNERPACKTXFUNCTIONS_T1_INCLUDED
#define __INNERPACKTXFUNCTIONS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




extern WORD gbInnerPackTxByteNum;
extern WORD gbInnerPackTxByteCnt;
extern BYTE gbInnerPackTxDataBuf[MAX_INNER_PACK_BUF_SIZE];
extern bool gfInnerPackAckWait;
extern bool gfInnerPackDirectSending;

extern BYTE gbInnerPackRetryCount;
extern BYTE gbInnerPackAckCompleteData[2];

extern  BYTE gbInnerPackTxRandomKeybuffer[16];
extern	BYTE gbInnerpackModeSuspend;

#define	MAX_INNER_PACK_TX_QUEUE_SIZE		12


// gPackTxProcessStep�� 
enum{
	INNER_PACK_TX_WAITING_MODE = 0,
	INNER_PACK_TX_PACKET_CHECKING_MODE,
	INNER_PACK_TX_EN_SIG_TI_SENDING_WAIT,
	INNER_PACK_TX_EN_SIG_TI_SENDING_MODE,
	INNER_PACK_TX_EN_SIG_TWU_SENDING_MODE,
	INNER_PACK_TX_PACKET_SENDING_MODE,
	INNER_PACK_TX_ACK_WAITING_MODE,
	INNER_PACK_TX_ENCRYPT_MODE

//	PACK_TX_START_CHECK,	
//	PACK_TX_START_ENSIG,
//	PACK_TX_SENDING_ENSIG,	
//	PACK_TX_START_DATA,
//	PACK_TX_SENDING_DATA,	
//	PACK_TX_ACK_WAITING,
};



__weak BYTE Get_Checksum_BCC(BYTE *buf, BYTE len);
__weak WORD Get_Checksum_CRC16(BYTE *buf, BYTE len);

void InnerPackTx_QueueInit(void);
bool InnerPackTx_QueueIsEmpty(void);
bool InnerPackTx_QueueIsFull(void);
void InnerPackTxWaitTime_EnQueue(BYTE TxWaitTime);
void InnerPackTx_EnQueue(void);
void InnerPackTx_EnQueueAtFirst(void);
void InnerPackTx_DeQueueStart(void);
void InnerPackTx_DeQueueEnd(void);
void InnerPackTx_AddChecksum(BYTE *buf, WORD *len);
void InnerPackTx_MakePacketSinglePort(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength);
void InnerPackTx_MakePacketForInitial(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength);
void InnerPackTx_MakeAndSendPacketNow(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength);
void InnerPackTx_MakeNack(BYTE* bpData , BYTE bMajor_Code, BYTE bMinor_Code);
__weak void PackTx_MakeAlarmPacket(BYTE bAlarmType, BYTE bAlarmLevel_H, BYTE bAlarmLevel_L);



void InnerPackDataTransmitProcess(void);

void InnerPackTxEnSigSetDefault(void);
void InnerPackTxEnSigTimeSet(BYTE* pTimeData);

void InnerPackTxProcessStepClear(void);
BYTE GetInnerPackTxProcessStep(void);


void InnerPackTxProcessTimeCounter(void);

void InnerPackTxWaitTimeCounter(void);


void InnerPackTxAckWaitTimeCounter(void);


void InnerPackTxEndProcess(void);


void InnerPackTxDataProcess(void);


extern BYTE gInnerPackTxAckWaitTimer100ms;
void InnerPackTxAckClearCheck(BYTE* bpData);
void InnerPackTxAckWaitCallback(void);



__weak void CopySlotNumberForPack(WORD SlotNumber);
__weak WORD GetSlotNumberForPack(void);
__weak void CopyCredentialDataForPack(BYTE* bDataBuf, BYTE bNum);
__weak void GetCredentialDataForPack(BYTE* bDataBuf, BYTE bNum);

__weak void PackTxEventPincodeAdded(WORD wUserSlot);
__weak void PackTxEventPincodeDeleted(WORD wUserSlot);
__weak void PackTxEventCredentialAdded(BYTE CredentialType, WORD wUserSlot);
__weak void PackTxEventCredentialDeleted(BYTE CredentialType, WORD wUserSlot);
__weak void PackAlarmReportInnerForcedLockOpenCloseFailSend(void);


void InnerPackTxEncapsulateData(BYTE *bQueueSendingData , uint16_t length);
void InnerPackTxGetCheckSumOfExisting(void);
BYTE InnerPackTxEncryptionKeyExchange(BYTE* bCheckSumData);



#endif


