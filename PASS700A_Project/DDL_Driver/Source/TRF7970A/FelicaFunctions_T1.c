//------------------------------------------------------------------------------
/** 	@file		FelicaFunctions_T1.c
	@version 0.1.00
	@date	2017.09.13
	@brief	8 bytes Felica Card IDm Read Functions using TRF7970
	@remark	 TRF7970을 이용해 Felica 카드의 IDm를 읽어들이기 위한 함수

	@see	TRF7970Functions_T1.c
	@see	CardProcess_T1.c
	@see	ParallelFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.08		by Jay
			- 일본 Felica 카드를 읽어 들이기 위한 함수
*/
//------------------------------------------------------------------------------


#include "Main.h"



void Felica_Polling_Send(void)
{
	TrfSetRegister(ISO_CONTROL, 0x1A);					//ISO Control (Address 01) = 0x1A
														//	Rx CRC
														//	Felica 212kbps

	TrfSetRegister(RX_WAIT_TIME, 0x01);					// 9.44us (Felica)
	TrfSetRegister(RX_SPECIAL_SETTINGS, 0x80);			// Appropriate for 212KHz subcarrier system(Felica)

	gTrfFifoDataBuf[0] = 0x8F;	// Reset Command
	gTrfFifoDataBuf[1] = 0x91;	// Transmit CRC Command
	gTrfFifoDataBuf[2] = 0x3D;	// Address of Register TX_LENGTH_BYTE_1
	gTrfFifoDataBuf[3] = 0x00;	// Data of TX_LENGTH_BYTE_1
	gTrfFifoDataBuf[4] = 0x60;	// Data of TX_LENGTH_BYTE_2
	
	gTrfFifoDataBuf[5] = 06;					// LEN
	gTrfFifoDataBuf[6] = FELICA_POLLING;		// REQC
	gTrfFifoDataBuf[7] = 0xFF;					// System Code,
	gTrfFifoDataBuf[8] = 0xFF;					// 0xFFFF : All PICCs shall respond	
	gTrfFifoDataBuf[9] = 0x00;					// Reserved			
	gTrfFifoDataBuf[10] = 0x00;					// System Code, 0x00 : Time Slot Number 
	
	TrfSendInventoryCommand(gTrfFifoDataBuf, 11);	

	//송신 완료할 때까지 대기
	Delay(SYS_TIMER_1MS);

	TrfSendCommand(ADJUST_GAIN);		//This command should be here to receive data properly.

}




BYTE Felica_Polling_Respond(void)
{
	BYTE FifoLength;

	//이 함수는 TRF7970을 사용하였을 경우에만 해당. 
	//TRF7963을 사용할 경우에는 FIFO가 12byte밖에 되지 않기 때문에 함수 변경 필요

	TrfGetRegisterData(FIFO_CONTROL, &FifoLength, 1);
	FifoLength &= 0x7F;

	if(FifoLength > 18 || FifoLength == 0)
	{
		return (STATUS_FAIL);
	}

	//TRF7970의 경우 FIFO가 127byte이므로 읽어 들인 18byte를 모두 저장해도 되지만, 
	//	gTrfFifoDataBuf의 size에 맞게만 읽어 들임.
	TrfGetRegisterData(FIFO, gTrfFifoDataBuf, FIFO_MAX_SIZE);

	return STATUS_SUCCESS;
}



BYTE FelicaFindTag(void)
{
	BYTE bNum;
	BYTE bSendingCnt = 2;;

	RfidPowerOn();

	RfidTxOn();

	IrqClear();

	// Maximum start-up tim of card = 20ms
	// It is recommened that the Reader/Writer continues generating the magnetic field for a period of time
	//	of at least 20.ms. thereafter, the Reader/Writer can transmit the Polling command.
	//	Although the Reader/Writer may transmit the Polling command before the 20.4ms period has 
	//	elapsed, it is recommened to retry the transmission of the Polling command, while considering
	//	the cae where no response is returned from the card.
	Delay(SYS_TIMER_20MS);

	while(bSendingCnt--)
	{
		Felica_Polling_Send();
		
		// Reference Source에서는 TX Wait Time 5ms, RX Wait Time 50ms 설정하여 사용하였지만,
		// 배터리 문제로 RX Wait Time을 약 4~5ms로 조정하여 사용 
		bNum = 20; // 약 4ms ~ 5ms 정도 대기
		while(--bNum)
		{
			// 카드 ID 읽어 오는 동안 불필요한 Access를 줄이기 위해 최소 200us 간격으로 확인
			Delay(SYS_TIMER_200US);

			if(IrqCheck() == IRQ_RX_COMPLETED)
			{
				// 18byte를 읽어들이기 위해 대기
				Delay(SYS_TIMER_2MS);

				if(Felica_Polling_Respond() == STATUS_SUCCESS)
				{
					return 1;
				}
				break;
			}
		}
	}
	
	RfidPowerOff();

   	return 0;
}



void CopyFelicaCardUid(BYTE *TargetBuf)
{
	// 읽어 들인 FIFO Data
	//	0x12 + 0x01 + IDm(8bytes) + PMm(8bytes)
	//	이 중 IDm을 UID로 저장
	memcpy(TargetBuf, gTrfFifoDataBuf+2, 8);
}




