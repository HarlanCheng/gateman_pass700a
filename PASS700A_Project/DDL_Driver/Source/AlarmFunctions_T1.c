//------------------------------------------------------------------------------
/** 	@file		AlarmFunctions_T1.c
	@version 0.1.00
	@date	2016.04.19
	@brief	Alarm Process (Intrusion, Broken, High Temperature)
	@remark  경보 처리 (침입, 파손, 고온)
	@remark  내부강제잠금 설정 경고 처리
	@see 	AutolockFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.19		by Jay
			- 침입/고온/파손 경보 및 내부강제잠금 상태 경고 처리
			- 침입/파손 경보는 해제하지 않을 경우 30분 발생
			- 고온 경보는 해제하지 않을 경우 Reset할 때까지 발생
			- 고온 경보가 발생할 경우 주기적으로 모터 열림 시도
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gAlarmPrcsStep = 0;					/**< 경보음 발생 및 경고음 발생 수행 단계  */					 
WORD gAlarmOnTimer1s = 0;					/**< 경고음 발생 시간, 1s Tick  */
WORD gAbnormalOnTimer1s = 0;					/**< 경고음 발생 시간, 1s Tick  */



//------------------------------------------------------------------------------
/** 	@brief	Alarm On Time Counter
	@param	None
	@return 	None
	@remark 경보음 발생 시간 계산, BasicTimer_1000ms_local 함수에 추가되어야 함. 
*/
//------------------------------------------------------------------------------
void AlarmOnTimeCounter(void)
{
	if(gAlarmOnTimer1s)		--gAlarmOnTimer1s;
	if(gAbnormalOnTimer1s) 	--gAbnormalOnTimer1s;
}



//------------------------------------------------------------------------------
/** 	@brief	Intrusion Alarm Start
	@param	None
	@return 	None
	@remark 침입 경보 발생
*/
//------------------------------------------------------------------------------
void AlarmGotoIntrusionAlarm(void)
{
	gfMute =0;

	gAlarmPrcsStep = ALARMPRCS_INTRUSION_SET;		
}

#ifdef P_KEY_INTER_CL_T
void AlarmGotoKeyInterCylinderAlarm(void)
{
	gfMute =0;

	gAlarmPrcsStep = ALARMPRCS_KEY_INTER_CLYNDER_SET;		
}
#endif 


//------------------------------------------------------------------------------
/** 	@brief	High Temperature Alarm Start
	@param	None
	@return 	None
	@remark 고온 경보 발생
*/
//------------------------------------------------------------------------------
void AlarmGotoHighTempAlarm(void)
{
	gfMute =0;

	gAlarmPrcsStep = ALARMPRCS_HIGH_TEMPERATURE_SET;
}



//------------------------------------------------------------------------------
/** 	@brief	Broken Alarm Start
	@param	None
	@return 	None
	@remark 파손 경보 발생
*/
//------------------------------------------------------------------------------
void AlarmGotoBrokenAlarm(void)
{
	gfMute =0;

	gAlarmPrcsStep = ALARMPRCS_BROKEN_SET;
}



//------------------------------------------------------------------------------
/** 	@brief	Inner Forced Lock Warning Start
	@param	None
	@return 	None
	@remark 내부강제잠금 상태 경고 발생
*/
//------------------------------------------------------------------------------
void AlarmGotoInnerLockOutWarning(void)
{
	gfMute =0;

//	if((AutoLockSetCheck() == 0) || 

	if(gAlarmPrcsStep)		return;
	
	gAlarmPrcsStep = ALARMPRCS_INNER_LOCKOUT_WARNING_SET;
}

#ifdef DDL_CFG_EMERGENCY_119
//------------------------------------------------------------------------------
/** 	@brief	중국 119 Alarm Start
	@param	None
	@return 	None
*/
//------------------------------------------------------------------------------
void AlarmGotoEmergency119(void)
{
#ifdef STD_GA_374_2019
	//BYTE	 bTmpArray[7];
#endif 
	
	gAlarmPrcsStep = ALARMPRCS_EMERGENCY_119_ALARM_SET;
#ifdef STD_GA_374_2019
	// 방식이 확정 되면 완전 삭제 
	//bTmpArray[0] = CREDENTIALTYPE_PANIC_CODE;
	//memset(&bTmpArray[1],0xFF, 6);
	//PackTx_MakePacket(EV_USER_CREDENTIAL_UNLOCK, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 7);
#endif 	
}
#endif 

//------------------------------------------------------------------------------
/** 	@brief	Alarm Clear
	@param	None
	@return 	None
	@remark 발생된 경보음 해제, 인증 수단이나 내부 버튼에 의한 해제
*/
//------------------------------------------------------------------------------
void AlarmGotoAlarmClear(void)
{
	gAlarmPrcsStep = ALARMPRCS_ALARM_CLEAR;
}



//------------------------------------------------------------------------------
/** 	@brief	Alarm Clear
	@param	None
	@return 	None
	@remark 내부 버튼+Reset에 의한 해제
*/
//------------------------------------------------------------------------------
void AlarmGotoAlarmClearNoFeedback(void)
{
	gAlarmPrcsStep = ALARMPRCS_ALARM_CLEAR_NO_FEEDBACK;
}



//------------------------------------------------------------------------------
/** 	@brief	Alarm Status Check
	@param	None
	@return 	[STATUS_SUCCESS] : 침입/고온/파손 경보 발생 중
	@return 	[STATUS_FAIL] : 침입/고온/파손 경보 해제 상태
	@remark 현재 경보음이 발생 중인지를 확인
*/
//------------------------------------------------------------------------------
BYTE AlarmStatusCheck(void)
{
#ifdef P_KEY_INTER_CL_T
	if(gfHighTempAlarm || gfIntrusionAlarm || gfBrokenAlarm || gfKeyInterClAlarm)
#else 
	if(gfHighTempAlarm || gfIntrusionAlarm || gfBrokenAlarm)
#endif 		
	{
		return (STATUS_SUCCESS);
	}

	return (STATUS_FAIL);
}



//------------------------------------------------------------------------------
/** 	@brief	Front Broken Check
	@param	None
	@return 	None
	@remark 파손 경보 발생 조건 확인
*/
//------------------------------------------------------------------------------
void FrontBrokenCheck(void)
{
	if(!gfXORFrontBroken)	return;						
	gfXORFrontBroken = 0;

	if(gfFrontBroken)
	{
		// 지그 모드일 경우 실제 경보는 발생하지 않음
		if(gbJigTimer1s)	return;
		AlarmGotoBrokenAlarm(); 		
 	}
}

#ifdef P_KEY_INTER_CL_T
void KeyInterCylinderCheck()	
{
#ifdef	CYLINDER_KEY_ALARM	//실린더 알람 사용 여부 판단 하는 eeprom data저장 변수 
	BYTE bTmp = 0xff;
#endif

	if(!gfXORkeyInterCl)	return;						
	gfXORkeyInterCl = 0;

#ifdef	CYLINDER_KEY_ALARM	//실린더키 알람 발생여부 판단 
	RomRead(&bTmp, (WORD)CYLINDER_KEY_ALARM_SET, 1);
	if(bTmp == 0x00)	return;
#endif

	if(gfkeyInterCl)
	{
		AlarmGotoKeyInterCylinderAlarm(); 		
 	}
}

void KeyInterCylinderAlarmStart(void)
{
	ModeClear();
	gAlarmOnTimer1s = ALARM_RUNNING_TIME;						
	gAlarmPrcsStep = ALARMPRCS_KEY_INTER_CLYNDER_RUN;

	DDLStatusFlagSet(DDL_STS_KEY_INTER_CL_ALARM);
	FeedbackAlarm(FEEDBACK_ALARM_INTRUSION);

	// Alarm 발생 Event 전송
	PackTx_MakeAlarmPacket(AL_TAMPER_ALARM, 0x00, 0x03);
#ifdef  P_EXTEND_BREAK_IN
	P_EXTEND_BREAK_IN(1);
#endif 
}

void KeyInterCylinderAlarmProcess(void)
{
	if(gAlarmOnTimer1s == 0)
	{
		gAlarmPrcsStep = 0;
#ifdef P_EXTEND_BREAK_IN
		P_EXTEND_BREAK_IN(0);
#endif 
		DDLStatusFlagClear(DDL_STS_KEY_INTER_CL_ALARM);
#ifdef P_SW_FACTORY_BROKEN_T
		ChangeFactoryPintoBrokenPinControl(0);
#endif 
		return;
	}
#ifdef P_SW_FACTORY_BROKEN_T
	ChangeFactoryPintoBrokenPinControl(1);
#endif 
	if((GetLedMode()== 0) && (GetMainMode() == 0))
	{

		LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
		LedSetting(gcbLedAlert, LED_DISPLAY_OFF);
#endif
									
#ifdef	DDL_CFG_NO_DIMMER
		LedSetting(gcbNoDimLedErr3, LED_DISPLAY_OFF);
#endif
	}
}
#endif 

void AlarmStartClearParameters(void)
{
#ifndef		P_SNS_LOCK_T	
	if(InnerForcedLockCheck())
	{
		InnerForcedLockClear();
	}
#endif

	TamperCountClear();
	DDLStatusFlagClear(DDL_STS_OUTLOCK);
}



//------------------------------------------------------------------------------
/** 	@brief	Intrusion Alarm Set
	@param	None
	@return 	None
	@remark 침입 경보음 발생 설정, 경보는 해제하지 않을 경우 30분 동안 발생
*/
//------------------------------------------------------------------------------
void IntrusionAlarmStart(void)
{
	ModeClear();
	gAlarmOnTimer1s = ALARM_RUNNING_TIME;						

	gAlarmPrcsStep = ALARMPRCS_INTRUSION_RUN;

//	TamperProofLockoutClear();
	DDLStatusFlagSet(DDL_STS_INTRUSION_ALARM);
	FeedbackAlarm(FEEDBACK_ALARM_INTRUSION);

	// Alarm 발생 Event 전송
	PackTx_MakeAlarmPacket(AL_TAMPER_ALARM, 0x00, 0x03);
	PackTxWaitTime_EnQueue(5);
	InnerPackTxWaitTime_EnQueue(5); 
#ifdef P_EXTEND_BREAK_IN
	P_EXTEND_BREAK_IN(1);
#endif 

}



//------------------------------------------------------------------------------
/** 	@brief	Intrusion Alarm Running Process
	@param	None
	@return 	None
	@remark 침입 경보음 발생 처리
*/
//------------------------------------------------------------------------------
void IntrusionAlarmProcess(void)
{
	if(gAlarmOnTimer1s == 0)
	{
		gAlarmPrcsStep = 0;
#ifdef P_EXTEND_BREAK_IN
		P_EXTEND_BREAK_IN(0);
#endif 
		DDLStatusFlagClear(DDL_STS_INTRUSION_ALARM);
		return;
	}

	AlarmFeedbackProcess(gcbBuzWar);
}


//------------------------------------------------------------------------------
/** 	@brief	High Temperature Alarm Set 
	@param	None
	@return 	None
	@remark 고온 경보음 발생 설정, 경보는 해제하지 않을 경우 Reset할 때까지 발생
*/
//------------------------------------------------------------------------------
void HighTemperatureAlarmStart(void)
{
	ModeClear();

	// 고온 경보는 제한 시간이 없고, Reset될 경우 해제됨
//	gAlarmOnTimer1s = ALARM_RUNNING_TIME;	
	gAlarmOnTimer1s = 2;	
	
	gAlarmPrcsStep = ALARMPRCS_HIGH_TEMPERATURE_RUN;
	gfHighTempAlarm = 1;
	FeedbackAlarm(FEEDBACK_ALARM_HIGHTEMP);
	
	// Alarm 발생 Event 전송
	PackTx_MakeAlarmPacket(AL_TAMPER_ALARM, 0x00, 0x04);
}



//------------------------------------------------------------------------------
/** 	@brief	High Temperature Alarm Running Process
	@param	None
	@return 	None
	@remark 고온 경보음 발생 처리, 주기적으로 모터 열림 시도
*/
//------------------------------------------------------------------------------
void HighTemperatureAlarmProcess(void)
{
	// 고온 경보는 제한 시간이 없고, Reset될 경우 해제됨
/*
	if(gAlarmOnTimer1s == 0)
	{
		gfHighTempAlarm = 0;
		gAlarmPrcsStep = 0;
		return;
	}
*/
	AlarmFeedbackProcess(gcbBuzFire);

	// 2초마다 확인하여 모터가 잠겨 있을 경우 열림 시도
	if(gAlarmOnTimer1s == 0)
	{
		gAlarmOnTimer1s = 2;

		StartMotorOpen();
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Broken Alarm Set 
	@param	None
	@return 	None
	@remark 파손 경보음 발생 설정, 경보는 해제하지 않을 경우 30분 동안 발생
*/
//------------------------------------------------------------------------------
void BrokenAlarmStart(void)
{
	gAlarmOnTimer1s = ALARM_RUNNING_TIME;						

	gAlarmPrcsStep = ALARMPRCS_BROKEN_RUN;

//	TamperProofLockoutClear();
	DDLStatusFlagSet(DDL_STS_BROKEN_ALARM);
	FeedbackAlarm(FEEDBACK_ALARM_BROKEN);

	// Alarm 발생 Event 전송
	PackTx_MakeAlarmPacket(AL_TAMPER_ALARM, 0x00, 0x02);
#ifdef P_EXTEND_BREAK_IN
	P_EXTEND_BREAK_IN(1);
#endif 
}




//------------------------------------------------------------------------------
/** 	@brief	Broken Alarm Running Process
	@param	None
	@return 	None
	@remark 파손 경보음 발생 처리
*/
//------------------------------------------------------------------------------
void BrokenAlarmProcess(void)
{
	if(gAlarmOnTimer1s == 0)
	{
		gAlarmPrcsStep = 0;
#ifdef P_EXTEND_BREAK_IN
		P_EXTEND_BREAK_IN(0);
#endif 
		DDLStatusFlagClear(DDL_STS_BROKEN_ALARM);
		return;
	}

	AlarmFeedbackProcess(gcbBuzBroken);
#ifdef P_SW_FACTORY_BROKEN_T
	ChangeFactoryPintoBrokenPinControl(1);
#endif
}



//------------------------------------------------------------------------------
/** 	@brief	Inner Forced Lock Waring Process
	@param	None
	@return 	None
	@remark 내부강제잠금 상태 경고 처리
*/
//------------------------------------------------------------------------------
void InnerLockoutWarning(void)
{
	BYTE bTmp;

	bTmp = MotorSensorCheck();

	if((bTmp & SENSOR_LOCK_STATE) && (!(bTmp & SENSOR_CLOSE_STATE)))
	{
		//내부강제잠금이 해제되거나 CLOSELOCK이 될때까지 경보 발생 
		AlarmFeedbackProcess(gcbBuzInlock);
	}
	else
	{
		FeedbackModeClear();
		gAlarmPrcsStep = 0;
	}
}

#ifdef DDL_CFG_EMERGENCY_119
//------------------------------------------------------------------------------
/** 	@brief	Emergency119AlarmStart
	@param	None
	@return 	None
*/
//------------------------------------------------------------------------------
void Emergency119AlarmStart(void)
{
	gAlarmOnTimer1s = 3; // 3 sec 항주 연동기 spec 
	gAlarmPrcsStep = ALARMPRCS_EMERGENCY_119_ALARM_RUN;
	gfEmergencyRun = 1;
#ifdef P_EXTEND_EMERGENCY
	P_EXTEND_EMERGENCY(1);
#endif 
}

void Emergency119AlarmProcess(void)
{
	if(gAlarmOnTimer1s == 0)
	{
		gAlarmPrcsStep = 0;
		gfEmergencyRun = 0;
#ifdef P_EXTEND_EMERGENCY
		P_EXTEND_EMERGENCY(0);
#endif 
	}
}
#endif 
//------------------------------------------------------------------------------
/** 	@brief	Alarm Process
	@param	None
	@return 	None
	@remark 경보음 및 경고음 설정/해제 처리 및 구동
*/
//------------------------------------------------------------------------------
void AlarmProcess(void)
{
	FrontBrokenCheck();
#ifdef P_KEY_INTER_CL_T
	KeyInterCylinderCheck();	
#endif 
	
	switch(gAlarmPrcsStep)
	{
		case 0:
			break;

		case ALARMPRCS_INTRUSION_SET:					
			AlarmStartClearParameters();
			IntrusionAlarmStart();
			break;

		case ALARMPRCS_INTRUSION_RUN:
			IntrusionAlarmProcess();
			break;
#ifdef P_KEY_INTER_CL_T
		case ALARMPRCS_KEY_INTER_CLYNDER_SET:
			AlarmStartClearParameters();
			KeyInterCylinderAlarmStart();			
			break;

		case ALARMPRCS_KEY_INTER_CLYNDER_RUN:
			KeyInterCylinderAlarmProcess();
			break;			
#endif			
		case ALARMPRCS_HIGH_TEMPERATURE_SET:
			PCErrorClearSet();

			AlarmStartClearParameters();

			HighTemperatureAlarmStart();
			break;
			
		case ALARMPRCS_HIGH_TEMPERATURE_RUN:
			HighTemperatureAlarmProcess();
			break;

		case ALARMPRCS_BROKEN_SET:					
			AlarmStartClearParameters();
			BrokenAlarmStart();
			break;

		case ALARMPRCS_BROKEN_RUN:
			BrokenAlarmProcess();
			break;

		case ALARMPRCS_INNER_LOCKOUT_WARNING_SET:
			InnerLockoutWarning();	
			break;

#ifdef DDL_CFG_EMERGENCY_119
		case ALARMPRCS_EMERGENCY_119_ALARM_SET:
			Emergency119AlarmStart();	
			break;

		case ALARMPRCS_EMERGENCY_119_ALARM_RUN:
			Emergency119AlarmProcess();	
			break;
#endif 

		case ALARMPRCS_ALARM_CLEAR:
			FeedbackModeClear();

		case ALARMPRCS_ALARM_CLEAR_NO_FEEDBACK:
			gfHighTempAlarm = 0;
#ifdef P_KEY_INTER_CL_T
			DDLStatusFlagClear(DDL_STS_INTRUSION_ALARM|DDL_STS_BROKEN_ALARM|DDL_STS_KEY_INTER_CL_ALARM);
#else 
			DDLStatusFlagClear(DDL_STS_INTRUSION_ALARM|DDL_STS_BROKEN_ALARM);
#endif 
#if defined (P_EXTEND_EMERGENCY) && defined (P_EXTEND_BREAK_IN)
			gfEmergencyRun = 0;
			P_EXTEND_EMERGENCY(0);
			P_EXTEND_BREAK_IN(0);
#endif 

//#ifdef P_KEY_INTER_CL_T
#ifdef P_SW_FACTORY_BROKEN_T
			ChangeFactoryPintoBrokenPinControl(0);
#endif 
//#endif 
	 	default:
			gAlarmPrcsStep = 0;
			break;
	}	
}




#ifdef	ST_TEMPERATURE_SENSOR
WORD TempSensorCheck(void)
{
	static BYTE AvgDegree[5] = {0x00,};	
	static BYTE Index = 0x00;
	WORD wTmp;
	WORD ST32_TS_30_CAL = *(__IO uint16_t*)(0x1FF8007A); // 3v 30 도 에 대한 CAL 값 
	WORD ST32_TS_110_CAL = *(__IO uint16_t*)(0x1FF8007E); // 3v 110 도 에 대한 CAL 값 	
	WORD ST32_VREFINT_CAL = *(__IO uint16_t*)(0x1FF80078);	
	WORD Vdda; 
	float Degree = 0.0;
	BYTE result;

#ifdef	DDL_CFG_HIGHTEMP_SENSOR
	/* VREF 값 ADC 측정 후 VDDA 값 계산 */
	/* 3v * vrefin_cal / vref_value */
	Vdda = (3000 * ST32_VREFINT_CAL) /ADCCheck(ADC_VREFINT_ST);

	/* ST 내부 TS ADC 값 측정 */ 
	wTmp = ADCCheck(ADC_TEMPERATURE_CHECK_ST);

	/* ADC_16 (TS) , ADC_17 (VREF) adc 측정 할 경우 ADC_CCR_TSVREFE enable 되므로 여기서 off */ 
	CLEAR_BIT(ADC->CCR, ADC_CCR_TSVREFE);
	
	/* 측정된 값을 ST 에서 제공하는 계산 식에 사용 하려면 3.0 volt 기준 data 로 변경 해야 함 */
	/* adc * 3.3v / 3v */
	wTmp = wTmp * Vdda / 3000; 

	/* 3.3 v 기준 adc 값을 계산식에 맞춰 계산 */
	/* ((110 -30) /(110_cal - 30_cal)) * (adc - 30_cal)) + 30 */
	AvgDegree[Index++]  = Degree = (((110.0 - 30.0)/(ST32_TS_110_CAL-ST32_TS_30_CAL))*((float)wTmp-ST32_TS_30_CAL))+30.0;
	Index %= 5;

	if(AvgDegree[1] == 0x00)
	{
		for(BYTE i = 0 ; i < 5 ; i++)
			AvgDegree[i] = AvgDegree[0];
		Index = 0;
	}
	else 
	{
		result = (AvgDegree[0] + AvgDegree[1] + AvgDegree[2] + AvgDegree[3] + AvgDegree[4]) / 5;
	}

#if 0 // test 
	P_FIRE_EN(1);
	wTmp = ADCCheck(ADC_TEMPERATURE_CHECK);
	P_FIRE_EN(0);
	printf("ADC [%d] ST TS = [%f] C , AVG = [%d] \n",wTmp,Degree,result);
#endif 

#else 
	result = 26u; //0x020A; 12bit -> 10bit 변환 문제 
#endif 
	return (result);
	
}
#else 
WORD TempSensorCheck(void)
{
	WORD wTmp;

#ifdef		DDL_CFG_HIGHTEMP_SENSOR
	P_FIRE_EN(1);
	wTmp = ADCCheck(ADC_TEMPERATURE_CHECK);
	P_FIRE_EN(0);
#else
	wTmp = 0x0829;//0x020A; 12bit -> 10bit 변환 문제 
#endif

	return (wTmp);
}
#endif 


#ifdef		DDL_CFG_HIGHTEMP_SENSOR

BYTE gbTempSensorTimer1s  = 0;
BYTE gbTempSensorCheckCnt = 0;
BYTE gbTempSensorBeforeVal = 0;
BYTE gbTempSensorCheckDisable	= 0;

#ifdef 	TEMPERATURE_HIGH_56_DEGREE 
		// 56 도 
		// 3.453k 는 table 상 56도로 정도 
		// ADC 값은 3023 
#ifdef	ST_TEMPERATURE_SENSOR
#define	ADCVAL_TEMPERATURE_HIGH		56
#else 		
#define	ADCVAL_TEMPERATURE_HIGH		3023 
#endif 
#else 
		// 49 도 
#ifdef	ST_TEMPERATURE_SENSOR
#define	ADCVAL_TEMPERATURE_HIGH		49
#else 		
#define	ADCVAL_TEMPERATURE_HIGH		2886
#endif 
#endif 

//#define	ADCVAL_TEMPERATURE_HIGH		2080		// For Test
#define	TEMPERATURE_CHK_MAXCNT		1


void TempSensorCheckCounter(void)
{
	if(gbTempSensorTimer1s)		--gbTempSensorTimer1s;
}


void TempSensorProcess(void)
{
	BYTE bTmp;
	
	if(gbTempSensorTimer1s)		return;
	gbTempSensorTimer1s = 5;

	bTmp = 0;
	if(TempSensorCheck() > ADCVAL_TEMPERATURE_HIGH)
	{
		bTmp = 1;
	}

	if(gbTempSensorBeforeVal != bTmp)
	{
		gbTempSensorCheckCnt = 0;
		gbTempSensorBeforeVal = bTmp;
	}
	else
	{
		if(gbTempSensorCheckCnt >= TEMPERATURE_CHK_MAXCNT)
		{
			if(bTmp == 1)
			{
				// 한번 고온 경보가 발생한 후에는
				// 온도가 떨어졌다가 다시 고온이 되어야만 경보 발생
				// Reset될 경우에는 고온이 유지되면 다시 경보 발생
				if(gbTempSensorCheckDisable)	return;

				gbTempSensorCheckDisable = 1;

#ifdef	P_SNS_EDGE_T			// Edge sensor
				if(gfDoorSwOpenState)
				{
					//문이 열려 있을 경우에는 고온 경보 발생하지 않음.
					gbTempSensorCheckCnt = 0;
					return;
				}
#endif
				
				AlarmGotoHighTempAlarm();
			}
			else
			{
				gbTempSensorCheckDisable = 0;
			}
		}
		else 
		{
			gbTempSensorCheckCnt++;
		}
	}
}

#endif


void AlarmGotoAbnormalAlarm(void)
{
#ifdef DDL_CFG_FP
	BioOffModeClear();
#else 
	ModeClear();
#endif 
	gbMainMode = MODE_ABNORMAL_PROCESS;
	gbModePrcsStep = 0;
 }

void ModeAbnormalProcess(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
#ifdef _MOTOR_BREAK
			_MOTOR_BREAK;
#endif 
			LedModeRefresh();
			gAbnormalOnTimer1s = 60;
#ifdef	DDL_CFG_DIMMER
			LedSetting(gcbLedAlert, LED_DISPLAY_OFF);
#endif
												
#ifdef	DDL_CFG_NO_DIMMER
			LedSetting(gcbNoDimLedErr3, LED_DISPLAY_OFF);
#endif
			gbModePrcsStep = 1;
			break;
			
		case 1:
			if(gAbnormalOnTimer1s == 0)
			{
				ModeClear();
#ifdef _MOTOR_BREAK
				_MOTOR_STOP;
#endif 
				break;	
 			}

#ifdef _MOTOR_BREAK
			_MOTOR_BREAK;
#endif 

			if((GetLedMode()== 0))
			{
				LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
				LedSetting(gcbLedAlert, LED_DISPLAY_OFF);
#endif
											
#ifdef	DDL_CFG_NO_DIMMER
				LedSetting(gcbNoDimLedErr3, LED_DISPLAY_OFF);
#endif
			}
			break;	

		default:
			ModeClear();
			break;
	}
}

