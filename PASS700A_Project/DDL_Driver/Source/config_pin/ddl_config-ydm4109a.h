//------------------------------------------------------------------------------
/** 	@file		ddl_config-ydm4109a.h
	@version 0.1.05
	@date	2018.05.29
	@brief	DDL의 전체 구성에 대한 Macro를 정의한다.
	@see
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00	2017.11.27		by hyojoon
			- 2017.11.27 일자 define 정리 통합라이브러리 에서 사용 하는 define 에 대한 정의 정리 

		V0.1.01 2017.12.06		by Jay
			- Remocon 신호 전송 기능 정의 추가 (Gate Lock에서 사용)
			- 기본 동작 모드 정의 (Gate Lock에서 사용)
			- 일본 Felica 카드 적용 여부를 관리하는 정의 추가
			
		V0.1.02 2018.02.01		by Jay
			- _MASTERKEY_IBUTTON_SUPPORT 설정 조건에서 DDL_CFG_DS1972_IBUTTON 사전 정의 확인 부분 삭제
			  DDL_CFG_DS1972_IBUTTON 설정이 될 경우 오히려 에러 발생. 
			  _MASTERKEY_IBUTTON_SUPPORT 단독으로 동작 가능

		V0.1.03 2018.02.06		by Jay
			- Touch IC Sensitivity 설정 내용 추가

		V0.1.04 2018.04.13		by hyojoon
			- IAR 설정 추가 
			- _USE_IREVO_CRYPTO_ EEPROM 설정에 상관없이 비밀 번호 , Card UID를 암호 화 설정 내용 추가
			- _USE_STM32_CRYPTO_LIB_ STM32 lib crypto 사용시 설정 내용 추가

		V0.1.05 2018.05.29		by Jay
			- _KEYPAD_WAKEUP_FOR_CARD_USE 설정 내용 추가 
			- _MASTER_CARD_SUPPORT 설정 내용 추가 
			- DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER 설정 내용 추가 
				=> 일본 Felica 카드 지원으로 wake up 후 카드 인증
				=> 일본향 제품의 경우 Master Card 등록 모드
				=> 해당 Master Card로 내부강제잠금 상태에서도 문 열림 가능
*/
//------------------------------------------------------------------------------


/*

기본 설정 시작 
==================================================

CubeMX 에서 생성 해주는 기본 프로젝트 설정에서 하기와 같이 변경 사용 

IAR project option
1. C/C++ Compiler : Optimization : Level -> Low
	Optimization 은 현재 Low 로 설정 사용 
	
2. C/C++ Compiler : Preprocessor: 기본으로 생성 되는 PATH 외에 아래 내용 복사 사용 필요 없는 부분 삭제 
$PROJ_DIR$/../DDL_Driver/Source
$PROJ_DIR$/../DDL_Driver/Source/config_pin
$PROJ_DIR$/../DDL_Driver/Source/ModeFunctions
$PROJ_DIR$/../DDL_Driver/Source/PackFunctions
$PROJ_DIR$/../DDL_Driver/Source/MOTOR
$PROJ_DIR$/../DDL_Driver/Source/DS1972_IBUTTON
$PROJ_DIR$/../DDL_Driver/Source/IBUTTON
$PROJ_DIR$/../DDL_Driver/Source/IREVO_HFPM
$PROJ_DIR$/../DDL_Driver/Source/PFM_3000
$PROJ_DIR$/../DDL_Driver/Source/TCS4K
$PROJ_DIR$/../DDL_Driver/Source/Goodix_FPM
$PROJ_DIR$/../DDL_Driver/Source/TRF7970A
$PROJ_DIR$/../DDL_Driver/Source/RTC
$PROJ_DIR$/../DDL_Driver/Source/Crypto
$PROJ_DIR$/../DDL_Driver/Source/Crypto/STM32_Cryptographic/Inc

Crypto 선언 해서 사용 할 경우 
STM32_Cryptographic 내에서 lib 파일을 선택해서 추가 해야 함 

3. OutPut Converter : Generate additional output
					output format : binary
					
4. Debugger : Step : I-jet/JTAGjet 선택 (자신이 가지고 있는 개발 장비 셋팅)

5. I-jet/JTAGjet : JTAG/SWD : SWD 선택 

==================================================
기본 설정  끝 


stack / heap 설정 
Linker : Config : *.icf 을 Edit... 해서 vector Table , Memory Regions , Stack / Heap Size 설정 가능 

stack usage 설정 
Linker : Advanced : Enable stack usage analysis
build 후에 map 파일을 열어 보면 stack usage 를 확인 할 수 있음 

*/

#ifndef		_DDL_CONFIG_YDM4109A_H_
#define		_DDL_CONFIG_YDM4109A_H_


//------------------------------------------------------------------------------
// Ten Key 입력형태에 대한 정의
// touch 10key 를 사용 하는 제품의 경우 해당 macro 를 define 한다.
//통합라이브러리에서는 TSM12 touch 센서를 사용 그외 IC 는 없다.
// 정전방식 터치키
#define	DDL_CFG_TOUCHKEY

#ifdef	DDL_CFG_TOUCHKEY
	//TSM12 를 쓰는 경우 define 한다. 현재 TSM12 밖에 안쓰므로 
	//DDL_CFG_TOUCHKEY 가 정의 되면 DDL_CFG_TOUCHKEY_TSM12 
	//를 같이 정의 한다. 
	#define	DDL_CFG_TOUCHKEY_TSM12

	// TSM12 를 사용 하면서 버튼 배열이 TYPE 1 인경우 
	//Z10-IH, WF20-DMP, A20-IH, E300-FH, WV-200
	//FPCB type
	#define	DDL_CFG_KEY_ARRANGE_TYPE1 

	// TSM12 를 사용 하면서 버튼 배열이 TYPE 2 인경우 
	//J20-IH, WV40-DMP, WE40-DMP, WE30-DMP
	// EMI GASKET type
	//#define	DDL_CFG_KEY_ARRANGE_TYPE2

	//	멀티 터치에 의한 일회용 무음 모드 지원
	#define	DDL_CFG_ONETIME_MUTE

	// Touch IC Sensitivity 값 설정
	#define	TOUCH_IC_SENSITIVITY_VALUE	0xAA

	// Touch IC Response Time Control 
	// Touch IC 에서 사용 하는 체터링 타임 
	// default 값 0x03 이면 3+2 구조로 5번 체터링 함 
	// 이를 1 + 2 하여 3번 체터링 하는 것으로 변경 하려면 아래 define enable 
	// 효과 : 반응 속도 빨라 짐 
	#define TOUCH_IC_RESPONSE_TIME_CONTROL_VALUE		0x31
	
#else 
	// push switch
	// ADC 를 이용하는 Push Switch Ten key 
	#define	DDL_CFG_PUSHKEY	

	//버튼 배열에 따른 정의로 현재 한가지 type 만 존재한다. 
	//DDL_CFG_PUSHKEY 가 정의 되면 DDL_CFG_PUSHKEY_TYPE1 같이 
	//정의 한다.
	#define	DDL_CFG_PUSHKEY_TYPE1
#endif
// Ten Key 입력형태에 대한 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// LED 에 대한 정의

// Front LED Dimmer 
// 통합라이브러리 에서는 PCA9532 IC 만 사용 한다.
#define		DDL_CFG_DIMMER

#ifdef		DDL_CFG_DIMMER
	// 16bit I2C LED dimmer PCA9532
	// DDL_CFG_DIMMER 가 정의 되면 DDL_CFG_DIMMER_PCA9532 같이 정의 한다. 
	#define	DDL_CFG_DIMMER_PCA9532

	//DIMMER 사용중 Main에 장착 되어 있는 GPIO제어 OPEN/CLOSE LED 제어 정의 
	//#define	DDL_MAIN_OC_LED
#else 
	// 개별 라이팅이 아닌 방식
	// 아래 type 별로 제품에 맞는 type 만 정의 해서 쓰고 나머지는 undefine 한다.
	#define	DDL_CFG_NO_DIMMER

	// Front LED_TENKEY , LED_DECO 
	#define	DDL_CFG_LED_TYPE1

	// Front LED_TENKEY , LED_LOWBAT (front) , LED_DEAD
	#define	DDL_CFG_LED_TYPE2

	// LED_TENKEY , LED_LOWBAT (front) , LED_DEAD , LED_IB
	#define	DDL_CFG_LED_TYPE3

	// Front LED_TENKEY , LED_DECO (TYPE1 과 같지만 커버에 의해 DECO 가 안보이는 경우가 있어 4로 차이를 둠)
	#define	DDL_CFG_LED_TYPE4

	//Front LED_TENKEY , LED_LOWBAT (front) , LED_DEAD , LED_OPEN , LED_CLOSE
	#define	DDL_CFG_LED_TYPE5

	//Front LED_TENKEY , LED_OPEN , LED_CLOSE
	#define	DDL_CFG_LED_TYPE6	

	//FDS
	//Front LED_TENKEY, LED_LOWBAT, LED_DEAD가 있는 경우
	#define	DDL_CFG_LED_TYPE7

	//PANPAN FC2A-P-A/F5P-A
	//Front LED_TENKEY, LED_LOWBAT, LED_DEAD
	#define	DDL_CFG_LED_TYPE8


#endif

//	Inside Low Battery LED 정의
	// low battery led 가 main 에 이는 경우 define 한다. 
	//#define	DDL_CFG_INSIDE_LED_LOW
	
// LED 에 대한 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//Ten Key 이외의 다른 인증 수단에 대한 정의 
// Ten key + 지문 구현 완료 
// Ten Key + Card 구현 완료 
// Ten Key + I-Button 구현 완료 
// Ten Key + Card + 지문 구현 완료 
// 위 조함 외에 Ten Key + 2way 이상 정의시 GetSupportCredentialCount() 함수 분석 할것 
// GetSupportCredentialCount() 함수는 memu mode 중 등록 삭제 에대한 하위 menu 를 자동으로 
// 구성 하기 위한 함수 이 부분 말고도 손대야 할 부분이 많음 (BLE 라든지...)
//------------------------------------------------------------------------------
// RFID
// Card 지원 제품 사용시 정의 한다 
//#define	DDL_CFG_RFID

#ifdef	DDL_CFG_RFID
	// RFID IC TRF7970A 만 사용 하므로 같이 정의
	#define	DDL_CFG_RFID_TRF7970A

	//통신 라인에 대한 정의 DDL_CFG_RFID_PARALLEL 가 정의 되면 PARALLEL 통신을 한다.
	//SPI 통신을 하려면 해당 macro 를 undefine 한다.
	#define	DDL_CFG_RFID_PARALLEL

	// OPEN UID 설정 여부를 관리 
	// 최초 빌드시 혹은 N1 , N2 팩에 의해서만 초기화 시에 
	// #define IREVO_CARD_ONLY    아이레보 카드 4byte 만 가능 
	// #undef IREVO_CARD_ONLY    4byte + 7 byte 카드 모두 가능 
	// PC 생산 프로그램 , Test mode 에서 enable , disable 설정 가능 
	// 현재는  #define OPEN_UID_EN  (ROM_TEST+13) 통해서 enable , disable 가능 하다. 
	#define	IREVO_CARD_ONLY		

	// 일본 Felica 카드 적용 여부를 관리
	// Felica 카드 적용할 때에는 전류 문제가 발생할 수 있기 때문에 Wake Up 후 카드 사용하도록 UI 적용 필요
	#define	FELICA_CARD_SUPPORTED


//#ifndef IREVO_CARD_ONLY
	// OPEN UID card read time 을 설정 한다. 
	// OPEN UID 를 사용 하기 위해 IREVO_CARD_ONLY가 undefie 되면 자동으로 OPEN_UID_DELAY
	// 를 define 하며 기본 값은 2MS 로 한다 (HW 적 파형은 3MS 로 동작한다, OPEN UID 가 아니면 HW 적으로 1MS로 동작)
	// 제품의 특성에 따라 delay 값을 유동적으로 가지고 간다. 
	// 값이 클수록 소모 전류가 올라 간다. 
	#define	OPEN_UID_DELAY		SYS_TIMER_2MS
//#endif 
	
	//카드 On 시간을 줄이는 부분
	//REQA(4ms) * 4회 동작 -> 2회 동작으로 변경
	//기본 undefine, YDM7116, YDM7216 제품에 사용
	//해당 설정 사용시TenKey에 Card 안테나 카드와 TenKey 동작 반드시 확인 
	//#define CARD_READ_OPTIMIZATION 	


	// 키패드 Wake Up 후 카드 사용 정의
	//	정의할 경우 카드 폴링 수행하지 않음
	#define _KEYPAD_WAKEUP_FOR_CARD_USE


	// 일본 제품을 위한 마스터 카드 기능 정의
	// 일본향 제품(TOHO)과 같이 마스터 카드 5장을 별도로 등록 사용하는 경우 (ex WV-40 JAPAN)
	#define	_MASTER_CARD_SUPPORT 

#ifdef	_MASTER_CARD_SUPPORT	
	// 일본향 제품(TOHO)과 같이 마스터 카드에 의해서만 내부강제잠금 상태에서도 문 열림 가능
	//	DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN 과는 처리 내용이 조금 다름
	//	마스터 카드가 아닌 경우에는 내부강제잠금 상태에서 문 열림 불가능
	#define	DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER
#endif

#endif
// RFID 에 대한 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// 지문
//지문 지원 제품 사용시 정의 한다
#define	DDL_CFG_FP

#ifdef	DDL_CFG_FP
	//지문 등록 총 수 보통 20 , Biosec TS1071M 의 경우 40개 까지 사용 가능 
	//_MAX_REG_BIO 값을 20이 아닌 40으로 변경 할 경우 
	// BLE 관련 함수인 CredentialOccupiedCheck() 를 추가 구현 해야 한다. (Biosec TS1071M 만 구현 됨)
	#define	_MAX_REG_BIO		100

	//TCS4K , TCS4C 사용시 
	//#define	DDL_CFG_FP_TCS4K

	// Area type 필리아 지문 모듈 (구현 되어 있지 않음)  
	//#define	DDL_CFG_FP_PFM_3000

	// Area type Biosec 지문 모듈 	
	//#define	DDL_CFG_FP_TS1071M	

	// Area type iRevo PPI 지문 모듈 HFPM (Handle Finger Print Module)
	//#define	DDL_CFG_FP_HFPM

	// Goodix 를 시작으로 하는 iRevo protocol FPM
	#define DDL_CFG_FP_INTEGRATED_FPM	
#ifdef	DDL_CFG_FP_TCS4K
	//지문 cover 가 motor 로 제어 되는 경우 
	#define	DDL_CFG_FP_MOTOR_COVER_TYPE
#endif 

#endif
// 지문 에 대한 정의 끝
//------------------------------------------------------------------------------





//------------------------------------------------------------------------------
// I-BUTTON
// I-Button 사용시 정의 
// DDL_CFG_IBUTTON : GM touch key 사용시(사각형 key) , DDL_CFG_DS1972_IBUTTON DS1972 사용시 (원형키)
// GM touch key 사용시 
//#define	DDL_CFG_IBUTTON //GM touch key

//DS1972 I-Buuton 사용시 
//#define	DDL_CFG_DS1972_IBUTTON // DS1972 

// 지문 주키 제품을 위한 마스터키 iButton 기능 정의
// DS1972 이면서 마스터키 ibutton 일때 정의 (ex WF-200 , 지문 모델 이면서 master i-button 지원)
//#define	_MASTERKEY_IBUTTON_SUPPORT 
// I-BUTTON 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	Stop Mode에서 깨어나는 RTC wakeup time 지정 하기 값(3472)을 일반적으로 사용 함 
/*
	STM32L151의 경우 RTCCLK는 37kHz
	Wakeup clock으로 RTCCLK/16을 사용하는 경우,
	tick당 시간은 1 / (37000/16) = 1/2312.5 = 0.000432 sec

	Example) 1.7sec wakeup time := 3931
*/
#define	DDL_CFG_RTC_WAKUPTIME	3472
//#define	DDL_CFG_RTC_WAKUPTIME	10000

// RTC wakeup time 지정 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	모터 구동 정의
// 현재 사용 하고 있지 않는 define 임 
//#define	DDL_CFG_MOTOR_TYPE1
// 하기 define 중 제품에 맞는 motor 선택 후 정의 나머지는 undef 

//GR14_S Motor 사용시 정의 (YDG413)
//#define	DDL_CFG_RIM_GR14_S_MOTOR

//RIM Motor 사용시 정의 (보조키 계열 Z10-IH 등등)
//#define	DDL_CFG_RIM_MOTOR

//L-MECHA 계열 모티스 사용시 정의
//#define	DDL_CFG_L_MECHA

//M60G 계열 모티스 사용시 정의 (YMG30 , 40 등 중국 계열 제품)
#define	DDL_CFG_M60G_MECHA

//Clutch type 사용시 정의 (YMH70)
//#define	DDL_CFG_CLUTCH

//DeadBolt 사용시 정의 (YDD424)
//#define	DDL_CFG_DEADBOLT

//Clutch type 사용시 정의 (YDM7111)
//#define	DDL_CFG_CMPL_CLUTCH

// Y-Mecha 사용시 정의 (vulcan) 
//#define	DDL_CFG_Y_MECHA

//A-MECHA 사용시 정의 (A990)
//#define	DDL_CFG_A_MECHA

//Dual Clutch사용시 정의(Front Door Suite)
//#define DDL_CFG_DUAL_CLUTCH

//2개의 모터 사용시 정의(Front Door Suite)
//#define USED_SECOND_MOTOR

//CSL_MECHA사용시 정의 (YDM7116_CSL)
//#define	DDL_CFG_CSL_MECHA

//AL_MECHA사용시 정의 mecha 와 달리 close 시에 50ms 더 밀어 주는 구간 존재 
//#define	DDL_CFG_AL_MECHA


#if defined (DDL_CFG_CLUTCH) || defined (DDL_CFG_DUAL_CLUTCH)
	// 내부 강제 잠금 상태에서 open 할 수 있도록 해주는 define 
	#define	DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
#endif 

//문이 열려 있는 경우 멀티 터치에 의해 모터 닫힘 동작 못하도록 하는 정의
//국내제품을 제외하고 모두 적용(APAC, China) 
#define	MOTOR_CLOSED_IN_OPEN_STATUS

//Open close key 가 아닌 close 전용 key 에 대한 정의
//#define	CLOSE_ONLY_BUTTON

//------------------------------------------------------------------------------
// O/C button disable 
// port 는 살아 있지만 실제 사용 하지 않는 경우 define 하여 사용 한다 
// o/c 가 감지 되면 바로 경보로 이동 한다. 
#define	DDL_CFG_DISABLE_OC
//	DDL_CFG_DISABLE_OC 정의  끝
//------------------------------------------------------------------------------

// PWM 제어 
// PWM 제어는 Motor-xxxxx.h 파일에서 정의 한다. 
// 기본 적으로 M_CLOSE_PWMCH1_Pin 이 CubeMX에서 정의가 되고 timer9 이 PWM 설정이 되야 PWM 으로 동작한다.  
// L-mecha , Dead bolt 의 경우 Test 가 완료 되었으나 , 나머지의 경우 튜닝이 필요 하다. 
// 아래는 각 Motor-xxxxx.h 에 정의 하는 define 에대한 설명을 한다. define 은 각 Motor-xxxxx.h 에서 한다.

// timer9 이 일반 timer 로 선언 되고 , PWM_BACKTURN_SKIP_COUNT 이 정의가 되면 
// PWM 사용 없이 timer9 은 motor 동작 시간 체크 용으로 쓰인다. 

	//PWM 시작 Duty 설정 50 으로 50%로 설정 
	//#define	PWM_START_DUTY	50

	//16Khz 로 동작 하는 PWM 적용시 100count 당 약 6ms 를 인지 할 수 있다
	//PWM 으로 제어 하는 경우 50% 에서 PWM_DUTY_HOLD_COUNTER 150 값 이후 다음 step 으로 % 를 올린다. 
	//현재 구현은 50% 9ms -> 60% 9ms -> 70% 9ms -> 80% 9ms -> 90% 9ms -> 100% 
	//9ms 는 100 count 당 6ms 이므로 150 의 경우 약 9ms 가 된다.  
	//현재 L-mecha 와 Deadbolt 값만 수렴 되어 있으므로 다른 모터 메카에서 PWM 사용시
	//값들을 측정후 적절히 사용 해야 한다.
	//#define	PWM_DUTY_HOLD_COUNTER	150

	//PWM 제어시나 timer9 을 PWM 아닌 시간 측정용으로 사용시 define 한다.
	//이 define 은 모터 닫힘 동작 시도중 바로 오프 시도를 하는 경우 모터 부하 상태로 남는 문제를 해결 하기 위한 값으로 모터가 닫힌 중일때 오픈 시도를 하는 경우 얼마나 빨리 open sensor 에 값이 인지가 되느냐를 측정 하고 그값이 PWM_BACKTURN_SKIP_COUNT 값보다 적을 경우 back turn 을 skip 하기 위한 define 이다. L-Mecha 만 측정 되었고 값은 5000 이다 
	//Timer9 을 선언하고  PWM_BACKTURN_SKIP_COUNT , PWM_HOLD_OPEN_TIME 이 둘다 선언될경우 위와 같은 논리로 PWM 없이 동작 한다.
	//motor_control_open_type1() 함수를 분석 하라.
	//#define	PWM_BACKTURN_SKIP_COUNT	5000

	//모터 닫힘 동작중 오픈 시도시 PWM_BACKTURN_SKIP_COUNT 값보다 적을 경우 센터를 어느 정도라도 맞추기 위해 close 방향으로 얼마만큼 돌리느냐를 설정 
	//L-mecha 만 측정 되어 있고 크게 중요 한 값은 아니다 현재 100 (200ms) 로 설정 
	//#define	PWM_HOLD_OPEN_TIME		100
//	모터 구동 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// Language 및 비밀번호 자리수 정의 
// PIN 최소 최대 길이를 정의 한다. 이곳에서 정의 되어 있지 않으면,
// PincodeFunctions.h에 정의된 default가 사용된다.
// 음성 IC 사용 하는 경우 : 중국어 선택시 자동으로 MIN 값이 6으로 변경 된다. 
// 음성 IC 사용 하는 하지 않는 경우 : 아래 define 값으로 정의 된다. 
#define	MIN_PIN_LENGTH			4 // 6
#define	MAX_PIN_LENGTH			10

//voice IC 를 지원 하는 경우 
// 아래중 하나 선택 
//4개 언어 지원 한국어 , 중국어 , 영어 , 대만어 지원 하면서 중국어 기본 설정 
#define	DDL_CFG_LANGUAGE_SET_TYPE1

//6개 언어 지원 (아직 없음) 
//#define	DDL_CFG_LANGUAGE_SET_TYPE2

//4개 언어 지원 한국어 , 중국어 , 영어 , 대만어 지원 하면서 한국어 기본 설정
//#define	DDL_CFG_LANGUAGE_SET_TYPE3

// Language 및 비밀번호 자리수 정의  끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	Touch O/C 정의 
// 이중 Open Close 버튼에 대한 정의 
//#define	DDL_CFG_TOUCH_OC
//	Touch O/C 정의  끝
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//	MUTE 키 정의
// Mute key 지원 제품시 설정 (Touch Ten key 이회용 무음과는 관계없음)
//#define	DDL_CFG_MUTE_KEY
//	MUTE 키 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	Ten Key slide cover 에 대한 정의  
//	DDL_CFG_COVER_SWITCH 를 정의시 해당 PIN 의 번호를 명시 해 준다. 
//	HAL_GPIO_EXTI_Callback() 의 DDL_CFG_COVER_SWITCH 부분 확인 할 것 
//	PIN 이 9번이면 9 . 0번이면 0 둘중에 하나 define 할 것 
//#define	DDL_CFG_COVER_SWITCH		9
//#define	DDL_CFG_COVER_SWITCH		0
//	Ten Key slide cover 에 대한 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	High Temperature Sensor 정의
//	고온 감지 지원 제품시 설정
//#define	DDL_CFG_HIGHTEMP_SENSOR

//------------------------------------------------------------------------------
//	ST 내부 TS 사용시 enable 
//	DDL_CFG_HIGHTEMP_SENSOR 도 같이 enable 해야 함 
//#define	ST_TEMPERATURE_SENSOR

//	56 도 제한시 설정 46도의 경우 undefine 
//#define	TEMPERATURE_HIGH_56_DEGREE 
//	High Temperature Sensor 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	와치독 정의
// 기본적으로 ddl_config_model.h 파일에서는 define 한 상태에서 사용 하고 
// 개발 용이나 , 디버깅 용으로 IAR 디버거를 사용 할 경우 
// ddl_config.h 파일에서 undef 해서 사용 
#define	DDL_CFG_WATCHDOG
//	와치독 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	Buzzer Volume Type 정의
//	Software volume control
//#define	DDL_CFG_BUZ_VOL_TYPE1	

//	HW volume control
#define	DDL_CFG_BUZ_VOL_TYPE2	

// 	특수 한 경우로 Buzzer 가 없는 제품의 경우 define 함 (YMH70)
//#define	DDL_CFG_NO_BUZZER
//	Buzzer Volume Type 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// main menu 관련 정의 

//	Lock Setting Menu 정의
//	메뉴 모드의 4번 하위 메뉴에 대한 정의 (advanced mode 6번)
//	FeedbackFunctions_T1.h , ModeMenuLockSetting_T1.h 관련 부분 확인 

//모드 변경 , 자동 잠금  설정 , 음량 설정 , 언어 설정
//#define	DDL_CFG_LOCK_SET_TYPE1	

//모드 변경 , 자동 잠금  설정 , 음량 설정 
//#define 	DDL_CFG_LOCK_SET_TYPE2

//모드 변경 
//#define 	DDL_CFG_LOCK_SET_TYPE3

//모드 변경 , 언어 설정
#define 	DDL_CFG_LOCK_SET_TYPE4

//모드 변경 , 언어 설정 , 좌우수 설정 
//#define 	DDL_CFG_LOCK_SET_TYPE5

//모드 변경 , 자동 잠금  설정 , 음량 설정 , 언어 설정 , 좌우수 설정
//#define 	DDL_CFG_LOCK_SET_TYPE6

//모드 변경 , 자동 잠금  설정
//#define 	DDL_CFG_LOCK_SET_TYPE7

//모드 변경 , 자동 잠금  , 언어 설정 설정
//#define 	DDL_CFG_LOCK_SET_TYPE8

//	Lock Setting Menu 정의  끝


//	듀얼 팩 8 번 메뉴 관련 정의 
//	듀얼 팩 지원 제품의 경우 팩 종류에 상관없이 정의 
//	정의 하게 되면 메뉴의 8번 하위 에 8번 리모콘 등록 메뉴가 활성화
//#define 	DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU
//	듀얼 팩 8 번 메뉴 관련 정의  끝


//	Zwave soft reset menu 정의 
//	일반 적으로 0번을 soft reset 기능으로 사용 하나 , R200-CH 같은 모델의 경우 9번을 사용 
//	9번을 soft reset 으로 사용시 define 
//#define 	_ZWAVE_SOFT_RESET_MENU_9_
//	Zwave soft reset menu 정의 끝

// main menu 관련 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	Abov voice index type define
//
//	AVML_MSG_T1 : 2016년 상반기에 정의된 list
//	AVML_MSG_T2 : 2016년 하반기에 정의된 list

//#define	AVML_MSG_T1
#define	AVML_MSG_T2
//	Ten-key pad [*]버튼 대신 [원형] 버튼을 쓰는 경우 정의
//	음성 안내에 차이가 있음	
//#define TKEY_CIRCLE_BTN_TYPE
//	Abov voice index type define  끝


// POWER_ON_MSG 
// power on 시 MSG 
// AVML_HELLO_MSG2 , AVML_HELLO_MSG1 둘중에 하나를 설정 해서 사용 
// define 하지 않으면 buzzer sound 
//AVML_HELLO_MSG1 당신의 세상을 지키다 게이트맨 
//AVML_HELLO_MSG2 당신의 세상을 지키다 게이트맨 타국 말의 경우 YALE brand 
#define	POWER_ON_MSG 		AVML_HELLO_MSG2
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	BLE-N 정의
//	스마트 리빙 BLE 팩 사용시 정의 
//	기본적으로 define 하여 사용
#define	BLE_N_SUPPORT

//	N-Protocol 3.0 적용할 경우 정의(BLE 3.0)
#define	DDL_CFG_BLE_30_ENABLE 0x32 
//	BLE-N 정의 끝
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//	BLE control enable / disable 정의 
//	Front 에 BLE가 실장된 제품에 한해서 사용
//	BLE module 쪽으로 enable , disable command 처리 
//#define	__FRONT_BLE_CONTROL__
//	__FRONT_BLE_CONTROL__ 정의 끝
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// Interrupt 및 input PIN 에 대한 active state 관련 정의 

#ifdef	DDL_CFG_FP
// 지문 커버나 , 에어리어 타잎 베젤 의 active state 를 정의 low , High 에 따라 High 인 경우 define 함.
#ifdef DDL_CFG_FP_INTEGRATED_FPM
#undef	DDL_CFG_FP_COVER_ACTIVE_HIGH
#else 
#define	DDL_CFG_FP_COVER_ACTIVE_HIGH
#endif 
#endif 

#ifdef	DDL_CFG_COVER_SWITCH
//	slide cover 가 active High 인 경우 
#define	DDL_CFG_COVER_ACTIVE_HIGH
#endif

//edge sns 가 active high 인 경우 
//#define	DDL_CFG_SNS_EDGE_ACTIVE_HIGH

// 등록 버튼이 active low 인 경우 
//#define	DDL_CFG_SW_REG_ACTIVE_LOW

// 내부 강제 잠금이 active high 인 경우 
//#define	DDL_CFG_SNS_LOCK_ACTIVE_HIGH

// CLOSE 센서가 active low 인 경우 
//#define	DDL_CFG_SNS_CLOSE_ACTIVE_LOW

// Interrupt 및 input PIN 에 대한 active state 관련 정의  끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// Remocon 신호 전송 기능 정의
// - Gate Lock의 기본 장착 통신팩에 적용된 기능
// - Gate Lock이 열리면 다른 연동된 도어록에 열림 신호를 전송하도록 하는 기능 추가

//#define	DDL_CFG_ADD_REMOCON_LINK_FUNCTION

//	Remocon 신호 전송 기능 정의 끝
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// 기본 동작 모드 정의
// 동작 모드 중 1개만 define 설정할 것

#define	DDL_CFG_NORMAL_DEFAULT		// Normal Mode, Advanced Mode 모두 지원 ; Normal Mode가 Default
//#define	DDL_CFG_ADVANCED_DEFAULT	// Normal Mode, Advanced Mode 모두 지원 ; Advanced Mode가 Default
//#define	DDL_CFG_NORMAL_ONLY			// Normal Mode 만 지원
//#define	DDL_CFG_ADVANCED_ONLY		// Advanced Mode 만 지원

// 모터 열림/닫힘 동작 후, FEEDBACK 재생 
// define 하지 않으면 동시 모터가 동작하는 즉시 음성FEEDBACK 재생
// define 하면 모터가 완전히 BACKTURN 까지 마친 후, 음성FEEDBACK 재생 
#define	FEEDBACK_AFTER_MOTOR_BACKTURN

// 기본 동작 모드 정의 끝
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// 기타 정의 

// PC 생산 프로그램 , N1 , N2 팩으로 초기화를 못하여 factory reset 만으로 초기화 하는경우 
// FactoryDefaultSetting(); 함수를 새로 제품에 맞게 구현 할것 
// 현재는 YMH70 기준으로 정의 되어 있음 
// 주의 사항!!!!!!!!!! 언어는 변경 하지 않는다. 
//#define	DDL_CFG_FACTORY_DEFAULT_SETTING

// 항주 연동기 
// 119+비번 , 비번+119 시 연동기로 신호를 보냄 (PIN 움직는건 확인 했으나 .. 실제 동작은 모름)
//#define	DDL_CFG_EMERGENCY_119

// SmartLeverLock 프론트 HW reset을 눌러 Lock Open하는 경우
// 일반적인 Standard DDL 제품에는 사용하지않음(국내 ROOM LOCK용) 
//#define DDL_CFG_EMERGENCY_UNLOCK


// EEPROM 사용 macro 
// ST 내부 EEPROM 사용시 define 
// 외부 EEPROM 사용시 undef 
#define _USE_ST32_EEPROM_
  
// EEPROM 사용 macro 
// 내부 EEPROM 사용시 비밀 번호 , Card UID 암호화 define 
// define 시 비밀 번호 , Card UID 관련 암호 복호 enable 
#ifdef _USE_ST32_EEPROM_
#define _USE_IREVO_CRYPTO_
#endif 

// AES128 사용시 
// 현재 STM32L 은 crypto HW 블럭이 없으므로 STM32 SW lib 을 사용 해야 함 
// STM32_Cryptographic 폴더 안에 STM32CryptographicV3.0.0_CM3_IAR_otnsc.a 사용 중임
// _USE_STM32_CRYPTO_LIB_ 가 선언되면 
// main.h 에서 crypto.h 파일을 include 하게 되어 있음 
// 해당 파일을 include 하면 config.h 파일을 indclude 하는데 
// config.h 파일의 안에 define 여부에 따라 AES 의 종류를 설정 할수 있음 
// 현재는 AES128 ECB 사용으로 설정 
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
#define _USE_STM32_CRYPTO_LIB_
#endif 

// DEBUG_FUNCTIONS_ENABLE 정의 
// 기본적인 처리는 uart1 번을 pack 용도가 아닌 다른 용도로 사용 debug 용이나 
// 다른 test mode 를 위해 사용 disable 상태가 기본 
// DEBUG_AGING 와 TERY_LIFE_CYCLE_TEST 가 자동으로 enable 됨 
// uart 1 port 115200
//#define	DEBUG_FUNCTIONS_ENABLE

 // DEBUG_PRINTF_ENABLE 정의 
 // IAR 없이 uart 1 번 port 로 printf 를 사용 하기 위해 
 // uart 1 port 19200
// #define	DEBUG_PRINTF_ENABLE

// KC 인증 바이너리 
// KC 인증 바이너리의 평상시 모드 : card 를 대고 있으면 open / close 를 반복 
// KC 인증 바이너리의 무변조 모드 : 등록 버튼 1회 입력시 무변조 card on 
//#define  DDL_TEST_SET_FOR_CARD


// Auto key 로 motor 를 control 해야 하는 경우 enable 
// Pych pull type 제품의 경우 main 에 oc 버튼이 없으면 
// motor 가 부하 구간에 멈출 경우 문 열기가 어려워 auto key 로 열거나 닫는다 
//#define DDL_CFG_AUTO_KEY_PROCESS

// 정전용량 Pincode test mode
// 중국 신뢰성에서 사용 한다고 함. 95% 
#define FACOTRY_PINCODE_TEST_MODE 

// china standard GS 374 
#define STD_GA_374_2019

#ifdef STD_GA_374_2019
#define	_USE_LOGGING_MODE_
#define	DDL_CFG_EMERGENCY_119
#undef	DDL_CFG_HIGHTEMP_SENSOR
#define	RTC_PCF85063
#define	EX_EEPROM		0x80000000  // CEE-512T8-08  24C512CI-T8T1U4 8PIN(TSSOP) EEPROM
#endif

// 통합 라이브러리 MS 사양 
// 통합 라이브러리의 MS 대응을 위한 define 
//#define	DDL_CFG_MS

#ifdef DDL_CFG_MS
/* MS 사양은 normal mode 만 지원 */
#undef	DDL_CFG_NORMAL_DEFAULT		// Normal Mode, Advanced Mode 모두 지원 ; Normal Mode가 Default
#undef	DDL_CFG_ADVANCED_DEFAULT	// Normal Mode, Advanced Mode 모두 지원 ; Advanced Mode가 Default
#define	DDL_CFG_NORMAL_ONLY			// Normal Mode 만 지원
#undef	DDL_CFG_ADVANCED_ONLY		// Advanced Mode 만 지원
/* BLE 미지원 */
#undef	BLE_N_SUPPORT
/* 내부 강제 잠금 상태에서 motor open */ 
#define DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
#endif 

//통합 라이브러리 PANPAN 특수 사양 
//#define	DDL_CFG_PANPAN_FUNCTION

#ifdef DDL_CFG_PANPAN_FUNCTION
#define CLOSE_BYPINCODE
#define CLOSE_BYCREDENTIAL
#define CYLINDER_KEY_ALARM
#endif

//----------------------------------AAAU UI-----------------------------------------------------------------------------------------------------------------

//#define DDL_DONT_USE_INNER_FORCED_LOCK	//버튼을 이용한 내부 강제 잠금 사용 금지 선언
//#define CLOSE_BYCREDENTIAL					//카드를 이용한 잠금을 사용하는 경우 선언 
//#define DDL_AAAU_REG_BUTTON_UI				//호주 및 뉴질랜드 등록 버튼 전용 UI
//#define DDL_AAAU_AUTORELOCK_UI				//호주 및 뉴질랜드 AUTO LOCK UI
//#define DDL_AAAU_FACTORY_RESET_UI			//호주 및 뉴질랜드 전용 Factory Reset UI
//#define DDL_LED_DISPLAY_SELECT				//led display 결정 
//#define DDL_AAAU_AUTOLOCK_HOLD			//호주 및 뉴질랜드 전용 autolock hold 기능
//#define DDL_DUAL_POINT_DPS					//FDS에서 사용하는 투 포인트 DPS 사용 정의
//#define DDL_DPS_OPENED_DONT_CRAD_ACCESS	//FDS , SCREENDOOR의 경우 문이 열려 있으면 Card를 이용한 해정 금지가 UI임
//#define DDL_FACTORY_RESET_PRECESSED_DONT_CHECK	//FDS,SCREENDOOR의 경우 키패드가 없고 자동 잠김 비활성화 그리고 카드 등록 없이 사용이 가능 함으로 비밀번호 등록 여부를 판단할 필요가 없음

//----------------------------------AAAU UI-----------------------------------------------------------------------------------------------------------------


/* long sleep 에 진입해서 소모 전류 등 확인 목적 */
//#define DDL_CFG_LONG_SLEEP

// ddl_config 내용 이동 
#define DDL_SET_CODE_MAJOR		0x19
#define DDL_SET_CODE_MINOR		0x05
#define PROGRAM_VERSION_MAJOR	11
#define PROGRAM_VERSION_MINOR	3

#define FW1_MAIN		PROGRAM_VERSION_MAJOR
#define FW1_SUB			PROGRAM_VERSION_MINOR
	
#define _PTODUCT_TYPE_ID1		0xFF		
#define _PTODUCT_TYPE_ID2		0xFF	
#define _PTODUCT_ID1			DDL_SET_CODE_MAJOR
#define _PTODUCT_ID2			DDL_SET_CODE_MINOR

// PC 생산 프로그램에서 테스트 기능을 자동 설정하는 데 필요한 내용(제품 사양에 맞게 설정값 변경하여 사용)
// -> 도어록 입력 추가 상태 확인 내용 1개 존재
//   => Front 9V 존재 (0x01), Front Reset 존재 (0x02)
#define ADD_DDL_STATUS_DATA_NUM                 0x01                // 추가 상태 확인 정보 1개
#define HW_CHECK_DATA                           0x03                // Front 9V | Front Reset    


// 기타 정의 끝
//------------------------------------------------------------------------------
#endif

