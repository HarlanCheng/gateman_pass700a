//------------------------------------------------------------------------------
/** 	@file		ModeMenuRemoconRegDel_T1.h
	@brief	All Remocons Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUSUBREMOCONREGDEL_T1_INCLUDED
#define __MODEMENUSUBREMOCONREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoSubRemoconRegDelCheck(void);
void SubRemoconSlotNumberSet(BYTE SlotNumber);
BYTE GetSubRemoconSlotNumber(void);
void ModeSubRemoconRegDelCheck(void);
void ModeSubRemoconRegister(void);
void ModeSubRemoconDelete(void);


#endif


