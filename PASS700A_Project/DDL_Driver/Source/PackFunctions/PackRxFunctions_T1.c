//------------------------------------------------------------------------------
/** 	@file		PackRxFunctions_T1.c
	@version 0.1.04
	@date	2016.07.26
	@brief	Communication Pack RX Process Functions
	@see	
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00	2016.06.13		by Jay
			- 통신팩을 RX Data 수신 처리에 대한 함수
		V0.1.01	2016.06.15		by Hoon
			<Lock과 Module 사이의 통신 원칙>
				1. Module은 Lock에게 한가지 명령 처리가 종료되기 전까지 다른 명령을 보내지 않는다
					1) Lock은 수신된 Command에 대하여 Ack를 바로 보내며 처리 시간이 오래 걸리는 Command는 처리 완료후 별도의 Alarm을 Module에게 보낸다.
					2) Lock이 Ack 송신시 Ack 송신완료까지 대기하지 않고 Tx 인터럽트로 보내면서 바로 다음 동작을 수행한다.
				2. Lock은 수신된 Command에 대하여 처리하는 동안 새로 수신된 Command에 대하여 최근 1개까지만 처리하고 그 이전의 수신 Command는 무시한다.
				3. Lock은 송신한 Alarm에 대하여 Ack를 수신하거나 Time Out 될 때까지 다음 송신 내용을 보류한다.
				4. Lock은 Ack 수신이 되지 않아 Time Out 되어도 재전송을 하지 않는다. 단, 0x44 명령은 예외.


			<Lock에서의 Pack 통신 기능 구현 원칙>
				1. Rx ISR에서는 1 Byte 수신시 마다 Checksum까지 확인한다. 올바른 Checksum 수신시 패킷 처리를 위해 PackRxProcessStepStart 함수를 호출한다.
				2. Rx 패킷 처리는 DDL_main에서 PackRxDataProcess 함수를 호출하여 처리한다.
				3. Tx ISR에서는 송신할 패킷의 마지막 Byte에 도달하면 Tx Queue 인덱스를 증가한다.
				4. 송신할 패킷은 언제든지 Tx Queue에 등록하면 되고, Tx Queue에 담긴 내용 송신은 DDL_main에서 PackTxDataProcess 함수를 호출하여 처리한다.
				5. User slot 1byte, 2byte 구분 처리
					: 타 라이브러리와 연계시 오류를 줄이기 위해서 기본은 WORD 형으로 처리하되 패킷 송신시에만 #ifdef로 구분 처리함

			- (0x10) Lock Operation (M -> L) 명령 추가
				PackRxDataProcess 함수 수정 
				PackRxCmdProcess 함수 추가
		V0.1.02 2016.06.29		by Hoon, 	[통신 Pack] Tx Queue 구조 추가
				PackTxFunctions_T1 파일 수정
					Get_Checksum_BCC, Get_Checksum_CRC16, PackTx_AddChecksum 함수 추가
					PackTx_MakePacket, PackTx_MakeAndSendPacketNow, PackTx_MakeNack, PackTx_MakeAlarmPacket 함수 추가
					PACK_TX_ENQUEUE_FIRSTLY, PACK_TX_ENQUEUE_FIRSTLY 상수 추가
					PackTx_QueueIsEmpty, PackTx_QueueIsFull, PackTx_EnQueue, PackTx_EnQueueAtFirst, PackTx_DeQueue 함수 추가
			(0x11) Get Lock Status (M -> L) 명령 추가
			(0x2B) Get Door Status (M -> L)
				ModePackLockStatusCheck_T1 파일 추가
					LockStsCheck 함수 추가
					DoorStsCheck 함수 추가
				PackRxCmdProcess 함수 수정
					case F0_GET_LOCK_STATUS:			// 0x11 명령
					case F0_GET_DOOR_STATUS:			// 0x2B 명령
			(0x12) Add 1 User Code (M -> L) 명령 추가
			(0x82) Add 1 User Code Extended ID (M -> L)
				PackRxCmdProcess 함수 수정
					case F0_ADD_1_USER_CODE:			// 0x12 또는 0x82 명령 
				PackTx_MakeNack 함수 추가
				UpdatePincodeToMemoryByModule 함수 수정
			(0x15) Get 1 User Code (M -> L) 명령 추가
			(0x85) Get 1 User Code Extended ID (M -> L)
				PackRxCmdProcess 함수 수정
					case F0_GET_1_USER_CODE:			// 0x15 또는 0x85 명령
				GetUserStatus, PackGetUserCode, AsciiToBcd, BcdToAscii 함수 추가
			(0x13) Delete User Code(s) (M -> L) 명령 추가
			(0x83) Delete User Code(s) Extended ID (M -> L)
				PackRxCmdProcess 함수 수정
					case F0_DELETE_USER_CODE:		// 0x13 또는 0x83 명령
				SetUserStatus, PackDeleteUserCode 함수 추가
			(0x16) Set 1 User Code Status (M -> L) 명령 추가
			(0x86) Set 1 User Code Status Extended ID (M -> L)
				PackRxCmdProcess 함수 수정
					case F0_SET_1_USER_CODE_STATUS: // 0x16 또는 0x86 명령
			(0x17) Get 1 User Code Status (M -> L) 명령 추가
			(0x87) Get 1 User Code Status Extended ID (M -> L)
				PackRxCmdProcess 함수 수정
					case F0_GET_1_USER_CODE_STATUS: // 0x17 또는 0x87 명령
		V0.1.03 2016.07.07		by Hoon, N-Protocol Product List v1.9 참조하여 수정
			(0x12) Add 1 User Code (M -> L) 명령 수정
				PackRxCmdProcess 함수 수정
					case F0_ADD_1_USER_CODE:		// 0x12 또는 0x82 명령 
				PackAdd1UserCode 함수 수정
			(0x13) Delete User Code(s) (M -> L) 명령 수정
				AllUserCodeDelete 함수 수정
				PackRxCmdProcess 함수 수정
					case F0_DELETE_USER_CODE:		// 0x13 또는 0x83 명령
			(0x15) Get 1 User Code (M -> L) 명령 수정
				PackRxCmdProcess 함수 수정
					case F0_GET_1_USER_CODE:

		V0.1.04 2016.07.26		by Jay
			- BLE-N 기능 추가
*/
//------------------------------------------------------------------------------

#include "Main.h"



WORD gbPackRxByteCnt = 0;
BYTE gbPackRxAddDataLen = 0;
BYTE gbPackRxChecksum = 0;
BYTE gbPackRxDataBuf[MAX_PACK_BUF_SIZE];

WORD gbPackRxCopiedByteCnt = 0;
BYTE gbPackRxDataCopiedBuf[MAX_PACK_BUF_SIZE];

BYTE gPackRxProcessStep = 0;


void PackRxDataClear(void)
{
	gbPackRxByteCnt = 0;
	gbPackRxAddDataLen = 0;
	gbPackRxChecksum = 0;
}

BYTE GetPackRxProcessStep(void)
{
	return (gPackRxProcessStep); 
}

#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
static void SetOTA_From(void)
{
	BYTE bTmp;
	/*
	Smart living 0x10 | uart1 
	Z-wave       0x20 | uart1 	
	CBA             0x30 | uart1
	*/
	bTmp = 0x01 |(gfPackTypeiRevoBleN30 ? 0x10 : gfPackTypeZBZWModule ? 0x20 : gfPackTypeCBABle ? 0x30 : 0x00);

	RomWrite(&bTmp,OTA_FROM,1);
}
#endif 

#if defined (P_COM_DDL_EN_T)

void PackDataReceive(void)	//ST에서 사용 하는 함수 : void PackDataReceiveProcess(void)
// Pack Rx ISR에서 체크섬까지는 확인한다. 체크섬 맞으면 PackRxProcess 시작을 준비한다
{
	BYTE bTmp;

	SetPackModeTimeCount(20);
	
	if ( CommPack_DequeueRx( &bTmp, 1 ) != 1 )	// receive queue is empty
		return;

	if(gbPackRxByteCnt >= MAX_PACK_BUF_SIZE)
	{
		gbPackRxByteCnt = 0;
	}	

	if(GetPackRxProcessStep() > PACK_RX_WAKEUP)	// 이전에 수신한 내용 처리중에는 수신 내용 무시
	{
#if 0		
		/* 여기서 gbPackRxByteCnt 를 초기화 하면 gbPackRxDataCopiedBuf 가 이전것으로 남아 있어서 */
		/* 이전 msg 를 두번 실행하는 경우가 생김 이전 msg 에 문제를 야기 하니 초기화 는 복사후 하는걸로 */
		gbPackRxByteCnt = 0;
#endif 
	}
	else
	{
		gbPackRxDataBuf[gbPackRxByteCnt] = bTmp;
		if(gbPackRxByteCnt >= (gbPackRxAddDataLen + 4))
		{
			gbPackRxByteCnt++;

			if(gbPackRxDataBuf[4 + gbPackRxAddDataLen] == gbPackRxChecksum)	// 체크섬이 맞으면 
			{
				PackRxProcessStepStart();
			}
		}
		else
		{	
			gbPackRxChecksum ^= gbPackRxDataBuf[gbPackRxByteCnt];
			gbPackRxByteCnt++;
			if(gbPackRxByteCnt == (PACK_F_LENGTH_ADDDATA + 1))
			{
				gbPackRxAddDataLen = gbPackRxDataBuf[PACK_F_LENGTH_ADDDATA];
			}
		}
	}
}



void PackDataReceiveProcess(void)
{
	if(JigModeStatusCheck() == STATUS_SUCCESS)
	{
		JigDataReceive();
	}
	else
	{
		PackDataReceive();
	}
}



void PackRxDataProcess(void)
{
	BYTE bSource;
	WORD i;

	switch(gPackRxProcessStep)
	{
		case PACK_RX_WAITING_MODE:
			break;

		case PACK_RX_WAKEUP:
			if(GetPackModeTimeCount()) 	break;
			
			// 통신팩 탈착 과정에서는 해당 내용 수행하지 않기 위해
			if(gbModuleMode && PackConnectedCheck() && (PackModeCheck() == STATUS_FAIL))
			{
				OffGpioCommPack();
				SetupCommPack_UART();
				//반드시 UART Setup 후에 Service callback 함수 등록을 해야 정상적으로 동작됨. 
				InitCommPackService();
			}

			gPackRxProcessStep = PACK_RX_WAITING_MODE;
			break;

		case PACK_RX_PACKET_CHECKING_MODE:	
			
			if(!gbEncryptionType)
			{
				gbPackRxCopiedByteCnt = gbPackRxByteCnt;	// 수신된 바이트 수 백업
				for(i = 0; i < gbPackRxCopiedByteCnt; i++)		// 수신 버퍼 백업(처리가 오래 걸릴 경우 데이터 무결성 확보를 위해 필요)
					gbPackRxDataCopiedBuf[i] = gbPackRxDataBuf[i];
			}
		
			PackRxDataClear();
			
			bSource = gbPackRxDataCopiedBuf[PACK_F_SOURCE];
			//gbComCnt = gbPackRxDataCopiedBuf[2];	// 20200807 제거 hyojoon kim // 패킷 Count 저장

			if((bSource == ES_MODULE_EVENT) || (bSource == ES_MODULE_EVENT_NOACK))  
			{
				gPackRxProcessStep = PACK_RX_CMD_PACKET_PROCESSING_MODE;
			}
			else if(bSource == ES_MODULE_ACK)
			{
				gPackRxProcessStep = PACK_RX_ACK_PACKET_PROCESSING_MODE;
			}
			else
			{
				gPackRxProcessStep = PACK_RX_WAITING_MODE; 
			}
			break;


		case PACK_RX_ACK_PACKET_PROCESSING_MODE:	
			
			PackTxAckClearCheck(gbPackRxDataCopiedBuf);
			
			if(PackRxAckProcess())
			{
				gPackRxProcessStep = PACK_RX_WAITING_MODE; 
				break;
			}
			else if(PackRemoconAckProcess())
			{
				gPackRxProcessStep = PACK_RX_WAITING_MODE; 
				break;
			}
			gPackRxProcessStep = PACK_RX_WAITING_MODE; 
			break;


		case PACK_RX_CMD_PACKET_PROCESSING_MODE:	
			if(PackRemoconCmdProcess())
			{
				gPackRxProcessStep = PACK_RX_WAITING_MODE; 
				break;
			}
			else if(PackRxCmdProcess())
			{
				gPackRxProcessStep = PACK_RX_WAITING_MODE; 
				break;
			}
#ifdef	BLE_N_SUPPORT
			else if(PackRxBleNCmdProcess())
			{
				gPackRxProcessStep = PACK_RX_WAITING_MODE; 
				break;
			}
#endif
			else
			{
				// 등록되어 사용 가능한(ID가 저장된) HCP만 처리 가능한 Command
				PackRxPermittedCmdProcess();
			}
			gPackRxProcessStep = PACK_RX_WAITING_MODE; 
			break;


		case PACK_RX_CMD_PACKET_DECRYPT_PROCESSING_MODE:
			if(PackRxDecapsulateData(&gbPackRxDataBuf[PACK_F_ADDDATA],gbPackRxDataBuf[PACK_F_LENGTH_ADDDATA]))
			{
				gPackRxProcessStep = PACK_RX_PACKET_CHECKING_MODE; 
			}
			else
			{
				gPackRxProcessStep = PACK_RX_WAITING_MODE; 
			}
			break;


		default:
			gPackRxProcessStep = PACK_RX_WAITING_MODE; 
			break;
	}		
}



void PackRxProcessStepStart(void)
{
	if(gbEncryptionType)
	{
		gPackRxProcessStep = PACK_RX_CMD_PACKET_DECRYPT_PROCESSING_MODE;
	}
	else
	{
		gPackRxProcessStep = PACK_RX_PACKET_CHECKING_MODE;
	}
}



void PackRxProcessWakeUp(void)
{
	gPackRxProcessStep = PACK_RX_WAKEUP;
}


void PackRxProcessStepClear(void)
{
	gPackRxProcessStep = 0; 
}

BYTE PackRxAckProcess(void)
{
	BYTE RetVal = 0;
	BYTE bTmp;

#if 0
	if(memcmp(gbAckCompleteData,gbPackRxDataCopiedBuf,2) == 0)
	{
		gfAckWait = false;	// 응답(Ack, Nack) 대기 해제 (중간에 다른 패킷 처리가 끼어들지 않는 다는 전제에서 구현함)
	}
#endif 

	switch(gbPackRxDataCopiedBuf[PACK_F_EVENT])
	{
		case EV_DEVICE_CHK :
			bTmp = PackTypeCheck(gbPackRxDataCopiedBuf[PACK_F_LOCKTYPE + 1]);
			if(bTmp != PACK_TYPE_JIG)
			{
				PackDeviceCheck();
			}

			PackTxEnSigTimeSet(&gbPackRxDataCopiedBuf[PACK_F_WAKEUPTIMING]);

			if(gbModuleProtocolVersion >= 0x30)
			{
				PackTxGetCheckSumOfExisting();
			}

			RetVal = 1;
			break;

		case F0_RFM_CHECK:
#ifdef	_NEW_YALE_ACCESS_UI
			if(gfPackTypeCBABle)
			{
				if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA] == 0x00)
				{
					SetYaleAccessMode(YALE_ACCESS_MODE_REGISTERED);
				}
				else
				{
					SetYaleAccessMode(YALE_ACCESS_MODE_DELETED);
				}
			}
			else
			{
				if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA] == 0x00)
				{
					gfPackZBZWRegisterd = 1;
				}
				else
				{
					gfPackZBZWRegisterd = 0;
				}
			}
#else	// _NEW_YALE_ACCESS_UI
			if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA] == 0x00)
			{
				gfPackZBZWRegisterd = 1;
			}
			else
			{
				gfPackZBZWRegisterd = 0;
			}
#endif // _NEW_YALE_ACCESS_UI
			break;			

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
		case F0_GET_TIME_AND_DATE:
#ifdef RTC_PCF85063 
			{
				if(Timecheck(&gbInnerPackRxDataCopiedBuf[PACK_F_ADDDATA]))
				{
					EX_RTCTimeDef clock;
				
					clock.time.Seconds = 0x00;
					clock.time.Minutes = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+6];
					clock.time.Hours = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+5];
					clock.time.Days = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+3];
					clock.time.WeekDays = ConvertWeekDays(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+4]);	
					clock.time.Mounth = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2];
					clock.time.Years = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]; 
				
					SetEXRTCTime(&clock.Timebuffer[0]);
				}
			}
#endif 
#endif
		case EV_GET_TIME:
			TimeDataSet(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA]);
			break;

		case F0_ENCRYPTION_GET_CHECKSUM:
			PackTxEncryptionKeyExchange(&gbPackRxDataCopiedBuf[4]);
			break;

		case F0_ENCRYPTION_KEY_CHANGE:
			PackRxEncryptionKeyExchangeCheck(&gbPackRxDataCopiedBuf[4]);
			break;

		default:
			break;
	}			

	return (RetVal);
}




BYTE PackRxCmdProcess(void)	
{
	BYTE RetVal = 0;
	BYTE bTmp;
	BYTE bSource;
	BYTE bTmpArray[16];

	bSource = gbPackRxDataCopiedBuf[PACK_F_SOURCE];

	switch(gbPackRxDataCopiedBuf[PACK_F_EVENT])
	{
		// 해당 명령이 Event로 오는 경우는 없지만, 
		//	현재 생산지그팩이 Event로 잘못 응답하고 있고, 실제 Event로 온다고 해서 문제될 것은 없어 해당 내용 추가
		case EV_DEVICE_CHK :
			if(bSource == ES_MODULE_EVENT_NOACK)
			{
				PackConnectionStart();
			}
			else
			{
				bTmp = PackTypeCheck(gbPackRxDataCopiedBuf[PACK_F_LOCKTYPE+1]);
				if(bTmp != PACK_TYPE_JIG)
				{
					PackDeviceCheck();
				}
			}
			RetVal = 1;
			break;

		case F0_NETWORK_REGISTER_EVENT: 	// 0x21 명령  --------------------------------------------------------------------------
			if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA] == 0x00)
			{
				SetPackProcessResult(PACK_HCP_JOIN_UNJOIN_OK);
			}
			else if(gbPackRxDataBuf[4] == 0xFF)
			{
				SetPackProcessResult(PACK_HCP_JOIN_UNJOIN_FAIL);
			}

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
			RetVal = 1;
			break;

		case EV_LOCK_LOOP_TEST:
			// 모듈 테스트를 위한 명령이라 무조건 ACK 처리하기 위해
			if(gbModuleMode == 0)
			{
				gbModuleMode = PACK_ID_TEST_CONFIRMED;
			}

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], &gbPackRxDataCopiedBuf[PACK_F_ADDDATA], gbPackRxDataCopiedBuf[PACK_F_LENGTH_ADDDATA]);

			if(gbModuleMode == PACK_ID_TEST_CONFIRMED)
			{
				gbModuleMode = 0;
			}
			RetVal = 1;
			break;			

		case F0_GET_PRODUCT_DATA:		// 0x1B 명령  -------------------------------------------------------------------------------
			bTmpArray[0] = _PTODUCT_TYPE_ID1;
			bTmpArray[1] = _PTODUCT_TYPE_ID2;	//RomRead(&gtbRomReadBuf[1],(WORD)LOCK_TYPE,1);
			bTmpArray[2] = _PTODUCT_ID1;
			bTmpArray[3] = _PTODUCT_ID2;
			bTmpArray[4] = 0x00;
			bTmpArray[5] = FW1_MAIN;
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 6);
			RetVal = 1;
			break;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29) 
		case F0_GET_LOCKFUNCTIONS:
			LockFunctionsLoad(bTmpArray);
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray,13);
			RetVal = 1;
			break;
#endif 
			
	}

	return (RetVal);
}





#ifdef	DDL_CFG_RFID
static void PackAdd1UserCard(BYTE *pCardUidData, BYTE CardUidLength, WORD wUserSlot);
static void PackDeleteUserCard(WORD wUserSlot);
static void PackGetUserCard(WORD wUserSlot);
static void PackGetSupportedCardNumber(void);
#endif

#ifdef	DDL_CFG_DS1972_IBUTTON
static void PackDeleteUserDS1972_Ibutton(WORD wUserSlot);
#endif 

BYTE LockStsCheck(void);
void DoorStsCheck(BYTE *bTmpArray);
static void PackAdd1UserCode(BYTE *pPincodeData, BYTE bPincodeLength, BYTE bUserStatus, WORD wUserSlot);
static void PackDeleteUserCode(WORD wUserSlot);
static void PackGetUserCode(WORD wUserSlot);
void SetUserStatus(WORD wUserSlot, BYTE bUserStatus);
BYTE GetUserStatus(WORD wUserSlot);



BYTE PackRxCmdSetSchedule(BYTE* pScheduleData, WORD SlotNumber);
BYTE PackRxCmdGetSchedule(BYTE* pScheduleData, WORD SlotNumber);
BYTE PackRxCmdEnableUserSchedule(BYTE Status, WORD SlotNumber);




void PackRxExtraParameterSet(BYTE bPara, BYTE bValue)
{
	switch(bPara)
	{
		case PA_RELOCK_TIME:								// Auto Relock time setting
			RomWrite(&bValue, (WORD)RELOCK_TIME, 1);
			break;

		case PA_WRONG_CODE_ENTRY_LIMIT: 					// Wrong code limit
			RomWrite(&bValue, (WORD)WRONG_CODE_ENTRY_LIMIT, 1);
			break;

		case PA_SHUT_DOWN_TIME: 							// shutdown time setting
			RomWrite(&bValue, (WORD)SHUT_DOWN_TIME, 1);
			break;
			
		case PA_OPERATING_MODE: 							// operating mode
			RomWrite(&bValue, (WORD)OPERATING_MODE, 1);
			break;

		case PA_ONE_TOUCH_LOCKING:							// One Touch Locking
			RomWrite(&bValue, (WORD)ONE_TOUCH_LOCKING, 1);
			break;

		case PA_PRIVACY_BUTTON: 							// Privacy Button
			RomWrite(&bValue, (WORD)PRIVACY_BUTTON, 1);
			break;

		case PA_LOCK_STATUS_LED:							// Lock Status LED 0x00 = OFF, 0xFF = ON
#ifdef DDL_LED_DISPLAY_SELECT //led display 결정 			
			LedDisplayEnableStateSet(bValue);				//YALE ACCESS가 적용 되면서 Sandman에서는 LED Display를 실행 할지 말지를 결정 하는 용도로 변경 함			
#else //DDL_LED_DISPLAY_SELECT #else
			RomWrite(&bValue, (WORD)LOCK_STATUS_LED, 1);
#endif //DDL_LED_DISPLAY_SELECT #endif

			break;
	}	
}



void PackRxExtraParameterGet(BYTE bPara, BYTE *bValue)
{
	switch(bPara)
	{
		case PA_RELOCK_TIME:								// Auto Relock time setting
			RomRead(bValue, (WORD)RELOCK_TIME, 1);
			break;

		case PA_WRONG_CODE_ENTRY_LIMIT: 					// Wrong code limit
			RomRead(bValue, (WORD)WRONG_CODE_ENTRY_LIMIT, 1);
			break;

		case PA_SHUT_DOWN_TIME: 							// shutdown time setting
			RomRead(bValue, (WORD)SHUT_DOWN_TIME, 1);
			break;
			
		case PA_OPERATING_MODE: 							// operating mode
			RomRead(bValue, (WORD)OPERATING_MODE, 1);
			break;

		case PA_ONE_TOUCH_LOCKING:							// One Touch Locking
			RomRead(bValue, (WORD)ONE_TOUCH_LOCKING, 1);
			break;

		case PA_PRIVACY_BUTTON: 							// Privacy Button
			RomRead(bValue, (WORD)PRIVACY_BUTTON, 1);
			break;

		case PA_LOCK_STATUS_LED:							// Lock Status LED
			RomRead(bValue, (WORD)LOCK_STATUS_LED, 1);
			break;
	}	
}



void PackRxPermittedCmdProcess(void)		// WGA6의 TB_PackRxCmdProcess 함수와 유사
{
	WORD	wUserSlot;
	BYTE		bTmp, bTmpArray[100];
//	char		*tmpStr;
//	BYTE bUserSlot;
	BYTE bUserStatus;
	BYTE TBTmpBYTE;	
	BYTE bCnt;
#ifdef	USED_SECOND_MOTOR //모터 두개 대응(Secure mode 포함)
	BYTE bTemp;
#endif	//USED_SECOND_MOTOR #endif

	// 모듈의 ID가 저장된 것이 아니면 아래 내용은 동작하지 않음. 
	if(gbModuleMode != PACK_ID_CONFIRMED)		return;

	switch(gbPackRxDataCopiedBuf[PACK_F_EVENT])
	{
//----------------------------------------------------------------------------------------- iRevo Module Only Start
		case EV_OUTER_FORCED_LOCK_SET:
			DDLStatusFlagSet(DDL_STS_OUTLOCK);

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
			break;
					
		case EV_OUTER_FORCED_LOCK_REL:
			DDLStatusFlagClear(DDL_STS_OUTLOCK);
		   
		   	PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
			break;					

#ifdef	DDL_CFG_RFID
		case EV_ADD_1_USER_CARD:
			if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA] != CREDENTIALTYPE_CARD)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 	// Nack 송신 준비 
				break;
			}

			wUserSlot = (WORD)gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]; // User Slot
			if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
			{
				if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCARD_NUMBER) ) // User slot이 안맞으면
				{
					PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 		// Nack 송신 준비 
					break;
				}
			}
			else									// Basic New Normal Mode
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 	// Nack 송신 준비 
				break;
			}
			bUserStatus = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2];

			if(bUserStatus != USER_STATUS_OCC_ENABLED && bUserStatus != USER_STATUS_OCC_DISABLED && bUserStatus != USER_STATUS_NON_ACCESS_USER) 
			{																// User status는 occupied/enabled, occupied/disabled, Non-access user만 허용
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 	// Nack 송신 준비 
				break;
			}

			if((gbPackRxDataCopiedBuf[PACK_F_LENGTH_ADDDATA] < (MIN_CARDUID_LENGTH+3)) 
				|| (gbPackRxDataCopiedBuf[PACK_F_LENGTH_ADDDATA] > (MAX_CARDUID_LENGTH+3)))	
			{																// 카드 UID 등록이 가능한 BYTE만 허용
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 	// Nack 송신 준비 
				break;
			}

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
			PackAdd1UserCard(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+3], (gbPackRxDataCopiedBuf[PACK_F_LENGTH_ADDDATA]-3), wUserSlot);
			break;
#endif	// DDL_CFG_RFID

		case EV_DELETE_1_USER_CARD:
			if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA] == CREDENTIALTYPE_CARD)
			{
#ifdef	DDL_CFG_RFID
				wUserSlot = (WORD)gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]; // User Slot
				if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
				{
					if(( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCARD_NUMBER) ) && (wUserSlot != 0xFF)) // User slot이 안맞으면
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 		// Nack 송신 준비 
						break;
					}
				}
				else									// Basic New Normal Mode
				{
					// BLE-N에서 카드 전체 삭제 요청 기능이 있어 추가
					if(wUserSlot != 0xFF)
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 	// Nack 송신 준비 
						break;
					}
				}

				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
				PackDeleteUserCard(wUserSlot);
#else	// DDL_CFG_RFID
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_NOT_SUPPORTED_MESSAGE,0x00); 	// Nack 송신 준비 
#endif	// DDL_CFG_RFID
			}
			else if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA] == CREDENTIALTYPE_FINGERPRINT)
			{
#if	defined(DDL_CFG_FP) && defined(BLE_N_SUPPORT)
				if(gfPackTypeiRevoBleN || gfPackTypeCBABle)
				{
					if(gbManageMode == _AD_MODE_SET && gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1] != 0xFF) // Basic New Advanced Mode //hyojoon_20160831 PFM-3000 add  advanced mode 0xFF all slot 의 경우 예외 
					{
						gPinInputKeyFromBeginBuf[0] = ConvertByteToInputKey(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);
						ModeGotoOneFingerDeleteByBleN();
					}
					else
					{
						ModeGotoAllFingerDeleteByBleN();
					}
					PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
				}
				else
				{
					PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 	// Nack 송신 준비 
				}
#else
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_NOT_SUPPORTED_MESSAGE,0x00); 	// Nack 송신 준비 
#endif
			}
			else if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA] == CREDENTIALTYPE_GMTOUCHKEY)
			{
#if (defined (DDL_CFG_DS1972_IBUTTON) || defined (DDL_CFG_IBUTTON)) && defined(BLE_N_SUPPORT)

				if(gbModuleProtocolVersion >= 0x30)
				{
					wUserSlot = ((WORD)(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1] << 8 ) | (WORD)gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2] ); // User Slot
#if	defined (DDL_CFG_DS1972_IBUTTON)
					if( wUserSlot < 1 || (wUserSlot > SUPPORTED_DS1972KEY_NUMBER && wUserSlot != RET_2BYTE_MASTER_CODE_MATCH) ) // User slot이 안맞으면
#elif defined (DDL_CFG_IBUTTON)
					if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERKEY_NUMBER && wUserSlot != RET_2BYTE_MASTER_CODE_MATCH) ) // User slot이 안맞으면
#endif
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00);  	// Nack 송신 준비 
						break;
					}
				}
				else
				{
					wUserSlot = (WORD)gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]; // User Slot
					if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
					{
#if	defined (DDL_CFG_DS1972_IBUTTON)
						if(( wUserSlot < 1 || (wUserSlot > SUPPORTED_DS1972KEY_NUMBER) ) && (wUserSlot != 0xFF)) // User slot이 안맞으면
#elif defined (DDL_CFG_IBUTTON)
						if(( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERKEY_NUMBER) ) && (wUserSlot != 0xFF)) // User slot이 안맞으면
#endif
						{
							PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00);  		// Nack 송신 준비 
							break;
						}
					}
					else									// Basic New Normal Mode
					{
						// BLE-N에서 카드 전체 삭제 요청 기능이 있어 추가
						if(wUserSlot != 0xFF)
						{
							PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00);  	// Nack 송신 준비 
							break;
						}
					}
				}

				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);

#if	defined (DDL_CFG_DS1972_IBUTTON)
				PackDeleteUserDS1972_Ibutton(wUserSlot);
#elif defined (DDL_CFG_IBUTTON)
				PackDeleteUserGMTouchKey(wUserSlot);
#endif

#else	// (defined (DDL_CFG_DS1972_IBUTTON) || defined (DDL_CFG_IBUTTON)) && defined(BLE_N_SUPPORT)	
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_NOT_SUPPORTED_MESSAGE,0x00); 	// Nack 송신 준비 
#endif	// (defined (DDL_CFG_DS1972_IBUTTON) || defined (DDL_CFG_IBUTTON)) && defined(BLE_N_SUPPORT)
			}
			else if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA] == CREDENTIALTYPE_BLE)
			{
#ifdef	BLE_N_SUPPORT
				if(gfPackTypeiRevoBleN)
				{
					if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
					{
						gPinInputKeyFromBeginBuf[0] = ConvertByteToInputKey(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);
						ModeGotoOneRemoconDeleteByBleN();
					}
					else
					{
						ModeGotoRemoconDeleteByBleN();
					}
					PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
				}
				else
				{
					PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_NOT_SUPPORTED_MESSAGE,0x00); 	// Nack 송신 준비 
				}
#else	// _BLE_N_SUPPORT	
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_NOT_SUPPORTED_MESSAGE,0x00); 	// Nack 송신 준비 
#endif	// _BLE_N_SUPPORT
			}
			else
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 	// Nack 송신 준비 
			}
			break;

#ifdef	DDL_CFG_RFID
		case EV_GET_1_USER_CREDENTIAL:
			if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA] != CREDENTIALTYPE_CARD)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 	// Nack 송신 준비 
				break;
			}

			wUserSlot = (WORD)gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]; // User Slot
			if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCARD_NUMBER) ) // User slot이 안맞으면
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
				break;
			}

			PackGetUserCard(wUserSlot);
			break;

		case EV_GET_NUMBER_OF_CARDS_SPT:
			PackGetSupportedCardNumber();
			break;
#endif	// DDL_CFG_RFID

//----------------------------------------------------------------------------------------- iRevo Module Only End


//----------------------------------------------------------------------------------------- Protocol V1 Only Start
		case EV_SET_USER_SCHEDULE:
			if(PackRxCmdSetSchedule(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1], (WORD)gbPackRxDataCopiedBuf[PACK_F_ADDDATA]) == 0)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 	// Nack 송신 준비 
				break;
			}

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
			break;
			

		case EV_GET_USER_SCHEDULE:
			if(PackRxCmdGetSchedule(gbPackTxAddNone, (WORD)gbPackRxDataCopiedBuf[PACK_F_ADDDATA]) == 0)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 	// Nack 송신 준비 
				break;
			}

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 5);
			break;

		case EV_ENABLE_USER_SCHEDULE:
			if(PackRxCmdEnableUserSchedule(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1], (WORD)gbPackRxDataCopiedBuf[PACK_F_ADDDATA]) == 0)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 	// Nack 송신 준비 
				break;
			}

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
			break;
			
//----------------------------------------------------------------------------------------- Protocol V1 Only End


		case F0_LOCK_OPERATION:		// 0x10 명령  -----------------------------------------------------------------------------------
//			if((gbMotorMode != 0) || (gbMainMode == MODE_HANDING_LOCK))		// 모터 도는 중간에 다시 명령 받으면 오동작 하는 것을 방지하기 위해
//			{															// 모터 도는 중간에 명령 받으면 명령을 무시해 버림.
//				PackVariableInitial();
//				gbPackRmcReCnt = 0;
//				break;
//			}

//			PackTx_MakeAndSendPacketNow를 쓰면 Ack 보내는 시간이 20 -> 12ms로 줄어들지만 
//			PackTx_MakeAndSendPacketNow 함수는 실행시간이 2ms를 초과하는 경우가 많으므로 가급적 BasicCount 업데이트 주기인 2m를 깨지 않도록 
//			명령처리시간이 특별히 긴 경우에만 사용하는 것이 좋다.

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
//			PackTx_MakeAndSendPacketNow(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
			ModeGotoPackLockOperation();

			// buffer 를 다 초기화 하지 말고 안 쓰는 놈만 땡빵 
			gbInnerPackRxDataCopiedBuf[4] = 0xAB;			
			break;


		case F0_GET_LOCK_STATUS:			// 0x11 명령  -------------------------------------------------------------------------------
			bTmpArray[0] = LockStsCheck();

#ifdef	P_SNS_EDGE_T			// Edge sensor			
			if(bTmpArray[0] == 0x00 && gfDoorSwOpenState)		bTmpArray[0] = 0x00; 	// Motor Unlock 이고  Door Open 이면 
			else if(bTmpArray[0] == 0x00 && !gfDoorSwOpenState)	bTmpArray[0] = 0x12; 	// Motor Unlock 이고  Door Close 이면 
#ifdef	USED_SECOND_MOTOR //모터 두개 대응(Secure mode 포함)
			if(bTmpArray[0] == 0xFF)	//Front motor가 Close 상태일때 
			{
				bTemp = MotorSensorCheck_Second();	//두번째(Main) 모터 상태를 읽어 
				if(bTemp == SENSOR_CLOSE_STATE || bTemp == SENSOR_CLOSELOCK_STATE || bTemp == SENSOR_CLOSECEN_STATE) //모터 상태가 닫힘 이면 
				{
					bTmpArray[0] = 0x04;	//Secure mode 상태로 버퍼를 채운다.
				}
				else
				{
					if(bTmpArray[0] == 0xFF && gfDoorSwOpenState)	bTmpArray[0] = 0x21;	// Motor Lock 이고	Door Open 이면 
					else if(bTmpArray[0] == 0xFF && !gfDoorSwOpenState) bTmpArray[0] = 0xFF;	// Motor Lock 이고	Door Close 이면 
				}
			}			
#else	//USED_SECOND_MOTOR #else		
			else if(bTmpArray[0] == 0xFF && gfDoorSwOpenState)	bTmpArray[0] = 0x21;	// Motor Lock 이고	Door Open 이면 
			else if(bTmpArray[0] == 0xFF && !gfDoorSwOpenState) bTmpArray[0] = 0xFF;	// Motor Lock 이고	Door Close 이면 
#endif	//USED_SECOND_MOTOR #endif


			bTmp = 1;				
#else
			bTmp = 1;				
			if((GetRemainingAutoRelockTime() != 0) 
				&& (!gfPackTypeiRevo) && (!gfPackTypeiRevoBleN))
			{
				bTmpArray[0] = 0x01;
				bTmpArray[1] = (BYTE)GetRemainingAutoRelockTime();
				bTmp = 2;				
			}
#endif 
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, bTmp);
//			if(tmpArray[0] == 0x01) 		// Unlock with relock time
//			{
//				if(gbMotorRetryStep ==2)			// Locking Jam 발생시 unlocked로 보고함 
//				{
//					tmpArray[0] = 0x00;
//					PackTxAddDataSend(tmpArray, 1);
//				}
//				else
//				{
//					tmpArray[1] = gbLockTime1s;
//					PackTxAddDataSend(tmpArray, 2); 		
//				}
//			}
//			else
//			{
//				PackTxAddDataSend(tmpArray, 1);
//			}
			break;


		case F0_GET_DOOR_STATUS:			// 0x2B 명령  -------------------------------------------------------------------------------
			DoorStsCheck(bTmpArray);
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 6);
			break;


		case F0_ADD_1_USER_CODE:			// 0x12 또는 0x82 명령  ----------------------------------------------------------------------
		case F0_ADD_1_USER_CODE_EX:		

			if(gbModuleProtocolVersion >= 0x30)
			{
				wUserSlot = ((WORD)(gbPackRxDataCopiedBuf[4] << 8 ) | (WORD)gbPackRxDataCopiedBuf[5] ); // User Slot
				if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCODE_NUMBER && wUserSlot != RET_2BYTE_MASTER_CODE_MATCH) ) // User slot이 안맞으면
				{
					PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 	// Nack 송신 준비 
					break;
				}
				bUserStatus = gbPackRxDataCopiedBuf[6];
			}
			else
			{
				wUserSlot = (WORD)gbPackRxDataCopiedBuf[4]; // User Slot
				if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
				{
					if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCODE_NUMBER && wUserSlot != RET_MASTER_CODE_MATCH && wUserSlot != RET_TEMP_CODE_MATCH) ) // User slot이 안맞으면
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
						break;
					}
				}
				else									// Basic New Normal Mode
				{
					if( wUserSlot != RET_TEMP_CODE_MATCH && wUserSlot != 0x01 && wUserSlot != 0x02 ) // User slot 0x01, 0x02, 0xf0만 허용
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
						break;
					}
				}
				bUserStatus = gbPackRxDataCopiedBuf[5];
			}
		
			if(bUserStatus != USER_STATUS_OCC_ENABLED && bUserStatus != USER_STATUS_OCC_DISABLED && bUserStatus != USER_STATUS_NON_ACCESS_USER)	
			{																// User status는 occupied/enabled, occupied/disabled, Non-access user만 허용
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ;		// Nack 송신 준비 
				break;
			}

#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
			if((gbPackRxDataCopiedBuf[3] < (gPinInputMinLength+2)) || (gbPackRxDataCopiedBuf[3] > (MAX_PIN_LENGTH+2)))	
#else
#ifdef	P_VOICE_RST  // defined (DDL_CFG_SUPPORT_CHINESE)
			if((gbPackRxDataCopiedBuf[3] < (gPinInputMinLength+2)) || (gbPackRxDataCopiedBuf[3] > (MAX_PIN_LENGTH+2)))	
#else 
			if((gbPackRxDataCopiedBuf[3] < (MIN_PIN_LENGTH+2)) || (gbPackRxDataCopiedBuf[3] > (MAX_PIN_LENGTH+2)))	
#endif 				
#endif
			{																// 비밀번호 등록이 가능한 자리수만 허용
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ;		// Nack 송신 준비 
				break;
			}

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);

			if(gbModuleProtocolVersion >= 0x30)
			{
				bTmp = gbPackRxDataCopiedBuf[PACK_F_LENGTH_ADDDATA] - 3;
			}
			else
			{
				bTmp = gbPackRxDataCopiedBuf[PACK_F_LENGTH_ADDDATA] - 2;
			}

			PackAdd1UserCode(&gbPackRxDataCopiedBuf[PACK_F_LENGTH_ADDDATA + 3], bTmp, bUserStatus, wUserSlot);
			break;


		case F0_DELETE_USER_CODE:		// 0x13 또는 0x83 명령  ---------------------------------------------------------------------
		case F0_DELETE_USER_CODE_EX:				

			if(gbModuleProtocolVersion >= 0x30)
			{
				wUserSlot = ((WORD)(gbPackRxDataCopiedBuf[4] << 8 ) | (WORD)gbPackRxDataCopiedBuf[5] ); // User Slot
				if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCODE_NUMBER && wUserSlot != 0xffff) ) // User slot이 안맞으면
				{
					PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 	// Nack 송신 준비 
					break;
				}
			}
			else
			{
				wUserSlot = (WORD)gbPackRxDataCopiedBuf[4]; // User Slot
				if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
				{
					if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCODE_NUMBER && wUserSlot != RET_TEMP_CODE_MATCH && wUserSlot != 0xFF) ) // User slot이 안맞으면
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
						break;
					}
				}
				else									// Basic New Normal Mode
				{
					if( wUserSlot != RET_TEMP_CODE_MATCH && wUserSlot != 0x01 &&wUserSlot != 0x02 && wUserSlot != 0xFF ) // User slot 0x01, 0x02, 0xf0, 0xff만 허용
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
						break;
					}
					if(wUserSlot == 0x01 || wUserSlot == 0xFF)	FactoryResetSet(FACTORYRESET_DONE_BY_RF);	//2016년11월14일 Normal Mode에서 User Code 또는 전체 삭제를 할 경우 문을 닫는 현상을 막기 위해 gbFactoryReset flag설정 by심재철
				}
			}

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
			PackDeleteUserCode(wUserSlot);
			break;


		case F0_GET_1_USER_CODE:			// 0x15 또는 0x85 명령  ---------------------------------------------------------------------
		case F0_GET_1_USER_CODE_EX:

			if(gbModuleProtocolVersion >= 0x30)
			{
				wUserSlot = ((WORD)(gbPackRxDataCopiedBuf[4] << 8 ) | (WORD)gbPackRxDataCopiedBuf[5] ); // User Slot
				if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCODE_NUMBER && wUserSlot != RET_2BYTE_MASTER_CODE_MATCH) ) // User slot이 안맞으면
				{
					PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 	// Nack 송신 준비 
					break;
				}
			}
			else
			{
				wUserSlot = (WORD)gbPackRxDataCopiedBuf[4];	// User Slot
				if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
				{
					if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCODE_NUMBER && wUserSlot != RET_MASTER_CODE_MATCH && wUserSlot != RET_TEMP_CODE_MATCH) ) // User slot이 안맞으면
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
						break;
					}
				}
				else									// Basic New Normal Mode
				{
					if( wUserSlot != RET_TEMP_CODE_MATCH && wUserSlot != 0x01 && wUserSlot != 0x02 ) // User slot 0x01, 0x02, 0xf0만 허용
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
						break;
					}
				}
			}
			PackGetUserCode(wUserSlot);
			break;


		case F0_SET_1_USER_CODE_STATUS: // 0x16 또는 0x86 명령  -----------------------------------------------------------------------
		case F0_SET_1_USER_CODE_STATUS_EX:
			if(gbModuleProtocolVersion >= 0x30)
			{
				wUserSlot = ((WORD)(gbPackRxDataCopiedBuf[4] << 8 ) | (WORD)gbPackRxDataCopiedBuf[5] ); // User Slot
				if( wUserSlot < 1 || wUserSlot > SUPPORTED_USERCODE_NUMBER ) // User slot이 안맞으면
				{
					PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 	// Nack 송신 준비 
					break;
				}
				bUserStatus = gbPackRxDataCopiedBuf[6];
			}
			else
			{
				wUserSlot = (WORD)gbPackRxDataCopiedBuf[4]; // User Slot
				if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
				{
					if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCODE_NUMBER) ) // User slot이 안맞으면
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
						break;
					}
				}
				else									// Basic New Normal Mode
				{
					if( wUserSlot != 0x01 &&wUserSlot != 0x02 ) // User slot 0x01, 0x02만 허용
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
						break;
					}
				}
				bUserStatus = gbPackRxDataCopiedBuf[5];
			}

			if(bUserStatus != USER_STATUS_OCC_ENABLED && bUserStatus != USER_STATUS_OCC_DISABLED && bUserStatus != USER_STATUS_NON_ACCESS_USER) 
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 	// Nack 송신 준비 
				break;
			}
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], gbPackTxAddNone, 0);
			SetUserStatus(wUserSlot, bUserStatus);			// User status 설정
			break;


		case F0_GET_1_USER_CODE_STATUS: // 0x17 또는 0x87 명령  ------------------------------------------------------------------------
		case F0_GET_1_USER_CODE_STATUS_EX: 

			if(gbModuleProtocolVersion >= 0x30)
			{	
				wUserSlot = ((WORD)(gbPackRxDataCopiedBuf[4] << 8 ) | (WORD)gbPackRxDataCopiedBuf[5] ); // User Slot
				if( wUserSlot < 1 || wUserSlot > SUPPORTED_USERCODE_NUMBER ) // User slot이 안맞으면
				{
					PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 	// Nack 송신 준비 
					break;
				}
			}
			else
			{
				wUserSlot = (WORD)gbPackRxDataCopiedBuf[4]; // User Slot
				if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
				{
					if( wUserSlot < 1 || (wUserSlot > SUPPORTED_USERCODE_NUMBER) ) // User slot이 안맞으면
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
						break;
					}
				}
				else									// Basic New Normal Mode
				{
					if( wUserSlot != 0x01 &&wUserSlot != 0x02 ) // User slot 0x01, 0x02만 허용
					{
						PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); ; 		// Nack 송신 준비 
						break;
					}
				}
			}
			bUserStatus = GetUserStatus(wUserSlot);
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], &bUserStatus, 1);
			break;

		case F0_GET_BATTERY_LEVEL:		// 0x1A 명령  -------------------------------------------------------------------------------
			BatteryCheck_No_LED_control();

			if(gfLowBattery)
			{
				PackAlarmReportLowBatterySet();

				// 저전압일 경우 무조건 10% 전송
				bTmpArray[0] = PERCENT_OF_LOWBATTERY;
			}
			else
			{
				bTmpArray[0] = MakePercetageOfBatteryLevel();
			}

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 1);
			break;

		case F0_GET_NUMBER_OF_USERS_SUPPORTED:	// 0x8C 명령  ---------------------------------------------------------------------
		case F0_GET_NUMBER_OF_USERS_SUPPORTED_EX:	

			if(gbModuleProtocolVersion >= 0x30)
			{
				if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
				{
					bTmpArray[0] = (BYTE)((SUPPORTED_USERCODE_NUMBER >> 8) & 0xFF);
					bTmpArray[1] = (BYTE)(SUPPORTED_USERCODE_NUMBER & 0xFF);
				}
				else
				{
					bTmpArray[0] = 0x00;
					bTmpArray[1] = 2;
				}
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 2);
			}
			else
			{
				if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
				{
					if(gfPackTypeZBZWModule)
						bTmpArray[0] = 30U;
					else
						bTmpArray[0] = SUPPORTED_USERCODE_NUMBER;
				}
				else
				{
					bTmpArray[0] = 2;
				}
				
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 1);
			}
			break;

		case F0_GET_SERIAL_NUMBER:			// 0x22 명령  --------------------------------------------------------------------------
//			Serial number is a unique ID number programmed into the main board
			//임시 버전.. 수정 필요
			bTmpArray[0] = 0;
			bTmpArray[1] = 0;
			bTmpArray[2] = 0;
			bTmpArray[3] = 0;

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 4);
			break;

		case F0_SET_CONFIGURATION_PARAMETER:	// 0x18 명령  ---------------------------------------------------------------------------
			TBTmpBYTE = 0;			// 올바른 정보인지 확인하는 변수
			switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA])
			{
				case PA_SILENT_MODE_ONOFF: 								// Buzzer Silent Mode ON/OFF
#ifdef	LOCKSET_VOLUME_SET
#if 0 // #if defined (N_PROTOCOL_V2)
	
					switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1])
					{
						case 1:
							VolumeSetting(VOL_HIGH);
							break;
							
						case 2:
							VolumeSetting(VOL_LOW);
							break;

						case 3:
							VolumeSetting(VOL_SILENT);
							break;

						default:
							break;
					}
#else
					switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1])
					{
						case 3:
							VolumeSetting(VOL_HIGH);
							break;
							
						case 2:
							VolumeSetting(VOL_LOW);
							break;

						case 1:
							VolumeSetting(VOL_SILENT);
							break;

						default:
							break;
					}
	#endif
#endif
					break;

				case PA_AUTO_RELOCK_ONOFF: 								// Auto Relock ON/OFF
#ifdef	LOCKSET_AUTO_RELOCK
					switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1])
					{
						case 0:
							AutoRelockDisable();
							break;
							
						case 255:
							AutoRelockEnable();
							AutoLockTimeAndAutoReLockTimeDefaultSetAndWrite();
							break;

						default:
							break;
					}
#endif
					break;
/*
				case PA_RELOCK_TIME: 							// Auto Relock time setting
					TBTmpBYTE = 0x0f;
					break;

				case PA_WRONG_CODE_ENTRY_LIMIT: 							// Wrong code limit
					TBTmpBYTE = 0x0f;
					break;

*/
				case PA_LANGUAGE: 							// Language Setting
#ifdef	LOCKSET_LANGUAGE_SET				
					switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1])
					{
#if	defined (DDL_CFG_LANGUAGE_SET_TYPE1) || defined (DDL_CFG_LANGUAGE_SET_TYPE3)
						case LANGUAGE_KOREAN: 
						case LANGUAGE_CHINESE: 
						case LANGUAGE_ENGLISH: 
						case LANGUAGE_TAIWANESE: 							
							LanguageSetting(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);
							break;
							
#elif defined DDL_CFG_LANGUAGE_SET_TYPE2
						case LANGUAGE_KOREAN: 
						case LANGUAGE_CHINESE: 
						case LANGUAGE_ENGLISH: 
						case LANGUAGE_SPANISH: 						
						case LANGUAGE_PORTUGUESE: 
						case LANGUAGE_TAIWANESE: 													
							LanguageSetting(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);
							break;
#endif 							
						default:
							break;
					}
#endif
					break;

/*
				case PA_SHUT_DOWN_TIME: 							// shutdown time setting
					TBTmpBYTE = 0x0f;
					break;

				case PA_OPERATING_MODE: 							// operating mode
					TBTmpBYTE = 0x0f;
					break;

				case PA_ONE_TOUCH_LOCKING:							// One Touch Locking
					TBTmpBYTE = 0x0f;
					break;


				case PA_PRIVACY_BUTTON:							// Privacy Button
					TBTmpBYTE = 0x0f;
					break;

				case PA_LOCK_STATUS_LED:								// Lock Status LED
					TBTmpBYTE = 0x0f;
					break;
*/
				case PA_RESET_TO_FACTORY_DEFAULT:								// Reset To Factory De-faults
					switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1])
					{
						case 1:
						case 2:
							if(gbModuleProtocolVersion >= 0x30 || gbInnerModuleProtocolVersion >= 0x30)
							{							
								ModeGotoAllDataReset(0x03);
							}
							else
							{
								FactoryResetSet(FACTORYRESET_REQ_BY_RF);	// Factory Reset by RF	
							}
							bTmpArray[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
							bTmpArray[1] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1];
							PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 2);
							return; // Factory Reset은 0x33 Configuration Report 하지 않는다

						default:
							TBTmpBYTE = 0x0f;
							break;
					}
					break;

				case PA_DOUBLE_CHECK_UNLOCK_LOCK:	//Enable = 0xFF, Disable = 0x00
#if defined(DDL_CFG_TOUCH_OC)
					if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1] ==  0xFF)
					{
						SafeOCButtonSet();
					}
					else if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1] ==  0x00) 
					{
						SafeOCButtonClear();
					}
					else
					{
						TBTmpBYTE = 0x0f;
					}
#else
					TBTmpBYTE = 0x0f;
					
#endif
					break;

				case PA_EMERGENCY_UNLOCK:	//Enable = 0xFF, Disable = 0x00
#if defined(DDL_CFG_EMERGENCY_LOCK)
					if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]) ==	0xFF)
					{
							//미구현
					}
					else if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]) ==  0x00)
					{
							//미구현 
					}
					else
					{
						TBTmpBYTE = 0x0f;
					}
#else
					TBTmpBYTE = 0x0f;
#endif
					break;			


// Z-Wave CTT 테스트 과정에서 문제가 되어 사용하지 않는 Configuration Parameter라고 설정되고 응답하도록 수정
				case PA_RELOCK_TIME: 							// Auto Relock time setting
				case PA_WRONG_CODE_ENTRY_LIMIT: 							// Wrong code limit
				case PA_SHUT_DOWN_TIME: 							// shutdown time setting
				case PA_OPERATING_MODE: 							// operating mode
				case PA_ONE_TOUCH_LOCKING:							// One Touch Locking
				case PA_PRIVACY_BUTTON: 						// Privacy Button
				case PA_LOCK_STATUS_LED:								// Lock Status LED
					PackRxExtraParameterSet(gbPackRxDataCopiedBuf[PACK_F_ADDDATA], gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);
					break;

				case 0xFF:
#ifdef	LOCKSET_VOLUME_SET
#if 0		//	#if defined (N_PROTOCOL_V2)
					switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1])
					{
						case 1:
							VolumeSetting(VOL_HIGH);
							break;
							
						case 2:
							VolumeSetting(VOL_LOW);
							break;

						case 3:
							VolumeSetting(VOL_SILENT);
							break;

						default:
							TBTmpBYTE = 0x0f;
							break;
					}
#else
					switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1])
					{
						case 3:
							VolumeSetting(VOL_HIGH);
							break;
							
						case 2:
							VolumeSetting(VOL_LOW);
							break;

						case 1:
							VolumeSetting(VOL_SILENT);
							break;

						default:
							TBTmpBYTE = 0x0f;
							break;
					}
	#endif

#endif			//LOCKSET_VOLUME_SET						

#ifdef	LOCKSET_AUTO_RELOCK
					switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2])
					{
						case 0:
							AutoRelockDisable();
							break;
							
						case 255:
							AutoRelockEnable();
							AutoLockTimeAndAutoReLockTimeDefaultSetAndWrite();
							break;

						default:
							TBTmpBYTE = 0x0f;
							break;
					}
#endif				

#ifdef	LOCKSET_LANGUAGE_SET				
					switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+5])
					{
#if	defined (DDL_CFG_LANGUAGE_SET_TYPE1) || defined (DDL_CFG_LANGUAGE_SET_TYPE3)
						case LANGUAGE_KOREAN: 
						case LANGUAGE_CHINESE: 
						case LANGUAGE_ENGLISH: 
						case LANGUAGE_TAIWANESE:							
							LanguageSetting(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+5]);
							break;
							
#elif defined DDL_CFG_LANGUAGE_SET_TYPE2
						case LANGUAGE_KOREAN: 
						case LANGUAGE_CHINESE: 
						case LANGUAGE_ENGLISH: 
						case LANGUAGE_SPANISH:						
						case LANGUAGE_PORTUGUESE: 
						case LANGUAGE_TAIWANESE:													
							LanguageSetting(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+5]);
							break;
#endif 							
						default:
							TBTmpBYTE = 0x0f;
							break;
					}
#endif

				default:
					TBTmpBYTE = 0xf0;
					break;
			}

			if(TBTmpBYTE == 0)		// 올바른 정보
			{	
				BYTE bTmpsize = 0x00;
				bTmpArray[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
				bTmpArray[1] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1];

				if(gbModuleProtocolVersion >= 0x30)
				{
					bTmpsize = 2;
				}
				else
				{
					bTmpsize = 0;
				}

				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, bTmpsize);
				
				// 100ms Delay 후 전송
				PackTx_MakePacketSinglePort(F0_CONFIGRATION_REPORT, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 2);
				PackTxWaitTime_EnQueue(10);

				if(gfPackTypeiRevoBleN)
				{
					if(GetTamperProofPrcsStep())
					{
						TamperCountClear();
					}

					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					ModeClear();
				}
				break;
			}
			else				// 올바르지 않은 정보면 NACK 날림
			{
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_NACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 0);
			}
			break;


		case F0_GET_CONFIGURATION_PARAMETER:	// 0x19 명령  -----------------------------------------------------------------------
			bTmp = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];

			TBTmpBYTE = 0;		// 올바른 정보인지 확인하는 변수

			if(bTmp == 1)	//Slient mode
			{
//#ifdef	LOCKSET_VOLUME_SET
#if 0 //	#if defined (N_PROTOCOL_V2)
				bTmpArray[1] = VolumeSettingCheck();
				if(bTmpArray[1] == VOL_LOW)
				{
					bTmpArray[1] = 2;
				}
				else if(bTmpArray[1] == VOL_SILENT)
				{
					bTmpArray[1] = 3;
				}
				else
				{
					bTmpArray[1] = 1;
				}
#else
				bTmpArray[1] = VolumeSettingCheck();
				if(bTmpArray[1] == VOL_LOW)
				{
					bTmpArray[1] = 2;
				}
				else if(bTmpArray[1] == VOL_SILENT)
				{
					bTmpArray[1] = 1;
				}
				else
				{
					bTmpArray[1] = 3;
				}
#endif
			}
			else if(bTmp == 2)	//Aute lock
			{
				bTmpArray[1] = AutoLockSetCheck();
				if(bTmpArray[1] == 1)
				{
					bTmpArray[1] = 255;
				}
				else
				{
					bTmpArray[1] = 0;
				}
			}
			else if(bTmp == 5)	//Language
			{
#ifdef	LOCKSET_LANGUAGE_SET	
				LanguageLoad();
				switch (LanguageCheck())
				{
					case _KOREAN_MODE:
						bTmpArray[1] = 0x01;
						break;
						
					case _SPANISH_MODE:
						bTmpArray[1] = 0x05;
						break;
			
					case _PORTUGUESE_MODE:
						bTmpArray[1] = 0x04;
						break;
			
					case _ENGLISH_MODE:
						bTmpArray[1] = 0x02;
						break;
			
					case _CHINESE_MODE:
						bTmpArray[1] = 0x03;
						break;
			
					case _TAIWANESE_MODE:
						bTmpArray[1] = 0x06;
						break;
			
					default :
						TBTmpBYTE = 0x0f;
						bTmpArray[1] = 0x03;
						break;
				}
#else
				TBTmpBYTE = 0x0f;
#endif
			}
/*
			else if(bTmp == 7)	//Shut down time
			{
				TBTmpBYTE = 0xf0;
			}
			else if(bTmp == 8)	//Operating mode
			{
				TBTmpBYTE = 0xf0;
			}
*/
			else if(bTmp == 9)	//Handing State
			{
				TBTmpBYTE = 0xf0;
			}
/*
			else if(bTmp == 11)	//One touch locking
			{
				TBTmpBYTE = 0xf0;
			}
			else if(bTmp == 12)	//Privacy button
			{
				TBTmpBYTE = 0xf0;
			}
			else if(bTmp ==13)	//Lock Status LED
			{
				TBTmpBYTE = 0xf0;
			}
*/
			else if(bTmp ==15)	//Reset to factory default
			{
				TBTmpBYTE = 0xf0;
			}
			else if(bTmp ==18)	//Door propped time
			{
				TBTmpBYTE = 0xf0;
			}
			else if(bTmp ==19)	//DPS Alarm
			{
				TBTmpBYTE = 0xf0;
			}
			else if(bTmp == 0xFF)
			{
				memset(&bTmpArray[1], 0x00, 13);
			
#if 0 // #if defined (N_PROTOCOL_V2)
				bTmpArray[1] = VolumeSettingCheck();
				if(bTmpArray[1] == VOL_LOW)
				{
					bTmpArray[1] = 2;
				}
				else if(bTmpArray[1] == VOL_SILENT)
				{
					bTmpArray[1] = 3;
				}
				else
				{
					bTmpArray[1] = 1;
				}
#else
				bTmpArray[1] = VolumeSettingCheck();
				if(bTmpArray[1] == VOL_LOW)
				{
					bTmpArray[1] = 2;
				}
				else if(bTmpArray[1] == VOL_SILENT)
				{
					bTmpArray[1] = 1;
				}
				else
				{
					bTmpArray[1] = 3;
				}
#endif
			
				bTmpArray[2] = AutoLockSetCheck();
				if(bTmpArray[2] == 1)
				{
					bTmpArray[2] = 255;
				}
				else
				{
					bTmpArray[2] = 0;
				}
				
				RomRead(&bTmpArray[3], (WORD)RELOCK_TIME, 1);
				RomRead(&bTmpArray[4], (WORD)WRONG_CODE_ENTRY_LIMIT, 1);


#ifdef	LOCKSET_LANGUAGE_SET	
				LanguageLoad();
				switch (LanguageCheck())
				{
					case _KOREAN_MODE:
						bTmpArray[5] = 0x01;
						break;
						
					case _SPANISH_MODE:
						bTmpArray[5] = 0x05;
						break;
			
					case _PORTUGUESE_MODE:
						bTmpArray[5] = 0x04;
						break;
			
					case _ENGLISH_MODE:
						bTmpArray[5] = 0x02;
						break;
			
					case _CHINESE_MODE:
						bTmpArray[5] = 0x03;
						break;
			
					case _TAIWANESE_MODE:
						bTmpArray[5] = 0x06;
						break;
			
					default :
						TBTmpBYTE = 0x0f;
						bTmpArray[5] = 0x03;
						break;
				}
#else 				
// 잘못된 코드 
//				wTmp = LanguageCheck();
//				wTmp = wTmp >> 8;
//				bTmpArray[5] = (BYTE)wTmp;
#endif 				
				
				RomRead(&bTmpArray[7], (WORD)SHUT_DOWN_TIME, 1);
				RomRead(&bTmpArray[8], (WORD)OPERATING_MODE, 1);
				RomRead(&bTmpArray[11], (WORD)ONE_TOUCH_LOCKING, 1);
				RomRead(&bTmpArray[12], (WORD)PRIVACY_BUTTON, 1);
				RomRead(&bTmpArray[13], (WORD)LOCK_STATUS_LED, 1);

				bTmpArray[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 14);
				break;
			}
			else if((bTmp == 3) || (bTmp == 4) || (bTmp == 7) || (bTmp == 8)
				|| (bTmp == 11) || (bTmp == 12) || (bTmp == 13))
			{
				// Z-Wave CTT 테스트 과정에서 문제가 되어 사용하지 않는 Configuration Parameter라고 설정되고 응답하도록 수정
				PackRxExtraParameterGet(gbPackRxDataCopiedBuf[PACK_F_ADDDATA], &bTmpArray[1]);
			}
			else
			{
				TBTmpBYTE = 0x0f;
			}

			if(TBTmpBYTE == 0)
			{
				bTmpArray[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 2);
			}
			else				// 올바르지 않은 정보면 NACK 날림
			{
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_NACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 0);
			}
			break;

		case F0_SET_CONFIGURATION_PARAMETER_TO_DEFAULT_VALUE:	// 0x24 명령  --------------------------------------------------
			bTmp = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];

			TBTmpBYTE = 0;

			if(bTmp == 1){ 									// 1. Silent mode on/off
#ifdef	LOCKSET_VOLUME_SET
				VolumeSetting(VOL_HIGH);
#else
//				TBTmpBYTE = 0x0f;
#endif
			}else if(bTmp == 2){									// 2. Auto Relock on/off
#ifdef	LOCKSET_AUTO_RELOCK
				AutoRelockEnable();
				AutoLockTimeAndAutoReLockTimeDefaultSetAndWrite();
#else
//				TBTmpBYTE = 0x0f;
#endif
			}else if(bTmp == 3){									// 3. Relock time
// Z-Wave CTT 테스트 과정에서 문제가 되어 사용하지 않는 Configuration Parameter라고 설정되고 응답하도록 수정
//				TBTmpBYTE = 0x0f;
				bTmp = 30;
				RomWrite(&bTmp, (WORD)RELOCK_TIME, 1);
			}else if(bTmp == 4){									// 4. Wrong Code Entry Limit
//				TBTmpBYTE = 0x0f;
				bTmp = 5;
				RomWrite(&bTmp, (WORD)WRONG_CODE_ENTRY_LIMIT, 1);
			}else if(bTmp == 5){									// 5. Language
#ifdef	LOCKSET_LANGUAGE_SET				
	/*
				switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1])
				{
					case 1:
					case 2:
					case 3:
						gbLanguageMode = gbPackRxDataCopiedBuf[5];
						RomWrite(&gbLanguageMode, (WORD)LANG_MODE, 1);
						break;
					default:
						TBTmpBYTE = 0x0f;
						break;
				}
	*/
#else
//				TBTmpBYTE = 0x0f;
#endif
			}
			else if(bTmp == 7){									// 7. Shut down time (after wrong code entries)
//				TBTmpBYTE = 0x0f;
				bTmp = 60;
				RomWrite(&bTmp, (WORD)SHUT_DOWN_TIME, 1);
			}else if(bTmp == 8){									// 8. operating mode
//				TBTmpBYTE = 0x0f;
				bTmp = 0x00;
				RomWrite(&bTmp, (WORD)OPERATING_MODE, 1);
			}
			else if(bTmp == 11){									// 11. One Touch Locking
//				TBTmpBYTE = 0x0f;
				bTmp = 0xFF;
				RomWrite(&bTmp, (WORD)ONE_TOUCH_LOCKING, 1);
			}else if(bTmp == 12){									// 12. Privacy Button
//				TBTmpBYTE = 0x0f;
				bTmp = 0x00;
				RomWrite(&bTmp, (WORD)PRIVACY_BUTTON, 1);
			}
			else if(bTmp == 13){									// 13. Lock Status LED
//				TBTmpBYTE = 0x0f;
				bTmp = 0x00;
				RomWrite(&bTmp, (WORD)LOCK_STATUS_LED, 1);
			}
			else if(bTmp == 0xFF){
#ifdef	LOCKSET_VOLUME_SET
				VolumeSetting(VOL_HIGH);
#endif

#ifdef	LOCKSET_AUTO_RELOCK
				AutoRelockEnable();
				AutoLockTimeAndAutoReLockTimeDefaultSetAndWrite();
#endif

#ifdef	LOCKSET_LANGUAGE_SET				
	/*
				switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1])
				{
					case 1:
					case 2:
					case 3:
						gbLanguageMode = gbPackRxDataCopiedBuf[5];
						RomWrite(&gbLanguageMode, (WORD)LANG_MODE, 1);
						break;
					default:
						TBTmpBYTE = 0x0f;
						break;
				}
	*/
#endif
			}
			else
			{
				TBTmpBYTE = 0x0f;
			}

			if(TBTmpBYTE == 0)
			{
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 0);
			}
			else				// 올바르지 않은 정보면 NACK 날림
			{
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_NACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 0);
			}
			break;

#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
		case F0_SET_LED_MODE:
			PackSetLedMode(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA]);
			ModeGotoPackSetLedProcess();
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray,0);
			break;	

		case F0_GET_LED_MODE:
			PackGetLedMode(bTmpArray);
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 4);
			break;	

		case EV_CHANGE_TO_ADD_USER_CREDENTIAL_MODE:		
			if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2] == BLE_CREDENCIAL_REGISTER)
			{
				BLENTxChangeCredentialRegistrationMode(gbPackRxDataCopiedBuf[PACK_F_ADDDATA], gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);
			}
			else if(gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2] == BLE_CREDENCIAL_REG_CANCLE)
			{
				if(BLENCredentialCancleCheck(gbPackRxDataCopiedBuf[PACK_F_ADDDATA]))
				{ 	
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
				}
			}
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray,0);
			break;
#endif 


#if (DDL_CFG_BLE_30_ENABLE >= 0x29) && defined (_USE_ST32_EEPROM_)
		case F0_REBOOT:
		case F0_BEGIN_FLASH:
#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
			SetOTA_From();
#endif 

#ifdef	DDL_CFG_DIMMER
			LedOTAReady();
#endif
			HAL_NVIC_SystemReset();
			break;

		case F0_RUN_APPLICATION:
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 0);
			break;
		
		case F0_GET_ALL_FIRMWARE_VERSIONS:
			bTmpArray[0] = 1;						// Number of FW Targets
			bTmpArray[1] = PROGRAM_VERSION_MAJOR;	// MP Version
			bTmpArray[2] = PROGRAM_VERSION_MINOR;	// Test Version
			bTmpArray[3] = 0x01;					// OTA Upgradable
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 4);
			break;

		case F0_GET_MODEL_IDENTIFIER_STRING:
			RomRead(&bTmpArray[0], (WORD)MODEL_IDENTIFIER_STRING, 16);

			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 16);
			break;

		case F0_GET_PROTOCOL_VERSION:	
			bTmpArray[0] = DDL_CFG_BLE_30_ENABLE;					// Protocol Version
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 1);
			break;

		case F0_SET_DAILY_REPEATING_SCHEDULE_WITH_SLOT_NUMBER:
			bTmp = SlotNumberExtendCheck(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);
			if(bTmp == 0)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00);  	// Nack 송신 준비 
				break;
			}

			bTmpArray[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1];
			bTmpArray[1] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2];
			bTmpArray[2] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+3];
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 3);

			SetDailyRepeatingSchdule(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1], &gbPackRxDataCopiedBuf[PACK_F_ADDDATA]);
			break;
			
		case F0_GET_DAILY_REPEATING_SCHEDULE_WITH_SLOT_NUMBER:
			bTmp = GetDailyRepeatingSchdule(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA], bTmpArray);
			if(bTmp == 0)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00);  	// Nack 송신 준비 
				break;
			}

			bTmpArray[1] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
			bTmpArray[2] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1];
			bTmpArray[3] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2];
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 9);
			break;

		case F0_ENABLE_DISABLE_DAILY_REPEATING_SCHEDULE_WITH_SLOT_NUMBER:
			bTmp = SlotNumberExtendCheck(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);
			if(bTmp == 0)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00);  	// Nack 송신 준비 
				break;
			}
			bTmpArray[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
			bTmpArray[1] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1];
			bTmpArray[2] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2];
			bTmpArray[3] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+3];
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 4);

			SetDailyRepeatingEnable(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1], &gbPackRxDataCopiedBuf[PACK_F_ADDDATA]);
			break;

		case F0_SET_USER_YEAR_DAY_SCHEDULE:
			bTmp = SlotNumberExtendCheck(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);
			if(bTmp == 0)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00);  	// Nack 송신 준비 
				break;
			}

			bTmpArray[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1];
			bTmpArray[1] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2];
			bTmpArray[2] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+3];
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 3);

			SetYearDaySchdule(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1], &gbPackRxDataCopiedBuf[PACK_F_ADDDATA]);
			break;
			
		case F0_GET_USER_YEAR_DAY_SCHEDULE:	
			bTmp = GetYearDaySchdule(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA], bTmpArray);
			if(bTmp == 0)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 	// Nack 송신 준비 
				break;
			}

			bTmpArray[1] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
			bTmpArray[2] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1];
			bTmpArray[3] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2];
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 16);
			break;

		case F0_ENABLE_DISABLE_YEAR_DAY_SCHEDULE:
			bTmp = SlotNumberExtendCheck(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1]);
			if(bTmp == 0)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00);  	// Nack 송신 준비 
				break;
			}

			bTmpArray[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
			bTmpArray[1] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1];
			bTmpArray[2] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2];
			bTmpArray[3] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+3];
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 4);

			SetYearDayEnable(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1], &gbPackRxDataCopiedBuf[PACK_F_ADDDATA]);
			break;
			
		case F0_GET_USER_SCHEDULES_SUPPORTED:
			bTmpArray[0] = 1;					// Number of Weekday
			bTmpArray[1] = NUM_OF_DAILY_REPEAT;	// Number of Daily Repeating
			bTmpArray[2] = NUM_OF_YEARDAY;		// Number of Year Day
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 3);
			break;	

		case F0_GET_SCHEDULE_STATUS:
			bTmp = GetUserSchduleStatus(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA]); 
			if(bTmp == 0xFF)
			{
				PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_INVAILD_PARAMETER,0x00); 	// Nack 송신 준비 
				break;
			}

			bTmpArray[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
			bTmpArray[1] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+1];
			bTmpArray[2] = bTmp;
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 3);
			break;			

		case EV_RESET_DATA:
			bTmpArray[0] = 0x00;
			PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 1);
			ModeGotoAllDataReset(gbPackRxDataCopiedBuf[PACK_F_ADDDATA]);
			break;

		case F0_SET_CONFIGURATION_PARAMETER_TLV:
			switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA])
			{
				case PA_AUTOLOCK_AUTORELOCK_TIME:						
					AutoLockTimeAndAutoReLockTimeCalculationAndWrite(&gbPackRxDataCopiedBuf[PACK_F_ADDDATA+2]);
					TBTmpBYTE = 0x00;
					break;

				default :
					TBTmpBYTE = 0x0f;
					break;
			}	

			if(TBTmpBYTE == 0x00)		// 올바른 정보
			{
				BYTE bTmpsize = 0; 
				BYTE bcnt;

				bTmpsize = gbPackRxDataCopiedBuf[PACK_F_ADDDATA-1];
				for(bcnt=0;bcnt<bTmpsize;bcnt++)
				{
					bTmpArray[bcnt] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+bcnt];
				}
				
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 0);
				// 100ms Delay 후 전송
				PackTx_MakePacketSinglePort(F0_CONFIGURATION_REPORT_TLV, ES_LOCK_EVENT, &gbInnerComCnt, bTmpArray, bTmpsize);
				PackTxWaitTime_EnQueue(10);								
				break;
			}
			else				// 올바르지 않은 정보면 NACK 날림
			{
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_NACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 0);
			}
			break;	
			
		case F0_GET_CONFIGURATION_PARAMETER_TLV:
			switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA])
			{
				case PA_AUTOLOCK_AUTORELOCK_TIME:					
					bTmpArray[0] = gbPackRxDataCopiedBuf[PACK_F_ADDDATA];
					bTmpArray[1] = 4;
					RomRead(&bTmpArray[2],AUTOLOCK_AUTORELOCK_TIME_TLV,4);
//					bTmpArray[2] = (BYTE)((gwAutoLockTimeTLV/10)/0x100);
//					bTmpArray[3] = (BYTE)((gwAutoLockTimeTLV/10)%0x100);
//					bTmpArray[4] = (BYTE)((gwAutoReLockTimeTLV/10)/0x100);
//					bTmpArray[5] = (BYTE)((gwAutoReLockTimeTLV/10)%0x100);
					PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 6);
					break;

				default :
					PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_NACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 0);
					break;
			}	
				
			break;
		case F0_UNLOCK_BY_ACCESSORY:
			switch(gbPackRxDataCopiedBuf[PACK_F_ADDDATA])
			{
				case 0x01:
					if(GetTamperProofPrcsStep())	//3분락 상태이면 
					{
						/*bTmpArray[0] = 0x00;
						bTmpArray[1] = 0x03;
						InnerPackTx_MakePacketSinglePort(gbInnerPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbInnerPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 2);	*/
						PackTxLockOperationbyAccessoryAckSend(0x00, 0x03);
						ModeClear();
					}
					else
					{			
						TenKeyVariablesClear();
						for(bCnt=0;bCnt<(gbPackRxDataCopiedBuf[PACK_F_LENGTH_ADDDATA]-1);bCnt++)
						{
							TenKeySave(MAX_PIN_LENGTH, (gbPackRxDataCopiedBuf[PACK_F_ADDDATA+(bCnt+1)]-0x30), 0);								
						}	
						gbLockOperationbyAccessoryAckAccess = 0x01;
						VerifiedProcessByPincode();
					}
					break;
			}
			break;	
			
#endif	// DDL_CFG_BLE_30_ENABLE

		default:
			PackTx_MakeNack(gbPackRxDataCopiedBuf,NACK_UNKNOWN_MESSAGE,0x00); 	// Nack 송신 준비 
			break;
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Pack Lock Status Check
	@param	None
	@return 	0x00	: Permanent Unlock
	@return 	0x01	: Unlock with relock time
	@return 	0xFF	: Permanent Lock
	@return 	0xEE	: Error
	@remark	RF에 의한 Lock 상태 조회
*/
//------------------------------------------------------------------------------
BYTE LockStsCheck(void)
{
	BYTE bTmp;

	bTmp = MotorSensorCheck();
	switch(bTmp)
	{
		case SENSOR_CLOSE_STATE:
		case SENSOR_CLOSELOCK_STATE:                      	//LOCKED
		case SENSOR_CLOSECEN_STATE:                      	
		
			return 0xFF;


		case SENSOR_OPEN_STATE:
		case SENSOR_OPENLOCK_STATE:                        	// UNLOCKED
		case SENSOR_OPENCEN_STATE:                        	// UNLOCKED		
		case SENSOR_NOT_STATE:					// UNKNOWN STATUS
//			if(AutoLockSetCheck() == 1)					// 자동잠김 ON 이면
//			{
//				return 0x01;
//			}
//			else										// 자동잠김 OFF 이면 
//			{
				return 0x00;
//			}
			break;


		default:
                    return 0xEE;
			break;
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Pack Door Status Check
	@param	tmpArray	: Get Door Status 명령에 응답할 내용
	@return 	None
	@remark	RF에 의한 Door 상태 조회
*/
//------------------------------------------------------------------------------
void DoorStsCheck(BYTE *bTmpArray)
{
	bTmpArray[0] = LockStsCheck();	// Lock State
#ifdef	P_SNS_EDGE_T			// Edge sensor	
	if(gfDoorSwOpenState)	bTmpArray[1] = 0x00; 			// DPS (opened)
	else					bTmpArray[1] = 0xFF; 			// DPS (closed)
#else 
	bTmpArray[1] = 0xFF;  //??
#endif 
	if(bTmpArray[0] == 0xFF)	bTmpArray[2] = 0xFF;			// Deadbolt (closed)
	else						bTmpArray[2] = 0x00; 			// Deadbolt (opened)
	bTmpArray[3] = 0xFE;								// Latch (not supported)
	bTmpArray[4] = 0xFE;								// Rx (not supported)
	if(bTmpArray[0] == 0x01)	bTmpArray[5] = (BYTE)gLockStatusTimer100ms;		// Remain time
	else						bTmpArray[5] = 0xFE;					// Remain time
}



static void PackAdd1UserCode(BYTE *pPincodeData, BYTE bPincodeLength, BYTE bUserStatus, WORD wUserSlot)
{							// UserStatus에 대한 처리가 안되어 있음, 사양 확인 후 작업 예정임
	BYTE		i, bByteLength, bTmpArray[10];
	WORD	wTmp;
	BYTE bTmpsize = 0x00;
	BYTE bTmpEvent = 0x00;

	if(gbModuleProtocolVersion >= 0x30)
	{
		pPincodeData++;
	}
	
	SetUserStatus(wUserSlot, bUserStatus);					// User status 설정
	AsciiToBcd(pPincodeData, gPinInputKeyFromBeginBuf, bPincodeLength);		// user code를 gPinInputKeyFromBeginBuf (key buffer)에 넣기
	if(bPincodeLength % 2) 		bByteLength = (bPincodeLength + 1) /2;	// PinVerify를 위한 유효 바이트수 구하기
	else						bByteLength = bPincodeLength /2;
	gPinInputKeyCnt = bPincodeLength;
	wTmp = (WORD)PincodeVerify(0);	// 기존 코드 중복 검사
	switch(wTmp)
	{
		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
			break;
	
		case RET_NO_MATCH:
			UpdatePincodeToMemoryByModule(&gPinInputKeyFromBeginBuf[0], bByteLength, wUserSlot, bPincodeLength);

			if(gbModuleProtocolVersion >= 0x30)
			{
				bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
				bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
				bTmpArray[2] = bUserStatus;
				for(i = 0; i < bPincodeLength; i++)		bTmpArray[i + 3]  = pPincodeData[i];
				bTmpsize = 3;
				bTmpEvent = F0_USER_ADDED_EX;
			}
			else
			{
				bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
				bTmpArray[1] = bUserStatus;
				for(i = 0; i < bPincodeLength; i++) 	bTmpArray[i + 2]  = pPincodeData[i];
				bTmpsize = 2;	
				bTmpEvent = F0_USER_ADDED;
			}

			PackTx_MakePacketSinglePort(bTmpEvent, ES_LOCK_EVENT, &gbComCnt, bTmpArray, bPincodeLength + bTmpsize);
			PackTxWaitTime_EnQueue(10);
			
			if(gfPackTypeiRevoBleN || gfPackTypeCBABle) //YALE ACCESS Module도 완료음이 나도록 수정)	
			{
				if(GetTamperProofPrcsStep())
				{
					TamperCountClear();
				}

				FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
				ModeClear();
			}
			break;
	
		default:	// 중복 PIN 코드가 존재하는 경우
			if(wTmp == wUserSlot)	// 등록하려는 슬롯과 중복이면 덮어쓰고 중복 알람 보내지 않음
			{
				UpdatePincodeToMemoryByModule(&gPinInputKeyFromBeginBuf[0], bByteLength, wUserSlot, bPincodeLength);

				if(gbModuleProtocolVersion >= 0x30)
				{
					bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
					bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
					bTmpArray[2] = bUserStatus;
					for(i = 0; i < bPincodeLength; i++) 	bTmpArray[i + 3]  = pPincodeData[i];
					bTmpsize = 3;
					bTmpEvent = F0_USER_ADDED_EX;
				}
				else
				{
					bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
					bTmpArray[1] = bUserStatus;
					for(i = 0; i < bPincodeLength; i++) 	bTmpArray[i + 2]  = pPincodeData[i];
					bTmpsize = 2;
					bTmpEvent = F0_USER_ADDED;
				}

				PackTx_MakePacketSinglePort(bTmpEvent, ES_LOCK_EVENT, &gbComCnt, bTmpArray, bPincodeLength + bTmpsize);
				PackTxWaitTime_EnQueue(10);

				if(gfPackTypeiRevoBleN || gfPackTypeCBABle) //YALE ACCESS Module도 완료음이 나도록 수정)	
				{
					if(GetTamperProofPrcsStep())
					{
						TamperCountClear();
					}
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					ModeClear();
				}
			}
			else
			{
				PackTx_MakeAlarmPacket(AL_DUPLICATE_PIN_ERROR, (wTmp>>8) & 0xff, wTmp & 0xff);
				PackTxWaitTime_EnQueue(10);
			}
			break;
	}

#ifdef DDL_CFG_EMERGENCY_119
	memset(gPinInputKeyFromBeginBuf, 0x00,((MAX_PIN_LENGTH>>1)+2));	
#else 
	memset(gPinInputKeyFromBeginBuf, 0x00,(MAX_PIN_LENGTH>>1));	
#endif
	gPinInputKeyCnt = 0;

}


static void PackDeleteUserCode(WORD wUserSlot)
{	// User status 0(avlilable)로 설정, User code 0xFF로 채우기, User Schedule 0로 채우기
	BYTE bTmpArray[2];
	BYTE bTmpsize = 0x00;
	BYTE bTmpEvent = 0x00;
	
#if defined (_USE_IREVO_CRYPTO_)
	BYTE bTemp[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
#endif 
	if((wUserSlot & 0xFF) == 0xFF)
	{
		// User Code 전체 삭제 명령에는 One-time Code  삭제하지 않도록 수정
		AllUserCodeDelete();
//		OnetimeCodeDelete();
	}
	else if(wUserSlot == RET_TEMP_CODE_MATCH)
	{
#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(bTemp,8);
		RomWrite(bTemp, ONETIME_CODE, 8);					// Temporary code 0xFF로 채우기		
#else 
		RomWriteWithSameData(0xFF, ONETIME_CODE, 8);					// Temporary code 0xFF로 채우기
#endif 			
	}
	else
	{
		SetUserStatus(wUserSlot, USER_STATUS_AVAILABLE);			// User status 0(avlilable)로 설정
#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(bTemp,8);
		RomWrite(bTemp, USER_CODE + ((wUserSlot -1) * 8), 8);	// User code 0xFF로 채우기
#else 		
		RomWriteWithSameData(0xFF, USER_CODE + ((wUserSlot -1) * 8), 8);	// User code 0xFF로 채우기
#endif 		

	// 도어록에서 비밀번호를 새로 등록하거나 변경하는 경우에는 기존 Schedule 관련 정보 모두 초기화
		// Schedule status 영역 삭제 
		SetScheduleStatus(wUserSlot, SCHEDULE_STATUS_DISABLE);
	
		// Schedule 영역 삭제 
		if((wUserSlot-1) < 30U)
			RomWriteWithSameData(0xFF, USER_SCHEDULE+(((WORD)wUserSlot-1)*4), 4);

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
		ScheduleDelete_Ble30(CREDENTIALTYPE_PINCODE, (BYTE)wUserSlot);
#endif
	}

	if(gbModuleProtocolVersion >= 0x30)
	{
		bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		bTmpsize =2;
		bTmpEvent = F0_USER_DELETED_EX;
	}
	else
	{
		bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
		bTmpsize =1;		
		bTmpEvent = F0_USER_DELETED;
	}

	PackTx_MakePacketSinglePort(bTmpEvent, ES_LOCK_EVENT, &gbComCnt, bTmpArray, bTmpsize);
	PackTxWaitTime_EnQueue(10);

	if(gfPackTypeiRevoBleN || gfPackTypeCBABle) //YALE ACCESS Module도 완료음이 나도록 수정)
	{
		if(GetTamperProofPrcsStep())
		{
			TamperCountClear();
		}

		FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
		ModeClear();
	}
}




static void PackGetUserCode(WORD wUserSlot)
{
	BYTE i, bTmpArray[16];
	BYTE bBcdArray[8], bAsciiArray[16];

	if(wUserSlot == RET_MASTER_CODE_MATCH ||wUserSlot == RET_2BYTE_MASTER_CODE_MATCH)	// Master code
		{
			bTmpArray[0] = USER_STATUS_OCC_ENABLED;	// Master code는 상태 확인 안함
			RomRead(bBcdArray, MASTER_CODE, 8);
#if defined (_USE_IREVO_CRYPTO_)
			EncryptDecryptKey(bBcdArray,8);
#endif 	
			if(bBcdArray[0] == 0xFF)
			{
				bTmpArray[0] = USER_STATUS_AVAILABLE;
			}		
		}
		else if(wUserSlot == RET_TEMP_CODE_MATCH)	// Temporary code
		{
			bTmpArray[0] = USER_STATUS_OCC_ENABLED;	// Onetime code는 상태 확인 안함
			RomRead(bBcdArray, ONETIME_CODE, 8);
#if defined (_USE_IREVO_CRYPTO_)
			EncryptDecryptKey(bBcdArray,8);
#endif 	
			if(bBcdArray[0] == 0xFF)
			{
				bTmpArray[0] = USER_STATUS_AVAILABLE;
			}		
		}
		else										// User code
		{
			bTmpArray[0] = GetUserStatus(wUserSlot);
			if(bTmpArray[0] != USER_STATUS_OCC_ENABLED && bTmpArray[0] != USER_STATUS_OCC_DISABLED && bTmpArray[0] != USER_STATUS_NON_ACCESS_USER) 
			{
				bTmpArray[0] = (BYTE)(wUserSlot & 0x00FF);
				PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 1);
				return;
			}
			RomRead(bBcdArray, USER_CODE + ((wUserSlot - 1) * 8), 8);
#if defined (_USE_IREVO_CRYPTO_)
			EncryptDecryptKey(bBcdArray,8);
#endif 	
		}

	BcdToAscii(bBcdArray, bAsciiArray);
	for(i = 0; i < MAX_PIN_LENGTH; i++)
	{
		if(bAsciiArray[i] == 0xFF)	break;
		bTmpArray[i + 1] = bAsciiArray[i];
	}
	PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, i + 1);
}



void SetUserStatus(WORD wUserSlot, BYTE bUserStatus)
{
	RomWrite(&bUserStatus, USER_STATUS + (wUserSlot - 1), 1);
}



BYTE GetUserStatus(WORD wUserSlot)
{
	BYTE bTmp;
	
	RomRead(&bTmp, USER_STATUS + (wUserSlot - 1), 1);

	return	bTmp;
}


void SetScheduleStatus(WORD wUserSlot, BYTE bStatus)
{
	if((wUserSlot - 1) < 30U)
		RomWrite(&bStatus, USER_SCH_STA+ (wUserSlot - 1), 1);
}
	


BYTE GetScheduleStatus(WORD wUserSlot)
{
	BYTE bTmp;

	if((wUserSlot - 1) >= 30U)
	{
		return SCHEDULE_STATUS_DISABLE;
	}
	
	RomRead(&bTmp, USER_SCH_STA + (wUserSlot - 1), 1);

	return	bTmp;
}



void AsciiToBcd(BYTE *bAsciiArray, BYTE *bBcdArray, BYTE bNum)
{
	BYTE i, bTmp;
	BYTE bLoop;

	// RF RX Packet을 받아서 변환하는 것이라 자리 수에 따라 처리하지 않을 경우 자리 수를 넘어 잘못 변환되는 문제 수정
	memset(bBcdArray, 0xFF, 8);

	bLoop = bNum >> 1;

	for(i = 0; i < bLoop; i++)
	{
		bTmp = bAsciiArray[2 * i];
		if(bTmp >= 0x30 && bTmp <= 0x39)
		{
			bTmp -= 0x30;
			bBcdArray[i] = (bTmp<<4) & 0xF0;
		}
		else
			bBcdArray[i] = 0xF0;

		bTmp = bAsciiArray[2 * i + 1];
		if(bTmp >= 0x30 && bTmp <= 0x39)
		{
			bTmp -= 0x30;
			bBcdArray[i] = bBcdArray[i] | bTmp & 0x0F;
		}
		else
			bBcdArray[i] = bBcdArray[i] | 0x0F;
	}

	if(bNum & 0x01)
	{
		bTmp = bAsciiArray[2 * i];
		if(bTmp >= 0x30 && bTmp <= 0x39)
		{
			bTmp -= 0x30;
			bBcdArray[i] = (bTmp<<4) & 0xF0;
		}
		else
			bBcdArray[i] = 0xF0;

		bBcdArray[i] |= 0x0F;
	}
}



void BcdToAscii(BYTE *bBcdArray, BYTE *bAsciiArray)
{
	BYTE i, bTmp;

	for(i = 0; i < 8; i++)
	{
		bTmp = (bBcdArray[i] >> 4) & 0x0F;	// 상위 nibble
		if(bTmp == 0x0F)	bAsciiArray[2 * i] = 0xFF;		// 0~9 이외의 값은 0xFF로 채운다 
		else				bAsciiArray[2 * i] = bTmp + 0x30;
		bTmp = bBcdArray[i] & 0x0F;		// 하위 nibble
		if(bTmp == 0x0F)	bAsciiArray[2 * i + 1] = 0xFF;		// 0~9 이외의 값은 0xFF로 채운다 
		else				bAsciiArray[2 * i + 1] = bTmp + 0x30;
	}
}


#ifdef	DDL_CFG_RFID

static void PackAdd1UserCard(BYTE *pCardUidData, BYTE CardUidLength, WORD wUserSlot)
{							
	BYTE	i, bTmpArray[12];
	WORD	wTmp;
	BYTE bTmpsize = 0x00;

#if 0
	if(gbModuleProtocolVersion >= 0x30)
	{
		/* Protocol version 이 3.0 이상이면 pointer 증가 */
		pCardUidData++;
	}
#endif 

	memset(gCardAllUidBuf[0], 0xFF, MAX_CARD_UID_SIZE);
	memcpy(gCardAllUidBuf[0], pCardUidData, CardUidLength);
	wTmp = (WORD)CardVerify(1);
	if((wTmp == _DATA_NOT_IDENTIFIED) || (wTmp == wUserSlot))
	{
		UpdateCardUidToMemoryByModule(gCardAllUidBuf[0], (BYTE)wUserSlot);

		memset(bTmpArray, 0xFF, 12);

		if(gbModuleProtocolVersion >= 0x30)
		{
			for(i = 0; i < (CardUidLength+4); i++)	bTmpArray[i]  = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+i];
			bTmpsize = 12;
		}
		else 
		{
		
			for(i = 0; i < (CardUidLength+3); i++) 	bTmpArray[i]  = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+i];
			bTmpsize = 11;			
		}

		PackTx_MakePacketSinglePort(EV_USER_CARD_ADDED, ES_LOCK_EVENT, &gbComCnt, bTmpArray, bTmpsize);
		// 100ms Delay 후 전송
		PackTxWaitTime_EnQueue(10);
		
	}
	else
	{
		// 중복 알람 발생
		PackTx_MakeAlarmPacket(AL_DUPLICATE_RFID_ERROR, (wTmp>>8) & 0xff, wTmp & 0xff);
		// 100ms Delay 후 전송
		PackTxWaitTime_EnQueue(10);
	}	
}




static void PackDeleteUserCard(WORD wUserSlot)
{							
	BYTE bTmpArray[3];
	BYTE bTmpsize = 0x00;

	if(wUserSlot == 0xFF)
	{
		AllCardDelete();
	}
	else
	{
		memset(gCardAllUidBuf[0], 0xFF, MAX_CARD_UID_SIZE);

		UpdateCardUidToMemoryByModule(gCardAllUidBuf[0], (BYTE)wUserSlot);

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
		ScheduleDelete_Ble30(CREDENTIALTYPE_CARD, (BYTE)wUserSlot);
#endif
	}

	bTmpArray[0] = CREDENTIALTYPE_CARD;
	bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
	bTmpsize = 2;

	PackTx_MakePacketSinglePort(EV_USER_CARD_DELETED, ES_LOCK_EVENT, &gbComCnt, bTmpArray, bTmpsize);
	PackTxWaitTime_EnQueue(10);
	
	if(gfPackTypeiRevoBleN || gfPackTypeCBABle)
	{
		if(GetTamperProofPrcsStep())
		{
			TamperCountClear();
		}

		FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
		ModeClear();
	}
}



static void PackGetUserCard(WORD wUserSlot)
{
	BYTE i, bTmpArray[16];
	BYTE bTmpsize = 0x00;

	for(i = 0; i < 2; i++)	bTmpArray[i]  = gbPackRxDataCopiedBuf[PACK_F_ADDDATA+i];

	memset(&bTmpArray[3], 0xFF, MAX_CARDUID_LENGTH);
	RomRead(&bTmpArray[3], CARD_UID + ((wUserSlot -1) * MAX_CARD_UID_SIZE), MAX_CARD_UID_SIZE);

#if defined (_USE_IREVO_CRYPTO_)
	EncryptDecryptKey(&bTmpArray[3],8);
#endif 	

	if(DataCompare(&bTmpArray[3], 0xFF, MAX_CARDUID_LENGTH) == 0)
	{
		bTmpArray[2] = USER_STATUS_AVAILABLE;
	}
	else
	{
		bTmpArray[2] = USER_STATUS_OCC_ENABLED;
	}

	bTmpsize = 3;

	PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, MAX_CARDUID_LENGTH+bTmpsize);	
}

static void PackGetSupportedCardNumber(void)
{
	BYTE i, bTmpArray[16];

	bTmpArray[0] = CREDENTIALTYPE_CARD;
	bTmpArray[1] = SUPPORTED_USERCARD_NUMBER;
	bTmpArray[2] = 0;

	for(i = 0; i < SUPPORTED_USERCARD_NUMBER; i++)
	{
		RomRead(&bTmpArray[3], CARD_UID+((WORD)i*MAX_CARD_UID_SIZE), MAX_CARD_UID_SIZE);
#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(&bTmpArray[3],8);
#endif 	
		if(DataCompare(&bTmpArray[3], 0xFF, MAX_CARD_UID_SIZE) != 0)
			bTmpArray[2]++;
	}

	PackTx_MakePacketSinglePort(gbPackRxDataCopiedBuf[PACK_F_EVENT], ES_LOCK_ACK, &gbPackRxDataCopiedBuf[PACK_F_COUNT], bTmpArray, 3);
}




#endif	//DDL_CFG_RFID

#ifdef	DDL_CFG_DS1972_IBUTTON
static void PackDeleteUserDS1972_Ibutton(WORD wUserSlot)
{							
	BYTE bTmpArray[3];
	BYTE bTmpsize = 0x00;

	if(wUserSlot == 0xFF)
	{
		AllDS1972TouchKeyDelete();
	}
	else
	{
		ProcessDeleteOneDS1972TouchKey((BYTE)wUserSlot);
	}

	if(gbModuleProtocolVersion >= 0x30)
	{
		bTmpArray[0] = CREDENTIALTYPE_DS1972TOUCHKEY;
		bTmpArray[1] = (BYTE)((wUserSlot>>8) & 0x00ff);
		bTmpArray[2] = (BYTE)(wUserSlot & 0x00ff);
		bTmpsize = 3;
	}
	else
	{
		bTmpArray[0] = CREDENTIALTYPE_DS1972TOUCHKEY;
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		bTmpsize = 2;		
	}

	PackTx_MakePacketSinglePort(EV_USER_CARD_DELETED, ES_LOCK_EVENT, &gbComCnt, bTmpArray, bTmpsize);
	PackTxWaitTime_EnQueue(10);
	
	if(gfPackTypeiRevoBleN)
	{
		if(GetTamperProofPrcsStep())
		{
			TamperCountClear();
		}
		FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
	}
}
#endif 


#ifdef	DDL_CFG_IBUTTON
static void PackDeleteUserGMTouchKey(WORD wUserSlot)
{							
	BYTE bTmpArray[3];
	BYTE bTmpsize = 0x00;

	if(wUserSlot == 0xFF)
	{
		AllGMTouchKeyDelete();
	}
	else
	{
		ProcessDeleteOneGMTouchKeyByBleN((BYTE)wUserSlot);
	}

	if(gbModuleProtocolVersion >= 0x30)
	{
		bTmpArray[0] = CREDENTIALTYPE_GMTOUCHKEY;
		bTmpArray[1] = (BYTE)((wUserSlot>>8) & 0x00ff);
		bTmpArray[2] = (BYTE)(wUserSlot & 0x00ff);
		bTmpsize = 3;
	}
	else
	{
		
		bTmpArray[0] = CREDENTIALTYPE_GMTOUCHKEY;
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		bTmpsize = 2;
	}

	PackTx_MakePacketSinglePort(EV_USER_CARD_DELETED, ES_LOCK_EVENT, &gbComCnt, bTmpArray, bTmpsize);
	PackTxWaitTime_EnQueue(10);

	if(gfPackTypeiRevoBleN)
	{
		if(GetTamperProofPrcsStep())
		{
			TamperCountClear();
		}

		FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
	}
}
#endif 	// DDL_CFG_IBUTTON





//------------------------------------------------------------------------------
/** 	@brief	RX Command - Set User Schedule
	@param 	None
	@return 	0 : Slot Number error
			1 : Set completed
	@remark Set User Schedule에 대한 처리
	@remark 
			| Slot Number | Description |
			|-----|----------|
			|1 ~ SUPPORTED_USERCODE_NUMBER | 해당 번지의 User Code Schedule 설정 |
			|Master Code |설정 불가능 |
			|Onetime Code | 설정 불가능 |

	@remark Schedule time data : 4 bytes 
	@remark Schedule time data : 0xFF, 0xFF, 0xFF, 0xFF 일 경우 Delete 
	@remark 
			| Schedule Status | Description |
			|-----|----------|
			| 0x00 | Disable Schedule |
			| 0xFF | Enable Schedule |

	@remark Schedule time data를 설정할 경우 Schedule Status는 Enable  
	@remark Schedule time data를 삭제할 경우 Schedule Status는 Disable  
*/
//------------------------------------------------------------------------------
BYTE PackRxCmdSetSchedule(BYTE* pScheduleData, WORD SlotNumber)
{
//Master Code cannot be scheduled
	if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
	{
		if((SlotNumber == 0) || (SlotNumber > 30U))
		{
			return 0;
		}
	}
	else
	{
		if( SlotNumber != 0x01 && SlotNumber != 0x02 ) // User slot 0x01, 0x02만 허용
		{
			return 0;
		}
	}		
	
	RomWrite(&gbPackRxDataBuf[5], USER_SCHEDULE + ((SlotNumber-1)*4), 4);

	if(DataCompare(&gbPackRxDataBuf[5], 0xFF, 4) == _DATA_IDENTIFIED)
	{
		SetScheduleStatus(SlotNumber,SCHEDULE_STATUS_DISABLE);
	}
	else
	{
		SetScheduleStatus(SlotNumber,SCHEDULE_STATUS_ENABLE);
	}		

	return 1;
}


//------------------------------------------------------------------------------
/** 	@brief	RX Command - Get User Schedule
	@param 	None
	@return 	0 : Slot Number error
			1 : Get completed
	@remark Get User Schedule에 대한 처리
	@remark 
			| Slot Number | Description |
			|-----|----------|
			|1 ~ SUPPORTED_USERCODE_NUMBER | 해당 번지의 User Code Schedule 조회 |
			|Master Code |조회 불가능 |
			|Onetime Code | 조회 불가능 |

	@remark Schedule time data : 4 bytes 
	@remark Schedule time data : 0xFF, 0xFF, 0xFF, 0xFF 일 경우 Delete 
	@remark 
			| Schedule Status | Description |
			|-----|----------|
			| 0x00 | Disable Schedule |
			| 0xFF | Enable Schedule |

	@remark Schedule time data를 설정할 경우 Schedule Status는 Enable  
	@remark Schedule time data를 삭제할 경우 Schedule Status는 Disable  
*/
//------------------------------------------------------------------------------
BYTE PackRxCmdGetSchedule(BYTE* pScheduleData, WORD SlotNumber)
{
//Master Code cannot be scheduled
	if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
	{
		if((SlotNumber == 0) || (SlotNumber > 30U))
		{
			return 0;
		}
	}
	else
	{
		if( SlotNumber != 0x01 && SlotNumber != 0x02 ) // User slot 0x01, 0x02만 허용
		{
			return 0;
		}
	}		

	RomRead(&pScheduleData[0], USER_SCH_STA+(SlotNumber-1), 1);
	RomRead(&pScheduleData[1], USER_SCHEDULE+((SlotNumber-1)*4), 4);

	if(DataCompare(&pScheduleData[1], 0xFF, 4) == _DATA_IDENTIFIED)
	{
		pScheduleData[0] = SCHEDULE_STATUS_DISABLE;
	}
	return 1;
}


//------------------------------------------------------------------------------
/** 	@brief	RX Command - Enable/Disable User Schedule
	@param 	None
	@return 	0 : Slot Number error
			1 : Set completed
	@remark Enable/Disable Schedule에 대한 처리
	@remark 
			| Slot Number | Description |
			|-----|----------|
			|1 ~ SUPPORTED_USERCODE_NUMBER | 해당 번지의 User Schedule Status 설정 |
			|Master Code |설정 불가능 |
			|Onetime Code | 설정 불가능 |
			| 0xFF | All User Codes 설정 |

	@remark 
			| Schedule Status | Description |
			|-----|----------|
			| 0x00 | Disable Schedule |
			| 0xFF | Enable Schedule |

*/
//------------------------------------------------------------------------------
BYTE PackRxCmdEnableUserSchedule(BYTE Status, WORD SlotNumber)
{
	if(SlotNumber == 0xFF)
	{
		RomWriteWithSameData(Status, USER_SCH_STA, 30U);
	}			
	else
	{
//Master Code cannot be scheduled
		if(gbManageMode == _AD_MODE_SET)		// Basic New Advanced Mode
		{
			if((SlotNumber == 0) || (SlotNumber > 30U))
			{
				return 0;
			}
		}
		else
		{
			if( SlotNumber != 0x01 && SlotNumber != 0x02 ) // User slot 0x01, 0x02만 허용
			{
				return 0;
			}
		}		
	
		RomWrite(&Status, USER_SCH_STA+(SlotNumber-1), 1);
	}
	
	return 1;	
}

BYTE PackRxDecapsulateData(BYTE *bAdditionalData , uint16_t length)
{
	BYTE bTmpArray[16] = {0x00,};
#ifdef _USE_STM32_CRYPTO_LIB_
	BYTE bTmpkeyNull[16]  = {0x00,};
#endif 
	
	RomRead(bTmpArray,COM_PACK_ENCRYPT_KEY,16);

#ifdef _USE_IREVO_CRYPTO_
	EncryptDecryptKey(bTmpArray,sizeof(bTmpArray));
#endif 

#ifdef _USE_STM32_CRYPTO_LIB_
	if(memcmp(gbPackTxRandomKeybuffer,bTmpkeyNull,16) != 0x00)
	{
		// key 교환 과정 gbPackTxRandomKeybuffer key 를 사용 하고 ACK 응 받아서 이놈을 초기화 한다 
		//gbPackTxRandomKeybuffer 이게 0x00 들이 아니면 E3 하는 중이라고 보고 처리 
		DataDecrypt(bAdditionalData,length,gbPackTxRandomKeybuffer,gbPackRxDataCopiedBuf);
	}
	else
	{
		DataDecrypt(bAdditionalData,length,bTmpArray,gbPackRxDataCopiedBuf);
	}
#endif 

	return 1;
}

BYTE PackRxEncryptionKeyExchangeCheck(BYTE* bCheckSumData)
{
	WORD TmpCheckSum = (WORD)(((bCheckSumData[0] << 8) & 0xFF00) | bCheckSumData[1]);

	if((Get_Checksum_CRC16(gbPackTxRandomKeybuffer,16) & 0xFFFF) == TmpCheckSum)
	{
#ifdef _USE_IREVO_CRYPTO_
		EncryptDecryptKey(gbPackTxRandomKeybuffer,sizeof(gbPackTxRandomKeybuffer));
#endif 
		RomWrite(gbPackTxRandomKeybuffer,COM_PACK_ENCRYPT_KEY,16);
		memset(gbPackTxRandomKeybuffer,0x00,16);	// 이후 다시 E3 하기 까지 naver use 	
		gbpackModeSuspend = 0x00;
		return 1;
	}

	gbpackModeSuspend = 0x01;
	gbModuleMode = PACK_ID_NOT_CONFIRMED; 
	return 0;
}


#endif 

