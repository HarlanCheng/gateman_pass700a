//------------------------------------------------------------------------------
/** 	@file		ModeMenuAutoLockSet_T1.c
	@version 0.1.00
	@date	2016.04.26
	@brief	Programmable Auto Lock Setting
	@remark	자동 잠김 설정 모드
	@see	ModeMenuMainSelect_T1.c
	@see	ModeMenuLockSetting_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.26		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


#ifdef	LOCKSET_AUTO_RELOCK

void ModeGotoAutoLockSetting(void)
{
	gbMainMode = MODE_MENU_AUTOLOCK_SET;
	gbModePrcsStep = 0;
}




void AutoLockSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		//AutoLock Enable
		case 1: 	
			DDLStatusFlagClear(DDL_STS_MANUALLOCK); 

			// Auto Lock 설정 Event 전송

//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			AutoLockTimeAndAutoReLockTimeDefaultSetAndWrite();
			gbModePrcsStep++;
			break;
	
		//AutoLock Disable
		case 3: 		
			DDLStatusFlagSet(DDL_STS_MANUALLOCK);	

			// Auto Lock 해제 Event 전송

//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);

			gbModePrcsStep++;
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 이전 모드로 이동
			gbModePrcsStep--;
			break;
	}			
}



void MenuAutoLockSetSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		//AutoLock Enable
		case TENKEY_1:			
//			MenuSelectKeyTempSaveNTimeoutReset(154, gbInputKeyValue);		
			MenuSelectKeyTempSaveNTimeoutReset(AVML_TURNON_CONT_SHARP, gbInputKeyValue);		
			break;

		//AutoLock Disable
		case TENKEY_3:			
//			MenuSelectKeyTempSaveNTimeoutReset(155, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_TURNOFF_CONT_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_SHARP:		 //# 버튼음 출력
#ifdef	P_VOICE_RST	
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
				FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuLockSetting();
			break;			

		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;

		default:
			TimeExpiredCheck();
			break;
	}	
}




//------------------------------------------------------------------------------
/** 	@brief	Auto Lock Setting Mode
	@param	None
	@return 	None
	@remark 자동잠김/수동잠김 설정 모드 시작
*/
//------------------------------------------------------------------------------
void ModeAutoLockSetting(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			Feedback13MenuOn(153, VOL_CHECK);
			Feedback13MenuOn(AVML_TURNON1_TURNOFF3, VOL_CHECK);

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuAutoLockSetSelectProcess();
			break;	
	
		case 2:
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
			{
				if(GetBuzPrcsStep() || GetVoicePrcsStep()) 	break;
			}

			AutoLockSetSelected();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoMenuLockSetting);
			break;

		default:
			ModeClear();
			break;
	}

}

#endif		// LOCKSET_AUTO_RELOCK


