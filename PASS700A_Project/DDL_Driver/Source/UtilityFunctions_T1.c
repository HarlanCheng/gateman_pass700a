#include	"Main.h"

BYTE DataCompare(BYTE *bpData, BYTE bData, BYTE bNum)
{
	BYTE bCnt;

	for(bCnt = 0; bCnt < bNum; bCnt++)
	{
		if(*bpData != bData)	return _DATA_NOT_IDENTIFIED;	 
		bpData++;
	}	
	return _DATA_IDENTIFIED;
}

#define P_DEBUG_EN(A) P_COM_HCP_EN(A);

void DebugWith_1wire_protocol(BYTE* bData , BYTE bSize)
{
	/* 1-wire 지만 실제 1-wire 동작을 하고 싶을 때는 소스 손봐야 한다 */
	/* 순전히 로직 아날라이져에서 값 확인 용도로 구현 */
	/* delay 함수를 사용 하면 부팅 초반 부터 볼수 없으므로 for 문으로 변경 */
	/* gpio init 이후 부터 사용 가능 */
	BYTE i = 0x00;
	BYTE j = 0x00;	
	WORD k = 0x00;	
	

	for(j = bSize ; j > 0 ; j--)
	{
		// prestate 
		P_DEBUG_EN(1);		
		for( k=0; k< 13000 ;k++ )  //5ms
			__NOP();	

		// reset state L 480us
		P_DEBUG_EN(0);
		for( k=0; k< 1410; k++ )  //480us
			__NOP();	

		// Bus release 70us
		P_DEBUG_EN(1);
		for( k=0; k< 250; k++ )  //70us
			__NOP();	

		// 원래 protocol 상은 device 으로 부터 input 받아서 H 면 없는 상태 , L 면 있는 상태로 판단 
		// 연결 된것이 없으니 그냥 L 상태로 잠시 둔다 
		// 로직 아날라이져에서 값을 확인 하기 위해 
		P_DEBUG_EN(0);
		for( k=0; k< 160 ;k++ ) //60us
			__NOP();		

		P_DEBUG_EN(1);

		for(i = 0 ; i < 8 ; i++)
		{
			if(*bData & ((0x01) << i))
			{
				// High
				P_DEBUG_EN(0);
				for( k=0; k< 13 ;k++ ) //6us
					__NOP();	
				P_DEBUG_EN(1);
				for( k=0; k< 130 ;k++ ) //60us
					__NOP();					
			}
			else 
			{
				// Low 
				P_DEBUG_EN(0);
				for( k=0; k< 130 ;k++ ) //60us
					__NOP();
				P_DEBUG_EN(1);
				for( k=0; k< 20 ;k++ ) //10us
					__NOP();
			}
		}
		bData++;
	}

	P_DEBUG_EN(0);
	
}


void QWORDToBYTE(uint64_t Data , BYTE* OutputBuffer , BYTE Order)
{
	QWORDTOBYTE SortData = {0x00,};

	SortData.InputData = Data ;

	if(Order) //LSB_FIRST	
	{
		memcpy(SortData.OutputData, OutputBuffer,sizeof(uint64_t));
	}
	else  // MSB FIRST
	{
		for (BYTE i = 0 ; i < sizeof(uint64_t) ; i++)
		{
			*(OutputBuffer + i) = SortData.OutputData[(sizeof(uint64_t)-1)-i];
		}
	}
}


void BYTEToQWORD(uint64_t *Data , BYTE* InputBuffer , BYTE Order)
{
	QWORDTOBYTE SortData = {0x00,};

	if(Order) //LSB_FIRST	
	{
		memcpy(InputBuffer,SortData.OutputData,sizeof(uint64_t));
	}
	else  // MSB FIRST
	{
		for (BYTE i = 0 ; i < sizeof(uint64_t) ; i++)
		{
			SortData.OutputData[(sizeof(uint64_t)-1)-i] = *(InputBuffer + i);
		}
	}

	*Data = SortData.InputData;
}


uint64_t RandomNumberGenerator(BYTE* bpData , BYTE bSize)
{
	uint64_t qresult = 0x00;
	DWORD dwTemp;
	WORD RandSeed;
	BYTE i ;

#ifdef P_LOWBAT_EN
	P_LOWBAT_EN(1);
	RandSeed = ADCCheck(ADC_BATTERY_CHECK);
	P_LOWBAT_EN(0);
	RandSeed += SystickLoad();
#else 
	RandSeed += SystickLoad();
#endif 	

	srand(RandSeed);

	for(i = 0 ; i < bSize ; i++)
	{
		dwTemp = (DWORD)rand();
		bpData[i] = (BYTE)(dwTemp & 0xFF);

		if(i < 8)
		{
			qresult = (qresult << 8);			
			qresult |= bpData[i];
		}
	}

	/* 만들어진 난수 값을 8개를 MSB 방향으로 밀어서 큰 난수를 만듬(MAX 64bit) 이를 return 하여 사용 */
	/* 외부에서 필요한 만큼 casting 해서 사용 할것 */
	/* ex) (BYTE)RandomNumberGenerator(buffer,size); */
	return qresult; 
}



BYTE BCDToDEC(BYTE BCDData)
{
	BYTE DECData;

	DECData = (BCDData >> 4) * 10;
	DECData += (BCDData & 0x0F);

	return (DECData);
}

#if 0
uint32_t*	MSP_psp = NULL;

#if 0
사용법 
호출 되는 함수를 누가 불렀는가에 대해서 debug 하고 싶으면 
호출 되는 함수 가장 위에서 

MSP_psp = (uint32_t*)__get_MSP();

를 붙여 넣는다 그러면 MSP_psp 는 현재 stack 의 top 을 가지고 있게 되고 
SaveMSP2ROM 
함수를 실행 하면 원하는 EEPROM 에 stack top 에서 bottom 으로 10개 내용을 저장 한다 
이걸 가지고 *.map 파일의 object 주소를 비교 하여 누가 호출 했는지 찾아 낸다. 

ex)
void ModeClear(void)
{
	MSP_psp = (uint32_t*)__get_MSP();
	SaveMSP2ROM();
	
	gbMainMode = 0;
	gbModePrcsStep = 0;

#if  defined (DDL_CFG_MS)
	Ms_Credential_Info.bSelected_Item = 0xFF;
#endif 

#ifdef	DDL_CFG_RFID
	if(GetCardMode())
	{
		CardGotoReadStop();
	}
#elif	defined (DDL_CFG_IBUTTON)
	GMTouchKeyInputClear();
#elif	defined (DDL_CFG_DS1972_IBUTTON) || defined (_MASTERKEY_IBUTTON_SUPPORT)
	if(GetDS1972ModeProcess())
	{
		iButtonModeClear();
	}
#endif
	LockStatusCheckClear();
}
#endif 

#define MSP_SAVE_ADDRESS 0x1EFD
#define MSP_SAVE_COUNT 10
#define MSP_BOOT_COUNT_ADDRESS MSP_SAVE_ADDRESS+(4*MSP_SAVE_COUNT)

void SaveMSP2ROM(void)
{
	uint32_t address = (MSP_SAVE_ADDRESS + FLASH_EEPROM_BASE);

	for(uint8_t i = 0 ; i < MSP_SAVE_COUNT ; i++)
	{
		HAL_FLASHEx_DATAEEPROM_Unlock(); //속도 차가 안나서 딱 쓸때만 unlock 하도록 
		if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,*MSP_psp) != HAL_OK)
		{
			HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,*MSP_psp);
		}
		HAL_FLASHEx_DATAEEPROM_Lock();
		MSP_psp++;
		address+=4;
	}
}

void InitMSPROM(void)
{
	uint32_t address = (0x1EFD + FLASH_EEPROM_BASE);
	uint32_t wdata = 0x00000000;
	
	for(uint8_t i = 0 ; i < 10 ; i++)
	{
		HAL_FLASHEx_DATAEEPROM_Unlock(); 
		if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,wdata) != HAL_OK)
		{
			HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD,address,wdata);
		}
		HAL_FLASHEx_DATAEEPROM_Lock();
		address+=4;
	}

	/* reboot count */
	BootCount(1);
}

void BootCount(uint8_t clear)
{
	uint8_t data = 0xFF;
	
	if(clear)
	{
		RomWrite(&data,MSP_BOOT_COUNT_ADDRESS,1);
 	}
	else
	{
		RomRead(&data,MSP_BOOT_COUNT_ADDRESS,1);
		data++;
		RomWrite(&data,MSP_BOOT_COUNT_ADDRESS,1);		
	}
}
#endif 