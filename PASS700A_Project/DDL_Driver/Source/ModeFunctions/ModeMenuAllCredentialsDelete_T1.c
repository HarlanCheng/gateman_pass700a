//------------------------------------------------------------------------------
/** 	@file		ModeMenuAllCredentialsDelete_T1.c
	@version 0.1.00
	@date	2016.05.19
	@brief	All Credentials Delete Mode
	@remark	전체 카드/지문, 사용자 비밀번호, 일회용 비밀번호 삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.19		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"

static uint8_t bResetType = 0x00;

void ModeGotoAllCredentialsDelete(void)
{
	gbMainMode = MODE_MENU_ALLCREDENTIALS_DELETE;
	gbModePrcsStep = 0;
}

void ProcessDeleteAllCredentials(void)
{
	BYTE bTmp;

	bTmp = PincodeVerify(0);
	switch(bTmp)
	{
		case RET_MASTER_CODE_MATCH:
			OnetimeCodeDelete();
			AllUserCodeDelete();

#ifdef	DDL_CFG_RFID
			AllCardDelete();
//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);	//	completed, Press the star for setting other options or press the "R" button to finish
#ifndef	DDL_CFG_FP //지문 카드 동시에 지원 하는 경우 feedback 은 지문 쪽에서 
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);	//	completed, Press the star for setting other options or press the "R" button to finish
#endif			
			gbModePrcsStep++;
#endif		// ~DDL_CFG_RFID

#ifdef	DDL_CFG_IBUTTON
			AllGMTouchKeyDelete();
//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);	//	completed, Press the star for setting other options or press the "R" button to finish
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);	//	completed, Press the star for setting other options or press the "R" button to finish
			gbModePrcsStep++;
#endif  	// ~DDL_CFG_IBUTTON    

#ifdef	DDL_CFG_DS1972_IBUTTON
			AllDS1972TouchKeyDelete();
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);	//	completed, Press the star for setting other options or press the "R" button to finish
			gbModePrcsStep++;
#endif  	

			// All Credential Delete Event 전송

#ifdef	DDL_CFG_FP
			 //TCS4K 지문 모듈의 지문을 지우는 것은 MainMode를 변화시켜 처리한다.
			 gbModeChangeFlag4Finger = 2; 
			ModeGotoAdvancedAllFingerDelete2();
#endif
			PackTxEventCredentialDeleted(0xFF, 0xFF);

			break;

		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	
			ModeClear();
			break;				
	}
}




void ModeAllCredentialsDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(55, VOL_CHECK);				// Enter the master code to delete all credentials, then press the hash to continue.		
			FeedbackKeyPadLedOn(AVML_IN_MPIN_FOR_DELALL_SHARP, VOL_CHECK);				// Enter the master code to delete all credentials, then press the hash to continue.		

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:		 //# 버튼음 출력
					FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
					gbModePrcsStep++;
					break;		

				case TENKEY_STAR:
					ReInputOrModeMove(ModeGotoMenuMainSelect);
					break;
					
				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessDeleteAllCredentials();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			break;

		default:
			ModeClear();
			break;
	}
}



void ModeGotoAllDataReset(uint8_t data)
{
	gbMainMode = MODE_ALL_DATA_RESET;
	gbModePrcsStep = 0x00;

	if(data != 0xFF) 
	{
		bResetType = data;
	}
}

void ModeAllDataReset(void)
{
#ifdef DDL_AAAU_FACTORY_RESET_UI//호주 및 뉴질랜드 전용 Factory Reset UI
	BYTE bTmpArray[4];
#endif
#ifdef	_NEW_YALE_ACCESS_UI
	uint8_t bTmp;
	uint8_t bTmpBuffer[8];
#endif

	switch(gbModePrcsStep)
	{
		case 0:	
			if(!PackTx_QueueIsEmpty())
				break;

#if defined (P_BT_WAKEUP_T)
			if(!InnerPackTx_QueueIsEmpty())
				break;
#endif	
			//2020년 07월 14일 Screendoor만 이 printf가 활성화 되어 있으면 APP을 이용한 Factory reset수행시 Watchdog reset이 발생함 
			//또한 debuging시에만 사용하고 이 printf만 적절하지 않은 위치(test 용도가 아닌 실제 프로그램이 동작 하는 부분)에 위치 하고 있고
			//팀원 들과 상의 했을때 빼도 상관 없고 필요없다는 의견이 있어 주석 처리함 
			//printf("ModeAllDataReset [%d] \r\n",gbModePrcsStep);	
			
			TamperCountClear();
#ifndef	P_SNS_LOCK_T	
			InnerForcedLockClear();
#endif			
			gbDDLStateData = 0x00;							
			RomWrite(&gbDDLStateData, (WORD)DDL_STATE, 1);		

			OnetimeCodeDelete();

			gbModePrcsStep++;
			break;

		case 1:
#ifdef	_NEW_YALE_ACCESS_UI
			LockWorkingModeLoad();
			if(gbManageMode != _AD_MODE_SET)
			{
				RomRead(bTmpBuffer, USER_CODE, 8);
				RomWrite(bTmpBuffer, MASTER_CODE, 8);

				AdvancedModeSet();
			}
#endif
			AllUserCodeDelete();
			gbModePrcsStep++;
			break;

		case 2:	
#ifdef	DDL_CFG_RFID
			AllCardDelete();
#endif
			gbModePrcsStep++;
			break;

		case 3:
#ifdef	DDL_CFG_FP
			gbModeChangeFlag4Finger = 3;
			ModeGotoAdvancedAllFingerDelete2();
#else 
			gbModePrcsStep++;
#endif
			break;

		case 4: //master code 와 language 처리 
			switch(bResetType)
			{
				case 0x00:				
					MasterCodeDelete();
				case 0x01:
					break;

				case 0x02:
					MasterCodeDelete();
				case 0x03:
					/* config parameter */
#ifdef DDL_AAAU_FACTORY_RESET_UI //호주 및 뉴질랜드 전용 Factory Reset UI
					AutoRelockDisable();		//자동 잠김 해제 
					//AutoLockTimeAndAutoReLockTimeDefaultSetAndWrite();
					bTmpArray[0] = 0;
					bTmpArray[1] = 0;
					bTmpArray[2] = 0;
					bTmpArray[3] = 0;
					AutoLockTimeAndAutoReLockTimeCalculationAndWrite(bTmpArray);
#ifdef DDL_AAAU_AUTOLOCK_HOLD //호주 및 뉴질랜드 전용 autolock hold 기능			
					AutolockHoldSetting(AUTOLOCKUNHOLD);	//AutoHold 기능은 Unhold 상태로 설정
#endif			
#ifdef DDL_LED_DISPLAY_SELECT //led display 결정 
					LedDisplayEnableStateSet(0xFF);//LED Display Enable 0x00 = OFF, 0xFF = ON default is 0xFF or ON.
#endif

#else //DDL_AAAU_FACTORY_RESET_UI #else
					AutoRelockEnable();
					AutoLockTimeAndAutoReLockTimeDefaultSetAndWrite();
#endif //DDL_AAAU_FACTORY_RESET_UI #endif
					VolumeSetting(VOL_HIGH); //볼륨 LOW
#if defined(DDL_CFG_TOUCH_OC)
					SafeOCButtonSet();
#endif 

#ifdef DDL_LANGUAGE_DEFAULT_SET

#if defined 	(DDL_LANGUAGE_DEFAULT_KOREAN)
					LanguageSetting(LANGUAGE_KOREAN);
#elif defined (DDL_LANGUAGE_DEFAULT_CHINA)
					LanguageSetting(LANGUAGE_CHINESE);
#elif defined (DDL_LANGUAGE_DEFAULT_ENGLISH)
					LanguageSetting(LANGUAGE_ENGLISH);
#elif defined (DDL_LANGUAGE_DEFAULT_TAIWANESE)
					LanguageSetting(LANGUAGE_TAIWANESE);
#elif defined (DDL_LANGUAGE_DEFAULT_SPANISH)
					LanguageSetting(LANGUAGE_SPANISH);
#elif defined (DDL_LANGUAGE_DEFAULT_PORTUGUESE)
					LanguageSetting(LANGUAGE_PORTUGUESE);
#endif

#else	//DDL_LANGUAGE_DEFAULT_SET #else
					LanguageSetDefault();
#endif	//DDL_LANGUAGE_DEFAULT_SET #endif
					break;

				default:
					// ModeClear();
					break;
			}

			bResetType = 0;
			FactoryResetDoneCheck();
			/* to Do */
			/* send alarm report */

#ifdef	_NEW_YALE_ACCESS_UI
			gbModePrcsStep++;
				
#else
			ModeClear();
#endif
			break;

#ifdef	_NEW_YALE_ACCESS_UI
		case 5:
			PackTx_MakePacket(F0_RFM_CHECK, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);
			
			SetYaleAccessMode(YALE_ACCESS_MODE_INIT);
			SetModeTimeOut(20);
			gbModePrcsStep++;
			break;

		case 6:
			if(GetModeTimeOut() != 0)	break;

			if(Is_Yale_Access_Deleted() == true)
			{
				SetPackCBARegisterSet(0);
			}
			else
			{
				SetPackCBARegisterSet(1);
			}

			ModeClear();
			break;
#endif	// _NEW_YALE_ACCESS_UI

		default:
			ModeClear();
			break;
	}
}



