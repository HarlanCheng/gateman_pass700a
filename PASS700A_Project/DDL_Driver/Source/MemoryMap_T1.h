//------------------------------------------------------------------------------
/** 	\file		MemoryMap_T1.h
	@version 1.1.00
	@date	2018.05.17
	@brief	Doorlock Memoey Map(24C08, 신규 UI) Header		
	@see	SerialEeprom_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.03.31		by Jay
			- 24C08 Serial EEPROM에 적합하도록 설계
			- 신규 UI에 맞도록 설계
			- Card 40개, User Code 30개, Fingerprint 20개 

		V0.1.01 2016.05.18		by Jay
			- MANAGE_MODE 추가
	
		V0.1.02 2016.07.07		by Jay
			- OCBUTTON_MODE 추가

		V0.1.02 2016.07.28		by Jay
			- ALL_LOCK_OUT 추가

		V0.1.03 2016.09.05		by hyojoon
			- 지문 모듈 20 -> 40 개 저장 가능 하도록 추가 	

		V0.1.04 2016.09.07		by hyojoon
			- DS1972 IBUTTON 추가  

		V0.1.05 2016.12.13		by Jay
			- OPEN_UID_EN 내용 추가  

		V0.1.06 2017.03.23		by Jay
			- Z-Wave CTT 테스트 Configuration Parameter 저장 장소 추가  

		V1.1.00 2017.05.17		by Jay
			- 일본향 제품의 MASTER CARD 저장 장소 추가	
*/
//------------------------------------------------------------------------------

#ifndef __MEMORYMAP_T1_INCLUDED
#define __MEMORYMAP_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"

#if defined (DDL_CFG_MS)
//------------------------------------------------------------------------------
//	EEPROM MEMORY MAP
//------------------------------------------------------------------------------
// 공용 사용 영역 (0x000 ~ 0x02F) - 임의 변경 금지
//------------------------------------------------------------------------------
/*! 	\def	ROM_TEST		
	Serial EEPROM 통신 테스트에 사용되는 내용을 저장	1 byte * 1
	
	\def	DDL_STATE		
	Doorlock Status Flag 내용 저장					1 byte * 1

	\def	WRONG_NUM		
	3분락 진입을 위한 인증 시도 실패 회수 저장			1 byte * 1

	\def	KEY_NUM 	
	카드, iButton, 지문 등의 인증 수단의 등록된 개수 저장	1 byte * 1

	\def	VOICE_STATE 	
	Volume 설정 상태 저장 (고음, 저음, 무음 등) 			1 byte * 1

	\def	RIGHT_LEFT		
		좌수/우수 설정 상태 저장						1 byte * 1

	\def	INLOCK_STATE		
	내부강제잠금 설정 상태 저장						1 byte * 1

	\def	LANG_MODE		
	음성 안내 언어 설정 상태 저장						1 byte * 1

	\def	WORKING_MODE		
	동작 모드 저장 (Easy Mode, Advanced Mode 등)		1 byte * 1

	\def	FACTORY_RST		
	Factory Reset 진행 내용 저장						1 byte * 1

	\def	MANAGE_MODE 	
	Normal Mode/Advanced Mode 상태 저장			1 byte * 1

	\def	OCBUTTON_MODE 	
	이중 열림/닫힘 버튼 설정 상태 저장 				1 byte * 1

	\def	ALL_LOCK_OUT 	
	전체 LOCKOUT 설정 상태 저장					1 byte * 1

	\def	OPEN_UID_EN 	
	카드 제품에서 Open UID 기능 지원 여부 내용 저장		1 byte * 1
*/	
//---------------------------------------------------------------------------
#define	ROM_TEST		0x00						
#define	DDL_STATE		(ROM_TEST+1)
#define	WRONG_NUM		(ROM_TEST+2)					 	
#define	KEY_NUM		(ROM_TEST+3)	
#define	VOICE_STATE		(ROM_TEST+4)	
#define	RIGHT_LEFT		(ROM_TEST+5) 
#define	INLOCK_STATE	(ROM_TEST+6)	
#define	LANG_MODE		(ROM_TEST+7)	
#define	WORKING_MODE	(ROM_TEST+8)	
#define	FACTORY_RST	(ROM_TEST+9)
#define	MANAGE_MODE	(ROM_TEST+10)
#define	OCBUTTON_MODE	(ROM_TEST+11)
#define	ALL_LOCK_OUT	(ROM_TEST+12)
#define	OPEN_UID_EN	(ROM_TEST+13)

//------------------------------------------------------------------------------
/*! 	\def	PACK_ID		
	통신팩 ID 저장
	4 byte * 1
*/
//---------------------------------------------------------------------------
#define	PACK_ID			0x010


//------------------------------------------------------------------------------
/*! 	\def	INNER_PACK_ID		
	통신팩 ID 저장
	4 byte * 1
*/
//---------------------------------------------------------------------------
#define	INNER_PACK_ID	0x014


//------------------------------------------------------------------------------
/*! 	\def	DOORLOCK_ID		
	도어록 ID 저장
	16 byte * 1
*/
//---------------------------------------------------------------------------
#define	DOORLOCK_ID			0x020



//------------------------------------------------------------------------------
/*! 	\def	MS_CREDENTIAL_DATA_1		
	
	8 byte * 1

	|	Byte1	Byte2	Byte3	Byte4	Byte5	Byte6	Byte7	|	Byte8	|
	|															|비밀번호 자리수|
	|	비밀번호 or 카드 UID											|인증 수당 구분	|


	Byte 8
	|	Bit7		Bit6		Bit5		Bit4		|	Bit3		Bit2		Bit1		Bit0		|
	|	인증 수단 구분 						|	비밀 번호 자리 수 					|
	|	0x00				Manager Card		|	0x00				Reserved 			|
	|	0x10				Manager Code		|	0x01				4자리 			|
	|	0x20				Master Card		|	0x02				5자리			|
	|	0x30				Master Code		|	0x03				6자리			|
	|	0x40				User Card			|	0x04				7자리			|
	|	0x50				User Code		|	0x05				8자리			|
	|	0x60				Reserved			|	0x06				9자리			|	
	|	0x70				Reserved			|	0x07				10자리			|		
	
	ex ) 0x12345FFFFFFFFF12 : 비밀 번호가 1 2 3 4 5 이면서 Manager code 이고 5자리 

	0x30 부터 0x348		100 개 
*/
//---------------------------------------------------------------------------
#define	MS_CREDENTIAL_DATA_1		0x30
#define	MS_CREDENTIAL_DATA_100	0x348


/* 인증 수단 별 등록 된 갯수 */
#define	MS_MANAGER_CARD_NUM			0x350
#define	MS_MASTER_CARD_NUM			0x351
#define	MS_USER_CARD_NUM			0x352
#define	MS_MANAGER_CODE_NUM			0x353
#define	MS_MASTER_CODE_NUM			0x354
#define	MS_USER_CODE_NUM			0x355

/* PC 생산 지그 프로그램 사용시 일시 적으로 사용 하는 code 삭제를 따로 하지는 않는다 */
#define	MS_TEMP_PINCODE			0x358


/*	0x360 ~ 0x3BF Reserved */

//------------------------------------------------------------------------------
/*! 	\def	MS_MANAGER_CARD_INFO	~ 		MS_USER_CODE_INFO
	8 byte * 1
	각 인증 수단의 정보 설정
	인증 수단의 개수가 0이거나 255일 경우에는 모두 0으로 처리한다.
	인증 수단의 개수가 0이거나 255일 경우에는 나머지 권한 설정 byte는 모두 무시된다.
	각 인증 수단의 개수 총합은 최대 지원 개수를 넘을 수 없다.
	해당 설정은 개발 단계에서 정의하여 이관하는 방법도 있고, 양산 단계에서 정의하여 출하하는 방법도 있음.

	|	Byte1	|	Byte2	Byte3	Byte4	Byte5	Byte6	Byte7	Byte8	|
	|인증 수단의 수	|						권한 설정 								|

	Byte 1 : 인증 수단의 개수 (1~254)

	우선 순위가 더 높은 인증 수단을 등록 가능하도록 설정하더라도, 실제 처리되지 않는다. 예를 들면, User Card의 권한을 “Manager Card 등록 가능”으로 설정한다 하더라도, 실제 동작되지 않는다.
	권한 설정 2 ~ 권한 설정 7까지는 추후 정의 (권한 설정 Default : 0xFF)
	Byte 2 ~ Byte 8 : 권한 설정 1 ~ 7 (0~7 Bit)
	Bit 0 : Manager Card	등록 / 삭제 
	Bit 1 : Manager Code	등록 / 삭제 
	Bit 2 : Master Card		등록 / 삭제 
	Bit 3 : Master Code		등록 / 삭제 
	Bit 4 : User Card		등록 / 삭제 
	Bit 5 : User Code		등록 / 삭제 
	Bit 6 : Menu 진입 가능 
	Bit 7 : Motor Open 가능 

*/
//---------------------------------------------------------------------------
#define	MS_MANAGER_CARD_INFO			0x360
#define	MS_MASTER_CARD_INFO			0x368
#define	MS_USER_CARD_INFO			0x370
#define	MS_MANAGER_CODE_INFO			0x378
#define	MS_MASTER_CODE_INFO			0x380
#define	MS_USER_CODE_INFO			0x388
//---------------------------------------------------------------------------
/*
	MS 사양중 특수 사양에 대한 정류를 구분 한다.
	프로그램 소스에서는 해당 구분에 따라 처리될 수 있도록 누적되어 구현된다.
	특수 사양에 대한 구분은 1byte 코드를 이용하며, 0~254의 범위 중 한 가지 값으로 설정된다.
	1 byte * 1
	0x00 : No Special Specification		기본 기능
	0x01 : V100-F-MS(LH)			관리자 카드(Manager Card)를  이용한 초기화 기능  (사용자 카드(User Card) 초기화, 비밀번호(User Code) “0000”으로 초기화)
	0x02 : G7-MS(영남대)			마스터 카드(Master Card)를  이용해 사용자 비밀번호(User Code) 변경할 경우 사용자 카드(User Card) 전체 삭제 기능
	0x03 : S170-MS(인천대 기숙사)		마스터 카드(Master Card) 사용에 의한 사용자 카드(User Card) 전체 삭제 기능 필요
	0x04 : V20-MS(청와대)			사용자 카드(User Card) 등록은 추가 등록 개념
								관리자 카드(Manager Card)를  이용한 사용자 카드(User Card) 개별 삭제 기능 (삭제할 사용자 카드의 입력이 있어야 함) 
								관리자 카드(Manager Card)를  이용한 사용자 카드(User Card) 전체 삭제 기능
	...
	Reserved
	...
	0xff : Not Used
*/
//---------------------------------------------------------------------------
#define	EXCLUSIVE_FUNCTIONS	0x390

//------------------------------------------------------------------------------
/*! 	\def	MP_HISTORY		
	통신팩 ID 저장
	1 byte * 5
	MP_HISTORY_COUNTER : 총 초기화한 count 저장 
	MP_HISTORY_COUNTER+1~4 : 초기화한 수단 (종류) 저장 
	
*/
//---------------------------------------------------------------------------
#define	MP_HISTORY_COUNTER			0x18


//------------------------------------------------------------------------------
/*! 	\def	SILENT_RESET		
	SILENT_RESET 
	gwLockModeStatus 내용 저장 
	24 byte
*/
//---------------------------------------------------------------------------
#define	LOCK_MODE_STATUS		0x3E0 


/* 여기서 부터 하위 define 들은 compile error 때문에 남겨 둔다 */
#define	SUPPORTED_USERCODE_NUMBER		30
#define	SUPPORTED_USERCARD_NUMBER		40
#define	SUPPORTED_USERKEY_NUMBER		40
#define	SUPPORTED_DS1972KEY_NUMBER	20
#ifdef 	DDL_CFG_FP //hyojoon_20160831 PFM-3000 add
#define	SUPPORTED_FINGERPRINT_NUMBER	_MAX_REG_BIO  
#endif 


#define	USER_STATUS		0x3FF
#define	USER_SCH_STA	0x3FF
#define	USER_SCHEDULE	0x3FF
#define	ONETIME_CODE	0x3FF
#define	MASTER_CODE		0x3FF
#define	USER_CODE		0x3FF
#define	CARD_UID		0x3FE
#define	TOUCHKEY_UID	0x3FF
#define	TOUCHKEY_FV		0x3FF
#define	TOUCHKEY_RC	0x3FF
#define	BIO_ID			0x3FF						
#define	DDL_HWMODEL_CODE	0x3FF
#define	RELOCK_TIME				0x3FF
#define	WRONG_CODE_ENTRY_LIMIT	0x3FF
#define	SHUT_DOWN_TIME			0x3FF
#define	OPERATING_MODE			0x3FF
#define	ONE_TOUCH_LOCKING		0x3FF
#define	PRIVACY_BUTTON			0x3FF
#define	LOCK_STATUS_LED			0x3FF



#else 	// DDL_CFG_MS

#define	SUPPORTED_USERCODE_NUMBER		30
#define	SUPPORTED_USERCARD_NUMBER		40
#define	SUPPORTED_USERKEY_NUMBER		40
#define	SUPPORTED_DS1972KEY_NUMBER	20
#ifdef 	DDL_CFG_FP //hyojoon_20160831 PFM-3000 add
#define	SUPPORTED_FINGERPRINT_NUMBER	_MAX_REG_BIO  
#endif 

#ifdef	_MASTER_CARD_SUPPORT
#define	SUPPORTED_MASTERCARD_NUMBER		5
#endif	

//------------------------------------------------------------------------------
//	EEPROM MEMORY MAP
//------------------------------------------------------------------------------
// 공용 사용 영역 (0x000 ~ 0x02F) - 임의 변경 금지
//------------------------------------------------------------------------------
/*! 	\def	ROM_TEST		
	Serial EEPROM 통신 테스트에 사용되는 내용을 저장	1 byte * 1
	
	\def	DDL_STATE		
	Doorlock Status Flag 내용 저장					1 byte * 1

	\def	WRONG_NUM		
	3분락 진입을 위한 인증 시도 실패 회수 저장			1 byte * 1

	\def	KEY_NUM 	
	카드, iButton, 지문 등의 인증 수단의 등록된 개수 저장	1 byte * 1

	\def	VOICE_STATE 	
	Volume 설정 상태 저장 (고음, 저음, 무음 등) 			1 byte * 1

	\def	RIGHT_LEFT		
		좌수/우수 설정 상태 저장						1 byte * 1

	\def	INLOCK_STATE		
	내부강제잠금 설정 상태 저장						1 byte * 1

	\def	LANG_MODE		
	음성 안내 언어 설정 상태 저장						1 byte * 1

	\def	WORKING_MODE		
	동작 모드 저장 (Easy Mode, Advanced Mode 등)		1 byte * 1

	\def	FACTORY_RST		
	Factory Reset 진행 내용 저장						1 byte * 1

	\def	MANAGE_MODE 	
	Normal Mode/Advanced Mode 상태 저장			1 byte * 1

	\def	OCBUTTON_MODE 	
	이중 열림/닫힘 버튼 설정 상태 저장 				1 byte * 1

	\def	ALL_LOCK_OUT 	
	전체 LOCKOUT 설정 상태 저장					1 byte * 1

	\def	OPEN_UID_EN 	
	카드 제품에서 Open UID 기능 지원 여부 내용 저장		1 byte * 1
*/	
//---------------------------------------------------------------------------
#define ROM_TEST		0x00						
#define DDL_STATE		(ROM_TEST+1)
#define WRONG_NUM		(ROM_TEST+2)					 	
#define KEY_NUM			(ROM_TEST+3)	
#define VOICE_STATE		(ROM_TEST+4)	
#define RIGHT_LEFT		(ROM_TEST+5) 
#define INLOCK_STATE	(ROM_TEST+6)	
#define LANG_MODE		(ROM_TEST+7)	
#define WORKING_MODE	(ROM_TEST+8)	
#define FACTORY_RST		(ROM_TEST+9)
#define	MANAGE_MODE		(ROM_TEST+10)
#define	OCBUTTON_MODE	(ROM_TEST+11)
#define	ALL_LOCK_OUT	(ROM_TEST+12)
#define	OPEN_UID_EN		(ROM_TEST+13)



//------------------------------------------------------------------------------
/*! 	\def	PACK_ID		
	통신팩 ID 저장
	4 byte * 1
*/
//---------------------------------------------------------------------------
#define	PACK_ID			0x010


//------------------------------------------------------------------------------
/*! 	\def	INNER_PACK_ID		
	통신팩 ID 저장
	4 byte * 1
*/
//---------------------------------------------------------------------------
#define	INNER_PACK_ID			0x014

//------------------------------------------------------------------------------
/*! 	\def	MP_HISTORY		
	통신팩 ID 저장
	1 byte * 5
	MP_HISTORY_COUNTER : 총 초기화한 count 저장 
	MP_HISTORY_COUNTER+1~4 : 초기화한 수단 (종류) 저장 
	
*/
//---------------------------------------------------------------------------
#define	MP_HISTORY_COUNTER			0x18

//------------------------------------------------------------------------------
/*! 	\def	DOORLOCK_ID		
	도어록 ID 저장
	16 byte * 1
*/
//---------------------------------------------------------------------------
#define DOORLOCK_ID		0x020



// 권장 사용 영역 (0x030 ~ 0x34F)
//------------------------------------------------------------------------------
/*! 	\def	USER_STATUS		
	User Code의 사용 상태 저장 (Available, Occupied & Enable, Occupied & Disable 등)
	1 byte * 30
*/
//---------------------------------------------------------------------------
#define USER_STATUS		0x030


//------------------------------------------------------------------------------
/*! 	\def	USER_SCH_STA		
	User Code의 Schedule 상태 저장 (Schedule Enale/Disable 등)
	1 byte * 30
*/
//---------------------------------------------------------------------------
#define USER_SCH_STA	0x050


//------------------------------------------------------------------------------
/*! 	\def	USER_SCHEDULE		
	User Code의 Schedule 저장 (4byte 단위)
	4 byte * 30
*/
//---------------------------------------------------------------------------
#define USER_SCHEDULE	0x070


//------------------------------------------------------------------------------
/*! 	\def	ONETIME_CODE		
	One Time Code 저장 (8byte 단위)
	8 byte * 1
*/
//---------------------------------------------------------------------------
#define ONETIME_CODE	0x0E8


//------------------------------------------------------------------------------
/*! 	\def	MASTER_CODE		
	Master Code 저장 (8byte 단위)
	8 byte * 1
*/
//---------------------------------------------------------------------------
#define MASTER_CODE		0x0F0


//------------------------------------------------------------------------------
/*! 	\def	USER_CODE		
	User Code 저장 (8byte 단위)
	8 byte * 30
*/
//---------------------------------------------------------------------------
#define USER_CODE		0x0F8


//------------------------------------------------------------------------------
/*! 	\def	CARD_UID		
	Card UID 저장 (4byte 단위)
	4 byte * 40

	Card UID 저장 (8byte 단위)
	8 byte * 40
*/
//---------------------------------------------------------------------------
#define CARD_UID		0x1E8


//------------------------------------------------------------------------------
/*! 	\def	TOUCHKEY_UID		
	Touch Key UID 저장 (8byte 단위)
	8 byte * 게이트맨 40 DS1972 20 
*/
//---------------------------------------------------------------------------
#define	TOUCHKEY_UID	0x1E8


//------------------------------------------------------------------------------
/*! 	\def	TOUCHKEY_FV		
	Touch Key UID 저장 (4byte 단위)
	4 byte * 20
*/
//---------------------------------------------------------------------------
#define	TOUCHKEY_FV		0x288

//------------------------------------------------------------------------------
/*! 	\def	TOUCHKEY_RC		
	Touch Key UID 저장 (1byte 단위)
	1 byte * 20
*/
//---------------------------------------------------------------------------
#define	TOUCHKEY_RC	0x2D8


//------------------------------------------------------------------------------
/*! 	\def	BIO_ID		
	Fingerprint ID 저장 (2byte 단위)
	2 byte * 20  -> 2byte * 40 
*/
//---------------------------------------------------------------------------
#define	BIO_ID			0x328						

//------------------------------------------------------------------------------
/*! 	\def	ENCRYPT_KEY		
	AES128 16byte key 값 저장 
	16 byte 
*/
//---------------------------------------------------------------------------
#define	ENCRYPT_KEY	0x378 

//------------------------------------------------------------------------------
/*! 	\def	ENCRYPT_KEY		
	HFPM_MODULE_ID 값 저장 
	16 byte 
*/
//---------------------------------------------------------------------------
#define	HFPM_MODULE_ID	0x388 

//------------------------------------------------------------------------------
/*! 	\def	COM_PACK_ENCRYPT_KEY		
	uart1 comm pack key 
	16 byte 
*/
//---------------------------------------------------------------------------
#define	COM_PACK_ENCRYPT_KEY		0x3C0 

//------------------------------------------------------------------------------
/*! 	\def	INNER_PACK_ENCRYPT_KEY		
	uart2 inner pack key 
	16 byte 
*/
//---------------------------------------------------------------------------
#define	INNER_PACK_ENCRYPT_KEY	0x3D0 


#ifdef	_MASTER_CARD_SUPPORT
#define MASTER_CARD_UID	0x398
#endif

//------------------------------------------------------------------------------
/*! 	\def	SILENT_RESET		
	SILENT_RESET 
	gwLockModeStatus 내용 저장 
	24 byte
*/
//---------------------------------------------------------------------------
#define	LOCK_MODE_STATUS		0x3E0 

// 임의 사용 영역 (0x03C0 ~ 0x3FF) - 개발자가 임의로 설정 사용 가능

//------------------------------------------------------------------------------
/*! 	\def	DDL_HWMODEL_CODE
	유사한 구조인 HW에서 model별 차이를 식별하기 위한 변수
	각 Firmware에서 임의로 값을 사용한다.
	Example)
		0xFF, 0x00 : Default model (base model), 1st HW type
		0x01 : 2nd HW type
		0x03 : 3rd HW type
	1 byte
*/
//---------------------------------------------------------------------------
// #define	DDL_HWMODEL_CODE	0x378 사용 하지 않음 삭제 




#define	RELOCK_TIME				0x3F8
#define	WRONG_CODE_ENTRY_LIMIT	(RELOCK_TIME+1)
#define	SHUT_DOWN_TIME			(RELOCK_TIME+2)
#define	OPERATING_MODE			(RELOCK_TIME+3)
#define	ONE_TOUCH_LOCKING		(RELOCK_TIME+4)
#define	PRIVACY_BUTTON			(RELOCK_TIME+5)
#define	LOCK_STATUS_LED			(RELOCK_TIME+6)

#endif 		// DDL_CFG_MS




//------------------------------------------------------------------------------
// ST 마이컴 내부 사용 영역 (0x400 ~ 0xBFF)
//------------------------------------------------------------------------------
#if (DDL_CFG_BLE_30_ENABLE >= 0x29) && defined (_USE_ST32_EEPROM_)
#define	SCHEDULE_YEAR_DAY_PIN		0x400
#define	SCHEDULE_DAILY_REPEAT_PIN	0x588

#define	SCHEDULE_YEAR_DAY_CARD		0x640
#define	SCHEDULE_DAILY_REPEAT_CARD	0x848

#define	SCHEDULE_YEAR_DAY_FINGERPRINT		0x938
#define	SCHEDULE_DAILY_REPEAT_FINGERPRINT	0xB40

#define	MODEL_IDENTIFIER_STRING		0xFF0
#endif



#endif
