//------------------------------------------------------------------------------
/** 	@file		ModeGMTouchKeyVerify_T1.c
	@brief	GateMan TouchKey Verify Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEGMTOUCHKEYVERIFY_T1_INCLUDED
#define __MODEGMTOUCHKEYVERIFY_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


void ModeGotoGMTouchKeyVerify(void);
void ModeGMTouchKeyVerify(void);


BYTE GMTouchKeyVerify(void);


#endif


