#ifndef	__SYSTEMTICK_INCLUDED
#define	__SYSTEMTICK_INCLUDED

#include "DefineMacro.h"


//TIM4에 의해 설정된 clock 100kHz(10usec)
//TIM4의 counter가 16bit라서 최대 tick 값은 65535
#define	MAIN_CLOCK_FREQ		100000
#define	MAX_SYS_TICK_VALUE	65535

#define	SYS_TICK_100US			10
#define	SYS_TICK_1MS			100
#define	SYS_TICK_2MS			200


#define	SYS_TIMER_10US			(1)
#define	SYS_TIMER_1MS			(100)

#define	SYS_TIMER_20US		(2*SYS_TIMER_10US)
#define	SYS_TIMER_30US		(3*SYS_TIMER_10US)
#define	SYS_TIMER_40US		(4*SYS_TIMER_10US)
#define	SYS_TIMER_50US		(5*SYS_TIMER_10US)

#define	SYS_TIMER_100US		(10*SYS_TIMER_10US)
#define	SYS_TIMER_200US		(20*SYS_TIMER_10US)
#define	SYS_TIMER_300US		(30*SYS_TIMER_10US)
#define	SYS_TIMER_400US		(40*SYS_TIMER_10US)
#define	SYS_TIMER_500US		(50*SYS_TIMER_10US)

#define	SYS_TIMER_2MS			(2*SYS_TIMER_1MS)
#define	SYS_TIMER_3MS			(3*SYS_TIMER_1MS)
#define	SYS_TIMER_4MS			(4*SYS_TIMER_1MS)
#define	SYS_TIMER_5MS			(5*SYS_TIMER_1MS)
#define	SYS_TIMER_6MS			(6*SYS_TIMER_1MS)
#define	SYS_TIMER_7MS			(7*SYS_TIMER_1MS)
#define	SYS_TIMER_8MS			(8*SYS_TIMER_1MS)
#define	SYS_TIMER_9MS			(9*SYS_TIMER_1MS)
#define	SYS_TIMER_10MS		(10*SYS_TIMER_1MS)
#define	SYS_TIMER_15MS		(15*SYS_TIMER_1MS)
#define	SYS_TIMER_20MS		(20*SYS_TIMER_1MS)
#define	SYS_TIMER_30MS		(30*SYS_TIMER_1MS)

#define	SYS_TIMER_100MS		(100*SYS_TIMER_1MS)


extern WORD gModeTimeOutTimer100ms;
extern WORD gbWakeUpMinTime10ms;
extern uint32_t gwBasicTimerInitCnt ;


#ifdef	_SYSTEM_TICK_C_

uint32_t	systick_2ms_flag;

#else

extern	uint32_t	systick_2ms_flag;

#endif


void BasicTimer2ms(void);

uint32_t SystickLoad(void);
void SysTimerStart(uint32_t *wData);
BYTE SysTimerEndCheck(uint32_t wData, uint32_t wLimit);

void SysTickEnable(void);
void SysTickDisable(void);

void SystickVariableInit(void);
void TamperProofLockoutCounter(void);
void Delay(uint32_t dlyTicks);


#endif

