#define		_MODE_FINGER_DELETE_C_

#include "main.h"

extern	BYTE		no_fingers;
extern	BYTE 	FingerReadBuff[4];
extern	BYTE 	FingerWriteBuff[4];

#define	 _MODE_TIME_OUT_5S			50

void	ModeGotoNormalAllFingerDelete( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 0;
#ifdef	BLE_N_SUPPORT
	gbEnterMode = 0x00;
#endif 
}


void	ModeGotoAdvancedAllFingerDelete1( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
#ifdef	DDL_CFG_DIMMER	
	gbModePrcsStep = 1;			// 2; //이 함수를 부르는 함수에서 gbModeProcsStep을 하나 증가 시키므로 여기서는 원래 가고자 하는 값보다 1 작게 해야 한다.
#else 
	gbModePrcsStep = 2; 		
#endif 
}

void	ModeGotoAdvancedAllFingerDelete2( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 2;
}

#ifdef	BLE_N_SUPPORT
void	ModeGotoAllFingerDeleteByBleN( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 2;
	gbEnterMode = 0x01;
}
#endif 

void	ModeAllFingerDelete( void )
{
	BYTE	bTmp;

	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_PIN_SHARP, VOL_CHECK);		//비밀번호를 입력하세요. 계속하시려면 샵 버턴을 누르세요.
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_20S);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
					switch(PincodeVerify(0))
					{
						case 1:
							gbModePrcsStep++;
							break;

						case RET_NO_INPUT:
						case RET_WRONG_DIGIT_INPUT:
							FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
							ModeClear();
							break;

						default:
							gbModePrcsStep=20;
							break;
					}
					break;		

				case TENKEY_STAR:
					if(GetSupportCredentialCount() > 1)
					{
						ReInputOrModeMove(ModeGotoMenuCredentialSelect);
					}
					else 
					{
						ReInputOrModeMove(ModeGotoMenuMainSelect);
					}
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					BioOffModeClear();
					break;
			}	
			break;

		case 2:
			SetModeTimeOut(_MODE_TIME_OUT_5S);
			gbModePrcsStep++;
			break;
			
		case 3:
			if(GetModeTimeOut() == 0){
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			HFPMTxEventDelete(0xFF);
			SetModeTimeOut(_MODE_TIME_OUT_20S);
			gbModePrcsStep++;
			break;

		case 4: 		
			if(GetModeTimeOut() == 0){
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == IREVO_HFPM_SUB_EVENT_FP_DELETE)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case IREVO_HFPM_RESULT_CODE_STATUS_OK:	//명령 성공 
							gbModePrcsStep = 5;
							break;		

						case IREVO_HFPM_RESULT_CODE_PACKET_ERR:	
						case IREVO_HFPM_RESULT_CODE_CHECK_SUM_ERR:	
						case IREVO_HFPM_RESULT_CODE_SLOT_EMPTY:	
						default :
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}				
			}
			break;
		
		case 5:
			if(GetModeTimeOut() == 0){
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			no_fingers = 0;
			RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장

			FingerReadBuff[0] = FingerReadBuff[1] = 0xFF;
			for(bTmp = 0; bTmp< _MAX_REG_BIO; bTmp++){		// 모두 초기화..
				RomWrite(FingerReadBuff, (WORD)BIO_ID+(2*bTmp), 2);
			}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			ScheduleDeleteOfAllUserFingerprint();
#endif

			DDLStatusFlagClear(DDL_STS_FAIL_BIO);

			if( PowerDownModeForJig() == STATUS_SUCCESS ) { // jigmode의 동작이면
				FeedbackModeCompleted(AVML_DEL_ALL_FINGER_DONE_MSG, VOL_CHECK);	//	등록된 모든 지문이 모두 삭제 되었습니다.
				BioOffModeClear();
			}
			else 
			{
				BioModuleOff();
				if ( gbModeChangeFlag4Finger == 1 ) {		//모드 전환을 위해 전체 지문을 지운 것이라면 pincode 디스플레이를 한다.
					gbModePrcsStep = 7;
					gbModeChangeFlag4Finger= 0;
				}
				else if ( gbModeChangeFlag4Finger == 2 ) {
					// 안심모드에서 5번으로 전체 삭제일 경우 
					// 이경우 pack 으로 0xFF 0xFF 를 ProcessDeleteAllCredentials() 보내도록 수정 여긴 바로 break					
					gbModePrcsStep = 9;
					gbModeChangeFlag4Finger= 0;
					FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
					break;
				}
				else if(FactoryResetRunCheck() == STATUS_SUCCESS) // factory 중이라면  //hyojoon_20160922
				{
					ModeClear();
				}
#ifdef	BLE_N_SUPPORT
				else if(gbEnterMode)
				{
					gbEnterMode = 0x00;
					BioOffModeClear();
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					ModeClear();
				}
#endif 				
				else 
				{
					FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
					gbModePrcsStep = 8;
				}
			
				// 지문 전체 삭제 정보 전송
				PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, 0xFF);
				// 지문 전체 삭제 정보 전송
			}
			break;

		case 7:		// display pincode after changing mode (normal <-> advanced)
			DisplayRegisteredPincode(PincodeRegisterCompleteProcess);
			//위의 함수 호출 후 mode clear가 일어난다.
			break;

		case 8:
			if(GetSupportCredentialCount() > 1)
			{
				GotoPreviousOrComplete(ModeGotoMenuCredentialSelect);
			}
			else 
			{
				GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			}			
			//위 함수호출로 mode clear가 일어날 수 있다.
			break;

		case 9:
			// 안심모드에서 5번으로 전체 삭제일 경우 
			GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			//위 함수호출로 mode clear가 일어날 수 있다.
			break;

		case 20:		//비밀번호 인증이 틀린 경우 에러처리
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			BioOffModeClear();
			break;

		case 200:		
			BioOffModeClear();
			break;
		default:
			BioOffModeClear();
			break;
	}
}

