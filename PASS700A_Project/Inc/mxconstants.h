/**
  ******************************************************************************
  * File Name          : mxconstants.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define JIG_POINT_Pin GPIO_PIN_2
#define JIG_POINT_GPIO_Port GPIOE
#define IBUTTON_COVER_RFID_GND_Pin GPIO_PIN_3
#define IBUTTON_COVER_RFID_GND_GPIO_Port GPIOE
#define OP_RFID_Pin GPIO_PIN_4
#define OP_RFID_GPIO_Port GPIOE
#define OP_FP_Pin GPIO_PIN_5
#define OP_FP_GPIO_Port GPIOE
#define OP_VOICE_Pin GPIO_PIN_6
#define OP_VOICE_GPIO_Port GPIOE
#define COM_DDL_EN_Pin GPIO_PIN_13
#define COM_DDL_EN_GPIO_Port GPIOC
#define _BT_WAKEUP_Pin GPIO_PIN_14
#define _BT_WAKEUP_GPIO_Port GPIOC
#define _N_C_Pin GPIO_PIN_15
#define _N_C_GPIO_Port GPIOC
#define _VOL_OUT_Pin GPIO_PIN_0
#define _VOL_OUT_GPIO_Port GPIOH
#define LOW_BAT_EN_Pin GPIO_PIN_1
#define LOW_BAT_EN_GPIO_Port GPIOH
#define _RFID_DETECT_Pin GPIO_PIN_0
#define _RFID_DETECT_GPIO_Port GPIOC
#define _VOL_IN_Pin GPIO_PIN_1
#define _VOL_IN_GPIO_Port GPIOC
#define _FIRE_AD_Pin GPIO_PIN_2
#define _FIRE_AD_GPIO_Port GPIOC
#define LOWBAT_AD_Pin GPIO_PIN_3
#define LOWBAT_AD_GPIO_Port GPIOC
#define RFID_IO00_Pin GPIO_PIN_0
#define RFID_IO00_GPIO_Port GPIOA
#define RFID_IO01_Pin GPIO_PIN_1
#define RFID_IO01_GPIO_Port GPIOA
#define RFID_IO02_Pin GPIO_PIN_2
#define RFID_IO02_GPIO_Port GPIOA
#define RFID_IO03_Pin GPIO_PIN_3
#define RFID_IO03_GPIO_Port GPIOA
#define RFID_IO04_Pin GPIO_PIN_4
#define RFID_IO04_GPIO_Port GPIOA
#define RFID_IO05_Pin GPIO_PIN_5
#define RFID_IO05_GPIO_Port GPIOA
#define RFID_IO06_Pin GPIO_PIN_6
#define RFID_IO06_GPIO_Port GPIOA
#define RFID_IO07_Pin GPIO_PIN_7
#define RFID_IO07_GPIO_Port GPIOA
#define _SW_AUTO_Pin GPIO_PIN_4
#define _SW_AUTO_GPIO_Port GPIOC
#define RFID_IRQ_Pin GPIO_PIN_5
#define RFID_IRQ_GPIO_Port GPIOC
#define TKEY_SDA_Pin GPIO_PIN_0
#define TKEY_SDA_GPIO_Port GPIOB
#define TKEY_SCL_Pin GPIO_PIN_1
#define TKEY_SCL_GPIO_Port GPIOB
#define R10K_GND_Pin GPIO_PIN_2
#define R10K_GND_GPIO_Port GPIOB
#define TKEY_EN_Pin GPIO_PIN_7
#define TKEY_EN_GPIO_Port GPIOE
#define TKEY_RST_Pin GPIO_PIN_8
#define TKEY_RST_GPIO_Port GPIOE
#define TKEY_INT_Pin GPIO_PIN_9
#define TKEY_INT_GPIO_Port GPIOE
#define _RFID_EN_Pin GPIO_PIN_10
#define _RFID_EN_GPIO_Port GPIOE
#define SW_O_C_Pin GPIO_PIN_11
#define SW_O_C_GPIO_Port GPIOE
#define _SNS_LEFT_RIGHT_Pin GPIO_PIN_12
#define _SNS_LEFT_RIGHT_GPIO_Port GPIOE
#define VOICE_F_HOLD_Pin GPIO_PIN_13
#define VOICE_F_HOLD_GPIO_Port GPIOE
#define VOICE_RST_Pin GPIO_PIN_14
#define VOICE_RST_GPIO_Port GPIOE
#define VOICE_BUSY_Pin GPIO_PIN_15
#define VOICE_BUSY_GPIO_Port GPIOE
#define VOICE_CLK_Pin GPIO_PIN_10
#define VOICE_CLK_GPIO_Port GPIOB
#define VOICE_DATA_Pin GPIO_PIN_11
#define VOICE_DATA_GPIO_Port GPIOB
#define M_CLOSE_1_Pin GPIO_PIN_12
#define M_CLOSE_1_GPIO_Port GPIOB
#define M_CLOSE_2_Pin GPIO_PIN_13
#define M_CLOSE_2_GPIO_Port GPIOB
#define M_OPEN_1_Pin GPIO_PIN_14
#define M_OPEN_1_GPIO_Port GPIOB
#define M_OPEN_2_Pin GPIO_PIN_15
#define M_OPEN_2_GPIO_Port GPIOB
#define FP_DDL_TX_Pin GPIO_PIN_8
#define FP_DDL_TX_GPIO_Port GPIOD
#define FP_DDL_RX_Pin GPIO_PIN_9
#define FP_DDL_RX_GPIO_Port GPIOD
#define FP_EN_Pin GPIO_PIN_10
#define FP_EN_GPIO_Port GPIOD
#define _SNS_LOCK_Pin GPIO_PIN_11
#define _SNS_LOCK_GPIO_Port GPIOD
#define LED_CLOSE_Pin GPIO_PIN_12
#define LED_CLOSE_GPIO_Port GPIOD
#define LED_OPEN_Pin GPIO_PIN_13
#define LED_OPEN_GPIO_Port GPIOD
#define _LED_ERROR0_Pin GPIO_PIN_14
#define _LED_ERROR0_GPIO_Port GPIOD
#define BU_VO_MID_Pin GPIO_PIN_15
#define BU_VO_MID_GPIO_Port GPIOD
#define SNS_OPEN_Pin GPIO_PIN_6
#define SNS_OPEN_GPIO_Port GPIOC
#define SNS_CLOSE_Pin GPIO_PIN_7
#define SNS_CLOSE_GPIO_Port GPIOC
#define _SNS_CENTER_Pin GPIO_PIN_8
#define _SNS_CENTER_GPIO_Port GPIOC
#define BU_FREQ_Pin GPIO_PIN_9
#define BU_FREQ_GPIO_Port GPIOC
#define _COM_HCP_RST_Pin GPIO_PIN_8
#define _COM_HCP_RST_GPIO_Port GPIOA
#define COM_DDL_TX_Pin GPIO_PIN_9
#define COM_DDL_TX_GPIO_Port GPIOA
#define COM_DDL_RX_Pin GPIO_PIN_10
#define COM_DDL_RX_GPIO_Port GPIOA
#define BU_VO_HIGH_Pin GPIO_PIN_11
#define BU_VO_HIGH_GPIO_Port GPIOA
#define COM_HCP_EN_Pin GPIO_PIN_12
#define COM_HCP_EN_GPIO_Port GPIOA
#define MCU_SW_DAT_Pin GPIO_PIN_13
#define MCU_SW_DAT_GPIO_Port GPIOA
#define _COM_BUSY_Pin GPIO_PIN_2
#define _COM_BUSY_GPIO_Port GPIOH
#define MCU_SW_CLK_Pin GPIO_PIN_14
#define MCU_SW_CLK_GPIO_Port GPIOA
#define SNS_EN_Pin GPIO_PIN_15
#define SNS_EN_GPIO_Port GPIOA
#define SW_REG_Pin GPIO_PIN_10
#define SW_REG_GPIO_Port GPIOC
#define _FIRE_EN_Pin GPIO_PIN_11
#define _FIRE_EN_GPIO_Port GPIOC
#define SNS_EDGE_Pin GPIO_PIN_12
#define SNS_EDGE_GPIO_Port GPIOC
#define RFID_EN_Pin GPIO_PIN_0
#define RFID_EN_GPIO_Port GPIOD
#define _KEY_INTER_CL_Pin GPIO_PIN_1
#define _KEY_INTER_CL_GPIO_Port GPIOD
#define SNS_F_BROKEN_Pin GPIO_PIN_2
#define SNS_F_BROKEN_GPIO_Port GPIOD
#define RFID_EN2_Pin GPIO_PIN_3
#define RFID_EN2_GPIO_Port GPIOD
#define RFID_CLK_Pin GPIO_PIN_4
#define RFID_CLK_GPIO_Port GPIOD
#define _BT_TX_Pin GPIO_PIN_5
#define _BT_TX_GPIO_Port GPIOD
#define _BT_RX_Pin GPIO_PIN_6
#define _BT_RX_GPIO_Port GPIOD
#define _INT_7_Pin GPIO_PIN_7
#define _INT_7_GPIO_Port GPIOD
#define MCU_SW_OB_Pin GPIO_PIN_3
#define MCU_SW_OB_GPIO_Port GPIOB
#define _NC_Pin GPIO_PIN_4
#define _NC_GPIO_Port GPIOB
#define _BT_EN_Pin GPIO_PIN_5
#define _BT_EN_GPIO_Port GPIOB
#define I2C_SCL_Pin GPIO_PIN_6
#define I2C_SCL_GPIO_Port GPIOB
#define I2C_SDA_Pin GPIO_PIN_7
#define I2C_SDA_GPIO_Port GPIOB
#define SW_FACTORY_Pin GPIO_PIN_8
#define SW_FACTORY_GPIO_Port GPIOB
#define _TOUCH_O_C_Pin GPIO_PIN_9
#define _TOUCH_O_C_GPIO_Port GPIOB
#define DIM_SDA_Pin GPIO_PIN_0
#define DIM_SDA_GPIO_Port GPIOE
#define DIM_SCL_Pin GPIO_PIN_1
#define DIM_SCL_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
