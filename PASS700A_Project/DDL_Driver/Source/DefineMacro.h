#ifndef __DEFINEMACRO_INCLUDED
#define __DEFINEMACRO_INCLUDED
							

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <yfuns.h>


typedef	unsigned char		BYTE ;
typedef	char				SBYTE;
typedef	unsigned int		WORD ;
typedef	int				SWORD;
typedef	unsigned long		DWORD;
typedef	long				SDWORD;
typedef	unsigned short int	DBYTE;	//sjc_RC523



typedef union
{
	BYTE STATEFLAGDATA;
	struct
	{
		unsigned char Bit0 :1;
	    	unsigned char Bit1 :1;
	    	unsigned char Bit2 :1;
	    	unsigned char Bit3 :1;
	    	unsigned char Bit4 :1;
	    	unsigned char Bit5 :1;
	    	unsigned char Bit6 :1;
	    	unsigned char Bit7 :1;
	}STATEFLAG;
}FLAG;


typedef union
{
	BYTE BCARDBUF[16][8];
	struct
	{
		BYTE BIBUTTONBUF[75];
		BYTE BIBUTTONTMP[75];
		BYTE BNANSUBUF[10];
	}STIBUTTON;
}UNKEYUIDBUF;


typedef union
{
	BYTE BARRAYBUFFER[2];						
	WORD WVARIABLE;
}UNWORDBUF;



#define	DDL_TYPE				0x06				//
													//0x01 = Cap touch Lever 
													//0x02 = Cap touch Deadbolt//
													//0x03 = Keypad Lever 
													//0x04 = Keypad Deadbolt
													//0x05 = YDM3168
													//0x06 = NewDin-RFID

// 1.1 : 	UI 변경. 신규 UI로 변경됨. 
//		N-Protocol용 신규 생산 지그 프로그램 적용.
//		엣지볼트 안눌려진 경우 손바닥 터치로 문잠김 수행하지 않음. 
//		지문커버 열려있는 경우 3회 Retry기능.

#define DDL_PROGRAM_VERSION    	 	0x10		               		//Ver1.0
/*
#define SET_BIT(x,y)    (x |= y)
#define CLR_BIT(x,y)    (x &= (BYTE)~y)
#define CHK_BIT(x,y)    (x & y)

#define Bit_Set(sfr,bit)    	(sfr |= (1 << bit))
#define Bit_Clear(sfr,bit)  	(sfr &= (BYTE)~(1 << bit))
#define Bit_Test(sfr,bit)   	(sfr & (1 << bit))
*/
//#define SET_BIT(x,y)    	(x |= y)
#define CLR_BIT(x,y)    	(x &= (unsigned int)~y)
#define CHK_BIT(x,y)    	(x & y)

#define Bit_Set(sfr,bit)    	(sfr |= (1 << bit))
#define Bit_Clear(sfr,bit)  	(sfr &= (unsigned int)~(1 << bit))
#define Bit_Test(sfr,bit)   	(sfr & (1 << bit))


// Bit opreation macros

#define	Macro_Set_Bit(dest, position)					((dest) |=  ((unsigned)0x1<<(position)))
#define	Macro_Clear_Bit(dest, position)				((dest) &= ~((unsigned)0x1<<(position)))
#define	Macro_Invert_Bit(dest, position)				((dest) ^=  ((unsigned)0x1<<(position)))

#define	Macro_Clear_Area(dest, bits, position)			((dest) &= ~(((unsigned)bits)<<(position)))
#define	Macro_Set_Area(dest, bits, position)			((dest) |=  (((unsigned)bits)<<(position)))
#define	Macro_Invert_Area(dest, bits, position)		((dest) ^=  (((unsigned)bits)<<(position)))

#define	Macro_Write_Block(dest, bits, data, position)	((dest) = ((unsigned)dest) & ~(((unsigned)bits)<<(position)) | (((unsigned)data)<<(position)))
#define	Macro_Extract_Area(dest, bits, position)		((((unsigned)dest)>>(position)) & (bits))

#define Macro_Check_Bit_Set(dest, position)				((((unsigned)dest)>>(position)) & 0x1)
#define Macro_Check_Bit_Clear(dest, position)			(!((((unsigned)dest)>>(position)) & 0x1))


#define PortDirection(sfr, bit, direction)      			(sfr &= (unsigned int)~(0x03 <<(2*bit))) ;sfr|= (unsigned int)(direction << (2*bit))


#define	FAIL		0
#define	OK		1

#define	INPUT		1
#define	OUTPUT		0

#define DDL_LOW_BATT_CNT      	100


#define _VOICE	1
#define _BUZZ 	0

#define	LOW			0x00
#define	HIGH			0xFF


#define	OFF			0x00
#define	ON			0xFF

#define TENKEYSTOPTIME	4

#define UNREGISTERED		0xCC
#define REGISTERED		0xDD

#define FRONT_POSITION	0xCC
#define REAR_POSITION	0xDD




typedef uint8_t u08_t;
typedef uint16_t u16_t;
typedef uint32_t u32_t;

typedef int8_t s08_t;
typedef int16_t s16_t;
typedef int32_t s32_t;



#endif

