//------------------------------------------------------------------------------
/** 	@file		CardProcess_T1.h
	@brief	Card UID Read Functions 
*/
//------------------------------------------------------------------------------

#ifndef __CARDPROCESS_T1_INCLUDED
#define __CARDPROCESS_T1_INCLUDED


#include "DefineMacro.h"
#include "DefinePin.h"
#include "RC523Functions_T1.h"

//==============================================================================
//      S U C C E S S                                                                                            
// name Success
//==============================================================================

//------------------------------------------------------------------------------
/*! 	\def	MAX_CARD_ANTI_SIZE		
	Anticollision 알고리즘 수행을 통해 읽어들일 수 있는 최대 카드 수
	
	\def	MAX_CARD_UID_SIZE		
	읽어들일 수 있는 카드 UID의 최대 Byte 수

	\def	CARD_LOOP_DEFAULT_COUNT		
	입력된 카드의 UID를 읽어들이기 위한 전체 과정 반복 회수,
	READY 또는 ACTIVE 상태에서 REQA 전송할 경우에는 다시 IDLE 상태가 되기 때문에 최소 2회는 반복해야 함.
*/
//------------------------------------------------------------------------------
#define	MAX_CARD_ANTI_SIZE		5
#define	MAX_CARD_UID_SIZE		8
#define	MAX_CARD_FID_SIZE		8


//MFRC523_SJC  추가 시작
#define	MIN_CARDUID_LENGTH		4
#define	MAX_CARDUID_LENGTH		8
//MFRC523_SJC 심재철 추가 끝


#define	CARD_LOOP_DEFAULT_COUNT		3

#define	DETECT_TH			10			//  100mv차이 나면 카드 인식
//#define	DETECT_TH			5			//  30mv차이 나면 카드 인식

//------------------------------------------------------------------------------
/*! 	\def	CARDREAD_SUCCESS		
	카드 읽기 결과 - 카드 UID 읽기 성공

	\def	CARDREAD_NO_CARD		
	카드 읽기 결과 - 카드 떨어짐

	\def	CARDREAD_FAIL		
	카드 읽기 결과 - 카드 UID 읽기 실패

	\def	CARDREAD_INNER_FORCED_LOCK		
	카드 읽기 결과 - 내부강제잠금 설정 상태

///	제품에 따라 설정 추가 가능
*/

enum{
	CARDREAD_SUCCESS = 1,
	CARDREAD_NO_CARD,
	CARDREAD_FAIL,
	CARDSET_SUCCESS,
	CARDSTATUS_FAIL,
	MKSREAD_SUCCESS,
	CARDMKS_EMER_FAIL,
	CARDREAD_INNER_FORCED_LOCK,


	CARDREAD_SUPPORT_CARD_SUCCESS,					//  key A를 가지고 Block 1을 읽음. 즉 지원 가능한 카드인지 확인함. 
	CARDREAD_READ_COUNT_SUCCESS,					
	CARDREAD_WRITE_COUNT_SUCCESS,

	CARDREAD_VERIFY_FID_SUCCESS,
	CARDREAD_VERIFY_FID_FAIL,
	CARDREAD_WRITE_FID_SUCCESS,
	CARDREAD_WRITE_FID_FAIL,
};

//------------------------------------------------------------------------------




#define	CARDREAD_ING			250



#define	MAX_REG_CARD_SLOT		6				// FID저장을 위해 카드 한장당 가능한 SLOT 수.. 즉 한카드당 등록 가능한 도어락 수
#define	CARD_DETECTION_CHECK_CNT	5
//------------------------------------------------------------------------------
/*! 	\def	UID_NONE		
	TempSaveCompletedUID 함수의 Return 값 - UID가 모두 00h이거나 FFh일 경우 
	
	\def	UID_AVAILABLE		
	TempSaveCompletedUID 함수의 Return 값 - UID가 사용 가능할 경우 

	\def	UID_DUPICATED		
	TempSaveCompletedUID 함수의 Return 값 - UID가 이미 입력된 카드의 것과 동일할 경우 
*/
//------------------------------------------------------------------------------
#define	UID_NONE			0
#define	UID_AVAILABLE		1
#define	UID_DUPICATED		2


//gCardMode
enum{
	CARDMODE_POWER_ONOFF = 1,		//CardModePowerOnOff();	
	CARDMODE_SELECT,				//CardModeSelect();
	CARDMODE_READ_RESULT,			//CardModeReadResult();
	CARDMODE_CARD_AWAY_CHK,			//CardModeAwayFromReaderCheck();
};

//gCardProcessStep
enum
{
	//CardModePowerOnOff 함수에서의 ProcessStep
	CARDPRCS_IDLE_STATE = 0,
	CARDPRCS_RFID_POWERON,
	CARDPRCS_RFID_POWEROFF,
	CARDPRCS_RFID_TXON,
	CARDPRCS_RFID_TXOFF,
	CARDPRCS_READ_STOP,

	//CardModeSelect 함수에서의 ProcessStep	
	CARDPRCS_REQA_SEND,	
	CARDPRCS_WUPA_SEND,
	CARDPRCS_REQA_RESPOND,
	CARDPRCS_ANTICOLLISION_SEND,
	CARDPRCS_ANTICOLLISION_RESPOND,
	CARDPRCS_SELECT_SEND,
	CARDPRCS_SELECT_RESPOND,

	//CardModeReadResult 함수에서의 ProcessStep
	CARDPRCS_MIFARE_WAIT,							// 13
	
	CARDPRCS_MIFARE_PROCESS,
	CARDPRCS_MIFARE_KEYA_SEND,
	CARDPRCS_MIFARE_KEYA_RESPOND,
	CARDPRCS_MIFARE_GET_BLOCK_KEY_SEND,
	CARDPRCS_MIFARE_GET_BLOCK_KEY_RESPONSE,
	CARDPRCS_MIFARE_READ_COUNT_SEND,
	CARDPRCS_MIFARE_READ_COUNT_RESPONSE,			// 20
	CARDPRCS_MIFARE_WRITE_REG_COUNT_SEND,
	CARDPRCS_MIFARE_WRITE_REG_COUNT_RESPONSE,
	CARDPRCS_MIFARE_WRITE_REG_COUNT_VERIFY,

	CARDPRCS_MIFARE_READ_REG_DATA_START,
	CARDPRCS_MIFARE_READ_REG_DATA_SEND,
	CARDPRCS_MIFARE_READ_REG_DATA_RESPONSE,

	CARDPRCS_MIFARE_READ_AUTH_DATA_START,
	CARDPRCS_MIFARE_READ_AUTH_DATA_SEND,
	CARDPRCS_MIFARE_READ_AUTH_DATA_RESPONSE,
	CARDPRCS_MIFARE_READ_AUTH_DATA_NEXT_CHECH,



	CARDPRCS_MIFARE_WRITE_AUTH_DATA_SEND,
	CARDPRCS_MIFARE_WRITE_AUTH_DATA_RESPONSE,

	CARDPRCS_MIFARE_WRITE_DATA_SEND,
	CARDPRCS_MIFARE_WRITE_DATA_RESPONSE,	
	CARDPRCS_MIFARE_WRITE_DATA_VERIFY,	


	//CardModeAwayFromReaderCheck 함수에서의 ProcessStep
	CARDPRCS_AWAY_CHECK,
	CARDPRCS_AWAY_REQA_SEND,
	CARDPRCS_AWAY_REQA_RESPOND,
};

extern BYTE gCardAllUidBuf[MAX_CARD_ANTI_SIZE][MAX_CARD_UID_SIZE];

void SetupCard(void);

void CardDetectionProcess(void);

void CardProcessTimeCounter(void);
void CardProcessTimeClear(void);
void CardAwayCheckTimeCounter(void);
BYTE GetCardMode(void);

void CardTxOn(void);

void CardGotoReadStart(void);
void CardGotoReadStop(void);
void CardGotoAwayFromReaderCheck(void);


void CardGotoReadFidCountStart(void);
void CardGotoWriteFidCountStart(void);
void CardGotoWriteFidDataStart(BYTE* bFid);
void CardGotoReadFidDataStart(BYTE bBlock);
BYTE GetCardFidBlock(void);


void CardReadStatusClear(void);
BYTE GetCardReadStatus(void);
BYTE GetCardInputNumber(void);

void CardModeProcess(void);



#endif

