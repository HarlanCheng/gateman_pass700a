//------------------------------------------------------------------------------
/** 	@file		ISO14443AFunctions_T1.c
	@version 0.1.00
	@date	2016.04.08
	@brief	4~7 bytes ISO14443A Card UID Read Functions using TRF7970
	@remark	 TRF7970을 이용해 ISO14443A 카드의 UID를 읽어들이기 위한 함수
	@remark	 4~7 byte의 UID를 가진 Mifare Classic 카드 Read 대응 함수
	@remark	 Anticollision 알고리즘 수행
	@remark	 ========== ISO 14443 규정 =========================================================
	@remark	  1.Cascade Level 1 = 0x93 	
	@remark	 	UID Size => Single		
	@remark	 	Number of UID bytes => 4
	@remark	     Cascade Level 1 = 0x95 	
	@remark	 	UID Size => Double		
	@remark	 	Number of UID bytes => 7
	@remark
	@remark   2.NVB = 0x20 
	@remark		This value defines that the PCD will transmit no part of UID CLn. 
	@remark		Consequently this command forces all PICCs in the field to respond with	their complete UID CLn. 		
	@remark
	@remark		Collision 발생 이전 전송 Data => 0x00+0x20+0x93+0x20
	@remark		Collision 발생 이후 전송 Data => 0x00+TxLengthByte2+0x93+NVB+깨진 Byte전까지의 Data+깨진 Byte의 Bit까지의 Data
	@remark		ANTICOLLISION Command => SEL+NVB 
	@remark				  				    SEL+NVB+UID CLn 
	@remark	  3.NVB										
	@remark	 	Upper 4 bit(Byte Count) : PCD에서 보내어지는 모든 valid data bit를 8로 나눈 정수값을 지정 (SEL, NVB 포함) (2~7) 		
	@remark	 	Lower 4 bit(Bit Count) : PCD에서 보내어지는 모든 valid data bit를 8로 나눈 값의 나머지를 지정 (SEL, NVB 포함) (0~7)
	@remark
	@remark	  4. SELECT
	@remark		If no further collision occurs, the PCD shall assign NVB with the value of '70'.
	@remark       	This value defines that the PCD will transmit the complete UID CLn.
	@remark	 	SELECT Command => SEL+NVB+UID CLn+CRC_A
	@remark
	@remark	  5. SAK(Select Acknowledge) 
	@remark		NVB가 40 valid data bit로 지정되었거나 모든 data bit가 UID CLn과 일치하였을 때 PICC가 보냄	
	@remark		bit3=1(& 0x04) => Cascade bit set;UID not complete	
	@remark		bit6=1(& 0x20) => UID complete, PICC compliant with ISO/IEC 14443-4 
	@remark		bit3=0 && bit6=0 => UID complete, PICC not compliant with ISO/IEC 14443-4
	@remark	 ==============================================================================

	@see	TRF7970Functions_T1.c
	@see	CardProcess_T1.c
	@see	ParallelFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.08		by Jay
			- TRF7970을 SPI로 Serial 제어
			- 해당 함수는 ISO14443A 규격 대응 내용을 포함하며, 카드 구동을 위해서는 
				TRF7970Functions_T1 과 CardProcess_T1 이 모두 연결되어 있어 같이 결합하여 사용해야 함.
			- 설정에 의해 아이레보 카드만 대응도 가능하고, OPEN UID 대응도 가능하게 설계
			- 별도 설정 없이 4byte UID 카드 및 7byte UID 카드 모두 대응 가능

*/
//------------------------------------------------------------------------------


#include "Main.h"
#ifdef DDL_CFG_RFID_RC523
#include "ParallelFunctions_T2.h"
#include "MX_spi_lapper.h"
#include "RC523Functions_T1.h"
#include "ISO14443AFunctions_T2.h"
#endif


BYTE gIso14443aCascadeLevel = 0;				/**< Anticollision 과정 중에서 카드의 UID 읽기 위한 Cascase level  */

BYTE gIsoCollisionSet = 0;						/**< Collision 에러 발생 여부 확인 Flag 변수, 1일 경우 Collision 발생  */
BYTE gIso14443aTempUidBuf[5];					/**< Anticollision 알고리즘 수행 중에 읽어 온 UID를 임시 저장하는 버퍼, 최대 5byte만 저장  */
												/**< NVB값을 참조한 값 (Collision Bit Count가 없을 경우 NVB와 동일) */
									
BYTE gIso14443aNVB = 0;							/**< 깨진 Bit 다음부터 전송 요구를 하기 위한 NVB값 */




//------------------------------------------------------------------------------
/** 	@brief	UID size check 
	@param 	[Atqa1stData] : ATQA의 첫번째 Byte 
	@return 	[7] : double UID (7byte UID 카드)
	@return 	[4] : single UID (4byte UID 카드)
	@remark 입력된 카드의 UID byte 확인, double UID(7byte)까지만 구현
	@remark ISO14443A 규격 참조
*/
//------------------------------------------------------------------------------
BYTE UidSizeCheck(BYTE Atqa1stData)
{
	switch(Atqa1stData & 0xC0)
	{
		case 0x00:	
			// single UID (4byte)
			return 4; 
			break;

		case 0x40:	
			// double UID (7byte)
			return 7; 
			break;
	}

	return 4; 
}


//------------------------------------------------------------------------------
/** 	@brief	Input UID BCC Check
	@param 	[Uid] UID 버퍼의 포인터 	
	@return 	[STATUS_SUCCESS] BCC matched
	@return 	[STATUS_FAIL] BCC Not matched
	@remark 입력된 UID 4byte의 BCC Data 검증
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
BYTE isUidBccOk(BYTE *Uid)
{
	BYTE Bcc = 0;
	BYTE bCnt;	

	// 5byte의 Data 중 마지막 byte가 BCC
	for(bCnt = 0; bCnt < 4; bCnt++)
	{
		Bcc ^= *Uid;
		Uid++;
	}

	if(Bcc == *Uid)
	{
		return STATUS_SUCCESS;
	}

	return STATUS_FAIL;
}


//------------------------------------------------------------------------------
/** 	@brief	Make a mask data to delete broken bits in broken byte
	@param 	[CollisionBit] Collision이 발생한 바로 다음 Bit, 해당 Bit는 1 또는 0으로 설정하여 전송 	
	@return 	[bMask] Collision이 발생한 Bit 바로 다음까지의 Bit를 제외하고는 모두 0으로 처리하기 위한 변수
	@remark Anticollision 수행 시 Collision 발생 이후 수신한 Byte와 Bit까지의 정보 전송에 사용
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
BYTE MakeMaskBrokenBit(BYTE CollisionBit)
{
	BYTE bCnt;
	BYTE bMask = 0;

	for(bCnt = 0; bCnt < CollisionBit; bCnt++)
	{
		// left shift to make a mask for last broken byte (create all bit 1 belong to how many bit in broken byte)
		bMask = (bMask << 1) + 1;			
	}

	return (bMask);
}


//------------------------------------------------------------------------------
/** 	@brief	Make a mask data to delete unbroken bits in broken byte
	@param 	[CollisionBit] Collision이 발생한 바로 다음 Bit, 이미 수신한 Bit와 새로 수신한 Bit를 겹합하여 Byte 완성 	
	@return 	[bMask] Collision이 발생한 Bit까지의 Bit는 그대로 두고 새로 수신한 Bit를 결합하기 위한 변수
	@remark Anticollision 수행에 따라 수신한 수신한 Bit와 이미 수신된 Bit의 결합을 위해 사용
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
BYTE MakeMaskUnbrokenBit(BYTE CollisionBit)
{
	BYTE bCnt;
	BYTE bMask = 0xFF;
	BYTE bBit = 0x01;
	
	for(bCnt = 0; bCnt < CollisionBit; bCnt++)
	{
		bMask &= ~bBit;
		bBit <<= 1;
	}

	return (bMask);
}


//------------------------------------------------------------------------------
/** 	@brief	Set Cascade Level 
	@param 	[Level] : Cascade Level 
	@return 	None
	@remark Anticollision 및 Select 과정에서의 Cascade Level 설정, 2단계까지만 구현
	@remark ISO14443A 규격 참조
*/
//------------------------------------------------------------------------------
void CascadeLevelSet(BYTE Level)
{
	if(Level == 2)
	{
		gIso14443aCascadeLevel = ISO14443A_SEL_CASCADE2;
	}
	else
	{
		gIso14443aCascadeLevel = ISO14443A_SEL_CASCADE1;
	}		
}



//------------------------------------------------------------------------------
/** 	@brief	Clear anticollision variables
	@param 	None 	
	@return 	None
	@remark Anticollision에 사용된 모든 변수 초기화
*/
//------------------------------------------------------------------------------
void AntiColVariInit(void)
{
	gIsoCollisionSet = 0;
	gIso14443aNVB = 0x20;

	memset(gIso14443aTempUidBuf, 0xFF, 5);		
}



//------------------------------------------------------------------------------
/** 	@brief	Copy read UID to target buffer
	@param 	[TargetBuf] Target Buffer to copy
	@param 	[UidSize] UID Size (4byte or 7 byte)
	@return 	None
	@remark TRF7970에서 읽어 들인 UID를 요청한 Buffer에 복사
*/
//------------------------------------------------------------------------------
void CopyCardUid(BYTE *TargetBuf, BYTE UidSize)
{
	if(UidSize == 7)
	{
		if(gIso14443aCascadeLevel == ISO14443A_SEL_CASCADE1)
		{
			memcpy(TargetBuf, gIso14443aTempUidBuf+1, 3);
		}
		else
		{
			memcpy(TargetBuf+3, gIso14443aTempUidBuf, 4);
		}
	}
	else
	{
		// 4바이트 UID 저장
		memcpy(TargetBuf, gIso14443aTempUidBuf, 4); 	
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Send WUPA for detecting card
	@param 	None 	
	@return 	[0] : 감지된 카드 없음
	@return 	[1] : 감지된 카드 있음
	@remark 폴링하면서 카드 입력 감지하기 위한 함수
	@remark 카드 접촉이 없을 경우 EN ON부터 EN OFF까지 약 2~3ms 소요 		
	@remark [1] Power On (TRF7970 EN->1ms Delay->Register Intialization)
	@remark [2] RF TX On	
	@remark [3] Wait
	@remark		-> Open UID : 원래 4ms 대기였는데 ISO 규정 만족을 위해 6ms로 변경 
	@remark		-> iRevo Card : 500us 대기 
	@remark [4] Send REQA
	@remark [5] ATQA Check
	@remark 	-> 1 : Received			
*/
//------------------------------------------------------------------------------
BYTE Iso14443aFindTag(BYTE *UidSize)
{
	WORD wNum = 350;	// 약 500us 정도 대기

	RfidPowerOn();
//	Delay(SYS_TIMER_1MS);				// wait until system clock started
	RfidTxOn();

#ifdef 	IREVO_CARD_ONLY

	Delay(SYS_TIMER_500US);

#else
	// When a PICC is exposed to an unmodulated operating field
	// it shall be able to accept a quest within 5 ms.
	// PCDs should periodically present an unmodulated field of at least
	// 5,1 ms duration. (ISO14443-3)
	Delay(SYS_TIMER_2MS);

#endif

	IrqClear();

	Iso14443aReqaSend(ISO14443A_WUPA);
	
	// Reference Source에서는 TX Wait Time 5ms, RX Wait Time 50ms 설정하여 사용하였지만,
	// 배터리 문제로 RX Wait Time을 약 500us로 조정하여 사용 
	while(--wNum)
	{
		if(IrqCheck() == IRQ_RX_COMPLETED)
		{
			if(Iso14443aReqaRespond(UidSize) == STATUS_SUCCESS)
			{
				return 1;
			}
			break;
		}
	}
	RfidPowerOff();

   	return 0;
}



//=============================================================
//	ISO14443A INTERFACE FUNCTION						
//=============================================================

//------------------------------------------------------------------------------
/** 	@brief	ISO14443A Standard, Send REQA or WUPA
	@param 	Command : REQA or WUPA 	
	@return 	None 
	@remark ISO14443A 규격의 REQA 또는 WUPA 신호 전송
	@remark ISO_CONTROL
	@remark 	- No Rx CRC
	@remark		- ISO14443A, bit rate 106kbs
	@remark TRF7960 Datasheet 참조
*/
//------------------------------------------------------------------------------
void Iso14443aReqaSend(BYTE Command)
{
	ISO14443A_REQA_Send(Command);
}


//------------------------------------------------------------------------------
/** 	@brief	ISO14443A Standard, Response Check on REQA
	@param 	[UidSize] : ATQA를 통해 확인한 UID Size 정보 저장 	
	@param 	[gRfidFifoDataBuf] : 수신한 2 byte Data 저장 	
	@return 	[STATUS_FAIL] : FIFO에 수신된 Data가 2byte가 아님
	@return 		-> TRF7960의 경우에는 FIFO Status+1이 실제 수신한 byte 수
	@return 		-> TRF7970의 경우에는 FIFO Status가 실제 수신한 byte 수
	@return 	[STATUS_SUCCESS] : 2 byte 수신 완료
	@remark ISO14443A 규격의 REQA or WUPA 신호에 대한 응답 확인
	@remark 2 byte 응답이 수신되어야 정상 처리
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
BYTE Iso14443aReqaRespond(BYTE *UidSize)
{
	BYTE bTmp = 0;
	
	bTmp = ISO14443A_REQA_Respond(UidSize);
	return bTmp;
}


//------------------------------------------------------------------------------
/** 	@brief	ISO14443A Standard, Halt
	@param 	None 	
	@return 	None 
	@remark 선택된 ISO14443A PICC를 Halt 처리
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void Iso14443aHalt(void)
{
	ISO14443A_HALT_Send();

	Delay(SYS_TIMER_2MS);
	IrqClear();
}


//------------------------------------------------------------------------------
/** 	@brief	Send Anticollision
	@param 	[gIso14443aCascadeLevel] : 전송할 Cascade Level 결정 	
	@return 	[STATUS_SUCCESS] 전송 성공
	@return 	[STATUS_SUCCESS] 전송 실패 (NVB byte가 처리 범위를 넘어 바로 Select 전송 처리 필요)
	@remark Anticollision 처리를 위한 명령 전송
	@remark 4~7 byte의 UID Read
	@remark ISO14443A 규격 참조
	@remark
	@remark ========== ISO 14443 규정 ============================================
	@remark   1.Cascade Level 1 = 0x93 
	@remark			UID Size => Single			
	@remark			Number of UID bytes => 4
	@remark      Cascade Level 1 = 0x95 
	@remark 		UID Size => Double			
	@remark 		Number of UID bytes => 7
	@remark								
	@remark	  2.NVB = 0x20 							
	@remark               This value defines that the PCD will transmit no part of UID CLn.
	@remark			Consequently this command forces all PICCs in the field to respond with	their complete UID CLn. 						
	@remark										
	@remark			Collision 발생 이전 전송 Data => 0x00 + 0x20 + 0x93 + 0x20
	@remark			Collision 발생 이후 전송 Data => 0x00 + TxLengthByte2 + 0x93 + NVB
	@remark										+ 깨진 Byte전까지의 Data
	@remark										+ 깨진 Byte의 Bit까지의 Data
	@remark			ANTICOLLISION Command 	=> 	SEL+NVB
	@remark										SEL+NVB+UID CLn 				
	@remark =================================================================
*/
//------------------------------------------------------------------------------
BYTE Iso14443aAnticollisionSend(void)
{
	BYTE bTmp;
	
	bTmp = ISO14443A_Anticollision_Send();
	return bTmp;
}



//------------------------------------------------------------------------------
/** 	@brief	Anticollision Respond Process
	@param 	[bStatus] IRQ Status
	@param 		- IRQ_RX_COMPLETED : IRQ Rx Complete
	@param 		- IRQ_COLLISION_ERROR : IRQ Collision Occurrence
	@return 	[STATUS_FAIL] : 규정에 맞지 않아 처리 실패
	@return 	[STATUS_SUCCESS] : UID 완성
	@return 	[STATUS_PROCESSING] : Collision 발생 등으로 추가 처리 필요
	@remark Anticollision에 응답한 결과 처리
	@remark 4~7 byte의 UID Read
	@remark ISO14443A 규격 참조
	@remark
	@remark ========== ISO 14443 규정 =============================================
	@remark   1.NVB										
	@remark		Upper 4 bit(Byte Count) : PCD에서 보내어지는 모든 valid data bit를 8로 나눈 정수값을 지정 
	@remark		(SEL, NVB 포함) (2~7) 		
	@remark		Lower 4 bit(Bit Count) : PCD에서 보내어지는 모든 valid data bit를 8로 나눈 값의 나머지를 지정 
	@remark		(SEL, NVB 포함) (0~7)
	@remark ==================================================================
*/
//------------------------------------------------------------------------------
BYTE Iso14443aAnticollisionRespond(BYTE bStatus)
{
	BYTE bTmp = 0;

	bTmp = ISO14443A_Anticollision_Respond(bStatus);
	return bTmp;
}




//------------------------------------------------------------------------------
/** 	@brief	Send Select
	@param 	[gIso14443aCascadeLevel : Cascade Level 결정 	
	@return 	None
	@remark Select 처리를 위한 명령 전송
	@remark 4~7 byte의 UID Read
	@remark ISO14443A 규격 참조
	@remark 
	@remark ========== ISO 14443 규정 =================================
	@remark     If no further collision occurs, the PCD shall assign NVB	with the value of '70'.
	@remark     This value defines that the PCD will transmit the complete UID CLn.
	@remark 	SELECT Command => SEL+NVB+UID CLn+CRC_A	
	@remark ======================================================
*/
//------------------------------------------------------------------------------
void Iso14443aSelectSend(void)
{
	ISO14443A_Select_Send();
}


//------------------------------------------------------------------------------
/** 	@brief	Select Respond Process
	@param 	None
	@return 	[SAK_FAIL] : 처리 실패
	@return 	[SAK_UID_NOT_COMPLETE] : UID not complete
	@return 	[SAK_UID_COMPLETE] : UID complete
	@remark Select에 응답한 결과 처리
	@remark 4~7 byte의 UID Read
	@remark ISO14443A 규격 참조
	@remark 
	@remark ========== ISO 14443 규정 ====================================
	@remark    SAK(Select Acknowledge) : NVB가 40 valid data bit로 지정되었거나 모든 data bit가 
	@remark 	UID CLn과 일치하였을 때 PICC가 보냄
	@remark 		bit3=1(& 0x04) => Cascade bit set;UID not complete	
	@remark 		bit6=1(& 0x20) => UID complete, PICC compliant with ISO/IEC 14443-4
	@remark 		bit3=0 && bit6=0 => UID complete, PICC not compliant with ISO/IEC 14443-4
	@remark =========================================================
*/
//------------------------------------------------------------------------------
BYTE Iso14443aSelectRespond(BYTE *sak)
{
	BYTE bTmp;
	
	bTmp = ISO14443A_Select_Respond(sak);
	return bTmp;
}

BYTE gCardOpenUidSupported = 0;

void CardOpenUidSupportedInfoLoad(void)
{
	RomRead(&gCardOpenUidSupported, OPEN_UID_EN, 1);
}

void CardOpenUidSupportedInfoSave(BYTE SetValue)
{
	gCardOpenUidSupported = SetValue;
	RomWrite(&gCardOpenUidSupported, OPEN_UID_EN, 1);
}


