#include "Main.h"



uint16_t		Second_sns_o, Second_sns_c;
void	Second_test_Motor( void )
{
#if 0
	uint32_t		dir = 0;

	P_SNS_EN( 0 );
	Delay(SYS_TIMER_1MS);

	if ( Second_sns_o = P_SNS_OPEN_T )
		dir = 1;
	else if ( Second_sns_c = P_SNS_CLOSE_T )
		dir = 2;


	if ( dir == 1 ) {
		_MOTOR_CLOSE;
		do {
			Second_sns_c = P_SNS_CLOSE_T;
		}
		while( !Second_sns_c );

		_MOTOR_STOP;
	}
	else if ( dir == 2 ) {
		_MOTOR_OPEN;
		do {
			Second_sns_o = P_SNS_OPEN_T;
		}
		while( !Second_sns_o );

		_MOTOR_STOP;
	}
	
	P_SNS_EN( 1 );
#endif 
}


/******************************************************************/
#if 0	//PWM 제어 부분 추가는 추구 진행 2018년06월07일 by sim
#ifdef M_CLOSE_PWMCH1_Pin	//second pwm_주석 by sjc
void SetMotorPWMFreq_Second(void) // 10% ~ 100%
{
	static WORD  TempDuty = 0xFFFF;
	uint16_t	Duty = 0x0000;
	uint32_t	period = 1000; //16Mhz / period = 16Khz

	//L-Mecha 와 YDD424+ deadbolt 확인시 
	// start duty 50% 정도에 각 구간별 PWM_DUTY_HOLD_COUNTER 을 정해서 
	// 총 6구간으로 나누고 각 구간을 지나가면 10% 씩 올려 준다 
	// PWM_DUTY_HOLD_COUNTER 는 100 count 당 6ms 정도 소요 
	// PWM_DUTY_HOLD_COUNTER 이 값이 높을 수록 전압 주저 않는 정도는 줄어 드나 그만큼 도는 속도가 느리게 된다. 
	// 적당히 잘 잡아서 사용 할것 

	if(tim9_count < PWM_DUTY_HOLD_COUNTER)
	{
		Duty = PWM_START_DUTY;
	}
	else if(tim9_count < (PWM_DUTY_HOLD_COUNTER*2))
	{
		Duty = PWM_START_DUTY+10; // 60%
	}
	else if(tim9_count < (PWM_DUTY_HOLD_COUNTER*3))
	{
		Duty = PWM_START_DUTY+20; // 70%
	}
	else if(tim9_count < (PWM_DUTY_HOLD_COUNTER*4))
	{
		Duty = PWM_START_DUTY+30; // 80%
	}
	else if(tim9_count < (PWM_DUTY_HOLD_COUNTER*5))
	{
		Duty = PWM_START_DUTY+40; // 90%
	}
	else if(tim9_count >= (PWM_DUTY_HOLD_COUNTER*5))
	{
		Duty = PWM_START_DUTY+51; //100% 이상  
	}

	Duty *= 10; // period 가 1000 이라 10을 곱해서 100% 을 맞춘다 계산 편의 .... 

	if(TempDuty != Duty)
	{
		gh_mcu_motor_timer9->Instance->ARR = period; // 값이 변하지는 않지만 혹시 모르니 ...

		if(Get_PWM_Direction() == MCTRL_DIR_OPEN_Second)
			gh_mcu_motor_timer9->Instance->CCR2 = Duty;	// open duty
		else 
			gh_mcu_motor_timer9->Instance->CCR1 = Duty;	// close duty

		gh_mcu_motor_timer9->Instance->EGR = TIM_EGR_UG;		 //PWM 설정을 바꾸고 그 설정으로 PWM shadow register들을 update할 것을 지정한다
		TempDuty = Duty;		
	}
}
#endif 
#endif	//PWM 제어 부분 추가는 추구 진행 2018년06월07일 by sim

void		rim_lock_sensor_on_Second( void )
{
	P_SNS_EN_SECOND( 0 );								//Sensor Power On (Acitive Low)
}

void		rim_lock_sensor_off_Second( void )
{
	P_SNS_EN_SECOND( 1 );								//Sensor Power Off (Acitive Low)
}

void		rim_lock_motor_open_Second( void )
{
	_MOTOR_PWR_EN_Second;
	_MOTOR_SLEEP_EXIT_Second;
	_MOTOR_OPEN_Second;

#if 0	//PWM 제어 부분 추가는 추구 진행 2018년06월07일 by sim	
#ifdef M_CLOSE_PWMCH1_Pin
	//PWM 제어시 
	Motor_PWM_Init(MCTRL_DIR_OPEN_Second);
#endif 	

#if !defined (M_CLOSE_PWMCH1_Pin) && defined (PWM_BACKTURN_SKIP_COUNT)
	// PWM 이 아닌 timer9 으로 제어시 
	tim9_count = 0;
	HAL_TIM_Base_Start_IT(gh_mcu_motor_timer9);
#endif 
#endif	//PWM 제어 부분 추가는 추구 진행 2018년06월07일 by sim

}

void		rim_lock_motor_close_Second( void )
{
	_MOTOR_PWR_EN_Second;
	_MOTOR_SLEEP_EXIT_Second;
	_MOTOR_CLOSE_Second;
#if 0	
#ifdef M_CLOSE_PWMCH1_Pin//second pwm_주석 by sjc
	Motor_PWM_Init(MCTRL_DIR_CLOSE_Second);
#endif 	

#if !defined (M_CLOSE_PWMCH1_Pin) && defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
	HAL_TIM_Base_Stop_IT(gh_mcu_motor_timer9);
	tim9_count = 0;
#endif 
#endif
}

void		rim_lock_motor_stop_Second( void )
{
#if 0	
#ifdef M_CLOSE_PWMCH1_Pin//second pwm_주석 by sjc
	Motor_PWM_DeInit();
#endif 	

#if !defined (M_CLOSE_PWMCH1_Pin) && defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
	HAL_TIM_Base_Stop_IT(gh_mcu_motor_timer9);
	tim9_count = 0;
#endif	
#endif
	_MOTOR_STOP_Second;
	_MOTOR_PWR_DISABLE_Second;
	_MOTOR_SLEEP_EN_Second;

}

void		rim_lock_motor_hold_Second( void )
{
#if 0	
#ifdef M_CLOSE_PWMCH1_Pin//second pwm_주석 by sjc
	Motor_PWM_DeInit();
#endif 	

#if !defined (M_CLOSE_PWMCH1_Pin) && defined (PWM_BACKTURN_SKIP_COUNT)//second pwm_주석 by sjc
	HAL_TIM_Base_Stop_IT(gh_mcu_motor_timer9);
	tim9_count = 0;
#endif	
#endif
	_MOTOR_PWR_EN_Second;
	_MOTOR_SLEEP_EXIT_Second;
	_MOTOR_BREAK_Second;
}

uint32_t		rim_lock_is_motor_open_Second( void )
{
	return	P_SNS_OPEN_T_SECOND;
}

uint32_t		rim_lock_is_motor_close_Second( void )
{
	return	P_SNS_CLOSE_T_SECOND;
}

#ifdef	P_SNS_CENTER_T
uint32_t		rim_lock_is_motor_center_Second( void )
{
	return	P_SNS_CENTER_T;
}
#endif

#ifdef	P_SNS_LOCK_T
uint32_t		rim_lock_is_motor_lock_Second( void )
{
#ifdef	DDL_CFG_SNS_LOCK_ACTIVE_HIGH
	return	(P_SNS_LOCK_T);
#else 
	return	(!P_SNS_LOCK_T);
#endif 
}
#endif


ddl_motor_ctrl_t_Second		Dual_Sensor_Cluch_motor_Second  = {
	.TO_open_Second= { 1000, 0, 0, 75, 0, 0, 0, 0, 100},		// unit 2msec
	.TO_close_Second= { 1000, 0, 0, 75, 0, 0, 0, 0, 100},			// unit 2msec

	.init_sensor_hw_Second	= NULL,
	.sensor_on_Second		= rim_lock_sensor_on_Second,
	.sensor_off_Second		= rim_lock_sensor_off_Second,

	.motor_open_Second	= rim_lock_motor_open_Second,
	.motor_close_Second	= rim_lock_motor_close_Second,
	.motor_stop_Second		= rim_lock_motor_stop_Second,
	.motor_hold_Second		= rim_lock_motor_hold_Second,

	.is_motor_open_Second	= rim_lock_is_motor_open_Second,
	.is_motor_close_Second	= rim_lock_is_motor_close_Second,
#ifdef	P_SNS_CENTER_T
	.is_motor_center_Second = rim_lock_is_motor_center_Second,
#else 
	.is_motor_center_Second = NULL,
#endif 
#ifdef		P_SNS_LOCK_T	
	.is_motor_lock_Second	= rim_lock_is_motor_lock_Second
#else
	.is_motor_lock_Second	= InnerForcedLockCheck_Second	//InnerForcedLockCheck_Second()
#endif
};

void SetupBatteryPwrFactor_Second(void);	

void	Init_Motor_Second( void )
{
#ifdef	P_SNS_LEFT_RIGHT_T
	HandingLockAutoRun();
#endif
	register_motor_ctrl_Second( &Dual_Sensor_Cluch_motor_Second );
	MotorStatusInitialCheck_Second();
	SetupBatteryPwrFactor_Second();
}

//=============================================================
// Inner Forced Lock set by O/C button
//=============================================================

#ifdef		P_SNS_LOCK_T	

uint32_t InnerForcedLockCheck_Second(void) // 이 함수는 안쓰는 것으로 생각 한다. //InnerForcedLockCheck_Second()
{
#ifdef	DDL_CFG_SNS_LOCK_ACTIVE_HIGH
	if(P_SNS_LOCK_T)	return 1;
#else 
	if(!P_SNS_LOCK_T)	return 1;
#endif 	
	return 0;
}

#else

BYTE gbInLockEn_Second = 0; //gbInLockEn _Second

void InnerForcedLockSet_Second(void)	//InnerForcedLockSet_Second
{
	gbInLockEn_Second = 0xAA;//gbInLockEn _Second
	//RomWrite(&gbInLockEn, (WORD)INLOCK_STATE, 1);	
}

void InnerForcedLockClear_Second(void)	//InnerForcedLockClear_Second
{
	gbInLockEn_Second = 0xBB;//gbInLockEn _Second
	//RomWrite(&gbInLockEn, (WORD)INLOCK_STATE, 1);	
}


void InnerForcedLockSettingLoad_Second(void)	//InnerForcedLockSettingLoad_Second
{
	//RomRead(&gbInLockEn, (WORD)INLOCK_STATE, 1);		
	if(gbInLockEn_Second != 0xAA)//gbInLockEn _Second
	{
		gbInLockEn_Second = 0xBB;//gbInLockEn _Second
	}
}

uint32_t InnerForcedLockCheck_Second(void)//InnerForcedLockCheck_Second()
{
	if(gbInLockEn_Second == 0xAA)	return 1;//gbInLockEn _Second

	return 0;
}

#endif



float AdcToPwrVoltFactor_Second;
float PwrVolt_Second;


void SetupBatteryPwrFactor_Second(void)			// 파워 전압 계산을 하기 위한 인자들 설정
{
///////////// 사용자 정의 변수 //////////////////////////////////////////////////////////////////////////////////////////////////////////
	float R1 = 5.6;						// 항상 소수점으로 표시 2 -> 2.0 			// 위쪽 분배 저항 R1,  (실제저항값) 나누기 (1000)해서 적기.. 5.6K옴  -> 5600/1000 = 5.6
	float R2 = 2.0;						// 항상 소수점으로 표시 2 -> 2.0			// 아래 분배 저항 R2,  (실제저항값) 나누기 (1000)해서 적기.. 2.0K옴  -> 2000/1000 = 2.0
	float AdcRefVolt = 3.3;				// ADC 레퍼런스 전압. Vref가 3.3V면 3.3, 3.0V면 3.0
//	BYTE AdcResolutionBit = 10;			// 12비트 레졸루션이면 12 씀,.. 10비트 레졸류션이면 10
	BYTE AdcResolutionBit = 12;			// 12비트 레졸루션이면 12 씀,.. 10비트 레졸류션이면 10
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	float AdcToVoltFactor;				// ADC값을 전압으로 변환해주는 팩터
	float VoltToPwrFactor;				// 분배된 전압을 pwr 전압으로 변환해 주는 팩터
	WORD AdcResolution;					// 레졸루션 10진수 값
	BYTE bCnt;

	AdcResolution = 1;
	for(bCnt = 0; bCnt < AdcResolutionBit; bCnt++)
	{	
		AdcResolution = AdcResolution * 2;
	}

	AdcToVoltFactor = AdcRefVolt / ((float)AdcResolution);				
	VoltToPwrFactor = R2 / (R1 + R2);							// 전압 분배 값

	AdcToPwrVoltFactor_Second = AdcToVoltFactor / VoltToPwrFactor;

}

void PwrVoltCalculation_Second(void)						// 파워 전압 계산
{
	PwrVolt_Second = ((float)GetBatteryAdcValue()) * AdcToPwrVoltFactor_Second;		// 파워 전압 계산	
}

BYTE MotorTimeIndexCalculation_Second(void)				// 모터 구동 타임의 배열 인덱스 설정
{
	BYTE bMotorTimeIndex;
	
	if(PwrVolt_Second > 6.99)	PwrVolt_Second = 6.9;						// 전압이 6.9v이상이면 무조건 6.9볼트로 고정, 6.9볼트 이상은 계산하지 않는다.
	if(PwrVolt_Second < 3.0)	PwrVolt_Second = 3.0;						// 전압이 3v이하면 무조건 3볼트로 고정, 3볼트 이하는 계산하지 않는다.

	bMotorTimeIndex = (BYTE)((PwrVolt_Second * 10.0) - 30.0);		// volt에서 30을 뺌, Index의 범위를 0~39(총 40종류, 3.0V, 3.1V,3.2V ~ 6.9V)까지 하기 위해서 30을 뺌

	if(bMotorTimeIndex > 39) bMotorTimeIndex = 39;		// 배열의 인덱스가 됨

	return bMotorTimeIndex;
}

BYTE MotorTimeGenerate_Second(BYTE ArryIndex, BYTE MinTime10ms, BYTE MaxTime10ms)
{
	BYTE bCnt;
	BYTE bFinalMotorTime;
	float bTimeInterval;
	float bTime;

	bFinalMotorTime = 0;

	if(MinTime10ms > MaxTime10ms)
	{
		return 0;
	}

	if(MinTime10ms == MaxTime10ms)
	{
		return MinTime10ms;
	}

	bCnt = MaxTime10ms - MinTime10ms;
	bTimeInterval = ((float)bCnt) / (40.0 - 1.0);		// -1을 하는 이유는 40단계이지만 개수는 39개로 함.

	bTime = (float)MinTime10ms;	

	for(bCnt = 40; bCnt > 0; bCnt--)				// 직선보간법	
	{
		if( ArryIndex == (bCnt - 1) )
		{
			bFinalMotorTime = (BYTE)bTime;
			break;
		}
		
		bTime = bTime + bTimeInterval;
	}

	return bFinalMotorTime;

}

// 현재 파워 전압에 따른 모터 구동 시간 리턴 해줌
BYTE MotorTimeSetting_Second(BYTE MinTime10ms, BYTE MaxTime10ms)		// 이 함수 구동 시간 STM32L151VBXXA 32Mhz 기준 130us
{
	BYTE bTmp;

	PwrVoltCalculation_Second();
	bTmp = MotorTimeIndexCalculation_Second(); 					// 파워 전압에 따른 모터 시간 배열 인덱스 추출
	bTmp = MotorTimeGenerate_Second(bTmp, MinTime10ms, MaxTime10ms);		// 파워 전압 대 모터 구동 시간 테이블 생성

	return bTmp;
}



#define		TIME_PWR_FULL 		38
#define		TIME_PWR_LOW		72	



uint32_t MotorBackTurnTimeSetting_open_Second(void)
{
	uint32_t RetTimeValue;
	
	RetTimeValue = (uint32_t)MotorTimeSetting_Second(TIME_PWR_FULL-1, TIME_PWR_LOW-1); 

	// 10ms Timer 기준으로 나온 값을 2ms Timer에 사용할 수 있도록 처리
	RetTimeValue = RetTimeValue * 5;
	
	return (RetTimeValue);
}
	

uint32_t MotorBackTurnTimeSetting_close_Second(void)
{
	uint32_t RetTimeValue;
	
	RetTimeValue = (uint32_t)MotorTimeSetting_Second(TIME_PWR_FULL-7, TIME_PWR_LOW-7); 

	// 10ms Timer 기준으로 나온 값을 2ms Timer에 사용할 수 있도록 처리
	RetTimeValue = RetTimeValue * 5;
	
	return (RetTimeValue);
}


