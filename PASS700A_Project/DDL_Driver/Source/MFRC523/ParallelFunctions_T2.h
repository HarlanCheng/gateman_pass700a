#ifndef __PARALLELFUNCTION_NEC_TI_INCLUDED
#define __PARALLELFUNCTION_NEC_TI_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"


void ParallelSOF(void);
void ParallelEOF(void);
void ParallelEOFContinuous(void);

void ParallelDataSend(BYTE *bData, BYTE bSize);
void ParallelDataReceive(BYTE* bpData, BYTE bSize);



#endif
