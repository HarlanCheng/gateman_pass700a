#define		_MODE_MENU_ONEFINGER_REGDEL_C_

#include	"main.h"


BYTE	bcd_slot_save;

#define _TCS4K_OVER_WRITE_ 1 

uint8_t flag_overwrite;

void		ModeGotoOneFingerRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGDEL;
	gbModePrcsStep = 0;
}


void		ModeGotoOneFingerRegister(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGISTER;
	gbModePrcsStep = 0;
}

void		ModeGotoOneFingerDelete(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_DELETE;
	gbModePrcsStep = 0;
}




void		OneFingerModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 지문 개별 등록
		case 1: 	
			ModeGotoOneFingerRegister();
			break;
	
		// 지문 개별 삭제
		case 3: 		
			ModeGotoOneFingerDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}

void		MenuOneFingerModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 지문 개별 등록
		case TENKEY_1:			
//			MenuSelectKeyTempSaveNTimeoutReset(65, gbInputKeyValue);	//지문등록 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_FINGER_SHARP, gbInputKeyValue);	//지문등록 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			break;

		// 지문 개별 삭제
		case TENKEY_3:		
//			MenuSelectKeyTempSaveNTimeoutReset(66, gbInputKeyValue);	//지문삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_FINGER_SHARP, gbInputKeyValue);	//지문삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			break;

		case TENKEY_SHARP:
			OneFingerModeSetSelected();
			break;
									
		case TENKEY_STAR:
			if(GetSupportCredentialCount() > 1)
				ModeGotoMenuAdvancedCredentialSelect();
			else 
				ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}

void		ModeOneFingerRegDelCheck(void )
{
	switch(gbModePrcsStep)
	{
		case 0:
//			Feedback13MenuOn(68, VOL_CHECK);		//지문 등록은 1번, 지문 삭제는 3번을 누르세요.
			Feedback13MenuOn(AVML_REGFINGER1_DELFINGER3, VOL_CHECK);		//지문 등록은 1번, 지문 삭제는 3번을 누르세요.
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOneFingerModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}


//==============================================================================//
// Comment	: 지문 번호가 메모리에 존재하는지 검사
// Return 	: 0:없음 	1:존재 함.
//==============================================================================//
BYTE	FingerSlotVerify()
{
	BYTE	bTmp, bCnt;

	for(bCnt = 0; bCnt < _MAX_REG_BIO; bCnt++){
		RomRead( &bTmp, (WORD)BIO_ID+(2*bCnt), 1);

		if ( bTmp == bcd_slot_save ) {		// EEPROM에 저장된 slot 정보와 비교
			gbFID = bCnt;
			return 1;
		}else{
			gbFID=0xff;
		}
	}

	return 0;	
}

void FingerSlotNumberToRegister(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();		//slot number는 1~20
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= _MAX_REG_BIO))
	{
		// 2자리의 Slot Number 입력만 처리하도록 수정
//		if(GetInputKeyCount() == 1)
//			bcd_slot_save = gPinInputKeyFromBeginBuf[0]>>4;		// save bcd slot no //한자리수 입력인 경우의 BCD 코드 생성
//		else			
			bcd_slot_save = gPinInputKeyFromBeginBuf[0];		// save bcd slot no

		TenKeyVariablesClear();

		bTmp = FingerSlotVerify();		// 지문 번호 일치 하는 것 확인	..
 
		if(bTmp == 0){			// 지문 번호 일치 하는 것 없음
 			// 해당 지문 번호 등록 시작.
			gbModePrcsStep++;
		}
		else {					
#ifdef _TCS4K_OVER_WRITE_
			gbModePrcsStep = 50;
#else			
					
			// 지문 번호 일치 하는 것 존재함 ..
//			FeedbackError(107, VOL_CHECK);		//이미 사용하고 있는 사용자번호입니다.
			FeedbackError(AVML_ALREADY_USE_SLOT, VOL_CHECK);		//이미 사용하고 있는 사용자번호입니다.
//			ModeClear();
			gbBioTimer10ms = 10;
			gbModePrcsStep = 30;		// reinput
#endif 			
		}
	}
	else
	{
		bcd_slot_save = 0xFF;		// clear saved slot no
//		FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();
//		gbBioTimer10ms = 10;
//		gbModePrcsStep = 30;		// reinput
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}
}

void BackwardFunctionForOneFingerRegDel(void)
{
	ReInputOrModeMove(ModeGotoOneFingerRegDelCheck);
}


void ModeOneFingerRegister(void)
{
	BYTE bTmp;
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if ( no_fingers >= _MAX_REG_BIO ) {			// 이미 최대로 가득 차 있으면 개별 등록 거부.
//				FeedbackError(125, VOL_CHECK);		// Error, 저장공간이 부족합니다.
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		// Error, 저장공간이 부족합니다.
				gbModePrcsStep=40;					// 개별 등록 삭제 메뉴로 가기
				break;
			}

//			FeedbackKeyPadLedOn(8, VOL_CHECK);			//개별 비밀번호 관리를 위하여 사용자 번호 두자리를 입력하세요, 계속하시려면 샵 버튼을 누르세요.
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//개별 비밀번호 관리를 위하여 사용자 번호 두자리를 입력하세요, 계속하시려면 샵 버튼을 누르세요.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(FingerSlotNumberToRegister, BackwardFunctionForOneFingerRegDel,1);
			break;

		case 2:
//			AudioFeedback(177, gcbBuzSta, VOL_CHECK);			// (음성)	등록하실 지운을 세번 입력하세요.
			AudioFeedback(AVML_ENTER_FINGER_3TIME, gcbBuzSta, VOL_CHECK);			// (음성)	등록하실 지운을 세번 입력하세요.
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
			FPMotorCoverAction(_OPEN);
#endif
			BioModuleON();
			LedModeRefresh();
			gbModePrcsStep++;
			break;
		
		case 3:
			if ( gbBioTimer10ms > BIO_WAKEUP_TM - 50 )
				break;		//500ms delay
			BioReTry();
			gbModePrcsStep++;
			break;

		case 4:
			if(gbBioTimer10ms>(BIO_WAKEUP_TM-43)) break;
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			gbFingerPTMode = FINGER_MODE_EACH_REGISTER;
			ModeGotoFingerPTOpen();
			break;



		case 5:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			// ENROLL  
			gbSQNum = gbSQNum+1;					// 전송 SQ NUM 10 의한 셋팅 .
			pFingerRx = &FingerRx.PACKET.Start_st;

			FingerWriteBuff[0] = bcd_slot_save;		// bcd slot #
			FingerWriteBuff[1] = bcd_slot_save;

			PTENROLL_SEND( FingerWriteBuff[0], FingerWriteBuff[1], no_fingers );			// ENROLL CMD 전송 
			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;			
			break;

		case 6: 	// PTENROLL SQ2
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}

			if(gfUART0RxEnd==1){
				gbModePrcsStep = 200;
				break;
			}
			if(gfUART0TxEnd){		// ENROLL CMD 전송 완료 
				gfUART0TxEnd=0;
				gfUART0RxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM+750;
				gbModePrcsStep++;
				gbBIORMCNT=0;
				gbFingerLongTimeWait100ms = 10;
				break;
			}
			break;			

		case 7: 	// PTENROLL SQ3   //   swipe check 부분
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			// FM ENROLL CMD ACK  
			if(gModeTimeOutTimer100ms == 0){
				BioModuleOff();
//				FeedbackError(101, VOL_CHECK);		//	times up
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			switch(gbInputKeyValue){				//
				case FUNKEY_OPCLOSE:
				case FUNKEY_REG:
					AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK);//(음성) 버튼음					
					gbModePrcsStep = 201;
					break;
			}

			if(gfUART0RxEnd == 1)	{
				gfUART0RxEnd=0;

				gbUart6_data=4;
				gfUART0RxEnd=0;
				gfUART0TxEnd=0;
				gfBioUARTST=0;
				gfBioUARTStuff=0;
				gfBioTXStuff=0;

				pFingerRx = &FingerRx.PACKET.Start_st+1;	//리셋 에크 수신 CMD 부
	
				if((*pFingerRx == 0x05)&& (gbErrorCnt<2)){		// PTOPEN ACK CMD 재 수신 받을 시 
					gbModePrcsStep = 5; 	// ENROLL  
					gbErrorCnt++;
					break;
				}
				pFingerRx = &FingerRx.PACKET.Start_st+7;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx == 0xD6){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFB){
//						FeedbackError(119, VOL_CHECK);//(음성) Check the fingerprint module.
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);//(음성) Check the fingerprint module.
						BioModuleOff();
						gbBIOErrorCNT++;
						ModeClear();
						break;
					}
				}

				pFingerRx = &FingerRx.PACKET.Start_st+7;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx == 0xDF){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFF){
//						FeedbackError(119, VOL_CHECK);//(음성) Check the fingerprint module.
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);//(음성) Check the fingerprint module.
						BioModuleOff();
						gbBIOErrorCNT++;
						ModeClear();
						break;
					}
				}
				pFingerRx = &FingerRx.PACKET.Start_st+10;		//ENROLL CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx != 0x20){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+11;			//메세지 준비 CMD 부

				if(*pFingerRx != 0x01){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+15;			//각 상황  CMD 부

				bTmp = *pFingerRx;

				if(bTmp == 0){						// #define PT_GUIMSG_GOOD_IMAGE (0) 3번 등록이 완료시			
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbModePrcsStep = 10; 					// 등록 완료 부분으로 점프 ... PTENROLL SQ6로 가기...
					LedSetting(gcbLedOff, LED_DISPLAY_OFF);
					break;
				}else if(bTmp == 0x20){ 				//#define PT_GUIMSG_GOOD_REG  (32) (각 상호아에서 정상 지문이면  나타남 )					
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbModePrcsStep = 22;
//					gbREG_CNT++;			//TCS4K로 인한 주석 체크(YMG는 원래 주석 처리 되어 있었음)				//등록 하실 지문을 입력 하세요	2회만 반복을 위한 값.
					AudioFeedback(VOICE_MIDI_FINGER,	gcbBuzNum, VOL_CHECK);//(음성) 버튼음					
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					//gbBIOErrCheck = bTmp;
					gbFingerLongTimeWait100ms = 0;
					break;

				}
				else if(bTmp== 0x0c){						// #define PT_GUIMSG_PUT_FINGER (12)	1번 등록이 완료 상황		
					gbREG_CNT = 1;			//TCS4K로 인한 추가 부분						//등록 하실 지문을 입력 하세요	2회만 반복을 위한 값.
					gbBIOErrCheck = bTmp;		//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){ //ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG1, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0d){ 				// 2번 등록 지문
					if(gbBIOErrCheck != bTmp){						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
//						AudioFeedback(44, gcbBuzNum, VOL_CHECK); //TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)		//The Fingerprint is registered for the first time. Swipe the same fingerprint again, 
						AudioFeedback(AVML_1ST_FINGER_IN, gcbBuzNum, VOL_CHECK); //TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)		//The Fingerprint is registered for the first time. Swipe the same fingerprint again, 
						gbBIOErrCheck = bTmp;						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					}											//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					gbREG_CNT = 2;			//TCS4K로 인한 추가 부분					//등록 하실 지문을 입력 하세요	2회만 반복을 위한 값.				
					
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){ //ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG2, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0e){ 				// 3번 등록 지문 
					if(gbBIOErrCheck != bTmp){						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
//						AudioFeedback(70, gcbBuzNum, VOL_CHECK); //TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)		// The Fingerprint is registered for the second time. Swipe the same fingerprint again, 
						AudioFeedback(AVML_2ND_FINGER_IN, gcbBuzNum, VOL_CHECK); //TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)		// The Fingerprint is registered for the second time. Swipe the same fingerprint again, 
						gbBIOErrCheck = bTmp;						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					}											//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					gbREG_CNT = 3;								//TCS4K로 인한 추가 부분		//등록 하실 지문을 입력 하세요	2회만 반복을 위한 값.
					
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){ //ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG3, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0f){ 				// 3번 등록 지문 
					gbBIORMCNT++;
					if((gbBIORMCNT > 200)){
						AudioFeedback(VOICE_MIDI_ERROR, gcbBuzErr, VOL_CHECK);		// 지문 을 떼어라.
						gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
						gbBIORMCNT=0;
					}	
				}else{										// 기타 지문이 잘못 들어 오면  0x1E : 너무 적게, 0x1C 너무 ㅂ 바르게 
					AudioFeedback(VOICE_MIDI_ERROR, gcbBuzErr, VOL_CHECK);		// 지문 을 떼어라.
					gbModePrcsStep++;

					gbFingerLongTimeWait100ms = 10;

					break;
				}
				
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				
				break;
			}

			break;

		case 8: 	// PTENROLL SQ4
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			gbSQNum = gbSQNum + 1;					// 전송 SQ 1 증가 
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTCON_SEND();								// Call back contin....CMD 전송 	


			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;

		case 9: 	// PTENROLL SQ5 --> ?SQ3
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}


			if(gfUART0TxEnd){							// Call back contin....CMD 전송 	완료		
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM+750;
				gbModePrcsStep = 7;
				break;
			}
			break;
		

		case 10: 	// PTENROLL SQ6
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			gbSQNum = gbSQNum + 1;			// 등록이 완료 되어 ... ID 받기 위해 Con 명령어 보냄..
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTCON_SEND();


			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;

		case 11:	// PTENROLL SQ7
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}


			if(gfUART0TxEnd){					// 송신 완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;

		case 12:	// PTENROLL SQ8  END   
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			if(gfUART0RxEnd == 1)	{		
				gfUART0RxEnd=0;

				pFingerRx = &FingerRx.PACKET.Start_st+7;		// 등록 CMD
	
				if(*pFingerRx == 0xE5){
					pFingerTx = &FingerTx.PACKET.Start_st;	
					gbBioTimer10ms = BIO_WAKEUP_TM;
					//LedSetting(gcbLedErr, LED_DISPLAY_OFF);
					//gbModeTimeOutTimer100ms = 300;	  //음성이 길어서 시간 늘림
					//AudioFeedback(VOICE_MIDI_ERROR, gcbBuzReg, VOL_HIGH);// The three scanned fingerprint data does not match.
//					gbModePrcsStep++;
					gbModePrcsStep = 100;

					break;
					
				}else if(*pFingerRx == 0x0F){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFC){
#if 1
						FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		//(음성)no more space to memorize.
						BioOffModeClear();
#else 
						gbFID = 19;
						no_fingers = _MAX_REG_BIO;						// 등록 수 증가..
						RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
						RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2);	//등록 ID 저장..	
						RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
						no_fingers = FingerReadBuff[0];
						DDLStatusFlagSet(DDL_STS_FAIL_BIO);
//						AudioFeedback(1, gcbBuzReg, VOL_CHECK); 	// Full 20 fingerprints have been registered
						AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK); 	// Full 20 fingerprints have been registered
						LedSetting(gcbLedOk, LED_DISPLAY_OFF);
						BioModuleOff();
						ModeClear();
#endif 						
					break;
					}
					
				}else if(*pFingerRx != 0x00){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD ACK

				if(*pFingerRx != 0x00){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD
	
				if(*pFingerRx != 0x02){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD ACK

				if(*pFingerRx != 0x12){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+11;		//// 등록 ID
				gbFID = *pFingerRx;

				no_fingers++; 					// 등록 수 증가..
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
				RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2);	//등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, ConvertFingerprintSlotNumber(gbFID));
#endif

				RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
				no_fingers = FingerReadBuff[0];

				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;

				// 지문 등록 정보 전송
				memset(bTmpArray, 0x00, 8);
				CopyCredentialDataForPack(bTmpArray, 8);
				PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
				// 지문 등록 정보 전송

				if(no_fingers >= _MAX_REG_BIO){ 								// 20개 모두 등록시 등록 완료.
//					AudioFeedback(1, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
					AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
					LedSetting(gcbLedOk, LED_DISPLAY_OFF);
					BioModuleOff();
					ModeClear();
					break;
				}

//				FeedbackModeCompletedKeepMode(42, VOL_CHECK);	//추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
				FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);	//추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
				gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
				gbModePrcsStep = 20;
				break;

			}

			break;

	
		case 20:			// 등록 연속  or 종료...시..
			switch(gbInputKeyValue){
				case FUNKEY_REG:
//					FeedbackModeCompleted(1, VOL_CHECK);
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
					FPMotorCoverAction(_CLOSE);
#endif
					BioOffModeClear();
					break;
//				case TENKEY_SHARP:
//					break;
				case TENKEY_STAR:
					// 추가 등록
					gbBioTimer10ms = 10;
					gbModePrcsStep = 0;		
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_HIGH);
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(gModeTimeOutTimer100ms	== 0){
//						FeedbackModeCompleted(1, VOL_CHECK);
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
						FPMotorCoverAction(_CLOSE);
#endif
						BioOffModeClear();
					}
					break;
				
				default:
					break;
			}
			break;


		case 22:
			if(GetBuzPrcsStep())	break;
			if(gfVoiceOut)	break;
			gbModePrcsStep = 8;
			gbBioTimer10ms = BIO_WAKEUP_TM;
			break;


		case 30:			//잘못된 입력(01~20 이 아닌 경우)이면 다시...
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
			ModeGotoOneFingerRegister();
			break;


		case 40:			//지문 등록이 최대로 된상태에서 개별 등록을 하려하면 에러 내고, 등록/삭제 메뉴로...
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
			ModeGotoOneFingerRegDelCheck();
			break;

#ifdef _TCS4K_OVER_WRITE_
		case 50:
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR
			flag_overwrite = 1;
			BioModuleON();
			LedModeRefresh();
			gbModePrcsStep++;
		break;
	
		case 51:
			if ( gbBioTimer10ms > BIO_WAKEUP_TM - 50 )
				break;		//500ms delay
			BioReTry();
			gbModePrcsStep++;
			break;

		case 52:
			if(gbBioTimer10ms>(BIO_WAKEUP_TM-43)) break;
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			gbFingerPTMode = FINGER_MODE_EACH_DELETE;
			ModeGotoFingerPTOpen();
			break;


		case 53:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			gbSQNum=1;
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTDEL_SEND(gbFID);

			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;			
			break;

		case 54:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			if(gfUART0TxEnd){		// 송신 완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;

		case 55: // PTENROLL SQ3 	: 반복 되는 부분 19 ~ 20..
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			if(gfUART0RxEnd == 1)	
			{		// 수신 완료 ..
				BYTE bResult[4] = {0x00,0x00,0x06,0x12};
				gfUART0RxEnd=0;

#if 1 // 예의 처리 변경 	
				// command tag + result code 를 전부 본다 
				// 0x1206+0000 

				pFingerRx = &FingerRx.PACKET.Start_st+7;

				if(memcmp(pFingerRx,bResult,4) != 0)
				{
					gbModePrcsStep=200;
					break;
				}
#else 
				pFingerRx = &FingerRx.PACKET.Start_st+9;

				if(*pFingerRx != 0x06){ 		// 지문 삭제 ack 명령어 .. 상위 1바이트 
	//					bTmp	= 3;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+10;

				if(*pFingerRx != 0x12){ 		// 지문 삭제 ack 명령어 .. 상위 1바이트 
	//					bTmp	= 4;
					break;
				}
#endif
				no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
				no_fingers--;
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
				FingerWriteBuff[0]=0xff;
				FingerWriteBuff[1]=0xff;
				
				RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	

				DDLStatusFlagClear(DDL_STS_FAIL_BIO);
				gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
				gbModePrcsStep = 2;
				BioModuleOff();
				Delay(SYS_TIMER_15MS);
				break;
			}
			break;
#endif 

		case 100:
//			FeedbackError(122, VOL_CHECK); 	//The Fingerprint is not registered.
			FeedbackError(AVML_ERR_FINGER_MSG1, VOL_CHECK); 	//The Fingerprint is not registered.
			BioOffModeClear();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
//			FeedbackError(119, VOL_CHECK); //			Check the fingerprint module.
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;



		default:
			BioOffModeClear();
			break;
	}
}



void FingerSlotNumberToDelete(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
//		FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();		//slot number는 1~20
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= _MAX_REG_BIO))
	{
		// 2자리의 Slot Number 입력만 처리하도록 수정
//		if(GetInputKeyCount() == 1)
//			bcd_slot_save = gPinInputKeyFromBeginBuf[0]>>4;		// save bcd slot no //한자리수 입력인 경우의 BCD 코드 생성
//		else			
			bcd_slot_save = gPinInputKeyFromBeginBuf[0];		// save bcd slot no

		TenKeyVariablesClear();

		bTmp = FingerSlotVerify();		// 지문 번호 일치 하는 것 확인	..
		if(bTmp == 0){			// 지문 번호 일치 하는 것 없음
			//지우는 시늉만 한다.
			gbBioTimer10ms = 10;
			gbModePrcsStep = 10;		// reinput
		}
		else {					// 지문 번호 일치 하는 것 존재함 ..
			// 해당 번호 지문 삭제 시작.
			gbModePrcsStep++;
		}

		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);	//# 버튼에 대한 효과음
	}
	else
	{
//		FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();
//		gbBioTimer10ms = 10;
//		gbModePrcsStep = 30;		// reinput
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}
}



void ModeOneFingerDelete(void)
{
//	BYTE bTmp;
	const BYTE bResult[4] = {0x00,0x00,0x06,0x12};

	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
/***	등록된 지문수와 관계없이 개별 삭제모드 진입하도록 주석처리
			if ( no_fingers == 0 ) {			// 등록된 지문이 없으면 ...
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); //
				gbModePrcsStep=40;					// 개별 등록 삭제 메뉴로 가기
				break;
			}
***/
//			FeedbackKeyPadLedOn(49, VOL_CHECK);			//삭제를 원하시는 사용자 번호를 입력하세요. 계속하시려면 샵 버튼을 누르세요.
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_FOR_DEL_SHARP, VOL_CHECK);			//삭제를 원하시는 사용자 번호를 입력하세요. 계속하시려면 샵 버튼을 누르세요.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(FingerSlotNumberToDelete, BackwardFunctionForOneFingerRegDel,0);
			break;

		case 2:
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			BioModuleON();
			LedModeRefresh();
			gbModePrcsStep++;
			break;
		
		case 3:
			if ( gbBioTimer10ms > BIO_WAKEUP_TM - 50 )
				break;		//500ms delay
			BioReTry();
			gbModePrcsStep++;
			break;

		case 4:
			if(gbBioTimer10ms>(BIO_WAKEUP_TM-43)) break;
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			gbFingerPTMode = FINGER_MODE_EACH_DELETE;
			ModeGotoFingerPTOpen();
			break;


		case 5:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			gbSQNum=1;
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTDEL_SEND(gbFID);

			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;			
			break;

		case 6:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			if(gfUART0TxEnd){		// 송신 완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;

		case 7:	// PTENROLL SQ3 	: 반복 되는 부분 19 ~ 20..
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}
			if(gfUART0RxEnd == 1)	{		// 수신 완료 ..
				gfUART0RxEnd=0;



#if 1 // 예의 처리 변경 	
				// command tag + result code 를 전부 본다 
				// 0x1206+0000 

				pFingerRx = &FingerRx.PACKET.Start_st+7;

				if(memcmp(pFingerRx,bResult,4) != 0)
				{
					gbModePrcsStep=200;
					break;
				}
#else 
				pFingerRx = &FingerRx.PACKET.Start_st+9;
	
				if(*pFingerRx != 0x06){ 		// 지문 삭제 ack 명령어 .. 상위 1바이트 
//					bTmp	= 3;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+10;

				if(*pFingerRx != 0x12){ 		// 지문 삭제 ack 명령어 .. 상위 1바이트 
//					bTmp	= 4;
					break;
				}
#endif
				no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
				no_fingers--;
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
				FingerWriteBuff[0]=0xff;
				FingerWriteBuff[1]=0xff;
				
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, ConvertFingerprintSlotNumber(gbFID));
#endif

				RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	

				DDLStatusFlagClear(DDL_STS_FAIL_BIO);
				gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
				gbModePrcsStep = 10;

				// 지문 삭제 정보 전송
				PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
				// 지문 삭제 정보 전송

				break;
			}
			break;

		case 10:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;			//버튼음 마침을 기다림
//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);		//완료되었습니다. 다른 설정을 원하시면 별표를 누르시고, 종료를 원하시면 R버튼을 누르세요.
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R2, VOL_CHECK);		//완료되었습니다. 추가 삭제를 원하시면 별표를 누르시고, 종료를 원하시면 R버튼을 누르세요.
			PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);			
			gbModePrcsStep++;
			break;

		case 11:			// 등록 연속  or 종료...시..
			switch(gbInputKeyValue){
				case FUNKEY_REG:
//					FeedbackModeCompleted(1, VOL_CHECK);
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					BioOffModeClear();
					break;
				case TENKEY_STAR:
					// 추가 삭제
					gbBioTimer10ms = 10;
					gbModePrcsStep = 0; 	
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_CHECK);
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(gModeTimeOutTimer100ms	== 0){
//						FeedbackModeCompleted(1, VOL_CHECK);
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
				
				default:
					break;
			}
			break;
			








		case 30:			//이미 등록된 slot에 등록하려 할 때 에러 내고, 재입력으로
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
			ModeGotoOneFingerDelete();
			break;


		case 40:			//등록된 지문이 없으면 에러 내고, 등록/삭제 메뉴로...
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ModeGotoOneFingerRegDelCheck();
			break;




		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
//			FeedbackError(119, VOL_CHECK); //			Check the fingerprint module.
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;


		default:
			BioOffModeClear();
			break;
	}

}



#ifdef	BLE_N_SUPPORT
void ModeGotoOneFingerRegisterByBleN(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGISTER_BYBLEN;
	gbModePrcsStep = 0;
}


void ModeGotoOneFingerDeleteByBleN(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_DELETE_BYBLEN;
	gbModePrcsStep = 0;
}


void ModeOneFingerRegisterByBleN(void)
{
	BYTE bTmp;
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if ( no_fingers >= _MAX_REG_BIO ) {			// 이미 최대로 가득 차 있으면 개별 등록 거부.
//				FeedbackError(125, VOL_CHECK);		// Error, 저장공간이 부족합니다.
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		// Error, 저장공간이 부족합니다.
				gbModePrcsStep=40;					// 개별 등록 삭제 메뉴로 가기
				break;
			}

			gbModePrcsStep++;
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			break;
			
		case 1:
			gPinInputKeyCnt = 2;
			bcd_slot_save = gPinInputKeyFromBeginBuf[0];		// save bcd slot no

//			TenKeyVariablesClear();
			gSelectedSlotNumber = (bcd_slot_save>>4)*10 + (bcd_slot_save&0x0F);		//BCD --> int (slot number는 1~20)

			bTmp = FingerSlotVerify();		// 지문 번호 일치 하는 것 확인	..
			if(bTmp == 0){			// 지문 번호 일치 하는 것 없음
				// 해당 지문 번호 등록 시작.
				gbModePrcsStep++;
			}
			else {					// 지문 번호 일치 하는 것 존재함 ..
#ifdef _TCS4K_OVER_WRITE_
				gbModePrcsStep = 50;
#else
				FeedbackError(AVML_ALREADY_USE_SLOT, VOL_CHECK);		//이미 사용하고 있는 사용자번호입니다.
				BioOffModeClear();
#endif 				
			}

			break;

		case 2:
//			AudioFeedback(177, gcbBuzSta, VOL_CHECK);			// (음성)	등록하실 지운을 세번 입력하세요.
			AudioFeedback(AVML_ENTER_FINGER_3TIME, gcbBuzSta, VOL_CHECK);			// (음성)	등록하실 지운을 세번 입력하세요.
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
			FPMotorCoverAction(_OPEN);
#endif
			BioModuleON();
			LedModeRefresh();
			gbModePrcsStep++;
			break;
		
		case 3:
			if ( gbBioTimer10ms > BIO_WAKEUP_TM - 50 )
				break;		//500ms delay
			BioReTry();
			gbModePrcsStep++;
			break;

		case 4:
			if(gbBioTimer10ms>(BIO_WAKEUP_TM-43)) break;
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			gbFingerPTMode = FINGER_MODE_EACH_REGISTER_BYBLEN;
			ModeGotoFingerPTOpen();
			break;

		case 5:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			// ENROLL  
			gbSQNum = gbSQNum+1;					// 전송 SQ NUM 10 의한 셋팅 .
			pFingerRx = &FingerRx.PACKET.Start_st;

			FingerWriteBuff[0] = bcd_slot_save;		// bcd slot #
			FingerWriteBuff[1] = bcd_slot_save;

			PTENROLL_SEND( FingerWriteBuff[0], FingerWriteBuff[1], no_fingers );			// ENROLL CMD 전송 
			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;			
			break;

		case 6: 	// PTENROLL SQ2
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}

			if(gfUART0RxEnd==1){
				gbModePrcsStep = 200;
				break;
			}
			if(gfUART0TxEnd){		// ENROLL CMD 전송 완료 
				gfUART0TxEnd=0;
				gfUART0RxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM+750;
				gbModePrcsStep++;
				gbBIORMCNT=0;
				gbFingerLongTimeWait100ms = 10;
				break;
			}
			break;			

		case 7: 	// PTENROLL SQ3   //   swipe check 부분
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			// FM ENROLL CMD ACK  
			if(gModeTimeOutTimer100ms == 0){
				BioModuleOff();
//				FeedbackError(101, VOL_CHECK);		//	times up
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			switch(gbInputKeyValue){				//
				case FUNKEY_OPCLOSE:
				case FUNKEY_REG:
					AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK);//(음성) 버튼음					
					gbModePrcsStep = 201;
					break;
			}

			if(gfUART0RxEnd == 1)	{
				gfUART0RxEnd=0;

				gbUart6_data=4;
				gfUART0RxEnd=0;
				gfUART0TxEnd=0;
				gfBioUARTST=0;
				gfBioUARTStuff=0;
				gfBioTXStuff=0;

				pFingerRx = &FingerRx.PACKET.Start_st+1;	//리셋 에크 수신 CMD 부
	
				if((*pFingerRx == 0x05)&& (gbErrorCnt<2)){		// PTOPEN ACK CMD 재 수신 받을 시 
					gbModePrcsStep = 5; 	// ENROLL  
					gbErrorCnt++;
					break;
				}
				pFingerRx = &FingerRx.PACKET.Start_st+7;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx == 0xD6){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFB){
//						FeedbackError(119, VOL_CHECK);//(음성) Check the fingerprint module.
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);//(음성) Check the fingerprint module.
						BioModuleOff();
						gbBIOErrorCNT++;
						ModeClear();
						break;
					}
				}

				pFingerRx = &FingerRx.PACKET.Start_st+7;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx == 0xDF){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFF){
//						FeedbackError(119, VOL_CHECK);//(음성) Check the fingerprint module.
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);//(음성) Check the fingerprint module.
						BioModuleOff();
						gbBIOErrorCNT++;
						ModeClear();
						break;
					}
				}
				pFingerRx = &FingerRx.PACKET.Start_st+10;		//ENROLL CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx != 0x20){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+11;			//메세지 준비 CMD 부

				if(*pFingerRx != 0x01){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+15;			//각 상황  CMD 부

				bTmp = *pFingerRx;

				if(bTmp == 0){						// #define PT_GUIMSG_GOOD_IMAGE (0) 3번 등록이 완료시			
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbModePrcsStep = 10; 					// 등록 완료 부분으로 점프 ... PTENROLL SQ6로 가기...
					LedSetting(gcbLedOff, LED_DISPLAY_OFF);
					break;
				}else if(bTmp == 0x20){ 				//#define PT_GUIMSG_GOOD_REG  (32) (각 상호아에서 정상 지문이면  나타남 )					
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbModePrcsStep = 22;
//					gbREG_CNT++;			//TCS4K로 인한 주석 체크(YMG는 원래 주석 처리 되어 있었음)				//등록 하실 지문을 입력 하세요	2회만 반복을 위한 값.
					AudioFeedback(VOICE_MIDI_FINGER,	gcbBuzNum, VOL_CHECK);//(음성) 버튼음					
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					//gbBIOErrCheck = bTmp;
					gbFingerLongTimeWait100ms = 0;
					break;

				}
				else if(bTmp== 0x0c){						// #define PT_GUIMSG_PUT_FINGER (12)	1번 등록이 완료 상황		
					gbREG_CNT = 1;			//TCS4K로 인한 추가 부분						//등록 하실 지문을 입력 하세요	2회만 반복을 위한 값.
					gbBIOErrCheck = bTmp;		//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){ //ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG1, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0d){ 				// 2번 등록 지문
					if(gbBIOErrCheck != bTmp){						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
//						AudioFeedback(44, gcbBuzNum, VOL_CHECK); //TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)		//The Fingerprint is registered for the first time. Swipe the same fingerprint again, 
						AudioFeedback(AVML_1ST_FINGER_IN, gcbBuzNum, VOL_CHECK); //TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)		//The Fingerprint is registered for the first time. Swipe the same fingerprint again, 
						gbBIOErrCheck = bTmp;						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					}											//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					gbREG_CNT = 2;			//TCS4K로 인한 추가 부분					//등록 하실 지문을 입력 하세요	2회만 반복을 위한 값.				
					
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){ //ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG2, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0e){ 				// 3번 등록 지문 
					if(gbBIOErrCheck != bTmp){						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
//						AudioFeedback(70, gcbBuzNum, VOL_CHECK); //TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)		// The Fingerprint is registered for the second time. Swipe the same fingerprint again, 
						AudioFeedback(AVML_2ND_FINGER_IN, gcbBuzNum, VOL_CHECK); //TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)		// The Fingerprint is registered for the second time. Swipe the same fingerprint again, 
						gbBIOErrCheck = bTmp;						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					}											//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					gbREG_CNT = 3;								//TCS4K로 인한 추가 부분		//등록 하실 지문을 입력 하세요	2회만 반복을 위한 값.
					
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){ //ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG3, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0f){ 				// 3번 등록 지문 
					gbBIORMCNT++;
					if((gbBIORMCNT > 200)){
						AudioFeedback(VOICE_MIDI_ERROR, gcbBuzErr, VOL_CHECK);		// 지문 을 떼어라.
						gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
						gbBIORMCNT=0;
					}	
				}else{										// 기타 지문이 잘못 들어 오면  0x1E : 너무 적게, 0x1C 너무 ㅂ 바르게 
					AudioFeedback(VOICE_MIDI_ERROR, gcbBuzErr, VOL_CHECK);		// 지문 을 떼어라.
					gbModePrcsStep++;

					gbFingerLongTimeWait100ms = 10;

					break;
				}
				
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				
				break;
			}

			break;

		case 8: 	// PTENROLL SQ4
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			gbSQNum = gbSQNum + 1;					// 전송 SQ 1 증가 
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTCON_SEND();								// Call back contin....CMD 전송 	


			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;

		case 9: 	// PTENROLL SQ5 --> ?SQ3
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}


			if(gfUART0TxEnd){							// Call back contin....CMD 전송 	완료		
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM+750;
				gbModePrcsStep = 7;
				break;
			}
			break;
		

		case 10: 	// PTENROLL SQ6
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			gbSQNum = gbSQNum + 1;			// 등록이 완료 되어 ... ID 받기 위해 Con 명령어 보냄..
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTCON_SEND();


			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;

		case 11:	// PTENROLL SQ7
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}


			if(gfUART0TxEnd){					// 송신 완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;

		case 12:	// PTENROLL SQ8  END   
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			if(gfUART0RxEnd == 1)	{		
				gfUART0RxEnd=0;

				pFingerRx = &FingerRx.PACKET.Start_st+7;		// 등록 CMD
	
				if(*pFingerRx == 0xE5){
					pFingerTx = &FingerTx.PACKET.Start_st;	
					gbBioTimer10ms = BIO_WAKEUP_TM;
					//LedSetting(gcbLedErr, LED_DISPLAY_OFF);
					//gbModeTimeOutTimer100ms = 300;	  //음성이 길어서 시간 늘림
					//AudioFeedback(VOICE_MIDI_ERROR, gcbBuzReg, VOL_HIGH);// The three scanned fingerprint data does not match.
//					gbModePrcsStep++;
					gbModePrcsStep = 100;

					break;
					
				}else if(*pFingerRx == 0x0F){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFC){
#if 1 // 등록 과정중 3번째 지문을 읽고 나서 0xFC0F error 이 나오면 그냥 error feedack 이후 modeclear 하는 걸로 
						FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK); 	//(음성)no more space to memorize.
						BioOffModeClear();
#else 						
						gbFID = 19;
						no_fingers = _MAX_REG_BIO;						// 등록 수 증가..
						RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
						RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2);	//등록 ID 저장..	
						RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
						no_fingers = FingerReadBuff[0];
						DDLStatusFlagSet(DDL_STS_FAIL_BIO);
//						AudioFeedback(1, gcbBuzReg, VOL_CHECK); 	// Full 20 fingerprints have been registered
						AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK); 	// Full 20 fingerprints have been registered
						LedSetting(gcbLedOk, LED_DISPLAY_OFF);
						BioModuleOff();
						ModeClear();
#endif 						
					break;
					}
					
				}else if(*pFingerRx != 0x00){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD ACK

				if(*pFingerRx != 0x00){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD
	
				if(*pFingerRx != 0x02){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD ACK

				if(*pFingerRx != 0x12){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+11;		//// 등록 ID
				gbFID = *pFingerRx;

				no_fingers++; 					// 등록 수 증가..
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
				RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2);	//등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, ConvertFingerprintSlotNumber(gbFID));
#endif

				RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
				no_fingers = FingerReadBuff[0];

				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;

//				AudioFeedback(1, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
				AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK); 	// Full 20 fingerprints have been registered
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
				FPMotorCoverAction(_CLOSE);
#endif
				LedSetting(gcbLedOk, LED_DISPLAY_OFF);
				BioModuleOff();
				ModeClear();

				// 지문 등록 정보 전송
				memset(bTmpArray, 0x00, 8);
				CopyCredentialDataForPack(bTmpArray, 8);
				PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
				// 지문 등록 정보 전송
				break;

			}
			break;

		case 22:
			if(GetBuzPrcsStep())	break;
			if(gfVoiceOut)	break;
			gbModePrcsStep = 8;
			gbBioTimer10ms = BIO_WAKEUP_TM;
			break;


		case 30:			//잘못된 입력(01~20 이 아닌 경우)이면 다시...
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
			ModeGotoOneFingerRegister();
			break;


		case 40:			//지문 등록이 최대로 된상태에서 개별 등록을 하려하면 에러 내고, 등록/삭제 메뉴로...
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
			ModeGotoOneFingerRegDelCheck();
			break;

#ifdef _TCS4K_OVER_WRITE_
		case 50:
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR
			flag_overwrite = 1;
			BioModuleON();
			LedModeRefresh();
			gbModePrcsStep++;
		break;
	
		case 51:
			if ( gbBioTimer10ms > BIO_WAKEUP_TM - 50 )
				break;		//500ms delay
			BioReTry();
			gbModePrcsStep++;
			break;

		case 52:
			if(gbBioTimer10ms>(BIO_WAKEUP_TM-43)) break;
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			gbFingerPTMode = FINGER_MODE_EACH_DELETE_BYBLEN;
			ModeGotoFingerPTOpen();
			break;


		case 53:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			gbSQNum=1;
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTDEL_SEND(gbFID);

			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;			
			break;

		case 54:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			if(gfUART0TxEnd){		// 송신 완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;

		case 55: // PTENROLL SQ3	: 반복 되는 부분 19 ~ 20..
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			if(gfUART0RxEnd == 1)	
			{		// 수신 완료 ..
				BYTE bResult[4] = {0x00,0x00,0x06,0x12};
				gfUART0RxEnd=0;

#if 1 // 예의 처리 변경 	
				// command tag + result code 를 전부 본다 
				// 0x1206+0000 

				pFingerRx = &FingerRx.PACKET.Start_st+7;

				if(memcmp(pFingerRx,bResult,4) != 0)
				{
					gbModePrcsStep=200;
					break;
				}
#else 
				pFingerRx = &FingerRx.PACKET.Start_st+9;

				if(*pFingerRx != 0x06){ 		// 지문 삭제 ack 명령어 .. 상위 1바이트 
	//					bTmp	= 3;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+10;

				if(*pFingerRx != 0x12){ 		// 지문 삭제 ack 명령어 .. 상위 1바이트 
	//					bTmp	= 4;
					break;
				}
#endif
				no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
				no_fingers--;
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
				FingerWriteBuff[0]=0xff;
				FingerWriteBuff[1]=0xff;
				
				RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	

				DDLStatusFlagClear(DDL_STS_FAIL_BIO);
				gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
				gbModePrcsStep = 2;
				BioModuleOff();
				Delay(SYS_TIMER_15MS);
				break;
			}
			break;
#endif 


		case 100:
//			FeedbackError(122, VOL_CHECK); 	//The Fingerprint is not registered.
			FeedbackError(AVML_ERR_FINGER_MSG1, VOL_CHECK); 	//The Fingerprint is not registered.
			BioOffModeClear();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
//			FeedbackError(119, VOL_CHECK); //			Check the fingerprint module.
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;



		default:
			BioOffModeClear();
			break;
	}
}


void ModeOneFingerDeleteByBleN(void)
{
//	BYTE bTmp;
	const BYTE bResult[4] = {0x00,0x00,0x06,0x12};

	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
/***	등록된 지문수와 관계없이 개별 삭제모드 진입하도록 주석처리
			if ( no_fingers == 0 ) {			// 등록된 지문이 없으면 ...
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); //
				gbModePrcsStep=40;					// 개별 등록 삭제 메뉴로 가기
				break;
			}
***/
			gbModePrcsStep++;
			break;
			
		case 1:
			gPinInputKeyCnt = 2;
			FingerSlotNumberToDelete();
			break;

		case 2:
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			BioModuleON();
			LedModeRefresh();
			gbModePrcsStep++;
			break;
		
		case 3:
			if ( gbBioTimer10ms > BIO_WAKEUP_TM - 50 )
				break;		//500ms delay
			BioReTry();
			gbModePrcsStep++;
			break;

		case 4:
			if(gbBioTimer10ms>(BIO_WAKEUP_TM-43)) break;
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			gbFingerPTMode = FINGER_MODE_EACH_DELETE_BYBLEN;
			ModeGotoFingerPTOpen();
			break;


		case 5:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			gbSQNum=1;
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTDEL_SEND(gbFID);

			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;			
			break;

		case 6:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			if(gfUART0TxEnd){		// 송신 완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;

		case 7:	// PTENROLL SQ3 	: 반복 되는 부분 19 ~ 20..
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}
			if(gfUART0RxEnd == 1)	{		// 수신 완료 ..
				gfUART0RxEnd=0;
#if 1 // 예의 처리 변경 	
				// command tag + result code 를 전부 본다 
				// 0x1206+0000 

				pFingerRx = &FingerRx.PACKET.Start_st+7;

				if(memcmp(pFingerRx,bResult,4) != 0)
				{
					gbModePrcsStep=200;
					break;
				}
#else 
				pFingerRx = &FingerRx.PACKET.Start_st+9;
	
				if(*pFingerRx != 0x06){ 		// 지문 삭제 ack 명령어 .. 상위 1바이트 
//					bTmp	= 3;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+10;

				if(*pFingerRx != 0x12){ 		// 지문 삭제 ack 명령어 .. 상위 1바이트 
//					bTmp	= 4;
					break;
				}
#endif
				no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
				no_fingers--;
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
				FingerWriteBuff[0]=0xff;
				FingerWriteBuff[1]=0xff;
				
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, ConvertFingerprintSlotNumber(gbFID));
#endif

				RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	

				DDLStatusFlagClear(DDL_STS_FAIL_BIO);
				gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
				gbModePrcsStep = 10;

				// 지문 삭제 정보 전송
				PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
				// 지문 삭제 정보 전송

				break;
			}
			break;

		case 10:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;			//버튼음 마침을 기다림

//			AudioFeedback(1, gcbBuzReg, VOL_CHECK); // Full 20 fingerprints have been registered
			AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK); 	// Full 20 fingerprints have been registered
			LedSetting(gcbLedOk, LED_DISPLAY_OFF);
			BioModuleOff();
			ModeClear();
			break;







		case 30:			//이미 등록된 slot에 등록하려 할 때 에러 내고, 재입력으로
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
			ModeGotoOneFingerDelete();
			break;


		case 40:			//등록된 지문이 없으면 에러 내고, 등록/삭제 메뉴로...
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ModeGotoOneFingerRegDelCheck();
			break;




		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
//			FeedbackError(119, VOL_CHECK); //			Check the fingerprint module.
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;


		default:
			BioOffModeClear();
			break;
	}

}
#endif




