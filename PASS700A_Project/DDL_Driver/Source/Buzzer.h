#ifndef __BUZZER_INCLUDED        
#define __BUZZER_INCLUDED   

#include "DefineMacro.h"
//#include "DefinePin.h"

																				
//============================================================
//      BUZZER MELODY(1 clock = 437KHz)(14MHz/32)        
//============================================================
//      Data = 14MHz/(ideal*2*32)                                        
//============================================================
//                              		Data		ideal			real            

/*
#define         BZ_DO6N		209      	//1046.502	;1046.651
#define         BZ_DO6U       197      	//1108.731    	;1110.406
#define         BZ_RE6N		186      	//1174.659    	;1176.075
#define         BZ_RE6U        176      	//1244.508    	;1242.898
#define         BZ_ME6N		166      	//1318.510    	;1317.771
#define         BZ_FA6N		157      	//1396.913    	;1393.312
#define         BZ_FA6U		148      	//1479.978    	;1478.041
#define         BZ_SO6N		140      	//1567.982    	;1562.500
#define         BZ_SO6U		132      	//1661.219    	;1657.191
#define         BZ_RA6N		124     	//1760.000    	;1764.113
#define         BZ_RA6U		117      	//1864.655    	;1869.658
#define         BZ_SI6N		111		//1975.533    	;1970.721
#define         BZ_DO7N		105    	//2093.005    	;2083.333        
#define         BZ_DO7U		99     	//2217.461    	;2209.596        
#define         BZ_RE7N		93      	//2349.318    	;2352.151
#define         BZ_RE7U		88		//2489.016    	;2485.795
#define         BZ_ME7N		83      	//2637.020    	;2635.542
#define         BZ_FA7N		78      	//2793.826    	;2804.487
#define         BZ_FA7U		74      	//2959.955    	;2956.081
#define         BZ_SO7N		70      	//3135.963    	;3125.000
#define         BZ_SO7U		66      	//3322.438    	;3314.394
#define         BZ_RA7N		62      	//3520.000    	;3528.226
#define         BZ_RA7U		59      	//3729.310    	;3707.627
#define         BZ_SI7N		55      	//3951.066    	;3977.273
#define         BZ_DO8N		52      	//4186.009    	;4206.731
#define         BZ_DO8U		49      	//4434.922    	;4464.286
#define         BZ_ENDD		255

*/
/*
#define         BZ_DO6N		13377     	//1046.502	;1046.651
#define         BZ_DO6U       	12626     	//1108.731    	;1110.406
#define         BZ_RE6N		11917     	//1174.659    	;1176.075
#define         BZ_RE6U        	11248    	//1244.508    	;1242.898
#define         BZ_ME6N		10617     	//1318.510    	;1317.771
#define         BZ_FA6N		10021     	//1396.913    	;1393.312
#define         BZ_FA6U		9459      	//1479.978    	;1478.041
#define         BZ_SO6N		8928      	//1567.982    	;1562.500
#define         BZ_SO6U		8427      	//1661.219    	;1657.191
#define         BZ_RA6N		7954     	//1760.000    	;1764.113
#define         BZ_RA6U		7507      	//1864.655    	;1869.658
#define         BZ_SI6N		7086		//1975.533    	;1970.721
#define         BZ_DO7N		6688    	//2093.005    	;2083.333        
#define         BZ_DO7U		6313     	//2217.461    	;2209.596        
#define         BZ_RE7N		5958      	//2349.318    	;2352.151
#define         BZ_RE7U		5624		//2489.016    	;2485.795
#define         BZ_ME7N		5308      	//2637.020    	;2635.542
#define         BZ_FA7N		5010      	//2793.826    	;2804.487
#define         BZ_FA7U		4729      	//2959.955    	;2956.081
#define         BZ_SO7N		4463      	//3135.963    	;3125.000
#define         BZ_SO7U		4213      	//3322.438    	;3314.394
#define         BZ_RA7N		3976      	//3520.000    	;3528.226
#define         BZ_RA7U		3753      	//3729.310    	;3707.627
#define         BZ_SI7N		3542      	//3951.066    	;3977.273
#define         BZ_DO8N		3343      	//4186.009    	;4206.731
#define         BZ_DO8U		3156      	//4434.922    	;4464.286
#define         BZ_ENDD		255
*/
#define         BZ_DO6N		1047     	//1046.502	;1046.651
#define         BZ_DO6U     1110     	//1108.731    	;1110.406
#define         BZ_RE6N		1176     	//1174.659    	;1176.075
#define         BZ_RE6U     1242    	//1244.508    	;1242.898
#define         BZ_ME6N		1317     	//1318.510    	;1317.771
#define         BZ_FA6N		1393     	//1396.913    	;1393.312
#define         BZ_FA6U		1478      	//1479.978    	;1478.041
#define         BZ_SO6N		1562      	//1567.982    	;1562.500
#define         BZ_SO6U		1657      	//1661.219    	;1657.191
#define         BZ_RA6N		1764     	//1760.000    	;1764.113
#define         BZ_RA6U		1869      	//1864.655    	;1869.658
#define         BZ_SI6N		1970		//1975.533    	;1970.721
#define         BZ_DO7N		2083    	//2093.005    	;2083.333        
#define         BZ_DO7U		2209     	//2217.461    	;2209.596        
#define         BZ_RE7N		2352      	//2349.318    	;2352.151
#define         BZ_RE7U		2485		//2489.016    	;2485.795
#define         BZ_ME7N		2635      	//2637.020    	;2635.542
#define         BZ_FA7N		2804      	//2793.826    	;2804.487
#define         BZ_FA7U		2956      	//2959.955    	;2956.081  ..
#define         BZ_SO7N		3125      	//3135.963    	;3125.000
#define         BZ_SO7U		3314      	//3322.438    	;3314.394
#define         BZ_RA7N		3528      	//3520.000    	;3528.226
#define         BZ_RA7U		3707      	//3729.310    	;3707.627
#define         BZ_SI7N		3977      	//3951.066    	;3977.273
#define         BZ_DO8N		4206      	//4186.009    	;4206.731
#define         BZ_DO8U		4464      	//4434.922    	;4464.286
#define         BZ_ENDD		255


//#define		BZ_WAR_START	BZ_SI6N
#define		BZ_WAR_START	BZ_FA7N
#define		BZ_WAR_STEP	50


//--------------------------------------------------------------------

extern const WORD gcbBuzOff[];																				
extern const WORD gcbBuzPwr[];
extern const WORD gcbBuzOpn[];
extern const WORD gcbBuzCls[];    
extern const WORD gcbBuzReg[];
extern const WORD gcbBuzSta[];
extern const WORD gcbBuzNum[];
#if defined (DDL_CFG_MS)
extern const WORD gcbBuzEnterVerify[];
#endif 
extern const WORD gcbBuzErr[];
extern const WORD gcbBuzErr3[];
extern const WORD gcbBuzMag[];
extern const WORD gcbBuzLow[];
extern const WORD gcbBuzWar[];              
extern const WORD gcbBuzBroken[];
extern const WORD gcbBuzFire[];
extern const WORD gcbBuzInlock[];

extern const WORD gcbBuzSysFrontNoConnect[];
extern const WORD gcbBuzSysErr4[];
extern const WORD gcbBuzSysErr5[];
extern const WORD gcbBuzSysErr6[];
extern const WORD gcbBuzSysErr7[];
extern const WORD gcbBuzSysErr8[];
extern const WORD gcbBuzSysErr9[];
extern const WORD gcbBuzSysErr10[];
extern const WORD gcbBuzSysErr11[]; 
extern const WORD gcbBuzSysErr12[]; 
extern const WORD gcbBuzPackLedControl[]; 



 
//extern WORD gbBuzPrcsStep;
extern uint32_t gdwBuzTimerInitCnt;
extern BYTE gbFirstVoice;


void SetupBuzzer(void);
void BuzzerProcess(void);
void BuzzerOff(void);
void BuzzerSetting(WORD const* bpBuz, WORD bMode);
void Setfreq(WORD freq);
BYTE GetBuzPrcsStep(void);

void TIM6_IRQHandler(void);
void BuzzerFeedbackinit();
void BuzzerFeedbackStart();
void BuzzerFeedbackStop();
void BuzzerFeedbackCallback(void);



//AudioFeedback의 bMode
//bit 0 : 1->매너모드에 따른 처리, 	0->매너모드 무시
//bit 1 : 1->설정된 볼륨에 따른 처리, 	0->최고 볼륨 처리
#define	MANNER_CHK		0x01
#define	VOL_CHECK		0x02
#define	VOL_HIGH		0x04

/*#define	MANNER_CHK		0x04
#define	VOL_CHECK		0x04
#define	VOL_HIGH		0x04*/


#define Freq                    13999    //1Khz, Freq = HFPERCLK/(2^PRSCx(TOP+1))



#endif
