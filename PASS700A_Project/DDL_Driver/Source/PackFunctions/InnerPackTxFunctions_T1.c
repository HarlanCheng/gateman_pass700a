//------------------------------------------------------------------------------
/** 	@file		InnerPackTxFunctions_T1.c
	@version 0.1.00
	@date	2016.06.13
	@brief	Communication Pack TX Process Functions
	@see	
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.06.13		by Jay
			- 통신팩을 TX Data 전송 처리에 대한 함수
*/
//------------------------------------------------------------------------------

#include "Main.h"


WORD gbInnerPackTxByteCnt = 0;		// 송신한 바이트 수 
WORD gbInnerPackTxByteNum = 0;	// 송신할 바이트 수 
BYTE gbInnerPackTxDataBuf[MAX_INNER_PACK_BUF_SIZE];

BYTE gbInnerPackTxDataQueue[MAX_INNER_PACK_TX_QUEUE_SIZE][MAX_INNER_PACK_BUF_SIZE];
BYTE gbInnerPackTxWaitTimeQueue[MAX_INNER_PACK_TX_QUEUE_SIZE];
WORD gbInnerPackTxQueue_front = 0;		// 송신 Queue의 맨 앞부분
WORD gbInnerPackTxQueue_rear = 0;		// 송신 Queue의 맨 뒷부분
WORD gbInnerPackTxQueue_sending = 0;	// 송신중인 Queue index
BYTE gbInnerPackTxByteNum_Queue = 0;	// 송신할 Queue의 바이트 수
BYTE gbInnerPackTxAddNone[10];
bool gfInnerPackAckWait = false;				// true : 패킷 송신후 응답(Ack, Nack) 대기중
bool gfInnerPackQueueSending = false;		// true : 패킷 송신중
bool gfInnerPackDirectSending = false;			// true : Main Loop - InnerPackTxDataProcess 기다리지 않고 직접 송신중

#define	INNER_PACK_TX_ENQUEUE_FIRSTLY		0x00
#define	INNER_PACK_TX_ENQUEUE_LASTLY		0xFF
BYTE gbInnerPackTxEnqueueOrder = INNER_PACK_TX_ENQUEUE_LASTLY;	// 0x00 : Pack Tx Queue에 추가시 맨 앞에 추가(먼저 보낼 내용 추가시 사용), 0xFF : 맨 뒤에 추가(보통 상태)

BYTE gbInnerPackRetryCount = 0x00;
BYTE gbInnerPackAckCompleteData[2] = {0x00,0x00};


BYTE gbInnerPackTxEnDataSendbuffer[MAX_PACK_BUF_SIZE];
BYTE gbInnerPackTxRandomKeybuffer[16];
WORD gbInnerPackTxEnDataByteNum = 0;
BYTE gbInnerpackModeSuspend = 0x00;



/*
#define	INNER_PACK_TX_ENSIG_TLUS_DEFAULT		300
#define	INNER_PACK_TX_ENSIG_TLMS_DEFAULT		0
#define	INNER_PACK_TX_ENSIG_TWUUS_DEFAULT		500
#define	INNER_PACK_TX_ENSIG_TWUMS_DEFAULT		0
*/


#define	INNER_PACK_TX_ENSIG_TLUS_DEFAULT		100
#define	INNER_PACK_TX_ENSIG_TLMS_DEFAULT		0
#define	INNER_PACK_TX_ENSIG_TWUUS_DEFAULT		150
#define	INNER_PACK_TX_ENSIG_TWUMS_DEFAULT		0



BYTE gInnerPackTxProcessStep = 0;
WORD gInnerPackTxEnSigTimer2ms = 0;

WORD gInnerPackTxEnSigTl1us = 0;
WORD gInnerPackTxEnSigTl1ms = 0;
WORD gInnerPackTxEnSigTwu1us = 0;
WORD gInnerPackTxEnSigTwu1ms = 0;


WORD gInnerPackTxAckWaitTimer10ms = 0;
BYTE gInnerPackTxWaitTimer10ms = 0;

BYTE gInnerPackTxAckWaitIndex;
BYTE gInnerPackTxAckWaitTimer100ms;
/*
gbInnerAckWaitBuffer[][timer,count,Datalength,Data .........]
*/
BYTE gbInnerAckWaitBuffer[ACK_WAIT_BUFFER_SIZE][MAX_PACK_BUF_SIZE+3];



__weak  BYTE Get_Checksum_BCC(BYTE *buf, BYTE len)
{
	BYTE i;
	BYTE x = 0;

	for(i = 0 ;  i < len; i++)
	{
		x ^= buf[i];
	}
	return x;
}



__weak  WORD Get_Checksum_CRC16(BYTE *buf, BYTE len)
{
	WORD crc = 0xffff; // initial value
	WORD x;
	BYTE i;

	for(i = 0; i < len; i++)
	{
		x = ((crc >> 8) ^ buf[i]) & 0xff;
		x ^= x >> 4;
		crc = (crc << 8) ^ (x << 12) ^ (x << 5) ^ x;
	}
	return crc;
}


bool InnerPackTx_QueueIsFull(void)
{
	if(((gbInnerPackTxQueue_rear+1) % MAX_INNER_PACK_TX_QUEUE_SIZE) == gbInnerPackTxQueue_front)
		return true;
	else
		return false;
}


void InnerPackTx_EnQueue(void)		// Queue에 추가 하기 : 방금 준비한 데이터를 Queue 맨 뒤에 추가하기 (순서대로 처리)
{
	BYTE bCnt;

	gbInnerPackTxQueue_rear = (gbInnerPackTxQueue_rear + 1) % MAX_INNER_PACK_TX_QUEUE_SIZE;
	for(bCnt = 0; bCnt < gbInnerPackTxByteNum; bCnt++)
	{
		gbInnerPackTxDataQueue[gbInnerPackTxQueue_rear][bCnt] = gbInnerPackTxDataBuf[bCnt];
	}
	// 바로 전송
	gbInnerPackTxWaitTimeQueue[gbInnerPackTxQueue_rear] = 0;
}

void InnerPackTx_MakeNack(BYTE* bpData , BYTE bMajor_Code, BYTE bMinor_Code)	// RF Module이 잘못된 명령을 보낸 경우 Negative Ack를 보냄
{
	BYTE		tmpArray[3] = {0,0,0};
	BYTE		tmpSize = 0;
	
	if(gbInnerModuleMode == 0)		return;

	if(gbInnerModuleProtocolVersion >= 0x30)
	{
		tmpArray[0] = bMajor_Code;
		tmpArray[1] = bMinor_Code;
		tmpSize = 2;
	}
	else
	{	
		tmpArray[0] = 0x00;
		tmpSize = 0;
	}	

	InnerPackTx_MakePacketSinglePort(bpData[PACK_F_EVENT], ES_LOCK_NACK, &bpData[PACK_F_COUNT], tmpArray, tmpSize);
}

void InnerPackTx_MakePacketSinglePort(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength)
{
	BYTE bCnt;

	if(gbInnerModuleMode)
	{
		gbInnerPackTxDataBuf[PACK_F_EVENT] = bEvent;
		gbInnerPackTxDataBuf[PACK_F_SOURCE] = bEventSrc;
		if(bEventSrc == ES_LOCK_EVENT)	(*bCount)++;			// Event 패킷일 때만 Count 증가
		gbInnerPackTxDataBuf[PACK_F_COUNT] = (*bCount);
		gbInnerPackTxDataBuf[PACK_F_LENGTH_ADDDATA] = bLength ; // checksum(1바이트) 길이 추가 
		for(bCnt = 0; bCnt < bLength; bCnt++)
		{
			gbInnerPackTxDataBuf[4 + bCnt] = *bpData;
			bpData++;
		}
		gbInnerPackTxByteNum = 4 + bLength;
		InnerPackTx_AddChecksum(gbInnerPackTxDataBuf, &gbInnerPackTxByteNum);

	//	if(gbPackTxEnqueueOrder == PACK_TX_ENQUEUE_FIRSTLY)
	//	{
	//		PackTx_EnQueueAtFirst();
	//	}
	//	else
	//	{
			if((!gfInnerPackDirectSending) &&	!InnerPackTx_QueueIsFull()) InnerPackTx_EnQueue();
	//	}
	}
	
}


void InnerPackTxWaitTime_EnQueue(BYTE TxWaitTime)
{
	gbInnerPackTxWaitTimeQueue[gbInnerPackTxQueue_rear] = TxWaitTime;
}

void InnerPackTx_AddChecksum(BYTE *buf, WORD *len)
{
	buf[*len] = Get_Checksum_BCC(buf,*len);
	*len = *len + 1;
}

BYTE GetInnerPackTxProcessStep(void)
{
	return (gInnerPackTxProcessStep); 
}

#if defined (P_BT_WAKEUP_T)

void InnerPackTx_QueueInit(void)
{
	gbInnerPackTxQueue_front = 0;
	gbInnerPackTxQueue_rear = 0;
	gbInnerPackTxAddNone[0] = 0;
	gfInnerPackAckWait = false;
//	gbInnerPackTxEnqueueOrder = INNER_PACK_TX_ENQUEUE_LASTLY;
}



bool InnerPackTx_QueueIsEmpty(void)
{
	if(gbInnerPackTxQueue_front == gbInnerPackTxQueue_rear)
		return true;
	else
		return false;
}

void InnerPackTx_EnQueueAtFirst(void)	// Queue 맨 앞에 추가 하기 :  방금 준비한 데이터를 가장 먼저 보내기 위해 보낼 데이터를 Queue 맨 앞에 추가하기 
{
//	BYTE bCnt;
//
//	if(InnerPackTx_QueueIsFull())
//	{
//		for(bCnt = 0; bCnt < gbInnerPackTxByteNum; bCnt++)
//			gbInnerPackTxDataQueue[gbInnerPackTxQueue_front][bCnt] = gbInnerPackTxDataBuf[bCnt];
//		gbInnerPackTxQueue_front = (gbInnerPackTxQueue_front + (MAX_INNER_PACK_TX_QUEUE_SIZE - 1)) % MAX_INNER_PACK_TX_QUEUE_SIZE;
//		gbInnerPackTxQueue_rear = (gbInnerPackTxQueue_rear + (MAX_INNER_PACK_TX_QUEUE_SIZE - 1)) % MAX_INNER_PACK_TX_QUEUE_SIZE;
//	}
//	else
//	{
//		for(bCnt = 0; bCnt < gbInnerPackTxByteNum; bCnt++)
//			gbInnerPackTxDataQueue[gbInnerPackTxQueue_front][bCnt] = gbInnerPackTxDataBuf[bCnt];
//		gbInnerPackTxQueue_front = (gbInnerPackTxQueue_front + (MAX_INNER_PACK_TX_QUEUE_SIZE - 1)) % MAX_INNER_PACK_TX_QUEUE_SIZE;
//	}
}



void InnerPackTx_DeQueueStart(void)	// Dequeue 시작(송신 준비)
{
	gbInnerPackTxQueue_sending = (gbInnerPackTxQueue_front + 1) % MAX_INNER_PACK_TX_QUEUE_SIZE;
	gbInnerPackTxByteNum_Queue = gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][3] + 5;	// Additional Data이외에 5바이트(Event, Source, Count, length, Checksum) 추가
}



void InnerPackTx_DeQueueEnd(void)	// Dequeue 마무리
{
	gbInnerPackTxQueue_front = (gbInnerPackTxQueue_front + 1) % MAX_INNER_PACK_TX_QUEUE_SIZE;
//	gbSendingWait[1] = true;
}

#if 0
__weak void PackTx_MakePacket(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength)
{
	BYTE bCnt;

	if(gbModuleMode)
	{
		gbPackTxDataBuf[PACK_F_EVENT] = bEvent;
		gbPackTxDataBuf[PACK_F_SOURCE] = bEventSrc;
		if(bEventSrc == ES_LOCK_EVENT)	(*bCount)++;			// Event 패킷일 때만 Count 증가
		gbPackTxDataBuf[PACK_F_COUNT] = (*bCount);
		gbPackTxDataBuf[PACK_F_LENGTH_ADDDATA] = bLength ;	// checksum(1바이트) 길이 추가 
		for(bCnt = 0; bCnt < bLength; bCnt++)
		{
			gbPackTxDataBuf[4 + bCnt] = *bpData;
			bpData++;
		}
		gbPackTxByteNum = 4 + bLength;
		PackTx_AddChecksum(gbPackTxDataBuf, &gbPackTxByteNum);

	//	if(gbPackTxEnqueueOrder == PACK_TX_ENQUEUE_FIRSTLY)
	//	{
	//		PackTx_EnQueueAtFirst();
	//	}
	//	else
	//	{
			if((!gfDirectSending) &&	!PackTx_QueueIsFull())	PackTx_EnQueue();
	//	}
	}

	if(gbInnerModuleMode)
	{
		gbInnerPackTxDataBuf[PACK_F_EVENT] = bEvent;
		gbInnerPackTxDataBuf[PACK_F_SOURCE] = bEventSrc;
		if(bEventSrc == ES_LOCK_EVENT)	(*bCount)++;			// Event 패킷일 때만 Count 증가
		gbInnerPackTxDataBuf[PACK_F_COUNT] = (*bCount);
		gbInnerPackTxDataBuf[PACK_F_LENGTH_ADDDATA] = bLength ; // checksum(1바이트) 길이 추가 
		for(bCnt = 0; bCnt < bLength; bCnt++)
		{
			gbInnerPackTxDataBuf[4 + bCnt] = *bpData;
			bpData++;
		}
		gbInnerPackTxByteNum = 4 + bLength;
		PackTx_AddChecksum(gbInnerPackTxDataBuf, &gbInnerPackTxByteNum);

	//	if(gbPackTxEnqueueOrder == PACK_TX_ENQUEUE_FIRSTLY)
	//	{
	//		PackTx_EnQueueAtFirst();
	//	}
	//	else
	//	{
			if((!gfDirectSending) &&	!InnerPackTx_QueueIsFull()) InnerPackTx_EnQueue();
	//	}
	}
	
}
#endif 

void InnerPackTx_MakePacketForInitial(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength)
{
	BYTE bCnt;

	gbInnerPackTxDataBuf[PACK_F_EVENT] = bEvent;
	gbInnerPackTxDataBuf[PACK_F_SOURCE] = bEventSrc;
	if(bEventSrc == ES_LOCK_EVENT)	(*bCount)++;			// Event 패킷일 때만 Count 증가
	gbInnerPackTxDataBuf[PACK_F_COUNT] = (*bCount);
	gbInnerPackTxDataBuf[PACK_F_LENGTH_ADDDATA] = bLength ;	// checksum(1바이트) 길이 추가 
	for(bCnt = 0; bCnt < bLength; bCnt++)
	{
		gbInnerPackTxDataBuf[4 + bCnt] = *bpData;
		bpData++;
	}
	gbInnerPackTxByteNum = 4 + bLength;
	InnerPackTx_AddChecksum(gbInnerPackTxDataBuf, &gbInnerPackTxByteNum);

//	if(gbInnerPackTxEnqueueOrder == INNER_PACK_TX_ENQUEUE_FIRSTLY)
//	{
//		InnerPackTx_EnQueueAtFirst();
//	}
//	else
//	{
		if((!gfInnerPackDirectSending) &&	!InnerPackTx_QueueIsFull())	InnerPackTx_EnQueue();
//	}
}



// 실행 시간이 2ms를 초과할 수 있으므로 메인 루프 타이밍을 깰 수 있기 때문에 오래 걸리는 명령에 대한 Ack송신시에만 사용할 것
void InnerPackTx_MakeAndSendPacketNow(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength)
{
	/* InnerPackTx_MakeAndSendPacketNow 한수는 현재 TI TWU 제어를 못하고 있다 */
	/* 꼭 바로 uart 를 사용 해야 해서 이함수를 사용할 경우 TI TWU 제어 부분 구현 할것 */
	/* 현재는 queue 에 무조건 쌓도록 하여 해당 항수를 호출 하는 부분을 없엠 */ 
	
	if(gbInnerModuleMode == 0)		return;

	gfInnerPackDirectSending = true; // Main Loop - PackTxDataProcess 기다리지 않고 직접 송신중
	PackTx_MakePacket(bEvent, bEventSrc, bCount, bpData, bLength);
	UartSendInnerPack(&gbInnerPackTxDataBuf[PACK_F_EVENT], gbInnerPackTxByteNum);
	while(1)			// 5바이트 패킷 송신시 2.7ms 소요(Debugger CycleCounter로 측정한 것)
	{
		if(!gfInnerPackDirectSending)		break;
#ifdef	DDL_CFG_WATCHDOG
		RefreshIwdg();
#endif
	}
}

void InnerPackDataTransmit(void)	//ST에서 사용 하는 함수 : void InnerPackDataTransmitProcess(void)
{
//	InnerPackTxEndProcess();
}



void InnerPackDataTransmitProcess(void)	// 1 Packet 송신시마다 호출됨
{
#if defined (P_COM_DDL_EN_T ) && defined (P_BT_WAKEUP_T) 
	if(!gfInnerPackDirectSending)		InnerPackTx_DeQueueEnd();		// PackConnectionMode 아니면  Tx Queue 처리 
	gfInnerPackDirectSending = false;
	gfInnerPackQueueSending = false;	// 송신 완료 표시 
#else 
	if(JigModeStatusCheck() == STATUS_SUCCESS)
	{
		__no_operation();
	}
	else
	{
	//	gbInnerPackTxByteCnt++;
		if(!gfInnerPackDirectSending)		InnerPackTx_DeQueueEnd();		// PackConnectionMode 아니면  Tx Queue 처리 
		gfInnerPackDirectSending = false;
//		gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
	}
	gfInnerPackQueueSending = false;	// 송신 완료 표시 
#endif 	
}



void InnerPackTxEnSigSetDefault(void)
{
	gInnerPackTxEnSigTl1us = INNER_PACK_TX_ENSIG_TLUS_DEFAULT;
	gInnerPackTxEnSigTl1ms = INNER_PACK_TX_ENSIG_TLMS_DEFAULT;
	gInnerPackTxEnSigTwu1us = INNER_PACK_TX_ENSIG_TWUUS_DEFAULT;
	gInnerPackTxEnSigTwu1ms = INNER_PACK_TX_ENSIG_TWUMS_DEFAULT;
}



void InnerPackTxEnSigTimeSet(BYTE* pTimeData)
{
	BYTE bTmp;

	BYTE TlTime;
	BYTE TlUnitDefine;
	BYTE TlMutiPlier;

	BYTE TwuTime;
	BYTE TwuUnitDefine;
	BYTE TwuMutiPlier;

	if(DataCompare(pTimeData, 0x00, 3) == 0)
	{
		InnerPackTxEnSigSetDefault();
		return;
	}

	if(DataCompare(pTimeData, 0xFF, 3) == 0)
	{
		InnerPackTxEnSigSetDefault();
		return;
	}

	bTmp = (pTimeData[2] & 0xF0) >> 4;
	TlUnitDefine = bTmp & 0x08;
	TlMutiPlier = bTmp & 0x07;

	TlTime = pTimeData[0]; 

	if(TlUnitDefine)
	{
		gInnerPackTxEnSigTl1us = 0;
		gInnerPackTxEnSigTl1ms = TlTime * TlMutiPlier;
	}
	else
	{
		gInnerPackTxEnSigTl1us = TlTime * TlMutiPlier;
		gInnerPackTxEnSigTl1ms = 0;
	}

	
	bTmp = (pTimeData[2] & 0x0F);
	TwuUnitDefine = bTmp & 0x08;
	TwuMutiPlier = bTmp & 0x07;

	TwuTime = pTimeData[1]; 

	if(TwuUnitDefine)
	{
		gInnerPackTxEnSigTwu1us = 0;
		gInnerPackTxEnSigTwu1ms = TwuTime * TwuMutiPlier;
	}
	else
	{
		gInnerPackTxEnSigTwu1us = TwuTime * TwuMutiPlier;
		gInnerPackTxEnSigTwu1ms = 0;
	}
}




#define	INNER_PACK_TX_ENSIG_MINIMUM_TIME		100		// Delay로 처리하는 최소 처리 단위 100ms


WORD InnerPackTxEnSigDelayProcess(WORD EnSigTxus, WORD EnSigTxms, BYTE CompensationValue)
{
	WORD EnSigTimer2ms = 0;

	if(EnSigTxus)
	{
		//CubeMx code를 사용하다보니 1usec 단위의 delay를 구현 못해서 10usec로 구현한 단위로 변환
		EnSigTxus = EnSigTxus/10;

		// 처리 시간 포함 등으로 인해 Delay가 맞지 않아 보정이 필요한 경우 10us 단위의 보정 값을 반영하여 계산
		if(EnSigTxus > (WORD)CompensationValue)
		{
			EnSigTxus -= (WORD)CompensationValue;
		}
		
		Delay(SYS_TIMER_10US*EnSigTxus);
	}
	else if(EnSigTxms < INNER_PACK_TX_ENSIG_MINIMUM_TIME)
	{
		//보조키 통합하면서 만든 Delay()의 최대 지연시간은 0.65535 sec, INNER_PACK_TX_ENSIG_MINIMUM_TIME 이 시간을 넘지 않도로 한다.
		Delay(SYS_TIMER_1MS*EnSigTxms);
	}
	else
	{
		// 1ms 정도는 무시 (INNER_PACK_TX_ENSIG_MINIMUM_TIME 이 100ms 정도로 큰 경우에 해당)
/*
		if(EnSigTxms & 0x01)
		{
			// 홀수일 경우 1ms를 Delay 함수로 처리
			Delay(SYS_TIMER_1MS);
			EnSigTxms--;
		}		
*/
		// Delay 최소 시간 보다 클 경우에는 Time Count 처리로 수행
		EnSigTimer2ms = (EnSigTxms >> 1);
	}

	return (EnSigTimer2ms);
}



void InnerPackTxProcessStepStart(WORD AckWaitTime)
{
//	BYTE bCnt;
//	BYTE bTmp;
//	
//	bTmp = 0;
//	for(bCnt = 0; bCnt < gbInnerPackTxByteNum; bCnt++)
//	{
//		bTmp ^= gbInnerPackTxDataBuf[bCnt];
//	}
//	gbInnerPackTxDataBuf[gbInnerPackTxByteNum] = bTmp;
//
//	gbInnerPackTxByteNum++;
//	gbInnerPackTxByteCnt = 0;
//
//	gInnerPackTxProcessStep = INNER_PACK_TX_PACKET_SENDING_MODE;
//	gInnerPackTxAckWaitTimer10ms = AckWaitTime;
}



void InnerPackTxProcessStepClear(void)
{
//	gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE; 
}

void InnerPackTxProcessTimeCounter(void)
{
	if(gInnerPackTxEnSigTimer2ms) 	--gInnerPackTxEnSigTimer2ms;
}


void InnerPackTxWaitTimeCounter(void)
{
	if(gInnerPackTxWaitTimer10ms)		--gInnerPackTxWaitTimer10ms;
}



void InnerPackTxAckWaitTimeCounter(void)
{
	if(gInnerPackTxAckWaitTimer10ms)		--gInnerPackTxAckWaitTimer10ms;
}


void InnerPackTxAckClearCheck(BYTE* bpData)
{
	BYTE i;
	BYTE Temp = 0x00;

	if(gbInnerModuleProtocolVersion >= 0x30)
	{
		for(i = 0x00 ; i < ACK_WAIT_BUFFER_SIZE ; i++)
		{
			if(gbInnerAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] == 0x00)
			{
				//timer 가 0 이면 buffer 비어 있는것 
				Temp++;
				continue;
			}
			else 
			{
				if(gbInnerAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX] == bpData[0] && ES_MODULE_ACK == bpData[1] &&
					gbInnerAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+2] == bpData[2])
				{
					//ack 왔으니 timer , count clear
					Temp++;
					gbInnerAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] = gbInnerAckWaitBuffer[i][ACK_WAIT_COUNT_INDEX] = 0x00;
				}
			}
		}

		if(Temp == ACK_WAIT_BUFFER_SIZE)
		{
			// buffer 가 다 비었음 sleep 모드로 
			gInnerPackTxAckWaitTimer100ms = 0x00;
		}
	}
}

void InnerPackTxAckWaitCallback(void)
{
	BYTE i;

	if(gbInnerModuleProtocolVersion >= 0x30)
	{
		if(gInnerPackTxAckWaitTimer100ms)
		{
			--gInnerPackTxAckWaitTimer100ms; // 이 timer 는 늘 최신으로 갱신 된다 

			for(i = 0x00 ; i < ACK_WAIT_BUFFER_SIZE ; i++)
			{
				if(gbInnerAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] == 0x00)
				{
					//timer 가 0 이면 buffer 비어 있는것
					continue;
				}
				else
				{
					// timer 감소 
					gbInnerAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX]--;

					if(gbInnerAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] == 0x00)
					{
						if(gbInnerAckWaitBuffer[i][ACK_WAIT_COUNT_INDEX])
						{
							//timer 가 만료 되었고 , count 가 남아 있으면 tx queue 에 넣는다 
							BYTE bCnt;

							gbInnerAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] = 0xFF;

							// PackTx_EnQueue() 을 쓰면 for 을 두번 돌아야 함 
							gbInnerPackTxQueue_rear = (gbInnerPackTxQueue_rear + 1) % MAX_PACK_TX_QUEUE_SIZE;
							for(bCnt = 0; bCnt < gbInnerAckWaitBuffer[i][ACK_WAIT_DATA_LENGTH_INDEX] ; bCnt++)
							{
								gbInnerPackTxDataQueue[gbInnerPackTxQueue_rear][bCnt] = gbInnerAckWaitBuffer[i][bCnt+3];
							}
							gbInnerPackTxWaitTimeQueue[gbInnerPackTxQueue_rear] = 0;
						}
						else
						{		
							// timer 도 끝났고 ack 도 못받은 경우 
#ifdef	_NEW_YALE_ACCESS_UI
							if((gbInnerAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE)
								|| (gbInnerAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
								|| (gbInnerAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+PACK_F_EVENT] == F0_RFM_CHECK))
#else	// _NEW_YALE_ACCESS_UI
							if(gbInnerAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE ||
	 							gbInnerAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX+PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
#endif	// _NEW_YALE_ACCESS_UI
							{
								// F0_ENCRYPTION_KEY_CHANGE  F0_ENCRYPTION_GET_CHECKSUM 를 최종 실패 하면 
								// susped mode 
								gbInnerpackModeSuspend = 0x01;
								gbInnerModuleMode = PACK_ID_NOT_CONFIRMED; 						
							}
						}
					}
				}
			}		
		}
	}
	else
	{
		gInnerPackTxAckWaitTimer100ms = 0; 
	}
}

BYTE InnerPackTxAckCheck(BYTE* bpData)
{
	BYTE i;

	for(i = 0x00 ; i < ACK_WAIT_BUFFER_SIZE ; i++)
	{
		if(gbInnerAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] == 0x00)
		{
			//timer 가 0 이면 buffer 비어 있는것
			continue;
		}
		else
		{
			if(memcmp(&gbInnerAckWaitBuffer[i][ACK_WAIT_DATA_START_INDEX],bpData,3) == 0x00)
			{
				// buffer 에 보냈던 것이 있으니 count , timer 갱신 
				gbInnerAckWaitBuffer[i][ACK_WAIT_TIMER_INDEX] = gInnerPackTxAckWaitTimer100ms;
				gbInnerAckWaitBuffer[i][ACK_WAIT_COUNT_INDEX]--; // = gbInnerAckWaitBuffer[i][ACK_WAIT_COUNT_INDEX] - 0x01;
				return 0xFF;
			}
		}
	}
	return 0x00;
}



void InnerPackTxEndProcess(void)
{
//	if(gInnerPackTxAckWaitTimer10ms)
//	{
//		gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
//	}
//	else
//	{
//		gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
//	}
}





void InnerPackTx_EnSigSendingStart(void)
{
	P_BT_EN(0);
	gInnerPackTxEnSigTimer2ms = InnerPackTxEnSigDelayProcess(gInnerPackTxEnSigTl1us, gInnerPackTxEnSigTl1ms, 0);
	if(gInnerPackTxEnSigTimer2ms)	// Ti가 2ms 이상이면 
	{
		gInnerPackTxProcessStep = INNER_PACK_TX_EN_SIG_TI_SENDING_MODE;
	}
	else						// Ti가 2ms 미만이면 
	{
		P_BT_EN(1);
		// us 단위에서는 Uart 시작까지 40us 처리 시간이 소요되어 보정 값 사용
		gInnerPackTxEnSigTimer2ms = InnerPackTxEnSigDelayProcess(gInnerPackTxEnSigTwu1us, gInnerPackTxEnSigTwu1ms, 4);
		if(gInnerPackTxEnSigTimer2ms)	// Twu가 2ms 이상이면 
		{
			gInnerPackTxProcessStep = INNER_PACK_TX_EN_SIG_TWU_SENDING_MODE;
		}
		else						// Twu가 2ms 미만이면 
		{
			gInnerPackTxAckWaitTimer10ms = 10;

#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(gbJigTimer1s && (gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][0] == 0x02))
			{
				UartSendInnerPack(&gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT], gbInnerPackTxByteNum_Queue);
				gfInnerPackQueueSending = true; 	// 송신 진행중 표시
				gInnerPackTxProcessStep = INNER_PACK_TX_PACKET_SENDING_MODE;
				return;		
			}
#endif
			if(gbInnerModuleProtocolVersion >= 0x30 && gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][0] != 0x44 && gbInnerEncryptionType == 0x01)
			{
				UartSendInnerPack(gbInnerPackTxEnDataSendbuffer, gbInnerPackTxEnDataByteNum);
			}
			else
			{
				UartSendInnerPack(&gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT], gbInnerPackTxByteNum_Queue);
			}
	
			gfInnerPackQueueSending = true;		// 송신 진행중 표시
			gInnerPackTxProcessStep = INNER_PACK_TX_PACKET_SENDING_MODE;
		}
	}
}




void InnerPackTxDataProcess(void)
{
	switch(gInnerPackTxProcessStep)
	{
		case INNER_PACK_TX_WAITING_MODE:	// TX 대기중
			// TX QUEUE에 패킷 준비되면  TX 패킷 확인중으로 이동
			if((!InnerPackTx_QueueIsEmpty()) && (!gfInnerPackAckWait))
			{
				InnerPackTx_DeQueueStart();

#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
				if(gbJigTimer1s && (gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][0] == 0x02))
				{
					gInnerPackTxWaitTimer10ms = gbInnerPackTxWaitTimeQueue[gbInnerPackTxQueue_sending]; 
					gInnerPackTxProcessStep = INNER_PACK_TX_EN_SIG_TI_SENDING_WAIT;
					break;
				}
#endif

				if(gbInnerModuleProtocolVersion >= 0x30 && gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][0] != 0x44 && gbInnerEncryptionType == 0x01) //AES128 ECB
				{
					gInnerPackTxProcessStep = INNER_PACK_TX_ENCRYPT_MODE;
				}	
				else
				{
					gInnerPackTxProcessStep = INNER_PACK_TX_PACKET_CHECKING_MODE;
				}				
			}
			break;


		case INNER_PACK_TX_PACKET_CHECKING_MODE:	// TX 패킷 확인중 
			// 보낼 패킷이 명령이면 RX 대기중이 되면 EN 신호 발생 시작 후 TX EN 신호 발생중으로 이동
			if(gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][1] == ES_LOCK_EVENT)
			{
				if(gInnerPackRxProcessStep == PACK_RX_WAITING_MODE)
				{
//					InnerPackTx_EnSigSendingStart();

					gInnerPackTxWaitTimer10ms = gbInnerPackTxWaitTimeQueue[gbInnerPackTxQueue_sending]; 
					gInnerPackTxProcessStep = INNER_PACK_TX_EN_SIG_TI_SENDING_WAIT;
				}
			}
			// 보낼 패킷이 ACK이면  EN 신호 발생 시작 후 TX EN 신호 송신중으로 이동
			else
			{
//				InnerPackTx_EnSigSendingStart();

				gInnerPackTxWaitTimer10ms = gbInnerPackTxWaitTimeQueue[gbInnerPackTxQueue_sending]; 
				gInnerPackTxProcessStep = INNER_PACK_TX_EN_SIG_TI_SENDING_WAIT;
			}
			break;

		case INNER_PACK_TX_EN_SIG_TI_SENDING_WAIT:
			if(gInnerPackTxWaitTimer10ms)	break;

			InnerPackTx_EnSigSendingStart();
			break;

		case INNER_PACK_TX_EN_SIG_TI_SENDING_MODE:		// TX EN 신호 Ti 송신중
			// Pack Wakeup 신호(Ti) 완료이면 TX EN 신호 Twu 송신중으로 이동
			if(gInnerPackTxEnSigTimer2ms)	break;

			P_BT_EN(1);
			// Ti 가 100ms 넘어갈 경우 Twu 를 위해 다시 timming 계산 
			gInnerPackTxEnSigTimer2ms = InnerPackTxEnSigDelayProcess(gInnerPackTxEnSigTwu1us, gInnerPackTxEnSigTwu1ms, 4);
			gInnerPackTxProcessStep = INNER_PACK_TX_EN_SIG_TWU_SENDING_MODE;
			break;


		case INNER_PACK_TX_EN_SIG_TWU_SENDING_MODE:		// TX EN 신호 Twu 송신중
			// Pack Wakeup 신호(Twu) 완료이면 패킷 첫바이트 송신 후 TX 패킷 송신중으로 이동
			if(gInnerPackTxEnSigTimer2ms)	break;

			gInnerPackTxAckWaitTimer10ms = 10;

#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
			if(gbJigTimer1s && (gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][0] == 0x02))
			{
				UartSendInnerPack(&gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT], gbInnerPackTxByteNum_Queue);
				gfInnerPackQueueSending = true; 	// 송신 진행중 표시
				gInnerPackTxProcessStep = INNER_PACK_TX_PACKET_SENDING_MODE;
				break;
			}
#endif

			if(gbInnerModuleProtocolVersion >= 0x30 && gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][0] != 0x44 && gbInnerEncryptionType == 0x01) //AES128 ECB
			{
				UartSendInnerPack(gbInnerPackTxEnDataSendbuffer, gbInnerPackTxEnDataByteNum);
			}	
			else
			{
				UartSendInnerPack(&gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT], gbInnerPackTxByteNum_Queue);
			}				

			gfInnerPackQueueSending = true;		// 송신 진행중 표시
			gInnerPackTxProcessStep = INNER_PACK_TX_PACKET_SENDING_MODE;
			break;

#if 1 // retry 
		case INNER_PACK_TX_PACKET_SENDING_MODE:	
			// TX send 중 
			// TX send 가 gInnerPackTxAckWaitTimer10ms 0 되기 전에 완료시 gfQueueSending true -> false 보 변경됨 
			// Tx send 가 완료가 되면 Lock event 인지 판단 lock event 면 ack wait 상태로 천이 
			
			if(gInnerPackTxAckWaitTimer10ms == 0)	{
				// Tx complete 가 안된 경우 
				// 순전히 uart 문제가 될듯 uart 를 초기화 하는게 맞나 ? 
				gfInnerPackQueueSending = false;
				gbInnerPackRetryCount = 0;
				gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
				break;
			}
#if 1
			if(gfInnerPackQueueSending == false)
			{
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
				if(gbJigTimer1s && (gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][0] == 0x02))
				{
					gfInnerPackAckWait = false;
					gbInnerPackRetryCount = 0;
					gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
					break;
				}
#endif
				
				// 0x44 는 retry 처리를 따로 하므로 제외 
				// ES_LOCK_EVENT 만 처리 
				// smart living 2.0 이상만 retry 처리 
				if(gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_SOURCE] == ES_LOCK_EVENT &&
					gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] != 0x44 &&
					(gbInnerModuleProtocolVersion >= 0x30))
				{
					// 내가 보낸 event 인지 gbAckCompleteData 저장 
					// 5초 간격으로 2회 retry  
					gfInnerPackAckWait = true;	

#ifdef	_NEW_YALE_ACCESS_UI
					if((gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE) 
						|| (gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
						|| (gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_RFM_CHECK))
#else	// _NEW_YALE_ACCESS_UI
					if(gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE ||
						gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
#endif	// _NEW_YALE_ACCESS_UI
					{
						// F0_ENCRYPTION_KEY_CHANGE  F0_ENCRYPTION_GET_CHECKSUM 는 
						// 500ms
						gInnerPackTxAckWaitTimer100ms = 5; 
					}					
					else 
					{
						gInnerPackTxAckWaitTimer100ms = 50;
					}

					if(InnerPackTxAckCheck(&gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][0]) == 0x00)
					{
						//buffer 에없는 새로운 packet 이면 buffer 에 넣고 간다 
						gbInnerAckWaitBuffer[gInnerPackTxAckWaitIndex][ACK_WAIT_TIMER_INDEX] = gInnerPackTxAckWaitTimer100ms; //timer 
						gbInnerAckWaitBuffer[gInnerPackTxAckWaitIndex][ACK_WAIT_COUNT_INDEX] = 2; //retry count 
						gbInnerAckWaitBuffer[gInnerPackTxAckWaitIndex][ACK_WAIT_DATA_LENGTH_INDEX] = gbInnerPackTxByteNum_Queue; //length
						memcpy(&gbInnerAckWaitBuffer[gInnerPackTxAckWaitIndex][ACK_WAIT_DATA_START_INDEX],&gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][0],gbInnerPackTxByteNum_Queue);

						// 사실 비어 있는 index 를 찾아도 되지만... 
						gInnerPackTxAckWaitIndex++;
						if(gInnerPackTxAckWaitIndex > ACK_WAIT_BUFFER_SIZE)
						{
							gInnerPackTxAckWaitIndex = 0x00;
						}
					}
					gfInnerPackAckWait = false;
					gbInnerPackRetryCount = 0;
					gInnerPackTxProcessStep = PACK_TX_WAITING_MODE;
				}
				else														// 송신 내용이 응답(Ack, Nack)이면 
				{
					gfInnerPackAckWait = false;
					gbInnerPackRetryCount = 0;
					gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
				}
			}
#else 
			if(gfInnerPackQueueSending == false)
			{
				// 0x44 는 retry 처리를 따로 하므로 제외 
				// ES_LOCK_EVENT 만 처리 
				// smart living 2.0 이상만 retry 처리 
				if(gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_SOURCE] == ES_LOCK_EVENT &&
					gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] != 0x44 &&
					(gbInnerModuleProtocolVersion >= 0x30))
				{
					// 내가 보낸 event 인지 gbAckCompleteData 저장 
					// 5초 간격으로 2회 retry  
					gfInnerPackAckWait = true;	

					if(gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE ||
 						gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
					{
						// F0_ENCRYPTION_KEY_CHANGE  F0_ENCRYPTION_GET_CHECKSUM 는 
						// 500ms
						gInnerPackTxAckWaitTimer10ms = 50; 
					}					
					else 
					{
						gInnerPackTxAckWaitTimer10ms = 500; 
					}
					gbInnerPackAckCompleteData[PACK_F_EVENT] = gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT];
					gbInnerPackAckCompleteData[PACK_F_SOURCE] = ES_MODULE_ACK;
					gInnerPackTxProcessStep = INNER_PACK_TX_ACK_WAITING_MODE;
				}
				else														// 송신 내용이 응답(Ack, Nack)이면 
				{
					gfInnerPackAckWait = false;
					gbInnerPackRetryCount = 0;
					gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
				}
			}
#endif 			
			break;

		case INNER_PACK_TX_ACK_WAITING_MODE:		// 응답(Ack, Nack) 대기중
			// ACK 수신완료이거나 TX TIME OUT이면 재전송 
			if(gInnerPackTxAckWaitTimer10ms == 0){

				if(gbInnerPackRetryCount > 1)
				{
					gbInnerPackRetryCount = 0;	// 2번 retry 완료 

#ifdef	_NEW_YALE_ACCESS_UI
					if((gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE)
						|| (gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
						|| (gbInnerAckWaitBuffer[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_RFM_CHECK))
#else	// _NEW_YALE_ACCESS_UI
					if(gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_KEY_CHANGE ||
 						gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT] == F0_ENCRYPTION_GET_CHECKSUM)
#endif	// _NEW_YALE_ACCESS_UI
					{
						// F0_ENCRYPTION_KEY_CHANGE  F0_ENCRYPTION_GET_CHECKSUM 를 최종 실패 하면 
						// susped mode 
						gbInnerpackModeSuspend = 0x01;
						gbInnerModuleMode = PACK_ID_NOT_CONFIRMED;
					}
				}
				else
				{
					// 1 회 2회 보내는 경우 
					gbInnerPackRetryCount++;
					gbWakeUpMinTime10ms = 2; //sleep 에 안들어가게 하기 위해 

					// 여기 까지 왔으면 이미 dequeue 상태					
					// queue 를 재 정리 하는것 보다 보낸 tx index 를 다시 조정 
					if(gbInnerPackTxQueue_front)
					{
						gbInnerPackTxQueue_front--;
					}
					else
					{
						gbInnerPackTxQueue_front = MAX_PACK_TX_QUEUE_SIZE;
					}
				}
				// index 를 조정 했으니 PACK_TX_WAITING_MODE 보내서 다시 send 하도록 
				gfInnerPackAckWait = false;
				gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
				break;
			}
			
			if(gfInnerPackAckWait == false)	
			{
				// 보낸 event 의 ack 를 성공 적으로 받으면 다음 frame 보낼 수 있도록 
				gInnerPackTxAckWaitTimer10ms = 0;
				gbInnerPackRetryCount = 0;
				gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
			}
			break;

		case INNER_PACK_TX_ENCRYPT_MODE:	
			InnerPackTxEncapsulateData(&gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_EVENT],gbInnerPackTxByteNum_Queue);
			gInnerPackTxProcessStep = INNER_PACK_TX_PACKET_CHECKING_MODE;
			break;			
#else 
		case INNER_PACK_TX_PACKET_SENDING_MODE:	// TX 패킷 송신중
			// TX 패킷 송신완료이고  보낸 패킷이 명령이면 ACK 수신대기중으로 이동
			// TX 패킷 송신완료이고  보낸 패킷이 ACK이면 TX 대기중으로 이동
			if(gInnerPackTxAckWaitTimer10ms == 0)	gfInnerPackQueueSending = false;
			if(gfInnerPackQueueSending == false)
			{
				if(gbInnerPackTxDataQueue[gbInnerPackTxQueue_sending][PACK_F_SOURCE] == ES_LOCK_EVENT)
				{
					gfInnerPackAckWait = true;	// 응답 대기중 표시
					gInnerPackTxAckWaitTimer10ms = 50;
					gInnerPackTxProcessStep = INNER_PACK_TX_ACK_WAITING_MODE;
				}
				else														// 송신 내용이 응답(Ack, Nack)이면 
				{
					gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
				}
			}
			break;


		case INNER_PACK_TX_ACK_WAITING_MODE:		// 응답(Ack, Nack) 대기중
			// ACK 수신완료이거나 TX TIME OUT이면 TX 대기중으로 이동
			if(gInnerPackTxAckWaitTimer10ms == 0)	gfInnerPackAckWait = false;
			if(gfInnerPackAckWait == false)	gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
			
			break;
#endif 
		default:
			gInnerPackTxProcessStep = INNER_PACK_TX_WAITING_MODE;
			break;
	}
}



static WORD gSlotNumberForPack = 0;

#if 1
__weak void CopySlotNumberForPack(WORD SlotNumber)
{
	gSlotNumberForPack = SlotNumber;
}


__weak WORD GetSlotNumberForPack(void)
{
	return (gSlotNumberForPack);
}



static BYTE gCredentialDataForPackBuf[16];

__weak void CopyCredentialDataForPack(BYTE* bDataBuf, BYTE bNum)
{
	BYTE bCnt;

	if(bNum > 16)	return;

	for(bCnt = 0; bCnt < bNum; bCnt++)
	{
		gCredentialDataForPackBuf[bCnt] = bDataBuf[bCnt];
	}
}


__weak void GetCredentialDataForPack(BYTE* bDataBuf, BYTE bNum)
{
	BYTE bCnt;

	if(bNum > 16)	return;

	for(bCnt = 0; bCnt < bNum; bCnt++)
	{
		bDataBuf[bCnt] = gCredentialDataForPackBuf[bCnt];
	}
}



__weak void PackTxEventPincodeAdded(WORD wUserSlot)
{
	BYTE i, bTmpArray[MAX_PIN_LENGTH+3];
	BYTE bBcdArray[8], bAsciiArray[16];
	BYTE tmpSize = 0;
	BYTE tmpEvent = 0;

#if defined (_USE_LOGGING_MODE_)
	bTmpArray[0] = F0_USER_ADDED;
	bTmpArray[1] = 0x00;
	bTmpArray[2] = ((wUserSlot >> 8) & 0xFF);
	bTmpArray[3] = (uint8_t)wUserSlot;
	SaveLog(bTmpArray,4);
#endif

	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbModuleProtocolVersion >= 0x30)
		{
			if(wUserSlot == RET_MASTER_CODE_MATCH)
			{
				// 확인 필요
				wUserSlot = RET_2BYTE_MASTER_CODE_MATCH;
			}
			else if(wUserSlot == RET_TEMP_CODE_MATCH)
			{
				// 확인 필요
				wUserSlot = 0xFFF0;
			}

			bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
			bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
			bTmpArray[2] = USER_STATUS_OCC_ENABLED;
			GetCredentialDataForPack(bBcdArray, 8);

			BcdToAscii(bBcdArray, bAsciiArray);
			for(i = 0; i < MAX_PIN_LENGTH; i++)
			{
				if(bAsciiArray[i] == 0xFF)	break;
				bTmpArray[i + 3] = bAsciiArray[i];
			}
			tmpSize = i+3;
			tmpEvent = F0_USER_ADDED_EX;
		}
		else 
		{
			bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
			bTmpArray[1] = USER_STATUS_OCC_ENABLED;
			GetCredentialDataForPack(bBcdArray, 8);

			BcdToAscii(bBcdArray, bAsciiArray);
			for(i = 0; i < MAX_PIN_LENGTH; i++)
			{
				if(bAsciiArray[i] == 0xFF)	break;
				bTmpArray[i + 2] = bAsciiArray[i];
			}
			tmpSize = i+2;
			tmpEvent = F0_USER_ADDED;
		}
		PackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbComCnt, bTmpArray,tmpSize); 	
	}

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbInnerModuleProtocolVersion >= 0x30)
		{
			if(wUserSlot == RET_MASTER_CODE_MATCH)
			{
				// 확인 필요
				wUserSlot = RET_2BYTE_MASTER_CODE_MATCH;
			}
			else if(wUserSlot == RET_TEMP_CODE_MATCH)
			{
				// 확인 필요
				wUserSlot = 0xFFF0;
			}

			bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
			bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
			bTmpArray[2] = USER_STATUS_OCC_ENABLED;
			GetCredentialDataForPack(bBcdArray, 8);

			BcdToAscii(bBcdArray, bAsciiArray);
			for(i = 0; i < MAX_PIN_LENGTH; i++)
			{
				if(bAsciiArray[i] == 0xFF)	break;
				bTmpArray[i + 3] = bAsciiArray[i];
			}
			tmpSize = i+3;
			tmpEvent = F0_USER_ADDED_EX;
		}
		else 
		{
			bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
			bTmpArray[1] = USER_STATUS_OCC_ENABLED;
			GetCredentialDataForPack(bBcdArray, 8);

			BcdToAscii(bBcdArray, bAsciiArray);
			for(i = 0; i < MAX_PIN_LENGTH; i++)
			{
				if(bAsciiArray[i] == 0xFF)	break;
				bTmpArray[i + 2] = bAsciiArray[i];
			}
			tmpSize = i+2;
			tmpEvent = F0_USER_ADDED;
		}
		InnerPackTx_MakePacketSinglePort(F0_USER_ADDED, ES_LOCK_EVENT, &gbInnerComCnt, bTmpArray,tmpSize);		
	}
}





__weak void PackTxEventPincodeDeleted(WORD wUserSlot)
{
	BYTE bTmpArray[4];
	BYTE tmpSize = 0;
	BYTE tmpEvent = 0;	

#if defined (_USE_LOGGING_MODE_)
	bTmpArray[0] = F0_USER_DELETED;
	bTmpArray[1] = 0x00;
	bTmpArray[2] = ((wUserSlot >> 8) & 0xFF);
	bTmpArray[3] = (uint8_t)wUserSlot;
	SaveLog(bTmpArray,4);
#endif

	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbModuleProtocolVersion >= 0x30)
		{
			bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
			bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
			tmpSize = 2;
			tmpEvent = F0_USER_DELETED_EX;
		}
		else 
		{
			bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
			tmpSize = 1;
			tmpEvent = F0_USER_DELETED; 		
		}
		PackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbComCnt, bTmpArray,tmpSize); 	
	}

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbInnerModuleProtocolVersion >= 0x30)
		{
			bTmpArray[0] = (BYTE)((wUserSlot>>8) & 0x00ff);
			bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
			tmpSize = 2;
			tmpEvent = F0_USER_DELETED_EX;
		}
		else 
		{
			bTmpArray[0] = (BYTE)(wUserSlot & 0x00ff);
			tmpSize = 1;
			tmpEvent = F0_USER_DELETED;
		}
		InnerPackTx_MakePacketSinglePort(F0_USER_DELETED_EX, ES_LOCK_EVENT, &gbInnerComCnt, bTmpArray,tmpSize);		
	}
}

__weak void PackTxEventCredentialAdded(BYTE CredentialType, WORD wUserSlot)
{
	BYTE bTmpArray[12];
	BYTE tmpSize = 0;

#if defined (_USE_LOGGING_MODE_)
	bTmpArray[0] = EV_USER_CARD_ADDED;
	bTmpArray[1] = CredentialType;
	bTmpArray[2] = ((wUserSlot >> 8) & 0xFF);
	bTmpArray[3] = (uint8_t)wUserSlot;
	SaveLog(bTmpArray,4);
#endif

	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		bTmpArray[0] = CredentialType;
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		bTmpArray[2] = USER_STATUS_OCC_ENABLED;
		if(wUserSlot == 0xFF)
		{
			memset(&bTmpArray[3], 0xFF, 8);
		}
		else
		{
			GetCredentialDataForPack(&bTmpArray[3], 8);
		}
		tmpSize = 11;
		PackTx_MakePacketSinglePort(EV_USER_CARD_ADDED, ES_LOCK_EVENT, &gbComCnt, bTmpArray,tmpSize);
	}

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		bTmpArray[0] = CredentialType;
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		bTmpArray[2] = USER_STATUS_OCC_ENABLED;
		if(wUserSlot == 0xFF)
		{
			memset(&bTmpArray[3], 0xFF, 8);
		}
		else
		{
			GetCredentialDataForPack(&bTmpArray[3], 8);
		}
		InnerPackTx_MakePacketSinglePort(EV_USER_CARD_ADDED, ES_LOCK_EVENT, &gbInnerComCnt, bTmpArray,tmpSize);
	}
}



__weak void PackTxEventCredentialDeleted(BYTE CredentialType, WORD wUserSlot)
{
	BYTE bTmpArray[4];
	BYTE tmpSize = 0;

#if defined (_USE_LOGGING_MODE_)
	bTmpArray[0] = EV_USER_CARD_DELETED;
	bTmpArray[1] = CredentialType;
	bTmpArray[2] = ((wUserSlot >> 8) & 0xFF);
	bTmpArray[3] = (uint8_t)wUserSlot;
	SaveLog(bTmpArray,4);
#endif 

	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		bTmpArray[0] = CredentialType;
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		tmpSize = 2;
		PackTx_MakePacketSinglePort(EV_USER_CARD_DELETED, ES_LOCK_EVENT, &gbComCnt, bTmpArray,tmpSize);		
	}

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		bTmpArray[0] = CredentialType;
		bTmpArray[1] = (BYTE)(wUserSlot & 0x00ff);
		tmpSize = 2;
		InnerPackTx_MakePacketSinglePort(EV_USER_CARD_DELETED, ES_LOCK_EVENT, &gbInnerComCnt, bTmpArray,tmpSize);		
	}
}

__weak void PackAlarmReportInnerForcedLockOpenCloseFailSend(void) //hyojoon.kim 
{
	BYTE bdata = 0x00;
	
	/* InnerPackAlarmReportInnerForcedLockOpenCloseFailSend 는 FeedbackLockOut 이후에 만 불리니 gbMotorSensorStatus 로 
	열림 닫힘만 체크, 여기 왔다는 얘긴 내부 강제 상태 인데 open 이나 close 하려 하는 것임  */
	
	switch (MotorSensorCheck()) 
	{
		case SENSOR_CLOSELOCK_STATE : //모터 닫힘 상태 에서 열려고 함 
			bdata = 0x02; 
			break;

		case SENSOR_OPENLOCK_STATE : 
		case SENSOR_CENTERLOCK_STATE:
		case SENSOR_LOCK_STATE:
			bdata = 0x01;  			//모터 열림 상태 에서 닫으려고 함 

		default : 
			break;
	}

	if(bdata)
	{
		PackTx_MakeAlarmPacket(AL_UNLOCK_FAIL_BY_FORCED_LOCKED, 0x00, bdata);
	}
}

__weak void PackTx_MakeAlarmPacket(BYTE bAlarmType, BYTE bAlarmLevel_H, BYTE bAlarmLevel_L)		// Alarm Event Packet 준비
{
	BYTE		tmpArray[4] = {0,0,0,0};
	BYTE		tmpSize = 0;
	BYTE		tmpEvent = 0x00;

#if defined (_USE_LOGGING_MODE_)
	tmpArray[0] = F0_ALARM_REPORT;
	tmpArray[1] = bAlarmType;
	tmpArray[2] = bAlarmLevel_H; 
	tmpArray[3] = bAlarmLevel_L;
	SaveLog(tmpArray,4);
#endif 

	if(gbModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbModuleProtocolVersion >= 0x30)
		{
			tmpArray[0] = bAlarmType;
			tmpArray[1] = bAlarmLevel_H;
			tmpArray[2] = bAlarmLevel_L;
			tmpSize = 3;
			tmpEvent = F0_ALARM_REPORT_EX;
		}
		else 
		{
			tmpArray[0] = bAlarmType;
			tmpArray[1] = bAlarmLevel_L;
			tmpSize = 2;
			tmpEvent = F0_ALARM_REPORT;
		}
		PackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray,tmpSize);		
	}

	if(gbInnerModuleMode == PACK_ID_CONFIRMED)
	{
		if(gbInnerModuleProtocolVersion >= 0x30)
		{
			tmpArray[0] = bAlarmType;
			tmpArray[1] = bAlarmLevel_H;
			tmpArray[2] = bAlarmLevel_L;
			tmpSize = 3;
			tmpEvent = F0_ALARM_REPORT_EX;
		}
		else 
		{
			tmpArray[0] = bAlarmType;
			tmpArray[1] = bAlarmLevel_L;
			tmpSize = 2;
			tmpEvent = F0_ALARM_REPORT;
		}
		InnerPackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray,tmpSize); 	
	}
}

#endif 


//------------------------------------------------------------------------------------
// 저전압 경고 신호 전송
//	문 열림/닫힘 신호와 충돌하지 않도록 저전압일 때 문 열림/닫힘 후 5초 후에 전송

 __weak void PackAlarmReportLowBatterySet(void)
{
	gbPackLowBatteryReportMode = 1;
	gbPackLowBatteryReportTimer1s = 5;
}




 __weak void PackAlarmReportLowBatteryCounter(void)
{
	if(gbPackLowBatteryReportTimer1s)	--gbPackLowBatteryReportTimer1s;
}


 __weak void PackTxAlarmReportLowBatteryProcess(void)
{
	switch(gbPackLowBatteryReportMode)
	{
		case 1:
			if(gbPackLowBatteryReportTimer1s)	break;

			if(GetPackTxProcessStep())		break;
			if(GetPackRxProcessStep())		break;
			if(GetInnerPackTxProcessStep()) 	break;			
			if(GetInnerPackRxProcessStep()) 	break;

			PackTx_MakeAlarmPacket(AL_LOW_BATTERY, 0x00, 0x01);
			gbPackLowBatteryReportMode = 0;
			break;

		default:
			break;
	}			
}

__weak void ZWaveZigbeeLockStatusAlarmRepot(void)
{
	BYTE		tmpArray[3] = {0,0,0};
	BYTE		tmpSize = 0;
	BYTE 	tmpEvent = 0x00;
		
	//gbPackLockStatusReportTimer 는 stop mode 에서 빠져 나올때 한번 증가 한다.
	if(((gbModuleMode ==PACK_ID_CONFIRMED) && gfPackTypeZBZWModule) ||
				((gbInnerModuleMode ==PACK_ID_CONFIRMED) && gfInnerPackTypeZBZWModule))
	{
		if(gbPackLockStatusReportTimer++ >= 7200)	//3시간
		{
			gbPackLockStatusReportTimer = 0;
			gbWakeUpMinTime10ms = 50;	
			
			if((gbModuleMode ==PACK_ID_CONFIRMED) && gfPackTypeZBZWModule)
			{
				if(gbModuleProtocolVersion >= 0x30)
				{
					tmpArray[0] = AL_LOCK_STATUS_REPORT;
					tmpArray[1] = 0x00;
					tmpArray[2] = LockStsCheck();
					tmpSize = 3;
					tmpEvent = F0_ALARM_REPORT_EX;
				}
				else
				{
					tmpArray[0] = AL_LOCK_STATUS_REPORT;
					tmpArray[1] = LockStsCheck();
					tmpSize = 2;
					tmpEvent = F0_ALARM_REPORT;
				}
				PackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbComCnt, tmpArray, tmpSize);
			}
			else
			{
				if(gbInnerModuleProtocolVersion >= 0x30)
				{
					tmpArray[0] = AL_LOCK_STATUS_REPORT;
					tmpArray[1] = 0x00;
					tmpArray[2] = LockStsCheck();
					tmpSize = 3;
					tmpEvent = F0_ALARM_REPORT_EX;
				}
				else
				{
					tmpArray[0] = AL_LOCK_STATUS_REPORT;
					tmpArray[1] = LockStsCheck();
					tmpSize = 2;
					tmpEvent = F0_ALARM_REPORT;
				}
				InnerPackTx_MakePacketSinglePort(tmpEvent, ES_LOCK_EVENT, &gbInnerComCnt, tmpArray, tmpSize);
			}
		}
	}
}

void InnerPackTxEncapsulateData(BYTE *bQueueSendingData , uint16_t length)
{
	BYTE bTmpArray[48] = {0x00,};	// zero padding 하기 귀찮아서... 
	BYTE bTmpkey[16]  = {0x00,};
	uint32_t Encryptsize = ((length /16U) + ((length % 16 == 0) ? 0 : 1)) * 16; // AES128 를 돌릴 size 판단 
	
	memcpy(bTmpArray,bQueueSendingData,length); // 원래 size 만큼 복사 

	/* 실제 보낼 data 를 만들어줌 */
	if(gbMenuInnerExchangeKey == 0x01 && gbInnerpackModeSuspend)
	{
		//key 가 맞지 않아 suspend 상태 이면서 menu 로 강제 key 교환 하는 경우 
		//suspend 상태가 아니고 key 교환을 하는 경우는 자기가 가지고 있는 key 로 한다 
//		strcpy((SBYTE*)bTmpkey,"YaleIrevoSecured"); ??? 
		bTmpkey[0] = 0x59;
		bTmpkey[1] = 0x61;
		bTmpkey[2] = 0x6c;
		bTmpkey[3] = 0x65;
		bTmpkey[4] = 0x49;
		bTmpkey[5] = 0x72;
		bTmpkey[6] = 0x65;
		bTmpkey[7] = 0x76;
		bTmpkey[8] = 0x6f;
		bTmpkey[9] = 0x53;
		bTmpkey[10] = 0x65;
		bTmpkey[11] = 0x63;
		bTmpkey[12] = 0x75;
		bTmpkey[13] = 0x72;
		bTmpkey[14] = 0x65;
		bTmpkey[15] = 0x64;
	}
	else
	{
		RomRead(bTmpkey,INNER_PACK_ENCRYPT_KEY,16);
#ifdef _USE_IREVO_CRYPTO_
		EncryptDecryptKey(bTmpkey,sizeof(bTmpkey));
#endif 
	}

	gbInnerPackTxEnDataSendbuffer[PACK_F_EVENT] = F0_ENCRYPTION_MESSAGE;
	
	if(bQueueSendingData[PACK_F_SOURCE] ==  ES_LOCK_NACK)
	{
		gbInnerPackTxEnDataSendbuffer[PACK_F_SOURCE] = ES_LOCK_ACK; // E1 에 대한 NACK 존재 불가 
	}
	else
	{
		gbInnerPackTxEnDataSendbuffer[PACK_F_SOURCE] = bQueueSendingData[PACK_F_SOURCE];
	}
	gbInnerPackTxEnDataSendbuffer[PACK_F_COUNT] = bTmpArray[PACK_F_COUNT];
	gbInnerPackTxEnDataSendbuffer[PACK_F_LENGTH_ADDDATA] = Encryptsize;

#ifdef _USE_STM32_CRYPTO_LIB_
	DataEncrypt(bTmpArray,Encryptsize,bTmpkey,&gbInnerPackTxEnDataSendbuffer[PACK_F_ADDDATA]); // AES128 ECB 	
#endif 

	gbInnerPackTxEnDataByteNum = Encryptsize + 4;

	InnerPackTx_AddChecksum(gbInnerPackTxEnDataSendbuffer, &gbInnerPackTxEnDataByteNum);

}

void InnerPackTxGetCheckSumOfExisting(void)
{
	BYTE bTmpArray = 0x00;
	InnerPackTx_MakePacketSinglePort(F0_ENCRYPTION_GET_CHECKSUM, ES_LOCK_EVENT, &gbInnerComCnt, &bTmpArray,0);
}

BYTE InnerPackTxEncryptionKeyExchange(BYTE* bCheckSumData)
{
	BYTE bTmpArray[16] = {0x00,};
	WORD TmpCheckSum = (WORD)(((bCheckSumData[0] << 8) & 0xFF00) | bCheckSumData[1]);

	RomRead(bTmpArray,INNER_PACK_ENCRYPT_KEY,16);
	
#ifdef _USE_IREVO_CRYPTO_
	EncryptDecryptKey(bTmpArray,sizeof(bTmpArray));
#endif 

	if((Get_Checksum_CRC16(bTmpArray,16) & 0xFFFF) != TmpCheckSum)
	{
		gbInnerpackModeSuspend = 0x01;
		gbInnerModuleMode = PACK_ID_NOT_CONFIRMED;
		return -1;
	}

	gbInnerpackModeSuspend = 0x00;
	RandomNumberGenerator(gbInnerPackTxRandomKeybuffer,16);
	InnerPackTx_MakePacketSinglePort(F0_ENCRYPTION_KEY_CHANGE, ES_LOCK_EVENT, &gbInnerComCnt, gbInnerPackTxRandomKeybuffer,16);
	
	return 0;
}



#endif 


