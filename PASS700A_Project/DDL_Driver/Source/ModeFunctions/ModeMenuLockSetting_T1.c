//------------------------------------------------------------------------------
/** 	@file		ModeLockSetting_T1.c
	@version 0.1.00
	@date	2016.04.19
	@brief	Lock Setting Mode 
	@remark	Lock Setting 모드
	@see	MainModeProcess_T1.c
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.19		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"




void ModeGotoMenuLockSetting(void)
{
	gbMainMode = MODE_MENU_LOCK_SETTING;
	gbModePrcsStep = 0;
}



void AssignLockSettingSelectedMenu(void)
{
	switch(gMenuSelectKeyTempSave)
	{
	
#if	defined (DDL_CFG_NORMAL_ONLY) || defined (DDL_CFG_ADVANCED_ONLY)
		// 모드 전환 기능이 없는 제품일 경우 1번 메뉴 처리하지 않음
		case 1: 	
			break;

#else
		//change the mode Advanced<->Normal
		case 1: 	
			if(gbManageMode == _AD_MODE_SET)
			{
				ModeGotoChangeToNormalMode();
			}
			else
			{
				ModeGotoChangeToAdvancedMode();
			}
			break;
#endif
	
#ifdef	LOCKSET_AUTO_RELOCK
		//Auto relock setting
		case 2: 		
			ModeGotoAutoLockSetting();
			break;
#endif
	
#ifdef	LOCKSET_VOLUME_SET
		//Volume setting
		case 3: 		
			ModeGotoVolumeSetting();
			break;
#endif

#ifdef	DDL_CFG_TOUCH_OC
		//Safe Open/Close Button setting
		case 4: 		
			ModeGotoSafeOCButtonSetting();
			break;
#endif

#if !defined (DDL_CFG_MS)	
#ifdef	LOCKSET_LANGUAGE_SET
		//language setting
		case 5: 	
			ModeGotoLanguageSetting(); 
			break;
#endif
#endif 

#ifdef	CYLINDER_KEY_ALARM//실린더 알람 설정 메뉴 진입
		case 7:
			ModeGotoCylinderKeyAlarmSetting(); 	
			break;
#endif

			
#ifdef	LOCKSET_HANDING_LOCK
		//Handing the lock
		case 9: 		
			ModeGotoHandingLockSetting();
			break;
#endif

		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuLockSettingSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
#if	defined (DDL_CFG_NORMAL_ONLY) || defined (DDL_CFG_ADVANCED_ONLY)
		// 모드 전환 기능이 없는 제품일 경우 1번 메뉴 처리하지 않음
		case TENKEY_1:
			TimeExpiredCheck();
			break;

#else
		//change the mode Advanced<->Normal
		case TENKEY_1:			
			if(gbManageMode == _AD_MODE_SET)
			{
//				MenuSelectKeyTempSaveNTimeoutReset(56, gbInputKeyValue);	// Changing the mode to normal, press the hash to continue
				MenuSelectKeyTempSaveNTimeoutReset(AVML_GO_NOR_MODE_SHARP, gbInputKeyValue);	// Changing the mode to normal, press the hash to continue
			}
			else
			{
//				MenuSelectKeyTempSaveNTimeoutReset(30, gbInputKeyValue);	// Changing the mode to advanced, press the hash to continue	
				MenuSelectKeyTempSaveNTimeoutReset(AVML_GO_ADV_MODE_SHARP, gbInputKeyValue);	// Changing the mode to advanced, press the hash to continue	
			}
			break;
#endif

#ifdef	LOCKSET_AUTO_RELOCK
		//Auto relock setting
		case TENKEY_2:			
//			MenuSelectKeyTempSaveNTimeoutReset(152, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_AUTO_RELOCK_SHARP, gbInputKeyValue);	
			break;
#endif
				
#ifdef	LOCKSET_VOLUME_SET
		//Volume setting
		case TENKEY_3:			
//			MenuSelectKeyTempSaveNTimeoutReset(142, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_CHG_VOL_SHARP, gbInputKeyValue);	
			break;
#endif

#ifdef	DDL_CFG_TOUCH_OC
		//Safe Open/Close Button setting
		case TENKEY_4:			

	#if	defined(AVML_MSG_T1)
			MenuSelectKeyTempSaveNTimeoutReset(VOICE_MIDI_BUTTON, gbInputKeyValue);
	#elif	defined(AVML_MSG_T2)
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DOUBLE_OC_MENU, gbInputKeyValue);
	#else
			MenuSelectKeyTempSaveNTimeoutReset(VOICE_MIDI_BUTTON, gbInputKeyValue);
	#endif

			break;
#endif

#if !defined (DDL_CFG_MS)
#ifdef	LOCKSET_LANGUAGE_SET
		//language setting
		case TENKEY_5:			
//			MenuSelectKeyTempSaveNTimeoutReset(33, gbInputKeyValue);	// Changing Language, press the hash to continue	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_LANG_MENU_SHARP, gbInputKeyValue);	// Changing Language, press the hash to continue	
			break;
#endif
#endif 

#ifdef	CYLINDER_KEY_ALARM	//실린더키 설정 메뉴 선택
		case TENKEY_7:
			MenuSelectKeyTempSaveNTimeoutReset(AVML_SELECT_MENU, gbInputKeyValue);	
#endif
		
#ifdef	LOCKSET_HANDING_LOCK
		//Handing the lock
		case TENKEY_9:			
//			MenuSelectKeyTempSaveNTimeoutReset(232, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_HANDLING_SHARP, gbInputKeyValue);	
			break;
#endif

		case TENKEY_SHARP:
			AssignLockSettingSelectedMenu();
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuMainSelect();
			break;

		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;

		default:
			TimeExpiredCheck();
			break;
	}	
}




//------------------------------------------------------------------------------
/** 	@brief	Lock Setting Mode
	@param	None
	@return 	None
	@remark Lock Setting 모드 시작
*/
//------------------------------------------------------------------------------
void ModeMenuLockSetting(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackLockSettinglMenuOn(2, VOL_CHECK);
			FeedbackLockSettinglMenuOn(AVML_SELECT_MENU, VOL_CHECK);

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuLockSettingSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}

}




