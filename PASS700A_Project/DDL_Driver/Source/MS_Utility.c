//------------------------------------------------------------------------------
/** 	@file		MS_Utility.c
	@version 0.1.01
	@date	2017.12.22
	@brief	통합라이브러리 MS 사양 대응 utility 
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00	2017.12.22		by hyojoon.kim
			- 최초 구현 
*/
//------------------------------------------------------------------------------

#include	"main.h"

#if defined (DDL_CFG_MS)

//#define MS_DEBUGG

CredentialInfoTypeDef Ms_Credential_Info; 
static BYTE bRegDelSelect; 

/* 사양 별로 정보를 입력 해서 JIG 초기화시 info setting 할것 */
/* 권한 셋팅 프로그램으로 정보 확인 해서 입력 할 것 */
BYTE bMs_Credential_Info_table[13] ={
#if 1
	/* MS 사양 default setting */
	5,149,	/* Manager Card   :  5, Manager , Master , User card 등록 삭제 , motor open 권한 */
	20,128,	/* Master Card      : 20, motor open 권한 */
	70,128,	/* User Card         : 70, motor open 권한 */
	0,0,		/* Manager Code  :   0 */
	0,0,		/* Master Code     :   0 */ 
	1,128,	/* User Code        :   1, motor open 권한 */
	MS_EXCLUSICE_TYPE_1 /* Exclusive */ /* manager card 로 motor open 시 user card 삭제 user code "0000" setting */
#else 
	/* A20-IH 인천공항 사양 */
	0,0,		/* Manager Card  개수 , 권한 */
	5,132,	/* Master Card 개수 , 권한 */
	40,128,	/* User Card 개수 , 권한 */
	0,0,		/* Manager Code 개수 , 권한 */
	0,0,		/* Master Code 개수 , 권한 */
	1,128,	/* User Code 개수 , 권한 */
	0		/* Exclusive */
#endif 	
};

void MS_All_Credential_Init(void)
{
#ifdef MS_DEBUGG
	printf("MS_All_Credential_Init \n");
#endif 
	/* MS_CREDENTIAL_DATA_1 부터 MS_CREDENTIAL_DATA_100 까지 삭제 */
	RomWriteWithSameData(0xFF,MS_CREDENTIAL_DATA_1,MS_CREDENTIAL_SIZE);
	/* 등록된 인증 갯수 초기화 */
	RomWriteWithSameData(0x00,MS_MANAGER_CARD_NUM,MS_CREDENTIAL_NUM_SIZE);	
	FactoryResetSet(FACTORYRESET_DONE_BY_MANUAL);
}

void MS_Credential_Info_Init(void)
{
	BYTE bTemp[2];

	/* MS_MANAGER_CARD_INFO 부터 MS_USER_CODE_INFO 까지 삭제 */
	//RomWriteWithSameData(0xFF,MS_MANAGER_CARD_INFO,(8*6));

	/* MS_MANAGER_CARD_INFO 부터 MS_USER_CODE_INFO 까지 */

	RomWrite(&bMs_Credential_Info_table[0],MS_MANAGER_CARD_INFO,2);
	RomWrite(&bMs_Credential_Info_table[2],MS_MASTER_CARD_INFO,2);	
	RomWrite(&bMs_Credential_Info_table[4],MS_USER_CARD_INFO,2);	
	RomWrite(&bMs_Credential_Info_table[6],MS_MANAGER_CODE_INFO,2);
	RomWrite(&bMs_Credential_Info_table[8],MS_MASTER_CODE_INFO,2);	
	RomWrite(&bMs_Credential_Info_table[10],MS_USER_CODE_INFO,2);
	RomWrite(&bMs_Credential_Info_table[12],EXCLUSIVE_FUNCTIONS,1);
	MS_Get_Credential_Info();
}

void MS_Exclusive_Functions_Init(void)
{
	BYTE bTemp = 0x00;
#ifdef MS_DEBUGG
	printf("MS_Exclusive_Functions_Init \n");
#endif 	
	/* EXCLUSIVE_FUNCTIONS init */
	RomWrite(&bTemp,EXCLUSIVE_FUNCTIONS,1);
}

void MS_Get_Credential_Info(void)
{
	BYTE bTemp = 0x00;
	BYTE* bInfoIndex = &Ms_Credential_Info.bManager_Card_Info[0];
	WORD* bAddressIndex = &Ms_Credential_Info.wCertification_Address[0];
	
	RomRead(Ms_Credential_Info.bManager_Card_Info,MS_MANAGER_CARD_INFO,(8*6));

	Ms_Credential_Info.wCertification_Address[MANAGER_CARD_INDEX] = MS_CREDENTIAL_DATA_1;
	Ms_Credential_Info.wCertification_Address[MASTER_CARD_INDEX] = (WORD)(Ms_Credential_Info.wCertification_Address[MANAGER_CARD_INDEX] + (Ms_Credential_Info.bManager_Card_Info[0]*8));
	Ms_Credential_Info.wCertification_Address[USER_CARD_INDEX] = (WORD)(Ms_Credential_Info.wCertification_Address[MASTER_CARD_INDEX] + (Ms_Credential_Info.bMaster_Card_Info[0]*8));	

	Ms_Credential_Info.wCertification_Address[MANAGER_CODE_INDEX] = (WORD)(Ms_Credential_Info.wCertification_Address[USER_CARD_INDEX] + (Ms_Credential_Info.bUser_Card_Info[0]*8));
	Ms_Credential_Info.wCertification_Address[MASTER_CODE_INDEX] = (WORD)(Ms_Credential_Info.wCertification_Address[MANAGER_CODE_INDEX] + (Ms_Credential_Info.bManager_Code_Info[0]*8));	
	Ms_Credential_Info.wCertification_Address[USER_CODE_INDEX] = (WORD)(Ms_Credential_Info.wCertification_Address[MASTER_CODE_INDEX] + (Ms_Credential_Info.bMaster_Code_Info[0]*8));

	/* Credential_Info 의 첫번째 byte 에 0x00 이 아닌 값이 있는 경우 setting 되었다고 본다 */
	Ms_Credential_Info.bCredential_Info_Set_Complete = 0x00;

	for(bTemp = 0x00 ; bTemp < 6 ; bTemp++)
	{
		if(*bInfoIndex != 0x00 && *bInfoIndex != 0xFF)
		{
			/*
			Bit 0 : MANAGER_CARD_INDEX setting 유무 
			Bit 1 : MASTER_CARD_INDEX setting 유무 
			Bit 2 : USER_CARD_INDEX setting 유무 
			Bit 3 : MANAGER_CODE_INDEX setting 유무 
			Bit 4 : MASTER_CODE_INDEX setting 유무 
			Bit 5 : USER_CODE_INDEX setting 유무 
			Bit 6 : Reserved
			Bit 7 : Reserved
			*/
			Ms_Credential_Info.bCredential_Info_Set_Complete |= 0x1 << bTemp;
		}

		bInfoIndex += 8;
		bAddressIndex++;
	}

	/* setting 에 정의한 총 등록 가능 수 */
	Ms_Credential_Info.bSumofCard = Ms_Credential_Info.bManager_Card_Info[0] + Ms_Credential_Info.bMaster_Card_Info[0] + Ms_Credential_Info.bUser_Card_Info[0];
	Ms_Credential_Info.bSumofCode = Ms_Credential_Info.bManager_Code_Info[0] + Ms_Credential_Info.bMaster_Code_Info[0] + Ms_Credential_Info.bUser_Code_Info[0];	

#ifdef MS_DEBUGG
	printf("MS_Get_Credential_Info \n");
	printf("Manager		Card Num [%d] Authority [%d] \n",Ms_Credential_Info.bManager_Card_Info[0],Ms_Credential_Info.bManager_Card_Info[1]);	
	printf("Master		Card Num [%d] Authority [%d] \n",Ms_Credential_Info.bMaster_Card_Info[0],Ms_Credential_Info.bMaster_Card_Info[1]);	
	printf("User		Card Num [%d] Authority [%d] \n",Ms_Credential_Info.bUser_Card_Info[0],Ms_Credential_Info.bUser_Card_Info[1]);	
	printf("Manager		Code Num [%d] Authority [%d] \n",Ms_Credential_Info.bManager_Code_Info[0],Ms_Credential_Info.bManager_Code_Info[1]);	
	printf("Master		Code Num [%d] Authority [%d] \n",Ms_Credential_Info.bMaster_Code_Info[0],Ms_Credential_Info.bMaster_Code_Info[1]);	
	printf("User		Code Num [%d] Authority [%d] \n\n",Ms_Credential_Info.bUser_Code_Info[0],Ms_Credential_Info.bUser_Code_Info[1]);

	printf("Manager		Card EEPROM Start Address [0x%x] \n",Ms_Credential_Info.wCertification_Address[0]);
	printf("Master		Card EEPROM Start Address [0x%x] \n",Ms_Credential_Info.wCertification_Address[1]);
	printf("User		Code EEPROM Start Address [0x%x] \n",Ms_Credential_Info.wCertification_Address[2]);
	printf("Manager		Code EEPROM Start Address [0x%x] \n",Ms_Credential_Info.wCertification_Address[3]);
	printf("Master		Code EEPROM Start Address [0x%x] \n",Ms_Credential_Info.wCertification_Address[4]);
	printf("User		Code EEPROM Start Address [0x%x] \n",Ms_Credential_Info.wCertification_Address[5]);	
#endif

	RomRead(&Ms_Credential_Info.bExclusive,EXCLUSIVE_FUNCTIONS,1);
}

void MS_ModeGotoCardCodeRegDelCheck(void)
{
	gbMainMode = MS_MODE_MENU_CARD_CODE_REGDEL;
	gbModePrcsStep = 0;
}

void MS_CardCodeModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// Card or Code 등록 삭제 
		case TENKEY_1: 	
		case TENKEY_3: 	
			MS_ModeGotoAuthorityVerify();			
			break;

		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}


void MS_MenuCardCodeModeSelectProcess(void)
{
	BYTE bTemp = 0x00;
	
	switch(gbInputKeyValue)
	{
		case TENKEY_1:	
			/* 등록의 경우 현재 선택된 아이템의 등록 가능 수와 등록된 수를 비교 */
			/* 이미 전부 채워진 경우 등록 메뉴로 못가게 */
			MS_Get_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,&bTemp);

			if(bTemp >= MS_GetSupported_Credential_Number(Ms_Credential_Info.bSelected_Item))
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
				gMenuSelectKeyTempSave = TENKEY_NONE;
			}
			else 
			{
				MenuSelectKeyTempSaveNTimeoutReset(((Ms_Credential_Info.bSelected_Item < MANAGER_CODE_INDEX) ? AVML_REG_CARD_SHARP : AVML_REG_PIN_MODE_SHARP), gbInputKeyValue);
			}
			break;

		case TENKEY_3:		
			MenuSelectKeyTempSaveNTimeoutReset(((Ms_Credential_Info.bSelected_Item < MANAGER_CODE_INDEX) ? AVML_DEL_CARD_SHARP : AVML_DEL_PIN_SHARP), gbInputKeyValue);	
			break;

		case TENKEY_SHARP:
			MS_CardCodeModeSetSelected(); 
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}

void MS_ModeCardCodeRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			Feedback13MenuOn((Ms_Credential_Info.bSelected_Item < MANAGER_CODE_INDEX) ? AVML_REGCARD1_DELCARD3 : AVML_REGPIN1_DELPIN3, VOL_CHECK);

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;
			gbModePrcsStep = 1;
			break;
			
		case 1: 		
			MS_MenuCardCodeModeSelectProcess();
			break;	

		default:
			ModeClear();
			break;
	}

}

void MS_ModeGotoAuthorityVerify(void)
{
	BYTE bTemp[6][2] = {0xFF,}; 
	BYTE bTemp1 = 0x00;	
	
	gbMainMode = MS_MODE_MENU_AUTHORITY_VERIFY;
	bRegDelSelect	 = gMenuSelectKeyTempSave;

	/* 인증 수단 별로 등록 된것이 있는 지 확인 */ 
	/* 2 차원 배열로 [] [] = {등록된 인증 수단 갯수 , 해당 인증 수단의 권한 } */
	for(bTemp1 = 0x00 ; bTemp1 < (USER_CODE_INDEX + 1); bTemp1++)
	{
		MS_Get_Registered_Credential_Num(bTemp1,&bTemp[bTemp1][0]);
		bTemp[bTemp1][1] = *(Ms_Credential_Info.bManager_Card_Info+(bTemp1*8)+1);
	}

	/* 등록 삭제를 하기 위해 선택된 종류에 의해 비교 대상 변경 */
	/* Card 나 비번이 없으면 해당 등록 삭제에 대해서 권한 인증을 skip 할수 있음 */
	switch(Ms_Credential_Info.bSelected_Item)
	{
		case MANAGER_CARD_INDEX:
		case MANAGER_CODE_INDEX:				

			if(Ms_Credential_Info.bSelected_Item == MANAGER_CARD_INDEX)
				bTemp1 = (MANAGER_CARD_REG_DEL_ALLOW);
			else 
				bTemp1 = (MANAGER_CODE_REG_DEL_ALLOW);			

			if(((bTemp[MANAGER_CARD_INDEX][0] != 0x00) && (bTemp[MANAGER_CARD_INDEX][1] &bTemp1)) ||
				((bTemp[MANAGER_CODE_INDEX][0] != 0x00) && (bTemp[MANAGER_CODE_INDEX][1] &bTemp1)))
			{
				/* manager card 나 code 가 있고 권한이 있으면 인증 하러 */ 
				gbModePrcsStep = 0;
			}
			else 
			{
				gbModePrcsStep = 4;
			}
		break;

		case MASTER_CARD_INDEX:
		case MASTER_CODE_INDEX:

			if(Ms_Credential_Info.bSelected_Item == MASTER_CARD_INDEX)
				bTemp1 = (MASTER_CARD_REG_DEL_ALLOW);
			else 
				bTemp1 = (MASTER_CODE_REG_DEL_ALLOW);
			
			if(((bTemp[MANAGER_CARD_INDEX][0] != 0x00) && (bTemp[MANAGER_CARD_INDEX][1] &bTemp1)) ||
				((bTemp[MASTER_CARD_INDEX][0] != 0x00) && (bTemp[MASTER_CARD_INDEX][1] &bTemp1)) ||
				((bTemp[MANAGER_CODE_INDEX][0] != 0x00) && (bTemp[MANAGER_CODE_INDEX][1] &bTemp1)) ||
				((bTemp[MASTER_CODE_INDEX][0] != 0x00) && (bTemp[MASTER_CODE_INDEX][1] &bTemp1)))
			{
				/* master card code 와 상위 인증 수단인 manager card code 가 등록 되어 있으면서 */
				/* master card code 에 대한 인증 권한이 있으면 인증 수단 입력 받으러 */
				gbModePrcsStep = 0;
			}
			else
			{	
				gbModePrcsStep = 4;
			}
		break;

		case USER_CARD_INDEX:
		case USER_CODE_INDEX:
		case SETTING_MENU_INDEX:

			if(Ms_Credential_Info.bSelected_Item == SETTING_MENU_INDEX)
				bTemp1 = (MENU_MODE_ALLOW);
			else if(Ms_Credential_Info.bSelected_Item == USER_CARD_INDEX)
				bTemp1 = (USER_CARD_REG_DEL_ALLOW);
			else 
				bTemp1 = (USER_CODE_REG_DEL_ALLOW);
			
			if(((bTemp[MANAGER_CARD_INDEX][0] != 0x00) && (bTemp[MANAGER_CARD_INDEX][1] & bTemp1)) ||
				((bTemp[MASTER_CARD_INDEX][0] != 0x00) && (bTemp[MASTER_CARD_INDEX][1] & bTemp1)) ||
				((bTemp[USER_CARD_INDEX][0] != 0x00) && (bTemp[USER_CARD_INDEX][1] & bTemp1)) ||				
				((bTemp[MANAGER_CODE_INDEX][0] != 0x00) && (bTemp[MANAGER_CODE_INDEX][1] & bTemp1)) ||
				((bTemp[MASTER_CODE_INDEX][0] != 0x00) && (bTemp[MASTER_CODE_INDEX][1] & bTemp1)) ||
				((bTemp[USER_CODE_INDEX][0] != 0x00) && (bTemp[USER_CODE_INDEX][1] & bTemp1)))
			{
				/* User card code 이면서 상위 인증 수단 master , manager 를 다 비교 */
				gbModePrcsStep = 0;
			}
			else
			{	
				gbModePrcsStep = 4;
			}
		break;

		case CREDENTIAL_INFO_SET:
			/* test mode 에서 52 번 입력 하여 인증 수단 개수 / 권한 설정 메뉴 진입 시 */
#if 1
			/* 원래 권한을 주려 했는데 어짜피 비번도 다 초기화 해야 하니 인증 없는 걸로 */
			gbModePrcsStep = 4;
#else 
			/* 최상위 권한 인증 수단만 있으면 진입 */ 
			if((bTemp[MANAGER_CARD_INDEX][0] == 0x00 && bTemp[MANAGER_CODE_INDEX][0] == 0x00) &&
				(bTemp[MASTER_CARD_INDEX][0] == 0x00 && bTemp[MASTER_CODE_INDEX][0] == 0x00) &&
				(bTemp[USER_CARD_INDEX][0] == 0x00 && bTemp[USER_CODE_INDEX][0] == 0x00))
			{
				bRegDelSelect = 0xFF;
				Ms_Credential_Info.bSelected_Item = 0xFF;
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
			}
			else 
			{
				gbModePrcsStep = 0;
			}
#endif 			
		break;

		default :
		break;
	}
}

void MS_ModeAuthorityVerify(void)
{
	BYTE bTemp = 0x00;
	
	switch(gbModePrcsStep)
	{
		case 0:
			//FeedbackKeyPadLedOn(AVML_IN_PIN_SHARP, VOL_CHECK); 
			/* verify mode 때 와 등록 때 구분 하기 위해 소리를 다른 소리를 낸다 */
			AudioFeedback(AVML_IN_PIN_SHARP, gcbBuzEnterVerify, VOL_CHECK);
			FeedbackKeyPadLedOnLedOnly();
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			SetModeProcessTime(0);
			CardGotoReadStart();
			gbModePrcsStep = 1;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:		 
					CardGotoReadStop();

					if(GetInputKeyCount() == 0x00)
					{
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						ModeClear();
						break;
					}
					
					if(bRegDelSelect == TENKEY_3)
						FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
					
					gbModePrcsStep = 2;
					break;		

				case TENKEY_STAR:
					if(Ms_Credential_Info.bSelected_Item == SETTING_MENU_INDEX)
						ReInputOrModeMove(ModeGotoMenuMainSelect);
					else 
						ReInputOrModeMove(MS_ModeGotoCardCodeRegDelCheck);						
					break;
					
				case TENKEY_NONE:
					if(GetCardReadStatus() == CARDREAD_SUCCESS)
					{
						FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
						gbModePrcsStep = 3;
					}
					CardReadStatusClear();
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;

			bTemp = PincodeVerify(0);
		
			switch(bTemp)
			{
				case RET_NO_INPUT:
				case RET_WRONG_DIGIT_INPUT:
				case RET_NO_MATCH:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;				
			
				default:
					bTemp = MS_Supported_Authority_Check(Ms_Credential_Info.bSelected_Item);

					if(bTemp == 0xFF)
					{
						//AudioFeedback(VOICE_MIDI_INTO, gcbBuzSta,  VOL_CHECK);
						gbModePrcsStep = 4;
					}
					else 
					{
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);		
						ModeClear();
					}
					break;				
			}
		break;

		case 3:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;

			bTemp = CardVerify(1);

			if(bTemp == _DATA_NOT_IDENTIFIED)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				gbModePrcsStep = 7;
			}
			else 
			{
				bTemp = MS_Supported_Authority_Check(Ms_Credential_Info.bSelected_Item);

				if(bTemp == 0xFF)
				{
					gbModePrcsStep = 8;
				}
				else 
				{
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);		
					gbModePrcsStep = 7;
				}
			}
		break;

		case 4:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() || GetModeProcessTime()) break;

			switch (Ms_Credential_Info.bSelected_Item)
			{
				case MANAGER_CARD_INDEX : 
				case MASTER_CARD_INDEX : 
				case USER_CARD_INDEX : 
					if(bRegDelSelect == TENKEY_1)
					{
						/* Card 등록 */
						MS_Get_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,&bTemp);
						FeedbackCredentialInput(VOICE_MIDI_INTO, bTemp);
						CardGotoReadStart();
						ModeGotoCardRegister();
					}
					else 
					{
						/* Card 삭제 */
						AllCardDelete();
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						gbModePrcsStep = 6;
					}
				break;

				case MANAGER_CODE_INDEX : 
				case MASTER_CODE_INDEX : 
				case USER_CODE_INDEX : 
					if(bRegDelSelect == TENKEY_1)
					{
						/* Code 등록 */
						TenKeyVariablesClear();
						FeedbackKeyPadLedOn(VOICE_MIDI_INTO, VOL_CHECK);
						ModeGotoPincodeRegister();
					}
					else 
					{
						/* Code 삭제 */
						MS_PinCodeDelete();
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						if(Ms_Credential_Info.bExclusive == MS_EXCLUSICE_TYPE_2)
						{
							/* 영남대*/
							MS_Do_Exclusive_Function();
						}
						gbModePrcsStep =6;
					}
				break;

				case SETTING_MENU_INDEX : 
					/* 7번 setting menu 로 */
					ModeGotoMenuLockSetting();
				break;

				case CREDENTIAL_INFO_SET : 
					/* 52 번 credential info setting 메뉴로 */
					gbMainMode = MS_MODE_MENU_CREDENTIAL_INFO_SET;
					gbModePrcsStep = 0;
				break;

				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;				
			}

			break;		

		case 5:
			GotoPreviousOrComplete(MS_ModeGotoCardCodeRegDelCheck);
			break;

		case 6:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ModeGotoMenuMainSelect();
			break;

		case 7:
			/* error 종료 하는 경우 */
			if(GetCardReadStatus() == CARDREAD_NO_CARD)
			{
				ModeClear();
			}	
			break;

		case 8:
			/* card 떨어짐 상태를 보고 등록이나 / 삭제로 가능 경우 */
			if(GetCardReadStatus() == CARDREAD_NO_CARD)
			{
				gbModePrcsStep = 4;
			}	
			SetModeProcessTime(50);
			break;


		default:
			ModeClear();
			break;
	}
}


void MenuCredentialInfoSetSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		case TENKEY_1:
		case TENKEY_2:
		case TENKEY_3:
		case TENKEY_4:			
			MenuSelectKeyTempSaveNTimeoutReset(VOICE_MIDI_BUTTON, gbInputKeyValue);		
			break;

		case TENKEY_SHARP:		 //# 버튼음 출력
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
			{
				FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
				gbModePrcsStep++;
				break;
			}
			TimeExpiredCheck();
			break;
									
		case TENKEY_STAR:
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	
			ModeClear();
			break;

		default:
			TimeExpiredCheck();
			break;
	}	
}



void MS_ModeMenuCredentialInfoSet(void)
{
	WORD bTemp = 0x00;
	BYTE bTemp1 = 0x00;
	BYTE bTemp2 = 0x00;
	static BYTE bTemp3[6] = {0x00,}; 
	WORD bTemp4 = 0x00;	
		
	switch(gbModePrcsStep)
	{
		case 0:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			Feedback123MenuOn(AVML_SELECT_MENU, VOL_CHECK);
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gMenuSelectKeyTempSave = TENKEY_NONE;
			gbModePrcsStep = 1;
			memset(bTemp3,0x00,6);
			Ms_Credential_Info.bSelected_Item = MANAGER_CARD_INDEX;
#ifdef MS_DEBUGG
			MS_Get_Credential_Info();
#endif 
			break;
		
		case 1:
			MenuCredentialInfoSetSelectProcess();
			break;		

		case 2:
			FeedbackKeyPadLedOn(VOICE_MIDI_INTO, VOL_CHECK); 
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gbModePrcsStep = 3;
			break;

		case 3:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(3, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		
						ModeClear();
						break;
					}
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:		
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);
					bTemp = GetInputKeyCount();

					if(bTemp == 0x00)
					{
						if(gMenuSelectKeyTempSave == TENKEY_1)	/* 인증 개수 입력 */
						{
							/* 건너 뛰게 되면 0x00 으로 */
							bTemp3[Ms_Credential_Info.bSelected_Item] = 0x00;
#ifdef MS_DEBUGG
							printf("Input [%d] \n",bTemp3[Ms_Credential_Info.bSelected_Item]);
#endif
							Ms_Credential_Info.bSelected_Item++; 

							if(Ms_Credential_Info.bSelected_Item >= SETTING_MENU_INDEX)
							{
								bTemp4 = (bTemp3[0]+bTemp3[1]+bTemp3[2]+bTemp3[3]+bTemp3[4]+bTemp3[5]);
								
								if( bTemp4 > 100 ||bTemp4 == 0x00)
								{
									/* credentail 수가 100 을 넘어 가면 저장 없이 error */ 
#ifdef MS_DEBUGG
									printf("Error over 100 or 0 \n");
#endif									
									FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
									ModeClear();
								}
								else
								{
									RomWrite(&bTemp3[0],MS_MANAGER_CARD_INFO,1);
									RomWrite(&bTemp3[1],MS_MASTER_CARD_INFO,1);
									RomWrite(&bTemp3[2],MS_USER_CARD_INFO,1);								
									RomWrite(&bTemp3[3],MS_MANAGER_CODE_INFO,2);								
									RomWrite(&bTemp3[4],MS_MASTER_CODE_INFO,1);	
									RomWrite(&bTemp3[5],MS_USER_CODE_INFO,1);	
									
									MS_All_Credential_Init();
									MS_Get_Credential_Info();							
									FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
									gbModePrcsStep = 0;
								}
							}
							else 
							{
								gbModePrcsStep = 2;
							}
						}
						else if(gMenuSelectKeyTempSave == TENKEY_2)	/* 권한  입력 */
						{
							/* 건너 뛰게 되면 0x00 으로 */
							BYTE bPTemp = 0x00;
#ifdef MS_DEBUGG
							printf("Input [%d] \n",*bPTemp);
#endif
							RomWrite(&bPTemp,MS_MANAGER_CARD_INFO+(Ms_Credential_Info.bSelected_Item*8)+1,1);
							Ms_Credential_Info.bSelected_Item++; 

							if(Ms_Credential_Info.bSelected_Item >= SETTING_MENU_INDEX)
							{
								MS_All_Credential_Init();
								MS_Get_Credential_Info();							
								FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
								gbModePrcsStep = 0;
							}
							else 
							{
								gbModePrcsStep = 2;
							}
						}
						else 
						{
							/* 특수 사양 설정 이나  세팅 초기화 시 */
							/* 입력 없으면 error 처리 */
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
							ModeClear();
						}
						break;
					}
					else if(bTemp == 1)
					{
						bTemp = (gPinInputKeyFromBeginBuf[0] >> 4) & 0x0F;
					}
					else if(bTemp == 2)
					{
						bTemp = (gPinInputKeyFromBeginBuf[0] >> 4) & 0x0F;
						bTemp1 = gPinInputKeyFromBeginBuf[0] & 0x0F;
						bTemp = (bTemp * 10) + bTemp1;
					}
					else if(bTemp == 3)
					{
						bTemp = (gPinInputKeyFromBeginBuf[0] >> 4) & 0x0F;
						bTemp1 = gPinInputKeyFromBeginBuf[0] & 0x0F;
						bTemp2 = (gPinInputKeyFromBeginBuf[1] >> 4) & 0x0F;
						bTemp = (bTemp * 100) + (bTemp1 * 10) + bTemp2;
					}
					
#ifdef MS_DEBUGG
					printf("Input [%d] \n",bTemp);
#endif 

					if(gMenuSelectKeyTempSave == TENKEY_1)
					{
						if(bTemp > 100)
						{
							/*입력 값이 100 보다 크면 error */
#ifdef MS_DEBUGG
							printf("Error over 100\n");
#endif								
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
							ModeClear();
							break;
						}
						
						bTemp3[Ms_Credential_Info.bSelected_Item] = (BYTE)bTemp;
						Ms_Credential_Info.bSelected_Item++;
						
						if(Ms_Credential_Info.bSelected_Item >= SETTING_MENU_INDEX)
						{
							bTemp4 = (bTemp3[0]+bTemp3[1]+bTemp3[2]+bTemp3[3]+bTemp3[4]+bTemp3[5]);
							if( bTemp4 > 100 ||bTemp4 == 0x00)
							{
								/* credentail 수가 100 을 넘어 가면 저장 없이 error */ 
#ifdef MS_DEBUGG
								printf("Error over 100 or 0 \n");
#endif								
								FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
								ModeClear();
							}
							else
							{
								RomWrite(&bTemp3[0],MS_MANAGER_CARD_INFO,1);
								RomWrite(&bTemp3[1],MS_MASTER_CARD_INFO,1);
								RomWrite(&bTemp3[2],MS_USER_CARD_INFO,1);								
								RomWrite(&bTemp3[3],MS_MANAGER_CODE_INFO,2);								
								RomWrite(&bTemp3[4],MS_MASTER_CODE_INFO,1);	
								RomWrite(&bTemp3[5],MS_USER_CODE_INFO,1);	
								
								MS_All_Credential_Init();
								MS_Get_Credential_Info();							
								FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
								gbModePrcsStep = 0;
							}
						}
						else
						{
							gbModePrcsStep = 2;
						}
						break;
					}
					else if (gMenuSelectKeyTempSave == TENKEY_2)
					{
						if(bTemp > 255)
						{
							/* 권한 설정 255 보다 크면 */
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
							ModeClear();
							break;
						}

						bTemp1 = (BYTE)bTemp;
						RomWrite(&bTemp1,MS_MANAGER_CARD_INFO+(Ms_Credential_Info.bSelected_Item*8)+1,1);
						Ms_Credential_Info.bSelected_Item++;

						if(Ms_Credential_Info.bSelected_Item >= SETTING_MENU_INDEX)
						{
							MS_All_Credential_Init();
							MS_Get_Credential_Info();
							FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);								
							gbModePrcsStep = 0;
						}
						else
						{
							gbModePrcsStep = 2;
						}
						break;
					}
					else if (gMenuSelectKeyTempSave == TENKEY_3)
					{
						if(bTemp > 254) // 0 ~ 254 까지 
						{
							/* 특수 사양 254 보다 크면 */
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
							ModeClear();
							break;
						}

						bTemp1 = (BYTE)bTemp;
						RomWrite(&bTemp1,EXCLUSIVE_FUNCTIONS,1);
						MS_Get_Credential_Info();
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						gbModePrcsStep = 0;
						break;
					}
					else if (gMenuSelectKeyTempSave == TENKEY_4)
					{
						switch(bTemp)
						{
							case 1 :
								MS_Credential_Info_Init();
							break;

							default :
								MS_Credential_Info_Init();								
							break;
						}
						MS_All_Credential_Init();
						MS_Get_Credential_Info();	
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						gbModePrcsStep = 0;
						break;
					}
					break;		

				case FUNKEY_OPCLOSE:
				case TENKEY_STAR:
				case FUNKEY_REG:					
					/* 실수를 하더라도 기회를 주는게 아니라 처음 부터 */
					/* 헤깔리니까.... */ 
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
					
				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		default:
			ModeClear();
			break;
	}
}


void MS_PinCodeDelete(void)
{
	RomWriteWithSameData(0xFF,MS_GetSupported_Credential_Start_Address(Ms_Credential_Info.bSelected_Item),(MS_GetSupported_Credential_Number(Ms_Credential_Info.bSelected_Item)*8)); 
	MS_Set_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,0x00);	
	FactoryResetDoneCheck();
}

BYTE MS_Supported_Authority_Check(BYTE Selected)
{
	/* 등록 삭제 시 선택된 인증 수단의 권한을 확인 하는 함수 */
	/* 0xFF 면 권한이 있다는 ... main menu 가 돌고 있는 중에 만 ....*/
	
	BYTE bTemp = RET_NO_MATCH;
	
	switch(Selected)
	{
		case MANAGER_CARD_INDEX:
			if(MS_GetSupported_Authority(Ms_Credential_Info.bVerifiedType,MANAGER_CARD_REG_DEL_ALLOW))
				bTemp = 0xFF; 
		break;
		
		case MASTER_CARD_INDEX : 
			if(MS_GetSupported_Authority(Ms_Credential_Info.bVerifiedType,MASTER_CARD_REG_DEL_ALLOW))
				bTemp = 0xFF;				
		break;

		case USER_CARD_INDEX : 
			if(MS_GetSupported_Authority(Ms_Credential_Info.bVerifiedType,USER_CARD_REG_DEL_ALLOW))
				bTemp = 0xFF;				
		break;
		
		case MANAGER_CODE_INDEX : 
			if(MS_GetSupported_Authority(Ms_Credential_Info.bVerifiedType,MANAGER_CODE_REG_DEL_ALLOW))
				bTemp = 0xFF;				
		break;
		
		case MASTER_CODE_INDEX : 
			if(MS_GetSupported_Authority(Ms_Credential_Info.bVerifiedType,MASTER_CODE_REG_DEL_ALLOW))
				bTemp = 0xFF;				
		break;

		case USER_CODE_INDEX : 
			if(MS_GetSupported_Authority(Ms_Credential_Info.bVerifiedType,USER_CODE_REG_DEL_ALLOW))
				bTemp = 0xFF;				
		break;		

		case SETTING_MENU_INDEX : 
			if(MS_GetSupported_Authority(Ms_Credential_Info.bVerifiedType,MENU_MODE_ALLOW))
				bTemp = 0xFF;				
		break;

#if 0			
		case CREDENTIAL_INFO_SET : 
			/* test mode 의 CREDENTIAL_INFO_SET 설정시 최상위 권한 만 본다 */
			if(MS_GetSupported_Credential_Number(MANAGER_CARD_INDEX) || MS_GetSupported_Credential_Number(MANAGER_CODE_INDEX))
			{
				if(Ms_Credential_Info.bVerifiedType == CREDENTIAL_TYPE_MANAGER_CARD || Ms_Credential_Info.bVerifiedType == CREDENTIAL_TYPE_MANAGER_CODE)
					bTemp = 0xFF;
			}
			else if (MS_GetSupported_Credential_Number(MASTER_CARD_INDEX) || MS_GetSupported_Credential_Number(MASTER_CODE_INDEX))
			{
				if(Ms_Credential_Info.bVerifiedType == CREDENTIAL_TYPE_MASTER_CARD || Ms_Credential_Info.bVerifiedType == CREDENTIAL_TYPE_MASTER_CODE)
					bTemp = 0xFF;
			}
			else if (MS_GetSupported_Credential_Number(USER_CARD_INDEX) || MS_GetSupported_Credential_Number(USER_CODE_INDEX))
			{
				if(Ms_Credential_Info.bVerifiedType == CREDENTIAL_TYPE_USER_CARD || Ms_Credential_Info.bVerifiedType == CREDENTIAL_TYPE_USER_CODE)
					bTemp = 0xFF;
			}
			else 
			{
				bTemp = RET_NO_MATCH;
			}
		break;
#endif 
		default :
		break;
	}
	return bTemp;
}

BYTE MS_GetSupported_Authority(BYTE bVerifiedType , BYTE Authority_Mask)
{
	/* 인증 완료 후 이후 동작에 대한 권한을 보고 등록 / 삭제 / menu / Motor 제어 권을 check 하는 함수 */
	/*PincodeVerify 나 CardVerify 이후 호출 해야 함  */
	/*Ms_Credential_Info.bVerifiedType 을  PincodeVerify 나 CardVerify 여기서 update 하므로 */ 
	
	BYTE bTemp = 0x00;
	
	switch (bVerifiedType)
	{
		case CREDENTIAL_TYPE_MANAGER_CARD: // Manager Card 
			bTemp = Ms_Credential_Info.bManager_Card_Info[1];
		break;

		case CREDENTIAL_TYPE_MASTER_CARD: // Master Card 
			bTemp = Ms_Credential_Info.bMaster_Card_Info[1];		

			if(Authority_Mask == MANAGER_CARD_REG_DEL_ALLOW || Authority_Mask == MANAGER_CODE_REG_DEL_ALLOW)
			{
				/* 인증 완료된 인증 수단의 권한 보다 더 높은 것을 요구 할경우 제외 */
				bTemp &=  ~(MANAGER_CARD_REG_DEL_ALLOW|MANAGER_CODE_REG_DEL_ALLOW);
			}
		break;

		case CREDENTIAL_TYPE_USER_CARD: // User Card
			bTemp = Ms_Credential_Info.bUser_Card_Info[1];

			if(Authority_Mask == MANAGER_CARD_REG_DEL_ALLOW || Authority_Mask == MANAGER_CODE_REG_DEL_ALLOW ||
				Authority_Mask == MASTER_CARD_REG_DEL_ALLOW || Authority_Mask == MASTER_CODE_REG_DEL_ALLOW)
			{
				/* 인증 완료된 인증 수단의 권한 보다 더 높은 것을 요구 할경우 제외 */
				bTemp &=  ~(MANAGER_CARD_REG_DEL_ALLOW|MANAGER_CODE_REG_DEL_ALLOW|MASTER_CARD_REG_DEL_ALLOW|MASTER_CODE_REG_DEL_ALLOW);
			}

			/* User 권한은 내부 강제 잠금 상태 일때 문 열지 못하게 */
			/* 나머지 인증 수단은 권한만 motor open 권한만 있으면 내부 강제 잠금 상태라도 열수 있다 */
			if((MotorSensorCheck() & SENSOR_LOCK_STATE) == SENSOR_LOCK_STATE)
			{
				bTemp &= ~(MOTOR_OPEN_ALLOW);
			}
			
		break;

		case CREDENTIAL_TYPE_MANAGER_CODE:  // Manager Code 
			bTemp = Ms_Credential_Info.bManager_Code_Info[1];
		break;

		case CREDENTIAL_TYPE_MASTER_CODE:  // Master Code 
			bTemp = Ms_Credential_Info.bMaster_Code_Info[1];

			if(Authority_Mask == MANAGER_CARD_REG_DEL_ALLOW || Authority_Mask == MANAGER_CODE_REG_DEL_ALLOW)
			{
				/* 인증 완료된 인증 수단의 권한 보다 더 높은 것을 요구 할경우 제외 */						
				bTemp &=  ~(MANAGER_CARD_REG_DEL_ALLOW|MANAGER_CODE_REG_DEL_ALLOW);
			}			
		break;

		case CREDENTIAL_TYPE_USER_CODE:  // User Code 
			bTemp = Ms_Credential_Info.bUser_Code_Info[1];
			if(Authority_Mask == MANAGER_CARD_REG_DEL_ALLOW || Authority_Mask == MANAGER_CODE_REG_DEL_ALLOW ||
				Authority_Mask == MASTER_CARD_REG_DEL_ALLOW || Authority_Mask == MASTER_CODE_REG_DEL_ALLOW)
			{
				/* 인증 완료된 인증 수단의 권한 보다 더 높은 것을 요구 할경우 제외 */
				bTemp &=  ~(MANAGER_CARD_REG_DEL_ALLOW|MANAGER_CODE_REG_DEL_ALLOW|MASTER_CARD_REG_DEL_ALLOW|MASTER_CODE_REG_DEL_ALLOW);
			}

			/* User 권한은 내부 강제 잠금 상태 일때 문 열지 못하게 */
			/* 나머지 인증 수단은 권한만 motor open 권한만 있으면 내부 강제 잠금 상태라도 열수 있다 */
			if((MotorSensorCheck() & SENSOR_LOCK_STATE) == SENSOR_LOCK_STATE)
			{
				bTemp &= ~(MOTOR_OPEN_ALLOW);
			}
		break;
		
		default :
		break;
	}

	if(JigModeStatusCheck() == STATUS_SUCCESS)
	{
		bTemp |= MOTOR_OPEN_ALLOW;
	}

	if(bTemp & Authority_Mask)
		return 0x01;
	else 
		return 0x00;
}


BYTE MS_GetSupported_Credential_Number(BYTE Selected)
{
	/* 저장 가능한 수를 각 인증 수단 별로 get */
	BYTE bTemp = 0x00;

	switch (Selected)
	{
		case MANAGER_CARD_INDEX: // Manager Card 
			bTemp = Ms_Credential_Info.bManager_Card_Info[0];
		break;

		case MASTER_CARD_INDEX: // Master Card 
			bTemp = Ms_Credential_Info.bMaster_Card_Info[0];
		break;

		case USER_CARD_INDEX: // User Card
			bTemp = Ms_Credential_Info.bUser_Card_Info[0];
		break;

		case MANAGER_CODE_INDEX:  // Manager Code 
			bTemp = Ms_Credential_Info.bManager_Code_Info[0];
		break;

		case MASTER_CODE_INDEX:  // Master Code 
			bTemp = Ms_Credential_Info.bMaster_Code_Info[0];
		break;

		case USER_CODE_INDEX:  // User Code 
			bTemp = Ms_Credential_Info.bUser_Code_Info[0];
		break;

		case SUM_OF_CARD:  // Sum of Card 
			bTemp = Ms_Credential_Info.bSumofCard;
		break;

		case SUM_OF_CODE:  // Sum of Code  
			bTemp = Ms_Credential_Info.bSumofCode;
		break;
		
		default :
		break;
	}

	return bTemp;
	
}

WORD MS_GetSupported_Credential_Start_Address(BYTE Selected)
{
	return  Ms_Credential_Info.wCertification_Address[Selected];
}

void MS_Set_Selected_Item(BYTE SelectedItem)
{
	/* main menu 에서 등록 삭제를 선택 했을 때 어떤 인증 수단인지 지정 하는 함수 */
	Ms_Credential_Info.bSelected_Item = (SelectedItem == TENKEY_1) ? MANAGER_CARD_INDEX:
									(SelectedItem == TENKEY_2) ? MASTER_CARD_INDEX:
									(SelectedItem == TENKEY_3) ? USER_CARD_INDEX:
									(SelectedItem == TENKEY_4) ? MANAGER_CODE_INDEX:
									(SelectedItem == TENKEY_5) ? MASTER_CODE_INDEX:
									(SelectedItem == TENKEY_6) ? USER_CODE_INDEX:
									(SelectedItem == TENKEY_7) ? SETTING_MENU_INDEX:0xFF;
}

void MS_Set_Registered_Credential_Num(BYTE Selected , BYTE data)
{
	/* 저장할 인증 수단의 갯수 를 set */ 
	WORD bTemp = 0x00;
	
	switch(Selected)
	{
		case MANAGER_CARD_INDEX: // Manager Card 
			bTemp = MS_MANAGER_CARD_NUM;
		break;

		case MASTER_CARD_INDEX: // Master Card 
			bTemp = MS_MASTER_CARD_NUM;
		break;

		case USER_CARD_INDEX: // User Card
			bTemp = MS_USER_CARD_NUM;
		break;

		case MANAGER_CODE_INDEX:  // Manager Code 
			bTemp = MS_MANAGER_CODE_NUM;
		break;

		case MASTER_CODE_INDEX:  // Master Code 
			bTemp = MS_MASTER_CODE_NUM;
		break;

		case USER_CODE_INDEX:  // User Code 
			bTemp = MS_USER_CODE_NUM;
		break;

		default :
		break;
	}

	RomWrite(&data,bTemp,1);
}

void MS_Get_Registered_Credential_Num(BYTE Selected , BYTE* data)
{
	/* 현재 저장된 각 인증 수단의 갯수 get */
	WORD bTemp = 0x00;
	
	switch(Selected)
	{
		case MANAGER_CARD_INDEX: // Manager Card 
			bTemp = MS_MANAGER_CARD_NUM;
		break;

		case MASTER_CARD_INDEX: // Master Card 
			bTemp = MS_MASTER_CARD_NUM;
		break;

		case USER_CARD_INDEX: // User Card
			bTemp = MS_USER_CARD_NUM;
		break;

		case MANAGER_CODE_INDEX:  // Manager Code 
			bTemp = MS_MANAGER_CODE_NUM;
		break;

		case MASTER_CODE_INDEX:  // Master Code 
			bTemp = MS_MASTER_CODE_NUM;
		break;

		case USER_CODE_INDEX:  // User Code 
			bTemp = MS_USER_CODE_NUM;
		break;

		default :
		break;
	}

	RomRead(data,bTemp,1);
}


void MS_Set_Verified_Type(BYTE data)
{
	/* 인증 완료 되어 ROM 에서 읽어온 인증 종류를 set */
	Ms_Credential_Info.bVerifiedType = (data & 0xF0);
}

BYTE MS_Get_Verified_Type(void)
{
	/* 인증 완료 되어 ROM 에서 읽어온 인증 종류를 get */
	return Ms_Credential_Info.bVerifiedType;
}

BYTE MS_Get_Exclusive_Type(void)
{
	return Ms_Credential_Info.bExclusive;
}


void MS_Do_Exclusive_Function(void)
{	
	BYTE bTemp = 0x00;
	/* 이 함수를 호출 하기 전에 꼭 if 문으로 bExclusive 종류를 보고 호출 할것 */
	/* 함수의 트리거 위치가 중요 */
	
	switch (Ms_Credential_Info.bExclusive)
	{
		case MS_NO_SPECIAL_TYPE:
		case MS_NOT_USED:			
			/* 아무것도 안한다 */
		break;

		case MS_EXCLUSICE_TYPE_1:
			/* V100-F-MS LH 사양 */
			/* 관리자 카드로 motor open 을 완료시 user card 전체 삭제 및 user code 를 0000 으로 초기화 */
			if(MS_Get_Verified_Type() == CREDENTIAL_TYPE_MANAGER_CARD)
			{
				/* 여기 까지 왔으면 motor 다 열리고 manager card 로 문 열었을 때 이므로 사양 처리 */
				/* user card 초기화 */
				Ms_Credential_Info.bSelected_Item = USER_CARD_INDEX;
				AllCardDelete();

				/* 비밀 번호 초기화 */
				Ms_Credential_Info.bSelected_Item = USER_CODE_INDEX;
				MS_PinCodeDelete();

				/* 비밀 번호 "0000" set*/
				gPinInputKeyFromBeginBuf[0] = 0x00;
				gPinInputKeyFromBeginBuf[1] = 0x00;
				gPinInputKeyFromBeginBuf[2] = 0xFF; 
				gPinInputKeyFromBeginBuf[3] = 0xFF;
				gPinInputKeyFromBeginBuf[4] = 0xFF;
				gPinInputKeyCnt = 4;
				UpdatePincodeToMemory(MS_GetSupported_Credential_Start_Address(Ms_Credential_Info.bSelected_Item));
				MS_Set_Registered_Credential_Num(Ms_Credential_Info.bSelected_Item,1);
			}
 		break;

		case MS_EXCLUSICE_TYPE_2:
		case MS_EXCLUSICE_TYPE_3:			
			/* G7-MS 영남대 사양 */
			/* Master card 를 이용해 user code 를 변경 할 경우(등록 / 삭제 둘다) user card 를 초기화  */
			/* 인천대 기숙사 사양 */
			/* Master card 를 이용해  motor open 을 완료시 user card 전체 */
			if(MS_Get_Verified_Type() == CREDENTIAL_TYPE_MASTER_CARD)
			{
				/* user card 초기화 */
				Ms_Credential_Info.bSelected_Item = USER_CARD_INDEX;
				MS_Get_Registered_Credential_Num(USER_CARD_INDEX,&bTemp);
				if(bTemp) // 등록 된게 있으면 지운다 
				AllCardDelete();
			}
		break;

		case MS_EXCLUSICE_TYPE_4:
			/* 요 사양이 좀 복잡 함 ... 여기서 대응 가능 할지 의문.... */
			/* 여긴 보류 ....  등록 함수나 / 삭제 함수에서 아예 분기 처리 해야 할 듯 */
		break;			

		default :
		break;
	}

	Ms_Credential_Info.bSelected_Item = 0xFF;

}

void MS_Add_Credential_Info(BYTE* address , BYTE Number)
{
	/* ROM 에 key 값 UID 저장시 마지막 byte 에 인증 종류와 자리수를 set */
	BYTE bTemp = 0x00;
	if(Ms_Credential_Info.bSelected_Item < MANAGER_CODE_INDEX)
	{
		/* Card */
		for(bTemp = 0x00;bTemp < Number ;bTemp++)
		{
			/* Card 정보의 마지막 byte 에 권한과 자리수를 넣어서 저장 한다 */
			if(Ms_Credential_Info.bSelected_Item == MANAGER_CARD_INDEX) // Manager Card
			{
				address[((bTemp*MAX_CARD_UID_SIZE)+MAX_CARD_UID_SIZE)-1] = (CREDENTIAL_NUMBER_4 |CREDENTIAL_TYPE_MANAGER_CARD);
			}
			else if(Ms_Credential_Info.bSelected_Item == MASTER_CARD_INDEX) // Master Card
			{
				address[((bTemp*MAX_CARD_UID_SIZE)+MAX_CARD_UID_SIZE)-1] = (CREDENTIAL_NUMBER_4 |CREDENTIAL_TYPE_MASTER_CARD);
			}
			else if(Ms_Credential_Info.bSelected_Item == USER_CARD_INDEX) // User Card
			{
				address[((bTemp*MAX_CARD_UID_SIZE)+MAX_CARD_UID_SIZE)-1] = (CREDENTIAL_NUMBER_4 |CREDENTIAL_TYPE_USER_CARD);
		}
	}
	}
	else
	{
		/* Code 버퍼의 마지막 길이에서 offset 만큼 빼서 MS 사양의 자리수 값을 넣고 type 을 같이 저장 */
		*address = (Number - CODE_LENGTH_OFFSET);
		
		if(Ms_Credential_Info.bSelected_Item == MANAGER_CODE_INDEX) // Manager Code
		{
			*address |= CREDENTIAL_TYPE_MANAGER_CODE;
		}
		else if(Ms_Credential_Info.bSelected_Item == MASTER_CODE_INDEX) // Master Code
		{
			*address |= CREDENTIAL_TYPE_MASTER_CODE;		
		}
		else if(Ms_Credential_Info.bSelected_Item == USER_CODE_INDEX) // User Code
		{
			*address |= CREDENTIAL_TYPE_USER_CODE;		
	}
	}

	/* card 나 code 를 등록 할 때 등록된 인증 수단이 motor open 권한 이 있는 지 체크 하여 권한 이 있으면 factory reset flag 를 clear 한다 */
	if(MS_GetSupported_Authority((Ms_Credential_Info.bSelected_Item <<  4),MOTOR_OPEN_ALLOW))
	{
		FactoryResetClear();
	}
}


#ifdef	DDL_CFG_DIMMER
WORD MS_Make_MainMenu_LED(void)
{
	/* 지원 하는 인증 수단을 미리 알아 내여 main menu 를 만드는 함수 */
	BYTE bTemp = 0x00;
	WORD wLedswitch = (LED_KEY_7|LED_KEY_STAR|LED_KEY_SHARP);

#ifdef	DDL_CFG_RFID
	for(bTemp = 0 ; bTemp < 6 ;bTemp++)
#else 
	for(bTemp = 3 ; bTemp < 6 ;bTemp++)
#endif 
	{
		if(Ms_Credential_Info.bCredential_Info_Set_Complete & (0x1 << bTemp))
			wLedswitch |= (1<<(bTemp+1)); // LED_KEY_1
	}

#if 0 /* 일단 연동기 관련은 menu 사용 못하게 */ 
	if(PackConnectedCheck() || InnerPackConnectedCheck())
	{
		wLedswitch |= (LED_KEY_8 | LED_KEY_9);
	}
#endif 

	return wLedswitch;
}
#endif 

#endif 

