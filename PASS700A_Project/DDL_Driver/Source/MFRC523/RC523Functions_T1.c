
#include "main.h"
#ifdef DDL_CFG_RFID_RC523
#include "ParallelFunctions_T2.h"
#include "MX_spi_lapper.h"
#include "RC523Functions_T1.h"
#include "ISO14443AFunctions_T2.h"
#endif

#define PCD_BUFFER_256_CID_0 (0x80)
#define PCD_BUFFER_128_CID_0 (0x70)
#define PCD_BUFFER_64_CID_0  (0x50)
#define PCD_BUFFER_32_CID_0  (0x40)
#define RATS_CMD 0xE0;
#define RATS_TIMEOUT 4733

BYTE gRfidFifoDataBuf[FIFO_MAX_SIZE];		/**< MFRC523 FIFO에 Write/Read하는 Buffer  */

BYTE gIrqTestBuf[2];
//BYTE gbRFID_IRQ_In = 0;
BYTE gbSioFrameBuf[15];


//==============================================================================
//	NXP MFRC522(523) SERIAL INTERFACE FUNCTION				
//==============================================================================

void RC523SpiDisable(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = RFID_SPI_MISO_Pin|RFID_SPI_MOSI_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
//	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(RFID_SPI_MOSI_GPIO_Port, &GPIO_InitStruct);


	GPIO_InitStruct.Pin = RFID_SPI_CLK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(RFID_SPI_CLK_GPIO_Port, &GPIO_InitStruct);
	
}

void RC523SpiEnable(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = RFID_SPI_MISO_Pin|RFID_SPI_MOSI_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(RFID_SPI_MOSI_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = RFID_SPI_CLK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(RFID_SPI_CLK_GPIO_Port, &GPIO_InitStruct);

	
}


void SetupMFRC523(void)
{
//	PortDirection(GPIOA->MODER, 6, GPIO_Mode_IN);			//P_RFID_MISO_T
//	PortDirection(GPIOB->MODER, 0, GPIO_Mode_IN);			//RFID_IRQ
//	P_RFID_EN(0);
	RfidPowerOn();
	Delay(SYS_TIMER_1MS);
	RfidPowerOff();

}

//==============================================================================
//Function:     RcByteSend
//Description:  SPI Write data to register of RC522
//Parameter:    Data - The value to be writen
//Return:       None
//==============================================================================
void	RcByteSend(BYTE bData)
{
	BYTE bTmp;
	bTmp = (BYTE)HAL_SPI_Transmit(gh_mcu_spi, &bData, 1, 1000);
	if(bTmp == HAL_OK){
		bTmp = HAL_OK ;
	}
/*
	BYTE count = 0;
	
	P_RFID_SCK(0);
	
	while(count < 8)
	{
		// MSB First
		if(bData & (0x80 >> count))
		{
			P_RFID_MOSI(1);
		}
		else
		{
			P_RFID_MOSI(0);
		}
		
		P_RFID_SCK(1);
		
		count ++;
//		NOP();

		P_RFID_SCK(0);
	}
*/
}

//==============================================================================
//Function:     RcByteSend
//Description:  SPI Write data to register of RC522
//Parameter:    None
//Return:       Data - The value to be read
//==============================================================================
BYTE RcByteRead(void)
{
	BYTE inData = 0;
	HAL_SPI_Receive(gh_mcu_spi, &inData , 1, 1000);
/*
	BYTE inData = 0;
	
	BYTE count = 0;
	
	P_RFID_SCK(0);
        
	while(count < 8)
	{
		P_RFID_SCK(1);
		
		inData = inData << 1;
		
		if(P_RFID_MISO_T)
		{
			inData |= 0x01;
		}
		
        	// MSB First
		P_RFID_SCK(0);
		
		count ++;
	}
*/
	return inData;


}

//==============================================================================
//Function:     RcSetReg
//Description:  Write data to register of RC522
//Parameter:    RegAddr         The address of the regitster
//              RegVal          The value to be writen
//Return:       None
//==============================================================================
void RcSetReg(BYTE RegAddr, BYTE RegVal)
{
	P_RFID_NSS(0);
	
	//code the first byte
	RegAddr &= 0x3f;
	
	RegAddr = RegAddr << 1;

	RcByteSend(RegAddr);

	RcByteSend(RegVal);
	
	P_RFID_NSS(1);
}


//==============================================================================
//Function:     qRcGetReg
//Description:  Write data to register of RC522
//Parameter:    RegAddr         The address of the regitster to be readed
//Return:       The value of the specify register
//==============================================================================
BYTE RcGetReg(BYTE RegAddr)
{
	BYTE RegVal;

	P_RFID_NSS(0);

	RegAddr = RegAddr & 0x3f;					//code the first byte
	RegAddr = RegAddr << 1;
	RegAddr = RegAddr | 0x80;

	RcByteSend(RegAddr);							// Address write
	
	RegVal = RcByteRead();							// data = 00|DATA

	P_RFID_NSS(1);

	return RegVal;
}
//------------------------------------------------------------------------------
/** 	@brief	IC EN on & Initialization
	@param 	None 	
	@return 	None 
	@remark TRF7970 EN pin high, EN2 pin은 항상 low
	@remark RF 출력을 ON하기 위해서는 별도의 RfidTxOn 함수를 수행해야 함
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void RfidPowerOn(void)
{
	P_RFID_EN(1);
	P_RFID_NSS(1);

	Delay(SYS_TIMER_1MS);				// wait until system clock started = start time of oscillator and MFRC523 stable time. MFRC523 stable time is 37.74us and oscillator start time is defined by crystal.

	RcSetReg(JREG_COMMAND,0x0F);		// 0x01 Softreset
}

//------------------------------------------------------------------------------
/** 	@brief	IC EN off
	@param 	None 	
	@return 	None 
	@remark TRF7970 EN pin low, EN2 pin은 항상 low, Complete power down
	@remark RF 출력도 자동으로 OFF됨
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void RfidPowerOff(void)
{
	BYTE bTmp;

	bTmp = RcGetReg(JREG_COMMAND);		//enter power down mode
	bTmp |= 0x10;
	RcSetReg(JREG_COMMAND, bTmp);

	P_RFID_NSS(1);
	P_RFID_EN(0);
}


//------------------------------------------------------------------------------
/** 	@brief	IC RF On
	@param 	None 	
	@return 	None 
	@remark TRF7970 RF 출력 On
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void RfidTxOn(void)
{
	RcSetReg(JREG_COMMAND, 0x0f);						//reset the RC522
	RcSetReg(JREG_TXASK, 0x40); 					//force to 100% ASK
							//ADDIQ = 10b; FixIQ = 1; RFU = 0; TauRcv = 11b; TauSync = 01b
	RcSetReg(JREG_DEMOD, 0x6D);
							//do settings common for all functions
	RcSetReg(JREG_RXTRESHOLD, 0x84);		//MinLevel = 5; CollLevel = 5
	
	RcSetReg(JREG_GSN, 0xF4);			//CWGsN = 0xF; ModGsN = 0x4
	
	RcSetReg(JREG_WATERLEVEL, 0x1A);

	RcSetReg(JREG_TXCONTROL, 0x83); 		//JBIT_TX2RFEN|JBIT_TX1RFEN

	RcSetReg(JREG_RFCFG, 0x30);	                // Rx DB 감도를 낮추면 좀더 안정적으로 동작하는 것으로 보임.

}


//------------------------------------------------------------------------------
/** 	@brief	IC RF Off
	@param 	None 	
	@return 	None 
	@remark TRF7970 RF 출력 Off
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void RfidTxOff(void)
{
	RcSetReg(JREG_TXCONTROL, 0x80); 
}


void IrqClear(void)
{
	RcSetReg(JREG_COMMIRQ, 0x7F);

}

//------------------------------------------------------------------------------
/** 	@brief	IRQ Check
	@param 	None 	
	@return 	[IRQ_NONE] : IRQ 미수신 또는 FIFO 관련 Warning이나 TX end 발생했을 경우 
	@return 	[IRQ_RX_COMPLETED] : RX start이고, Collision 에러나, 기타 Error가 발생하지 않았을 경우
	@return 	[IRQ_COLLISION_ERROR] : Collision error 발생 
	@return 	[IRQ_ERROR] : 기타 Error가 발생했을 경우 
	@remark IRQ_STATUS register 값을 확인하여 TX/RX 상태 확인
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
BYTE IrqCheck(void)
{
	BYTE IrqStatus[2];
		
	// IRQ Interrupt 발생할 경우에만 수행
	if(gbRFID_IRQ_In)
	{
		gbRFID_IRQ_In = 0;
		IrqStatus[0] = RcGetReg(JREG_COMMIRQ);
		IrqStatus[1] = RcGetReg(JREG_ERROR);
		RcSetReg(JREG_COMMIRQ, 0x7F);
		gIrqTestBuf[0] = IrqStatus[0];		gIrqTestBuf[1] = IrqStatus[1];
		// Collision Error 발생
		if(IrqStatus[1] & 0x08)
		{
			return IRQ_COLLISION_ERROR;	
		}	
		// Collision Error를 제외한 다른 Error 발생
		else if(IrqStatus[1] != 0x00)
		{
			return IRQ_ERROR;
		}
		// RX Start
		else if(IrqStatus[0] & 0x20) 
		{
			return IRQ_RX_COMPLETED;
		}
		else if(IrqStatus[0] & 0x40) 
		{
			return IRQ_TX_COMPLETED;
		}
	}
	// 그 외 FIFO 관련 Warning이나 TX end 발생, IRQ 미수신 등은 모두 0으로 return
	return IRQ_NONE;
}

void ISO14443A_REQA_Send(BYTE Command)
{
	//> Start Transceive:
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);		// 0x01 CommandReg - transceive

	//> Mifare Request:
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);		// 0x0A flush FIFO
//	RcSetReg(JREG_FIFODATA, ISO14443_3_REQIDL);	// 0x09 FIFO - Request code (14443-3A REQA '26')
	RcSetReg(JREG_FIFODATA, Command);	// 0x09 FIFO - Request code (14443-3A REQA '26')


//	RfidSetReg(JREG_COMMIEN, JBIT_RXI | JBIT_HIALERTI);
	RcSetReg(JREG_COMMIEN, JBIT_RXI);
	RcSetReg(JREG_DIVIEN, 0x80);

	RcSetReg(JREG_BITFRAMING, 0x87);		// 0x0D BitframingReg - StartSend, TxLastBits

/*	
//------------------------------------------------------------------------------ initialize
	RcSetReg(JREG_COMMIEN, JBIT_RXI);

	RcSetReg(JREG_DIVIEN, 0x80);
	
	RcSetReg(JREG_COLL, JBIT_VALUESAFTERCOLL);		//active values after coll
	RcSetReg(JREG_BITFRAMING, REQUEST_BITS);
	
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);	  //0c
	RcSetReg(JREG_FIFODATA, Command);

//	RcSetReg(JREG_TXCONTROL, 0x82); 		//JBIT_TX2RFEN
	RcSetReg(JREG_TXCONTROL, 0x83); 		//JBIT_TX2RFEN|JBIT_TX1RFEN

	RcSetReg(JREG_BITFRAMING, 0x87);	  //0c
*/
}

BYTE ISO14443A_REQA_Respond(BYTE *UidSize)
{
	BYTE bTmp = 0;

	//read number of bytes received (used for error check and correct transaction

	bTmp = RcGetReg(JREG_ERROR);
	if(bTmp != 0){
		return STATUS_FAIL; 	   
	}
	else{
		bTmp = RcGetReg(JREG_FIFOLEVEL);
		
		if(bTmp != 0x02){
			return STATUS_FAIL; 	   
		}else{
			gRfidFifoDataBuf[0] = RcGetReg(JREG_FIFODATA);
			gRfidFifoDataBuf[1] = RcGetReg(JREG_FIFODATA);
		}			
	}
	
	*UidSize = UidSizeCheck(gRfidFifoDataBuf[0]);

	return STATUS_SUCCESS;

}

void ISO14443A_HALT_Send(void)
{
//------------------------------------------------------------------------------ initialize
	RcSetReg(JREG_TXMODE, JBIT_CRCEN);
	RcSetReg(JREG_COMMIEN, JBIT_RXI);

	RcSetReg(JREG_DIVIEN, 0x80);
	
	RcSetReg(JREG_COLL, JBIT_VALUESAFTERCOLL);		//active values after coll
	RcSetReg(JREG_BITFRAMING, REQUEST_BITS);
	
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);	  //0c
	RcSetReg(JREG_FIFODATA, HALTA_CMD);
	RcSetReg(JREG_FIFODATA, 0x00);

//	RcSetReg(JREG_TXCONTROL, 0x82); 		//JBIT_TX2RFEN
	RcSetReg(JREG_TXCONTROL, 0x83); 		//JBIT_TX2RFEN|JBIT_TX1RFEN

	RcSetReg(JREG_BITFRAMING, 0x80);	  //0c
}


BYTE ISO14443A_Anticollision_Send(void)
{
	BYTE bCnt;
	BYTE Length;
	BYTE CollisionByteCnt;


	RcSetReg(JREG_COLL, 0);


	Length = 2;

	// 깨진 Bit가 발생한 Byte Count Load
//	CollisionByteCnt = (gIso14443aNVB >> 4)-2;
	CollisionByteCnt = (gIso14443aNVB - 0x20)/8;

	if(CollisionByteCnt > 4)
	{
		// 처리 범위를 넘어서기 때문에 바로 Select 처리
		return (STATUS_FAIL);
	}

	gRfidFifoDataBuf[0] = gIso14443aCascadeLevel;		// can be 0x93, 0x95 or 0x97
	gRfidFifoDataBuf[1] = gIso14443aNVB; 				// number of valid bits

	//깨진 byte, 깨진 bit 다음까지의 Data 저장
	for(bCnt = 0; bCnt < CollisionByteCnt; bCnt++)
	{
		gRfidFifoDataBuf[2+bCnt] = gIso14443aTempUidBuf[bCnt]; 
		Length++;
	}

	RcSetReg(JREG_TXMODE, (JBIT_106KBPS)); // 106kbps . 424kbps
	RcSetReg(JREG_RXMODE, (JBIT_106KBPS)|0x08);

	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);		

	for(bCnt = 0; bCnt < Length; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
	IrqClear();

	RcSetReg(JREG_BITFRAMING, 0x80);

	return (STATUS_SUCCESS);
}

BYTE ISO14443A_Anticollision_Respond(BYTE bStatus)
{
	BYTE bCnt;
	BYTE FifoLength;
	BYTE bMask;
	BYTE CollisionPosition;
	BYTE CollisionByteCnt;
	BYTE CollisionBitCnt;
	
	if(bStatus == IRQ_RX_COMPLETED)
	{
		//No Collision
		if(gIsoCollisionSet)
		{					
			//카드가 2장 이상 들어와서 Data를 모두 읽었을 때의 처리
			CollisionByteCnt = (gIso14443aNVB - 0x20)/8;
			CollisionBitCnt = (((gIso14443aNVB - 0x20)-1)%8);
	
			if((CollisionByteCnt == 0) || (CollisionByteCnt > 4))
			{
				return (STATUS_FAIL);
			}

			//최대 수신 Byte = 5 Byte (UID(4)+BCC(1))
			FifoLength = 0x05 - (CollisionByteCnt-1);		

			if(FifoLength == 0)
			{
				return (STATUS_FAIL);
			}
			for(bCnt=0;bCnt<FifoLength;bCnt++){
				gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
			}
			if(CollisionByteCnt)
			{
				CollisionPosition = CollisionByteCnt-1;

//				bMask = MakeMaskUnbrokenBit(CollisionBitCnt);
//				gIso14443aTempUidBuf[bCollisionPosition] |= (gRfidFifoDataBuf[bCollisionPosition] & bMask);

				//위와 같이 별도의 Mask Data를 만들어서 OR해도 되지만, 
				//실제 깨진 Bit 이후부터 Data가 들어오기 때문에 
				//아래와 같이 그냥 OR만 해도 문제 없음.
				gIso14443aTempUidBuf[CollisionPosition] |= gRfidFifoDataBuf[CollisionPosition];
			}

			for(bCnt = CollisionByteCnt; bCnt < 5; bCnt++)
			{
				gIso14443aTempUidBuf[bCnt] = gRfidFifoDataBuf[bCnt];
			}
		}
		else
		{
			FifoLength = RcGetReg(JREG_FIFOLEVEL);
			FifoLength &= 0x3F;				//How many bytes loaded in FIFO were not read out yet		MFRC523 have 64byte fifo
			
			if(FifoLength >= 5)		//For TRF7970
			{
				//Anticollision의 경우 모두 수신했을 경우가 5 Byte임
				FifoLength = 5;
				for(bCnt = 0; bCnt < FifoLength; bCnt++)
				{
					gIso14443aTempUidBuf[bCnt] = RcGetReg(JREG_FIFODATA);
				}
			}
		}

		if(isUidBccOk(gIso14443aTempUidBuf) == STATUS_SUCCESS)
		{
			return (STATUS_SUCCESS);
		}
		else
		{
			return (STATUS_FAIL);
		}
	}
	else if(bStatus == IRQ_COLLISION_ERROR)
	{	
		//Collision error found
		gIsoCollisionSet = 1;

		CollisionPosition = RcGetReg(JREG_COLL);
		if(CollisionPosition&0x20){		// collPosNotValid... the position of the collision is out of the range of CollPos[4:0]
			return (STATUS_FAIL);
		}
		CollisionPosition &= 0x1F;

		gIso14443aNVB += CollisionPosition;			// NVB := .‘20.’ + coll

		// 정상적인 경우가 아닐 경우 FAIL 처리
		if(CollisionPosition > 31) 
		{
			return (STATUS_FAIL);
		}
		else if(CollisionPosition == 0) CollisionPosition = 32;				// 0인 경우 32th bit에서 coll발생한것임.
		// 2. Splite Bit count save
		// 3. Next NVB save

	
		// 4. FIFO Read
		//Anticollision이 발생한 Byte까지 Read
		for(bCnt = 0; bCnt < (CollisionPosition/8); bCnt++)
		{
			gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
		}

		// 5. Save Get UID
		for(bCnt = 0; bCnt < (CollisionPosition/8); bCnt++)
		{
			//Anticollision이 발생한 Byte까지의 Data 저장
			gIso14443aTempUidBuf[bCnt] = gRfidFifoDataBuf[bCnt];
		}

		if(CollisionBitCnt)
		{
			//Anticollision이 발생한 Byte의 Anticollision이 발생한 bit까지의 Data 저장
			bMask = MakeMaskBrokenBit((CollisionPosition-1)%8);
		
			gIso14443aTempUidBuf[(CollisionPosition/8)-1] &= bMask;	
		}
	}

	return (STATUS_PROCESSING);
}

void ISO14443A_Select_Send(void)
{
	BYTE bCnt;
	
	RcSetReg(JREG_TXMODE, JBIT_CRCEN);
	RcSetReg(JREG_RXMODE, JBIT_RXNOERR|JBIT_CRCEN);

	//prepare data stream
	gRfidFifoDataBuf[0] = gIso14443aCascadeLevel;                	
	gRfidFifoDataBuf[1] = 0x70;       
	for(bCnt = 0; bCnt < 4; bCnt++)					// ISO14443A UID
	{
		gRfidFifoDataBuf[2+bCnt] = gIso14443aTempUidBuf[bCnt];       
	}
	gRfidFifoDataBuf[2+bCnt] = (BYTE)(gIso14443aTempUidBuf[0] ^ gIso14443aTempUidBuf[1] ^ gIso14443aTempUidBuf[2] ^ gIso14443aTempUidBuf[3]); 

	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 7; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
	//do seperate action if command to be executed is transceive
	//TRx is always an endless loop, Initiator and Target must set STARTSEND.
	//set TxLastBits and RxAlign to number of bits sent
	RcSetReg(JREG_BITFRAMING, 0x80);
}

BYTE ISO14443A_Select_Respond(BYTE *sak)
{
	
	gRfidFifoDataBuf[0] = RcGetReg(JREG_ERROR);
	gRfidFifoDataBuf[1] = RcGetReg(JREG_FIFOLEVEL);
	if((gRfidFifoDataBuf[0] != 0x00) || (gRfidFifoDataBuf[1] != 0x01))
	{
		return STATUS_FAIL;
	}

	// Coding of SAK Table 9 of 14443-3
	gRfidFifoDataBuf[0] = RcGetReg(JREG_FIFODATA);
	*sak = gRfidFifoDataBuf[0];

	//SAK Check
#ifdef 	IREVO_CARD_ONLY

	// 마이페어 1K만 통과
	if(gRfidFifoDataBuf[0] == 0x08)
	{
		//UID complete
		return (SAK_UID_COMPLETE);
	}
	else if(gRfidFifoDataBuf[0] == 0x20)
	{
		//ISO14443-4 지원하는7byte 카드
		return (SAK_UID_COMPLETE);
	}
	else if((gRfidFifoDataBuf[0] & 0x24) == 0x04)
	{
		//ISO14443-4 지원하지 않는 7byte 카드
		return (SAK_UID_NOT_COMPLETE);
	}
	else if((gRfidFifoDataBuf[0] & 0x24) == 0x24)
	{
		//if(gCardUidLength == 4) // todo: Check how to control
		{
			return (SAK_UID_NOT_COMPLETE);
		}
		//else
		{
		//	return (SAK_UID_COMPLETE);
		}
}
	else
	{
		return (SAK_FAIL);
	}

#else

	// OPEN UID, 중국, 한국을 제외한 해외향은 모두 통과 되도록 수정.(해외마케팅 요청)
	if(gRfidFifoDataBuf[0] & 0x04)
	{
		// ISO14443-4 지원하지 않거나 하지 않는  7byte 카드
		return (SAK_UID_NOT_COMPLETE);
	}
	else
	{
		//UID complete
		return (SAK_UID_COMPLETE);
	}

#endif
}






BYTE gbMifairMode = 0;
BYTE gbMifairProcessResult = 0;
WORD gMifairPrcsTimer2ms = 0;


BYTE glCardKeyA[6];			//keyA 값을 저장
BYTE glCardKeyARdmData[16];		//Block 1 의 DATA 값 저장
BYTE gbTest523 = 0;

BYTE glMifairBlock = 0;			//Card의 읽기 쓰기시에 사용하는 Block 값

BYTE* glMifairUidPoint;
BYTE* glMifairWritDataPoint;
	
BYTE GetMifairMode(void)
{
	return	gbMifairMode;
}

void ClearMifairMode(void)
{
	gbMifairMode = MIFAIR_NONE;
}

BYTE GetMifairPrsResult(void)
{
	return	gbMifairProcessResult;
}

void CardSaveKeyARdmData(BYTE * bRdmData)
{
	memcpy(glCardKeyARdmData, bRdmData, 16);
}

//==============================================================================
//Function:     CardKeyAMake
//Description:  Key A Data 를 생성
//Parameter:    bBlock     Read 할 Blcok
//Return:       glCardKeyA 에 저장함.
//==============================================================================
void CardKeyAMake(BYTE bBlock, BYTE *bUid)
{
	BYTE bTmp;
	BYTE sector;
	
	sector = bBlock / 4;
	bTmp = sector % 4;
	
	if(bBlock < 4)
	{
		glCardKeyA[0] = (0x82 + bUid[0]) ^ bUid[1];
		glCardKeyA[1] = (0x10 + bUid[0]) ^ bUid[1];
		glCardKeyA[2] = (0x90 + bUid[0]) ^ bUid[1];
		glCardKeyA[3] = (0x95 + bUid[0]) ^ bUid[1];
		glCardKeyA[4] = (0x31 + bUid[0]) ^ bUid[1];
		glCardKeyA[5] = (0x03 + bUid[0]) ^ bUid[1];
	}
	else
	{
		glCardKeyA[0] = (glCardKeyARdmData[0]  + glCardKeyARdmData[sector]) ^ bUid[bTmp];
		glCardKeyA[1] = (glCardKeyARdmData[3]  + glCardKeyARdmData[sector]) ^ bUid[bTmp];
		glCardKeyA[2] = (glCardKeyARdmData[4]  + glCardKeyARdmData[sector]) ^ bUid[bTmp];
		glCardKeyA[3] = (glCardKeyARdmData[7]  + glCardKeyARdmData[sector]) ^ bUid[bTmp];
		glCardKeyA[4] = (glCardKeyARdmData[8]  + glCardKeyARdmData[sector]) ^ bUid[bTmp];
		glCardKeyA[5] = (glCardKeyARdmData[13] + glCardKeyARdmData[sector]) ^ bUid[bTmp];
	}
}

void Authentication_Send(BYTE bBlock, BYTE *bUid)
{
	BYTE bCnt;
//	BYTE bTmp;
	
	//prepare data stream
	gRfidFifoDataBuf[0] = MIFARE_AUTHENT_A;                	
	gRfidFifoDataBuf[1] = bBlock;       
	memcpy(gRfidFifoDataBuf+2, glCardKeyA, 6);		//6 bytes key
	memcpy(gRfidFifoDataBuf+8, bUid, 4);		//4 bytes UID

	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);

	for(bCnt = 0; bCnt < 12; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}

	RcSetReg(JREG_COMMAND, JCMD_AUTHENT);      

}
BYTE Authentication_Respond(void)
{
	BYTE bTmp;
	
	bTmp = RcGetReg(JREG_STATUS2);
	if((bTmp & 0x0f) == 0x08)
	{
		return 1;
	}
	return 0;
}
void ReadBlockData_Send(BYTE bBlock)
{
	BYTE bCnt;

	gRfidFifoDataBuf[0] = MIFARE_READ;
	gRfidFifoDataBuf[1] = bBlock;

	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 2; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);

}
BYTE ReadBlockData_Response(void)
{
	BYTE bTmp;
	BYTE bCnt;
	
	bTmp = RcGetReg(JREG_ERROR);
	if(bTmp == 0){
		for(bCnt = 0; bCnt < 16; bCnt++){
			gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
		}
		return 1;
	}
	return 0;

}


void WriteBlock_Start_Send(BYTE bBlock)
{
	BYTE bCnt;
//	BYTE bTmp;
//	BYTE bTmp1;

	gRfidFifoDataBuf[0] = MIFARE_WRITE;
	gRfidFifoDataBuf[1] = bBlock;

	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 2; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);

}

void WriteBlock_Data_Send(BYTE *bWriteData)
{
	BYTE bCnt;
	
	for(bCnt = 0; bCnt < 16; bCnt++)
	{
		gRfidFifoDataBuf[bCnt] = bWriteData[bCnt];
	}

	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 16; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);


}

BYTE WriteBlock_Data_Response(void)
{
	BYTE bTmp, bTmp1;
	
	bTmp = RcGetReg(JREG_ERROR);
	if(bTmp == JBIT_CRCERR){
		bTmp = RcGetReg(JREG_FIFOLEVEL);
		bTmp1 = RcGetReg(JREG_CONTROL);
		if((bTmp == 1) && ((bTmp1&0x07) == 4)){
			bTmp = RcGetReg(JREG_FIFODATA);
			if((bTmp&0x0A) == 0x0A){
				return 1;
			}
		}
	}
	return 0;
}

void RATS_Send(void)
{
	BYTE bCnt;

    gRfidFifoDataBuf[0] = RATS_CMD;
    gRfidFifoDataBuf[1] = PCD_BUFFER_256_CID_0;
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 2; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);
}

BYTE RATS_Response(void)
{
    BYTE bTmp;
    BYTE bCnt;

    bTmp = RcGetReg(JREG_ERROR);
    if(bTmp == 0){
        for(bCnt = 0; bCnt < 6; bCnt++){
            gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
        }
        return 1;
    }
    return 0;
}

void DesfireSelectApp_Send(void)
{
	BYTE bCnt;

	// todo: Check the offset for hf data
	memcpy(gRfidFifoDataBuf, &gbSioFrameBuf[15], 10);
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 10; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);
}

BYTE DesfireSelectApp_Response(void)
{
    BYTE bTmp;
    BYTE bCnt;

    bTmp = RcGetReg(JREG_ERROR);
    if(bTmp == 0){
        for(bCnt = 0; bCnt < 3; bCnt++){
            gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
        }
        return 1;
    }
    return 0;
}

void DesfireAuthStep1_Send(void)
{
	BYTE bCnt;

	// todo: Check the offset for hf data
	memcpy(gRfidFifoDataBuf, &gbSioFrameBuf[15], 8);
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 8; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);
}

BYTE DesfireAuthStep1_Response(void)
{
    BYTE bTmp;
    BYTE bCnt;

    bTmp = RcGetReg(JREG_ERROR);
    if(bTmp == 0){
        for(bCnt = 0; bCnt < 19; bCnt++){
            gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
        }
        return 1;
    }
    return 0;
}

void DesfireAuthStep2_Send(void)
{
	BYTE bCnt;

	// todo: Check the offset for hf data
	memcpy(gRfidFifoDataBuf, &gbSioFrameBuf[15], 39);
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 39; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);
}

BYTE DesfireAuthStep2_Response(void)
{
    BYTE bTmp;
    BYTE bCnt;

    bTmp = RcGetReg(JREG_ERROR);
    if(bTmp == 0){
        for(bCnt = 0; bCnt < 19; bCnt++){
            gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
        }
        return 1;
    }
    return 0;
}

void DesfireReadFile00_Send(void)
{
	BYTE bCnt;

	// todo: Check the offset for hf data
	memcpy(gRfidFifoDataBuf, &gbSioFrameBuf[15], 14);
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 14; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);
}

BYTE DesfireReadFile00_Response(void)
{
    BYTE bTmp;
    BYTE bCnt;

    bTmp = RcGetReg(JREG_ERROR);
    if(bTmp == 0){
        for(bCnt = 0; bCnt < 19; bCnt++){
            gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
        }
        return 1;
    }
    return 0;
}

void DesfireReadFile01_Send(void)
{
	BYTE bCnt;

	// todo: Check the offset for hf data
	memcpy(gRfidFifoDataBuf, &gbSioFrameBuf[15], 14);
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 14; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);
}

BYTE DesfireReadFile01_Response(void)
{
    BYTE bTmp;
    BYTE bCnt;

    bTmp = RcGetReg(JREG_ERROR);
    if(bTmp == 0){
        for(bCnt = 0; bCnt < 19; bCnt++){
            gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
        }
        return 1;
    }
    return 0;
}

void DesfireWriteFile01Step1_Send(void)
{
	BYTE bCnt;

	// todo: Check the offset for hf data
	memcpy(gRfidFifoDataBuf, &gbSioFrameBuf[15], 30);
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 30; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);
}

BYTE DesfireWriteFile01Step1_Response(void)
{
    BYTE bTmp;
    BYTE bCnt;

    bTmp = RcGetReg(JREG_ERROR);
    if(bTmp == 0){
        for(bCnt = 0; bCnt < 11; bCnt++){
            gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
        }
        return 1;
    }
    return 0;
}

void DesfireWriteFile01Step2_Send(void)
{
	BYTE bCnt;

	// todo: Check the offset for hf data
	memcpy(gRfidFifoDataBuf, &gbSioFrameBuf[15], 6);
	RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
	RcSetReg(JREG_COMMAND, JCMD_TRANSCEIVE);      

	for(bCnt = 0; bCnt < 6; bCnt++)
	{
		RcSetReg(JREG_FIFODATA, gRfidFifoDataBuf[bCnt]);
	}
        
	RcSetReg(JREG_BITFRAMING, 0x80);
}

BYTE DesfireWriteFile01Step2_Response(void)
{
    BYTE bTmp;
    BYTE bCnt;

    bTmp = RcGetReg(JREG_ERROR);
    if(bTmp == 0){
        for(bCnt = 0; bCnt < 11; bCnt++){
            gRfidFifoDataBuf[bCnt] = RcGetReg(JREG_FIFODATA);
        }
        return 1;
    }
    return 0;
}

void GotoMifairAuthenticaion(BYTE bBlock, BYTE *bUidBuf)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;
	
	glMifairBlock = bBlock;
	glMifairUidPoint = bUidBuf;

	gbMifairMode = MIFAIR_AUTH_SEND;
}

void GotoMifairReadBlock(BYTE bBlock)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;
	
	glMifairBlock = bBlock;

	gbMifairMode = MIFAIR_READ_BLOCK_SEND;	
}

void GotoMifairWriteBlock(BYTE bBlock, BYTE *bDataBuf)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;
	
	glMifairBlock = bBlock;
	glMifairWritDataPoint = bDataBuf;

	gbMifairMode = MIFAIR_WRITE_BLOCK_START;	
}

void GotoMifairRATS(void)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;
	
	gbMifairMode = MIFAIR_RATS_SEND;
}

void GotoMifairDesfireSelectApp(void)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;

	gbMifairMode = MIFAIR_DESFIRE_SELECT_APP_SEND;
}

void GotoMifairDesfireAuthStep1(void)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;

	gbMifairMode = MIFAIR_DESFIRE_AUTH_STEP1_SEND;
}

void GotoMifairDesfireAuthStep2(void)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;

	gbMifairMode = MIFAIR_DESFIRE_AUTH_STEP2_SEND;
}

void GotoMifairDesfireReadFile00(void)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;

	gbMifairMode = MIFAIR_DESFIRE_READ_FILE00_SEND;
}

void GotoMifairDesfireReadFile01(void)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;

	gbMifairMode = MIFAIR_DESFIRE_READ_FILE01_SEND;
}

void GotoMifairDesfireWriteFile01Step1(void)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;

	gbMifairMode = MIFAIR_DESFIRE_WRITE_FILE01_STEP1_SEND;
}

void GotoMifairDesfireWriteFile01Step2(void)
{
	gbMifairProcessResult = MIFAIR_PROCESSING;

	gbMifairMode = MIFAIR_DESFIRE_WRITE_FILE01_STEP2_SEND;
}

void MifairProcessTimeCounter(void)
{
	if(gMifairPrcsTimer2ms)		--gMifairPrcsTimer2ms;
}

void MifairProcess(void)
{
	BYTE bTmp;
	
	switch(gbMifairMode)
	{
		case MIFAIR_NONE:
			break;

		case MIFAIR_AUTH_SEND:
			Authentication_Send(glMifairBlock, glMifairUidPoint);
			gMifairPrcsTimer2ms = 5;
			gbMifairMode = MIFAIR_AUTH_RESPONSE;
			break;

		case MIFAIR_AUTH_RESPONSE:
			bTmp = Authentication_Respond();
			if(bTmp != 1){
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;
			break;
		
		case MIFAIR_READ_BLOCK_SEND:
			ReadBlockData_Send(glMifairBlock);
			gMifairPrcsTimer2ms = 5;
			gbMifairMode = MIFAIR_READ_BLOCK_RESPONSE;
			break;
			
		case MIFAIR_READ_BLOCK_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;				
			}	
			bTmp = ReadBlockData_Response();
			if(bTmp != 1){
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;
			break;
			
		case MIFAIR_WRITE_BLOCK_START:
			gbRFID_IRQ_In = 0;
			WriteBlock_Start_Send(glMifairBlock);
			gMifairPrcsTimer2ms = 10;
			gbMifairMode = MIFAIR_WRITE_BLOCK_SEND;
			break;
			
		case MIFAIR_WRITE_BLOCK_SEND:
			if(gbRFID_IRQ_In){
				gbRFID_IRQ_In = 0;
				RcSetReg(JREG_COMMIRQ, 0x7F);
			}
			else {
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;
			}
			WriteBlock_Data_Send(glMifairWritDataPoint);
//			gMifairPrcsTimer2ms = 10;
			gMifairPrcsTimer2ms = 50;
			gbMifairMode = MIFAIR_WRITE_BLOCK_RESPONSE;
			break;
			
		case MIFAIR_WRITE_BLOCK_RESPONSE:
			
//			if(IrqCheck() != IRQ_RX_COMPLETED)
			gbTest523 = IrqCheck();
			if( gbTest523 == IRQ_NONE)
			{
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;				
			}
			else

/*
			if(gbRFID_IRQ_In){
				gbRFID_IRQ_In = 0;
				RcSetReg(JREG_COMMIRQ, 0x7F);
			}
			else {
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;
			}
*/
			bTmp = WriteBlock_Data_Response();		
			if(bTmp != 1){
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;			
			break;

		case MIFAIR_RATS_SEND:
			IrqClear();
			RATS_Send();
			gMifairPrcsTimer2ms = 10; // todo: Check timing
			gbMifairMode = MIFAIR_RATS_RESPONSE;
			break;

		case MIFAIR_RATS_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;				
			}
			bTmp = RATS_Response();
			if(bTmp != 1){
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;
			break;

		case MIFAIR_DESFIRE_SELECT_APP_SEND:
			IrqClear();
			DesfireSelectApp_Send();
			gMifairPrcsTimer2ms = 10; // todo: Check timing
			gbMifairMode = MIFAIR_DESFIRE_SELECT_APP_RESPONSE;
			break;

		case MIFAIR_DESFIRE_SELECT_APP_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;				
			}
			bTmp = DesfireSelectApp_Response();
			if(bTmp != 1){
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;
			break;

		case MIFAIR_DESFIRE_AUTH_STEP1_SEND:
			IrqClear();
			DesfireAuthStep1_Send();
			gMifairPrcsTimer2ms = 10; // todo: Check timing
			gbMifairMode = MIFAIR_DESFIRE_AUTH_STEP1_RESPONSE;
			break;

		case MIFAIR_DESFIRE_AUTH_STEP1_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;				
			}
			bTmp = DesfireAuthStep1_Response();
			if(bTmp != 1){
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;
			break;

		case MIFAIR_DESFIRE_AUTH_STEP2_SEND:
			IrqClear();
			DesfireAuthStep2_Send();
			gMifairPrcsTimer2ms = 10; // todo: Check timing
			gbMifairMode = MIFAIR_DESFIRE_AUTH_STEP2_RESPONSE;
			break;

		case MIFAIR_DESFIRE_AUTH_STEP2_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;				
			}
			bTmp = DesfireAuthStep2_Response();
			if(bTmp != 1){
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;
			break;

		case MIFAIR_DESFIRE_READ_FILE00_SEND:
			IrqClear();
			DesfireReadFile00_Send();
			gMifairPrcsTimer2ms = 10; // todo: Check timing
			gbMifairMode = MIFAIR_DESFIRE_READ_FILE00_RESPONSE;
			break;

		case MIFAIR_DESFIRE_READ_FILE00_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;
			}
			bTmp = DesfireReadFile00_Response();
			if(bTmp != 1){
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;
			break;

		case MIFAIR_DESFIRE_READ_FILE01_SEND:
			IrqClear();
			DesfireReadFile01_Send();
			gMifairPrcsTimer2ms = 10; // todo: Check timing
			gbMifairMode = MIFAIR_DESFIRE_READ_FILE01_RESPONSE;
			break;

		case MIFAIR_DESFIRE_READ_FILE01_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;
			}
			bTmp = DesfireReadFile01_Response();
			if(bTmp != 1){
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;
			break;

		case MIFAIR_DESFIRE_WRITE_FILE01_STEP1_SEND:
			IrqClear();
			DesfireWriteFile01Step1_Send();
			gMifairPrcsTimer2ms = 10; // todo: Check timing
			gbMifairMode = MIFAIR_DESFIRE_WRITE_FILE01_STEP1_RESPONSE;
			break;

		case MIFAIR_DESFIRE_WRITE_FILE01_STEP1_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;
			}
			bTmp = DesfireWriteFile01Step1_Response();
			if(bTmp != 1){
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;
			break;

		case MIFAIR_DESFIRE_WRITE_FILE01_STEP2_SEND:
			IrqClear();
			DesfireWriteFile01Step2_Send();
			gMifairPrcsTimer2ms = 10; // todo: Check timing
			gbMifairMode = MIFAIR_DESFIRE_WRITE_FILE01_STEP2_RESPONSE;
			break;

		case MIFAIR_DESFIRE_WRITE_FILE01_STEP2_RESPONSE:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gMifairPrcsTimer2ms)	break;
				gbMifairProcessResult = MIFAIR_FAIL;
				gbMifairMode = MIFAIR_NONE;
				break;
			}
			bTmp = DesfireWriteFile01Step2_Response();
			if(bTmp != 1){
				gbMifairProcessResult = MIFAIR_FAIL;
			}
			else{
				gbMifairProcessResult = MIFAIR_SUCESS;
			}
			gbMifairMode = MIFAIR_NONE;
			break;

		default:
			break;
	}
}
