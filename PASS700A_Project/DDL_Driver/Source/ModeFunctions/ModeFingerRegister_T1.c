#define		_MODE_FINGER_REGISTER_C_
//------------------------------------------------------------------------------
/** 	@file		ModeFingerRegister_T1.c
	@version 0.1.00
	@date	2016.04.26
	@brief	FingerPrint Register Mode 
	@remark	 User Code 등록 모드
	@see	MainModeProcess_T1.c
	@see	FingerTCS4K.c
	@see	TCS4K_UART.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.26		by Youn
*/
//------------------------------------------------------------------------------

#include "Main.h"



//------------------------------------------------------------------------------
/** 	@brief	FingerPrint Register Mode Start
	@param	None
	@return 	None
	@remark 지문 등록 모드 시작
*/
//------------------------------------------------------------------------------
void	ModeGotoFingerRegister( void )
{
	gbMainMode = MODE_MENU_FINGER_REGISTER;
	gbModePrcsStep = 0;
	gbBioTimer10ms = 250;
}




void	ModeFingerRegister( void )
{
	BYTE bTmp;
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			if(gbBioTimer10ms==248){	// 고장 진단 후 처리 
				BioReTry();
			}
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if(no_fingers >= _MAX_REG_BIO){
//				FeedbackError(125, VOL_CHECK);		//(음성)no more space to memorize.
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		//(음성)no more space to memorize.
				BioOffModeClear();
				break;
			}
			else if(no_fingers==0){
//				AudioFeedback(177, gcbBuzSta, VOL_CHECK);			// (음성) 	등록하실 지운을 세번 입력하세요.
				AudioFeedback(AVML_ENTER_FINGER_3TIME, gcbBuzSta, VOL_CHECK);			// (음성) 	등록하실 지운을 세번 입력하세요.
			}
			else{
//				AudioFeedback((70+no_fingers), gcbBuzSta, VOL_CHECK);	//(음성) n개의 지문이 등록 되었습니다. 등록하실 지문을 차례로 세번 입력하세요.
				AudioFeedback((AVML_FINGER01_REG_MSG-1+no_fingers), gcbBuzSta, VOL_CHECK);	//(음성) n개의 지문이 등록 되었습니다. 등록하실 지문을 차례로 세번 입력하세요.
			}
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
			FPMotorCoverAction(_OPEN);
#endif
			BioModuleON();
			LedModeRefresh();

			FingerReadBuff[0] = 0;		// 1의 자리
			FingerReadBuff[1] = 0;		// 10의 자리
			FingerReadBuff[2] = 0;		// 두자리 카운트
			FingerReadBuff[3] = 0;		// 임시 값 저장

			if(no_fingers == 0)
			{
			}
			else if(no_fingers < 10)
			{
				FingerReadBuff[0] = no_fingers;
			}
			else 
			{
				FingerReadBuff[1] = no_fingers / 10;
				FingerReadBuff[0] = no_fingers % 10;
			}			
			gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
			gbModePrcsStep ++;
			break;
			
		case 1:
			if(FingerReadBuff[2] >= 2)
			{
				FingerReadBuff[0] = 0;		// 1의 자리
				FingerReadBuff[1] = 0;		// 10의 자리
				FingerReadBuff[2] = 0;		// 두자리 카운트
				FingerReadBuff[3] = 0;		// 임시 값 저장
				gbModePrcsStep = 3;
				gbWakeUpMinTime10ms = 140;
			}
			else
			{
				if(FingerReadBuff[2] == 0)
					FingerReadBuff[3] = FingerReadBuff[1];
				else
					FingerReadBuff[3] = FingerReadBuff[0];

				LedPWLedSetting(FingerReadBuff[3]);
				gbModePrcsStep ++;
			}
			break;
		case 2:
			if(GetLedMode()) break;
			FingerReadBuff[2]++;
			gbModePrcsStep  --;
			break;
			
		case 3:
			if(gbBioTimer10ms>(BIO_WAKEUP_TM-43)) break;
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			gbFingerPTMode = FINGER_MODE_ALL_REGISTER;	
			ModeGotoFingerPTOpen();
			break;
			
		case 4:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			// ENROLL  
			gbSQNum = gbSQNum+1;					// 전송 SQ NUM 10 의한 셋팅 .
			pFingerRx = &FingerRx.PACKET.Start_st;

			FingerWriteBuff[0] = no_fingers+ 0x01;		//  TCS4K의 slot 번호는 1 부터...
			FingerWriteBuff[1] = no_fingers+ 0x01;		//  TCS4K의 slot 번호는 1 부터...

			PTENROLL_SEND( FingerWriteBuff[0], FingerWriteBuff[1], no_fingers );			// ENROLL CMD 전송 
			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;			
			break;
		case 5:		// PTENROLL SQ2
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}

			if(gfUART0RxEnd==1){
				gbModePrcsStep = 200;
				break;
			}
			if(gfUART0TxEnd){		// ENROLL CMD 전송 완료 
				gfUART0TxEnd=0;
				gfUART0RxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM+750;
				gbModePrcsStep++;
				gbBIORMCNT=0;
				gbFingerLongTimeWait100ms = 10;
				break;
			}
			break;			

		case 6: 	// PTENROLL SQ3   //   swipe check 부분
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			// FM ENROLL CMD ACK  
			if(gModeTimeOutTimer100ms == 0){
				BioModuleOff();
//				FeedbackError(101, VOL_CHECK);		//	times up
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			switch(gbInputKeyValue){				//
				case FUNKEY_OPCLOSE:
				case FUNKEY_REG:
//					AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_HIGH);//(음성) 버튼음					
					gbModePrcsStep = 201;
					break;
			}

			if(gfUART0RxEnd == 1)	{
				gfUART0RxEnd=0;

				pFingerRx = &FingerRx.PACKET.Start_st;
				gbUart6_data=4;
				gfUART0RxEnd=0;
				gfUART0TxEnd=0;
				gfBioUARTST=0;
				gfBioUARTStuff=0;
				gfBioTXStuff=0;

				pFingerRx = &FingerRx.PACKET.Start_st+1;	//리셋 에크 수신 CMD 부
	
				if((*pFingerRx == 0x05)&& (gbErrorCnt<2)){		// PTOPEN ACK CMD 재 수신 받을 시 
					gbModePrcsStep = 4; 	// ENROLL  
					gbErrorCnt++;
					break;
				}
				pFingerRx = &FingerRx.PACKET.Start_st+7;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx == 0xD6){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFB){
//						FeedbackError(119, VOL_CHECK);//(음성) Check the fingerprint module.
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);//(음성) Check the fingerprint module.
						BioModuleOff();
						gbBIOErrorCNT++;
						ModeClear();
						break;
					}
				}

				pFingerRx = &FingerRx.PACKET.Start_st+7;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx == 0xDF){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFF){
//						FeedbackError(119, VOL_CHECK);//(음성) Check the fingerprint module.
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);//(음성) Check the fingerprint module.
						BioModuleOff();
						gbBIOErrorCNT++;
						ModeClear();
						break;
					}
				}
				pFingerRx = &FingerRx.PACKET.Start_st+10;		//ENROLL CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx != 0x20){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+11;			//메세지 준비 CMD 부

				if(*pFingerRx != 0x01){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+15;			//각 상황  CMD 부

				bTmp = *pFingerRx;

				if(bTmp == 0){						// #define PT_GUIMSG_GOOD_IMAGE (0) 3번 등록이 완료시			
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbModePrcsStep = 9; 					// 등록 완료 부분으로 점프 ...case 9로 가기...
					LedSetting(gcbLedOff, LED_DISPLAY_OFF);
					break;
				}else if(bTmp == 0x20){ 				//#define PT_GUIMSG_GOOD_REG  (32) (각 상호아에서 정상 지문이면  나타남 )					
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbModePrcsStep = 22;
//					gbREG_CNT++;	//TCS4K로 인한 주석 체크(YMG는 원래 주석 처리 되어 있었음)							//등록 하실 지문을 입력 하세요	2회만 반복을 위한 값.
					AudioFeedback(VOICE_MIDI_FINGER, gcbBuzNum, VOL_CHECK);//(음성) 버튼음					
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					//gbBIOErrCheck = bTmp;
					gbFingerLongTimeWait100ms = 0;
					break;

				}
				else if(bTmp== 0x0c){						// #define PT_GUIMSG_PUT_FINGER (12)	1번 등록이 완료 상황	
					gbREG_CNT = 1;	//TCS4K로 인한 추가 부분						//등록 하실 지문을 입력 하세요  2회만 반복을 위한 값.
					gbBIOErrCheck = bTmp;//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){	//ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG1, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0d){ 				// 2번 등록 지문
					if(gbBIOErrCheck != bTmp){						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
//						AudioFeedback(44, gcbBuzNum, VOL_CHECK);	//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	//The Fingerprint is registered for the first time. Swipe the same fingerprint again, 
						AudioFeedback(AVML_1ST_FINGER_IN, gcbBuzNum, VOL_CHECK);	//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	//The Fingerprint is registered for the first time. Swipe the same fingerprint again, 
						gbBIOErrCheck = bTmp;						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
					}											//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
					gbREG_CNT = 2;								//TCS4K로 인한 추가 부분					//등록 하실 지문을 입력 하세요  2회만 반복을 위한 값.	
					
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){	//ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG2, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0e){ 				// 3번 등록 지문 
					if(gbBIOErrCheck != bTmp){						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
//						AudioFeedback(70, gcbBuzNum, VOL_CHECK); 	//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)// The Fingerprint is registered for the second time. Swipe the same fingerprint again, 
						AudioFeedback(AVML_2ND_FINGER_IN, gcbBuzNum, VOL_CHECK); 	//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)// The Fingerprint is registered for the second time. Swipe the same fingerprint again, 
						gbBIOErrCheck = bTmp;						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
					}											//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
					gbREG_CNT = 3;								//TCS4K로 인한 추가 부분		//등록 하실 지문을 입력 하세요  2회만 반복을 위한 값.
					
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){	//ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG3, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0f){ 				// 3번 등록 지문 
					gbBIORMCNT++;
					if((gbBIORMCNT > 200)){
						AudioFeedback(VOICE_MIDI_ERROR, gcbBuzErr, VOL_HIGH);		// 지문 을 떼어라.
						gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
						gbBIORMCNT=0;
					}	
				}else{										// 기타 지문이 잘못 들어 오면  0x1E : 너무 적게, 0x1C 너무 ㅂ 바르게 
					AudioFeedback(VOICE_MIDI_ERROR, gcbBuzErr, VOL_CHECK);		// 지문 을 떼어라.
					gbModePrcsStep++;

					gbFingerLongTimeWait100ms = 10;

					break;
				}
				
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				
				break;
			}

			break;

		case 7: 	// PTENROLL SQ4
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			gbSQNum = gbSQNum + 1;					// 전송 SQ 1 증가 
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTCON_SEND();								// Call back contin....CMD 전송 	


			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;

		case 8: 	// PTENROLL SQ5 --> ?SQ3
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}


			if(gfUART0TxEnd){							// Call back contin....CMD 전송 	완료		
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM+750;
				gbModePrcsStep = 6;
				break;
			}
			break;
		

		case 9: 	// PTENROLL SQ6
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			gbSQNum = gbSQNum + 1;			// 등록이 완료 되어 ... ID 받기 위해 Con 명령어 보냄..
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTCON_SEND();


			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;

		case 10: 	// PTENROLL SQ7
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}


			if(gfUART0TxEnd){					// 송신 완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;

		case 11: 	// PTENROLL SQ8  END   
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			if(gfUART0RxEnd == 1)	{		
				gfUART0RxEnd=0;

				pFingerRx = &FingerRx.PACKET.Start_st+7;		// 등록 CMD
	
				if(*pFingerRx == 0xE5){
					pFingerTx = &FingerTx.PACKET.Start_st;	
					gbBioTimer10ms = BIO_WAKEUP_TM;
					//LedSetting(gcbLedErr, LED_DISPLAY_OFF);
					//gModeTimeOutTimer100ms = 300;	  //음성이 길어서 시간 늘림
					//AudioFeedback(VOICE_MIDI_ERROR, gcbBuzReg, VOL_HIGH);// The three scanned fingerprint data does not match.
					gbModePrcsStep = 100;

					break;
					
				}else if(*pFingerRx == 0x0F){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFC){
#if 1 // 등록 과정중 3번째 지문을 읽고 나서 0xFC0F error 이 나오면 그냥 error feedack 이후 modeclear 하는 걸로 
						FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		//(음성)no more space to memorize.
						BioOffModeClear();
#else 
						gbFID = 19;
						no_fingers = _MAX_REG_BIO;						// 등록 수 증가..
						RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
						RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2);	//등록 ID 저장..	
						RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
						no_fingers = FingerReadBuff[0];
						DDLStatusFlagSet(DDL_STS_FAIL_BIO);
//						AudioFeedback(1, gcbBuzReg, VOL_HIGH); 	// Full 20 fingerprints have been registered
						AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK); 	// Full 20 fingerprints have been registered
						LedSetting(gcbLedOk, LED_DISPLAY_OFF);
						BioModuleOff();
						ModeClear();
#endif 												
						break;
					}
					
				}else if(*pFingerRx != 0x00){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD ACK

				if(*pFingerRx != 0x00){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD
	
				if(*pFingerRx != 0x02){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD ACK

				if(*pFingerRx != 0x12){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+11;		//// 등록 ID
				gbFID = *pFingerRx;

				no_fingers++; 					// 등록 수 증가..
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
				RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2);	//등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, ConvertFingerprintSlotNumber(gbFID));
#endif

				RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
				no_fingers = FingerReadBuff[0];

				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;

				// 지문 등록 정보 전송
				bTmp = FingerWriteBuff[0];
				
				memset(bTmpArray, 0x00, 8);
				CopyCredentialDataForPack(bTmpArray, 8);
				PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)bTmp);

				if(no_fingers >= _MAX_REG_BIO){ 								// 20개 모두 등록시 등록 완료.
//					AudioFeedback(1, gcbBuzReg, VOL_HIGH);	// Full 20 fingerprints have been registered
					AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
					LedSetting(gcbLedOk, LED_DISPLAY_OFF);
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
					FPMotorCoverAction(_CLOSE);
#endif								
					BioModuleOff();
					ModeClear();
					break;
				}


//				FeedbackKeyStarOn(42, VOL_CHECK);	 //추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
				FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);	 //추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요

				gbModePrcsStep = 20;
				gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
				break;
			}

			break;


		case 20:			// 등록 연속  or 종료...시..
			switch(gbInputKeyValue){				//등록 버튼 눌린후 키가 입력되어 비밀번호 입력 시작

				case FUNKEY_REG:						// 등록을 종료 하려면....눌러...
					AudioFeedback(VOICE_STOP,	gcbBuzNum, VOL_HIGH);//(음성) into음 
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE		//지문 등록 완료 되자 마자 지문모터커버 닫음
					FPMotorCoverAction(_CLOSE);
#endif					
					gbModePrcsStep = 24;
					break;
				
				case TENKEY_STAR:
					// 추가 등록
					gbBioTimer10ms = 10;
					gbModePrcsStep = 0;		
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_CHECK);//(음성) //Swipe the finger print 3 times.
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(gModeTimeOutTimer100ms == 0){
//						FeedbackModeCompleted(1, VOL_CHECK);
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE		//지문 등록 완료 되자 마자 지문모터커버 닫음
						FPMotorCoverAction(_CLOSE);
#endif
						BioOffModeClear();
					}
					break;
				
				default:
//					gbTenKeyCnt=0;
					break;
			}
			break;


		case 22:
			if(GetBuzPrcsStep())	break;
//			if(gbVoiceStep) break;
			if(gfVoiceOut)	break;
			gbModePrcsStep = 7;
			gbBioTimer10ms = BIO_WAKEUP_TM;
			break;

		case 23:
			if(GetBuzPrcsStep())	break;
			if(gfVoiceOut) break;
			gbModePrcsStep = 3;
			break;

		case 24:
			if(GetBuzPrcsStep())	break;
//			FeedbackModeCompleted(1, VOL_CHECK);
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			BioOffModeClear();
			break;

		case 100:
//			if(GetLedMode()) break;
//			FeedbackError(122, VOL_CHECK); 	//The Fingerprint is not registered.
			FeedbackError(AVML_ERR_FINGER_MSG1, VOL_CHECK); 	//The Fingerprint is not registered.
			BioOffModeClear();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
//			FeedbackError(119, VOL_HIGH); //			Check the fingerprint module.
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;

			
		default:
			BioOffModeClear();
			break;

	}

}







#ifdef	BLE_N_SUPPORT
void	ModeGotoFingerRegisterByBleN( void )
{
	gbMainMode = MODE_MENU_FINGER_REGISTER_BYBLEN;
	gbModePrcsStep = 0;
	gbBioTimer10ms = 250;
}




void	ModeFingerRegisterByBleN( void )
{
	BYTE bTmp;
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			if(gbBioTimer10ms==248){	// 고장 진단 후 처리 
				BioReTry();
			}
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if(no_fingers >= _MAX_REG_BIO){
//				FeedbackError(125, VOL_CHECK);		//(음성)no more space to memorize.
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		//(음성)no more space to memorize.
				BioOffModeClear();
				break;
			}
			else if(no_fingers==0){
//				AudioFeedback(177, gcbBuzSta, VOL_CHECK);			// (음성) 	등록하실 지운을 세번 입력하세요.
				AudioFeedback(AVML_ENTER_FINGER_3TIME, gcbBuzSta, VOL_CHECK);			// (음성) 	등록하실 지운을 세번 입력하세요.
			}
			else{
//				AudioFeedback((70+no_fingers), gcbBuzSta, VOL_CHECK);	//(음성) n개의 지문이 등록 되었습니다. 등록하실 지문을 차례로 세번 입력하세요.
				AudioFeedback((AVML_FINGER01_REG_MSG-1+no_fingers), gcbBuzSta, VOL_CHECK);	//(음성) n개의 지문이 등록 되었습니다. 등록하실 지문을 차례로 세번 입력하세요.
			}
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
			FPMotorCoverAction(_OPEN);
#endif
			BioModuleON();
			LedModeRefresh();

			FingerReadBuff[0] = 0;		// 1의 자리
			FingerReadBuff[1] = 0;		// 10의 자리
			FingerReadBuff[2] = 0;		// 두자리 카운트
			FingerReadBuff[3] = 0;		// 임시 값 저장

			if(no_fingers == 0)
			{
			}
			else if(no_fingers < 10)
			{
				FingerReadBuff[0] = no_fingers;
			}
			else 
			{
				FingerReadBuff[1] = no_fingers / 10;
				FingerReadBuff[0] = no_fingers % 10;
			}			
			gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
			gbModePrcsStep ++;
			break;
			
		case 1:
			if(FingerReadBuff[2] >= 2)
			{
				FingerReadBuff[0] = 0;		// 1의 자리
				FingerReadBuff[1] = 0;		// 10의 자리
				FingerReadBuff[2] = 0;		// 두자리 카운트
				FingerReadBuff[3] = 0;		// 임시 값 저장
				gbModePrcsStep = 3;
				gbWakeUpMinTime10ms = 140;
			}
			else
			{
				if(FingerReadBuff[2] == 0)
					FingerReadBuff[3] = FingerReadBuff[1];
				else
					FingerReadBuff[3] = FingerReadBuff[0];

				LedPWLedSetting(FingerReadBuff[3]);
				gbModePrcsStep ++;
			}
			break;
		case 2:
			if(GetLedMode()) break;
			FingerReadBuff[2]++;
			gbModePrcsStep  --;
			break;
			
		case 3:
			if(gbBioTimer10ms>(BIO_WAKEUP_TM-43)) break;
			gbBioTimer10ms	= BIO_WAKEUP_TM;		//250MS 이내에 응답 없으면 ERROR

			gbFingerPTMode = FINGER_MODE_ALL_REGISTER_BYBLEN;	
			ModeGotoFingerPTOpen();
			break;
			
		case 4:
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			// ENROLL  
			gbSQNum = gbSQNum+1;					// 전송 SQ NUM 10 의한 셋팅 .
			pFingerRx = &FingerRx.PACKET.Start_st;

			FingerWriteBuff[0] = no_fingers+ 0x01;		//  TCS4K의 slot 번호는 1 부터...
			FingerWriteBuff[1] = no_fingers+ 0x01;		//  TCS4K의 slot 번호는 1 부터...

			PTENROLL_SEND( FingerWriteBuff[0], FingerWriteBuff[1], no_fingers );			// ENROLL CMD 전송 
			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;			
			break;
		case 5:		// PTENROLL SQ2
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}

			if(gfUART0RxEnd==1){
				gbModePrcsStep = 200;
				break;
			}
			if(gfUART0TxEnd){		// ENROLL CMD 전송 완료 
				gfUART0TxEnd=0;
				gfUART0RxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM+750;
				gbModePrcsStep++;
				gbBIORMCNT=0;
				gbFingerLongTimeWait100ms = 10;
				break;
			}
			break;			

		case 6: 	// PTENROLL SQ3   //   swipe check 부분
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			// FM ENROLL CMD ACK  
			if(gModeTimeOutTimer100ms == 0){
				BioModuleOff();
//				FeedbackError(101, VOL_CHECK);		//	times up
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			switch(gbInputKeyValue){				//
				case FUNKEY_OPCLOSE:
				case FUNKEY_REG:
//					AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_HIGH);//(음성) 버튼음					
					gbModePrcsStep = 201;
					break;
			}

			if(gfUART0RxEnd == 1)	{
				gfUART0RxEnd=0;

				pFingerRx = &FingerRx.PACKET.Start_st;
				gbUart6_data=4;
				gfUART0RxEnd=0;
				gfUART0TxEnd=0;
				gfBioUARTST=0;
				gfBioUARTStuff=0;
				gfBioTXStuff=0;

				pFingerRx = &FingerRx.PACKET.Start_st+1;	//리셋 에크 수신 CMD 부
	
				if((*pFingerRx == 0x05)&& (gbErrorCnt<2)){		// PTOPEN ACK CMD 재 수신 받을 시 
					gbModePrcsStep = 4; 	// ENROLL  
					gbErrorCnt++;
					break;
				}
				pFingerRx = &FingerRx.PACKET.Start_st+7;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx == 0xD6){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFB){
//						FeedbackError(119, VOL_CHECK);//(음성) Check the fingerprint module.
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);//(음성) Check the fingerprint module.
						BioModuleOff();
						gbBIOErrorCNT++;
						ModeClear();
						break;
					}
				}

				pFingerRx = &FingerRx.PACKET.Start_st+7;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx == 0xDF){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFF){
//						FeedbackError(119, VOL_CHECK);//(음성) Check the fingerprint module.
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);//(음성) Check the fingerprint module.
						BioModuleOff();
						gbBIOErrorCNT++;
						ModeClear();
						break;
					}
				}
				pFingerRx = &FingerRx.PACKET.Start_st+10;		//ENROLL CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx != 0x20){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+11;			//메세지 준비 CMD 부

				if(*pFingerRx != 0x01){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+15;			//각 상황  CMD 부

				bTmp = *pFingerRx;

				if(bTmp == 0){						// #define PT_GUIMSG_GOOD_IMAGE (0) 3번 등록이 완료시			
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbModePrcsStep = 9; 					// 등록 완료 부분으로 점프 ...case 9로 가기...
					LedSetting(gcbLedOff, LED_DISPLAY_OFF);
					break;
				}else if(bTmp == 0x20){ 				//#define PT_GUIMSG_GOOD_REG  (32) (각 상호아에서 정상 지문이면  나타남 )					
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbModePrcsStep = 22;
//					gbREG_CNT++;	//TCS4K로 인한 주석 체크(YMG는 원래 주석 처리 되어 있었음)							//등록 하실 지문을 입력 하세요	2회만 반복을 위한 값.
					AudioFeedback(VOICE_MIDI_FINGER, gcbBuzNum, VOL_CHECK);//(음성) 버튼음					
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					//gbBIOErrCheck = bTmp;
					gbFingerLongTimeWait100ms = 0;
					break;

				}
				else if(bTmp== 0x0c){						// #define PT_GUIMSG_PUT_FINGER (12)	1번 등록이 완료 상황	
					gbREG_CNT = 1;	//TCS4K로 인한 추가 부분						//등록 하실 지문을 입력 하세요  2회만 반복을 위한 값.
					gbBIOErrCheck = bTmp;//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){	//ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG1, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0d){ 				// 2번 등록 지문
					if(gbBIOErrCheck != bTmp){						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
//						AudioFeedback(44, gcbBuzNum, VOL_CHECK);	//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	//The Fingerprint is registered for the first time. Swipe the same fingerprint again, 
						AudioFeedback(AVML_1ST_FINGER_IN, gcbBuzNum, VOL_CHECK);	//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	//The Fingerprint is registered for the first time. Swipe the same fingerprint again, 
						gbBIOErrCheck = bTmp;						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
					}											//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
					gbREG_CNT = 2;								//TCS4K로 인한 추가 부분					//등록 하실 지문을 입력 하세요  2회만 반복을 위한 값.	
					
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){	//ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG2, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0e){ 				// 3번 등록 지문 
					if(gbBIOErrCheck != bTmp){						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
//						AudioFeedback(70, gcbBuzNum, VOL_CHECK); 	//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)// The Fingerprint is registered for the second time. Swipe the same fingerprint again, 
						AudioFeedback(AVML_2ND_FINGER_IN, gcbBuzNum, VOL_CHECK); 	//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)// The Fingerprint is registered for the second time. Swipe the same fingerprint again, 
						gbBIOErrCheck = bTmp;						//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
					}											//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)
					gbREG_CNT = 3;								//TCS4K로 인한 추가 부분		//등록 하실 지문을 입력 하세요  2회만 반복을 위한 값.
					
					if(gbFingerLongTimeWait100ms >= 5){
						LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms < 5){ //else if(gbFingerLongTimeWait100ms >= 0){	//ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFMREG3, LED_KEEP_DISPLAY);
						if(gbFingerLongTimeWait100ms == 0){
							gbFingerLongTimeWait100ms = 10;
						}	
					}
				}else if(bTmp == 0x0f){ 				// 3번 등록 지문 
					gbBIORMCNT++;
					if((gbBIORMCNT > 200)){
						AudioFeedback(VOICE_MIDI_ERROR, gcbBuzErr, VOL_HIGH);		// 지문 을 떼어라.
						gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
						gbBIORMCNT=0;
					}	
				}else{										// 기타 지문이 잘못 들어 오면  0x1E : 너무 적게, 0x1C 너무 ㅂ 바르게 
					AudioFeedback(VOICE_MIDI_ERROR, gcbBuzErr, VOL_CHECK);		// 지문 을 떼어라.
					gbModePrcsStep++;

					gbFingerLongTimeWait100ms = 10;

					break;
				}
				
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				
				break;
			}

			break;

		case 7: 	// PTENROLL SQ4
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			gbSQNum = gbSQNum + 1;					// 전송 SQ 1 증가 
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTCON_SEND();								// Call back contin....CMD 전송 	


			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;

		case 8: 	// PTENROLL SQ5 --> ?SQ3
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}


			if(gfUART0TxEnd){							// Call back contin....CMD 전송 	완료		
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM+750;
				gbModePrcsStep = 6;
				break;
			}
			break;
		

		case 9: 	// PTENROLL SQ6
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			gbSQNum = gbSQNum + 1;			// 등록이 완료 되어 ... ID 받기 위해 Con 명령어 보냄..
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTCON_SEND();


			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;

		case 10: 	// PTENROLL SQ7
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}


			if(gfUART0TxEnd){					// 송신 완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;

		case 11: 	// PTENROLL SQ8  END   
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			if(gfUART0RxEnd == 1)	{		
				gfUART0RxEnd=0;

				pFingerRx = &FingerRx.PACKET.Start_st+7;		// 등록 CMD
	
				if(*pFingerRx == 0xE5){
					pFingerTx = &FingerTx.PACKET.Start_st;	
					gbBioTimer10ms = BIO_WAKEUP_TM;
					//LedSetting(gcbLedErr, LED_DISPLAY_OFF);
					//gModeTimeOutTimer100ms = 300;	  //음성이 길어서 시간 늘림
					//AudioFeedback(VOICE_MIDI_ERROR, gcbBuzReg, VOL_HIGH);// The three scanned fingerprint data does not match.
					gbModePrcsStep = 100;

					break;
					
				}else if(*pFingerRx == 0x0F){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFC){
#if 1 // 등록 과정중 3번째 지문을 읽고 나서 0xFC0F error 이 나오면 그냥 error feedack 이후 modeclear 하는 걸로 
						FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK); 	//(음성)no more space to memorize.
						BioOffModeClear();
#else 
						gbFID = 19;
						no_fingers = _MAX_REG_BIO;						// 등록 수 증가..
						RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
						RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	
						RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
						no_fingers = FingerReadBuff[0];
						DDLStatusFlagSet(DDL_STS_FAIL_BIO);
//						AudioFeedback(1, gcbBuzReg, VOL_HIGH);	// Full 20 fingerprints have been registered
						AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK); 	// Full 20 fingerprints have been registered
						LedSetting(gcbLedOk, LED_DISPLAY_OFF);
						BioModuleOff();
						ModeClear();
#endif 												

						break;
					}
					
				}else if(*pFingerRx != 0x00){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD ACK

				if(*pFingerRx != 0x00){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD
	
				if(*pFingerRx != 0x02){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx++;		// 등록 CMD ACK

				if(*pFingerRx != 0x12){
					gbModePrcsStep = 200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+11;		//// 등록 ID
				gbFID = *pFingerRx;

				no_fingers++; 					// 등록 수 증가..
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
				RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2);	//등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, ConvertFingerprintSlotNumber(gbFID));
#endif

				RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
				no_fingers = FingerReadBuff[0];

				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;

				// 지문 등록 정보 전송
				bTmp = FingerWriteBuff[0];
				
				memset(bTmpArray, 0x00, 8);
				CopyCredentialDataForPack(bTmpArray, 8);
				PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)bTmp);

//				AudioFeedback(1, gcbBuzReg, VOL_HIGH);	// Full 20 fingerprints have been registered
				AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
				LedSetting(gcbLedOk, LED_DISPLAY_OFF);
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE		//지문 등록 완료시 지문모터커버 닫음
			FPMotorCoverAction(_CLOSE);
#endif			
				BioModuleOff();
				ModeClear();
				break;
			}

			break;

		case 22:
			if(GetBuzPrcsStep())	break;
//			if(gbVoiceStep) break;
			if(gfVoiceOut)	break;
			gbModePrcsStep = 7;
			gbBioTimer10ms = BIO_WAKEUP_TM;
			break;

		case 23:
			if(GetBuzPrcsStep())	break;
			if(gfVoiceOut) break;
			gbModePrcsStep = 3;
			break;

		case 24:
			if(GetBuzPrcsStep())	break;
//			FeedbackModeCompleted(1, VOL_CHECK);
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			BioOffModeClear();
			break;

		case 100:
//			if(GetLedMode()) break;
//			FeedbackError(122, VOL_CHECK); 	//The Fingerprint is not registered.
			FeedbackError(AVML_ERR_FINGER_MSG1, VOL_CHECK); 	//The Fingerprint is not registered.
			BioOffModeClear();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
//			FeedbackError(119, VOL_HIGH); //			Check the fingerprint module.
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;

			
		default:
			BioOffModeClear();
			break;

	}

}
#endif

