#ifndef		_MX_UART_LAPPER_H_
#define		_MX_UART_LAPPER_H_


#ifdef		_MX_UART_LAPPER_C_
#else
#endif


uint32_t	register_uart_txcplt_callback( UART_HandleTypeDef *huart, void (*uart_txcplt_callback)(UART_HandleTypeDef *huart) );
uint32_t	register_uart_rxcplt_callback( UART_HandleTypeDef *huart, void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) );

#endif
