//------------------------------------------------------------------------------
/** 	@file		PackFunctions_T1.h
	@brief	Communication Pack Process Functions
*/
//------------------------------------------------------------------------------


#ifndef __PACKFUNCTIONS_T1_INCLUDED
#define __PACKFUNCTIONS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


#if defined (DDL_CFG_MS)
#define	MAX_PACK_BUF_SIZE		100
#else 
#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
#define	MAX_PACK_BUF_SIZE		53 // event + source + count + legth + checksum + 16 * 3  정도로 잡음 
#else 
#define	MAX_PACK_BUF_SIZE		40
#endif 
#endif 


#include "EventLog.h"
#include "CommPack_UART.h"
#include "PackTxFunctions_T1.h"
#include "PackRxFunctions_T1.h"
#include "RemoconFunctions_T1.h"

#ifdef	BLE_N_SUPPORT
#include "BLENFunctions_T1.h"
#endif



extern FLAG PackFlag;								
#define gfPackIDChecked			PackFlag.STATEFLAG.Bit0
#define	gfPackTypeiRevo			PackFlag.STATEFLAG.Bit1
#define	gfPackZBZWRegisterd		PackFlag.STATEFLAG.Bit2
#define	gfPackTypeiRevoBleN		PackFlag.STATEFLAG.Bit3
#define	gfPackTypeZBZWModule	PackFlag.STATEFLAG.Bit4
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
#define	gfPackTypeiRevoBleN30	PackFlag.STATEFLAG.Bit5 // BLE30 protocol flag 임 device flag 아님 
#endif
#define	gfPackTypeCBABle			PackFlag.STATEFLAG.Bit6


extern BYTE gbComCnt;

extern BYTE gPackConnectionMode;
extern BYTE gbModuleMode;
extern BYTE gbModuleProtocolVersion;
extern BYTE gbEncryptionType;

extern BYTE gbPackModeTimer10ms;



void PackWakeupInitial(void);

void InitCommPackService( void );


void PackConnectionStart(void);
void PackModuleModeClear(void);
void PackSendRunApplicaionAck(void);

BYTE PackConnectedCheck(void);
void PackCheckTimeCounter(void);
void PackModeTimeCounter(void);
void SetPackModeTimeCount(BYTE SetData);
BYTE GetPackModeTimeCount(void);


void PackConnectionStartCheck(void);
void PackDisconnectionCheck(void);

void PackConnectionProcess(void);


void PackTxProcessStepStart(WORD AckWaitTime);



//	gbModuleMode
enum{
	RF_TRX_MODULE = 0x01,				//장착된 양방향 모듈, 등록장치 없음
	RF_TRX_MODULE_1WAY = 0x05,			//장착되어 단방향 등록장치 등록된 양방향 모듈
	RF_TRX_MODULE_2WAY = 0x09,			//장착되어 양방향 등록장치 등록된 양방향 모듈
};	




BYTE PackModeCheck(void);
void PackTypeVariableClear(void);



#define	PACK_TYPE_IREVO				0x01
#define	PACK_TYPE_EMEA_ZWAVE_300	0x07
#define	PACK_TYPE_IREVO_BLEN		0x11
#define	PACK_TYPE_CBA_BLE			0x13
#define	PACK_TYPE_JIG				0xFE


#define	PACK_ID_CONFIRMED			0xAB
#define	PACK_ID_NOT_CONFIRMED		0x01
#define	PACK_ID_TEST_CONFIRMED		0x02



BYTE PackTypeCheck(BYTE TypeData);
void PackDeviceCheck(void);

void PackSaveID(void);




//#define	PACK_PACKET_EVENT				0
//#define	PACK_PACKET_SOURCE			1
//#define	PACK_PACKET_COUNT				2
//#define	PACK_PACKET_LENGTH_ADDDATA	3
//#define	PACK_PACKET_ADDDATA			4
#define	PACK_F_EVENT				0
#define	PACK_F_SOURCE				1
#define	PACK_F_COUNT				2
#define	PACK_F_LENGTH_ADDDATA		3
#define	PACK_F_ADDDATA				4

#define ACK_WAIT_TIMER_INDEX			0
#define ACK_WAIT_COUNT_INDEX 			1
#define ACK_WAIT_DATA_LENGTH_INDEX		2
#define ACK_WAIT_DATA_START_INDEX		3
#define ACK_WAIT_BUFFER_SIZE			5


//#define	PACK_PACKET_LOCKTYPE			4
//#define	PACK_PACKET_ENCRYPTIONTYPE		(PACK_F_LOCKTYPE+2)	
//#define	PACK_PACKET_DEIVCEID				(PACK_F_ENCRYPTIONTYPE+1)	
//#define	PACK_PACKET_CHECKSUMTYPE		(PACK_F_DEVICEID+16)	
//#define	PACK_PACKET_RESERVED			(PACK_F_CHECKSUMTYPE+1)	
//#define	PACK_PACKET_WAKEUPTIMING		(PACK_F_RESERVED+2)		
//#define	PACK_PACKET_PROTOCOLVERSION		(PACK_F_WAKEUPTIMING+3)	
#define	PACK_F_LOCKTYPE			4
#define	PACK_F_ENCRYPTIONTYPE		(PACK_F_LOCKTYPE+2)	
#define	PACK_F_DEVICEID			(PACK_F_ENCRYPTIONTYPE+1)	
#define	PACK_F_CHECKSUMTYPE		(PACK_F_DEVICEID+16)	
#define	PACK_F_RESERVED			(PACK_F_CHECKSUMTYPE+1)	
#define	PACK_F_WAKEUPTIMING		(PACK_F_RESERVED+2)		
#define	PACK_F_PROTOCOLVERSION	(PACK_F_WAKEUPTIMING+3)	



#define	PACK_PACKET_RMC_TYPE			4
#define	PACK_PACKET_RMC_SLOTNUMBER	5



void DelayForiRevoPackModule(void);


// gPackProcessResult
enum{
	PACK_RMC_REGDEVICE_IN = 1,
	PACK_RMC_ALLREG_DEV_END,
	PACK_RMC_ALL_REMOVE_END,
	PACK_RMC_ALL_REGISTRATION_ACK,

	PACK_HCP_JOIN_UNJOIN_OK,
	PACK_HCP_JOIN_UNJOIN_FAIL,
				

};


void SetPackProcessResult(BYTE bResult);
BYTE GetPackProcessResult(void);
void PackProcessResultClear(void);



void ChangePackPinToAnalog(void);



#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
BYTE GetPackCBARegisterSet(void);
void SetPackCBARegisterSet(BYTE bValue);
#endif


#ifdef	_NEW_YALE_ACCESS_UI
#define	YALE_ACCESS_MODE_INIT			0
#define	YALE_ACCESS_MODE_REGISTERED		1
#define	YALE_ACCESS_MODE_DELETED			2

void SetYaleAccessMode(uint8_t Yale_Access_Mode);
uint8_t GetYaleAccessMode(void);
bool Is_Yale_Access_Deleted(void);
#endif	// _NEW_YALE_ACCESS_UI



#endif


