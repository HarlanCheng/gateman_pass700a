#ifndef __FEEDBACKFUNCTIONS_T1_INCLUDED        
#define __FEEDBACKFUNCTIONS_T1_INCLUDED   

#include "DefineMacro.h"
//#include "DefinePin.h"


#ifdef	DDL_CFG_DIMMER
#define	LED_NORMAL_MENU			(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_STAR|LED_KEY_SHARP)
#define	LED_ADVANCED_MENU		(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_6|LED_KEY_STAR|LED_KEY_SHARP)

#ifdef __DDL_MODEL_B2C15MIN
#define	LED_NORMAL_MENU_PACK	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_8|LED_KEY_STAR|LED_KEY_SHARP)
#define	LED_ADVANCED_MENU_PACK	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_6|LED_KEY_8|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined __DDL_MODEL_YMH70A
#define	LED_NORMAL_MENU_PACK	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_STAR|LED_KEY_SHARP)
#define	LED_ADVANCED_MENU_PACK	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_6|LED_KEY_STAR|LED_KEY_SHARP)
#else
#define	LED_NORMAL_MENU_PACK	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_8|LED_KEY_9|LED_KEY_STAR|LED_KEY_SHARP)
#define	LED_ADVANCED_MENU_PACK	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_6|LED_KEY_8|LED_KEY_9|LED_KEY_STAR|LED_KEY_SHARP)
#endif 


#if	defined (DDL_CFG_NORMAL_ONLY) || defined (DDL_CFG_ADVANCED_ONLY)
// 모드 전환 기능이 없는 제품일 경우 1번 메뉴 처리하지 않음

#if defined (DDL_CFG_LOCK_SET_TYPE1) && defined (DDL_CFG_TOUCH_OC)
	#define	LED_LOCK_SETTING_MENU	(LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE1)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_2|LED_KEY_3|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE2) && defined (DDL_CFG_TOUCH_OC)
	#define	LED_LOCK_SETTING_MENU	(LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE2)
	#define	LED_LOCK_SETTING_MENU	(LED_KEY_2|LED_KEY_3|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE3) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_4|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE3)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE4) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_4|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE4)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE5) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_4|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE5)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_5|LED_KEY_9|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE6) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE6)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_2|LED_KEY_3|LED_KEY_5|LED_KEY_9|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE7) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_2|LED_KEY_4|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE7)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_2|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE8) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_2|LED_KEY_4|LED_KEY_5 |LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE8)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_2|LED_KEY_5 |LED_KEY_STAR|LED_KEY_SHARP)
#endif

#else	// DDL_CFG_NORMAL_ONLY || DDL_CFG_NORMAL_ONLY


#if defined (DDL_CFG_LOCK_SET_TYPE1) && defined (DDL_CFG_TOUCH_OC)
	#define	LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE1)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE2) && defined (DDL_CFG_TOUCH_OC)
	#define	LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE2)
	#define	LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE3) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_4|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE3)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE4) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_4|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE4)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE5) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_4|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE5)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_5|LED_KEY_9|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE6) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE6)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_5|LED_KEY_9|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE7) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_2|LED_KEY_4|LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE7)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_2|LED_KEY_STAR|LED_KEY_SHARP)
#endif

#if defined (DDL_CFG_LOCK_SET_TYPE8) && defined (DDL_CFG_TOUCH_OC)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_2|LED_KEY_4|LED_KEY_5 |LED_KEY_STAR|LED_KEY_SHARP)
#elif defined (DDL_CFG_LOCK_SET_TYPE8)
	#define LED_LOCK_SETTING_MENU	(LED_KEY_1|LED_KEY_2|LED_KEY_5 |LED_KEY_STAR|LED_KEY_SHARP)
#endif


#endif	// DDL_CFG_NORMAL_ONLY || DDL_CFG_NORMAL_ONLY


#endif		//DDL_CFG_DIMMER



#define	FEEDBACK_ALARM_INTRUSION	0
#define	FEEDBACK_ALARM_HIGHTEMP		1
#define	FEEDBACK_ALARM_BROKEN		2



void AudioFeedback(WORD wVoiceData, WORD const* bpBuzzerData, BYTE bMode);

void FeedbackLedAllOnOff(void);
void FeedbackKeyPadLedOnBuzOnly(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackKeyPadLedOnLedOnly(void);
void FeedbackKeyPadLedOn(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackKeyStarOn(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackKeySharpOn(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackAdvancedMenuOn(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackNormalMenuOn(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackLockSettinglMenuOn(WORD wVoiceData, BYTE bVoiceMode);
void Feedback13MenuOn(WORD wVoiceData, BYTE bVoiceMode);
void Feedback139MenuOn(WORD wVoiceData, BYTE bVoiceMode);
void Feedback138MenuOn(WORD wVoiceData, BYTE bVoiceMode);
void Feedback12MenuOn(WORD wVoiceData, BYTE bVoiceMode);
void Feedback123MenuOn(WORD wVoiceData, BYTE bVoiceMode);
void Feedback1234MenuOn(WORD wVoiceData, BYTE bVoiceMode); 
void Feedback123456MenuOn(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackSelectedMenuOn(WORD wVoiceData, BYTE bVoiceMode, WORD LedSwitch);
void FeedbackError(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackModeCompletedLedOff(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackModeCompleted(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackModeCompletedKeepMode(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackModeClear(void);
void FeedbackMotorOpen(void);
void FeedbackMotorClose(void);
void FeedbackLockIn(void);
#if defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN) \
	|| defined (DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN_BY_MASTER) 
void FeedbackLockInOpenAllowState(void);
#endif 
void FeedbackLockOut(void);
void FeedbackBuzStopLedDimOff(void);
void FeedbackBuzStopLedOff(void);
void FeedbackDoorSensorDetect(void);
void FeedbackAlarm(BYTE bMode);
void FeedbackLockOutSetting(BYTE bMode);
void FeedbackTamperProofLockout(BYTE bCnt);
void FeedbackTenKeyBlink(WORD wVoiceData, BYTE bVoiceMode, BYTE bKeyVal);
void FeedbackTenKeyErrorBlink(WORD wVoiceData, BYTE bVoiceMode, BYTE bKeyVal);


void AlarmFeedbackProcess(WORD const* BuzData);

void FeedbackCredentialInput(WORD wVoiceData, BYTE Number);
void FeedbackCredentialInput_1(WORD wVoiceData, BYTE Number);
void FeedbackPincodeDisplay(BYTE KeyVal);
void FeedbackErrorNumberOn(WORD wVoiceData, BYTE bVoiceMode, BYTE Number);
void FeedbackErrorModeKeepOn(WORD wVoiceData, BYTE bVoiceMode);
void FeedbackErrorFactoryReset(WORD wVoiceData, BYTE bVoiceMode);


void FeedbackSystemError(BYTE SystemErrorMode);
void FeedbackAllCodeLockOut(void);

#ifdef	DDL_TEST_SET_FOR_CARD
void Feedback_KC_Test_Noti(void);
#endif 




#define	FEEDBACK_MODE_NONE		0x00
#define	FEEDBACK_MODE_CLEAR		0x01
#define	FEEDBACK_MODE_STAY		0x02
#define	FEEDBACK_BUZ_ONLY		0x04
#define	FEEDBACK_LED_ONLY			0x08
#define	FEEDBACK_BUZ_ONLY_LED_OFF	0x10
#define	FEEDBACK_LED_DIMMING		0x20



#define	SYSTEM_ERR_FRONT_NO_CONNECT	1
#define	SYSTEM_ERR_MEMORY_IC		4
#define	SYSTEM_ERR_TOUCH_IC			5
#define	SYSTEM_ERR_MOTOR_SENSOR		6
#define	SYSTEM_ERR_CARD_DETECT		7
#define	SYSTEM_ERR_RTC_IC			8
#define	SYSTEM_ERR_HANDING_LOCK		9
#define	SYSTEM_ERR_PACK				10
#define	SYSTEM_ERR_BIO				11 
#define	SYSTEM_ERR_BIO_REGNUM		12 






#endif

