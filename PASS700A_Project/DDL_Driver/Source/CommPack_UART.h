//------------------------------------------------------------------------------
/** 	@file	CommPack_UART.h
	@brief	UART functions for communication pack module
*/
//------------------------------------------------------------------------------


#ifndef		_COMMPACK_UART_H_
#define		_COMMPACK_UART_H_

#include "DefineMacro.h"
//#include "DefinePin.h"


#define		MCU_COMMPACK_UART		USART1


#define		COMMPACK_RX_BUFF_LENGTH		256

#define		COMMPACK_NUM_TX_BUFF		2
#define		COMMPACK_TX_BUFF_LENGTH		300


void		SetupCommPack_UART( void );
void 	OffGpioCommPack( void );
void		RegisterCommPack_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) );
void		CommPackRxInterruptRequest( void );
uint16_t	CommPack_DequeueRx( BYTE *buff, uint16_t length );
uint16_t	UartSendCommPack( BYTE *data, uint16_t length );


void CommPack_uart_rxcplt_callback(UART_HandleTypeDef *huart);
void CommPack_uart_txcplt_callback(UART_HandleTypeDef *huart);

#endif

