#ifndef		_ABOV_MSG_INDEX_H_
#define		_ABOV_MSG_INDEX_H_


#define		AVML_STOP_VOICE					0

#if		defined(AVML_MSG_T1)
#include		"abov_index_T1.h"


#define		AVML_COMPLETE					AVML_MSG_T1_001			// 완료되었습니다.
#define		AVML_SELECT_MENU				AVML_MSG_T1_002         // 메뉴 선택 / 번호를 입력하세요.
#define		AVML_IN_0410PIN_R_CARD			AVML_MSG_T1_003         // 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 카드 등록을 원하시면 카드를 리더기에 입력하세요.
#define		AVML_REG_CARD_R					AVML_MSG_T1_004         // 추가로 등록할 카드키를 차례대로 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_REG_0410PIN_R_FINGER_C		AVML_MSG_T1_005         // 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 지문 등록을 원하시면 원형 아이콘을 누르세요.
#define		AVML_MAN_PIN_SHARP				AVML_MSG_T1_006         // 비밀번호 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REGUPIN1_DELUPIN3			AVML_MSG_T1_007         // 비밀번호 등록은 1번, 비밀번호 삭제는 3번을 눌러주세요.	// Press 1 to register a user code press 3 to delete a user code
#define		AVML_IN_02SLOT_SHARP			AVML_MSG_T1_008         // 개별 비밀번호 관리를 위하여 사용자 번호 두자리를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DEL_PIN_SHARP				AVML_MSG_T1_009         // 비밀번호 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REG_VPIN_SHARP				AVML_MSG_T1_010         // 방문자 비밀번호 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_IN_VPIN_SHARP				AVML_MSG_T1_011			// 등록하실 방문자 비밀번호 네자리를 입력하세요 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REG_OPIN_SHARP				AVML_MSG_T1_012         // 일회용 비밀번호 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DEL_OPIN_SHARP				AVML_MSG_T1_013         // 일회용 비밀번호 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_OPIN_SHARP				AVML_MSG_T1_014         // 등록하실 일회용 비밀번호 네자리를 입력하세요;. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_PIN_SHARP				AVML_MSG_T1_015         // 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_FOR_DELPIN_IN_PIN_SHARP	AVML_MSG_T1_016         // 비밀번호 삭제를 원하시면 등록된 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_LOCK_SET_MODE_SHARP		AVML_MSG_T1_017         // 설정모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHGMODE1_CHGLANG2			AVML_MSG_T1_018         // 모드 변경은 1번 / 언어 변경은 2번
#define		AVML_MAN_OPIN_SHARP				AVML_MSG_T1_019         // 일회용 비밀번호 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MAN_REMOCON_SHARP			AVML_MSG_T1_020         // 리모컨 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_REGREMOCON1_DELREMOCON3	AVML_MSG_T1_021         // 리모컨 등록은 1번, 리모컨 삭제는 3번을 눌러주세요.
#define		AVML_REG_REMOCON_SHARP			AVML_MSG_T1_022         // 리모컨 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_PRESS_REMOCONSET			AVML_MSG_T1_023         // 리모컨에  있는 등록버튼을 누르세요.
#define		AVML_DEL_REMOCON_SHARP			AVML_MSG_T1_024         // 리모컨 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MAN_HOMENET_SHARP			AVML_MSG_T1_025         // 홈네트워크 등록/삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REGHOMENET1_DELHOMENET3	AVML_MSG_T1_026         // 홈네트워크 등록은 1번, 홈네트워크 삭제는 3번을 눌러주세요.
#define		AVML_REG_HOMENET_SHARP			AVML_MSG_T1_027         // 홈네트워크 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_PRESS_HOMENETREG			AVML_MSG_T1_028         // 홈네트워크를 설정대기 모드로 전환해주세요.
#define		AVML_DEL_HOMENET_SHARP			AVML_MSG_T1_029         // 홈네트워크 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_GO_ADV_MODE_SHARP			AVML_MSG_T1_030         // 마스터 모드 전환 메뉴 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_IN_0410MPIN_SHARP			AVML_MSG_T1_031         // 네자리에서 열자리의 마스터 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REIN_MPIN_R				AVML_MSG_T1_032         // 입력하신 마스터 비밀번호를 다시 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_LANG_MENU_SHARP			AVML_MSG_T1_033			// 언어 설정 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_LANG_CODE				AVML_MSG_T1_034         // 언어 설정모드 입니다. / 번호를 입력하세요.
#define		AVML_CHG_ENGLISH_SHARP			AVML_MSG_T1_035         // 영어/ 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHG_CHINESE_SHARP			AVML_MSG_T1_036         // 중국어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHG_SPANISH_SHARP			AVML_MSG_T1_037         // 스페인어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHG_PORTGUESE_SHARP		AVML_MSG_T1_038         // 포르투갈어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_COMPLETE_STAR_R			AVML_MSG_T1_039         // 완료되었습니다. / 다른 설정을 하시려면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_COMPLETE_CIRCLE_R			AVML_MSG_T1_040         // 완료되었습니다. / 다른 설정을 하시려면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.

#define		AVML_COMPLETE_ADDCIRCLE_R		AVML_MSG_T1_041         // 완료되었습니다. / 추가 등록을 원하시면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_COMPLETE_ADDSTAR_R			AVML_MSG_T1_042         // 완료되었습니다. / 추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_REGPIN1_DELPIN3			AVML_MSG_T1_043         // 비밀번호 등록은 1번, 비밀번호 삭제는 3번을 누르세요.		// Press 1 to register a code press 3 to delete a code
#define		AVML_1ST_FINGER_IN              AVML_MSG_T1_044         // 지문이 한번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_IN_0410PIN_SHARP           AVML_MSG_T1_045         // 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.		// user code
#define		AVML_CHG_MPIN_SHARP				AVML_MSG_T1_046         // 마스터 비밀번호 변경모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REIN_PIN_R					AVML_MSG_T1_047         // 입력하신 비밀번호를 다시 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_IN_MPIN_FOR_DELPIN_SHARP	AVML_MSG_T1_048         // 비밀번호 삭제를 원하시면 마스터코드를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_02SLOT_FOR_DEL_SHARP	AVML_MSG_T1_049         // 삭제를 원하시는 사용자 번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_MPIN_R					AVML_MSG_T1_050         // 마스터 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요		// Enter the master code / press the R button to complete

#define		AVML_REIN_MPIN_SHARP			AVML_MSG_T1_051         // 입력하신 마스터 비밀번호를 다시 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_COMPLETE_ADDCIRCLE_R2		AVML_MSG_T1_052         // 완료되었습니다. / 추가 삭제를 원하시면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_REG_PIN_MODE_SHARP         AVML_MSG_T1_053         // 비밀번호 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DEL_ALLCRE_SHARP           AVML_MSG_T1_054         // 전체삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_MPIN_FOR_DELALL_SHARP	AVML_MSG_T1_055         // 전체삭제를 위해서 마스터 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_GO_NOR_MODE_SHARP          AVML_MSG_T1_056         // 일반 모드 전환 메뉴 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DEL_ALLCARD_SHARP          AVML_MSG_T1_057         // 카드키 전체삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_COMPLETE_ADDSTAR_R2		AVML_MSG_T1_058         // 완료되었습니다. / 추가 삭제를 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_MAN_CARD_SHARP				AVML_MSG_T1_059         // 카드키 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REGCARD1_DELCARD3			AVML_MSG_T1_060         // 카드키 등록은 1번, 카드키 삭제는 3번을 누르세요.

#define		AVML_REG_CARD_SHARP				AVML_MSG_T1_061         // 카드키 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CMPL_REG_CARD_SHARP        AVML_MSG_T1_062         // 카드키가 등록 되었습니다. / 카드키를 카드키 리더기에 접촉해 주세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_ENTER_CARD_SHARP			AVML_MSG_T1_063         // 카드키를 카드키 리더기에 접촉해 주세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DEL_CARD_SHARP             AVML_MSG_T1_064         // 카드키 삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REG_FINGER_SHARP			AVML_MSG_T1_065         // 지문 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DEL_ALLFINGER_SHARP		AVML_MSG_T1_066         // 지문삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MAN_FINGER_SHARP           AVML_MSG_T1_067         // 지문 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REGFINGER1_DELFINGER3		AVML_MSG_T1_068         // 지문 등록은 1번, 지문 삭제는 3번을 누르세요.
#define		AVML_DEL_FINGER_SHARP			AVML_MSG_T1_069         // 지문 삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_2ND_FINGER_IN				AVML_MSG_T1_070         // 지문이 두번 입력 되었습니다. 동일 지문을 다시 입력하세요.

#define		AVML_FINGER01_REG_MSG			AVML_MSG_T1_071         // 한 개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER02_REG_MSG			AVML_MSG_T1_072         // 두개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER03_REG_MSG			AVML_MSG_T1_073         // 세개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER04_REG_MSG			AVML_MSG_T1_074         // 네개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER05_REG_MSG			AVML_MSG_T1_075         // 다섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER06_REG_MSG			AVML_MSG_T1_076         // 여섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER07_REG_MSG			AVML_MSG_T1_077         // 일곱개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER08_REG_MSG			AVML_MSG_T1_078         // 여덟개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER09_REG_MSG			AVML_MSG_T1_079         // 아홉개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER10_REG_MSG			AVML_MSG_T1_080         // 열개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.

#define		AVML_FINGER11_REG_MSG			AVML_MSG_T1_081         // 열한개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER12_REG_MSG			AVML_MSG_T1_082         // 열두개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER13_REG_MSG			AVML_MSG_T1_083         // 열세개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER14_REG_MSG			AVML_MSG_T1_084         // 열네개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER15_REG_MSG			AVML_MSG_T1_085         // 열다섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER16_REG_MSG			AVML_MSG_T1_086         // 열여섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER17_REG_MSG			AVML_MSG_T1_087         // 열일곱개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER18_REG_MSG			AVML_MSG_T1_088         // 열여덟개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER19_REG_MSG			AVML_MSG_T1_089         // 열아홉개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_FINGER20_REG_MSG			AVML_MSG_T1_090         // 스무개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.

#define		AVML_HELLO_MSG1					AVML_MSG_T1_091         // 넘버원 프라이드 게이트맨 입니다.
#define		AVML_IN_0410MPIN				AVML_MSG_T1_092         // 네자리에서 열자리의 마스터 비밀번호를 입력하세요.
//#define										AVML_MSG_T1_093         // 마스터 비밀번호가 설정되었습니다.
//#define										AVML_MSG_T1_094         // 문이 닫혀있는지 확인하세요
//#define										AVML_MSG_T1_095         // 좌수 우수 상태를 확인하고 있습니다.
//#define										AVML_MSG_T1_096         // 문이 잠겼습니다.
//#define										AVML_MSG_T1_097         // 설정이 완료 되었습니다		// installation is completed
//#define										AVML_MSG_T1_098         // 이제 도어록 사용이 가능합니다.
//#define										AVML_MSG_T1_099         // 도어록 센서에 이상이 있습니다
//#define										AVML_MSG_T1_100         // 데드볼트는 동작했으나, 문이 제대로 잠기지 않았을 수 있습니다.

#define		AVML_TIMEOUT					AVML_MSG_T1_101         // 시간이 초과되었습니다.
#define		AVML_CHG_BATTERY				AVML_MSG_T1_102         // 건전지를 교체해주세요.
#define		AVML_OPEN_MSG					AVML_MSG_T1_103         // 열렸습니다.
#define		AVML_CLOSE_MSG					AVML_MSG_T1_104         // 닫혔습니다.
#define		AVML_WRONG_NO_PIN				AVML_MSG_T1_105         // 입력하신 자릿수가 맞지 않습니다.
#define		AVML_ALREADY_USE_MPIN			AVML_MSG_T1_106         // 이미 사용하고 있는 비밀번호 입니다.		// Error / The master code is already in use
#define		AVML_ALREADY_USE_SLOT			AVML_MSG_T1_107         // 이미 사용하고 있는 사용자번호 입니다.
#define		AVML_ALREADY_USE_CARD			AVML_MSG_T1_108         // 이미 사용하고있는 카드키 입니다.
#define		AVML_ALREADY_USE_PIN			AVML_MSG_T1_109         // 이미 사용하고 있는 비밀번호 입니다.		// Error / That code is alread in use
#define		AVML_OUT_FORCE_LOCK				AVML_MSG_T1_110         // 외부강제잠금 상태 입니다.

#define		AVML_IN_FORCE_LOCK				AVML_MSG_T1_111         // 내부강제잠금 상태 입니다.
#define		AVML_ERR_NEED_0410PIN			AVML_MSG_T1_112         // 비밀번호는 네 자리에서 열 자리까지 등록이 가능합니다
#define		AVML_TAMPER_ALARM				AVML_MSG_T1_113         // 파손 경보가 발생하였습니다.
#define		AVML_INVADE_ALARM				AVML_MSG_T1_114         // 침입 경보가 발생하였습니다.
#define		AVML_FIRE_ALARM					AVML_MSG_T1_115         // 화제 경보가 발생하였습니다.
#define		AVML_MALFUNCTION_LOCK_MSG		AVML_MSG_T1_116         // 도어록이 정상적으로 작동되지 않습니다
#define		AVML_MALFUNCTION_HCP_MSG		AVML_MSG_T1_117         // 통신팩이 정상적으로 작동되지 않습니다
#define		AVML_MALFUNCTION_FPM_MSG		AVML_MSG_T1_118         // 지문인증 모듈이 정상적으로 작동되지 않습니다
#define		AVML_CHECK_FPM_MSG				AVML_MSG_T1_119         // 지문인증 모듈을 점검하세요
#define		AVML_ERR_CARD_MSG1				AVML_MSG_T1_120         // 카드가 정상적으로 등록되지 않았습니다.	// Error / the card is not registered

#define		AVML_ERR_CARD_MSG2				AVML_MSG_T1_121         // 카드가 정상적으로 등록되지 않았습니다.	// Error / the cards are not registered
#define		AVML_ERR_FINGER_MSG1			AVML_MSG_T1_122         // 지문이 정상적으로 등록되지 않았습니다.
#define		AVML_ERR_FINGER_MSG2			AVML_MSG_T1_123         // 지문인증 모듈의 이물질을 제거해 주세요.
#define		AVML_ERR_FINGER_MSG3			AVML_MSG_T1_124         // 지문센서가 훼손되었습니다.
#define		AVML_ERR_NOSPACE				AVML_MSG_T1_125         // Error / 저장공간이 부족합니다.
#define		AVML_LHANDED_MSG				AVML_MSG_T1_126         // 좌수로 설정 되었습니다
#define		AVML_RHANDED_MSG				AVML_MSG_T1_127         // 우수로 설정 되었습니다
#define		AVML_RETRY_MSG					AVML_MSG_T1_128         // 다시 시도하세요.
#define		AVML_REG_PIN_DONE				AVML_MSG_T1_129         // 비밀번호가 등록 되었습니다.
#define		AVML_REG_MPIN_DONE				AVML_MSG_T1_130         // 마스터 비밀번호가 등록 되었습니다.

#define		AVML_ALL_SET					AVML_MSG_T1_131         // 모든 설정이 완료 되었습니다.		// all set
#define		AVML_CHG_PIN_DONE_MSG			AVML_MSG_T1_132         // 비밀번호가 변경 되었습니다.
#define		AVML_DEL_PIN_DONE_MSG			AVML_MSG_T1_133         // 비밀번호가 삭제 되었습니다.
#define		AVML_DEL_ALL_FINGER_DONE_MSG	AVML_MSG_T1_134         // 등록된 모든 지문이 삭제 되었습니다.
#define		AVML_DEL_ALL_CARD_DONE_MSG		AVML_MSG_T1_135         // 등록된 모든 카드키가 삭제 되었습니다.
#define		AVML_3MIN_LOCKED_MSG			AVML_MSG_T1_136         // 3분락 상태 입니다. 3분후 정상작동 됩니다.
#define		AVML_DONE_SETTING				AVML_MSG_T1_137         // 설정이 완료 되었습니다		// done with setting
#define		AVML_3MIN_LOCKED_MSG2			AVML_MSG_T1_138         // 3분락 상태 입니다. 3분이 지난 후에 다시 사용해주세요.
#define		AVML_ERR_CANCLE_REG				AVML_MSG_T1_139         // Error / 등록이 취소 되었습니다.
#define		AVML_ERR_CANCLE_DEL				AVML_MSG_T1_140         // Error / 삭제가 취소 되었습니다.

#define		AVML_ERR_CANCLE_MENU			AVML_MSG_T1_141         // Error / 메뉴 모드가 취소 되었습니다.
#define		AVML_CHG_VOL_SHARP				AVML_MSG_T1_142         // 음량 설정 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHG_VOL_MSG				AVML_MSG_T1_143         // 고음은 1번, 저음은 2번, 무음은 3번을 누르세요
#define		AVML_VOL_HIGH_SHARP				AVML_MSG_T1_144         // 고음으로 설정 되었습니다 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_VOL_LOW_SHARP				AVML_MSG_T1_145         // 저음으로 설정 되었습니다 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_VOL_MUTE_SHARP				AVML_MSG_T1_146         // 무음으로 설정 되었습니다 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_HIGH_VOLUME				AVML_MSG_T1_147         // 고음
#define		AVML_LOW_VOLUME					AVML_MSG_T1_148         // 저음
#define		AVML_SILENT_MODE				AVML_MSG_T1_149         // 무음
#define		AVML_IS_ON						AVML_MSG_T1_150         // 으로 설정 되었습니다.

#define		AVML_IS_OFF						AVML_MSG_T1_151         // 해제되었습니다.
#define		AVML_AUTO_RELOCK_SHARP			AVML_MSG_T1_152         // 자동잠금 설정모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_TURNON1_TURNOFF3			AVML_MSG_T1_153         // 설정은 1번, 해제는 3번을 누르세요.
#define		AVML_TURNON_CONT_SHARP			AVML_MSG_T1_154         // 설정되었습니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_TURNOFF_CONT_SHARP			AVML_MSG_T1_155         // 해제되었습니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_AUTO_RELOCK_ENABLE_MSG		AVML_MSG_T1_156         // 자동잠금이 설정되었습니다.
#define		AVML_AUTO_RELOCK_DISABLE_MSG	AVML_MSG_T1_157         // 자동잠금이 해제 되었습니다
//#define										AVML_MSG_T1_158         // 좌수 우수 설정모드 입니다.
#define		AVML_COMPLETE_HANDLE_DIR		AVML_MSG_T1_159         // 정상적으로 설정되었습니다.
//#define										AVML_MSG_T1_160         // 등록된 모든 비밀번호를 사용할 수 없도록 설정된 상태 입니다.

//#define										AVML_MSG_T1_161         // 마스터 비밀번호만 사용이 가능합니다.
//#define										AVML_MSG_T1_162         // 현재 등록된 모든 비밀번호를 사용할 수 없도록 설정된 상태 입니다.
#define		AVML_HELLO_MSG2                 AVML_MSG_T1_163         // 우리집 통합경비 시스템 게이트맨입니다.
#define		AVML_REG_HCP_MODE				AVML_MSG_T1_164         // 통신팩 등록 모드 입니다.
#define		AVML_DEL_HCP_MODE				AVML_MSG_T1_165         // 통신팩 삭제 모드 입니다.
#define		AVML_ARM_OK						AVML_MSG_T1_166         // 경비가 설정 되었습니다.
#define		AVML_ARM_FAIL					AVML_MSG_T1_167         // 경비 설정이 실패하였습니다.
#define		AVML_DOORWINDOW_OPEN_ERR		AVML_MSG_T1_168         // 문 또는 창문이 열려 있습니다.
#define		AVML_SYS_ARM					AVML_MSG_T1_169         // 시스템 경비가 설정되었습니다.
#define		AVML_SYS_DISARM					AVML_MSG_T1_170         // 시스템 경비가 해제되었습니다.

#define		AVML_DISARM_FAIL				AVML_MSG_T1_171         // 경비 해제에 실패하였습니다.
#define		AVML_ALARM_STILLACT				AVML_MSG_T1_172         // 경비 기능이 여전히 작동중일 수 있습니다.
//#define										AVML_MSG_T1_173         // 연동 시스템에 연결하세요
//#define										AVML_MSG_T1_174         // 연동을 시작합니다.
//#define										AVML_MSG_T1_175         // 연동이 활성화 되었습니다.
//#define										AVML_MSG_T1_176         // 활성화에 실패 하였습니다.
#define		AVML_ENTER_FINGER_3TIME			AVML_MSG_T1_177         // 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_REG_FINGER_ERR				AVML_MSG_T1_178         // Error / 지문이 정상적으로 등록되지 않았습니다. / 다시 시도하세요.
#define		AVML_OUT_FORCE_LOCK_IS_ON		AVML_MSG_T1_179         // 외부강제잠금 상태 입니다. / 으로 설정 되었습니다.
#define		AVML_DEL_VPIN_SHARP			AVML_MSG_T1_180         // 방문자 비밀번호 삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_BLEKEY_REG					AVML_MSG_T1_181         // 블루투스키가 등록 되었습니다.
#define		AVML_REG_VPIN_CONT_SHARP		AVML_MSG_T1_182         // 방문자 비밀번호 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REG_ADDREMOCON				AVML_MSG_T1_183         // 추가로 등록할 리모콘을 차례대로 입력하세요.
//#define		                                AVML_MSG_T1_184         //
//#define		                                AVML_MSG_T1_185         //
//#define		                                AVML_MSG_T1_186         //
//#define		                                AVML_MSG_T1_187         //
//#define		                                AVML_MSG_T1_188         //
//#define		                                AVML_MSG_T1_189         //
#define		AVML_REG_01_CARD_DONE			AVML_MSG_T1_190         // 한 개 / 카드키가 등록 되었습니다.

#define		AVML_REG_02_CARD_DONE			AVML_MSG_T1_191         // 두개 / 카드키가 등록 되었습니다.
#define		AVML_REG_03_CARD_DONE			AVML_MSG_T1_192         // 세개 / 카드키가 등록 되었습니다.
#define		AVML_REG_04_CARD_DONE			AVML_MSG_T1_193         // 네개 / 카드키가 등록 되었습니다.
#define		AVML_REG_05_CARD_DONE			AVML_MSG_T1_194         // 다섯개 / 카드키가 등록 되었습니다.
#define		AVML_REG_06_CARD_DONE			AVML_MSG_T1_195         // 여섯개 / 카드키가 등록 되었습니다.
#define		AVML_REG_07_CARD_DONE			AVML_MSG_T1_196         // 일곱개 / 카드키가 등록 되었습니다.
#define		AVML_REG_08_CARD_DONE			AVML_MSG_T1_197         // 여덟개 / 카드키가 등록 되었습니다.
#define		AVML_REG_09_CARD_DONE			AVML_MSG_T1_198         // 아홉개 / 카드키가 등록 되었습니다.
#define		AVML_REG_10_CARD_DONE			AVML_MSG_T1_199         // 열개 / 카드키가 등록 되었습니다.
#define		AVML_REG_11_CARD_DONE			AVML_MSG_T1_200         // 열한개 / 카드키가 등록 되었습니다.

#define		AVML_REG_12_CARD_DONE			AVML_MSG_T1_201         // 열두개 / 카드키가 등록 되었습니다.
#define		AVML_REG_13_CARD_DONE			AVML_MSG_T1_202         // 열세개 / 카드키가 등록 되었습니다.
#define		AVML_REG_14_CARD_DONE			AVML_MSG_T1_203         // 열네개 / 카드키가 등록 되었습니다.
#define		AVML_REG_15_CARD_DONE			AVML_MSG_T1_204         // 열다섯개 / 카드키가 등록 되었습니다.
#define		AVML_REG_16_CARD_DONE			AVML_MSG_T1_205         // 열여섯개 / 카드키가 등록 되었습니다.
#define		AVML_REG_17_CARD_DONE			AVML_MSG_T1_206         // 열일곱개 / 카드키가 등록 되었습니다.
#define		AVML_REG_18_CARD_DONE			AVML_MSG_T1_207         // 열여덟개 / 카드키가 등록 되었습니다.
#define		AVML_REG_19_CARD_DONE			AVML_MSG_T1_208         // 열아홉개 / 카드키가 등록 되었습니다.
#define		AVML_REG_20_CARD_DONE			AVML_MSG_T1_209         // 스무개 / 카드키가 등록 되었습니다.
#define		AVML_REG_21_CARD_DONE			AVML_MSG_T1_210         // 스물한개 / 카드키가 등록 되었습니다.

#define		AVML_REG_22_CARD_DONE			AVML_MSG_T1_211         // 스물두개 / 카드키가 등록 되었습니다.
#define		AVML_REG_23_CARD_DONE			AVML_MSG_T1_212         // 스물세개 / 카드키가 등록 되었습니다.
#define		AVML_REG_24_CARD_DONE			AVML_MSG_T1_213         // 스물네개 / 카드키가 등록 되었습니다.
#define		AVML_REG_25_CARD_DONE			AVML_MSG_T1_214         // 스물다섯개 / 카드키가 등록 되었습니다.
#define		AVML_REG_26_CARD_DONE			AVML_MSG_T1_215         // 스물여섯개 / 카드키가 등록 되었습니다.
#define		AVML_REG_27_CARD_DONE			AVML_MSG_T1_216         // 스물일곱개 / 카드키가 등록 되었습니다.
#define		AVML_REG_28_CARD_DONE			AVML_MSG_T1_217         // 스물여덟개 / 카드키가 등록 되었습니다.
#define		AVML_REG_29_CARD_DONE			AVML_MSG_T1_218         // 스물아홉개 / 카드키가 등록 되었습니다.
#define		AVML_REG_30_CARD_DONE			AVML_MSG_T1_219         // 서른개 / 카드키가 등록 되었습니다.
#define		AVML_REG_31_CARD_DONE			AVML_MSG_T1_220         // 서른한개 / 카드키가 등록 되었습니다.

#define		AVML_REG_32_CARD_DONE			AVML_MSG_T1_221         // 서른두개 / 카드키가 등록 되었습니다.
#define		AVML_REG_33_CARD_DONE			AVML_MSG_T1_222         // 서른세개 / 카드키가 등록 되었습니다.
#define		AVML_REG_34_CARD_DONE			AVML_MSG_T1_223         // 서른네개 / 카드키가 등록 되었습니다.
#define		AVML_REG_35_CARD_DONE			AVML_MSG_T1_224         // 서른다섯개 / 카드키가 등록 되었습니다.
#define		AVML_REG_36_CARD_DONE			AVML_MSG_T1_225         // 서른여섯개 / 카드키가 등록 되었습니다.
#define		AVML_REG_37_CARD_DONE			AVML_MSG_T1_226         // 서른일곱개 / 카드키가 등록 되었습니다.
#define		AVML_REG_38_CARD_DONE			AVML_MSG_T1_227         // 서른여덟개 / 카드키가 등록 되었습니다.
#define		AVML_REG_39_CARD_DONE			AVML_MSG_T1_228         // 서른아홉개 / 카드키가 등록 되었습니다.
#define		AVML_REG_40_CARD_DONE			AVML_MSG_T1_229         // 마흔개 / 카드키가 등록 되었습니다.
#define		AVML_PRESS_SHARP_CONT			AVML_MSG_T1_230         // 계속하시려면 샵 버튼을 누르세요.

#define		AVML_IN_0410MPIN_R				AVML_MSG_T1_231         // 네자리에서 열자리의 마스터 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_MAN_HANDLING_SHARP			AVML_MSG_T1_232         // 좌수 우수 설정 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_PRESS_SHARP_AUTOHANDLE		AVML_MSG_T1_233         // 샵 버튼을 누르시면 좌수 우수가 자동으로 설정됩니다.
#define		AVML_IN_MPIN_SHARP				AVML_MSG_T1_234         // 마스터 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.		// Enter the master code / then press the hash to continue
#define		AVML_IN_0410PIN_R_FINGER_C		AVML_MSG_T1_235         // 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 지문 등록을 원하시면 *버튼을 누르세요.
//#define										AVML_MSG_T1_236         //
//#define										AVML_MSG_T1_237         //
//#define										AVML_MSG_T1_238         //
//#define										AVML_MSG_T1_239         //
//#define										AVML_MSG_T1_240         //

//#define										AVML_MSG_T1_241         //
//#define										AVML_MSG_T1_242         //
//#define										AVML_MSG_T1_243         //
//#define										AVML_MSG_T1_244         //
//#define										AVML_MSG_T1_245         //
//#define										AVML_MSG_T1_246         //
//#define										AVML_MSG_T1_247         //
//#define										AVML_MSG_T1_248         //
#define		AVML_MAN_DIGITALKEY_SHARP		AVML_MSG_T1_249		// AVML_MSG_T1 compile error T1 은 음성 없음 
//#define										AVML_MSG_T1_250         //
                                                                    
//#define										AVML_MSG_T1_251         //
//#define										AVML_MSG_T1_252         //
//#define										AVML_MSG_T1_253         //
//#define										AVML_MSG_T1_254         //
//#define										AVML_MSG_T1_255         //


#elif	defined(AVML_MSG_T2)
#include		"abov_index_T2.h"

#define		AVML_COMPLETE					AVML_MSG_T2_001			// 완료되었습니다.
#define		AVML_SELECT_MENU				AVML_MSG_T2_002         // 메뉴 선택 / 번호를 입력하세요.
#define		AVML_IN_0410PIN_R_CARD			AVML_MSG_T2_003         // 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 카드 등록을 원하시면 카드를 리더기에 입력하세요.
#define		AVML_REG_CARD_R					AVML_MSG_T2_004         // 추가로 등록할 카드키를 차례대로 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요

#define		AVML_MAN_PIN_SHARP				AVML_MSG_T2_006         // 비밀번호 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REGUPIN1_DELUPIN3			AVML_MSG_T2_007         // 비밀번호 등록은 1번, 비밀번호 삭제는 3번을 눌러주세요.	// Press 1 to register a user code press 3 to delete a user code
#define		AVML_IN_02SLOT_SHARP			AVML_MSG_T2_008         // 개별 비밀번호 관리를 위하여 사용자 번호 두자리를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DEL_PIN_SHARP				AVML_MSG_T2_009         // 비밀번호 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REG_VPIN_SHARP				AVML_MSG_T2_010         // 방문자 비밀번호 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_IN_VPIN_SHARP				AVML_MSG_T2_011			// 등록하실 방문자 비밀번호 네자리를 입력하세요 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REG_OPIN_SHARP				AVML_MSG_T2_012         // 일회용 비밀번호 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DEL_OPIN_SHARP				AVML_MSG_T2_013         // 일회용 비밀번호 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_OPIN_SHARP				AVML_MSG_T2_014         // 등록하실 일회용 비밀번호 네자리를 입력하세요;. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_PIN_SHARP				AVML_MSG_T2_015         // 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_FOR_DELPIN_IN_PIN_SHARP	AVML_MSG_T2_016         // 비밀번호 삭제를 원하시면 등록된 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_LOCK_SET_MODE_SHARP		AVML_MSG_T2_017         // 설정모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHGMODE1_CHGLANG2			AVML_MSG_T2_018         // 모드 변경은 1번 / 언어 변경은 2번
#define		AVML_MAN_OPIN_SHARP				AVML_MSG_T2_019         // 일회용 비밀번호 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MAN_REMOCON_SHARP			AVML_MSG_T2_020         // 리모컨 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
                                                                    
#define		AVML_REGREMOCON1_DELREMOCON3	AVML_MSG_T2_021         // 리모컨 등록은 1번, 리모컨 삭제는 3번을 눌러주세요.
#define		AVML_REG_REMOCON_SHARP			AVML_MSG_T2_022         // 리모컨 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_PRESS_REMOCONSET			AVML_MSG_T2_023         // 리모컨에  있는 등록버튼을 누르세요.
#define		AVML_DEL_REMOCON_SHARP			AVML_MSG_T2_024         // 리모컨 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MAN_HOMENET_SHARP			AVML_MSG_T2_025         // 홈네트워크 등록/삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REGHOMENET1_DELHOMENET3	AVML_MSG_T2_026         // 홈네트워크 등록은 1번, 홈네트워크 삭제는 3번을 눌러주세요.
#define		AVML_REG_HOMENET_SHARP			AVML_MSG_T2_027         // 홈네트워크 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_PRESS_HOMENETREG			AVML_MSG_T2_028         // 홈네트워크를 설정대기 모드로 전환해주세요.
#define		AVML_DEL_HOMENET_SHARP			AVML_MSG_T2_029         // 홈네트워크 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_GO_ADV_MODE_SHARP			AVML_MSG_T2_030         // 마스터 모드 전환 메뉴 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_IN_0410MPIN_SHARP			AVML_MSG_T2_031         // 네자리에서 열자리의 마스터 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REIN_MPIN_R				AVML_MSG_T2_032         // 입력하신 마스터 비밀번호를 다시 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요

#define		AVML_LANG_MENU_SHARP			AVML_MSG_T2_033			// 언어 설정 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_LANG_CODE				AVML_MSG_T2_034         // 언어 설정모드 입니다. / 번호를 입력하세요.
#define		AVML_CHG_KOREAN_SHARP			AVML_MSG_T2_035         // 한국어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHG_ENGLISH_SHARP			AVML_MSG_T2_251         // 영어  / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHG_CHINESE_SHARP			AVML_MSG_T2_252         // 중국어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHG_SPANISH_SHARP			AVML_MSG_T2_253         // 스페인어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHG_PORTGUESE_SHARP		AVML_MSG_T2_254         // 포르투갈어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHG_TAIWANESE_SHARP			AVML_MSG_T2_255         // 포르투갈어 / 계속하시려면 샵 버튼을 누르세요.

#ifdef TKEY_CIRCLE_BTN_TYPE
#define		AVML_COMPLETE_CIRCLE_R			AVML_MSG_T2_037         // 완료되었습니다. / 다른 설정을 하시려면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.		
#define		AVML_COMPLETE_ADDCIRCLE_R		AVML_MSG_T2_038         // 완료되었습니다. / 추가 등록을 원하시면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.	
#define		AVML_COMPLETE_STAR_R			AVML_MSG_T2_037         // 완료되었습니다. / 다른 설정을 하시려면 [원형 아이콘]을 누르시고, 종료를 원하시면 R버튼을 누르세요. 
#define		AVML_COMPLETE_ADDSTAR_R			AVML_MSG_T2_038         // 완료되었습니다. / 추가 등록을 원하시면 [원형 아이콘]을 누르시고, 종료를 원하시면 R버튼을 누르세요.          
#else
#define		AVML_COMPLETE_STAR_R			AVML_MSG_T2_036         // 완료되었습니다. / 다른 설정을 하시려면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_COMPLETE_CIRCLE_R			AVML_MSG_T2_037         // 완료되었습니다. / 다른 설정을 하시려면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_COMPLETE_ADDCIRCLE_R		AVML_MSG_T2_038         // 완료되었습니다. / 추가 등록을 원하시면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_COMPLETE_ADDSTAR_R			AVML_MSG_T2_039         // 완료되었습니다. / 추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#endif
#define		AVML_REGPIN1_DELPIN3			AVML_MSG_T2_040         // 비밀번호 등록은 1번, 비밀번호 삭제는 3번을 누르세요.		// Press 1 to register a code press 3 to delete a code

#define		AVML_1ST_FINGER_IN				AVML_MSG_T2_041			// 지문이 한번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_IN_0410PIN_SHARP			AVML_MSG_T2_042			// 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CHG_MPIN_SHARP				AVML_MSG_T2_043			// 마스터 비밀번호 변경모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REIN_PIN_R					AVML_MSG_T2_044			// 입력하신 비밀번호를 다시 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_IN_MPIN_FOR_DELPIN_SHARP	AVML_MSG_T2_045			// 비밀번호 삭제를 원하시면 마스터코드를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_02SLOT_FOR_DEL_SHARP	AVML_MSG_T2_046			// 삭제를 원하시는 사용자 번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_MPIN_R					AVML_MSG_T2_047			// 마스터 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요		// Enter the master code / press the R button to complete
#define		AVML_REIN_MPIN_SHARP			AVML_MSG_T2_048			// 입력하신 마스터 비밀번호를 다시 입력하세요. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_REG_PIN_MODE_SHARP         AVML_MSG_T2_050			// 비밀번호 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_DEL_ALLCRE_SHARP           AVML_MSG_T2_051			// 전체삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_MPIN_FOR_DELALL_SHARP	AVML_MSG_T2_052			// 전체삭제를 위해서 마스터 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_GO_NOR_MODE_SHARP          AVML_MSG_T2_053			// 일반 모드 전환 메뉴 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DEL_ALLCARD_SHARP          AVML_MSG_T2_054			// 카드키 전체삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#ifdef TKEY_CIRCLE_BTN_TYPE
#define		AVML_COMPLETE_ADDCIRCLE_R2		AVML_MSG_T2_049			// 완료되었습니다. / 추가 삭제를 원하시면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_COMPLETE_ADDSTAR_R2		AVML_MSG_T2_049			// 완료되었습니다. / 추가 삭제를 원하시면  [원형 아이콘]을 누르시고, 종료를 원하시면 R버튼을 누르세요.  
#else
#define		AVML_COMPLETE_ADDCIRCLE_R2		AVML_MSG_T2_049			// 완료되었습니다. / 추가 삭제를 원하시면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_COMPLETE_ADDSTAR_R2		AVML_MSG_T2_055			// 완료되었습니다. / 추가 삭제를 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#endif


#define		AVML_MAN_CARD_SHARP				AVML_MSG_T2_056			// 카드키 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REGCARD1_DELCARD3			AVML_MSG_T2_057			// 카드키 등록은 1번, 카드키 삭제는 3번을 누르세요.
#define		AVML_REG_CARD_SHARP				AVML_MSG_T2_058			// 카드키 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_CMPL_REG_CARD_SHARP        AVML_MSG_T2_059			// 카드키가 등록 되었습니다. / 카드키를 카드키 리더기에 접촉해 주세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_ENTER_CARD_SHARP			AVML_MSG_T2_060			// 카드키를 카드키 리더기에 접촉해 주세요. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_DEL_CARD_SHARP             AVML_MSG_T2_061			// 카드키 삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REG_FINGER_SHARP			AVML_MSG_T2_062			// 지문 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DEL_ALLFINGER_SHARP		AVML_MSG_T2_063			// 지문삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.(전체삭제)
#define		AVML_MAN_FINGER_SHARP			AVML_MSG_T2_064			// 지문 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REGFINGER1_DELFINGER3		AVML_MSG_T2_065			// 지문 등록은 1번, 지문 삭제는 3번을 누르세요.
#define		AVML_DEL_FINGER_SHARP			AVML_MSG_T2_066			// 지문 삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_2ND_FINGER_IN				AVML_MSG_T2_067			// 지문이 두번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_REG_01_FACE_DONE			AVML_MSG_T2_068			// 한 개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_02_FACE_DONE			AVML_MSG_T2_069			// 두개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_03_FACE_DONE           AVML_MSG_T2_070			// 세개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 

#define		AVML_REG_04_FACE_DONE           AVML_MSG_T2_071			// 네개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_05_FACE_DONE           AVML_MSG_T2_072			// 다섯개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_06_FACE_DONE           AVML_MSG_T2_073			// 여섯개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_07_FACE_DONE           AVML_MSG_T2_074			// 일곱개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_08_FACE_DONE           AVML_MSG_T2_075			// 여덟개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_09_FACE_DONE           AVML_MSG_T2_076			// 아홉개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_10_FACE_DONE           AVML_MSG_T2_077			// 열개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_11_FACE_DONE           AVML_MSG_T2_078			// 열한개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_12_FACE_DONE			AVML_MSG_T2_079			// 열두개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_13_FACE_DONE           AVML_MSG_T2_080			// 열세개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 

#define		AVML_REG_14_FACE_DONE           AVML_MSG_T2_081			// 열네개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_15_FACE_DONE           AVML_MSG_T2_082			// 열다섯개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_16_FACE_DONE           AVML_MSG_T2_083			// 열여섯개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_17_FACE_DONE           AVML_MSG_T2_084			// 열일곱개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_18_FACE_DONE           AVML_MSG_T2_085			// 열여덟개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_19_FACE_DONE           AVML_MSG_T2_086			// 열아홉개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_20_FACE_DONE           AVML_MSG_T2_087			// 스무개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_21_FACE_DONE           AVML_MSG_T2_088			// 스물한개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_22_FACE_DONE			AVML_MSG_T2_089			// 스물두개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_23_FACE_DONE           AVML_MSG_T2_090			// 스물세개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 

#define		AVML_REG_24_FACE_DONE           AVML_MSG_T2_091			// 스물네개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_25_FACE_DONE           AVML_MSG_T2_092			// 스물다섯개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_26_FACE_DONE           AVML_MSG_T2_093			// 스물여섯개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_27_FACE_DONE           AVML_MSG_T2_094			// 스물일곱개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_28_FACE_DONE           AVML_MSG_T2_095			// 스물여덟개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_29_FACE_DONE           AVML_MSG_T2_096			// 스물아홉개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_30_FACE_DONE           AVML_MSG_T2_097			// 서른개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_31_FACE_DONE           AVML_MSG_T2_098			// 서른한개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_32_FACE_DONE			AVML_MSG_T2_099			// 서른두개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_33_FACE_DONE           AVML_MSG_T2_100			// 서른세개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 

#define		AVML_REG_34_FACE_DONE           AVML_MSG_T2_101			// 서른네개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_35_FACE_DONE           AVML_MSG_T2_102			// 서른다섯개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_36_FACE_DONE           AVML_MSG_T2_103			// 서른여섯개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_37_FACE_DONE           AVML_MSG_T2_104			// 서른일곱개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_38_FACE_DONE           AVML_MSG_T2_105			// 서른여덟개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_REG_39_FACE_DONE           AVML_MSG_T2_106			// 서른아홉개 / 얼굴이 등록 되었습니다.. 등록하실 어굴이 제품에 위치한 거울 원 안에 들어오도록 조절하세요. 등록은 자동으로 진행 됩니다. 
#define		AVML_IN_0410PIN_R_FACE           AVML_MSG_T2_107			// 네자리에서 열자리의 사용자 비밀번호를 입력하세요. 설정을 완료 하시려면 R 버튼을 누르세요. 얼굴 등록을 원하시면 * 버튼을 누르세요.
#define		AVML_HELLO_MSG1					AVML_MSG_T2_108			// 당신의 세상을 지키다, 게이트맨
#define		AVML_TIMEOUT					AVML_MSG_T2_109			// 시간이 초과되었습니다.
#define		AVML_CHG_BATTERY				AVML_MSG_T2_110			// 건전지를 교체해주세요.

#define		AVML_OPEN_MSG					AVML_MSG_T2_111			// 열렸습니다.
#define		AVML_CLOSE_MSG					AVML_MSG_T2_112			// 닫혔습니다.
#define		AVML_WRONG_NO_PIN				AVML_MSG_T2_113			// 입력하신 자릿수가 맞지 않습니다.
#define		AVML_ALREADY_USE_MPIN			AVML_MSG_T2_114			// 이미 사용하고 있는 비밀번호 입니다.		// Error / The master code is already in use
#define		AVML_ALREADY_USE_SLOT			AVML_MSG_T2_115			// 이미 사용하고 있는 사용자번호 입니다.
#define		AVML_ALREADY_USE_CARD			AVML_MSG_T2_116			// 이미 사용하고있는 카드키 입니다.
#define		AVML_ALREADY_USE_PIN			AVML_MSG_T2_117			// 이미 사용하고 있는 비밀번호 입니다.		// Error / That code is alread in use
#define		AVML_OUT_FORCE_LOCK				AVML_MSG_T2_118			// 외부강제잠금 상태 입니다.
#define		AVML_IN_FORCE_LOCK				AVML_MSG_T2_119			// 내부강제잠금 상태 입니다.
#define		AVML_ERR_NEED_0410PIN			AVML_MSG_T2_120			// 비밀번호는 네 자리에서 열 자리까지 등록이 가능합니다

#define		AVML_TAMPER_ALARM				AVML_MSG_T2_121			// 파손 경보가 발생하였습니다.
#define		AVML_INVADE_ALARM				AVML_MSG_T2_122			// 침입 경보가 발생하였습니다.
#define		AVML_FIRE_ALARM					AVML_MSG_T2_123			// 화재 경보가 발생하였습니다.
#define		AVML_FACE_POSITION_GUIDE		AVML_MSG_T2_124			// 등록하실 얼굴이 제품에 위치한 거울 원 안에 들어오도록 조절 하세요. 등록은 자동으로 진행 횝니다. 
#define		AVML_ERR_FACE_POSITION		AVML_MSG_T2_125			// 실패하였습니다. 정면이 아닌 다른 여러각도의 이미지는 인식이 어려울 수 있습니다. 다시 시도하시려면 * 버튼을 누르시고 , 종료를 원하시면 R 버튼을 누르세요.
#define		AVML_MALFUNCTION_FPM_MSG		AVML_MSG_T2_126			// 지문인증 모듈이 정상적으로 작동되지 않습니다
#define		AVML_CHECK_FPM_MSG				AVML_MSG_T2_127			// 지문인증 모듈을 점검하세요
#define		AVML_DEL_ALL_FACE_SHARP				AVML_MSG_T2_128			// 얼굴 전체 삭제 모드입니다. 계속 하시려면 # 버튼을 누르세요 
#define		AVML_MAN_SMART_LIVING_SHARP				AVML_MSG_T2_129			// 스마트리빙 등록/삭제 모드입니다. 계속 하시려면 샵 버튼을 누르세요. 
#define		AVML_ERR_FINGER_MSG1			AVML_MSG_T2_130			// 지문이 정상적으로 등록되지 않았습니다.

#define		AVML_ERR_FINGER_MSG2			AVML_MSG_T2_131			// 지문인증 모듈의 이물질을 제거해 주세요.
#define		AVML_ERR_FINGER_MSG3			AVML_MSG_T2_132			// 지문센서가 훼손되었습니다.
#define		AVML_ERR_NOSPACE				AVML_MSG_T2_133			// Error / 저장공간이 부족합니다.
#define		AVML_LHANDED_MSG				AVML_MSG_T2_134			// 좌수로 설정 되었습니다
#define		AVML_RHANDED_MSG				AVML_MSG_T2_135			// 우수로 설정 되었습니다
#define		AVML_RETRY_MSG					AVML_MSG_T2_136			// 다시 시도하세요.
#define		AVML_REG_PIN_DONE				AVML_MSG_T2_137			// 비밀번호가 등록 되었습니다.
#define		AVML_REG_MPIN_DONE				AVML_MSG_T2_138			// 마스터 비밀번호가 등록 되었습니다.
#define		AVML_REGBT1_DELBT3_REGREMOTE_8					AVML_MSG_T2_139			// 블루투스 등록은 1번 , 블루투스 삭제는 3번 , 리모컨 등록은 8번을 누르세요. 
#define		AVML_SMART_LIVING_APP_MSG			AVML_MSG_T2_140			// 등록하실 기기에서 스마트 리빙 앱을 켜신 후 등록 대기 상태로 설정해 주세요 

#define		AVML_ADDSTAR_COMPLETE_R			AVML_MSG_T2_141			// 다른 설정을 하시려면 별표 버튼을 누르시고 , 종료를 원하시면 R 버튼으로 누르세요.
#define		AVML_DEL_ALL_FINGER_DONE_MSG	AVML_MSG_T2_142			// 등록된 모든 지문이 삭제 되었습니다.
#define		AVML_DEL_ALL_CARD_DONE_MSG		AVML_MSG_T2_143			// 등록된 모든 카드키가 삭제 되었습니다.
#define		AVML_REG_BT_SHARP			AVML_MSG_T2_144			// 블루투스 등록 모드 입니다. 계속 하시려면 샵 버튼을 누르세요 
#define		AVML_COMPLETE_SHARP_CONT				AVML_MSG_T2_145			// 완료 되었습니다. 계속 하시려면 샵 버튼을 누르세요 
#define		AVML_3MIN_LOCKED_MSG2			AVML_MSG_T2_146			// 3분락 상태 입니다. 3분이 지난 후에 다시 사용해주세요.
#define		AVML_ERR_CANCLE_REG				AVML_MSG_T2_147			// Error / 등록이 취소 되었습니다.
#define		AVML_ERR_CANCLE_DEL				AVML_MSG_T2_148			// Error / 삭제가 취소 되었습니다.
#define		AVML_ERR_CANCLE_MENU			AVML_MSG_T2_149			// Error / 메뉴 모드가 취소 되었습니다.
#define		AVML_CHG_VOL_SHARP				AVML_MSG_T2_150			// 음량 설정 / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_CHG_VOL_MSG				AVML_MSG_T2_151			// 고음은 1번, 저음은 2번, 무음은 3번을 누르세요
#define		AVML_VOL_HIGH_SHARP				AVML_MSG_T2_152			// 고음으로 설정 되었습니다 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_VOL_LOW_SHARP				AVML_MSG_T2_153			// 저음으로 설정 되었습니다 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_VOL_MUTE_SHARP				AVML_MSG_T2_154			// 무음으로 설정 되었습니다 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_HIGH_VOLUME				AVML_MSG_T2_155			// 고음
#define		AVML_LOW_VOLUME					AVML_MSG_T2_156			// 저음
#define		AVML_SILENT_MODE				AVML_MSG_T2_157			// 무음
#define		AVML_DEL_BT_SHARP						AVML_MSG_T2_158			// 블루투스 삭제 모드입니다. 계속 하시려면 샵 버튼을 누르세요 
#define		AVML_MAN_FACE_SHARP						AVML_MSG_T2_159			// 얼굴 등록 / 삭제 모드 입니다. 계속 하시려면 샵 버튼을 누르세요. 
#define		AVML_AUTO_RELOCK_SHARP			AVML_MSG_T2_160			// 자동잠금 설정모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_TURNON1_TURNOFF3			AVML_MSG_T2_161			// 설정은 1번, 해제는 3번을 누르세요.
#define		AVML_TURNON_CONT_SHARP			AVML_MSG_T2_162			// 설정되었습니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_TURNOFF_CONT_SHARP			AVML_MSG_T2_163			// 해제되었습니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REGFACE1_DELFACE3		AVML_MSG_T2_164			// 얼굴 등록은 1번 얼굴 삭제는 3번을 누르세요 
#define		AVML_REG_FACE_SHARP	AVML_MSG_T2_165			// 얼굴 등록 모드 입니다. 계속 하시려면 샵 버튼을 누르세요 
#define		AVML_DEL_FACE_SHARP		AVML_MSG_T2_166			// 얼굴 삭제 모드입니다. 계속 하시려면 샵 버튼을 누르세요 
#define		AVML_DEL_ALL_DIGITAL_KEY_SHARP					AVML_MSG_T2_167			// 디지털 키 전체 삭제 모드입니다. 계속 하시려면 샵 버튼을 누르세요 
#define		AVML_DEL_ALL_FINGER_SHARP				AVML_MSG_T2_168			// 지문 전체 삭제 모드입니다. 계속 하시려면 샵 버튼을 누르세요 
#define		AVML_HELLO_MSG2				AVML_MSG_T2_169			// 국문 : 당신의 세상을 지키다 게이트맨  그외 : Welcome to the Talw digital world 
#define		AVML_ARM_OK						AVML_MSG_T2_170			// 경비가 설정 되었습니다.

#define		AVML_ARM_FAIL					AVML_MSG_T2_171			// 경비 설정이 실패하였습니다.
#define		AVML_DOORWINDOW_OPEN_ERR		AVML_MSG_T2_172			// 문 또는 창문이 열려 있습니다.
#define		AVML_SYS_ARM					AVML_MSG_T2_173			// 시스템 경비가 설정되었습니다.
#define		AVML_SYS_DISARM					AVML_MSG_T2_174			// 시스템 경비가 해제되었습니다.
#define		AVML_DISARM_FAIL				AVML_MSG_T2_175			// 경비 해제에 실패하였습니다.
#define		AVML_ALARM_STILLACT				AVML_MSG_T2_176			// 경비 기능이 여전히 작동중일 수 있습니다.
#define		AVML_ENTER_FINGER_3TIME			AVML_MSG_T2_177			// 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_REG_FINGER_ERR				AVML_MSG_T2_178			// Error / 지문이 정상적으로 등록되지 않았습니다. / 다시 시도하세요.
#define		AVML_OUT_FORCE_LOCK_IS_ON		AVML_MSG_T2_179			// 외부강제잠금 상태 입니다. / 으로 설정 되었습니다.
#define		AVML_DEL_VPIN_SHARP			AVML_MSG_T2_180			// 방문자 비밀번호 삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_BLEKEY_REG					AVML_MSG_T2_181			// 블루투스키가 등록 되었습니다.
#define		AVML_REG_VPIN_CONT_SHARP		AVML_MSG_T2_182			// 방문자 비밀번호 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REG_ADDREMOCON				AVML_MSG_T2_183			// 추가로 등록할 리모콘을 차례대로 입력하세요.
//#define										AVML_MSG_T2_184			// 
#define		AVML_FINGER01_REG_MSG			AVML_MSG_T2_185			// 한 개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER02_REG_MSG			AVML_MSG_T2_186			// 두개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER03_REG_MSG			AVML_MSG_T2_187			// 세개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER04_REG_MSG			AVML_MSG_T2_188			// 네개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER05_REG_MSG			AVML_MSG_T2_189			// 다섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER06_REG_MSG			AVML_MSG_T2_190			// 여섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.

#define		AVML_FINGER07_REG_MSG			AVML_MSG_T2_191			// 일곱개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER08_REG_MSG			AVML_MSG_T2_192			// 여덟개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER09_REG_MSG			AVML_MSG_T2_193			// 아홉개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER10_REG_MSG			AVML_MSG_T2_194			// 열개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER11_REG_MSG			AVML_MSG_T2_195			// 열한개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER12_REG_MSG			AVML_MSG_T2_196			// 열두개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER13_REG_MSG			AVML_MSG_T2_197			// 열세개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER14_REG_MSG			AVML_MSG_T2_198			// 열네개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER15_REG_MSG			AVML_MSG_T2_199			// 열다섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER16_REG_MSG			AVML_MSG_T2_200			// 열여섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.

#define		AVML_FINGER17_REG_MSG			AVML_MSG_T2_201			// 열일곱개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER18_REG_MSG			AVML_MSG_T2_202			// 열여덟개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER19_REG_MSG			AVML_MSG_T2_203			// 열아홉개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER20_REG_MSG			AVML_MSG_T2_204			// 스무개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER21_REG_MSG			AVML_MSG_T2_205			// 스물한개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER22_REG_MSG			AVML_MSG_T2_206			// 스물두개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER23_REG_MSG			AVML_MSG_T2_207			// 스물세개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER24_REG_MSG			AVML_MSG_T2_208			// 스물네개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER25_REG_MSG			AVML_MSG_T2_209			// 스물다섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER26_REG_MSG			AVML_MSG_T2_210			// 스물여섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.

#define		AVML_FINGER27_REG_MSG			AVML_MSG_T2_211			// 스물일곱개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER28_REG_MSG			AVML_MSG_T2_212			// 스물여덟개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER29_REG_MSG			AVML_MSG_T2_213			// 스물아홉개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER30_REG_MSG			AVML_MSG_T2_214			// 서른개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER31_REG_MSG			AVML_MSG_T2_215			// 서른한개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER32_REG_MSG			AVML_MSG_T2_216			// 서른두개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER33_REG_MSG			AVML_MSG_T2_217			// 서른세개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER34_REG_MSG			AVML_MSG_T2_218			// 서른네개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER35_REG_MSG			AVML_MSG_T2_219			// 서른다섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER36_REG_MSG			AVML_MSG_T2_220			// 서른여섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.

#define		AVML_FINGER37_REG_MSG			AVML_MSG_T2_221			// 서른일곱개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER38_REG_MSG			AVML_MSG_T2_222			// 서른여덟개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER39_REG_MSG			AVML_MSG_T2_223			// 서른아홉개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_FINGER40_REG_MSG			AVML_MSG_T2_224			// 마흔개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 입력하세요.
#define		AVML_PRESS_SHARP_CONT			AVML_MSG_T2_225			// 계속하시려면 샵 버튼을 누르세요.
#define		AVML_IN_0410MPIN_R				AVML_MSG_T2_226			// 네자리에서 열자리의 마스터 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_MAN_HANDLING_SHARP			AVML_MSG_T2_227			// 좌수 우수 설정 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_PRESS_SHARP_AUTOHANDLE		AVML_MSG_T2_228			// 샵 버튼을 누르시면 좌수 우수가 자동으로 설정됩니다.
#define		AVML_IN_MPIN_SHARP				AVML_MSG_T2_229			// 마스터 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.		// Enter the master code / then press the hash to continue

#ifdef TKEY_CIRCLE_BTN_TYPE
#define		AVML_REG_0410PIN_R_FINGER_C		AVML_MSG_T2_005         // 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 지문 등록을 원하시면 원형 아이콘을 누르세요.
#define		AVML_IN_0410PIN_R_FINGER_C		AVML_MSG_T2_005			// 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 지문 등록을 원하시면 [원형 아이콘]을 누르세요.
#else
#define		AVML_REG_0410PIN_R_FINGER_C		AVML_MSG_T2_005         // 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 지문 등록을 원하시면 원형 아이콘을 누르세요.
#define		AVML_IN_0410PIN_R_FINGER_C		AVML_MSG_T2_230			// 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 지문 등록을 원하시면 *버튼을 누르세요.
#endif

#define		AVML_REG_FINGER01_REIN			AVML_MSG_T2_231			// 지문이 한번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_REG_FINGER02_REIN			AVML_MSG_T2_232			// 지문이 두번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_REG_FINGER03_REIN			AVML_MSG_T2_233			// 지문이 세번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_REG_FINGER04_REIN			AVML_MSG_T2_234			// 지문이 네번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_REG_FINGER05_REIN			AVML_MSG_T2_235			// 지문이 다섯번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_REG_FINGER06_REIN			AVML_MSG_T2_236			// 지문이 여섯번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_REG_FINGER07_REIN			AVML_MSG_T2_237			// 지문이 일곱번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_REG_FINGER08_REIN			AVML_MSG_T2_238			// 지문이 여덟번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_ENTER_NUMBER				AVML_MSG_T2_239			// 번호를 입력하세요.
#define		AVML_DOUBLE_OC_MENU				AVML_MSG_T2_240			// 이중 열림/닫힘 버튼 설정 모드입니다.

#define		AVML_DOUBLE_OC_ON1_OFF3			AVML_MSG_T2_241			// 이중 열림/닫힘 버튼 설정은 1번, 해제는 3번을 누르세요.
#define		AVML_DOUBLE_OC_ON_SHARP			AVML_MSG_T2_242			// 이중 열림/닫힘 버튼 설정 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_DOUBLE_OC_OFF_SHARP		AVML_MSG_T2_243			// 이중 열림/닫힘 버튼 해제 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_REG_FINGER_PIN_CARD_R		AVML_MSG_T2_244			// 지문 등록을 원하시면 *버튼을 누르세요. / 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 카드 등록을 원하시면 카드를 리더기에 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_IN_MPIN_SHARP2				AVML_MSG_T2_245			// 마스터 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.		// Enter the master code / press the hash to continue
#define		AVML_ENTER_FINGER				AVML_MSG_T2_246			// 등록하실 지문을 차례로 입력 하세요. 
#define		AVML_OUT_FORCE_UNLOCK			AVML_MSG_T2_247			// 외부 강제 잠금이 해제 되었습니다. 
#define		AVML_IN_FORCE_UNLOCK				AVML_MSG_T2_248			// 내부 강제 잠금이 해제 되었습니다. 
#define		AVML_MAN_DIGITALKEY_SHARP		AVML_MSG_T2_249			// 디지털 키 등록/삭제 메뉴입니다. 계속 하시려면 샵 버튼을 누르세요 
//#define										AVML_MSG_T2_250			//

//#define										AVML_MSG_T2_251			//
//#define										AVML_MSG_T2_252			//
//#define										AVML_MSG_T2_253			//
//#define										AVML_MSG_T2_254			//
//#define										AVML_MSG_T2_255			//



#endif

#endif


