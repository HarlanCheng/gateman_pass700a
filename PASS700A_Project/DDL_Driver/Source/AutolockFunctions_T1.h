//------------------------------------------------------------------------------
/** 	@file		AutolockFunctions_T1.h
	@brief	AutoRelock Process
*/
//------------------------------------------------------------------------------

#ifndef __AUTOLOCKFUNCTIONS_T1_INCLUDED
#define __AUTOLOCKFUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"



//------------------------------------------------------------------------------
// gLockStatusPrcsStep에서 참조 - 자동 잠김 수행 단계
//------------------------------------------------------------------------------
enum{
	LOCKSTS_AUTOLOCK_CHECK = 1,
	LOCKSTS_AUTOLOCK_CLOSE,
	LOCKSTS_AUTOLOCK_CLOSED_CHK,
	LOCKSTS_AUTOLOCK_CLOSED_ERROR,
	LOCKSTS_ALARM_CHECK,
	LOCKSTS_AUTOLOCK_OPENED_CHK,
/*#ifdef	__DDL_MODEL_FRONTDOORSUITE		
	LOCKSTS_MAIN_MOTOR_AUTO_OPEN_CHECK,
	LOCKSTS_MAIN_MOTOR_AUTO_OPENED_CHK,
#endif*/
};


//------------------------------------------------------------------------------
/*! 	\def	_MAX_AUTOLOCK_TRYCNT		
	gAutoLockTryCnt에서 참조- 자동 잠김 수행 최대 회수, 총 2회 구동
*/
//------------------------------------------------------------------------------
#define	_MAX_AUTOLOCK_TRYCNT			2


//------------------------------------------------------------------------------
/*! 	\def	DEFAULT_AUTOLOCK_TIME		
	gLockStatusTimer1s에서 참조 - 문이 닫힐 때 3초 자동 잠김 시간
	
	\def	DEFAULT_RELOCK_TIME		
	gLockStatusTimer1s에서 참조 - 문이 닫혀 있을 때 7초 자동 잠김 시간
*/
//------------------------------------------------------------------------------
#define	DEFAULT_AUTOLOCK_TIME			3
#define	DEFAULT_RELOCK_TIME				7
//#define	MIN_RELOCK_TIME					7
//#define	MAX_RELOCK_TIME					60


#if defined (__DDL_MODEL_FRONTDOORSUITE) || defined (__DDL_MODEL_SCREENDOOR)
#define AUTOLOCK_DEFAULT_TIME_MSB	0
#define AUTOLOCK_DEFAULT_TIME_LSB	3
#define AUTORELOCK_DEFAULT_TIME_MSB	0
#define AUTORELOCK_DEFAULT_TIME_LSB	30		
#else 
#define AUTOLOCK_DEFAULT_TIME_MSB	0
#define AUTOLOCK_DEFAULT_TIME_LSB	3
#define AUTORELOCK_DEFAULT_TIME_MSB	0
#define AUTORELOCK_DEFAULT_TIME_LSB	7
#endif


//extern BYTE gLockStatusTimer1s;
extern WORD gLockStatusTimer100ms;



void LockStatusCheckTimeCount(void);
void LockStatusCheckTimeClear(void);
void LockStatusCheckClear(void);

BYTE AutoLockSetCheck(void);
BYTE AutoRelockProcessClearCheck(void);
//2019년 11월 11일 심재철 추가 시작 
//Credential로 Lock이 된 이후에 모듈에서 Lock Status를 조회할 때 Locked가 아닌 다른 값으로 응답하는 상황을 개선 하기 위함
//이유는 Lock Status를 수신 했을때 gLockStatusTimer1s가 초기화 되지 않기 때문임
void AutoRelockClear(void);


void AutoRelockEnable(void);
void AutoRelockDisable(void);
void AutoRelockTimeCounter(void);
void AutoRelockTime13Counter(void);
void LockStatusProcess(void);
BYTE GetLockStatusPrcsStep(void);

#ifdef	P_SNS_EDGE_T			// Edge sensor
void DoorClosedCheckCounter(void);
void DoorClosedTimeSet(void);
BYTE DoorClosedTimeCheck(void);
#endif

// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 시작
#ifdef		P_SNS_LOCK_T	
extern BYTE gbInnerForceLockDetectionValue;
extern BYTE gbInnerForceLockDetectionValueBackUp;
#endif
// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 끝


//BYTE GetRemainingAutoRelockTime(void);
WORD GetRemainingAutoRelockTime(void);


#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
extern BYTE gbFeedbackCloseOnceTimercnt100ms;
#endif

#ifdef DDL_CFG_AUTO_KEY_PROCESS
void AutoSwProcess(void);
#endif 

void AutoLockTimeAndAutoReLockTimeCalculationAndWrite(BYTE* bpData);
void AutoLockTimeAndAutoReLockTimeCalculationAndRead(void);
void AutoLockTimeAndAutoReLockTimeDefaultSetAndWrite(void);


extern WORD gwAutoLockTimeTLV;
extern WORD gwAutoReLockTimeTLV; 


#ifdef	DDL_AAAU_AUTOLOCK_HOLD //호주 및 뉴질랜드 전용 autolock hold 기능
BYTE  AutolockHoldSettingLoad(void);
void AutolockHoldSetting(BYTE bVal);
extern BYTE gbAutolockHold;
#define AUTOLOCKHOLD		0xcc
#define AUTOLOCKUNHOLD		0xdd

extern BYTE gAutolockHoldClearTimer1s;
extern BYTE gbAutolockHoldAutoClear;
void AutolockHoldClearTimerCounter(void);
void AutolockHoldClearTimerCounterClear(void);
/*
BYTE  SecureUsedSettingLoad(void);
void SecureUsedSetting(BYTE bVal);
extern BYTE gbSecureModeUsed; 
*/
extern BYTE gbAutolockTimeValue;

//void AutoUnlockCompleteCheck(void);
/*
extern BYTE gLockStatusMainMotorAutoOpenCheckTimer100ms;
void LockStatusMainMotorAutoOpenCheckTimer(void);
void LockStatusMainMotorAutoOpenCheckTimerClear(void);
*/
#endif	//DDL_AAAU_AUTOLOCK_HOLD #endif

#ifdef USED_SECOND_MOTOR
BYTE  AutoChangePassageModeLoad(void);
void AutoChangePassageModeLSetting(BYTE bVal);
extern BYTE gbAutoChangepassagemodevalue;

#define AUTOCHANGEPASSMODESTEP_CHECK	0XAA
#define AUTOCHANGEPASSMODESTEP_REDY	0XAC
#define AUTOCHANGEPASSMODESTEP_DONE	0xAE

#endif	//USED_SECOND_MOTOR #endif

#endif

