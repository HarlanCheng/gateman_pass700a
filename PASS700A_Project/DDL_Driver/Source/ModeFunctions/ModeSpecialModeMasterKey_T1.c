//------------------------------------------------------------------------------
/** 	@file		ModeSpecialModeMasterKey_T1.c
	@version 0.1.00
	@date	2016.10.31
	@brief	DS1972 TouchKey Register Mode For Fingerprint Product 
	@remark	B2B에 납품되는 지문 주키 제품을 위한 마스터키 터치키 등록 모드
	@see	MainModeProcess_T1.c
	@see	DS1972_Functions_T1.c
	@see	DS1972_IButtonProcess_T1.c	
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.10.31		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


#ifdef	_MASTERKEY_IBUTTON_SUPPORT


BYTE gbMasterKeyEnabled = 0;

void MasterKeyEnable(void)
{
	IBUTTON_DATA_MODE(INPUT);
	__HAL_GPIO_EXTI_CLEAR_IT(MASTERKEY_Pin);
	HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
}

void MasterKeyDisable(void)
{
	__HAL_GPIO_EXTI_CLEAR_IT(MASTERKEY_Pin);
	HAL_NVIC_DisableIRQ(EXTI0_IRQn);
}


void ModeGotoSpecialModeMasterKeyRegister(void)
{
	iButtonGotoRegModeStart();
	ModeGotoDS1972TouchKeyRegister();

	FeedbackKeyPadLedOn(VOICE_MIDI_INTO, VOL_CHECK);
}

void ModeGotoSpecialModeMasterKeyVerify(void)
{
	if(gbMasterKeyEnabled == 1)
	{
		DS1972TouchKeyDetectionProcess();
	}
}


void ModeGotoiButtonWithoutFloatingID(void)
{
	gbiButtonMode = IBTNMODE_FID_CHECK;
	gbiButtonPrcsStep = IBTNPRCS_MASTER_SAVE;
}


bool IsMasterKeyEnabled(void)
{
	if(gbMasterKeyEnabled == 1)		return (true);
	else							return (false);
}

void MasterKeyEnabledSet(void)
{
	gbMasterKeyEnabled = 1;
}


void MasterKeyEnabledClear(void)
{
	AllDS1972TouchKeyDelete();

	gbMasterKeyEnabled = 0;
}



void MasterKeyEnableCheck(void)
{
	BYTE bCnt;
	BYTE KeyUidData[MAX_KEY_UID_SIZE];

	gbMasterKeyEnabled = 0;

	for(bCnt = 0; bCnt < SUPPORTED_DS1972KEY_NUMBER; bCnt++)
	{
		RomRead(KeyUidData, TOUCHKEY_UID+(MAX_KEY_UID_SIZE*(WORD)bCnt), MAX_KEY_UID_SIZE);
	
		if(DataCompare(KeyUidData, 0xFF, MAX_KEY_UID_SIZE) == _DATA_IDENTIFIED)
		{
			continue;
		}
		else
		{
			gbMasterKeyEnabled = 1;
			return;
		}
	}
}




#endif	// _MASTERKEY_IBUTTON_SUPPORT

