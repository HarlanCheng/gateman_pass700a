//------------------------------------------------------------------------------
/** 	@file		PincodeFunctions_T1.h
	@brief	Pincode Process Functions
*/
//------------------------------------------------------------------------------


#ifndef __PINCODEFUNCTIONS_T1_INCLUDED
#define __PINCODEFUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"
//nclude "DefinePin.h"


//------------------------------------------------------------------------------
/*! 	\def	MIN_PIN_LENGTH		
	최소 허용 입력 비밀번호 자리수 
	
	\def	MAX_PIN_LENGTH		
	최대 허용 입력 비밀번호 자리수 
*/
//------------------------------------------------------------------------------
#ifndef	MIN_PIN_LENGTH
#define	MIN_PIN_LENGTH			4
#endif

#ifndef	MAX_PIN_LENGTH
#define	MAX_PIN_LENGTH			10
#endif

#ifndef	MAX_FIX_PIN_LENGTH
#define	MAX_FIX_PIN_LENGTH		4
#endif


//------------------------------------------------------------------------------
/*! 	\def	TENKEY_INPUT_OK		
	TenKeySave 함수의 Return 값 - TEN KEY 입력 처리 성공
	
	\def	TENKEY_INPUT_SAME		
	TenKeySave 함수의 Return 값 - TEN KEY 입력 자리수가 정해진 한계값과 동일 

	\def	TENKEY_INPUT_OVER		
	TenKeySave 함수의 Return 값 - TEN KEY 입력 자리수가 정해진 한계값보다 클 경우 
*/
//------------------------------------------------------------------------------
#define	TENKEY_INPUT_OK			0
#define	TENKEY_INPUT_SAME		1
#define	TENKEY_INPUT_OVER		2

#ifdef DDL_CFG_EMERGENCY_119
extern BYTE gPinInputKeyFromBeginBuf[(MAX_PIN_LENGTH>>1)+2];
#else 
extern BYTE gPinInputKeyFromBeginBuf[MAX_PIN_LENGTH>>1];
#endif 
extern BYTE gPinInputKeyCnt;


#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
extern BYTE gPinInputMinLength;
#else
#ifdef	P_VOICE_RST // defined (DDL_CFG_SUPPORT_CHINESE)
extern BYTE gPinInputMinLength;
#endif 
#endif

extern const BYTE ShiftDayOfWeekCheck[8];


void TenKeyVariablesClear(void);
BYTE TenKeySave(BYTE LimitKeyCnt, BYTE NewInputKey, BYTE FakeEnable);
BYTE PincodeVerify(BYTE FakeEnable);
BYTE ScheduleVerify(BYTE *pCurrentTimeBuf, BYTE SlotNumber);
void UpdatePincodeToMemory(WORD Address);
void CopyPincodeForDisplay(BYTE *pTargetBuf);
void UpdatePincodeToMemoryByModule(BYTE *pPincodeData, BYTE ByteLength, WORD SlotNumber, BYTE PincodeLength);

BYTE GetInputKeyCount(void);


//------------------------------------------------------------------------------
/*! 	\def	RET_TEMP_CODE_MATCH		
	PincodeVerify 함수의 Return 값 - 입력된 Pincode와 Onetime Code 일치
	
	\def	RET_MASTER_CODE_MATCH		
	PincodeVerify 함수의 Return 값 - 입력된 Pincode와 Master Code 일치

	\def	RET_NO_INPUT		
	PincodeVerify 함수의 Return 값 - 입력된 Key 없음

	\def	RET_WRONG_DIGIT_INPUT		
	PincodeVerify 함수의 Return 값 - 입력된 Key가 정해진 자리수에 맞지 않음

	\def	RET_NO_MATCH		
	PincodeVerify 함수의 Return 값 - 입력된 Pincode와 일치하는 Code 없음
*/
//------------------------------------------------------------------------------
#define	RET_TEMP_CODE_MATCH				0xF0
#define	RET_MASTER_CODE_MATCH			0xFB
#define	RET_2BYTE_MASTER_CODE_MATCH	0xFFEE
#define	RET_NO_INPUT					0xF1
#define	RET_WRONG_DIGIT_INPUT			0xF2
#define	RET_NO_MATCH					0xF3


extern BYTE gbTimeBuff[3];

void TimeDataClear(void);
void TimeDataSet(BYTE* pData);
bool IsGetTimeDataCompleted(void);

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
extern BYTE gbTimeBuff_Ble30[7];
#endif


//==============================================================
//	TEST FUNCTIONS
//==============================================================
// 아래 함수는 테스트 대상 함수가 수정되었을 때 제대로 동작하는지 여부를 확인하기 위한 테스트 함수임

#if 0
BYTE UnitTestTenKeySave(void);
BYTE UnitTestSchedule_Type1(void);
#endif


#endif

