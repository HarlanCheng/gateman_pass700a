//------------------------------------------------------------------------------
/** 	@file		FelicaFunctions_T1.h
	@brief	8 bytes Felica Card IDm Read Functions using TRF7970
*/
//------------------------------------------------------------------------------


#ifndef __FELICAFUNCTIONS_T1_INCLUDED
#define __FELICAFUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"


//=============================================================
//	FELICA COMMAND
//=============================================================
//------------------------------------------------------------------------------
/*! 	\def	FELICA_POLLING		
	FELICA Command - REQC (00h)

*/
//------------------------------------------------------------------------------

#define FELICA_POLLING		0x00


void Felica_Polling_Send(void);
BYTE Felica_Polling_Respond(void);
BYTE FelicaFindTag(void);
void CopyFelicaCardUid(BYTE *TargetBuf);



#endif
